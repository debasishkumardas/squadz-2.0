package com.andolasoft.squadz.utils;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by Biplab on 12/13/2015.
 */
public interface OnOperItemClickL {
    void onOperItemClick(AdapterView<?> parent, View view, int position, long id);
}
