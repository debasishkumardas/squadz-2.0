package com.andolasoft.squadz.utils;

import com.andolasoft.squadz.R;

/**
 * Created by Debasish Kumar Das on 4/25/2017.
 */
public class SportsSurfaceTypePicker {

    public static int getSportSurfaceType(String sport){
        int sportImage = 0;
        if(sport != null && sport.contains("Basketball")){
            sportImage = R.mipmap.ic_basketball_length;
        }else if(sport != null && sport.contains("Field")){
            sportImage = R.mipmap.ic_field_surface;
        }else if(sport != null && sport.contains("Tennis")){
            sportImage = R.mipmap.ic_tennis_volleyball_surface;
        }else if(sport != null && sport.contains("Volleyball")){
            sportImage = R.mipmap.ic_tennis_volleyball_surface;
        }else if(sport != null && sport.contains("Golf")){
            sportImage = R.mipmap.ic_golf_numberholes;
        }
        return sportImage;
    }

    public int getGolfHandicaps(String sport){
        int sportImage = 0;
        if(sport != null && sport.equalsIgnoreCase("Golf")){
            sportImage = R.mipmap.ic_golf_handicap;
        }
        return sportImage;
    }

    public int getIndoorOutdoorIcons(String type){
        int typeImage = 0;
        if(type != null && type.equalsIgnoreCase("indoor")){
            typeImage = R.mipmap.ic_court_indoor;
        }else if(type != null && type.equalsIgnoreCase("outdoor")){
            typeImage = R.mipmap.ic_court_outdoor;
        }
        return typeImage;
    }
}
