package com.andolasoft.squadz.utils;

/**
 * This Class designed only to handle the server side end points. This class is having end points for
 * Development
 * Staging
 * Production Environments.
 *
 * This class also holding the end point for firebase
 * @author Debasish Kumar Das
 * @since  2017-01-01
 * @version 1.0
 */

public class EndPoints {


   // public static final String BASE_URL = "http://54.164.124.188/api/v1/";

   /* public static final String BASE_URL = "http://54.164.124.188/api/v1/";

    //public static final String BASE_URL = "http://192.168.2.160:3000/";

    //Staging Server Deep Linking End Point
    public static final String BASE_URL_DEEP_LINKING = "http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/";
    //Staging Server Venue Model End Point
    public static final String BASE_URL_VENUE = "http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/api/";

    public static final String BASE_URL_FIREBASE = "https://squadz-dream-734.firebaseio.com/";*/

    //public static final String BASE_URL = "http://34.192.117.108/api/";
   /* public static final String BASE_URL_VENUE = "http://34.192.117.108/api/";
    public static final String BASE_URL_DEEP_LINKING = "http://34.192.117.108/";

    //Firebase End Point
    public static final String BASE_URL_FIREBASE = "https://squadz-dream-734.firebaseio.com/";
*/

   // public static final String BASE_URL = "http://54.164.124.188/api/v1/";

   /* public static final String BASE_URL = "http://54.164.124.188/api/v1/";
    public static final String BASE_URL_VENUE = "http://34.192.117.108/api/";
    //public static final String BASE_URL_VENUE_MIGRATION = "http://34.192.117.108/api/";
    public static final String BASE_URL_DEEP_LINKING = "http://34.192.117.108/";
    public static final String BASE_URL_FIREBASE = "https://squadz-dream-734.firebaseio.com/";



   /* //Dev Server End Points

    public static final String BASE_URL = "http://54.164.124.188/api/v1/";
    //public static final String BASE_URL = "http://192.168.2.109:3000/";
    //Dev Server Deep Linking End Point
    public static final String BASE_URL_VENUE = "http://34.192.117.108/api/";
    //Dev Server Venue Model End Point
    public static final String BASE_URL_DEEP_LINKING = "http://34.192.117.108/";

    //public static final String BASE_URL_VENUE = "http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/api/";
    //public static final String BASE_URL_VENUE = "http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/api/";
    //Dev Server URL
//    public static final String BASE_UR
// L = "http://54.164.124.188/api/v1/";
    //public static final String BASE_URL_VENUE = "http://34.192.117.108/api/";
//    public static final String BASE_URL_DEEP_LINKING = "http://34.192.117.108/";
    //public static final String BASE_URL_VENUE = "http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/api/";

    /**/

    //Dev Server URL
   // public static final String BASE_URL = "http://54.164.124.188/api/v1/";
    //public static final String BASE_URL = "http://192.168.2.109:3000/";
    //Dev Server URL
    //public static final String BASE_URL = "http://54.164.124.188/api/v1/";
    //public static final String BASE_URL_VENUE_MIGRATION = "http://34.192.117.108/api/";
    //public static final String BASE_URL = "http://192.168.2.160:3000/";
    public static final String BASE_URL = "http://34.192.117.108/api/";
    //public static final String BASE_URL = "http://54.164.124.188/api/v1/";
    public static final String BASE_URL_VENUE_MIGRATION = "http://34.192.117.108/api/";
    //public static final String BASE_URL = "http://192.168.2.160:3000/";
    public static final String BASE_URL_VENUE = "http://34.192.117.108/api/";
    public static final String BASE_URL_DEEP_LINKING = "http://34.192.117.108/";

    //Firebase End Point
    public static final String BASE_URL_FIREBASE = "https://squadz-dream-734.firebaseio.com/";
}
