package com.andolasoft.squadz.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Debasish Kumar Das on 1/14/2017.
 */
public class ImageUploadManager {
    private static final int INDEX_NOT_CHECKED = -1;
    private TransferUtility transferUtility;
    private static BasicAWSCredentials sCredProvider;
    private SimpleAdapter simpleAdapter;
    private List<TransferObserver> observers;
    private TransferObserver observer;
    private ArrayList<HashMap<String, Object>> transferRecordMaps;
    public static final String ARG_FILE_PATH = "file_path";
    public static final String UPLOAD_STATE_CHANGED_ACTION = "com.readystatesoftware.simpl3r.example.UPLOAD_STATE_CHANGED_ACTION";
    public static final String UPLOAD_CANCELLED_ACTION = "com.readystatesoftware.simpl3r.example.UPLOAD_CANCELLED_ACTION";
    public static final String S3KEY_EXTRA = "s3key";
    public static final String PERCENT_EXTRA = "percent";
    public static final String MSG_EXTRA = "msg";
    private static final int NOTIFY_ID_UPLOAD = 1337;
    String s3ObjectKey;
    private static AmazonS3Client s3Client;
    private NotificationManager nm;
    Context mActivity;
    String file_name;
    Dialog profilePicuploadDialog = null;
    int counter = 0;
    String imagePath = null, image_path = null;
    private int checkedIndex;
    String fullPath;
    public static String profilePicS3Url = null, uploadedFileName = null;


    public ImageUploadManager(Context activity, String filePath){
        mActivity = activity;
        this.fullPath = filePath;
        uploadToAWS(filePath);
        Constants.isImageUploaded = true;
    }



    private static BasicAWSCredentials getCredProvider(Context context) {
        if(sCredProvider == null) {
            sCredProvider = new BasicAWSCredentials(AppConstants.AWS_ACCESS_KEY, AppConstants.AWS_SECRET_KEY);
        }
        return sCredProvider;
    }

    private class UploadListener implements TransferListener {
        private UploadListener() {
        }

        public void onError(int id, Exception e) {
            updateList();
        }

        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            updateList();
        }

        public void onStateChanged(int id, TransferState newState) {
            updateList();
        }
    }






    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }


    private void updateList() {
        int progress = (int) ((double) observer.getBytesTransferred() * 100 / observer
                .getBytesTotal());
        if (progress == 100 ) {
            if (s3Client == null) {
                s3Client = new AmazonS3Client(getCredProvider(mActivity.getApplicationContext()));
            }
            counter = counter + 1;
            LoaderUtility.handleLoader(mActivity, false);
            String formattedFireName = getFullFilename(imagePath);
            profilePicS3Url = getProfilePictureUrl(AppConstants.AWS_BUCKET_FOLDER_NAME + file_name);
            uploadedFileName = file_name;
            Log.d("URL", profilePicS3Url);

            }
        }



    /**
     * Get the formatted File name
     * @param filename
     * @return
     */
    public String getFullFilename(String filename){
        file_name = file_name.replace(" ", "_");
        file_name = file_name.replace("-", "_");
        String[] file_arr =  file_name.split("\\.(?=[^\\.]+$)");
        String base_name = file_arr[0];
        String ext_type = file_arr[1];
        base_name = base_name.replaceAll("[#%@$(,`~;\\\\\\\\/:+*?\\\")<>|&']", "_");
        file_name = base_name + "."+ext_type;
        return file_name;
    }

    /**
     * Function responsible for getting the suer Profile Picture s3 Url
     * @return
     */
    public String getProfilePictureUrl(String fileName){
        String s3URL = s3Client.getResourceUrl(AppConstants.AWS_BUCKET_NAME, fileName);
        return s3URL;
    }

    /**
     * Function responsible for getting the suer Profile Picture s3 Url
     * @return
     */
    public static String getProfilePictureUrlAgain(String fileName){
        String s3URL = s3Client.getResourceUrl(AppConstants.AWS_BUCKET_NAME, fileName);
        return s3URL;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            profilePicuploadDialog = ProgressDialog
                    .show(mActivity, "", "Please wait ...");
            profilePicuploadDialog.getWindow().setGravity(Gravity.CENTER);
            profilePicuploadDialog.setCancelable(false);
        }else{
            profilePicuploadDialog.dismiss();
        }
    }


    /*
	 * Begins to upload the file specified by the file path.
	 */
    private void beginUpload(String filePath) {
        if (filePath == null) {
            Toast.makeText(mActivity,
                    "Could not find the file path of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath);
        LoaderUtility.handleLoader(mActivity, true);
        file_name = file.getName();
        file_name = file_name.replace(" ", "_");
        file_name = file_name.replace("-", "_");
        file_name = file_name.replaceAll("[#%@$(,`~;\\\\\\\\/:+*?\\\")<>|&']", "_");
        AppConstants.UPLOADED_IMAGE_FILE_NAME = file_name;
        observer = transferUtility.upload(AppConstants.AWS_BUCKET_NAME,AppConstants.AWS_BUCKET_FOLDER_NAME + file_name,
                file);
        observers.add(observer);
        HashMap<String, Object> map = new HashMap<String, Object>();
        Util.fillMap(map, observer, false);
        observer.setTransferListener(new UploadListener());
    }


    public void uploadToAWS(String fiuuPath){
        transferUtility = Util
                .getTransferUtility(mActivity);
        observers = transferUtility
                .getTransfersWithType(TransferType.UPLOAD);
        transferRecordMaps = new ArrayList<HashMap<String, Object>>();
        checkedIndex = INDEX_NOT_CHECKED;
        Uri tempUri;
        File file = new File(fiuuPath);
        /*long length = file.length();
        float exact_length = length / 1024;
        exact_length = exact_length / 1024;*/
        /*** round exact lenth to 2 decimal places ***/
        //String fileSize = String.format("%.2f", exact_length);

       // if(Constants.isPhotoTaken) {
            //db.insertUploadDetails(qrcode, fullPath, image_type,
                   // edit_message.getText().toString(),fileSize,"camera");
       // }else {
            //db.insertUploadDetails(qrcode, fullPath, image_type,
              //      edit_message.getText().toString(),fileSize,"gallery");
       // }
        //if (cd.isConnectingToInternet()) {
            beginUpload(fullPath);
        //} else {
          //  Toast.makeText(
            //        UploadActivity_new.this,
              //      "Please connect to a working internet connection",
                //    Toast.LENGTH_LONG).show();
        //}
    }



}
