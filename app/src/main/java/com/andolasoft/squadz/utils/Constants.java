package com.andolasoft.squadz.utils;

import java.util.ArrayList;

/**
 * Created by Debasish Kumar Das on 1/3/2017.
 */

public class Constants {

    //Normal Login API Parameters
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_FCMID = "registration_id";
    public static final String PARAM_DEVICETOKEN = "device_token";
    public static final String PARAM_PUSHNOTIFICATION_STATUS = "push_notification";
    public static final String PARAM_DEVICETYPE = "device_type";
    public static final String PARAM_LOCATION= "location";
    public static final String PARAM_TIMEZONE= "time_zone";
    public static final String PARAM_EMAIL= "email";
    public static final String PARAM_PHONENUMBER= "phone_no";
    public static final String PARAM_SPORTID= "sport_id";
    public static final String PARAM_SKILLLEVEL= "skill_level";
    public static final String PARAM_ABOUTME= "about_me";
    public static final String PARAM_BIRTHDATE= "birth_date";
    public static final String PARAM_OTHERSPORT= "other_sports";
    public static final String PARAM_PROFILE_IMAGE= "image";
    public static final String PARAM_PROFILE_IMAGE_FILENAME= "image_file_name";
    public static final String PARAM_PROFILE_IMAGE_CONTENT_TYPE= "image/jpeg";
    public static final String DEVICE_TYPE= "Android";


    public static final String PARAM_PRIMATY_SPORT = "primary_sport";

    //Facebook Login Paranmeters
    public static final String PARAM_UID = "uid";
    public static final String PARAM_FIRST_NAME = "first_name";
    public static final String PARAM_LAST_NAME = "last_name";
    public static final String PARAM_PROVIDER = "provider";
    public static final String VALUE_SOCIAL_USER_DATA_PROVIDER = "facebook";
    public static final String PARAM_USER_IMAGE = "image";
    public static final String PARAM_USER_ID = "user_id";

    public static final String PARAM_USER_IMAGE_NAME = "image_file_name";
    public static final String PARAM_USER_IMAGE_CONTENT_TYPE = "image_content_type";
    public static final String IMAGE_CONTENT_TYPE = "image/jpg";
    public static final String IMAGE_CONTENT_NAME = "image_1";

    public static final String PARAM_AUTHTOKEN= "auth_token";
    public static final String BADGE_COUNT_API_CALLED_TIME= "badge_count_api_called_time";

    public static final String VENUE_COURT_ID = "court_id";
    public static final String VENUE_ID = "court_id";
    public static final String EVENT_ID = "game_id";
    public static String SPORTS_NAME = "";
    public static String SPORTS_ID = "";

    public static final String PARAM_FRIEND_ID = "friend_id";
    public static final String PARAM_TEAM_ID = "team_id";


    public static final String PARAM_PRIVACY_TYPE = "type";
    public static final String PARAM_PRIVACY_STATUS = "status";



    //VALUES RELATED TO FILTER PAGE
    public static String FILTER_MAP_RADIUS_MILES = "20";
    public static int FILTER_MAP_RADIUS_MILES_COUNT = 20;
    public static String FILTER_PAID_VENUES_NAME = "";
    public static String FILTER_PROXIMITY = "";
    public static String FILTER_SELECT_DATE = "";
    public static String FILTER_FROM_TIME = "";
    public static String FILTER_TO_TIME = "";
    public static boolean FILTER_PUBLIC_OR_FREE_VENUES_STATUS = false;
    public static boolean FILTER_VENUES_NEARBY_STATUS = false;

    //VALUES RELATED TO FILTER SPECIFICATION ADAPTER
    public static ArrayList<String> FILTER_SPECIFICATIN_CHECKBOX_ARRAY = new ArrayList<>();

    //Additional Info page data Added on 01-01-17
    public static String GAME_NAME = "";
    public static String PLAYERS_COUNT = "";
    public static String GAME_VISIBILITY = "Public";
    public static String GAME_TYPE = "Indoor";
    public static String SKILL_LEVEL = "All";
    public static String NOTES_INFO = "";
    public static String VENUE_PRICE = "";
    public static String COURT_PRICE = "";
    public static String VENUE_NAME = "";
    public static String GAME_LOCATION = "";
    public static int COURT_MAX_PLAYER_= 0;
    public static int NO_OF_PARTICIPANT = 0;
    public static int COURT_TOTAL_MAX_PLAYER_EDIT= 0;

    //Event Filter data stored
    public static String EVENT_FILTER_SELECT_DATE = "";
    public static String EVENT_FILTER_PRICE = "";
    public static int EVENT_FILTER_MAP_RADIUS_MILES_COUNT = 20;
    public static String EVENT_FILTER_MAP_RADIUS_MILES = "20";
    public static int EVENT_MAX_PLAYER_= 10;
    public static String NO_OF_PLAYER = "";

    //Availability data stored
    public static String AVAILABILITY_SELECTED_DATE = "";
    public static String AVAILABILITY_TIME_SLOT = "";
    public static ArrayList<String> AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<>();

    public static boolean isEventCreated = false;
    public static boolean isEventUpdated = false;
    public static boolean isImageUploaded = false;


    //Event Filter variables
    public static String EVENT_FILTER_SPORT_NAME = "";

    //Map view variables
    public static boolean FROM_MAP_VIEW = false;

    //Venue filter apply button click
    public static boolean VENUE_FILTER_BTN_CLICK = false;

    //Event filter apply button click
    public static boolean EVENT_FILTER_BTN_CLICK = false;
    public static boolean UNCHECK_SPEC = false;

    //Add Payment Detail
    public static boolean ADD_PAYMENY_BTN_CLICKED = false;
    public static boolean FROM_WISHLIST = false;
    public static boolean FROM_WISHLIST_GAME_UPDATED = false;
    public static boolean FAVOURITE_CLICKED = false;

    public static boolean isUnRegisteredvenueGameCreated = false;
    public static boolean IS_NEWLY_ADDED_TEAM = false;
    public static boolean IS_UPDATED_TEAM = false;
    public static ArrayList<String> ADD_PLAYER_ARRAYLIST = new ArrayList<>();
    public static String UPCOMING_PAST_TYPE = "UPCOMING";
    public static String REVIEW_FILTER_TYPE = "date";
    public static boolean REVIEW_FILTER_BTN_CLICKED = false;
    public static ArrayList<String> ADD_TEAMMATES_IDS_ARRAY = new ArrayList<>();
    public static ArrayList<String> ADD_TEAMMATES_IMAGES_ARRAY = new ArrayList<>();
    public static ArrayList<String> ADD_TEAMMATES_NAMES_ARRAY = new ArrayList<>();
    public static String VISIBILITY = "";
    public static String TEAM_ID = "";
    public static String TEAM_NAME = "";
    public static String EDIT_NAME_SPORT_NAME = "";
    public static String NOTE = "";
    public static boolean IS_EDIT_BUTTON_CLICKED = false;
    public static ArrayList<String> ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST = new ArrayList<>();
    public static boolean FROM_NOTIFICATION = false;
    public static String PUSH_NOTIFICATION_GAME_ID = "";
    public static String TOTAL_REVIEW_COUNT = "";
    public static String TOTAL_REVIEW_AVERAGE = "";
    public static boolean UPCOMING_DETAIL_FAVORITE = false;
    public static boolean FROM_PUSH_NOTIFICATION = false;
    public static boolean FROM_PUSH_NOTIFICATION_INVITE_FRIEND = false;
    public static boolean FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY = false;
    public static boolean MY_SPORTS_CLICKED = false;
    public static boolean IS_PLAYER_ADDED = false;
    public static boolean REVIEW_ADDED = false;
    public static boolean REVIEW_ADDED_EVENT = false;
    public static int REVIEW_LIST_COUNT = 0;
    public static boolean REVIEW_ADDED_FOR_BACK = false;
    public static boolean IS_EDIT_GAME_CLICKED = false;
    public static boolean UPCOMING_PAST_CLICKED = false;
    public static boolean UPCOMING_PAST_CLICKED_FOR_UPDATE_GAME = false;
    public static boolean UPCOMING_GAME_UPDATED_SUCCESS = false;
    public static ArrayList<String> PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES = new ArrayList<>();
    public static ArrayList<String> PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES = new ArrayList<>();
    public static ArrayList<String> TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES = new ArrayList<>();

    /**POI game variables*/
    public static String POI_GAME_ADDRESS = "";
    public static String POI_GAME_DATE = "";
    public static String POI_GAME_START_TIME = "";
    public static String POI_GAME_END_TIME = "";
    public static String POI_GAME_END_COURT_TYPE = "";
    public static String POI_LATITUDE = "";
    public static String POI_LONGITUDE = "";

    public static String FLEXIBLE = "";
    public static String MODERATE = "";
    public static String STRICT = "";
    public static String SUPER_STRICT = "";
    public static String CANCELLATION_POLICY_TYPE = "";
    public static String CANCELLATION_POLICY_TYPE_POINT = "";
    public static String LIST_ID = "";
    public static String EVENT_END_TIME = "";
    public static String EVENT_NAME_FOR_RATING = "";
    public static String SEARCH_LOCATION_ADDRESS = "";
    public static String SEARCH_VENUE_ID = "";
    public static boolean LEAVED_STATUS = false;
    public static boolean CANCEL_STATUS = false;
    public static int PUSH_NOTIFICATION_BADGE_COUNT = 0;


    public static void setAllValuesToNull() {
        Constants.FILTER_MAP_RADIUS_MILES = "20";
        Constants.FILTER_MAP_RADIUS_MILES_COUNT = 20;
        Constants.FILTER_PAID_VENUES_NAME = "";
        Constants.FILTER_SELECT_DATE = "";
        Constants.FILTER_FROM_TIME = "";
        Constants.FILTER_TO_TIME = "";
        Constants.FILTER_PROXIMITY = "";
        Constants.SPORTS_ID = "";
        Constants.FILTER_PUBLIC_OR_FREE_VENUES_STATUS = false;
        Constants.FILTER_VENUES_NEARBY_STATUS = false;
        Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY = new ArrayList<>();
        Constants.GAME_NAME = "";
        Constants.PLAYERS_COUNT = "";
        Constants.GAME_VISIBILITY = "Private";
        Constants.SKILL_LEVEL = "All";
        Constants.NOTES_INFO = "";
        Constants.VENUE_PRICE = "";
        Constants.VENUE_NAME = "";
        Constants.EVENT_FILTER_SELECT_DATE = "";
        Constants.EVENT_FILTER_PRICE = "";
        Constants.EVENT_FILTER_MAP_RADIUS_MILES = "20";
        Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT = 20;
        Constants.NO_OF_PLAYER = "";
        Constants.AVAILABILITY_SELECTED_DATE = "";
        Constants.AVAILABILITY_TIME_SLOT = "";
        Constants.EVENT_FILTER_SPORT_NAME = "";
        Constants.NOTE = "";
        Constants.FROM_MAP_VIEW = false;
        Constants.VENUE_FILTER_BTN_CLICK = false;
        Constants.EVENT_FILTER_BTN_CLICK = false;
        Constants.UNCHECK_SPEC = false;
        Constants.ADD_PAYMENY_BTN_CLICKED = false;
        Constants.FROM_WISHLIST = false;
        Constants.IS_EDIT_BUTTON_CLICKED = false;
        Constants.FROM_NOTIFICATION = false;
        Constants.FROM_WISHLIST_GAME_UPDATED = false;
        Constants.isEventCreated = false;
        Constants.isEventUpdated = false;
        Constants.LEAVED_STATUS = false;
        Constants.CANCEL_STATUS = false;
        Constants.MY_SPORTS_CLICKED = false;
        Constants.POI_GAME_ADDRESS = "";
        Constants.POI_GAME_DATE = "";
        Constants.POI_GAME_START_TIME = "";
        Constants.POI_GAME_END_TIME = "";
        Constants.POI_GAME_END_COURT_TYPE = "";
        Constants.SEARCH_LOCATION_ADDRESS = "";
        Constants.SEARCH_VENUE_ID = "";

    }

    public static String VENUE_IMAGE = null;


    /**
     * Static Constants for Join Events
     */

    public static String EVENT_NAME = null;
    public static String EVENT_NAME_MAP = null;
    public static String CURRENT_EVENT_ID = null;
    public static String EVENT_DATE = null;
    public static String EVENT_TIME = null;
    public static String EVENT_PRICE = null;
    public static String EVENT_VENUE_COURT = null;
    public static String EVENT_PLAYERS_COUNT = null;
    public static String EVENT_PLAYERS_SPORT_NAME = null;
    public static String EVENT_COURT_NAME = null;
    public static double EVENT_LATITUDE = 0;
    public static double EVENT_LONGITUDE = 0;



}
