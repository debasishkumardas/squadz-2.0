package com.andolasoft.squadz.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.Gravity;

/**
 * Class responsible for handling the loader state.
 *
 * @author Debasish Kumar Das
 * @version 1.0
 * @since 2017-04-17
 */
public class LoaderUtility {

    public static Context mContext;
    public static ProgressDialog dialog;

    public static void handleLoader(Context context, boolean status){
        mContext = context;
        if(status){
            dialog = ProgressDialog
                    .show(mContext, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }
}
