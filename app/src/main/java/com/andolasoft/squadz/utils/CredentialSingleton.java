package com.andolasoft.squadz.utils;

/**
 * Created by Debasish Kumar Das on 2/3/2017.
 */
public class CredentialSingleton {
    private static CredentialSingleton ourInstance = new CredentialSingleton();
    private String aws_access_key, aws_secter_key, aws_bucket,
            google_api_key, google_places_api_key, stripe_secret_key,
            stripe_publishable_key;

    public static CredentialSingleton getInstance() {
        //if no instance is initialized yet then create new instance
        //else return stored instance
        if (ourInstance == null) {
            ourInstance = new CredentialSingleton();
        }
        return ourInstance;
    }

    private CredentialSingleton() {
    }

    public void setAwsSecretKey(String param_aws_secret_key) {
        this.aws_secter_key = param_aws_secret_key;
    }

    public String getAwsSecretKey() {
        return aws_secter_key;
    }

    public void setAwsAccessKey(String param_aws_access_key) {
        this.aws_access_key = param_aws_access_key;
    }

    public String getAwsAccessKey() {
        return aws_access_key;
    }


    public void setStripeSecretKey(String param_stripe_secret_key) {
        this.stripe_secret_key = param_stripe_secret_key;
    }

    public String getStripeSecretKey() {
        return stripe_secret_key;
    }

    public void setStripePublishableKey(String param_stripe_publishable_key) {
        this.stripe_publishable_key = param_stripe_publishable_key;
    }

    public String getStripePublishableKey() {
        return stripe_publishable_key;
    }
}
