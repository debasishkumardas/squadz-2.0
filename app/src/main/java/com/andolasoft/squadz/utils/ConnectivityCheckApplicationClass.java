package com.andolasoft.squadz.utils;

import android.app.Application;

import com.andolasoft.squadz.application.MyApplication;

/**
 * Created by Debasish Kumar Das on 4/12/2017.
 */
public class ConnectivityCheckApplicationClass extends Application {

    private static ConnectivityCheckApplicationClass mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static synchronized ConnectivityCheckApplicationClass getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


}
