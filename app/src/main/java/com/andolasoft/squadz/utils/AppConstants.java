package com.andolasoft.squadz.utils;

import com.andolasoft.squadz.models.UserMolel;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Debasish Kumar Das on 1/3/2017.
 */

public class AppConstants {


    public static final long SPLASH_SCREEN_ELAPSE_TIME = 5000;

    public static String AUTH_TOKEN = null;
    public static String USERNAME = null;
    public static String LOGGEDIN_USERID = null;
    public static JSONArray SPORT_ARRAY = new JSONArray();
    public static boolean USER_LOGIN_TYPE_FACEBOOK = false;
    public static String FCM_ID = null;
    public static String UPLOADED_IMAGE_FILE_NAME = null;
    public static double mCurrentLocationLat;
    public static double mCurrentLocationLon;

    public static boolean isLocationPicked = false;
    public static boolean isLocationPicked_venue_listing = false;
    public static boolean isLocationPicked_venue_mapview = false;
    public static boolean isPaymentCardAdded = false;
    public static boolean isApplicationEnteredToBackground = false;


    public static String PAYMENT_CURRENCY = "usd";

    public static ArrayList<UserMolel> userModelArary = new ArrayList<>();
    public static ArrayList<String> userIdArray = new ArrayList<>();

    //Aws informations
    public static final String AWS_ACCESS_KEY = "AKIAJ7WFCAFU5QA3DK4A";
    public static final String AWS_SECRET_KEY = "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";
    public static final String AWS_BUCKET_NAME = "squadz";
    public static final String AWS_BUCKET_FOLDER_NAME = "profile_picture/";
    public static final String AWS_IMAGE_BASE_URL = "https://squadz.s3.amazonaws.com/profile_picture/";


    //Applcation Constants
    public static final String API_RESPONSE_STATUS_SUCCESS = "success";
    public static final String API_RESPONSE_STATUS_ERROR = "error";
    public static final String API_RESPONSE_STATUS_CREATE_FAILED = "create_failed";
    public static final String API_RESPONSE_ERROR_MESSAGE = "Server Is Taking Long Time to Respond";
    public static final String API_RESPONSE_ERROR_MESSAGE_ERR = "Something went wrong";
    public static final String APPLICATION_NETWORK_ERR = "Please Connect To a Working Internet Connection.";
    public static String FACEBOOK_USER_IMAGE_BASE_URL = "https://graph.facebook.com/";
    public static String FACEBOOK_USER_IMAGE_OFFSET_URL = "/picture?type=large";

    public static String OBJECT_TYPE_GAME = "";
    public static String OBJECT_TYPE = "";
    public static String GAME_ID = null;

    public static boolean isDeepLinkRecieved = false;
    public static boolean isListDeepLinkRecieved = false;
    public static boolean isUserInNotificationPage = false;
    public static String DEEP_LINKING_EVENT_ID = null;
    public static String DEEP_LINKING_LIST_ID = null;



    /**
     * App Specific details fro firebase
     */
    public static final String APP_PACKAGE_NAME = "com.andolasoft.squadz";
    public static final String OBJECT_TYPE_LIST = "list";
    public static final String OBJECT_TYPE_EVENT = "event";
    public static final String APP_MINiMUM_SUPPORTED_VERSION = "15";
    public static final String APP_FALLBACK_LINK = "http://www.squadz.com";
    public static final String APP_FIREBASE_BASE_LINK = "https://m2792.app.goo.gl/?";

    /**
     * Squadz iOS App details
     */
    public static final String APP_IOS_BUNDLE_ID = "com.squad.squad";
    public static final String APP_IOS_STORE_ID = "956170418";
    public static final String APP_IOS_STORE = "squadz";


    //Event Details Data
    public static int EVENT_MAX_PLAYER = 0;
    public static int EVENT_ACCEPTED_PLAYER = 0;



    //Chat Constants
    public static String CHAT_USER_NAME = null;
    public static String CHAT_CHANNEL_ID = null;
    public static String CHAT_CHANNEL_ID_DUPLICATE = null;
    public static String CHAT_USER_IMAGE_URL = null;
    public static String CHAT_USER_ID = null;
    public static String CHAT_TYPE = null;
    public static String USER_SELECTED_CHAT_TYPE = null;
    public static int CHAT_THREAD_UNREAD_MESSAGE_COUNT = 0;


    public static String CONVERSATION_NODE = "conversation";
    public static String GROUP_MESSAGE_READ_COUNT_NODE = "groupmessagecount";
    public static String GROUP_NODE = "group";
    public static String MESSAGE_NODE = "message";
    public static String MESSAGE_BADGE_COUNT_NODE = "messageBadgeCount";
    public static String NEW_GROUP = "New Group";

    public static boolean isChannedCreated = false;
    public static boolean isgroupDeleted = false;
    public static boolean isUserGointToUpdateTheGroup = false;
    public static boolean isGroupUpdated = false;

    public static boolean isUserSentMessage = false;

    public static String CHAT_TYPE_INDIVIDUAL = "individual";
    public static String CHAT_TYPE_GROUP = "group";
    public static String CHAT_MESSAGE_PARTICIPANTS = "participants";

    public static boolean isUserinChatBoard = false;

    public static boolean isGropuChatMessage = false;

    public static boolean isAnyThingUpdated = false;

    public static boolean isChatMessageRecieveedInBackground = false;

    public static ArrayList<String> groupParticipantsName = new ArrayList<>();

    public static ArrayList<String> groupParticipantsIds = new ArrayList<>();






}
