package com.andolasoft.squadz.utils;

import com.andolasoft.squadz.R;

/**
 * Created by Debasish Kumar Das on 1/25/2017.
 */
public class SportsImagePicker {


    public SportsImagePicker() {

    }


    public int getSportImage(String sportName) {
        int sportImage = 0;
        if (sportName != null && sportName.equalsIgnoreCase("Basketball")) {
            sportImage = R.mipmap.ic_basketball_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Baseball")) {
            sportImage = R.mipmap.ic_baseball_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Cricket")) {
            sportImage = R.mipmap.ic_cricket_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Football")) {
            sportImage = R.mipmap.ic_footballblack;
        } else if (sportName != null && sportName.equalsIgnoreCase("Soccer")) {
            sportImage = R.mipmap.ic_soccer_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Tennis")) {
            sportImage = R.mipmap.ic_tennis_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Volleyball")) {
            sportImage = R.mipmap.ic_volleyball_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Golf")) {
            sportImage = R.mipmap.ic_golf_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Running")) {
            sportImage = R.mipmap.ic_running_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Biking")) {
            sportImage = R.mipmap.ic_biking_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Rugby")) {
            sportImage = R.mipmap.ic_rugby_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Ultimate frisbee")) {
            sportImage = R.mipmap.ic_ultimate_frisbee_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("All")) {
            sportImage = R.mipmap.ic_ultimate_frisbee_black;
        } else if (sportName != null && sportName.equalsIgnoreCase("Custom")) {
            sportImage = R.mipmap.ic_custom_black;
        }
        return sportImage;
    }



    public static int getDefaultSportImage(String sportName) {
        int sportImage = 0;
        if (sportName != null && sportName.equalsIgnoreCase("Basketball")) {
            sportImage = R.mipmap.basketball_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Baseball")) {
            sportImage = R.mipmap.baseball_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Cricket")) {
            sportImage = R.mipmap.cricket_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Football")) {
            sportImage = R.mipmap.football_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Soccer")) {
            sportImage = R.mipmap.soccer_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Tennis")) {
            sportImage = R.mipmap.tennis_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Volleyball")) {
            sportImage = R.mipmap.volleyball_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Golf")) {
            sportImage = R.mipmap.golf_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Running")) {
            sportImage = R.mipmap.running_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Biking")) {
            sportImage = R.mipmap.biking_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Rugby")) {
            sportImage = R.mipmap.rugby_default_image;
        } else if (sportName != null && sportName.equalsIgnoreCase("Ultimate frisbee")) {
            sportImage = R.mipmap.freebies_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("All")) {
            sportImage = R.mipmap.custom_default;
        } else if (sportName != null && sportName.equalsIgnoreCase("Custom")) {
            sportImage = R.mipmap.custom_default;
        }
        return sportImage;
    }
    public static int getOrangeSportImage(String sportName) {
        int sportImage = 0;
        if (sportName != null && sportName.equalsIgnoreCase("Basketball")) {
            sportImage = R.mipmap.ic_basketball_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Baseball")) {
            sportImage = R.mipmap.ic_baseball_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Cricket")) {
            sportImage = R.mipmap.ic_cricket_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Football")) {
            sportImage = R.mipmap.ic_football_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Soccer")) {
            sportImage = R.mipmap.ic_soccer_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Tennis")) {
            sportImage = R.mipmap.ic_tennis_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Volleyball")) {
            sportImage = R.mipmap.ic_volleyball_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Golf")) {
            sportImage = R.mipmap.ic_golf_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Running")) {
            sportImage = R.mipmap.ic_running_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Biking")) {
            sportImage = R.mipmap.ic_biking_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Rugby")) {
            sportImage = R.mipmap.ic_rugby_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Ultimate frisbee")) {
            sportImage = R.mipmap.ic_ultimate_frisbee_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("All")) {
            sportImage = R.mipmap.ic_custom_orange;
        } else if (sportName != null && sportName.equalsIgnoreCase("Custom")) {
            sportImage = R.mipmap.ic_custom_orange;
        }
        return sportImage;
    }
}
