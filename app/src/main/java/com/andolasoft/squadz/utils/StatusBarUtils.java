package com.andolasoft.squadz.utils;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.andolasoft.squadz.R;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.nispok.snackbar.listeners.ActionClickListener;

/**
 * Created by Biplab on 12/13/2015.
 */
public class StatusBarUtils {
    public static int getHeight(Context context) {
        int statusBarHeight = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
        }
        Log.d(StatusBarUtils.class.getSimpleName(), "statusBarHeight--->" + statusBarHeight);
        if (isFlymeOs4x()) {
            return 2 * statusBarHeight;
        }

        return statusBarHeight;
    }

    public static boolean isFlymeOs4x() {
        String sysVersion = Build.VERSION.RELEASE;
        if ("4.4.4".equals(sysVersion)) {
            String sysIncrement = Build.VERSION.INCREMENTAL;
            String displayId = Build.DISPLAY;
            if (!TextUtils.isEmpty(sysIncrement)) {
                return sysIncrement.contains("Flyme_OS_4");
            } else {
                return displayId.contains("Flyme OS 4");
            }
        }
        return false;
    }

    public static void setSnackBarMessage(String message,Context context) {
        //Typeface typeface = Typeface.createFromAsset(SplashScreen.this.getAssets(), "fonts/proxima_nova_bold.ttf");
        SnackbarManager.show(
                Snackbar.with(context) // context
                        .text(message) // text to display
                        .actionLabel("OK") // action button label
                        .color(Color.parseColor("#19212C"))//Change the background color
                        .actionColor(Color.RED)
                        .duration(Snackbar.SnackbarDuration.LENGTH_LONG)
                        .type(SnackbarType.MULTI_LINE)
                        .swipeToDismiss(true)
                        .actionListener(new ActionClickListener() {
                            @Override
                            public void onActionClicked(Snackbar snackbar) {

                            }
                        }) // action button's ActionClickListener
                , (Activity) context);
    }


    /*
     * Function to handle the Change StatusBar Color
     */
    public static void changeStatusBarColor(Activity ctx) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = ctx.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ctx.getResources().getColor(R.color.white));
        }
    }
}
