package com.andolasoft.squadz.utils;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ListUtils {
	@SuppressWarnings("unused")
	public static void setDynamicHeight(ListView mListView) {
		ListAdapter mListAdapter = mListView.getAdapter();

		if (mListAdapter == null) {
			// when adapter is null
			return;
		}
		int height = 0;
		int desiredWidth = MeasureSpec.makeMeasureSpec(mListView.getWidth(),
				MeasureSpec.AT_MOST);
		for (int i = 0; i < mListAdapter.getCount(); i++) {
			View listItem = mListAdapter.getView(i, null, mListView);
			listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			// height = height;
			height += listItem.getMeasuredHeight();

		}
		ViewGroup.LayoutParams params = mListView.getLayoutParams();
		params.height = height
		/* + (mListView.getDividerHeight() * (mListAdapter.getCount())) + 100 */;
		mListView.setLayoutParams(params);
		mListView.requestLayout();
	}

	// List View Height for Pick Venue
	@SuppressLint("NewApi")
	public static void getTotalHeightofListView(ListView listView) {
		// TODO Auto-generated method stub

		ListAdapter mAdapter = listView.getAdapter();

		int totalHeight = 0;

		for (int i = 0; i < mAdapter.getCount(); i++) {
			View mView = mAdapter.getView(i, null, listView);

			mView.measure(
					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),

					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
			totalHeight = totalHeight
					+ ((listView.getDividerHeight() * (mAdapter.getCount() - 1)));

			int measured_Height = mView.getMeasuredHeight();

			totalHeight += mView.getMeasuredHeight();
			Log.w("HEIGHT" + i, String.valueOf(totalHeight));

		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (mAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	public static void getTotalHeightofFilterListView(ListView listView) {
		// TODO Auto-generated method stub

		ListAdapter mAdapter = listView.getAdapter();

		int totalHeight = 0;

		for (int i = 0; i < mAdapter.getCount(); i++) {
			View mView = mAdapter.getView(i, null, listView);

			mView.measure(
					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),

					MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
			// totalHeight = totalHeight + ((listView.getDividerHeight() *
			// (mAdapter.getCount() - 1)));

			totalHeight += mView.getMeasuredHeight();
			Log.w("HEIGHT" + i, String.valueOf(totalHeight));

		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (mAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	public static void setDynamicHeight_teammate(ListView mListView) {
		ListAdapter mListAdapter = mListView.getAdapter();
		if (mListAdapter == null) {
			// when adapter is null
			return;
		}
		int height = 0;
		int desiredWidth = MeasureSpec.makeMeasureSpec(mListView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		for (int i = 0; i < mListAdapter.getCount(); i++) {
			View listItem = mListAdapter.getView(i, null, mListView);
			listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			height += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = mListView.getLayoutParams();
		params.height = height
				+ (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
		mListView.setLayoutParams(params);
		mListView.requestLayout();
	}
}
