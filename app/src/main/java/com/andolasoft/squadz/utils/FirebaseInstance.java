package com.andolasoft.squadz.utils;

import com.firebase.client.Firebase;

/**
 * Created by Debasish Kumar Das on 3/3/2017.
 */
public class FirebaseInstance {
    private static FirebaseInstance ourInstance = new FirebaseInstance();
    public static Firebase ref;

    public static FirebaseInstance getInstance() {

        if(ourInstance == null){
            ourInstance = new FirebaseInstance();
        }
        return ourInstance;
    }

    public static Firebase getFirebaseInstance(){
        ref = new Firebase(EndPoints.BASE_URL_FIREBASE);
        return ref;
    }

    private FirebaseInstance() {
    }
}
