package com.andolasoft.squadz.utils;

import okhttp3.OkHttpClient;

/**
 * Created by Debasish Kumar Das on 1/3/2017.
 */

public class Singleton {

    private static Singleton mInstance = null;

    private String mString;

    OkHttpClient client;

    private Singleton(){
        client = new OkHttpClient();
    }

    public static Singleton getInstance(){
        if(mInstance == null) {
            mInstance = new Singleton();
        }
        return mInstance;
    }

    public OkHttpClient getClient(){
        return this.client;
    }

}
