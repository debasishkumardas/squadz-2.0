package com.andolasoft.squadz.utils;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class Jsoncordinator {

	static InputStream iStream1 = null;
	static JSONObject jarray1 = null;
	static String json1 = "";
	String responseBody;

	public Jsoncordinator() {
	}

	public JSONObject getJSONFromUrl1(String url) {
		
		HttpClient client1 = new DefaultHttpClient();
		HttpPost httpGet1 = new HttpPost(url);
		try {
			HttpResponse response = client1.execute(httpGet1);
			responseBody = EntityUtils.toString(response.getEntity());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Parse String to JSON object
		try {
			
			jarray1 = new JSONObject(responseBody);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}
		// return JSON Object
		return jarray1;
	}

}
