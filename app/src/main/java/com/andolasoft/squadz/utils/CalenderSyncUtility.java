package com.andolasoft.squadz.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.util.Log;
import android.widget.Toast;

public class CalenderSyncUtility {
	ContentResolver cr;
	Uri eventsUri;
	Context context;

	public CalenderSyncUtility(Context context) {
		this.context = context;
	}

	/**
	 * Function responsible for syncing the event to the device calender
	 *
	 * @param game_date Event Date
	 * @param title     Event title
	 * @param note      Event note
	 * @param address   Event address
	 * @param starttime Event start time
	 * @param endtime   Event end time
	 */

	@SuppressLint("InlinedApi")
	public void addEvent(String eventId, String game_date, String title, String note,
						 String address, String starttime, String endtime) {
		java.util.Date temp = null, temp1 = null;
		cr = context.getContentResolver();
		try {
			temp = new SimpleDateFormat("dd/MM/yyyy h:mma").parse(game_date + " " + starttime);
			temp1 = new SimpleDateFormat("dd/MM/yyyy h:mma").parse(game_date + " " + endtime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long time_lng = temp.getTime();
		long time_lng1 = temp1.getTime();

		Cursor cursor = context.getContentResolver().query(Uri.parse("content://com.android.calendar/calendars"), (new String[]{Calendars._ID, Calendars.ACCOUNT_NAME, Calendars.CALENDAR_DISPLAY_NAME,}), null, null, null);
		if (cursor != null && cursor.moveToFirst()) {
			String[] calNames = new String[cursor.getCount()];
			int[] calIds = new int[cursor.getCount()];
			for (int i = 0; i < calNames.length; i++) {
				// retrieve the calendar names and ids
				// at this stage you can print out the display names to get an idea of what calendars the user has
				calIds[i] = cursor.getInt(0);
				calNames[i] = cursor.getString(1);
				cursor.moveToNext();
			}
			cursor.close();
			if (calIds.length > 0) {
				// we're safe here to do any further work
			}
			int cal_id = calIds[0];

			// set the content value
			ContentValues cv = new ContentValues();

			// make sure you add it to the right calendar
			cv.put(Events.CALENDAR_ID, cal_id);

			//cv.put(Events.EVENT_TIMEZONE, "UTC/GMT +5:30");
			cv.put(Events.EVENT_TIMEZONE, getTimeZone());

			//cv.put("event_id", eventId);

			//cv.put(Events._ID, eventId);

			// set the title of the event
			cv.put(Events.TITLE, title);

			// set the description of the event
			cv.put(Events.DESCRIPTION, note);

			// set the event's physical location
			cv.put(Events.EVENT_LOCATION, address);

			// set the event's starttime
			cv.put(Events.DTSTART, time_lng);

			// set the event's endtime
			cv.put(Events.DTEND, time_lng1);

            // Set the event's access level
            cv.put(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);

            // Set the event's access level
            cv.put(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);

			// let the calendar know whether this event goes on all day or not
			// true = 1, false = 0
			//cv.put("allDay", 0);

			// let the calendar know whether an alarm should go off for this event
			//cv.put("hasAlarm", 1);

			// once desired fields are set, insert it into the table
			context.getContentResolver().insert(Uri.parse("content://com.android.calendar/events"), cv);
		}
	}


    public String getTimeZone(){
        TimeZone tz = TimeZone.getDefault();
        String id = tz.getID();
        return id;
    }
	/**
	 * Dunction responsible for getting all the synced Events
	 *
	 * @param eventId Event title
	 * @return
	 */
	public int ListSelectedCalendars(String eventId) {
		Uri eventUri;
		if (android.os.Build.VERSION.SDK_INT <= 7) {
			// the old way
			eventUri = Uri.parse("content://calendar/events");
		} else {
			// the new way
			eventUri = Uri.parse("content://com.android.calendar/events");
		}
		int result = 0;
		String projection[] = {"_id", Events._ID};
		Cursor cursor = context.getContentResolver().query(eventUri, null, null, null, null);

		if (cursor.moveToFirst()) {
			String calName;
			String calID;
			int nameCol = cursor.getColumnIndex(projection[1]);
			int idCol = cursor.getColumnIndex(projection[0]);
			do {
				calName = cursor.getString(nameCol);
				calID = cursor.getString(idCol);
				if (calName != null && calName.contains(eventId)) {
					result = Integer.parseInt(calID);
				}
			} while (cursor.moveToNext());
			cursor.close();
		}
		DeleteCalendarEntry(result);
		return result;
	}


	public boolean getEventExistance(String game_date, String title, String starttime, String endtime) {
		// long begin = // starting time in milliseconds
		//long end = // ending time in milliseconds
		boolean isEventAdded = false;
		java.util.Date temp = null, temp1 = null;
		cr = context.getContentResolver();
		try {
			temp = new SimpleDateFormat("dd/MM/yyyy hh:mma").parse(game_date + " " + starttime);
			temp1 = new SimpleDateFormat("dd/MM/yyyy hh:mma").parse(game_date + " " + endtime);

			Log.d("Game Start Time", temp.toString());
			Log.d("Game End Time", temp1.toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long time_lng = temp.getTime();
		long time_lng1 = temp1.getTime();
		String[] proj = proj = new String[]{
				CalendarContract.Instances._ID,
				CalendarContract.Instances.BEGIN,
				CalendarContract.Instances.END,
				CalendarContract.Instances.EVENT_ID};
		Cursor cursor =
				CalendarContract.Instances.query(context.getContentResolver(), proj, time_lng, time_lng1, title);
		if (cursor.getCount() > 0) {
			// deal with conflict
			isEventAdded = true;
		}
		return isEventAdded;
	}

	/**
	 * Function responsibel for deleting a event from the calender
	 *
	 * @param entryID Calender entry ID
	 * @return returning the status of the deletation
	 */
	private int DeleteCalendarEntry(int entryID) {
		int iNumRowsDeleted = 0;
		Uri eventUri = ContentUris
				.withAppendedId(getCalendarUriBase(), entryID);
		iNumRowsDeleted = context.getContentResolver().delete(eventUri, null, null);
		return iNumRowsDeleted;
	}

	private Uri getCalendarUriBase() {
		Uri eventUri;
		if (android.os.Build.VERSION.SDK_INT <= 7) {
			// the old way
			eventUri = Uri.parse("content://calendar/events");
		} else {
			// the new way
			eventUri = Uri.parse("content://com.android.calendar/events");
		}
		return eventUri;
	}


	private long getCalendarId() {
		String[] projection = new String[]{Calendars._ID};
		String selection =
				Calendars.ACCOUNT_NAME +
						" = ? AND " +
						Calendars.ACCOUNT_TYPE +
						" = ? ";
		// use the same values as above:
		String[] selArgs =
				new String[]{"Google Calendar", CalendarContract.ACCOUNT_TYPE_LOCAL};
		Cursor cursor = context.getContentResolver().
				query(Calendars.CONTENT_URI,
						projection,
						selection,
						selArgs,
						null);
		if (cursor.moveToFirst()) {
			return cursor.getLong(0);
		}
		return -1;
	}

	public void addEventToCalendar(String eventId, String game_date, String title, String note,
								   String address, String starttime, String endtime) {

		int calId = getDeviceCalendarId();
		if (calId == -1) {
			// no calendar account; react meaningfully
			Toast.makeText(context, "No calendar found", Toast.LENGTH_SHORT).show();
			return;
		}
		long time_start = getTimeInMillis(game_date, starttime);
		long time_end = getTimeInMillis(game_date, endtime);
		ContentValues values = new ContentValues();
		values.put(Events.DTSTART, time_start);
		values.put(Events.DTEND, time_end);
		values.put(Events.TITLE, title);
		values.put(Events._ID, eventId);
		values.put(Events.EVENT_LOCATION, address);
		values.put(Events.CALENDAR_ID, calId);
		values.put(Events.EVENT_TIMEZONE, TimeZone.getDefault().toString());
		values.put(Events.DESCRIPTION, "");
		values.put(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
		values.put(Events.ALL_DAY, 1);
		values.put(Events.ORGANIZER, "some.mail@some.address.com");
		//Uri uri = context.getContentResolver().insert(Events.CONTENT_URI, values);
        Uri uri = context.getContentResolver().insert(Uri.parse("content://com.android.calendar/events"), values);
		//long eventId_calendar = new Long(uri.getLastPathSegment());
        //Log.d("Event Id",String.valueOf(eventId_calendar));
	}

    /**
     * Getting the time in mill second
     * @param date
     * @param time
     * @return
     */
	public long getTimeInMillis(String date, String time) {
		Date temp = null, temp1 = null;
		try {
			temp = new SimpleDateFormat("dd/MM/yyyy hh:mma").parse(date + " " + time);
			//temp1 = new SimpleDateFormat("dd/MM/yyyy hh:mma").parse(date+" "+endtime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp.getTime();
	}


    /**
     * Function responsible for getting the event add status in the calendar
     * @param eventName
     * @return
     */
	public boolean getCalendarEventExistance(String eventName) {

        boolean isEventAlreadyExist = false;
		String[] proj = new String[]{
				Events._ID,
				Events.DTSTART,
				Events.DTEND,
				Events.RRULE,
				Events.TITLE};
		Cursor cursor =
				context.getContentResolver().
						query(
								Events.CONTENT_URI,
								proj,
								Events.TITLE + " = ? ",
								new String[]{/*Long.toString(*/eventName/*)*/},
								null);
		if (cursor.moveToFirst()) {
			// read event data
			//Toast.makeText(context, "found the match", Toast.LENGTH_SHORT).show();
            isEventAlreadyExist = true;
		}
        return isEventAlreadyExist;
	}


    /**
     * Function responsible for getting the calender Id
     * @return calender id
     */
	public int getDeviceCalendarId() {
		int cal_id = 0;
		Cursor cursor = context.getContentResolver().query(Uri.parse("content://com.android.calendar/calendars"), (new String[]{Calendars._ID, Calendars.ACCOUNT_NAME, Calendars.CALENDAR_DISPLAY_NAME,}), null, null, null);
		if (cursor != null && cursor.moveToFirst()) {
			String[] calNames = new String[cursor.getCount()];
			int[] calIds = new int[cursor.getCount()];
			for (int i = 0; i < calNames.length; i++) {
				// retrieve the calendar names and ids
				// at this stage you can print out the display names to get an idea of what calendars the user has
				calIds[i] = cursor.getInt(0);
				calNames[i] = cursor.getString(1);
				cursor.moveToNext();
			}
			cursor.close();
			if (calIds.length > 0) {
				// we're safe here to do any further work
			}
			cal_id = calIds[0];
		}
		return cal_id;
	}
}
