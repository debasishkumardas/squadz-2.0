package com.andolasoft.squadz.utils;

import android.view.View;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

/**
 * Created by dell1 on 07-09-2016.
 */
public class AnimationUtils {

    public static void animateLayoutsInRight(LinearLayout layout){

        YoYo.with(Techniques.SlideInRight)
                .duration(1000)
                .playOn(layout);
        layout.setVisibility(View.VISIBLE);

    }

    public static void animateLayoutSlideOutLeft(LinearLayout layout){

        YoYo.with(Techniques.SlideOutLeft)
                .duration(1000)
                .playOn(layout);

    }
    public static void animateLayoutsInLeft(LinearLayout layout){

        YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(layout);


    }
    public static void animateLayoutSlideOutRight(LinearLayout layout){

        YoYo.with(Techniques.SlideOutRight)
                .duration(1000)
                .playOn(layout);

    }
}
