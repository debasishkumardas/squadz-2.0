package com.andolasoft.squadz.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;

import com.andolasoft.squadz.activities.Profile;
import com.andolasoft.squadz.models.ProfileOtherViewModel;
import com.andolasoft.squadz.views.adapters.FriendsAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Debasish Kumar Das on 4/5/2017.
 *
 *
 * Class responsible for Handling user Profile data and handling of the page redirections.
 */
public class UserProfileManager {

    Context mContext;
    Activity mActivity;
    ProgressDialog dialog;
    ProfileOtherViewModel profileOtherViewModel;
    String status = "",message = "", is_profile_visible = "";
    SnackBar snackBar;
    String auth_token, user_id;

    public UserProfileManager() {
    }

    //Function call to get the user profile data
    public void getUserProfileInfo(Context context, Activity activity,
                              String authToken, String userId) {

        //Getting all the values for the cunstructor call
        this.mContext = context;
        this.mActivity = activity;
        this.auth_token = authToken;
        this.user_id = userId;

        //Initializing the snack bar
        snackBar = new SnackBar(context);

        //Function call to get the user profile into
        getUserProfileInfo(this.auth_token, this.user_id);
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(mContext, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Function to build the user prolfile APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String auth_token, String user_id) {

        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL +"users/" + auth_token + "/profile").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to get the user profile info
     * @param auth_token
     * @param user_id
     */
    public void getUserProfileInfo(String auth_token, String user_id) {
        //Handling the loader state
        LoaderUtility.handleLoader(mContext, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(auth_token, user_id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(mContext, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String user_id = "",
                        username = "",phone_number = "",
                        name = "",email = "",
                        birth_date = "",image = "",
                        about_me = "",sport = "",
                        skill_level = "",rewards = "",
                        street_name = "",locality = "",
                        province_abbreviation = "",postal_code = "",
                        country = "";

                // Read data on the worker thread
                String responseData = response.body().string();

                profileOtherViewModel = new ProfileOtherViewModel();
                FriendsAdapter.profileOtherViewModelArrayList = new ArrayList<ProfileOtherViewModel>();
                ArrayList<String> secondary_aports_array = new ArrayList<String>();

                try {
                    JSONObject jsonObject = new JSONObject(responseData);

                    if(jsonObject.has("status") && !jsonObject.isNull("status")) {
                        status = jsonObject.getString("status");
                    }
                    if(jsonObject.has("message") && !jsonObject.isNull("message")) {
                        message = jsonObject.getString("message");
                    }

                    if(jsonObject.has("is_profile_visible") && !jsonObject.isNull("is_profile_visible")) {
                        is_profile_visible = jsonObject.getString("is_profile_visible");
                    }

                    if(jsonObject.has("profile") && !jsonObject.isNull("profile")) {
                        JSONObject profile_jsonObject = jsonObject.getJSONObject("profile");

                        if(profile_jsonObject.has("user_id") && !profile_jsonObject.isNull("user_id")) {
                            user_id = profile_jsonObject.getString("user_id");
                        }
                        if(profile_jsonObject.has("username") && !profile_jsonObject.isNull("username")) {
                            username = profile_jsonObject.getString("username");
                        }
                        if(profile_jsonObject.has("phone_number") && !profile_jsonObject.isNull("phone_number"))
                        {
                            phone_number = profile_jsonObject.getString("phone_number");
                        }
                        if(profile_jsonObject.has("name") && !profile_jsonObject.isNull("name")) {
                            name = profile_jsonObject.getString("name");
                        }
                        if(profile_jsonObject.has("email") && !profile_jsonObject.isNull("email")) {
                            email = profile_jsonObject.getString("email");
                        }
                        if(profile_jsonObject.has("birth_date") && !profile_jsonObject.isNull("birth_date")) {
                            birth_date = profile_jsonObject.getString("birth_date");
                        }
                        if(profile_jsonObject.has("image") && !profile_jsonObject.isNull("image")) {
                            image = profile_jsonObject.getString("image");
                        }
                        if(profile_jsonObject.has("about_me") && !profile_jsonObject.isNull("about_me")) {
                            about_me = profile_jsonObject.getString("about_me");
                        }
                        if(profile_jsonObject.has("sport") && !profile_jsonObject.isNull("sport")) {
                            sport = profile_jsonObject.getString("sport");
                        }
                        if(profile_jsonObject.has("skill_level") && !profile_jsonObject.isNull("skill_level")) {
                            skill_level = profile_jsonObject.getString("skill_level");
                        }
                        if(profile_jsonObject.has("rewards") && !profile_jsonObject.isNull("rewards")) {
                            rewards = profile_jsonObject.getString("rewards");
                        }
                        if(profile_jsonObject.has("street_name") && !profile_jsonObject.isNull("street_name")) {
                            street_name = profile_jsonObject.getString("street_name");
                        }
                        if(profile_jsonObject.has("locality") && !profile_jsonObject.isNull("locality")) {
                            locality = profile_jsonObject.getString("locality");
                        }
                        if(profile_jsonObject.has("province_abbreviation") && !profile_jsonObject.isNull("province_abbreviation")) {
                            province_abbreviation = profile_jsonObject.getString("province_abbreviation");
                        }
                        if(profile_jsonObject.has("postal_code") && !profile_jsonObject.isNull("postal_code")) {
                            postal_code = profile_jsonObject.getString("postal_code");
                        }
                        if(profile_jsonObject.has("country") && !profile_jsonObject.isNull("country")) {
                            country = profile_jsonObject.getString("country");
                        }
                        if(profile_jsonObject.has("secondary_sports") && !profile_jsonObject.isNull("secondary_sports")) {
                            JSONArray secondary_sports_jsonArray = profile_jsonObject.getJSONArray("secondary_sports");
                            for (int i = 0; i < secondary_sports_jsonArray.length(); i++) {

                                JSONObject jsonObject1 = secondary_sports_jsonArray.getJSONObject(i);
                                String sport_id = jsonObject1.getString("sport_id");
                                String skillLevel = jsonObject1.getString("skill_level");

                                secondary_aports_array.add(sport_id);
                            }
                        }

                    }

                    profileOtherViewModel.setStatus(status);
                    profileOtherViewModel.setUser_id(user_id);
                    profileOtherViewModel.setUsername(username);
                    profileOtherViewModel.setPhone_number(phone_number);
                    profileOtherViewModel.setName(name);
                    profileOtherViewModel.setEmail(email);
                    profileOtherViewModel.setBirth_date(birth_date);
                    profileOtherViewModel.setImage(image);
                    profileOtherViewModel.setAbout_me(about_me);
                    profileOtherViewModel.setSport(sport);
                    profileOtherViewModel.setSkill_level(skill_level);
                    profileOtherViewModel.setRewards(rewards);
                    profileOtherViewModel.setStreet_name(street_name);
                    profileOtherViewModel.setLocality(locality);
                    profileOtherViewModel.setProvince_abbreviation(province_abbreviation);
                    profileOtherViewModel.setPostal_code(postal_code);
                    profileOtherViewModel.setCountry(country);
                    profileOtherViewModel.setSecondary_sports_array(secondary_aports_array);
                    profileOtherViewModel.setProfile_visibility(is_profile_visible);

                    FriendsAdapter.profileOtherViewModelArrayList.add(profileOtherViewModel);
                }catch (Exception exp) {
                    exp.printStackTrace();
                }
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(mContext, false);

                        //Redirecting the user to the Profile Page after getting the Success Message
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                            FriendsAdapter.isFromFriendAdapter = true;
                            Intent intent = new Intent(mActivity, Profile.class);
                            mActivity.startActivity(intent);
                        } else if(!TextUtils.isEmpty(message)) {
                            snackBar.setSnackBarMessage(message);
                        } else{
                            snackBar.setSnackBarMessage("Network error, please try after sometime");
                        }
                    }
                });
            }
        });
    }
}
