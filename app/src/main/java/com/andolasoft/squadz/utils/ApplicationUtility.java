package com.andolasoft.squadz.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.andolasoft.squadz.views.widgets.SnackBar;
import com.stripe.android.model.Card;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Debasish Kumar Das on 1/9/2017.
 */
public class ApplicationUtility {
    public static long selected_date_milisecond;
    public static SnackBar snackBar;
    /**
     * Hiding the Key board
     */
    public static void hideSoftKeyboard(Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    //Get Device Installed android Version
    public static int getDeviceInstalledAndroidVersion(){
        return Build.VERSION.SDK_INT;
    }

    //Get Distance from Current location to destination
    public static double distance(Double latitude, Double longitude, double dest_lat, double dest_lng) {
        double d2r = Math.PI / 180;

        double dlong = (longitude - dest_lng) * d2r;
        double dlat = (latitude - dest_lat) * d2r;
        double a = Math.pow(Math.sin(dlat / 2.0), 2) + Math.cos(dest_lat * d2r)
                * Math.cos(latitude * d2r) * Math.pow(Math.sin(dlong / 2.0), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = 6367 * c;

        return Double.valueOf(new DecimalFormat("##.##").format(d * 0.62137)); // 0.62137 will convert km into miles

    }


    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedDate(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy");
        //SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMMM dd, yyyy");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedDateContainSpace(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd MMMM yyyy");
        //SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMMM dd, yyyy");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING GMT DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedGMTDate(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd MMMM yyyy");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING GMT DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedGMTDateForAvailabilityDisplay(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMMM dd, yyyy");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * Current date
     * @return
     */
    public static String currentDate() {
        Calendar c = Calendar.getInstance();
        //SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String customFormattedDate(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * Paring as Year-Month-Day
     */
    public static String customDateFormat(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String customFormattedDateForEventFilter(String inputDate) {

        String finalFormatedDate = "";

        //SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat inputFormatter = new SimpleDateFormat("MMMM dd, yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * Format the time to ex. 12:30 am
     * @param inputTime
     * @return
     */
    public static String formattedTime(String inputTime) {

        String finalFormatedTime = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("hh:mmaa");

        try {

            Date date1 = inputFormatter.parse(inputTime);
            finalFormatedTime = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedTime;
    }


    /**
     * Function to build the Card Parameters
     * @param cardNumber
     * @param expMonth
     * @param expYear
     * @param cvv
     * @return
     */
    public static Card buildCardParameters(String cardNumber, String expMonth,
                                    String expYear, String cvv){
        Card card = null;
        try{
            card = new Card(
                    cardNumber,
                    Integer.valueOf(expMonth),
                    Integer.valueOf(expYear),
                    cvv);
            card.setCurrency(AppConstants.PAYMENT_CURRENCY);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return card;
    }

    /**
     * Fi=unction to validate the cards
     * @param card
     * @return
     */
    public static boolean validateCard(Card card) {
        boolean validateCard = card.validateCard();
        return validateCard;
    }


    /**
     * Function responsible for building the Firebase deep link URL
     * @param Dynamic_link
     * @return the link to be shared
     */
    public static String buildFirebaseDeepLinkingURL(String Dynamic_link){
        String deepLink = null;

        try{
            deepLink = AppConstants.APP_FIREBASE_BASE_LINK
                    +"link="+Dynamic_link+"&apn="+AppConstants.APP_PACKAGE_NAME +
                    /*"&amv="+AppConstants.APP_MINiMUM_SUPPORTED_VERSION +*/
                    "&ibi="+AppConstants.APP_IOS_BUNDLE_ID+
                    "&isi="+AppConstants.APP_IOS_STORE_ID+
                    "&ius="+AppConstants.APP_IOS_STORE;
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return  deepLink;
    }


    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String customFormattedDateNew(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
            selected_date_milisecond = date1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    public static String getCurrentTime(){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        return  ts;
    }

    public static String getDeviceCurrentTime(){
        Calendar cal = Calendar.getInstance();
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("hh:mm a");
        String localTime = date.format(currentLocalTime);
        if(localTime.contains(" ")){
            localTime = localTime.replace(" ","");
        }
        return localTime;
    }

    public static String getTimeWithFormat(String timeToBeConverted){
        String convertedTime = null;
        DateFormat incoming_date_format = null;
        if(timeToBeConverted.contains(" ") && !timeToBeConverted.startsWith(" "))
        {
            incoming_date_format = new SimpleDateFormat("h:mm a");
        }
        else if(timeToBeConverted.contains(" ") && timeToBeConverted.startsWith(" "))
        {
            timeToBeConverted = timeToBeConverted.replaceFirst(" ","");
            incoming_date_format = new SimpleDateFormat("h:mma");
        }
        else{
            incoming_date_format = new SimpleDateFormat("hh:mma");
        }
        DateFormat converted_date_format = new SimpleDateFormat("h:mm a");
        try {
            Date date1 = incoming_date_format.parse(timeToBeConverted);
            convertedTime = converted_date_format.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedTime;
    }

    public static String getTimeWithFormatWithSpace(String timeToBeConverted){
        String convertedTime = null;
        DateFormat incoming_date_format = new SimpleDateFormat("hh:mm a");
        DateFormat converted_date_format = new SimpleDateFormat("h:mm a");
        try {
            Date date1 = incoming_date_format.parse(timeToBeConverted);
            convertedTime = converted_date_format.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedTime;
    }

    public static String getDeviceCurrentDate(){
        String formattedDate = null;
        try{
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            formattedDate = df.format(c.getTime());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * PARSING GMT DATE FORMAT WITH CUSTOM DATE
     */
    public static long UTCTimeFormat(String inputDate) {

        long time_milisecond = 0;
        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd MMMM yyyy");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            //finalFormatedDate = toConvertFormatter.format(date1);

            time_milisecond = date1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time_milisecond;
    }

    /**
     * PARSING UTC DATE FORMAT WITH CUSTOM DATE
     */
    public static String DateTimeFormatToCustom(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd MMMM");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING UTC DATE FORMAT WITH CUSTOM DATE
     */
    public static String DateTimeFormatForNotification(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd MMMM");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }
    public static String dateTimeFormatForNearByGameNotification(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("EEEE MMMM dd");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING UTC DATE FORMAT WITH CUSTOM DATE
     */
    public static String customReviewDateFormat(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd MMMM yyyy");

        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }


    /**
     * Get Alphanuemric Number
     * @return
     */
    public static String getChannelId(){
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }


    public static String getTimeAgo(long time) {

        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;

        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            //return "just now";
            return "1 s";
        } else if (diff < 2 * MINUTE_MILLIS) {
            //return "a minute ago";
            return "1 m";
        } else if (diff < 50 * MINUTE_MILLIS) {
            //return diff / MINUTE_MILLIS + " minutes ago";
            return diff / MINUTE_MILLIS + " m";
        } else if (diff < 90 * MINUTE_MILLIS) {
            //return "an hour ago";
            return "1 h";
        } else if (diff < 24 * HOUR_MILLIS) {
            //return diff / HOUR_MILLIS + " hours ago";
            return diff / HOUR_MILLIS + " h";
        } else if (diff < 48 * HOUR_MILLIS) {
            //return "yesterday";
            return "1 d";
        } else {
            //return diff / DAY_MILLIS + " days ago";
            return diff / DAY_MILLIS + " d";
        }
    }

    public static String getCurrentDateTime(){
        String formattedDate = null;
        try{
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
            formattedDate = df.format(c.getTime());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedDateForDisplay(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMMM dd, yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedDateForSignup(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("MMMM dd, yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd/MM/yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedDateForDisplayForProfile(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMMM dd, yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }


    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedDateForDisplayForEditProfile(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMMM dd, yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedDateForDisplayForEditProfileReverse(String inputDate) {

        String finalFormatedDate = "";
        SimpleDateFormat inputFormatter = new SimpleDateFormat("MMMM dd, yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return finalFormatedDate;
    }


    /**
     * Paring as Year-Month-Day
     */
    public static String customDateFormater(String inputDate) {
        String finalFormatedDate = "";
        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return finalFormatedDate;
    }

    /**
     * Function to calculate the days from hour
     * @param hour
     * @return
     */
    public static String getDaysFromHour(String hour) {
        String return_Days = "";
        if(hour.equalsIgnoreCase("24")) {
            return_Days = "24 hours";
        } else if(hour.equalsIgnoreCase("72")) {
            return_Days = "3 days";
        } else if(hour.equalsIgnoreCase("168")) {
            return_Days = "7 days";
        } else{
            return_Days = "2 hours";
        }
        return return_Days;
    }

    public static String getHour_Days(String hour) {
        String return_Days = "";
        if(hour.equalsIgnoreCase("24")) {
            return_Days = "24 hours";
        } else if(hour.equalsIgnoreCase("72")) {
            return_Days = "3 days";
        } else if(hour.equalsIgnoreCase("168")) {
            return_Days = "7 days";
        } else{
            return_Days = "2 hours";
        }
        return return_Days;
    }
    /**
     * Return two digit after decimal value
     */
    public static String twoDigitAfterDecimal(double price) {
        String final_price = "0.0";
        DecimalFormat twoValue = new DecimalFormat("0.00");
        final_price = twoValue.format(price);
        return final_price;
    }

    /**
     * Converted game creation time into milisecond
     * @param date_time
     * @return
     */
    public static long getGameDateTimeInMilisecond(String date_time) {
        long time_milisecond = 0;
        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mma");
        try {
            Date date1 = inputFormatter.parse(date_time);
            time_milisecond = date1.getTime();
            System.out.print(""+time_milisecond);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time_milisecond;
    }

    /**
     * Converted game creation time into milisecond
     * @param date_time
     * @return
     */
    public static long getEventDateTimeInMilisecond(String date_time) {
        long time_milisecond = 0;
        SimpleDateFormat inputFormatter = new SimpleDateFormat("MMMM dd, yyyy hh:mma");
        try {
            Date date1 = inputFormatter.parse(date_time);
            time_milisecond = date1.getTime();
            System.out.print(""+time_milisecond);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time_milisecond;
    }

    /**
     * PARSING GMT DATE FORMAT WITH CUSTOM DATE
     */
    public static String formattedCalenderDate(String inputDate) {
        String finalFormatedDate = "";
        SimpleDateFormat inputFormatter = new SimpleDateFormat("MMMM dd, yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("dd MMMM yyyy");
        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return finalFormatedDate;
    }

    /**
     * Current date
     * @return
     */
    public static String currentFormmatedDate() {
        Calendar c = Calendar.getInstance();
        //SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    /**
     * Function to check internet connection
     * @param context
     * @return the internet connection status
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) ||
                    (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    /**
     * Funcxtion responsible for displaying the message while
     * there is no internet connection
     * @param c Context of the Activity
     */
    public static void displayNoInternetMessage(final Context c) {
        snackBar = new SnackBar(c);
        snackBar.setSnackBarMessage(AppConstants.APPLICATION_NETWORK_ERR);
    }

    /**
     * Funcxtion responsible for displaying the message while there is no internet connection
     * @param c Context of the Activity
     */
    public static void displayErrorMessage(final Context c, String message) {
        snackBar = new SnackBar(c);
        snackBar.setSnackBarMessage(message);
    }

    /**Hide Keyboard*/
    public static void hideSoftKeyBoard(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    /**
     * Getting current Time zone
     */
    public static String getCurrentTimeZone() {
        String timeZone = "";
        TimeZone tz = TimeZone.getDefault();
        timeZone = tz.getID();//ex: Asia/Calcutta
        //String timeZ = tz.getDisplayName(false, TimeZone.SHORT);//ex: GMT+05:30
        return timeZone;
    }

    /**Getting Notification date in millisecond*/
    public static double getNotificationDateInMili(String input_date) {
        double notif_date_inMili = 0;
        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date date1 = inputFormatter.parse(input_date);
            notif_date_inMili = date1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return notif_date_inMili;
    }

    /**Getting Current date in millisecond*/
    public static double getCurrentDateInMili() {
        double current_date_inMili = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String currentDateandTime = sdf.format(new Date());
        Date date1 = null;
        try {

            date1 = sdf.parse(currentDateandTime);
            current_date_inMili = date1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return current_date_inMili;
    }

    /**Getting Next date from the current date*/
    public static String getNextDayDateFromCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        String todayAsString = dateFormat.format(today);
        String tomorrowAsString = dateFormat.format(tomorrow);

        System.out.println(todayAsString);
        System.out.println(tomorrowAsString);

        return tomorrowAsString;
    }


    /**
     * Checking Internet connection
     * @return
     */
    public static boolean isConnectingToInternet(Activity activity){
        ConnectivityManager connectivity = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isAvailable;
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }


    public static void displayNoInternetAlert(final Activity activity) {
        new AlertDialog.Builder(activity).setMessage("Please connect to a working internet connection and try again")
                .setTitle("Network Error")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton){
                                activity.finish();
                            }
                        })
                .show();
    }

    /**
     * Getting time in mili
     */
    public static long getCurrentDateTimeInMili(String date_time) {
        long time_milisecond = 0;
        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        try {
            Date date1 = inputFormatter.parse(date_time);
            time_milisecond = date1.getTime();
            System.out.print(""+time_milisecond);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time_milisecond;
    }
}
