package com.andolasoft.squadz.application;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

/**
 * Created by SpNayak on 1/3/2017.
 */

public class MyApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        //Initializing the multidex
        MultiDex.install(this);
    }
}
