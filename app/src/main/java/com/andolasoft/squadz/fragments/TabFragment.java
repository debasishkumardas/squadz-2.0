package com.andolasoft.squadz.fragments;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.NormalRegistrationStepTwo;
import com.andolasoft.squadz.activities.SplashScreen;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.ChatConversationModel;
import com.andolasoft.squadz.models.ChatModel;
import com.andolasoft.squadz.models.FirebaseGroupChatParticipantModel;
import com.andolasoft.squadz.models.GroupChatReadMessageCount;
import com.andolasoft.squadz.models.NotificationModel;
import com.andolasoft.squadz.models.UpcomingPastModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.adapters.ChatConversationList;
import com.andolasoft.squadz.views.adapters.NotificationAdapter;
import com.andolasoft.squadz.views.adapters.UpcomingPastAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;

import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TabFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static ArrayList<UpcomingPastModel> upcomingPastModelArrayList;
    public static JSONArray event_listing_array = null;
    public static String userId = null, auth_token = "";
    public static Button notification_count, notification_count_messages;
    ViewPagerAdapter adapter;
    TextView tabOne, tabTwo, tabThree, tabFour;
    ProgressDialog dialog;
    UpcomingPastModel upcomingPastModel;
    UpcomingPastAdapter upcomingPastAdapter;
    String userName = null;
    ChatConversationModel chatConversationModel;
    ChatConversationList chatListingAdapter;
    ArrayList<String> messageChannelIdsArray;
    SharedPreferences pref;
    SnackBar snackBar;
    String event_type = "UPCOMING";
    NotificationAdapter notificationAdapter;
    NotificationModel notificationModel;
    ArrayList<NotificationModel> notificationModelArrayList;
    MaterialDialog mMaterialDialog;
    boolean is_Page_Refresh_Status = false;
    int badgeCount = 0;
    DatabaseAdapter db;
    int reagMessageCount = 0;
    int apiCalledCount = 0;

    //This is the handler that will manager to process the broadcast intent
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("message");
            String type = intent.getStringExtra("TYPE");
            //Keeping the last API called time
            SplashScreen.keepTheBadgeCountApiCalledTime(getActivity(),
                    SplashScreen.getApiCalledTime());
            if (type != null && type.equalsIgnoreCase("ChatMessage")) {
                refreshChatBadgeCount();
            }else if(is_Page_Refresh_Status && TextUtils.isEmpty(type)) {
                getNotificationAPI(userId);
            }else if(type != null && !type.equalsIgnoreCase("ChatMessage")) {
                notification_count.setVisibility(View.VISIBLE);
                Constants.PUSH_NOTIFICATION_BADGE_COUNT = Constants.PUSH_NOTIFICATION_BADGE_COUNT + 1;
                notification_count.setText(String.valueOf(Constants.PUSH_NOTIFICATION_BADGE_COUNT));
            } else {
                if(TextUtils.isEmpty(type)){
                    notification_count.setVisibility(View.VISIBLE);
                    Constants.PUSH_NOTIFICATION_BADGE_COUNT = Constants.PUSH_NOTIFICATION_BADGE_COUNT + 1;
                    notification_count.setText(String.valueOf(Constants.PUSH_NOTIFICATION_BADGE_COUNT));
                } else if(type != null && type.equalsIgnoreCase("ChatMessage")){
                    refreshChatBadgeCount();
                }else if(is_Page_Refresh_Status && TextUtils.isEmpty(type)){
                    notification_count.setVisibility(View.VISIBLE);
                    Constants.PUSH_NOTIFICATION_BADGE_COUNT = Constants.PUSH_NOTIFICATION_BADGE_COUNT + 1;
                    notification_count.setText(String.valueOf(Constants.PUSH_NOTIFICATION_BADGE_COUNT));
                }
            }
        }
    };


    /**
     * Refreshing the chat messagebadge count when the chat push notification relieved
     */
    public static void refreshChatBadgeCount(){
        notification_count_messages.setVisibility(View.VISIBLE);
        SplashScreen.messageBadgeCount = SplashScreen.messageBadgeCount + 1;
        notification_count_messages.setText(String.valueOf(SplashScreen.messageBadgeCount));
    }


    /**
     * Refreshing the chat messagebadge count when the chat push notification relieved
     */
    public static void displayChatBadgeCount(int badgeCOunt){
        if(badgeCOunt > 0){
            notification_count_messages.setVisibility(View.VISIBLE);
            SplashScreen.messageBadgeCount = badgeCOunt;
            notification_count_messages.setText(String.valueOf(SplashScreen.messageBadgeCount));
        }else if(badgeCOunt == 0){
            notification_count_messages.setVisibility(View.GONE);
            SplashScreen.messageBadgeCount = badgeCOunt;
        }
    }

    public TabFragment() {
        //Required empty constructor
    }

    /*public void pushdata(){
        Firebase.setAndroidContext(getActivity());
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(userId);
        ChatConversationModel chatMoodel = new ChatConversationModel();
        chatMoodel.setChannelId("3frDKjVUjgEJyppv8rFUm8u2w6tyrccC");
        chatMoodel.setChatUserName("Mashable");
        chatMoodel.setDate("1488523472");
        chatMoodel.setImageUrl("http://s3.amazonaws.com/squadz/images/587c79bd6...");
        chatMoodel.setRecieverID("587c79bd6d05a6ca4b000102");
        chatMoodel.setSenderId("541ffed16d05a6157b000086");
        postRef.push().setValue(chatMoodel);
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x = inflater.inflate(R.layout.fragment_tab, null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);
        notification_count = (Button) x.findViewById(R.id.notification_count);
        notification_count_messages = (Button) x.findViewById(R.id.notification_count_messages);
        db = new DatabaseAdapter(getActivity());
        snackBar = new SnackBar(getActivity());
        pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userId = pref.getString("loginuser_id", null);
        userName = pref.getString("USERNAME", null);
        chatConversationModel = new ChatConversationModel();
        auth_token = pref.getString("auth_token", null);

        /**
         * ADDING FRAGMENT INSIDE VIEW PAGER
         */
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        /**
         * Adding icon inside Tab layout
         */
        initTablayout();

        /**
         * ADDING TAB ICON INSIDE TAB LAYOUT
         */
        setupTabIcons();

        getChatMessageBadgeCount();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //viewPager.setCurrentItem(tab.getPosition());
                //TextView text = (TextView) tab.getCustomView();

                onTabChange(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //TextView text = (TextView) tab.getCustomView();
                //assert text != null;
                //text.setTypeface(null, Typeface.NORMAL);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    is_Page_Refresh_Status = false;
                    Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND = false;
                    if(Constants.MY_SPORTS_CLICKED) {
                        getJoinGameAvailibity();
                        Constants.MY_SPORTS_CLICKED = false;
                    }
                }
                if (position == 1) {
                    is_Page_Refresh_Status = true;
                    Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND = false;
                    Constants.MY_SPORTS_CLICKED = false;
                    //Keeping the last API called time
                    SplashScreen.keepTheBadgeCountApiCalledTime(getActivity(),
                            SplashScreen.getApiCalledTime());
                    NotificationFragment.notification_recycler_view.setVisibility(View.GONE);
                    getNotificationAPI(userId);

                    //getFriendRequestInvitationAPI(auth_token);
                }

                if (position == 3) {
                    //do your Staff
                    is_Page_Refresh_Status = false;
                    Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND = false;
                    Constants.MY_SPORTS_CLICKED = false;
                    UpcomingPastGamesFragment.upcoming_recycler_view.setVisibility(View.INVISIBLE);
                    UpcomingPastGamesFragment.past_recycler_view.setVisibility(View.INVISIBLE);
                    UpcomingPastGamesFragment.upcoming_view.setVisibility(View.VISIBLE);
                    UpcomingPastGamesFragment.past_view.setVisibility(View.INVISIBLE);
                    UpcomingPastGamesFragment.upcoming_text.setTextColor(getResources().getColor(R.color.gray_new));
                    UpcomingPastGamesFragment.past_text.setTextColor(getResources().getColor(R.color.gray_additional));

                    getUpcomingEventListing();

                } else if (position == 2) {
                    is_Page_Refresh_Status = true;
                    Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND = false;
                    Constants.MY_SPORTS_CLICKED = false;
                    ChatFragmnet.conversationModel = new ArrayList<>();
                    messageChannelIdsArray = new ArrayList<String>();
                    //pushdata();
                    fetchValue();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //Getting the notification count
        getNotificationCount();




        return x;

    }

    public void getNotificationCount(){
        if(Constants.PUSH_NOTIFICATION_BADGE_COUNT != 0){
            notification_count.setVisibility(View.VISIBLE);
            notification_count.setText(String.valueOf(Constants.PUSH_NOTIFICATION_BADGE_COUNT));
        }
    }

    /**
     * Updating the user badge count
     */
    public static void getChatMessageBadgeCount(){
        if(SplashScreen.messageBadgeCount != 0 && SplashScreen.messageBadgeCount > 0){
            notification_count_messages.setVisibility(View.VISIBLE);
            notification_count_messages.setText(String.valueOf(SplashScreen.messageBadgeCount));
        }else if(SplashScreen.messageBadgeCount == 0){
            notification_count_messages.setVisibility(View.GONE);
        }else if(SplashScreen.messageBadgeCount < 0){
            SplashScreen.messageBadgeCount = 0;
            notification_count_messages.setVisibility(View.GONE);
        }
    }

    /**
     * Fetching conversation data from firebase
     */
    public void fetchValue() {
        //Handling the loader
        LoaderUtility.handleLoader(getActivity(), true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(getActivity());
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(userId);

        //callback to handle the user conversation data
        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                Log.d("TAG", dataSnapshot.toString());
                //Clearing the data from the model class
                ChatFragmnet.conversationModel.clear();
                //Clearing the messageChannelIdsArray array
                messageChannelIdsArray = new ArrayList<String>();

                if (dataSnapshot.hasChild("groupIds")) {
                    com.firebase.client.DataSnapshot dataSnapshot_groupIds = dataSnapshot.child("groupIds");
                    ArrayList<String> groupIdsArray = new ArrayList<String>();
                    //getting all the conversation data and assigning them to the model class
                    for (DataSnapshot postSnapshot : dataSnapshot_groupIds.getChildren()) {
                        String message_group_id = (String) postSnapshot.getValue();
                        String message_group_key = (String) postSnapshot.getKey();
                        if (!groupIdsArray.contains(message_group_id)) {
                            groupIdsArray.add(message_group_id);
                            Log.d("TAG", message_group_id);
                            fetchGroupValue(message_group_id);
                        }
                    }
                }

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String key = postSnapshot.getKey();
                    Log.d("Key", key);
                    if (!postSnapshot.getKey().equalsIgnoreCase("groupIds")) {
                        ChatConversationModel chatConversationListingmodel = postSnapshot.getValue(ChatConversationModel.class);

                        if(postSnapshot.hasChild("channelId")){
                            String message_channelId = (String) postSnapshot.child("channelId").getValue();
                            long message_date = (long) postSnapshot.child("date").getValue();
                            int date_str = (int) message_date;
                            fetchLastMessage(message_channelId, chatConversationListingmodel);
                            long getReadMessageCount = (long) postSnapshot.child("readMessageCount").getValue();
                            getUnreadMessageCOunt(message_channelId, chatConversationListingmodel, (int) getReadMessageCount);
                            messageChannelIdsArray.add(message_channelId);
                            chatConversationListingmodel.setDate(date_str);
                            chatConversationListingmodel.setChatType("individual");
                            ChatFragmnet.conversationModel.add(chatConversationListingmodel);
                        }
                    }

                    Collections.sort(ChatFragmnet.conversationModel, new Comparator<ChatConversationModel>() {
                        @Override
                        public int compare(ChatConversationModel o1, ChatConversationModel o2) {
                            //return o1.getDate().compareTo(o2.getDate());
                            return Integer.compare(o1.getDate(), o2.getDate());
                        }
                    });

                    Collections.reverse(ChatFragmnet.conversationModel);

                    //Setting the data in the list view adapter
                    chatListingAdapter = new ChatConversationList(getActivity(), ChatFragmnet.conversationModel);
                    ChatFragmnet.teammate_view.setAdapter(chatListingAdapter);
                    chatListingAdapter.notifyDataSetChanged();

                }


                //Handling the loader
                LoaderUtility.handleLoader(getActivity(), false);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    /**
     * Fetching conversation data from firebase
     */
    public void fetchGroupValue(final String groupId) {
        //Handling the loader
        // handleLoader(true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(getActivity());
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.GROUP_NODE);

        //callback to handle the user conversation data
        postRef.child(groupId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                Log.d("TAG", dataSnapshot.toString());
                ChatConversationModel chatConversationModel = new ChatConversationModel();
                String channelId = (String) dataSnapshot.child("channelId").getValue();

                if (!messageChannelIdsArray.contains(channelId)) {
                    fetchLastMessage(channelId, chatConversationModel);
                    messageChannelIdsArray.add(channelId);
                    //String date = (String) dataSnapshot.child("date").getValue();
                    String recieverID = (String) dataSnapshot.child("groupId").getValue();

                    ArrayList<String> getAllGroupIds = getAllGroupIds(ChatFragmnet.conversationModel);

                    if (!getAllGroupIds.contains(recieverID)) {
                        String chatUserName = (String) dataSnapshot.child("groupName").getValue();
                        String imageUrl = (String) dataSnapshot.child("imageUrl").getValue();
                        long date = (long) dataSnapshot.child("date").getValue();
                        int date_str = (int) date;
                        chatConversationModel.setChannelId(channelId);
                        chatConversationModel.setDate(date_str);
                        chatConversationModel.setRecieverID(recieverID);
                        chatConversationModel.setChatUserName(chatUserName);
                        chatConversationModel.setImageUrl(imageUrl);
                        chatConversationModel.setSenderId(null);
                        chatConversationModel.setChatType("group");
                        fetchReadMessageCountGroup(groupId, channelId, chatConversationModel);
                        fetchGroupParticipants(recieverID, chatConversationModel);
                        // ChatFragmnet.conversationModel.add(chatConversationModel);
                        //Setting the data in the list view adapter
                       /* chatListingAdapter = new ChatConversationList(getActivity(),ChatFragmnet.conversationModel);
                        ChatFragmnet.teammate_view.setAdapter(chatListingAdapter);
                        chatListingAdapter.notifyDataSetChanged();*/
                        //chatListingAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

        // handleLoader(false);
    }

    public ArrayList<String> getAllGroupIds(ArrayList<ChatConversationModel> grpIds) {
        ArrayList<String> getGrpIds = new ArrayList<>();
        if (grpIds != null && grpIds.size() > 0) {
            for (int i = 0; i < grpIds.size(); i++) {
                String group_ids = grpIds.get(i).getRecieverID();
                getGrpIds.add(group_ids);
            }
        }
        return getGrpIds;
    }

    /**
     * Fetching conversation data from firebase
     */
    public void fetchGroupParticipants(String groupId,
                                       final ChatConversationModel chatParticipantyModel) {
        //Handling the loader
        // handleLoader(true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(getActivity());
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.GROUP_NODE);

        //callback to handle the user conversation data
        postRef.child(groupId).child("participants").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                ArrayList<FirebaseGroupChatParticipantModel> participants_array =
                        new ArrayList<FirebaseGroupChatParticipantModel>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Log.d("TAG", dataSnapshot.toString());
                    FirebaseGroupChatParticipantModel chatConversationModel =
                            new FirebaseGroupChatParticipantModel();
                    String username = (String) postSnapshot.child("username").getValue();
                    String user_id = (String) postSnapshot.child("user_id").getValue();
                    String image = (String) postSnapshot.child("image").getValue();
                    chatConversationModel.setUser_id(user_id);
                    chatConversationModel.setUsername(username);
                    chatConversationModel.setImage(image);
                    participants_array.add(chatConversationModel);
                    chatParticipantyModel.setChatMembers(participants_array);
                }

                //Adding the values to the array
                ChatFragmnet.conversationModel.add(chatParticipantyModel);

                //Sorting the array
                Collections.sort(ChatFragmnet.conversationModel, new Comparator<ChatConversationModel>() {
                    @Override
                    public int compare(ChatConversationModel o1, ChatConversationModel o2) {
                        return Integer.compare(o1.getDate(), o2.getDate());
                    }
                });

                //Reversing the array
                Collections.reverse(ChatFragmnet.conversationModel);

                //refreshing the list view
                chatListingAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    /**
     * Fetching conversation data from firebase
     */
    public void fetchLastMessage(String channedId,
                                 final ChatConversationModel model) {
        //Handling the loader
        // handleLoader(true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(getActivity());
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_NODE);

        //callback to handle the user conversation data
        postRef.child(channedId).limitToLast(1).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    ChatModel chatModel = postSnapshot.getValue(ChatModel.class);
                    String message = chatModel.getText();
                    model.setLastMessage(message);
                    chatListingAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

    }


    /**
     * Fetching conversation data from firebase
     */
    public void fetchReadMessageCountGroup(String groupId, final String channelId,
                                           final ChatConversationModel chatConversationModel) {
        //Initializing the firebase instance
        Firebase.setAndroidContext(getActivity());
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //postRef.child(AppConstants.GROUP_MESSAGE_READ_COUNT_NODE).child(groupId).child(userId);

        //callback to handle the user conversation data
        postRef.child(AppConstants.GROUP_MESSAGE_READ_COUNT_NODE).child(groupId).child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    GroupChatReadMessageCount chatModel = postSnapshot.getValue(GroupChatReadMessageCount.class);
                    reagMessageCount = chatModel.getReadMessageCount();
                    getUnreadMessageCOunt(channelId,chatConversationModel,reagMessageCount);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

    }

    /**
     * Fetching conversation data from firebase
     * @param channedId
     * @param model
     * @param readMessageCount
     */
    public void getUnreadMessageCOunt(String channedId,
                                      final ChatConversationModel model,
                                      final int readMessageCount) {

        //Initializing the firebase instance
        Firebase.setAndroidContext(getActivity());
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_NODE);

        //callback to handle the user conversation data
        postRef.child(channedId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {

                apiCalledCount = apiCalledCount + 1;
                if(apiCalledCount == 1){
                    int getCount = (int) dataSnapshot.getChildrenCount();
                    int unreadMessageCount = getCount - readMessageCount;
                    model.setReadMessageCount(unreadMessageCount);
                    //chatListingAdapter.notifyDataSetChanged();
                    //ChatConversationList chatListingAdapter = new ChatConversationList(getActivity(), ChatFragmnet.conversationModel);
                    //ChatFragmnet.teammate_view.setAdapter(chatListingAdapter);
                    chatListingAdapter.notifyDataSetChanged();
                    apiCalledCount = 0;
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


    /**
     * Adding icon inside Tab layout
     */
    public void initTablayout() {

        if (Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND) {
            tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_gray_36dp, 0, 0, 0);
            tabOne.setPadding(40, 0, 0, 0);

            tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications_orange_36dp, 0, 0, 0);
            tabTwo.setPadding(50, 0, 0, 0);

            tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            //tabThree.setText("THRE");
            tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_gray_28dp, 0, 0, 0);
            //tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_gray_36dp, 0, 0, 0);
            tabThree.setPadding(70, 0, 0, 0);
            tabFour = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            //tabFour.setText("FOUR");
            tabFour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_football_gray_24dp, 0, 0, 0);
            tabFour.setPadding(40, 0, 0, 0);
        } else if(Constants.MY_SPORTS_CLICKED) {
            tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_gray_36dp, 0, 0, 0);
            tabOne.setPadding(40, 0, 0, 0);

            tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications_gray_36dp, 0, 0, 0);
            tabTwo.setPadding(50, 0, 0, 0);

            tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_gray_28dp, 0, 0, 0);
            tabThree.setPadding(70, 0, 0, 0);

            tabFour = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabFour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_football_orange, 0, 0, 0);
            tabFour.setPadding(40, 0, 0, 0);
        } else {
            tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_orange_36dp, 0, 0, 0);
            tabOne.setPadding(40, 0, 0, 0);

            tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            //tabTwo.setText("TWO");
            tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications_gray_36dp, 0, 0, 0);
            tabTwo.setPadding(50, 0, 0, 0);

            tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            //tabThree.setText("THRE");
            tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_gray_28dp, 0, 0, 0);
            //tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_gray_36dp, 0, 0, 0);
            tabThree.setPadding(70, 0, 0, 0);

            tabFour = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            //tabFour.setText("FOUR");
            tabFour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_football_gray_24dp, 0, 0, 0);
            tabFour.setPadding(40, 0, 0, 0);
        }

    }

    /**
     * ADDING TAB ICON INSIDE TAB LAYOUT
     */
    private void setupTabIcons() {

        //tabOne.setText("ONE");
        tabLayout.getTabAt(0).setCustomView(tabOne);
        tabLayout.getTabAt(1).setCustomView(tabTwo);
        tabLayout.getTabAt(2).setCustomView(tabThree);
        tabLayout.getTabAt(3).setCustomView(tabFour);

        if (Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND) {
            tabLayout.getTabAt(1).select();
            viewPager.setCurrentItem(1);
            is_Page_Refresh_Status = true;
            NotificationFragment.notification_recycler_view.setVisibility(View.GONE);
            getNotificationAPI(userId);
        } else if (Constants.MY_SPORTS_CLICKED) {
            tabLayout.getTabAt(3).select();
            viewPager.setCurrentItem(3);
            getUpcomingEventListing();
        }

    }

    /**
     * ADDING FRAGMENT INSIDE VIEW PAGER
     */
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFrag(new HomeFragment(), "ONE");
        adapter.addFrag(new NotificationFragment(), "TWO");
        adapter.addFrag(new ChatFragmnet(), "THREE");
        adapter.addFrag(new UpcomingPastGamesFragment(), "FOUR");
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        /** the ViewPager requires a minimum of 1 as OffscreenPageLimit */
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(limit);

        /**Default selecting the tab for Notification page if any invite friend notification found*/
        if (Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND) {
            viewPager.setCurrentItem(1);
        }
        else if (Constants.MY_SPORTS_CLICKED) {
            viewPager.setCurrentItem(3);
        }
    }

    public void onTabChange(int position) {
        if (position == 0) {
            tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_orange_36dp, 0, 0, 0);

            tabOne.setPadding(40, 0, 0, 0);
            tabLayout.getTabAt(0).setCustomView(tabOne);

            tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications_gray_36dp, 0, 0, 0);
            tabTwo.setPadding(50, 0, 0, 0);
            tabLayout.getTabAt(1).setCustomView(tabTwo);

            tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_gray_28dp, 0, 0, 0);
            tabThree.setPadding(70, 0, 0, 0);
            tabLayout.getTabAt(2).setCustomView(tabThree);

            tabFour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_football_gray_24dp, 0, 0, 0);
            tabFour.setPadding(40, 0, 0, 20);
            tabLayout.getTabAt(3).setCustomView(tabFour);
        } else if (position == 1) {
            tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_gray_36dp, 0, 0, 0);
            tabOne.setPadding(40, 0, 0, 0);
            tabLayout.getTabAt(0).setCustomView(tabOne);

            tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications_orange_36dp, 0, 0, 0);
            tabTwo.setPadding(50, 0, 0, 0);
            tabLayout.getTabAt(1).setCustomView(tabTwo);

            tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_gray_28dp, 0, 0, 0);
            tabThree.setPadding(70, 0, 0, 0);
            tabLayout.getTabAt(2).setCustomView(tabThree);

            tabFour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_football_gray_24dp, 0, 0, 0);
            tabFour.setPadding(40, 0, 0, 20);
            tabLayout.getTabAt(3).setCustomView(tabFour);
        } else if (position == 2) {
            tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_gray_36dp, 0, 0, 0);
            tabOne.setPadding(40, 0, 0, 0);
            tabLayout.getTabAt(0).setCustomView(tabOne);

            tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications_gray_36dp, 0, 0, 0);
            tabTwo.setPadding(50, 0, 0, 0);
            tabLayout.getTabAt(1).setCustomView(tabTwo);

            tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_orange_28dp, 0, 0, 0);
            tabThree.setPadding(70, 0, 0, 0);
            tabLayout.getTabAt(2).setCustomView(tabThree);

            tabFour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_football_gray_24dp, 0, 0, 0);
            tabFour.setPadding(40, 0, 0, 20);
            tabLayout.getTabAt(3).setCustomView(tabFour);
        } else if (position == 3) {
            tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_gray_36dp, 0, 0, 0);
            tabOne.setPadding(40, 0, 0, 0);
            tabLayout.getTabAt(0).setCustomView(tabOne);

            tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_notifications_gray_36dp, 0, 0, 0);
            tabTwo.setPadding(50, 0, 0, 0);
            tabLayout.getTabAt(1).setCustomView(tabTwo);

            tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_gray_28dp, 0, 0, 0);
            tabThree.setPadding(70, 0, 0, 0);
            tabLayout.getTabAt(2).setCustomView(tabThree);

            tabFour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_football_orange, 0, 0, 0);
            tabFour.setPadding(40, 0, 0, 20);
            tabLayout.getTabAt(3).setCustomView(tabFour);

        }
    }

    /**
     * Function to get Event listing from server
     */
    public void getUpcomingEventListing() {
        //Handling the loader state
        LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
                if(!ApplicationUtility.isConnected(getActivity())){
                    ApplicationUtility.displayNoInternetMessage(getActivity());
                }else{
                    ApplicationUtility.displayErrorMessage(getActivity(), e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", image = "", name = "", latitude = "", longitude = "",
                        review_rating = "", review_count = "", price = "", court_type = "", skill_level = "",
                        eventDate = "", no_of_participants = "", sport_name = "", total_number_players = "", startTime = "", endTime = "",
                        creator_username = "", creator_user_id = "", creator_profile_image = "",
                        actual_price = "", minimum_price = "", eventVisibility = "", address = "",
                        object_type = "", note = "", list_id = "";
                boolean isfavorite = false;
                ArrayList<String> participantsArray = null;

                try {
                    upcomingPastModelArrayList = new ArrayList<>();
                    event_listing_array = new JSONArray(responseData);

                    /**Remove older data & keeping new data*/
                    db.deleteRatingsInfo();
                    for (int i = 0; i < event_listing_array.length(); i++) {
                        upcomingPastModel = new UpcomingPastModel();

                        JSONObject responseObject = event_listing_array.getJSONObject(i);

                        if (responseObject.has("id") && !responseObject.isNull("id")) {
                            id = responseObject.getString("id");
                        }
                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            image = responseObject.getString("image");
                        }
                        if (responseObject.has("name") && !responseObject.isNull("name")) {
                            name = responseObject.getString("name");
                        }
                        if (responseObject.has("latitude") && !responseObject.isNull("latitude")) {
                            latitude = responseObject.getString("latitude");
                        }
                        if (responseObject.has("longitude") && !responseObject.isNull("longitude")) {
                            longitude = responseObject.getString("longitude");
                        }
                        if (responseObject.has("review_rating") && !responseObject.isNull("review_rating")) {
                            review_rating = responseObject.getString("review_rating");
                        }
                        if (responseObject.has("review_count") && !responseObject.isNull("review_count")) {
                            review_count = responseObject.getString("review_count");
                        }

                        if (responseObject.has("no_of_player") && !responseObject.isNull("no_of_player")) {
                            total_number_players = responseObject.getString("no_of_player");
                        }

                        if (responseObject.has("no_of_partitipants") && !responseObject.isNull("no_of_partitipants")) {
                            no_of_participants = responseObject.getString("no_of_partitipants");
                        }
                        if (responseObject.has("sport_name") && !responseObject.isNull("sport_name")) {
                            sport_name = responseObject.getString("sport_name");
                        }
                        if (responseObject.has("object_type") && !responseObject.isNull("object_type")) {
                            object_type = responseObject.getString("object_type");
                        }

                        if (responseObject.has("price") && !responseObject.isNull("price")) {
                            price = responseObject.getString("price");
                            try {
                                int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                                float price_pre_player = Float.parseFloat(price) / (float) totalPlayer;
                                actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                                float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                                minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        if (responseObject.has("skill_level") && !responseObject.isNull("skill_level")) {
                            skill_level = responseObject.getString("skill_level");
                        }

                        if (responseObject.has("date") && !responseObject.isNull("date")) {
                            eventDate = responseObject.getString("date");
                        }


                        if (responseObject.has("user_name") && !responseObject.isNull("user_name")) {
                            creator_username = responseObject.getString("user_name");
                        }

                        if (responseObject.has("user_id") && !responseObject.isNull("user_id")) {
                            creator_user_id = responseObject.getString("user_id");
                        }

                        if (responseObject.has("user_profile_image") && !responseObject.isNull("user_profile_image")) {
                            creator_profile_image = responseObject.getString("user_profile_image");
                        }

                        if (responseObject.has("user_profile_image") && !responseObject.isNull("user_profile_image")) {
                            creator_profile_image = responseObject.getString("user_profile_image");
                        }

                        if (responseObject.has("is_favorite") && !responseObject.isNull("is_favorite")) {
                            isfavorite = responseObject.getBoolean("is_favorite");
                        }

                        if (responseObject.has("time_slots") && !responseObject.isNull("time_slots")) {
                            String start_time = null;
                            JSONArray time_slot_array_json_array = responseObject.getJSONArray("time_slots");
                            for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                                JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                                if (j == 0) {
                                    startTime = getValues.getString("start_time");
                                }
                                endTime = getValues.getString("end_time");
                            }
                        }

                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            ArrayList<String> images_arraylist = new ArrayList<String>();
                            JSONArray images_json_array = responseObject.getJSONArray("image");
                            if (images_json_array.length() > 0) {
                                for (int j = 0; j < images_json_array.length(); j++) {
                                    images_arraylist.add(images_json_array.getString(j));
                                }
                                upcomingPastModel.setImages_array(images_arraylist);
                            } else {
                                int getEventImage = SportsImagePicker.getDefaultSportImage(sport_name);
                                upcomingPastModel.setImage(getEventImage);
                            }
                        } else {
                            int getEventImage = SportsImagePicker.getDefaultSportImage(sport_name);
                            upcomingPastModel.setImage(getEventImage);
                        }


                        if (responseObject.has("visibility") && !responseObject.isNull("visibility")) {
                            eventVisibility = responseObject.getString("visibility");
                        }
                        if (responseObject.has("address") && !responseObject.isNull("address")) {
                            address = responseObject.getString("address");
                        }
                        if (responseObject.has("list_id") && !responseObject.isNull("list_id")) {
                            list_id = responseObject.getString("list_id");
                        }



                        if (responseObject.has("partitipant_ids") && !responseObject.isNull("partitipant_ids")) {
                            participantsArray = new ArrayList<String>();
                            JSONArray images_json_array = responseObject.getJSONArray("partitipant_ids");
                            for (int j = 0; j < images_json_array.length(); j++) {
                                participantsArray.add(images_json_array.getString(j));
                            }
                            //eventListingModel.setImages_array(participantsArray);
                        }

                        if (eventVisibility != null &&
                                eventVisibility.equalsIgnoreCase("Private") &&
                                participantsArray.contains(userId)) {
                            //Setting the data in model
                            upcomingPastModel.setEvent_Id(id);
                            upcomingPastModel.setEvent_Image(image);
                            upcomingPastModel.setEvent_Name(name);
                            upcomingPastModel.setLatitude(latitude);
                            upcomingPastModel.setLongitude(longitude);
                            upcomingPastModel.setEvent_Ratings(review_rating);
                            upcomingPastModel.setEvent_Reviews(review_count);
                            upcomingPastModel.setEvent_Price(actual_price);
                            upcomingPastModel.setSkill_level(skill_level);
                            upcomingPastModel.setEventDate(eventDate);
                            upcomingPastModel.setEvent_Total_Participants(no_of_participants);
                            upcomingPastModel.setEventStartTime(startTime);
                            upcomingPastModel.setEventEndTime(endTime);
                            upcomingPastModel.setCapacity(total_number_players);
                            upcomingPastModel.setCreator_name(creator_username);
                            upcomingPastModel.setCreator_user_id(creator_user_id);
                            upcomingPastModel.setEvent_Creator_Image(creator_profile_image);
                            upcomingPastModel.setFavorite(isfavorite);
                            upcomingPastModel.setEvent_Minimum_Price(minimum_price);
                            upcomingPastModel.setVisibility(eventVisibility);
                            upcomingPastModel.setAddress(address);
                            upcomingPastModel.setEvent_Sports_Type(sport_name);
                            upcomingPastModel.setObject_Type(object_type);
                            upcomingPastModel.setEvent_Total_Price(price);
                            //upcomingPastModel.setNote(note);
                            upcomingPastModel.setList_Id(list_id);
                            upcomingPastModelArrayList.add(upcomingPastModel);
                        } else if (eventVisibility != null &&
                                eventVisibility.equalsIgnoreCase("Public")) {
                            //Setting the data in model
                            upcomingPastModel.setEvent_Id(id);
                            upcomingPastModel.setEvent_Image(image);
                            upcomingPastModel.setEvent_Name(name);
                            upcomingPastModel.setLatitude(latitude);
                            upcomingPastModel.setLongitude(longitude);
                            upcomingPastModel.setEvent_Ratings(review_rating);
                            upcomingPastModel.setEvent_Reviews(review_count);
                            upcomingPastModel.setEvent_Price(actual_price);
                            upcomingPastModel.setSkill_level(skill_level);
                            upcomingPastModel.setEventDate(eventDate);
                            upcomingPastModel.setEvent_Total_Participants(no_of_participants);
                            upcomingPastModel.setEventStartTime(startTime);
                            upcomingPastModel.setEventEndTime(endTime);
                            upcomingPastModel.setCapacity(total_number_players);
                            upcomingPastModel.setCreator_name(creator_username);
                            upcomingPastModel.setCreator_user_id(creator_user_id);
                            upcomingPastModel.setEvent_Creator_Image(creator_profile_image);
                            upcomingPastModel.setFavorite(isfavorite);
                            upcomingPastModel.setEvent_Minimum_Price(minimum_price);
                            upcomingPastModel.setVisibility(eventVisibility);
                            upcomingPastModel.setAddress(address);
                            upcomingPastModel.setEvent_Sports_Type(sport_name);
                            upcomingPastModel.setObject_Type(object_type);
                            upcomingPastModel.setEvent_Total_Price(price);
                            //upcomingPastModel.setNote(note);
                            upcomingPastModel.setList_Id(list_id);
                            upcomingPastModelArrayList.add(upcomingPastModel);
                        }

                        /** Verify whether game/event is created by this logged user or not for Rating pop up*/
                        if(userId.equals(creator_user_id) || (participantsArray != null && participantsArray.contains(userId)))
                        {
                            if(object_type.equalsIgnoreCase("Game") || object_type.equalsIgnoreCase("game") ) {
                                /**Keeping Game info inside local database for future review*/
                                db.insertGameDetailsForRating(name, list_id, "", "list", userId,eventDate,
                                        String.valueOf(ApplicationUtility.getGameDateTimeInMilisecond(eventDate + " " + endTime)));
                            }
                            else if(object_type.equalsIgnoreCase("Event") || object_type.equalsIgnoreCase("event") ) {
                                /**Keeping Game info inside local database for future review*/
                                db.insertGameDetailsForRating(name, "", id, "event", userId,eventDate,
                                        String.valueOf(ApplicationUtility.getGameDateTimeInMilisecond(eventDate + " " + endTime)));
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(getActivity(), false);
                        if (TabFragment.upcomingPastModelArrayList.size() > 0) {
                            if (TabFragment.event_listing_array != null &&
                                    TabFragment.event_listing_array.length() > 0) {
                                //Enabling the views
                                UpcomingPastGamesFragment.upcoming_recycler_view.setVisibility(View.VISIBLE);
                                UpcomingPastGamesFragment.past_recycler_view.setVisibility(View.INVISIBLE);
                                upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), TabFragment.upcomingPastModelArrayList, "UPCOMING");
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                UpcomingPastGamesFragment.upcoming_recycler_view.setLayoutManager(mLayoutManager);
                                UpcomingPastGamesFragment.upcoming_recycler_view.setItemAnimator(new DefaultItemAnimator());
                                UpcomingPastGamesFragment.upcoming_recycler_view.setAdapter(upcomingPastAdapter);
                                upcomingPastAdapter.notifyDataSetChanged();
                            } else {
                                UpcomingPastGamesFragment.upcoming_recycler_view.setVisibility(View.INVISIBLE);
                                UpcomingPastGamesFragment.past_recycler_view.setVisibility(View.INVISIBLE);
                                TabFragment.upcomingPastModelArrayList = new ArrayList<UpcomingPastModel>();
                                upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), TabFragment.upcomingPastModelArrayList, "UPCOMING");
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                UpcomingPastGamesFragment.upcoming_recycler_view.setLayoutManager(mLayoutManager);
                                UpcomingPastGamesFragment.upcoming_recycler_view.setItemAnimator(new DefaultItemAnimator());
                                UpcomingPastGamesFragment.upcoming_recycler_view.setAdapter(upcomingPastAdapter);
                                upcomingPastAdapter.notifyDataSetChanged();
                                snackBar.setSnackBarMessage("No upcoming events found");

                            }
                        } else {
                            UpcomingPastGamesFragment.upcoming_recycler_view.setVisibility(View.INVISIBLE);
                            UpcomingPastGamesFragment.past_recycler_view.setVisibility(View.INVISIBLE);
                            TabFragment.upcomingPastModelArrayList = new ArrayList<UpcomingPastModel>();
                            upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), TabFragment.upcomingPastModelArrayList, "UPCOMING");
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            UpcomingPastGamesFragment.upcoming_recycler_view.setLayoutManager(mLayoutManager);
                            UpcomingPastGamesFragment.upcoming_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            UpcomingPastGamesFragment.upcoming_recycler_view.setAdapter(upcomingPastAdapter);
                            upcomingPastAdapter.notifyDataSetChanged();
                            snackBar.setSnackBarMessage("No upcoming events found");

                        }
                    }
                });
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_user_events").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("current_time", ApplicationUtility.getDeviceCurrentTime());
        urlBuilder.addQueryParameter("date", ApplicationUtility.getDeviceCurrentDate());
        urlBuilder.addQueryParameter("v_type", "upcoming");
        urlBuilder.addQueryParameter("timezone",ApplicationUtility.getCurrentTimeZone());

        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }


    /**
     * Getting all Notifications from Server
     */
    public void getNotificationAPI(String user_id) {
        //Handling the loader state
        LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForAddPlayers(user_id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String userId = "", user_name = "",
                        user_profile_image = "",
                        notification_type = "",
                        sport_name = "", created_at = "",game_date = "", partitipants = "", visibility = "",
                        game_id = "",game_name = "",price = "",start_time = "",court = "",
                        object_type = "";
                String user_id = "", username = "", image = "", sport = "",
                        first_name = "",last_name = "",phone_number = "",
                        rewards = ""/*, notification_type = "friend_request_invited"*/;
                boolean is_registered_event = true;
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    notificationModelArrayList = new ArrayList<NotificationModel>();

                    JSONObject notif_jsonObject = new JSONObject(responseData);

                    if(notif_jsonObject.has("notifications") && !notif_jsonObject.isNull("notifications"))
                    {
                        JSONArray notification_jsonArray = notif_jsonObject.getJSONArray("notifications");
                        for (int i = 0; i < notification_jsonArray.length(); i++) {

                            notificationModel = new NotificationModel();
                            JSONObject notification_jsonObject = notification_jsonArray.getJSONObject(i);

                            if (notification_jsonObject.has("user_id") && !notification_jsonObject.isNull("user_id")) {
                                userId = notification_jsonObject.getString("user_id");
                            }
                            if (notification_jsonObject.has("user_name") && !notification_jsonObject.isNull("user_name")) {
                                user_name = notification_jsonObject.getString("user_name");
                            }
                            if (notification_jsonObject.has("user_profile_image") && !notification_jsonObject.isNull("user_profile_image")) {
                                user_profile_image = notification_jsonObject.getString("user_profile_image");
                            }
                            if (notification_jsonObject.has("notification_type") && !notification_jsonObject.isNull("notification_type")) {
                                notification_type = notification_jsonObject.getString("notification_type");
                            }
                            if (notification_jsonObject.has("sport_name") && !notification_jsonObject.isNull("sport_name")) {
                                sport_name = notification_jsonObject.getString("sport_name");
                            }
                            if (notification_jsonObject.has("created_at") && !notification_jsonObject.isNull("created_at")) {
                                created_at = notification_jsonObject.getString("created_at");
                            }
                            if (notification_jsonObject.has("game_date") && !notification_jsonObject.isNull("game_date")) {
                                game_date = notification_jsonObject.getString("game_date");
                            }
                            if (notification_jsonObject.has("partitipants") && !notification_jsonObject.isNull("partitipants")) {
                                partitipants = notification_jsonObject.getString("partitipants");
                            }
                            if (notification_jsonObject.has("visibility") && !notification_jsonObject.isNull("visibility")) {
                                visibility = notification_jsonObject.getString("visibility");
                            }
                            if (notification_jsonObject.has("game_id") && !notification_jsonObject.isNull("game_id")) {
                                game_id = notification_jsonObject.getString("game_id");
                            }

                            if (notification_jsonObject.has("game_name") && !notification_jsonObject.isNull("game_name")) {
                                game_name = notification_jsonObject.getString("game_name");
                            }
                            if (notification_jsonObject.has("is_registered_event") && !notification_jsonObject.isNull("is_registered_event")) {
                                is_registered_event = notification_jsonObject.getBoolean("is_registered_event");
                            }
                            if (notification_jsonObject.has("price") && !notification_jsonObject.isNull("price")) {
                                price = notification_jsonObject.getString("price");
                            }
                            if (notification_jsonObject.has("start_time") && !notification_jsonObject.isNull("start_time")) {
                                start_time = notification_jsonObject.getString("start_time");
                            }
                            if (notification_jsonObject.has("court") && !notification_jsonObject.isNull("court")) {
                                court = notification_jsonObject.getString("court");
                            }
                            if (notification_jsonObject.has("object_type") && !notification_jsonObject.isNull("object_type")) {
                                object_type = notification_jsonObject.getString("object_type");
                            }

                            long time_mili = ApplicationUtility.UTCTimeFormat(created_at);

                            notificationModel.setUser_id(userId);
                            notificationModel.setUser_name(user_name);
                            notificationModel.setUser_profile_image(user_profile_image);
                            notificationModel.setNotification_type(notification_type);
                            notificationModel.setGame_name(sport_name);
                            notificationModel.setCreated_at(created_at);
                            notificationModel.setGame_Date(game_date);
                            notificationModel.setPartitipants(partitipants);
                            notificationModel.setVisibility(visibility);
                            notificationModel.setGame_id(game_id);
                            notificationModel.setCreated_at_time_milisecond(String.valueOf(time_mili));
                            notificationModel.setGame_time(start_time);
                            notificationModel.setGame_final_name(game_name);
                            notificationModel.setPer_player_price(price);
                            notificationModel.setCourt_name(court);
                            notificationModel.setIs_registered_event(is_registered_event);
                            notificationModel.setObject_type(object_type);

                            notificationModelArrayList.add(notificationModel);
                        }
                    }

                    if(notif_jsonObject.has("all_users") && !notif_jsonObject.isNull("all_users")) {

                        JSONArray all_user_json_array = notif_jsonObject.getJSONArray("all_users");

                        for (int i = 0; i < all_user_json_array.length(); i++) {

                            notificationModel = new NotificationModel();

                            JSONObject user_json_object = all_user_json_array.getJSONObject(i);

                            if (user_json_object.has("user_id") && !user_json_object.isNull("user_id")) {
                                user_id = user_json_object.getString("user_id");
                            }
                            if (user_json_object.has("username") && !user_json_object.isNull("username")) {
                                username = user_json_object.getString("username");
                            }
                            if (user_json_object.has("image") && !user_json_object.isNull("image")) {
                                image = user_json_object.getString("image");
                            }
                            if (user_json_object.has("sport") && !user_json_object.isNull("sport")) {
                                sport = user_json_object.getString("sport");
                            }
                            if (user_json_object.has("game_id") && !user_json_object.isNull("game_id")) {
                                game_id = user_json_object.getString("game_id");
                            }
                            if (user_json_object.has("created_at") && !user_json_object.isNull("created_at")) {
                                created_at = user_json_object.getString("created_at");
                            }
                            if (user_json_object.has("first_name") && !user_json_object.isNull("first_name")) {
                                first_name = user_json_object.getString("first_name");
                            }
                            if (user_json_object.has("last_name") && !user_json_object.isNull("last_name")) {
                                last_name = user_json_object.getString("last_name");
                            }
                            if (user_json_object.has("phone_number") && !user_json_object.isNull("phone_number")) {
                                phone_number = user_json_object.getString("phone_number");
                            }
                            if (user_json_object.has("rewards") && !user_json_object.isNull("rewards")) {
                                rewards = user_json_object.getString("rewards");
                            }

                            long time_mili = ApplicationUtility.UTCTimeFormat(created_at);
                            notificationModel.setUser_id(user_id);
                            notificationModel.setUser_name(username);
                            notificationModel.setUser_profile_image(image);
                            notificationModel.setNotification_type("friend_request_invited");
                            notificationModel.setGame_name(sport);
                            notificationModel.setCreated_at(created_at);
                            notificationModel.setGame_id(game_id);
                            notificationModel.setFirst_name(first_name);
                            notificationModel.setLast_name(last_name);
                            notificationModel.setStatus("");
                            notificationModel.setPhone_number(phone_number);
                            notificationModel.setRewards(rewards);
                            notificationModel.setCreated_at_time_milisecond(String.valueOf(System.currentTimeMillis()));

                            notificationModelArrayList.add(notificationModel);
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(getActivity(), false);

                        /*TimeZone timezone = TimeZone.getDefault();
                        String timeZone = timezone.getDisplayName();
                        String timeZoneId = timezone.getID();
                        int TimeZoneOffset = timezone.getRawOffset()/(60 * 60 * 1000);
                        System.out.print(""+TimeZoneOffset);*/

                            if (notificationModelArrayList.size() > 0) {
                                Constants.PUSH_NOTIFICATION_BADGE_COUNT = 0;
                                notification_count.setVisibility(View.INVISIBLE);

                                Collections.sort(notificationModelArrayList, new Comparator<NotificationModel>() {
                                    @Override
                                    public int compare(NotificationModel lhs, NotificationModel rhs) {
                                        return String.valueOf(lhs.getCreated_at_time_milisecond()).compareTo(String.valueOf(rhs.getCreated_at_time_milisecond()));
                                    }
                                });
                                Collections.reverse(notificationModelArrayList);

                                NotificationFragment.notification_recycler_view.setVisibility(View.VISIBLE);
                                notificationAdapter = new NotificationAdapter(getActivity(), notificationModelArrayList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                NotificationFragment.notification_recycler_view.setLayoutManager(mLayoutManager);
                                NotificationFragment.notification_recycler_view.setItemAnimator(new DefaultItemAnimator());
                                NotificationFragment.notification_recycler_view.setAdapter(notificationAdapter);
                                notificationAdapter.notifyDataSetChanged();
                            } else {
                                notificationModelArrayList = new ArrayList<NotificationModel>();
                                notificationAdapter = new NotificationAdapter(getActivity(), notificationModelArrayList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                NotificationFragment.notification_recycler_view.setLayoutManager(mLayoutManager);
                                NotificationFragment.notification_recycler_view.setItemAnimator(new DefaultItemAnimator());
                                NotificationFragment.notification_recycler_view.setAdapter(notificationAdapter);
                                notificationAdapter.notifyDataSetChanged();
                                snackBar.setSnackBarMessage("No Notification available");
                            }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForAddPlayers(String user_id) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_notifications").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("location", String.valueOf(HomeFragment.currLocLatitude + "," + HomeFragment.currLocLongitude));
        urlBuilder.addQueryParameter("timezone",ApplicationUtility.getCurrentTimeZone());
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Getting all Friend request from Server
     */
    public void getFriendRequestInvitationAPI(String auth_token) {
        //Handling the loader state
        LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForFriendrequest(auth_token);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String user_id = "", username = "", image = "",
                        created_at = "0", sport = "", game_id = "",
                        first_name = "", last_name = "", phone_number = "",
                        rewards = "", notification_type = "friend_request_invited";
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject friend_request_jsonObject = new JSONObject(responseData);

                    String status = friend_request_jsonObject.getString("status");

                    if (friend_request_jsonObject.has("all_users") &&
                            !friend_request_jsonObject.isNull("all_users")) {
                        JSONArray all_user_json_array = friend_request_jsonObject.getJSONArray("all_users");

                        for (int i = 0; i < all_user_json_array.length(); i++) {
                            notificationModel = new NotificationModel();
                            JSONObject user_json_object = all_user_json_array.getJSONObject(i);
                            if (user_json_object.has("user_id") && !user_json_object.isNull("user_id")) {
                                user_id = user_json_object.getString("user_id");
                            }
                            if (user_json_object.has("username") && !user_json_object.isNull("username")) {
                                username = user_json_object.getString("username");
                            }
                            if (user_json_object.has("image") && !user_json_object.isNull("image")) {
                                image = user_json_object.getString("image");
                            }
                            if (user_json_object.has("sport") && !user_json_object.isNull("sport")) {
                                sport = user_json_object.getString("sport");
                            }
                            if (user_json_object.has("game_id") && !user_json_object.isNull("game_id")) {
                                game_id = user_json_object.getString("game_id");
                            }
                            if (user_json_object.has("created_at") && !user_json_object.isNull("created_at")) {
                                created_at = user_json_object.getString("created_at");
                            }
                            if (user_json_object.has("first_name") && !user_json_object.isNull("first_name")) {
                                first_name = user_json_object.getString("first_name");
                            }
                            if (user_json_object.has("last_name") && !user_json_object.isNull("last_name")) {
                                last_name = user_json_object.getString("last_name");
                            }
                            if (user_json_object.has("phone_number") && !user_json_object.isNull("phone_number")) {
                                phone_number = user_json_object.getString("phone_number");
                            }
                            if (user_json_object.has("rewards") && !user_json_object.isNull("rewards")) {
                                rewards = user_json_object.getString("rewards");
                            }
                            long time_mili = ApplicationUtility.UTCTimeFormat(created_at);
                            notificationModel.setUser_id(user_id);
                            notificationModel.setUser_name(username);
                            notificationModel.setUser_profile_image(image);
                            notificationModel.setNotification_type(notification_type);
                            notificationModel.setGame_name(sport);
                            notificationModel.setCreated_at(created_at);
                            notificationModel.setGame_id(game_id);
                            notificationModel.setFirst_name(first_name);
                            notificationModel.setLast_name(last_name);
                            notificationModel.setStatus(status);
                            notificationModel.setPhone_number(phone_number);
                            notificationModel.setRewards(rewards);
                            notificationModel.setCreated_at_time_milisecond(String.valueOf(System.currentTimeMillis()));

                            if(notificationModelArrayList == null){
                                notificationModelArrayList = new ArrayList<NotificationModel>();
                            }
                            notificationModelArrayList.add(notificationModel);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(getActivity(), false);
                        if (notificationModelArrayList.size() > 0) {
                            Collections.sort(notificationModelArrayList, new Comparator<NotificationModel>() {
                                @Override
                                public int compare(NotificationModel lhs, NotificationModel rhs) {
                                    return String.valueOf(lhs.getCreated_at_time_milisecond()).compareTo(String.valueOf(rhs.getCreated_at_time_milisecond()));
                                }
                            });
                            Collections.reverse(notificationModelArrayList);

                            NotificationFragment.notification_recycler_view.setVisibility(View.VISIBLE);
                            notificationAdapter = new NotificationAdapter(getActivity(), notificationModelArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            NotificationFragment.notification_recycler_view.setLayoutManager(mLayoutManager);
                            NotificationFragment.notification_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            NotificationFragment.notification_recycler_view.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                        } else {
                            notificationModelArrayList = new ArrayList<NotificationModel>();
                            notificationAdapter = new NotificationAdapter(getActivity(), notificationModelArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            NotificationFragment.notification_recycler_view.setLayoutManager(mLayoutManager);
                            NotificationFragment.notification_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            NotificationFragment.notification_recycler_view.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                            snackBar.setSnackBarMessage("No Notification available");
                        }
                    }
                });

            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForFriendrequest(String auth_token) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "pending_requests").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    @Override
    public void onResume() {
        super.onResume();

        //Refreshing the chat list view after the group name edit
        if (AppConstants.isUserSentMessage) {

            //Initializing the conversation list model array
            ChatFragmnet.conversationModel = new ArrayList<>();

            //Initializing the Chat channel id array
            messageChannelIdsArray = new ArrayList<String>();

            //pushdata();
            fetchValue();

            //Making the group name updated as false
            AppConstants.isUserSentMessage = false;
        }
        getActivity().registerReceiver(mMessageReceiver, new IntentFilter("unique_name"));

        if(AppConstants.isApplicationEnteredToBackground){
            //Making the flag false
            AppConstants.isApplicationEnteredToBackground = false;
            //Refreshing the badge count though the API call
            //When the app is brought to the fore ground from back ground
            String timeStamp = pref.getString(Constants.BADGE_COUNT_API_CALLED_TIME, null);

            if(!TextUtils.isEmpty(timeStamp)){

                if(!Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND){
                    //Getting the badge Count for the user
                    getBadgeCount(timeStamp);
                }
            }else{
                if(!Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND){
                    //Getting the badge Count for the user
                    getBadgeCount(null);
                }
            }
        }
    }

    //Must unregister onPause()
    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onStop() {
        super.onStop();
        boolean getApplicationState = isAppIsInBackground(getActivity());

        //Keeping the application state in a variable. When the application is going to background
        // (Minimized using the home button).
        if(getApplicationState) {
            Constants.PUSH_NOTIFICATION_BADGE_COUNT = 0;
            AppConstants.isApplicationEnteredToBackground = true;
        }
    }


    /**
     * Function responsible for update the notification badge count
     */
    public void getBadgeCount(String timeStamp) {
        //Handling the loader state
        //LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildBadgeCountApiRequestBody(timeStamp);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    badgeCount = responseObject.getInt("badge_count");
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //keeping the APi called time
                        SplashScreen.keepTheBadgeCountApiCalledTime(getActivity(),SplashScreen.called_time);
                        //Assigning the value after the API get called
                        if(badgeCount > 0 &&
                                !is_Page_Refresh_Status){
                            notification_count.setVisibility(View.VISIBLE);
                            Constants.PUSH_NOTIFICATION_BADGE_COUNT = badgeCount;
                            notification_count.setText(String.valueOf(Constants.PUSH_NOTIFICATION_BADGE_COUNT));
                        }

                        //Handling the loader state
                       // LoaderUtility.handleLoader(getActivity(), false);
                    }
                });
            }
        });
    }


    /**
     * Function to build the Badge count API request
     *
     * @return APi request
     */
    public Request buildBadgeCountApiRequestBody(String timeStamp) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_badge_count").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        if(!TextUtils.isEmpty(timeStamp)){
            urlBuilder.addQueryParameter("prev_seen_at", timeStamp);
        }else{
            timeStamp = ApplicationUtility.getCurrentDateTime();
            urlBuilder.addQueryParameter("prev_seen_at", timeStamp);
        }
        SplashScreen.called_time = ApplicationUtility.getCurrentDateTime();
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }


    /**
     * Check whether the app is in foreground or background
     * @param context
     * @return true if the app goes to the back ground, false if the app is in fore ground
     */
    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    /**
     * VIEW PAGER ADAPTER CLASS KEEPING ALL FRAGMENTS & TITLE WITH ICONS
     */
    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    /**
     * Function Responsible for the API call for the game Availibility
     */
    public void getJoinGameAvailibity() {
        //Handling the loader state
        LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        OkHttpClient client = new OkHttpClient();

        //Building the Request Parameters
        Request request = buildGameVerifyApiRequestBody();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    HomeFragment.isGameAvailable = responseObject.getBoolean("is_game_available");
                }catch(Exception ex){
                    ex.printStackTrace();
                }


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(getActivity(), false);

                        //Handling the API response
                        if(HomeFragment.isGameAvailable){
                            //Setting the back grond image of the image view
                            HomeFragment.join_game_icon.setBackgroundResource(R.mipmap.img_join_game_updated);
                            //Enabling the card view
                            HomeFragment.card_view_join_game.setEnabled(true);
                            //Enabling the card view for click events
                            HomeFragment.card_view_join_game.setClickable(true);
                            HomeFragment.join_game_icon.setClickable(true);
                            HomeFragment.join_game_icon.setEnabled(true);
                            HomeFragment.join_layout.setFocusable(true);
                            HomeFragment.join_layout.setClickable(true);
                            HomeFragment.join_layout.setEnabled(true);
                            HomeFragment.lay_placeholder.setVisibility(View.GONE);
                        }else{
                            //Setting the back grond image of the image view
                            HomeFragment.join_game_icon.setBackgroundResource(R.drawable.ic_join_game);
                            //Disabling the card view
                            HomeFragment.card_view_join_game.setEnabled(false);
                            //Disabling the card view for click events
                            HomeFragment.card_view_join_game.setClickable(false);
                            HomeFragment.join_game_icon.setClickable(false);
                            HomeFragment.join_game_icon.setEnabled(false);
                            HomeFragment.join_layout.setFocusable(false);
                            HomeFragment.join_layout.setClickable(false);
                            HomeFragment.join_layout.setEnabled(false);
                            HomeFragment.lay_placeholder.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });
    }



    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildGameVerifyApiRequestBody() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "verify_game_created").newBuilder();
        urlBuilder.addQueryParameter("location", String.valueOf(HomeFragment.gpsTracer.getLatitude()+","+ HomeFragment.gpsTracer.getLongitude()));
        urlBuilder.addQueryParameter("current_city", HomeFragment.current_city);
        urlBuilder.addQueryParameter("city", HomeFragment.current_location_city);
        urlBuilder.addQueryParameter("state", HomeFragment.current_location_state);
        urlBuilder.addQueryParameter("country", HomeFragment.current_location_country);
        urlBuilder.addQueryParameter("sport_name", Constants.SPORTS_NAME);
        urlBuilder.addQueryParameter("current_time", ApplicationUtility.getDeviceCurrentTime());
        urlBuilder.addQueryParameter("date", ApplicationUtility.getDeviceCurrentDate());
        urlBuilder.addQueryParameter("email", HomeFragment.user_email);
        urlBuilder.addQueryParameter("user_id", HomeFragment.user_id);
        urlBuilder.addQueryParameter("user_profile_image", HomeFragment.user_profile_image);
        urlBuilder.addQueryParameter("user_name", HomeFragment.user_name);
        urlBuilder.addQueryParameter("device_type", Constants.DEVICE_TYPE);

        if(HomeFragment.firebase_token_preferences.contains("Fcm_id")) {
            String Fcm_registration_id = HomeFragment.firebase_token_preferences.getString("Fcm_id","");
            urlBuilder.addQueryParameter("firebase_registration_id", Fcm_registration_id);
        }

        //Sending the signedup_at at the time of registration,
        // to keep track of the user notifications
        if(NormalRegistrationStepTwo.signedup_at != null){
            urlBuilder.addQueryParameter("signedup_at", NormalRegistrationStepTwo.signedup_at);
            NormalRegistrationStepTwo.signedup_at = null;
        }
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

}

