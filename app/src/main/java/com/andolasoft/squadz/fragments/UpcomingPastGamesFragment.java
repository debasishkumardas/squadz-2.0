package com.andolasoft.squadz.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.EventListing;
import com.andolasoft.squadz.activities.NavigationDrawerActivity;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.models.UpcomingPastModel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.adapters.UpcomingPastAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UpcomingPastGamesFragment extends Fragment {

    RelativeLayout upcoming_layout;
    RelativeLayout past_layout;
    public static TextView upcoming_text;
    public static TextView past_text;
    public static View upcoming_view;
    public static View past_view;
    public static RecyclerView upcoming_recycler_view;
    public static RecyclerView past_recycler_view;
    private ImageView toggle_button;
    private View rootView;
    SnackBar snackBar;
    ProgressDialog dialog;
    public static ArrayList<UpcomingPastModel> upcomingPastModelArrayList;
    JSONArray event_listing_array = null;
    UpcomingPastModel upcomingPastModel;
    UpcomingPastAdapter upcomingPastAdapter;
    String userId = null;
    SharedPreferences pref;
    SwipeRefreshLayout swipe_refresh_layout_upcoming_past;
    String event_type = "UPCOMING";
    int count = 0;
    DatabaseAdapter db;

    public UpcomingPastGamesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.mylocker_fragment, container, false);

        /**
         * Initialize all views belongs to this layoutINITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
         */
        initReferences(rootView);

        /**
         * Added click events on the views
         */
        setOnClickEvents();

        //getEventListing();

        return rootView;

    }

    /**
     * Added click events on the views
     */
    public void setOnClickEvents() {

        /**Toggle button click*/
        toggle_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(
                        ((NavigationDrawerActivity) getActivity()).openDrawerRunnable(true),
                        200);
            }
        });

        /**Upcoming button click*/
        upcoming_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                event_type = "UPCOMING";

                upcoming_view.setVisibility(View.VISIBLE);
                past_view.setVisibility(View.INVISIBLE);
                upcoming_text.setTextColor(getResources().getColor(R.color.gray_new));
                past_text.setTextColor(getResources().getColor(R.color.gray_additional));

                upcoming_recycler_view.setVisibility(View.INVISIBLE);
                past_recycler_view.setVisibility(View.INVISIBLE);

                /*count = count + 1;
                if(count == 1) {
                    getUpcomingListFromTab();
                }
                else{
                    /*//** Calling Already Past Event API *//*/
                    getUpcomingPastEventListing(event_type);
                }*/
                //** Calling Already Past Event API *//
                getUpcomingPastEventListing(event_type);

            }
        });

        /**Past button click*/
        past_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                event_type = "PAST";

                past_view.setVisibility(View.VISIBLE);
                upcoming_view.setVisibility(View.INVISIBLE);
                upcoming_text.setTextColor(getResources().getColor(R.color.gray_additional));
                past_text.setTextColor(getResources().getColor(R.color.gray_new));

                upcoming_recycler_view.setVisibility(View.INVISIBLE);
                past_recycler_view.setVisibility(View.INVISIBLE);

                /** Calling Already Past Event API */
                getUpcomingPastEventListing(event_type);

                /*count = count + 1;
                if(count == 1) {
                    *//** Calling Already Past Event API *//*
                    getUpcomingPastEventListing(event_type);
                }
                else{
                    *//**Set data inside Recycler view*//*
                    setDataToAdapter(event_type);
                }*/
            }
        });

        //Pull to refresh
        swipe_refresh_layout_upcoming_past.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                /** Calling Get Event API */

                if(event_type.equalsIgnoreCase("UPCOMING")) {
                    getUpcomingPastEventListing(event_type);
                }
                else{
                    getUpcomingPastEventListing(event_type);
                }

                swipe_refresh_layout_upcoming_past.setRefreshing(false);
            }
        });
    }


    /**
     * Function to get Event listing from server
     */
    public void getUpcomingPastEventListing(final String event_type) {
        //Handling the loader state
        LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(event_type);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
                snackBar.setSnackBarMessage("NetWork Error");
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", image = "", name = "", latitude = "", longitude = "",
                        review_rating = "", review_count = "", price = "", court_type = "", skill_level = "",
                        eventDate = "", no_of_participants = "",sport_name = "", total_number_players = "", startTime = "", endTime = "",
                        creator_username = "", creator_user_id = "", creator_profile_image = "",
                        actual_price = "", minimum_price = "", eventVisibility = "",address = "",venue_id = "",
                        object_type = "",list_id = "";
                boolean isfavorite = false;
                ArrayList<String> participantsArray = null;

                try {
                    upcomingPastModelArrayList = new ArrayList<>();
                    event_listing_array = new JSONArray(responseData);

                    /**Remove older data & keeping new data*/
                    db.deleteRatingsInfo();
                    for (int i = 0; i < event_listing_array.length(); i++) {
                        upcomingPastModel = new UpcomingPastModel();

                        JSONObject responseObject = event_listing_array.getJSONObject(i);

                        if (responseObject.has("id") && !responseObject.isNull("id")) {
                            id = responseObject.getString("id");
                        }
                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            image = responseObject.getString("image");
                        }
                        if (responseObject.has("name") && !responseObject.isNull("name")) {
                            name = responseObject.getString("name");
                        }
                        if (responseObject.has("latitude") && !responseObject.isNull("latitude")) {
                            latitude = responseObject.getString("latitude");
                        }
                        if (responseObject.has("longitude") && !responseObject.isNull("longitude")) {
                            longitude = responseObject.getString("longitude");
                        }
                        if (responseObject.has("review_rating") && !responseObject.isNull("review_rating")) {
                            review_rating = responseObject.getString("review_rating");
                        }
                        if (responseObject.has("review_count") && !responseObject.isNull("review_count")) {
                            review_count = responseObject.getString("review_count");
                        }

                        if (responseObject.has("no_of_player") && !responseObject.isNull("no_of_player")) {
                            total_number_players = responseObject.getString("no_of_player");
                        }

                        if (responseObject.has("no_of_partitipants") && !responseObject.isNull("no_of_partitipants")) {
                            no_of_participants = responseObject.getString("no_of_partitipants");
                        }
                        if (responseObject.has("sport_name") && !responseObject.isNull("sport_name")) {
                            sport_name = responseObject.getString("sport_name");
                        }
                        /**Keeping venue ID for NON-Register games*/
                        if (responseObject.has("venue_id") && !responseObject.isNull("venue_id")) {
                            venue_id = responseObject.getString("venue_id");
                        }


                        if (responseObject.has("price") && !responseObject.isNull("price")) {
                            price = responseObject.getString("price");
                            try {
                                int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                                float price_pre_player = Float.parseFloat(price) / (float) totalPlayer;
                                actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                                float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                                minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        if (responseObject.has("skill_level") && !responseObject.isNull("skill_level")) {
                            skill_level = responseObject.getString("skill_level");
                        }

                        if (responseObject.has("date") && !responseObject.isNull("date")) {
                            eventDate = responseObject.getString("date");
                        }


                        if (responseObject.has("user_name") && !responseObject.isNull("user_name")) {
                            creator_username = responseObject.getString("user_name");
                        }

                        if (responseObject.has("user_id") && !responseObject.isNull("user_id")) {
                            creator_user_id = responseObject.getString("user_id");
                        }

                        if (responseObject.has("user_profile_image") && !responseObject.isNull("user_profile_image")) {
                            creator_profile_image = responseObject.getString("user_profile_image");
                        }

                        if (responseObject.has("user_profile_image") && !responseObject.isNull("user_profile_image")) {
                            creator_profile_image = responseObject.getString("user_profile_image");
                        }

                        if (responseObject.has("is_favorite") && !responseObject.isNull("is_favorite")) {
                            isfavorite = responseObject.getBoolean("is_favorite");
                        }
                        if (responseObject.has("object_type") && !responseObject.isNull("object_type")) {
                            object_type = responseObject.getString("object_type");
                        }

                        /*if (responseObject.has("time_slots") && !responseObject.isNull("time_slots")) {
                            ArrayList<String> images_arraylist = new ArrayList<String>();
                            JSONArray images_json_array = responseObject.getJSONArray("time_slots");
                            for (int j = 0; j < images_json_array.length(); j++) {
                                JSONArray getValues = images_json_array.getJSONArray(j);
                                for (int x = 0; x < getValues.length(); x++) {
                                    if (j == 0) {
                                        startTime = getValues.getString(1);
                                    } else {
                                        endTime = getValues.getString(1);
                                    }
                                }
                            }
                            eventListingModel.setImages_array(images_arraylist);
                        }*/


                        if (responseObject.has("time_slots") && !responseObject.isNull("time_slots")) {
                            String start_time = null;
                            JSONArray time_slot_array_json_array = responseObject.getJSONArray("time_slots");
                            for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                                JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                                if (j == 0) {
                                    startTime = getValues.getString("start_time");
                                }
                                endTime = getValues.getString("end_time");
                            }
                        }

                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            ArrayList<String> images_arraylist = new ArrayList<String>();
                            JSONArray images_json_array = responseObject.getJSONArray("image");
                            if (images_json_array.length() > 0) {
                                for (int j = 0; j < images_json_array.length(); j++) {
                                    images_arraylist.add(images_json_array.getString(j));
                                }
                                upcomingPastModel.setImages_array(images_arraylist);
                            } else {
                                int getEventImage = SportsImagePicker.getDefaultSportImage(sport_name);
                                upcomingPastModel.setImage(getEventImage);
                            }
                        } else {
                            int getEventImage = SportsImagePicker.getDefaultSportImage(sport_name);
                            upcomingPastModel.setImage(getEventImage);
                        }

                        if (responseObject.has("visibility") && !responseObject.isNull("visibility")) {
                            eventVisibility = responseObject.getString("visibility");
                        }
                        if (responseObject.has("address") && !responseObject.isNull("address")) {
                            address = responseObject.getString("address");
                        }
                        if (responseObject.has("list_id") && !responseObject.isNull("list_id")) {
                            list_id = responseObject.getString("list_id");
                        }

                        if (responseObject.has("partitipant_ids") && !responseObject.isNull("partitipant_ids")) {
                            participantsArray = new ArrayList<String>();
                            JSONArray images_json_array = responseObject.getJSONArray("partitipant_ids");
                            for (int j = 0; j < images_json_array.length(); j++) {
                                participantsArray.add(images_json_array.getString(j));
                            }
                            //eventListingModel.setImages_array(participantsArray);
                        }

                        if (eventVisibility != null &&
                                eventVisibility.equalsIgnoreCase("Private") &&
                                participantsArray.contains(userId)) {
                            //Setting the data in model
                            upcomingPastModel.setEvent_Id(id);
                            upcomingPastModel.setVenue_id(venue_id);
                            upcomingPastModel.setEvent_Image(image);
                            upcomingPastModel.setEvent_Name(name);
                            upcomingPastModel.setLatitude(latitude);
                            upcomingPastModel.setLongitude(longitude);
                            upcomingPastModel.setEvent_Ratings(review_rating);
                            upcomingPastModel.setEvent_Reviews(review_count);
                            upcomingPastModel.setEvent_Price(actual_price);
                            upcomingPastModel.setSkill_level(skill_level);
                            upcomingPastModel.setEventDate(eventDate);
                            upcomingPastModel.setEvent_Total_Participants(no_of_participants);
                            upcomingPastModel.setEventStartTime(startTime);
                            upcomingPastModel.setEventEndTime(endTime);
                            upcomingPastModel.setCapacity(total_number_players);
                            upcomingPastModel.setCreator_name(creator_username);
                            upcomingPastModel.setCreator_user_id(creator_user_id);
                            upcomingPastModel.setEvent_Creator_Image(creator_profile_image);
                            upcomingPastModel.setFavorite(isfavorite);
                            upcomingPastModel.setEvent_Minimum_Price(minimum_price);
                            upcomingPastModel.setVisibility(eventVisibility);
                            upcomingPastModel.setAddress(address);
                            upcomingPastModel.setEvent_Sports_Type(sport_name);
                            upcomingPastModel.setObject_Type(object_type);
                            upcomingPastModel.setEvent_Total_Price(price);
                            upcomingPastModel.setList_Id(list_id);
                            upcomingPastModelArrayList.add(upcomingPastModel);
                        } else if (eventVisibility != null &&
                                eventVisibility.equalsIgnoreCase("Public")) {
                            //Setting the data in model
                            upcomingPastModel.setEvent_Id(id);
                            upcomingPastModel.setVenue_id(venue_id);
                            upcomingPastModel.setEvent_Image(image);
                            upcomingPastModel.setEvent_Name(name);
                            upcomingPastModel.setLatitude(latitude);
                            upcomingPastModel.setLongitude(longitude);
                            upcomingPastModel.setEvent_Ratings(review_rating);
                            upcomingPastModel.setEvent_Reviews(review_count);
                            upcomingPastModel.setEvent_Price(actual_price);
                            upcomingPastModel.setSkill_level(skill_level);
                            upcomingPastModel.setEventDate(eventDate);
                            upcomingPastModel.setEvent_Total_Participants(no_of_participants);
                            upcomingPastModel.setEventStartTime(startTime);
                            upcomingPastModel.setEventEndTime(endTime);
                            upcomingPastModel.setCapacity(total_number_players);
                            upcomingPastModel.setCreator_name(creator_username);
                            upcomingPastModel.setCreator_user_id(creator_user_id);
                            upcomingPastModel.setEvent_Creator_Image(creator_profile_image);
                            upcomingPastModel.setFavorite(isfavorite);
                            upcomingPastModel.setEvent_Minimum_Price(minimum_price);
                            upcomingPastModel.setVisibility(eventVisibility);
                            upcomingPastModel.setAddress(address);
                            upcomingPastModel.setEvent_Sports_Type(sport_name);
                            upcomingPastModel.setObject_Type(object_type);
                            upcomingPastModel.setEvent_Total_Price(price);
                            upcomingPastModel.setList_Id(list_id);
                            upcomingPastModelArrayList.add(upcomingPastModel);
                        }
                        if (event_type.equalsIgnoreCase("UPCOMING"))
                        {
                        /** Verify whether game/event is created by this logged user or not for Rating pop up*/
                            if (userId.equals(creator_user_id) || (participantsArray != null && participantsArray.contains(userId))) {
                                if (object_type.equalsIgnoreCase("Game") || object_type.equalsIgnoreCase("game")) {
                                    /**Keeping Game info inside local database for future review*/
                                    db.insertGameDetailsForRating(name, list_id, "", "list", userId, eventDate,
                                            String.valueOf(ApplicationUtility.getGameDateTimeInMilisecond(eventDate + " " + endTime)));
                                } else if (object_type.equalsIgnoreCase("Event") || object_type.equalsIgnoreCase("event")) {
                                    /**Keeping Game info inside local database for future review*/
                                    db.insertGameDetailsForRating(name, "", id, "event", userId, eventDate,
                                            String.valueOf(ApplicationUtility.getGameDateTimeInMilisecond(eventDate + " " + endTime)));
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(getActivity(), false);

                        /**Set data inside Recycler view*/
                        setDataToAdapter(event_type);
                    }
                });
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String event_type) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_user_events").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("current_time", ApplicationUtility.getDeviceCurrentTime());
        urlBuilder.addQueryParameter("date", ApplicationUtility.getDeviceCurrentDate());

        if(event_type.equalsIgnoreCase("UPCOMING")) {
            urlBuilder.addQueryParameter("v_type", "upcoming");
        } else if(event_type.equalsIgnoreCase("PAST")) {
            urlBuilder.addQueryParameter("v_type", "past");
        }

        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(getActivity(), "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Set data inside Recycler view
     */
    public void setDataToAdapter(String event_type)
    {
        if(event_type.equalsIgnoreCase("UPCOMING")) {
            if (upcomingPastModelArrayList.size() > 0) {
                if (event_listing_array != null &&
                        event_listing_array.length() > 0) {
                    //Enabling the views
                    upcoming_recycler_view.setVisibility(View.VISIBLE);
                    upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), upcomingPastModelArrayList,event_type);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    upcoming_recycler_view.setLayoutManager(mLayoutManager);
                    upcoming_recycler_view.setItemAnimator(new DefaultItemAnimator());
                    upcoming_recycler_view.setAdapter(upcomingPastAdapter);
                    upcomingPastAdapter.notifyDataSetChanged();
                } else {
                    upcoming_recycler_view.setVisibility(View.INVISIBLE);
                    upcomingPastModelArrayList = new ArrayList<UpcomingPastModel>();
                    upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), upcomingPastModelArrayList,event_type);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    upcoming_recycler_view.setLayoutManager(mLayoutManager);
                    upcoming_recycler_view.setItemAnimator(new DefaultItemAnimator());
                    upcoming_recycler_view.setAdapter(upcomingPastAdapter);
                    upcomingPastAdapter.notifyDataSetChanged();
                    snackBar.setSnackBarMessage("No upcoming events found");

                }
            }
            else{
                    upcoming_recycler_view.setVisibility(View.INVISIBLE);
                    upcomingPastModelArrayList = new ArrayList<UpcomingPastModel>();
                    upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), upcomingPastModelArrayList,event_type);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    upcoming_recycler_view.setLayoutManager(mLayoutManager);
                    upcoming_recycler_view.setItemAnimator(new DefaultItemAnimator());
                    upcoming_recycler_view.setAdapter(upcomingPastAdapter);
                    upcomingPastAdapter.notifyDataSetChanged();
                    snackBar.setSnackBarMessage("No upcoming events found");
            }
        }
        else{
            if (upcomingPastModelArrayList.size() > 0) {
                if (event_listing_array != null &&
                        event_listing_array.length() > 0) {
                    //Enabling the views
                    past_recycler_view.setVisibility(View.VISIBLE);
                    upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), upcomingPastModelArrayList,event_type);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    past_recycler_view.setLayoutManager(mLayoutManager);
                    past_recycler_view.setItemAnimator(new DefaultItemAnimator());
                    past_recycler_view.setAdapter(upcomingPastAdapter);
                    upcomingPastAdapter.notifyDataSetChanged();
                } else {
                    past_recycler_view.setVisibility(View.INVISIBLE);
                    upcomingPastModelArrayList = new ArrayList<UpcomingPastModel>();
                    upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), upcomingPastModelArrayList,event_type);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    past_recycler_view.setLayoutManager(mLayoutManager);
                    past_recycler_view.setItemAnimator(new DefaultItemAnimator());
                    past_recycler_view.setAdapter(upcomingPastAdapter);
                    upcomingPastAdapter.notifyDataSetChanged();
                    snackBar.setSnackBarMessage("No past events found");

                }
            }
            else{
                    past_recycler_view.setVisibility(View.INVISIBLE);
                    upcomingPastModelArrayList = new ArrayList<UpcomingPastModel>();
                    upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), upcomingPastModelArrayList,event_type);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    upcoming_recycler_view.setLayoutManager(mLayoutManager);
                    upcoming_recycler_view.setItemAnimator(new DefaultItemAnimator());
                    upcoming_recycler_view.setAdapter(upcomingPastAdapter);
                    upcomingPastAdapter.notifyDataSetChanged();
                    snackBar.setSnackBarMessage("No past events found");
            }
        }
    }
    /**
     * Getting Upcoming results from Tab fragment
     */
    public void getUpcomingListFromTab()
    {
        if (TabFragment.upcomingPastModelArrayList != null &&
                TabFragment.upcomingPastModelArrayList.size() != 0) {
            if (TabFragment.event_listing_array != null &&
                    TabFragment.event_listing_array.length() > 0) {
                //Enabling the views
                upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), TabFragment.upcomingPastModelArrayList,event_type);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                upcoming_recycler_view.setLayoutManager(mLayoutManager);
                upcoming_recycler_view.setItemAnimator(new DefaultItemAnimator());
                upcoming_recycler_view.setAdapter(upcomingPastAdapter);
                upcomingPastAdapter.notifyDataSetChanged();
                upcoming_recycler_view.setVisibility(View.VISIBLE);
            } else {
                upcomingPastModelArrayList = new ArrayList<UpcomingPastModel>();
                upcomingPastAdapter = new UpcomingPastAdapter(getActivity(), upcomingPastModelArrayList,event_type);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                upcoming_recycler_view.setLayoutManager(mLayoutManager);
                upcoming_recycler_view.setItemAnimator(new DefaultItemAnimator());
                upcoming_recycler_view.setAdapter(upcomingPastAdapter);
                upcomingPastAdapter.notifyDataSetChanged();
                snackBar.setSnackBarMessage("No upcoming events found");

            }
        }
    }

    /**
     *
     * @param rootView
     */
    public void initReferences(View rootView) {

        //ButterKnife.inject(getActivity());
        toggle_button = (ImageView) rootView.findViewById(R.id.toggle_button_mylocker_fargment);
        upcoming_layout = (RelativeLayout) rootView.findViewById(R.id.upcoming_layout);
        past_layout = (RelativeLayout) rootView.findViewById(R.id.past_layout);
        upcoming_text = (TextView) rootView.findViewById(R.id.upcoming_text);
        past_text = (TextView) rootView.findViewById(R.id.past_text);
        upcoming_view = (View) rootView.findViewById(R.id.upcoming_view);
        past_view = (View) rootView.findViewById(R.id.past_view);
        upcoming_recycler_view = (RecyclerView) rootView.findViewById(R.id.upcoming_past_recycler_view);
        past_recycler_view = (RecyclerView) rootView.findViewById(R.id.past_recycler_view);
        swipe_refresh_layout_upcoming_past = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout_upcoming_past);

        snackBar = new SnackBar(getActivity());
        db = new DatabaseAdapter(getActivity());
        pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userId = pref.getString("loginuser_id", null);

        //Change the Swipe layout color
        swipe_refresh_layout_upcoming_past.setColorSchemeResources(R.color.orange,
                R.color.orange,
                R.color.orange);

        upcoming_recycler_view.setVisibility(View.INVISIBLE);
        past_recycler_view.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Constants.UPCOMING_DETAIL_FAVORITE || Constants.UPCOMING_GAME_UPDATED_SUCCESS)
        {
            Constants.UPCOMING_DETAIL_FAVORITE = false;
            Constants.UPCOMING_GAME_UPDATED_SUCCESS = false;
            getUpcomingPastEventListing(event_type);
        }
        if(Constants.LEAVED_STATUS || Constants.CANCEL_STATUS)
        {
            Constants.LEAVED_STATUS = false;
            Constants.CANCEL_STATUS = false;
            getUpcomingPastEventListing(event_type);
        }
    }
}
