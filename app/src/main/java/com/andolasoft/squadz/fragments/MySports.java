package com.andolasoft.squadz.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.NavigationDrawerActivity;

public class MySports extends Fragment {

    private ImageView toggle_button;
    private View rootView;

    public MySports() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.mylocker_fragment, container, false);

        /**
         * Initialize all views belongs to this layoutINITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
         */
        initReferences(rootView);

        /**
         * Added click events on the views
         */
        setOnClickEvents();

        return rootView;

    }

    /**
     * Added click events on the views
     */
    public void setOnClickEvents() {

        toggle_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(
                        ((NavigationDrawerActivity) getActivity()).openDrawerRunnable(true),
                        200);
            }
        });
    }

    /**
     *
     * @param rootView
     */
    public void initReferences(View rootView) {
        toggle_button = (ImageView) rootView.findViewById(R.id.toggle_button_mylocker_fargment);

    }

}
