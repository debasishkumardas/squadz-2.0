package com.andolasoft.squadz.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.AddNewTeam;
import com.andolasoft.squadz.activities.NavigationDrawerActivity;
import com.andolasoft.squadz.activities.VenueListing;
import com.andolasoft.squadz.models.NotificationModel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.NestedListView;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.adapters.NotificationAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NotificationFragment extends Fragment {

    ImageView toggle_button;
    private View rootView;
    public static RecyclerView notification_recycler_view;
    ProgressDialog dialog;
    NotificationAdapter notificationAdapter;
    SwipeRefreshLayout swipe_refresh_layout_notification;
    ArrayList<NotificationModel> notificationModelArrayList;
    NotificationModel notificationModel;
    SnackBar snackBar;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_blank3, container, false);

        /**
         * Initialize all views belongs to this layoutINITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
         */
        initReferences(rootView);

        /**
         * Added click events on the views
         */
        setOnClickEvents();

        return rootView;

    }

    /**
     * Added click events on the views
     */
    public void setOnClickEvents() {

        toggle_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(
                        ((NavigationDrawerActivity) getActivity()).openDrawerRunnable(true),
                        200);
            }
        });

        swipe_refresh_layout_notification.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_layout_notification.setRefreshing(false);
                getNotificationAPI(TabFragment.userId);
            }
        });
    }

    /**
     * Getting all Notifications from Server
     */
    public void getNotificationAPI(String user_id) {
        //Handling the loader state
        LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForAddPlayers(user_id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String userId = "",user_name = "",
                        user_profile_image = "",
                        notification_type = "",
                        sport_name = "",created_at = "0",game_date = ""
                        ,partitipants = "",visibility = "",
                        game_id = "",game_name = "",price = "",start_time = "",court = "",
                        object_type = "";

                String user_id = "", username = "", image = "", sport = "",
                        first_name = "",last_name = "",phone_number = "",
                        rewards = ""/*, notification_type = "friend_request_invited"*/;

                boolean is_registered_event = true;
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    notificationModelArrayList = new ArrayList<NotificationModel>();

                    JSONObject notif_jsonObject = new JSONObject(responseData);

                    if(notif_jsonObject.has("notifications") && !notif_jsonObject.isNull("notifications"))
                    {
                        JSONArray notification_jsonArray = notif_jsonObject.getJSONArray("notifications");
                        for (int i = 0; i < notification_jsonArray.length(); i++) {

                            notificationModel = new NotificationModel();
                            JSONObject notification_jsonObject = notification_jsonArray.getJSONObject(i);

                            if (notification_jsonObject.has("user_id") && !notification_jsonObject.isNull("user_id")) {
                                userId = notification_jsonObject.getString("user_id");
                            }
                            if (notification_jsonObject.has("user_name") && !notification_jsonObject.isNull("user_name")) {
                                user_name = notification_jsonObject.getString("user_name");
                            }
                            if (notification_jsonObject.has("user_profile_image") && !notification_jsonObject.isNull("user_profile_image")) {
                                user_profile_image = notification_jsonObject.getString("user_profile_image");
                            }
                            if (notification_jsonObject.has("notification_type") && !notification_jsonObject.isNull("notification_type")) {
                                notification_type = notification_jsonObject.getString("notification_type");
                            }
                            if (notification_jsonObject.has("sport_name") && !notification_jsonObject.isNull("sport_name")) {
                                sport_name = notification_jsonObject.getString("sport_name");
                            }
                            if (notification_jsonObject.has("created_at") && !notification_jsonObject.isNull("created_at")) {
                                created_at = notification_jsonObject.getString("created_at");
                            }
                            if (notification_jsonObject.has("game_date") && !notification_jsonObject.isNull("game_date")) {
                                game_date = notification_jsonObject.getString("game_date");
                            }
                            if (notification_jsonObject.has("partitipants") && !notification_jsonObject.isNull("partitipants")) {
                                partitipants = notification_jsonObject.getString("partitipants");
                            }
                            if (notification_jsonObject.has("visibility") && !notification_jsonObject.isNull("visibility")) {
                                visibility = notification_jsonObject.getString("visibility");
                            }
                            if (notification_jsonObject.has("game_id") && !notification_jsonObject.isNull("game_id")) {
                                game_id = notification_jsonObject.getString("game_id");
                            }

                            if (notification_jsonObject.has("game_name") && !notification_jsonObject.isNull("game_name")) {
                                game_name = notification_jsonObject.getString("game_name");
                            }
                            if (notification_jsonObject.has("is_registered_event") && !notification_jsonObject.isNull("is_registered_event")) {
                                is_registered_event = notification_jsonObject.getBoolean("is_registered_event");
                            }
                            if (notification_jsonObject.has("price") && !notification_jsonObject.isNull("price")) {
                                price = notification_jsonObject.getString("price");
                            }
                            if (notification_jsonObject.has("start_time") && !notification_jsonObject.isNull("start_time")) {
                                start_time = notification_jsonObject.getString("start_time");
                            }
                            if (notification_jsonObject.has("court") && !notification_jsonObject.isNull("court")) {
                                court = notification_jsonObject.getString("court");
                            }
                            if (notification_jsonObject.has("object_type") && !notification_jsonObject.isNull("object_type")) {
                                object_type = notification_jsonObject.getString("object_type");
                            }

                            long time_mili = ApplicationUtility.UTCTimeFormat(created_at);

                            notificationModel.setUser_id(userId);
                            notificationModel.setUser_name(user_name);
                            notificationModel.setUser_profile_image(user_profile_image);
                            notificationModel.setNotification_type(notification_type);
                            notificationModel.setGame_name(sport_name);
                            notificationModel.setCreated_at(created_at);
                            notificationModel.setGame_Date(game_date);
                            notificationModel.setPartitipants(partitipants);
                            notificationModel.setVisibility(visibility);
                            notificationModel.setGame_id(game_id);
                            notificationModel.setCreated_at_time_milisecond(String.valueOf(time_mili));
                            notificationModel.setGame_time(start_time);
                            notificationModel.setGame_final_name(game_name);
                            notificationModel.setPer_player_price(price);
                            notificationModel.setCourt_name(court);
                            notificationModel.setIs_registered_event(is_registered_event);
                            notificationModel.setObject_type(object_type);

                            notificationModelArrayList.add(notificationModel);
                        }
                    }

                    if(notif_jsonObject.has("all_users") && !notif_jsonObject.isNull("all_users")) {

                            JSONArray all_user_json_array = notif_jsonObject.getJSONArray("all_users");

                            for (int i = 0; i < all_user_json_array.length(); i++) {

                                notificationModel = new NotificationModel();

                                JSONObject user_json_object = all_user_json_array.getJSONObject(i);

                                if (user_json_object.has("user_id") && !user_json_object.isNull("user_id")) {
                                    user_id = user_json_object.getString("user_id");
                                }
                                if (user_json_object.has("username") && !user_json_object.isNull("username")) {
                                    username = user_json_object.getString("username");
                                }
                                if (user_json_object.has("image") && !user_json_object.isNull("image")) {
                                    image = user_json_object.getString("image");
                                }
                                if (user_json_object.has("sport") && !user_json_object.isNull("sport")) {
                                    sport = user_json_object.getString("sport");
                                }
                                if (user_json_object.has("game_id") && !user_json_object.isNull("game_id")) {
                                    game_id = user_json_object.getString("game_id");
                                }
                                if (user_json_object.has("created_at") && !user_json_object.isNull("created_at")) {
                                    created_at = user_json_object.getString("created_at");
                                }
                                if (user_json_object.has("first_name") && !user_json_object.isNull("first_name")) {
                                    first_name = user_json_object.getString("first_name");
                                }
                                if (user_json_object.has("last_name") && !user_json_object.isNull("last_name")) {
                                    last_name = user_json_object.getString("last_name");
                                }
                                if (user_json_object.has("phone_number") && !user_json_object.isNull("phone_number")) {
                                    phone_number = user_json_object.getString("phone_number");
                                }
                                if (user_json_object.has("rewards") && !user_json_object.isNull("rewards")) {
                                    rewards = user_json_object.getString("rewards");
                                }

                                long time_mili = ApplicationUtility.UTCTimeFormat(created_at);
                                notificationModel.setUser_id(user_id);
                                notificationModel.setUser_name(username);
                                notificationModel.setUser_profile_image(image);
                                notificationModel.setNotification_type("friend_request_invited");
                                notificationModel.setGame_name(sport);
                                notificationModel.setCreated_at(created_at);
                                notificationModel.setGame_id(game_id);
                                notificationModel.setFirst_name(first_name);
                                notificationModel.setLast_name(last_name);
                                notificationModel.setStatus("");
                                notificationModel.setPhone_number(phone_number);
                                notificationModel.setRewards(rewards);
                                notificationModel.setCreated_at_time_milisecond(String.valueOf(System.currentTimeMillis()));

                                notificationModelArrayList.add(notificationModel);
                            }
                        }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(getActivity(), false);

                        if(notificationModelArrayList.size() > 0) {
                            Collections.sort(notificationModelArrayList, new Comparator<NotificationModel>() {
                                @Override
                                public int compare(NotificationModel lhs, NotificationModel rhs) {
                                    return String.valueOf(lhs.getCreated_at_time_milisecond()).compareTo(String.valueOf(rhs.getCreated_at_time_milisecond()));
                                }
                            });
                            Collections.reverse(notificationModelArrayList);

                            notification_recycler_view.setVisibility(View.VISIBLE);
                            notificationAdapter = new NotificationAdapter(getActivity(),notificationModelArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            notification_recycler_view.setLayoutManager(mLayoutManager);
                            notification_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            notification_recycler_view.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                        } else{
                            notificationModelArrayList = new ArrayList<NotificationModel>();
                            notificationAdapter = new NotificationAdapter(getActivity(),notificationModelArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            notification_recycler_view.setLayoutManager(mLayoutManager);
                            notification_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            notification_recycler_view.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                            snackBar.setSnackBarMessage("No Notification available");
                        }

                        /*if(notificationModelArrayList.size() > 0)
                        {
=======
>>>>>>> 48871df1f86cd4a4b09ee769fc6c593c9036f011
                            getFriendRequestInvitationAPI(TabFragment.auth_token);
                        } else{
                            getFriendRequestInvitationAPI(TabFragment.auth_token);
                        }*/
                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForAddPlayers(String user_id) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_notifications").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("location", String.valueOf(HomeFragment.currLocLatitude+","+ HomeFragment.currLocLongitude));
        urlBuilder.addQueryParameter("timezone",ApplicationUtility.getCurrentTimeZone());

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }



    /**
     * Getting all Friend request from Server
     */
    public void getFriendRequestInvitationAPI(String auth_token) {
        //Handling the loader state
        LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForFriendrequest(auth_token);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String user_id = "", username = "", image = "",
                        created_at = "0", sport = "", game_id = "",
                        first_name = "",last_name = "",phone_number = "",
                        rewards = "", notification_type = "friend_request_invited";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject friend_request_jsonObject = new JSONObject(responseData);

                    String status = friend_request_jsonObject.getString("status");

                    if(friend_request_jsonObject.has("all_users") && !friend_request_jsonObject.isNull("all_users")) {
                        JSONArray all_user_json_array = friend_request_jsonObject.getJSONArray("all_users");

                        for (int i = 0; i < all_user_json_array.length(); i++) {

                            notificationModel = new NotificationModel();

                            JSONObject user_json_object = all_user_json_array.getJSONObject(i);

                            if(user_json_object.has("user_id") && !user_json_object.isNull("user_id")) {
                                user_id = user_json_object.getString("user_id");
                            }
                            if(user_json_object.has("username") && !user_json_object.isNull("username")) {
                                username = user_json_object.getString("username");
                            }
                            if(user_json_object.has("image") && !user_json_object.isNull("image")) {
                                image = user_json_object.getString("image");
                            }
                            if(user_json_object.has("sport") && !user_json_object.isNull("sport")) {
                                sport = user_json_object.getString("sport");
                            }
                            if(user_json_object.has("game_id") && !user_json_object.isNull("game_id")) {
                                game_id = user_json_object.getString("game_id");
                            }
                            if(user_json_object.has("created_at") && !user_json_object.isNull("created_at")) {
                                created_at = user_json_object.getString("created_at");
                            }
                            if(user_json_object.has("first_name") && !user_json_object.isNull("first_name")) {
                                first_name = user_json_object.getString("first_name");
                            }
                            if(user_json_object.has("last_name") && !user_json_object.isNull("last_name")) {
                                last_name = user_json_object.getString("last_name");
                            }
                            if(user_json_object.has("phone_number") && !user_json_object.isNull("phone_number")) {
                                phone_number = user_json_object.getString("phone_number");
                            }
                            if(user_json_object.has("rewards") && !user_json_object.isNull("rewards")) {
                                rewards = user_json_object.getString("rewards");
                            }

                            long time_mili = ApplicationUtility.UTCTimeFormat(created_at);
                            notificationModel.setUser_id(user_id);
                            notificationModel.setUser_name(username);
                            notificationModel.setUser_profile_image(image);
                            notificationModel.setNotification_type(notification_type);
                            notificationModel.setGame_name(sport);
                            notificationModel.setCreated_at(created_at);
                            notificationModel.setGame_id(game_id);
                            notificationModel.setFirst_name(first_name);
                            notificationModel.setLast_name(last_name);
                            notificationModel.setStatus(status);
                            notificationModel.setPhone_number(phone_number);
                            notificationModel.setRewards(rewards);
                            notificationModel.setCreated_at_time_milisecond(String.valueOf(System.currentTimeMillis()));

                            notificationModelArrayList.add(notificationModel);
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(getActivity(), false);

                        //Clearing all the notification from the notification tray
                        NavigationDrawerActivity.clearNotifications(getActivity());

                        if(notificationModelArrayList.size() > 0) {
                            Collections.sort(notificationModelArrayList, new Comparator<NotificationModel>() {
                                @Override
                                public int compare(NotificationModel lhs, NotificationModel rhs) {
                                    return String.valueOf(lhs.getCreated_at_time_milisecond()).compareTo(String.valueOf(rhs.getCreated_at_time_milisecond()));
                                }
                            });
                            Collections.reverse(notificationModelArrayList);

                            notification_recycler_view.setVisibility(View.VISIBLE);
                            notificationAdapter = new NotificationAdapter(getActivity(),notificationModelArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            notification_recycler_view.setLayoutManager(mLayoutManager);
                            notification_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            notification_recycler_view.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                        } else{
                            notificationModelArrayList = new ArrayList<NotificationModel>();
                            notificationAdapter = new NotificationAdapter(getActivity(),notificationModelArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            notification_recycler_view.setLayoutManager(mLayoutManager);
                            notification_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            notification_recycler_view.setAdapter(notificationAdapter);
                            notificationAdapter.notifyDataSetChanged();
                            snackBar.setSnackBarMessage("No Notification available");
                        }
                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForFriendrequest(String auth_token) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "pending_requests").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(getActivity(), "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     *
     * @param rootView
     */
    public void initReferences(View rootView) {
        toggle_button = (ImageView) rootView.findViewById(R.id.toggle_button_fargment_three);
        notification_recycler_view = (RecyclerView) rootView.findViewById(R.id.notification_recycler_view);
        swipe_refresh_layout_notification = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout_notification);

        snackBar = new SnackBar(getActivity());


        //Change the Swipe layout color
        swipe_refresh_layout_notification.setColorSchemeResources(R.color.orange,
                R.color.orange,
                R.color.orange);

    }
}
