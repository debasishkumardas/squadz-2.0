package com.andolasoft.squadz.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v13.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.EventListing;
import com.andolasoft.squadz.activities.NavigationDrawerActivity;
import com.andolasoft.squadz.activities.NormalRegistrationStepTwo;
import com.andolasoft.squadz.activities.VenueListing;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.managers.GpsConnectionHelper;
import com.andolasoft.squadz.managers.LocationPickerManager;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {

    DatabaseAdapter db;
    private View rootView;
    public static CardView card_view_start_game,card_view_join_game;
    private static Spinner spinner_sports_names, spinner_appleton;
    private ArrayList<String> sports_name_array, sports_id_array,
            venue_spinner_name_array, venue_spinner_id_array;
    private HashMap<String, String> map_sports_name, map_venue_name_spiner;
    private ProgressDialog dialog;
    private ImageView toggle_button;
    public static  RelativeLayout start_layout,join_layout, lay_placeholder;
    private TextView tv_no_games_found;
    public static ImageView start_game_icon,join_game_icon;
    private SnackBar snackBar;
    public static String address,current_city,current_location_city,current_location_state,current_location_country;
    public static GPSTracker gpsTracer;
    public static double currLocLatitude,currLocLongitude;
    protected LocationRequest locationRequest;
    protected GoogleApiClient mGoogleApiClient;
    public static int REQUEST_CHECK_SETTINGS = 100;
    public static final int PERMISSION_REQUEST_CODE = 1;
    public int deviceAndroidVerion;
    public static FragmentActivity fragmentContext;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    public static boolean isGameAvailable = false;
    public static boolean isSportSelected = false;
    public static SharedPreferences prefs,firebase_token_preferences;
    public static String userSport = null, user_email = "", user_id = "",user_name = "",user_profile_image = "";
    RelativeLayout toggle_button_layout;
    SwipeRefreshLayout swipe_refresh_layout_event;
    int chat_badge_count = 0, badge_count = 0;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_blank2, container, false);

        NavigationDrawerActivity.toolbar.setVisibility(View.GONE);
        NavigationDrawerActivity.mDrawerToggle.setDrawerIndicatorEnabled(true);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userSport = prefs.getString("user_sport", null);
        user_email = prefs.getString("user_email", null);
        user_id = prefs.getString("loginuser_id", null);
        user_name = prefs.getString("username", null);
        user_profile_image = prefs.getString("image", null);

        firebase_token_preferences = getActivity().getSharedPreferences("Firebase_Token", Context.MODE_PRIVATE);

        fragmentContext = this.getActivity();
        /**
         * Initializing all viewss belongs to this layout
         */
        initReferences(rootView);

        /**
         * Added click events on the views
         */
        setOnClickEvents();

       // getSportsName();

       // getJoinGameAvailibity();

        //Getting the android Version from the device
        deviceAndroidVerion = ApplicationUtility.getDeviceInstalledAndroidVersion();

        //requesting permission for Android 6.0
        if(deviceAndroidVerion >= 23){
            boolean value = checkLocationPermission();
                if (!checkLocationPermission()) {
                    //Requesting permission fop Location
                    requestPermissionAccessLocation();
                }else{
                    //Initializing the Google API client
                    initializeGoogleClient();
                   // GpsConnectionHelper gpsHelper = new GpsConnectionHelper(getActivity());
                    //get Location
                   // getAddress();
                    /**
                     * Function to get Venue name from server
                     */
                    //getVenuesName();
                    /**
                     * Getting Sports name from database
                     */
                    getSportsName();
                }
            }else{
                //Initializing the Google API client
                initializeGoogleClient();
               // GpsConnectionHelper gpsHelper = new GpsConnectionHelper(getActivity());

                //get Location
               // getAddress();

                /**
                * Function to get Venue name from server
                 */
                // getVenuesName();

                /**
                 * Getting Sports name from database
                 */
                 getSportsName();
            }


        return rootView;
    }


    public void handlePageredirectionsStartGame(){
        if(!TextUtils.isEmpty(current_city) &&
                Constants.SPORTS_NAME != null){
            Intent intent = new Intent(getActivity(), VenueListing.class);
            startActivity(intent);
        }else{
            if(current_city == null){
                Toast.makeText(getActivity(), "Please Select your location", Toast.LENGTH_SHORT).show();
            }else if(Constants.SPORTS_NAME == null){
                Toast.makeText(getActivity(), "Please Select a Sport", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void handlePageredirectionsJoinGame(){
        if(!TextUtils.isEmpty(current_city) &&
                Constants.SPORTS_NAME != null){
            Intent intent = new Intent(getActivity(), EventListing.class);
            startActivity(intent);
        }else{
            if(current_city == null){
                Toast.makeText(getActivity(), "Please Select your location", Toast.LENGTH_SHORT).show();
            }else if(Constants.SPORTS_NAME == null){
                Toast.makeText(getActivity(), "Please Select a Sport", Toast.LENGTH_SHORT).show();
            }

        }
    }

    /**
     * Added click events on the views
     */
    public void setOnClickEvents() {
        card_view_start_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handlePageredirectionsStartGame();
            }
        });
        card_view_join_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handlePageredirectionsJoinGame();

            }
        });

        start_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handlePageredirectionsStartGame();
            }
        });

        join_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handlePageredirectionsJoinGame();
            }
        });

        start_game_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handlePageredirectionsStartGame();
            }
        });

        join_game_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handlePageredirectionsJoinGame();
            }
        });

        /*toggle_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(
                        ((NavigationDrawerActivity) getActivity()).openDrawerRunnable(true),
                        200);
            }
        });*/

        toggle_button_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(
                        ((NavigationDrawerActivity) getActivity()).openDrawerRunnable(true),
                        200);
            }
        });


                //Spinner Item Click event
        spinner_sports_names.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //Counter check for not calling getVenuesFilterListing() for first time
                // as it was called for first time automatically
                String sportName = spinner_sports_names.getSelectedItem().toString();
                Constants.SPORTS_NAME = sportName;
                String sportId = map_sports_name.get(sportName);
                Constants.SPORTS_ID = sportId;

                if(!isSportSelected){
                    //Calling the APi to refresh the View
                    getJoinGameAvailibity();
                }
                isSportSelected = false;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner_appleton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    /*Intent intent = new Intent(getActivity(), LocationPicker.class);
                    startActivity(intent);*/

                    //openAutocompleteActivity();
                    LocationPickerManager locationManager
                            = new LocationPickerManager(getActivity());
                }
                return true;
            }
        });

        //Pull to refresh functionality
        swipe_refresh_layout_event.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Checking game Created
                getJoinGameAvailibity();
                swipe_refresh_layout_event.setRefreshing(false);
            }
        });
    }

    //Getting Sports name from database
    public void getSportsName() {
        Cursor cursor = db.getSportsName();

        sports_name_array = new ArrayList<>();
        sports_id_array = new ArrayList<>();

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String sport_Name = cursor.getString(cursor
                            .getColumnIndex("sport_name"));
                    String sport_id = cursor.getString(cursor
                            .getColumnIndex("sport_id"));

                    sports_name_array.add(sport_Name);
                    sports_id_array.add(sport_id);

                } while (cursor.moveToNext());
            }

            //Sending sports name with sports id to the spinner
            setVenueSportsName(sports_name_array, sports_id_array);
        }
    }

    /**
     * Keeping sports name inside spinner
     */
    public void setVenueSportsName(ArrayList<String> sports_name_array, ArrayList<String> sports_id_array) {

        //Getting venue name & id as a array to display inside the spinner
        String[] spinnerArray = new String[sports_id_array.size()];
        map_sports_name = new HashMap<String, String>();

        for (int i = 0; i < sports_id_array.size(); i++) {
            map_sports_name.put(sports_name_array.get(i), sports_id_array.get(i));
            spinnerArray[i] = sports_name_array.get(i);
        }

        //Sending sports name as a String[]
        displaySpinnerInfo(spinnerArray);
    }

    /**
     * Displaying name inside spinner
     * @param spinnerArray
     */
    public void displaySpinnerInfo(String[] spinnerArray) {
       /* ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, spinnerArray);*/
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_layout, spinnerArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sports_names.setAdapter(dataAdapter);
        int pos = getCategoryPos(sports_name_array, userSport);
        spinner_sports_names.setSelection(pos);
        isSportSelected = true;
        Constants.SPORTS_NAME = spinnerArray[pos];
    }

    /**
     * Function to get Venue name from server
     */
    public void getVenuesName() {
        //Handling the loader state
        LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String venue_spinner_id = "", venue_spinner_name = "";
                boolean isStausSuccess = true;

                try {
                    JSONArray venue_listing_array = new JSONArray(responseData);
                    venue_spinner_name_array = new ArrayList<String>();
                    venue_spinner_id_array = new ArrayList<String>();

                    for (int i = 0; i < venue_listing_array.length(); i++) {
                        JSONObject venue_listing_court_object = venue_listing_array.getJSONObject(i);

                        venue_spinner_id = venue_listing_court_object.getString("id");
                        venue_spinner_name = venue_listing_court_object.getString("name");

                        venue_spinner_name_array.add(venue_spinner_name);
                        venue_spinner_id_array.add(venue_spinner_id);

                    }

                } catch (Exception ex) {
                    isStausSuccess = false;
                    //snackBar.setSnackBarMessage("Network error ! please try after sometime");
                    ex.printStackTrace();
                }

                if(isStausSuccess) {
                    //Handling the loader state
                    LoaderUtility.handleLoader(getActivity(), false);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //Sending venue names with ids to spinner
                            //setVenueNames(venue_spinner_name_array, venue_spinner_id_array);

                        }
                    });
                }
                else{
                    snackBar.setSnackBarMessage("Network error ! please try after sometime");
                }

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "venue_lists").newBuilder();
        //urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, user_name);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }
    

    /**
     * Keeping venue names with ids inside spinner
     */
    public static void setVenueNames(String address) {
        ArrayList<String> venueArray = new ArrayList<>();
        venueArray.add(current_city);

        //Displaying name inside spinner
        displayAppletonSpinnerInfo(venueArray);
    }

    /**
     * Displaying name inside spinner
     */
    public static void displayAppletonSpinnerInfo(ArrayList<String> spinnerArray) {

        try{
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(fragmentContext, R.layout.spinner_layout_location, spinnerArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_appleton.setAdapter(dataAdapter);
            spinner_appleton.setSelection(0);
            //spinner_appleton.setClickable(false);

           // Toast.makeText(fragmentContext,"venue Displayed", Toast.LENGTH_LONG).show();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Initializing all views belongs
     * to this layout
     */
    public void initReferences(View view) {
        card_view_start_game = (CardView) view.findViewById(R.id.card_view_start_game);
        card_view_join_game = (CardView) view.findViewById(R.id.card_view_join_game);
        card_view_join_game.setClickable(false);
        card_view_join_game.setEnabled(false);
        spinner_sports_names = (Spinner) view.findViewById(R.id.spinner_sports_names);
        spinner_appleton = (Spinner) view.findViewById(R.id.spinner_appleton);
        toggle_button = (ImageView) view.findViewById(R.id.toggle_button);
        start_layout = (RelativeLayout) view.findViewById(R.id.start_layout);
        join_layout = (RelativeLayout) view.findViewById(R.id.join_layout);
        tv_no_games_found  = (TextView) view.findViewById(R.id.tv_no_games_found);
        start_game_icon = (ImageView) view.findViewById(R.id.start_game_icon);
        join_game_icon = (ImageView) view.findViewById(R.id.join_game_icon);
        lay_placeholder = (RelativeLayout) view.findViewById(R.id.lay_placeholder);
        toggle_button_layout = (RelativeLayout) view.findViewById(R.id.toggle_button_layout);
        swipe_refresh_layout_event = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout_event);

        //Change the Swipe layout color
        swipe_refresh_layout_event.setColorSchemeResources(R.color.orange,
                R.color.orange,
                R.color.orange);

        db = new DatabaseAdapter(getActivity());
        snackBar = new SnackBar(getActivity());

        tv_no_games_found.setText("No games available to join. Proceed with"+ " \"Start a game\"" );

    }

    /**
     * Function to get the address
     * from latitude and longitude
     */
    public static void getAddress(){
        try{
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(fragmentContext, Locale.getDefault());
            addresses = geocoder.getFromLocation(currLocLatitude, currLocLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            current_city = addresses.get(0).getLocality();
            Log.d("Current City",current_city);
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            //Sending Address to spinner
            setVenueNames(current_city);
            // Toast.makeText(fragmentContext,"venue called", Toast.LENGTH_LONG).show();

        }catch(Exception e){
            e.printStackTrace();
        }

    }


    /**
     * Function to get the address
     * from latitude and longitude
     */
    public static void getCurrentAddress(){
        try{
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(fragmentContext, Locale.getDefault());
            addresses = geocoder.getFromLocation(currLocLatitude, currLocLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            current_city = addresses.get(0).getLocality();
            Log.d("Current City",current_city);
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            //Sending Address to spinner
            setVenueNames(current_city);
        }catch(Exception e){
            e.printStackTrace();
        }

    }


    /**
     * Function Responsible for proving the
     * current location latitude and longitude
     */
    public static void getCurrentLocation(){
        gpsTracer = new GPSTracker(fragmentContext);
        if(gpsTracer.canGetLocation()) {
            currLocLatitude = gpsTracer.getLatitude();
            currLocLongitude = gpsTracer.getLongitude();
        }
        getCurrentAddress();
        //Getting current location address
        getCurrentLocationAddresses();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );

        result.setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getActivity(), ""+connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // NO need to show the dialog;
                getCurrentLocation();
                if(!Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND &&
                        !Constants.MY_SPORTS_CLICKED) {
                    getJoinGameAvailibity();
                }
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }




    /**
     * Function to initialize the Google API clent
     */
    public void initializeGoogleClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
    }


    /**
     * Function to get the permissions statud for Android 6.0 Onwards
     *
     * @return
     */
    private boolean checkLocationPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function to request Permission
     *
     * @return
     */
    private void requestPermissionAccessLocation() {
        final String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION};
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("To Display events and games around you. You need to give permission to access your location");
            builder.setTitle("Access Location");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(getActivity(), permissions, PERMISSION_REQUEST_CODE);
                }
            });
            builder.show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }


    /**
     * Handling the permission Status
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Checking for the location Permissions
                    if (!checkLocationPermission()) {
                    }  else {
                        //Initializing the Google API client
                        GpsConnectionHelper gpsHelper =
                                new GpsConnectionHelper(getActivity());
                    }
                } else {
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        //Here Checking, is user changed the currenmt selected location
        // and Getting the current city and setting it on the view
        if(AppConstants.isLocationPicked){
            setVenueNames(current_city);
            LocationPickerManager.getCurrentCity(getActivity());
            AppConstants.isLocationPicked = false;
        }

        //Here Checking, Is there any game or event created using the selected
        //sport and location and calling the join game availability API accrodingly
        if(Constants.isEventCreated){
            //Making the event created variable false
            Constants.isEventCreated = false;

            //Calling the Join game availability API to
            // make the JOIN game button enable/Disable
            getJoinGameAvailibity();
         }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void openAutocompleteActivity() {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(getActivity());
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Log.e("Google", message);
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Function Responsible for the API call for the game Availibility
     */
    public void getJoinGameAvailibity() {

        //Handling the loader state
        LoaderUtility.handleLoader(getActivity(), true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildGameVerifyApiRequestBody();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(getActivity(), false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    isGameAvailable = responseObject.getBoolean("is_game_available");
                    chat_badge_count = responseObject.getInt("chat_bc");
                    if(responseObject.has("badge_count") && !responseObject.isNull("badge_count")) {
                        badge_count = responseObject.getInt("badge_count");
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(getActivity(), false);

                        //Handling the API response
                        if(isGameAvailable){
                            //Handling the View visiblity and clickability
                            TabFragment.displayChatBadgeCount(chat_badge_count);
                            handleViewVisibility(true);
                        }else{
                            //Handling the View visiblity and clickability
                            TabFragment.displayChatBadgeCount(chat_badge_count);
                            //Handling the View visiblity and clickability
                            handleViewVisibility(false);
                        }


                        if(badge_count != 0) {
                            TabFragment.notification_count.setVisibility(View.VISIBLE);
                            Constants.PUSH_NOTIFICATION_BADGE_COUNT = badge_count;
                            TabFragment.notification_count.setText(String.valueOf(Constants.PUSH_NOTIFICATION_BADGE_COUNT));
                        }
                        else{
                            TabFragment.notification_count.setVisibility(View.INVISIBLE);
                        }
                    }
                });
            }
        });
    }


    /**
     * Function responsible for handling the
     * button visiblity and click able
     *
     * @param status True/False
     */
    public void handleViewVisibility(boolean status){

        if(status){
            //Setting the back grond image of the image view
            join_game_icon.setBackgroundResource(R.mipmap.img_join_game_updated);
            //Disabling the card view
            card_view_join_game.setEnabled(status);
            //Disabling the card view for click events
            card_view_join_game.setClickable(status);
            join_game_icon.setClickable(status);
            join_game_icon.setEnabled(status);
            join_layout.setFocusable(status);
            join_layout.setClickable(status);
            join_layout.setEnabled(status);
            lay_placeholder.setVisibility(View.GONE);
        }else{
            //Setting the back grond image of the image view
            join_game_icon.setBackgroundResource(R.drawable.ic_join_game);
            //Disabling the card view
            card_view_join_game.setEnabled(status);
            //Disabling the card view for click events
            card_view_join_game.setClickable(status);
            join_game_icon.setClickable(status);
            join_game_icon.setEnabled(status);
            join_layout.setFocusable(status);
            join_layout.setClickable(status);
            join_layout.setEnabled(status);
            lay_placeholder.setVisibility(View.VISIBLE);
        }


    }



    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildGameVerifyApiRequestBody() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "verify_game_created").newBuilder();
        urlBuilder.addQueryParameter("location", this.currLocLatitude+","+ this.currLocLongitude);
        urlBuilder.addQueryParameter("current_city", current_city);
        urlBuilder.addQueryParameter("city", current_location_city);
        urlBuilder.addQueryParameter("state", current_location_state);
        urlBuilder.addQueryParameter("country", current_location_country);
        urlBuilder.addQueryParameter("sport_name", Constants.SPORTS_NAME);
        urlBuilder.addQueryParameter("current_time", ApplicationUtility.getDeviceCurrentTime());
        urlBuilder.addQueryParameter("date", ApplicationUtility.getDeviceCurrentDate());
        urlBuilder.addQueryParameter("email", user_email);
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("user_profile_image", user_profile_image);
        urlBuilder.addQueryParameter("user_name", user_name);
        urlBuilder.addQueryParameter("device_type", Constants.DEVICE_TYPE);
        urlBuilder.addQueryParameter("timezone",ApplicationUtility.getCurrentTimeZone());

        if(firebase_token_preferences.contains("Fcm_id")) {
            String Fcm_registration_id = firebase_token_preferences.getString("Fcm_id","");
            urlBuilder.addQueryParameter("firebase_registration_id", Fcm_registration_id);
        }

        //Sending the signedup_at at the time of registration,
        // to keep track of the user notifications
        if(NormalRegistrationStepTwo.signedup_at != null){
            urlBuilder.addQueryParameter("signedup_at", NormalRegistrationStepTwo.signedup_at);
            NormalRegistrationStepTwo.signedup_at = null;
        }
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * getting the position os a item in the arrayalist
     * @param SportsArray
     * @param category
     * @return
     */
    public static int getCategoryPos(ArrayList<String> SportsArray, String category) {
        return SportsArray.indexOf(category);
    }

    public static void getCurrentLocationAddresses() {
        try{
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(fragmentContext, Locale.getDefault());
            addresses = geocoder.getFromLocation(gpsTracer.getLatitude(), gpsTracer.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            current_location_city = addresses.get(0).getLocality();
            current_location_state = addresses.get(0).getAdminArea();
            current_location_country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        }catch(Exception e){
            e.printStackTrace();
        }
    }



}
