package com.andolasoft.squadz.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.AddFriendToChat;
import com.andolasoft.squadz.activities.ChatActivity;
import com.andolasoft.squadz.activities.NavigationDrawerActivity;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.ChatConversationModel;
import com.andolasoft.squadz.models.Model_Scheduler;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.FirebaseInstance;
import com.andolasoft.squadz.utils.ListUtils;
import com.andolasoft.squadz.views.adapters.TeammateListAdapter;
import com.firebase.client.ChildEventListener;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.drakeet.materialdialog.MaterialDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatFragmnet extends Fragment {

    private ImageView toggle_button;
    private View rootView;
    FloatingActionButton fbb_addplayertochat;
    public static ListView teammate_view;
    public static ArrayList<ChatConversationModel> conversationModel = new ArrayList<>();
    ProgressDialog dialog;
    MaterialDialog mMaterialDialog;
    SharedPreferences pref;
    String userId;
    DatabaseAdapter db;

    public ChatFragmnet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_blank, container, false);

        /**
         * Initialize all views belongs to this layoutINITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
         */
        initReferences(rootView);

        db = new DatabaseAdapter(getActivity());

        /**
         * Added click events on the views
         */
        setOnClickEvents();

       // fetchValue();

        return rootView;


    }

    /**
     * Getting all the friends from database and setting in the list view
     */
    public int getSelectedFriendFromDB() {
        Cursor cursor = db.getcurrentsportfrienlistRoster("All", null);
        cursor.moveToFirst();
        return cursor.getCount();
    }



    /**
     * Added click events on the views
     */
    public void setOnClickEvents() {

        toggle_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(
                        ((NavigationDrawerActivity) getActivity()).openDrawerRunnable(true), 200);
            }
        });

        fbb_addplayertochat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getSelectedFriendFromDB() > 0){
                    Intent intent = new Intent(getActivity(), AddFriendToChat.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getActivity(), "No Friends Found", Toast.LENGTH_SHORT).show();
                }

            }
        });

        teammate_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                AppConstants.CHAT_USER_NAME = ChatFragmnet.conversationModel.get(pos).getChatUserName();
                AppConstants.CHAT_CHANNEL_ID = ChatFragmnet.conversationModel.get(pos).getChannelId();
                AppConstants.CHAT_USER_IMAGE_URL = ChatFragmnet.conversationModel.get(pos).getImageUrl();
                AppConstants.CHAT_USER_ID = ChatFragmnet.conversationModel.get(pos).getRecieverID();

                Intent intent = new Intent(getActivity(), ChatActivity.class);
                startActivity(intent);
            }
        });
    }


    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayDialog(final String ChannelId, final String chatUserId){
        mMaterialDialog = new MaterialDialog(getActivity());
        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.layout_update_card_status,
                        null);
        TextView tv_card_numner = (TextView) view.findViewById(R.id.tv_card_number);
        TextView tv_card_option_primary_card = (TextView) view.findViewById(R.id.tv_make_card_primary);
        tv_card_option_primary_card.setVisibility(View.GONE);
        TextView tv_card_option_delete_card = (TextView) view.findViewById(R.id.tv_delete_card);
        tv_card_option_delete_card.setText("Are you sure want to delete this conversation?");
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_card_numner.setText("Delete Chat");
        tv_card_option_primary_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();

            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                deleteCOnversation(ChannelId, chatUserId);
            }
        });

        tv_card_option_delete_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }


    public void deleteCOnversation(final String channelId, final String chatUserId){
        //Initializing the firebase instance
        Firebase.setAndroidContext(getActivity());
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Initializing the child
        postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(userId).child(channelId);
        //Removing the channel from the logged in user id
        postRef.removeValue();

        //Initializing the firebase instance
        Firebase postRef_reciever = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Initializing the child
        postRef_reciever = postRef.child(AppConstants.CONVERSATION_NODE).child(chatUserId).child(channelId);
        //Removing the channel from the chat user id
        postRef_reciever.removeValue();

    }



    /**
     * @param rootView
     */
    public void initReferences(View rootView) {
        toggle_button = (ImageView) rootView.findViewById(R.id.toggle_button_blank_fargment);
        fbb_addplayertochat = (FloatingActionButton) rootView.findViewById(R.id.fab);
        teammate_view = (ListView) rootView.findViewById(R.id.teammate_view);

        pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userId = pref.getString("loginuser_id", null);
    }

    public void fetchValue(){
        Firebase.setAndroidContext(getActivity());
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child("conversation");
        postRef.child("individual").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
               // for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                Log.d("TAG", dataSnapshot.toString());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }



    /**
     * Start global listener for all Posts.
     */
   /* public static void startListeners() {
        Firebase postRef = FirebaseInstance.getInstance().getFirebaseInstance();
        postRef.child("posts").addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(com.firebase.client.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(com.firebase.client.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(com.firebase.client.DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(com.firebase.client.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildName) {
                *//*final String postId = dataSnapshot.getKey();
                final Post post = dataSnapshot.getValue(Post.class);

                // Listen for changes in the number of stars and update starCount
                addStarsChangedListener(post, postId);

                // Listen for new stars on the post, notify users on changes
                addNewStarsListener(dataSnapshot.getRef(), post);*//*
            }

            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildName) {}

            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildName) {}

            public void onCancelled(DatabaseError databaseError) {
                System.out.println("startListeners: unable to attach listener to posts");
                System.out.println("startListeners: " + databaseError.getMessage());
            }
        });
    }*/



}
