package com.andolasoft.squadz.calenderHelper;

/**
 * Created by SpNayak on 1/23/2017.
 */

import java.util.Date;

/**
 * Created by ismail.khan2 on 5/23/2016.
 */
public interface MonthViewClickListeners {
    void dateClicked(Date dateClicked);
}
