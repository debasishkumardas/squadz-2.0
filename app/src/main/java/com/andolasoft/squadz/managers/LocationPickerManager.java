package com.andolasoft.squadz.managers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.widget.Toast;

import com.andolasoft.squadz.fragments.HomeFragment;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.util.List;
import java.util.Locale;

/**
 * Created by Debasish Kumar Das on 1/17/2017.
 *
 * This class is designed to display the google location picker
 * @author Debasish Kumar Das
 */
public class LocationPickerManager {
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    Activity mActivity;

    public LocationPickerManager(Activity activity){
        this.mActivity = activity;
        openAutocompleteActivity();
    }

    private void openAutocompleteActivity() {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.

            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(this.mActivity);
            this.mActivity.startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
            //LocationPicker.this.finish();
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this.mActivity, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Log.e("Google", message);
            Toast.makeText(this.mActivity, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Function to get the current city
     * @param activity
     */
    public static void getCurrentCity(Context activity){
        try{
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(activity, Locale.getDefault());
            addresses = geocoder.getFromLocation(HomeFragment.currLocLatitude, HomeFragment.currLocLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            HomeFragment.current_city = addresses.get(0).getLocality();
            Log.d("Current City", HomeFragment.current_city);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Function to get the current city
     * @param activity
     */
    public static String getCity(Context activity, double latitude, double longitude){
        String city = null;
        try{
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(activity, Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            city = addresses.get(0).getLocality();
        }catch(Exception e){
            e.printStackTrace();
        }
        return city;
    }
}
