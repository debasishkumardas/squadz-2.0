package com.andolasoft.squadz.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.andolasoft.squadz.activities.SigninActivity;
import com.andolasoft.squadz.utils.AppConstants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by SpNayak on 1/6/2017.
 */

public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";
    DatabaseAdapter db;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        AppConstants.FCM_ID = refreshedToken;
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToDataBase(refreshedToken);
    }

    /**
     * Storing the data in the Local Database
     * @param token The new token.
     */
    private void sendRegistrationToDataBase(String token) {
        // Add custom implementation, as needed.
        db  = new DatabaseAdapter(FirebaseIDService.this);
        String device_token = SigninActivity.getDeviceToken(FirebaseIDService.this);
        db.insertFcmDetails(device_token,token);
        storeFcmId(token);
    }

    /**
     * Storing the Fcm_id in the temp storage
     * @param token
     */
    public void storeFcmId(String token){
        SharedPreferences firebase_token_preferences = getSharedPreferences("Firebase_Token", Context.MODE_PRIVATE);
        SharedPreferences.Editor firebase_editor = firebase_token_preferences.edit();
        firebase_editor.putString("Fcm_id", token);
        firebase_editor.apply();
    }
}
