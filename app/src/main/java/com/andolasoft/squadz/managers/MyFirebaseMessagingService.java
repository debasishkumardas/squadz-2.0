package com.andolasoft.squadz.managers;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.ChatActivity;
import com.andolasoft.squadz.activities.EventDetail;
import com.andolasoft.squadz.activities.NavigationDrawerActivity;
import com.andolasoft.squadz.activities.SplashScreen;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonArray;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static android.R.attr.data;

/**
 * Created by SpNayak on 1/6/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    String messagetype = null;
    String chat_type = null;
    String channelId = null;
    String sender_Id = null;
    String group_id = null ;
    String chatUserName = null;
    String chatBc = null;
    String game_id = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.

        //Getting the Data from the Payload
        Map<String, String> data = remoteMessage.getData();
        messagetype = data.get("key");

        if(messagetype != null &&
                messagetype.equalsIgnoreCase("ChatMessage")){
            chat_type = data.get("chat_type");
            channelId = data.get("channel_id");
            sender_Id = data.get("sender_id");
            group_id = data.get("group_id");
            chatBc = data.get("chat_bc");
            String message_title = remoteMessage.getNotification().getTitle();
            chatUserName = message_title.replace(" sent an message:","");

            //Message and Id handle for chat messages
            if(chat_type != null &&
                    chat_type.equalsIgnoreCase("group")){
                AppConstants.isGropuChatMessage = true;
                AppConstants.CHAT_CHANNEL_ID = channelId;
                AppConstants.CHAT_USER_IMAGE_URL = null;
                AppConstants.CHAT_USER_ID = group_id;
                AppConstants.CHAT_TYPE = chat_type;
                AppConstants.CHAT_CHANNEL_ID_DUPLICATE = "Recieved";
            }else{
                AppConstants.isGropuChatMessage = false;
                AppConstants.CHAT_USER_NAME = chatUserName;
                AppConstants.CHAT_CHANNEL_ID = channelId;
                AppConstants.CHAT_USER_IMAGE_URL = null;
                AppConstants.CHAT_USER_ID = sender_Id;
                AppConstants.CHAT_TYPE = chat_type;
                AppConstants.CHAT_CHANNEL_ID_DUPLICATE = "Recieved";
            }
            updateNotificationActivity(MyFirebaseMessagingService.this, message_title, "ChatMessage");
        }
        //Getting the game id for all the game and event related push notifications
        else if (messagetype != null &&
                (messagetype.equalsIgnoreCase("NearbyGame")
                        || messagetype.equalsIgnoreCase("JoinGame")
                        || messagetype.equalsIgnoreCase("GameInvitation")
                        || messagetype.equalsIgnoreCase("EditGame")
                        || messagetype.equalsIgnoreCase("LeaveGame")
                        || messagetype.equalsIgnoreCase("GameTransaction"))) {
            game_id = data.get("object_id");
        }

        //Getting the body and title from the push notifications
        String message_body = remoteMessage.getNotification().getBody();
        String message_title = remoteMessage.getNotification().getTitle();

        if(messagetype != null &&
                messagetype.equalsIgnoreCase("ChatMessage") &&
                !AppConstants.isUserinChatBoard){
            //Displaying the push notification
            sendNotification(message_title, "Squadz");
        } else if(messagetype != null
                && (messagetype.equalsIgnoreCase("NearbyGame")
                || messagetype.equalsIgnoreCase("InviteFriend")
                || messagetype.equalsIgnoreCase("EditGame")
                || messagetype.equalsIgnoreCase("GameTransaction"))) {

            //Displaying the push notification
            sendNotification(message_body, message_title);

            //Refreshing the badge count after getting the message through broadcast reciever
            updateNotificationActivity(MyFirebaseMessagingService.this, message_title, null);

        }else if((messagetype != null &&
                messagetype.equalsIgnoreCase("GameInvitation"))){

            //Displaying the push notification
            sendNotification(message_body, message_title);

            //Refreshing the badge count after getting the message through broadcast reciever
            updateNotificationActivity(MyFirebaseMessagingService.this, message_title, null);

        }else if(messagetype != null &&
                messagetype.equalsIgnoreCase("JoinGame")){

            //Displaying the push notification
            sendNotification(message_body, message_title);

            //Refreshing the badge count after getting the message through broadcast reciever
            updateNotificationActivity(MyFirebaseMessagingService.this, message_title, null);

        }else if(messagetype != null &&
                messagetype.equalsIgnoreCase("LeaveGame")){

            //Displaying the push notification
            sendNotification(message_body, message_title);

            //Refreshing the badge count after getting the message through broadcast reciever
            updateNotificationActivity(MyFirebaseMessagingService.this, message_title, null);

        } else if(messagetype != null &&
                messagetype.equalsIgnoreCase("InviteFriend")){

            //Displaying the push notification
            sendNotification(message_body, message_title);

            //Refreshing the badge count after getting the message through broadcast reciever
            updateNotificationActivity(MyFirebaseMessagingService.this, message_title, null);

        } else if(messagetype != null &&
                messagetype.equalsIgnoreCase("AcceptFriend")){

            //Displaying the push notification
            sendNotification(message_body, message_title);

            //Refreshing the badge count after getting the message through broadcast reciever
            updateNotificationActivity(MyFirebaseMessagingService.this, message_title, null);
        }
        else if(messagetype != null &&
                messagetype.equalsIgnoreCase("CancelGame")) {

            //Displaying the push notification
            sendNotification(message_body, message_title);

            //Refreshing the badge count after getting the message through broadcast reciever
            updateNotificationActivity(MyFirebaseMessagingService.this, message_title, null);
        }
    }

    private void sendNotification(String messageBody, String messageTitle) {
        Intent intent = null;
        if(messagetype != null && messagetype.equalsIgnoreCase("ChatMessage")){
            intent = new Intent(this, ChatActivity.class);
        } else if (messagetype != null
                && (messagetype.equalsIgnoreCase("NearbyGame")
                || messagetype.equalsIgnoreCase("JoinGame")
                || messagetype.equalsIgnoreCase("GameInvitation")
                || messagetype.equalsIgnoreCase("EditGame")
                || messagetype.equalsIgnoreCase("LeaveGame")
                || messagetype.equalsIgnoreCase("GameTransaction"))) {
            Constants.PUSH_NOTIFICATION_GAME_ID = game_id;
            Constants.FROM_PUSH_NOTIFICATION = true;
            intent = new Intent(this, EventDetail.class);
        }
        /**Redirect the user to Teammate page to see the newly accepted user*/
        else if(messagetype != null &&
                messagetype.equalsIgnoreCase("AcceptFriend")
               /* && messageTitle.equalsIgnoreCase("Teammate Request Accepted")*/){
            Constants.FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY = true;
            intent = new Intent(this, SplashScreen.class);
        }
        else if(messagetype != null &&
                messagetype.equalsIgnoreCase("InviteFriend")){
            Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND = true;
            intent = new Intent(this, SplashScreen.class);
        } else if(messagetype != null &&
                messagetype.equalsIgnoreCase("CancelGame")) {
            intent = new Intent(this, SplashScreen.class);
        }

        /*if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }*/

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_squadz_large_logo);
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new
                NotificationCompat.Builder(this)
                .setSmallIcon(getNotificationIcon(),80)
                .setLargeIcon(bitmap)
                .setAutoCancel(true)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(setNotification())
                .setVibrate(new long[] { 1000 })
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        //Create Push Notification
        notificationManager.notify(0, notificationBuilder.build());

    }

    /**
     * Function responsible for displaying the notification
     * icon accroding to the device OS version
     *
     * @return icon
     */
    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_squadz_large_logo : R.mipmap.ic_squadz_large_logo;
    }

    /**
     * Setting the Notification tone
     * @return the tone
     */
    public Uri setNotification(){
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        return alarmSound;
    }


    /**
     * Checking the application display state as background/foreground
     * @param context Context of the activity
     * @return The application state
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // This function will create an intent. This intent must take as parameter
    // the "unique_name" that you registered your activity with
    static void updateNotificationActivity(Context context,
                                           String message, String type) {

        Intent intent = new Intent("unique_name");
        //put whatever data you want to send, if any
        intent.putExtra("message", message);
        intent.putExtra("TYPE", type);
        //send broadcast
        context.sendBroadcast(intent);
    }
}
