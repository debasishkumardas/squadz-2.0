package com.andolasoft.squadz.managers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.andolasoft.squadz.models.FirebaseGroupChatParticipantModel;
import com.andolasoft.squadz.models.Profile;


public class DatabaseAdapter {

	private static final String TAG = "DataBaseAdapter";

	private static final int DATABASE_VERSION = 17;
	public static final String DATABASE_NAME = "SquardDatabaseHelp.db";
	public static final String USER = "USER";
	public static final String FRIEND = "FRIEND";
	public static final String SPORT = "SPORT";
	public static final String MYTEAM = "MYTEAM";
	public static final String FCM = "FCM";
	public static final String TEAMPLAYER = "TEAMPLAYER";
	public static final String TEAMMATE = "TEAMMATE";
	public static final String LOCALGAME_LIST = "LOCALGAME_LIST";
	public static final String ACTIVITY_FEEDS = "ACTIVITY_FEEDS";
	public static final String COMMENTS = "COMMENTS";
	public static final String NEXTSCHEDUEDGAME = "NEXTSCHEDUEDGAME";
	public static final String ALLSCHEDUEDGAME = "ALLSCHEDUEDGAME";
	public static final String PENDINGREQUEST = "PENDINGREQUEST";
	public static final String GAMEPLAYER = "GAMEPLAYER";
	public static final String GAME_PLAYERS = "GAME_PLAYERS";
	public static final String GAME_DETAIL = "GAME_DETAIL";
	public static final String GAMEHISTORY = "GAMEHISTORY";
	public static final String GAMERADIUS = "GAMERADIUS";
	public static final String PUSHNOTIFICATIONSTATUS = "PUSHNOTIFICATIONSTATUS";
	public static final String LOCATION_DETAILS = "LOCATION_DETAILS";
	public static final String BLOCK_FRIEND = "BLOCK_FRIEND";
	public static final String INVITATION_INFO = "INVITATION_INFO";
	public static final String USERPROFILE = "USERPROFILE";
	public static final String COUNTS = "COUNTS";
	public static final String SKILL_LEVEL = "SKILL_LEVEL";
	public static final String PENDINGFRIEND_INVITES = "PENDINGFRIEND_INVITES";
	public static final String CHECKIN_GAME = "CHECKIN_GAME";
	public static final String GCM_ID = "GCM_ID";
	public static final String NEARBY_GAMES = "NEARBY_GAMES";
	public static final String RECURRING_GAMES = "RECURRING_GAMES";
	public static final String CANCELL_GAME_CHECKIN = "CANCELL_GAME_CHECKIN";
	public static final String CARD_DETAILS = "CARD_DETAILS";
    public static final String CURRENT_USER = "CURRENT_USER";
	public static final String user_id = "friend_id";
	public static final String user_name = "friend_name";
	public static final String sport = "sport";
	public static final String phone = "phone";
	public static final String created_at = "created_at";
	public static final String updated_at = "updated_at";
	public static final String PUSH_USERNAME = "user_name";
	public static final String MESSAGE_COUNT = "message_count";
	public static final String USER_LOGIN_STATUS = "user_login_status";
	public static final String USER_CONVERSATION = "user_conversation";
	public static final String Group_Chat_Team_Status = "group_chat_team_status";
	public static final String CHAT_UNREAD_MESSAGES = "chat_unread_messages";
	public static final String GROUP_CHAT_TEAM_DETAILS = "chat_unread_messages";
	public static final String NONFRIEND_CHAT_DETAILS = "nonFriend_chat_details";
	public static final String CHAT_INFO = "chat_info";
	public static final String GAMES = "games";
    public static final String CREDENTIALS = "credentials";
	public static final String LISTS_COLUMN_ID = "_id";
	public static final String USER_RATING = "USER_RATING";
	private final Context context;
	private DatabaseHelper dBHelper;
	private SQLiteDatabase db;
	static Boolean table_status = false;

	public DatabaseAdapter(final Context ctx) {
		this.context = ctx;
		dBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

		private static String DB_PATH;
		private final Context myContext;

		@SuppressLint("SdCardPath")
		DatabaseHelper(final Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			this.myContext = context;

			try {
				DB_PATH = "/data/data/" + context.getPackageName()
						+ "/databases";
				Log.i("DataBaseAdapter", "DATABASE_NAME " + DATABASE_NAME);

				createDataBase();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void createDataBase() throws IOException {

			boolean dbExist = checkDataBase();

			if (dbExist) {
			} else {
				this.getReadableDatabase();

			}
		}

		private boolean checkDataBase() {

			SQLiteDatabase checkDB = null;

			try {
				String myPath = DB_PATH + "/" + DATABASE_NAME;
				checkDB = SQLiteDatabase.openDatabase(myPath, null,
						SQLiteDatabase.OPEN_READONLY);
			} catch (SQLiteException e) {

			}

			if (checkDB != null) {
				checkDB.close();
			}

			return checkDB != null ? true : false;
		}

		/*
		 * Creating Tales in Database
		 */
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("create table IF NOT EXISTS SPORT"
					+ "(_id integer ,sport_name text ,sport_id text primary key,created_at text,updated_at text)");

			db.execSQL("create table IF NOT EXISTS FCM"
					+ "(device_token text primary key, fcm_id text)");

            db.execSQL("create table IF NOT EXISTS CREDENTIALS"
                    + "(aws_access_key text, aws_secret_key text, aws_bucket text, google_api_key text, google_places_api_key text, facebook_id text, stripe_secret_key text, stripe_publishable_key text)");

			db.execSQL("create table IF NOT EXISTS GAMES"
					+ "(_id integer, user_id text, sport_id text, venue_name text, venue_id text, game_name text, venue_price text, " +
					"players_count text, date text, time text, players_amount text, game_visibility text, skill_level text, notes text)");

            db.execSQL("create table IF NOT EXISTS CARD_DETAILS"
                    + "(brand text, country text, funding text, lastfour text, customer_id text, card_id text primary key, cardHolderName text, " +
					"exp_month text, exp_year text, default_card INTEGER)");

			db.execSQL("create table IF NOT EXISTS FRIEND"
					+ "(user_id text primary key, username text, firstname text, lastname text, image text, sport text, phonenumber text, status text, reward text)");

            db.execSQL("create table IF NOT EXISTS MYTEAM"
                    + "(team_id text primary key, team_name text, team_sport_id text, team_custom_sport_name text, team_creator_user_id text, team_creator_user_name text, team_creator_profile_image text, team_created_at text, team_updated_at text)");

            db.execSQL("create table IF NOT EXISTS TEAMPLAYER"
                    + "(team_id text, player_id text, player_user_name text, player_first_name text, player_last_name text, player_profile_image text)");

			db.execSQL("create table CURRENT_USER"
					+ "(user_id text primary key,  username text, phone_number text, first_name text, last_name text,email text," +
					"birth_date text, image text, about_me text, sport text, skill_level text, secondary_sports text,rewards text," +
					"street_name text, locality text, province_abbreviation text, postal_code text, country text)");

			db.execSQL("create table USER_RATING"
					+ "(name text, list_id text, event_id text, object_type text, user_id text, date text, game_end_time text)");

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL("DROP TABLE IF EXISTS " + CREDENTIALS);
			db.execSQL("DROP TABLE IF EXISTS " + GAMES);
			db.execSQL("DROP TABLE IF EXISTS " + CARD_DETAILS);
            db.execSQL("DROP TABLE IF EXISTS " + FRIEND);
            db.execSQL("DROP TABLE IF EXISTS " + MYTEAM);
            db.execSQL("DROP TABLE IF EXISTS " + TEAMPLAYER);
            db.execSQL("DROP TABLE IF EXISTS " + CURRENT_USER);
			db.execSQL("DROP TABLE IF EXISTS " + USER_RATING);
			onCreate(db);
		}

	}

	/*
	 * ---opens the database---
	 */
	public DatabaseAdapter open() throws SQLException {
		try {
			String myPath = "/data/data/" + context.getPackageName()
					+ "/databases" + "/" + DATABASE_NAME;
			db = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	/*
	 * ---closes the database---
	 */
	public void close() {
		db.close();
	}

	/*
	 * INSERTING DATA INTO USER_LOGIn_STATUS TABLE
	 */
	public boolean insertuserloginStatus(String login_status, int version_code) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_login_status", login_status);
		contentValues.put("version_code", version_code);
		db.insert("USER_LOGIN_STATUS", null, contentValues);
		return true;
	}


	/**
	 * Keeping Game / Event info for displaying Rating pop up dialog
	 * @param name
	 * @param list_id
	 * @param event_id
	 * @param object_type
	 * @param user_id
     * @return
     */
	public boolean insertGameDetailsForRating(String name, String list_id,
											  String event_id, String object_type,
											  String user_id, String date,String game_end_time) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("name", name);
		contentValues.put("list_id", list_id);
		contentValues.put("event_id", event_id);
		contentValues.put("object_type", object_type);
		contentValues.put("user_id", user_id);
		contentValues.put("date", date);
		contentValues.put("game_end_time", game_end_time);
		db.insert("USER_RATING", null, contentValues);
		return true;
	}


	/**
	 * Inseting FCM registration Details
	 * @param deviceToken Device id
	 * @param fcmId Fcm registered Id
     * @return
     */
	public boolean insertFcmDetails(String deviceToken, String fcmId) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("device_token", deviceToken);
		contentValues.put("fcm_id", fcmId);
		db.insert("FCM", null, contentValues);
		return true;
	}


    /**
     *
     * @param user_id
     * @param username
     * @param phone_number
     * @param first_name
     * @param last_name
     * @param email
     * @param birth_date
     * @param image
     * @param about_me
     * @param sport
     * @param skill_level
     * @param secondary_sports
     * @param rewards
     * @param street_name
     * @param locality
     * @param province_abbreviation
     * @param postal_code
     * @param country
     * @return
     */
    public boolean insertCurrentUserDetails(String user_id, String  username, String phone_number, String first_name, String last_name,String  email,
                                            String  birth_date, String image, String  about_me, String  sport, String  skill_level, String  secondary_sports,String  rewards,
                                            String  street_name , String locality, String province_abbreviation , String postal_code , String country){
        SQLiteDatabase db = this.dBHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("user_id", user_id);
        contentValues.put("username", username);
        contentValues.put("phone_number", phone_number);
        contentValues.put("first_name", first_name);
        contentValues.put("last_name", last_name);
        contentValues.put("email", email);
        contentValues.put("birth_date", birth_date);
        contentValues.put("image", image);
        contentValues.put("about_me", about_me);
        contentValues.put("sport", sport);
        contentValues.put("skill_level", skill_level);
        contentValues.put("secondary_sports", secondary_sports);
        contentValues.put("rewards", rewards);
        contentValues.put("street_name", street_name);
        contentValues.put("locality", locality);
        contentValues.put("province_abbreviation", province_abbreviation);
        contentValues.put("postal_code", postal_code);
        contentValues.put("country", country);
        db.insert("CURRENT_USER", null, contentValues);
        return true;
    }

	/**
	 * Saving games detail in Summary page
	 * @param user_id
	 * @param sport_id
	 * @param venue_name
	 * @param venue_id
	 * @param game_name
	 * @param venue_price
	 * @param players_count
	 * @param date
	 * @param time
	 * @param players_amount
	 * @param game_visibility
     * @param skill_level
     * @param notes
     * @return
     */
	public boolean insertGamesDetails(String user_id, String sport_id,String venue_name, String venue_id,
									  String game_name, String venue_price,String players_count, String date,
									  String time, String players_amount,String game_visibility, String skill_level,
									  String notes) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", user_id);
		contentValues.put("sport_id", sport_id);
		contentValues.put("venue_name", venue_name);
		contentValues.put("venue_id", venue_id);
		contentValues.put("game_name", game_name);
		contentValues.put("venue_price", venue_price);
		contentValues.put("players_count", players_count);
		contentValues.put("date", date);
		contentValues.put("time", time);
		contentValues.put("players_amount", players_amount);
		contentValues.put("game_visibility", game_visibility);
		contentValues.put("skill_level", skill_level);
		contentValues.put("notes", notes);

		db.insert("GAMES", null, contentValues);
		return true;
	}


    public boolean insertCredentials(String aws_access_key, String aws_secret_key, String aws_bucket, String google_api_key,
                                      String google_places_api_key, String facebook_id, String stripe_secret_key,
                                      String stripe_publishable_key){
        SQLiteDatabase db = this.dBHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("aws_access_key", aws_access_key);
        contentValues.put("aws_secret_key", aws_secret_key);
        contentValues.put("aws_bucket", aws_bucket);
        contentValues.put("google_api_key", google_api_key);
        contentValues.put("google_places_api_key", google_places_api_key);
        contentValues.put("facebook_id", facebook_id);
        contentValues.put("stripe_secret_key", stripe_secret_key);
        contentValues.put("stripe_publishable_key", stripe_publishable_key);
        db.insert(CREDENTIALS, null, contentValues);
        return true;
    }

	/**
	 * Insert data into the CHAT_UNREAD_MESSAGES Table
	 *
	 * @return
	 */
	public boolean insertChatUnreadMessages(String channel_id,
			int unreadmessage_count) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("channel_id", channel_id);
		contentValues.put("unread_messages", unreadmessage_count);
		db.insert("CHAT_UNREAD_MESSAGES", null, contentValues);
		return true;
	}

    public boolean insertCarddetails(String cardBrand,
                                            String country, String funding,
                                     String lastfour, String custId, String cardId,
									 String cardHolderName, String exp_month,
                                     String exp_year, int default_card_status) {
        SQLiteDatabase db = this.dBHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("brand", cardBrand);
        contentValues.put("country", country);
        contentValues.put("funding", funding);
        contentValues.put("lastfour", lastfour);
        contentValues.put("customer_id", custId);
        contentValues.put("card_id", cardId);
		contentValues.put("cardHolderName", cardHolderName);
		contentValues.put("exp_month", exp_month);
		contentValues.put("exp_year", exp_year);
        contentValues.put("default_card", default_card_status);
        db.insert("CARD_DETAILS", null, contentValues);
        return true;
    }



	public boolean insertGroupChatStatus(String channelid, String teamid) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("channel_id", channelid);
		contentValues.put("team_id", teamid);
		db.insert("Group_Chat_Team_Status", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO CANCELL_GAME_CHECKIN TABLE
	 */
	public boolean insertCancelGameStatus(String gameid, int checkinid,
			String authtoken) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("game_id", gameid);
		contentValues.put("checkin_id", checkinid);
		contentValues.put("auth_token", authtoken);
		db.insert("CANCELL_GAME_CHECKIN", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO GCM TABLE
	 */
	public boolean insertNearByGameData(String game_id, String date) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("game_id", game_id);
		contentValues.put("date", date);
		db.insert("NEARBY_GAMES", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO Recurring games table
	 */
	public boolean insertRecurringGames(String game_id, String isrecurring,
			String recurring_type) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("game_id", game_id);
		contentValues.put("isRecurring", isrecurring);
		contentValues.put("recurring_type", recurring_type);
		db.insert("RECURRING_GAMES", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO GCM TABLE
	 */
	public boolean insertuserGCM(String device_token, String register_id) {
		table_status = true;
		System.out.println("ENTER DATA INTO GCM TABLE");
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put("device_token", device_token);
		contentValues.put("register_id", register_id);
		db.insert("GCM", null, contentValues);
		return true;
	}

	public boolean insertCounts(String username, int game_invitation_count,
			int player_request_count) {
		table_status = true;
		System.out.println("ENTER DATA INTO GCM TABLE");
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put("user_name", username);
		contentValues.put("game_invitation_count", game_invitation_count);
		contentValues.put("player_request_count", player_request_count);
		db.insert("COUNTS", null, contentValues);
		return true;
	}

	public boolean insertPendingFriend_invites(String user_id,
			String user_name, String user_sport, String user_image) {
		table_status = true;
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", user_id);
		contentValues.put("user_name", user_name);
		contentValues.put("user_sport", user_sport);
		contentValues.put("user_image", user_image);
		db.insert("PENDINGFRIEND_INVITES", null, contentValues);
		return true;
	}

	/**
	 * Inserting the non friend info for chatting
	 *
	 */

	public boolean insertNonFriendDetails(String user_id, String user_name,
			String firstname, String lastname, String image) {

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", user_id);
		contentValues.put("username", user_name);
		contentValues.put("first_name", firstname);
		contentValues.put("last_name", lastname);
		contentValues.put("image", image);
		db.insert("NONFRIEND_CHAT_DETAILS", null, contentValues);
		return true;

	}

	/**
	 * Insert group Chat team details
	 * 
	 * @param playerid
	 * @param playername
	 * @param playerfirstname
	 * @param playerlastname
	 * @param playerimage
	 * @return
	 */
	public boolean insertgroupChatDetaiils(String playerid, String playername,
			String playerfirstname, String playerlastname, String playerimage, String groupid) {

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("player_id", playerid);
		contentValues.put("player_name", playername);
		contentValues.put("plyaer_firstname", playerfirstname);
		contentValues.put("player_lastname", playerlastname);
		contentValues.put("player_image", playerimage);
		contentValues.put("group_id", groupid);
		db.insert("GROUP_CHAT_TEAM_DETAILS", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO PushNotification TABLE
	 */
	public boolean insertuserPushNotification(String username,
			String notification_main, String player_request,
			String game_invitation, String nearby_games, String message_board,
			String game_updates, String game_updates_post,
			String game_updates_comments, String game_updates_cancel,
			String game_updates_edit, String game_updates_join_leave,
			String nearby_games_option) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put("user_name", username);
		contentValues.put("notification_main", notification_main);
		contentValues.put("player_request", player_request);
		contentValues.put("game_invitation", game_invitation);
		contentValues.put("nearby_games", nearby_games);
		contentValues.put("message_board", message_board);
		contentValues.put("game_updates", game_updates);
		contentValues.put("game_updates_post", game_updates_post);
		contentValues.put("game_updates_comments", game_updates_comments);
		contentValues.put("game_updates_cancel", game_updates_cancel);
		contentValues.put("game_updates_edit", game_updates_edit);
		contentValues.put("nearby_games_option", nearby_games_option);
		contentValues.put("game_updates_join_leave", game_updates_join_leave);
		db.insert("PUSHNOTIFICATIONSTATUS", null, contentValues);
		return true;
	}

	// channel_id text,sender_id text,revicer_id text,time text,message text

	/*
	 * INSERTING DATA INTO CCHAT_CONVERSATION TABLE
	 */
	public boolean insertuserConversationData(String channel_id,
			String sender_id, String revicer_id, String sender_name,
			String message_time, String message_date, String user_message,
			boolean isSelf, String image, String username, String timestamp,
			String message_id, int deleteStatus) {

		int is_self = isSelf ? 1 : 0;

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("channel_id", channel_id);
		contentValues.put("sender_id", sender_id);
		contentValues.put("revicer_id", revicer_id);
		contentValues.put("sender_name", sender_name);
		contentValues.put("date", message_date);
		contentValues.put("time", message_time);
		contentValues.put("message", user_message);
		contentValues.put("is_self", is_self);
		contentValues.put("image", image);
		contentValues.put("username", username);
		contentValues.put("time_stamp", timestamp);
		contentValues.put("message_id", message_id);
		contentValues.put("isDeleted", deleteStatus);
		// contentValues.put("chat_type", chattype);
		db.insert("USER_CONVERSATION", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO CHAT_INFO TABLE
	 */

	public boolean insertChatInfoData(String channel_id, String user_id,
			String teammate_id, String teammate_name, String teammate_image,
			String type, String fullname, String message, String time_stamp) {

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("channel_id", channel_id);
		contentValues.put("user_id", user_id);
		contentValues.put("teammate_id", teammate_id);
		contentValues.put("teammate_name", teammate_name);
		contentValues.put("teammate_image", teammate_image);
		contentValues.put("type", type);
		contentValues.put("fullname", fullname);
		contentValues.put("chat_message", message);
		contentValues.put("time_stamp", time_stamp);
		db.insert("CHAT_INFO", null, contentValues);
		return true;
	}

	public boolean insertBlockFriend(String user_id, String user_name,
			String user_phonenumber, String user_sport) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", user_id);
		contentValues.put("user_name", user_name);
		contentValues.put("user_phonenumber", user_phonenumber);
		contentValues.put("user_sport", user_sport);
		db.insert("BLOCK_FRIEND", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO GAMERADIUS TABLE
	 */
	public boolean insertGameRadius(String username, String radius) {

		System.out.println("ENTER DATA INTO GCM TABLE");
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_name", username);
		contentValues.put("radius", radius);
		db.insert("GAMERADIUS", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO SKILL_LEVEL TABLE
	 */
	public boolean insertSkillLevel(String sportid, String skill_level) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("sport_id", sportid);
		contentValues.put("skill_level", skill_level);
		db.insert("SKILL_LEVEL", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO GAME DETAIL TABLE
	 */
	public boolean insertGameDetail(String game_title, String sport,
			String location, String date, String time, String skill_level,
			String player_count, String maxplayer_count, String game_mile,
			String created_by, String game_id, String address,
			String gamestatus, String invitedbyusername, String game_type,
			String initial_note, String visibility, String gamecategory,
			int message_count, String geo_place_id, String geoplace_referance,
			String geo_place_photo_referance, String enddate) {
		table_status = true;
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("game_title", game_title);
		contentValues.put("sport", sport);
		contentValues.put("location", location);
		contentValues.put("date", date);
		contentValues.put("time", time);
		contentValues.put("skill_level", skill_level);
		contentValues.put("player_count", player_count);
		contentValues.put("maxplayer_count", maxplayer_count);
		contentValues.put("game_mile", game_mile);
		contentValues.put("created_by", created_by);
		contentValues.put("address", address);
		contentValues.put("gamestatus", gamestatus);
		contentValues.put("invitedbyusername", invitedbyusername);
		contentValues.put("game_type", game_type);
		contentValues.put("initial_note", initial_note);
		contentValues.put("game_id", game_id);
		contentValues.put("visibility", visibility);
		contentValues.put("gamecategory", gamecategory);
		contentValues.put("geo_place_id", geo_place_id);
		contentValues.put("geo_place_referance", geoplace_referance);
		contentValues.put("geo_place_photo_referance",
				geo_place_photo_referance);
		contentValues.put("end_date", enddate);
		db.insert("GAME_DETAIL", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO GAME DETAIL TABLE
	 */
	public boolean insertinvitationInfo(String game_id, String request_id) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("game_id", game_id);
		contentValues.put("request_id", request_id);
		db.insert("INVITATION_INFO", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO GAME DETAIL TABLE
	 */
	public boolean insertGCMId(String devise_token, String reg_id) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("registration_id", reg_id);
		contentValues.put("device_token", devise_token);
		db.insert("GCM_ID", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO USER TABLE
	 */
	public boolean insertuser(String user_id, String auth_token,
			String user_name, String img, String current_status,
			String firstname, String lastname, String phone_number,
			String sport, String email, String birth_date,
			String data_provider, String fb_user_id) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", user_id);
		contentValues.put("auth_id", auth_token);
		contentValues.put("user_name", user_name);
		contentValues.put("profile_picture", img);
		contentValues.put("current_status", current_status);
		contentValues.put("firstname", firstname);
		contentValues.put("lastname", lastname);
		contentValues.put("phone_number", phone_number);
		contentValues.put("sport", sport);
		contentValues.put("created_at", getDateTime());
		contentValues.put("updated_at", getDateTime());
		contentValues.put("email", email);
		contentValues.put("birth_date", birth_date);
		contentValues.put("provider", data_provider);
		contentValues.put("fb_id", fb_user_id);
		db.insert("USER", null, contentValues);

		return true;
	}

	/*
	 * INSERTING DATA INTO FRIEND TABLE
	 */
	public boolean insertfriend(String userId, String userName, String firstName,
                                String lastName, String image,
                                String sport, String phoneNumber, String status, String reward) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", userId);
		contentValues.put("username", userName);
		contentValues.put("firstname", firstName);
		contentValues.put("lastname", lastName);
		contentValues.put("image", image);
		contentValues.put("sport", sport);
		contentValues.put("phonenumber", phoneNumber);
		contentValues.put("status", status);
		contentValues.put("reward", reward);
		db.insert("FRIEND", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO SPORT TABLE
	 */

	public boolean insertsport(String sport_name, String sport_id) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put("sport_name", sport_name);
		contentValues.put("sport_id", sport_id);
		contentValues.put("created_at", getDateTime());
		contentValues.put("updated_at", getDateTime());
		db.insert("SPORT", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO MYTEAM TABLE
	 */
	public boolean insertMyTeam(String teamId,String teamName,String teamSport,
                                String teamCustomSport, String teamCreatorId,
                                String teamCreatorName,String teamCreatorImage,
                                String teamCreatedAt,String teamUpdatedAt) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("team_id", teamId);
		contentValues.put("team_name", teamName);
		contentValues.put("team_sport_id", teamSport);
		contentValues.put("team_custom_sport_name", teamCustomSport);
		contentValues.put("team_creator_user_id", teamCreatorId);
		contentValues.put("team_creator_user_name", teamCreatorName);
		contentValues.put("team_creator_profile_image", teamCreatorImage);
		contentValues.put("team_created_at", teamCreatedAt);
		contentValues.put("team_updated_at", teamUpdatedAt);
		db.insert("MYTEAM", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO TEAM PLAYER TABLE
	 */
	public boolean insertTeamPlayer(String teamId,String player_id,String player_user_name,
                                    String player_first_name,String player_last_name,
                                    String player_image) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("team_id", teamId);
		contentValues.put("player_id", player_id);
		contentValues.put("player_user_name", player_user_name);
		contentValues.put("player_first_name", player_first_name);
        contentValues.put("player_last_name", player_last_name);
		contentValues.put("player_profile_image", player_image);
		db.insert("TEAMPLAYER", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO TEAM PLAYER TABLE
	 */
	public boolean insertTeammate(String auth_id, String game_id,
			String player_id, String player_name, String request_status,
			String invited_by, String connection_status, String teamorfriend,
			String player_status, String image_url) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		String status = "1";
		contentValues.put("auth_id", auth_id);
		contentValues.put("game_id", game_id);
		contentValues.put("player_id", player_id);
		contentValues.put("player_name", player_name);
		contentValues.put("request_status", request_status);
		contentValues.put("status", status);
		contentValues.put("invited_by", invited_by);
		contentValues.put("connection_status", connection_status);
		contentValues.put("teamorfriend", teamorfriend);
		contentValues.put("created_at", getDateTime());
		contentValues.put("updated_at", getDateTime());
		contentValues.put("player_status", player_status);
		contentValues.put("image_url", image_url);
		db.insert("TEAMMATE", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO TEAM PLAYER TABLE
	 */
	public boolean insertUserprofile(String user_id, String username,
			String name, String phone_number, String user_sport,
			String user_email, String user_profileimage, String user_aboutme,
			String expertlevel, int rewards, String birth_date) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("user_id", user_id);
		contentValues.put("user_name", username);
		contentValues.put("name", name);
		contentValues.put("phone_number", phone_number);
		contentValues.put("user_sport", user_sport);
		contentValues.put("user_email", user_email);
		contentValues.put("user_profileimage", user_profileimage);
		contentValues.put("user_aboutme", user_aboutme);
		contentValues.put("expertlevel", expertlevel);
		contentValues.put("rewards", rewards);
		contentValues.put("birth_date", birth_date);
		db.insert("USERPROFILE", null, contentValues);

		return true;
	}

	/*
	 * INSERTING DATA INTO LOCALGAME_LIST TABLE
	 */
	public boolean insertLocalGames(String title, String sport, String address,
			String initial_note, String start_time, String end_time,
			String latitude, String longitude, String miles, String game_id,
			String date, String player_count, String skill_level,
			String is_custom, String max_player, boolean isattending,
			String gamestatus, String latlng, String time, String game_type,
			String visibility, int message_count, String game_status,
			String end_date, String game_category, String geo_place_id,
			String geo_reference, String geo_photo_reference,
			String created_by, String curr_latitude, String curr_longitude,
			String customgame_name, String convert_time, String updated_at) {

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("title", title);
		contentValues.put("sport", sport);
		contentValues.put("address", address);
		contentValues.put("initial_note", initial_note);
		contentValues.put("start_time", start_time);
		contentValues.put("end_time", end_time);
		contentValues.put("latitude", latitude);
		contentValues.put("longitude", longitude);
		contentValues.put("miles", miles);
		contentValues.put("game_id", game_id);
		contentValues.put("game_date", date);
		contentValues.put("player_count", player_count);
		contentValues.put("skill_level", skill_level);
		contentValues.put("is_custom", is_custom);
		contentValues.put("max_player", max_player);
		contentValues.put("is_attending", isattending);
		contentValues.put("gamestatus", gamestatus);
		contentValues.put("latlng", latlng);
		contentValues.put("time", time);
		contentValues.put("game_type", game_type);
		contentValues.put("visibility", visibility);
		contentValues.put("message_count", message_count);
		contentValues.put("is_attending", isattending);
		contentValues.put("game_status", game_status);
		contentValues.put("end_date", end_date);
		contentValues.put("game_category", game_category);
		contentValues.put("geo_place_id", geo_place_id);
		contentValues.put("geo_reference", geo_reference);
		contentValues.put("geo_photo_reference", geo_photo_reference);
		contentValues.put("created_by", created_by);
		contentValues.put("curr_latitude", curr_latitude);
		contentValues.put("curr_longitude", curr_longitude);
		contentValues.put("customgame", customgame_name);
		contentValues.put("converted_time", convert_time);
		contentValues.put("updated_at", updated_at);
		db.insert("LOCALGAME_LIST", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO TEAM PLAYER TABLE
	 */
	public boolean insertGamePlayer(String game_id, String player_id,
			String player_name, String request_status, String invited_by) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put("game_id", game_id);
		contentValues.put("player_id", player_id);
		contentValues.put("player_name", player_name);
		contentValues.put("request_status", request_status);
		contentValues.put("invited_by", invited_by);

		db.insert("GAMEPLAYER", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO TEAM PLAYER TABLE
	 */
	public boolean insertGame_Players(String game_id, String player_id,
			String player_name, String request_status, String invited_by,
			String image, String connection_status) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put("game_id", game_id);
		contentValues.put("player_id", player_id);
		contentValues.put("player_name", player_name);
		contentValues.put("request_status", request_status);
		contentValues.put("invited_by", invited_by);
		contentValues.put("profile_image", image);
		contentValues.put("connection_status", connection_status);
		db.insert("GAME_PLAYERS", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO ACTIVITY_FEEDS TABLE
	 */
	public boolean insertActivityFeeds(String username, String sport,
			String address, String miles, String date, String image,
			String friend_id, String friend_username, String activity_type,
			String user_id, String start_time, String game_id,
			String activity_date, String game_title) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("username", username);
		contentValues.put("sport", sport);
		contentValues.put("address", address);
		contentValues.put("miles", miles);
		contentValues.put("date", date);
		contentValues.put("image_url", image);
		contentValues.put("friend_id", friend_id);
		contentValues.put("friend_username", friend_username);
		contentValues.put("activity_type", activity_type);
		contentValues.put("user_id", user_id);
		contentValues.put("start_time", start_time);
		contentValues.put("game_id", game_id);
		contentValues.put("activity_date", activity_date);
		contentValues.put("game_title", game_title);
		db.insert("ACTIVITY_FEEDS", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO COMMENT TABLE
	 */
	public boolean insertComments(String msg_id, String comments,
			String comment_id, String created_at, String username) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("msg_id", msg_id);
		contentValues.put("comment_id", comments);
		contentValues.put("username", username);
		contentValues.put("date", created_at);
		contentValues.put("comment", comment_id);
		db.insert("COMMENTs", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO COMMENT TABLE
	 */
	public boolean insertAllComments(String msg_id, String comments,
			String comment_id, String created_at, String username,
			int comment_count) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("msg_id", msg_id);
		contentValues.put("comment_id", comments);
		contentValues.put("username", username);
		contentValues.put("date", created_at);
		contentValues.put("comment", comment_id);
		contentValues.put("comment_count", comment_count);
		db.insert("COMMENTs", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO NEXTSCHEDULEDGAME TABLE
	 */
	public boolean insertNextScheduledGame(String game_id, String title,
			String sport, String address, String starttime, String endtime,
			String date, String skillevel, String note, String game_type,
			int player_count, String created_by, String miles, String end_date,
			int max_player, String visibility, int message_count,
			String game_categoty, String localtion, String geo_place_id,
			String geo_referance, String geo_photo_referance, String date_time,
			String customgame_name) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("game_id", game_id);
		contentValues.put("title", title);
		contentValues.put("sport", sport);
		contentValues.put("address", address);
		contentValues.put("starttime", starttime);
		contentValues.put("endtime", endtime);
		contentValues.put("date", date);
		contentValues.put("skillevel", skillevel);
		contentValues.put("note", note);
		contentValues.put("game_type", game_type);
		contentValues.put("player_count", player_count);
		contentValues.put("created_by", created_by);
		contentValues.put("miles", miles);
		contentValues.put("end_date", end_date);
		contentValues.put("max_player", max_player);
		contentValues.put("visibility", visibility);
		contentValues.put("message_count", message_count);
		contentValues.put("game_category", game_categoty);
		contentValues.put("location", localtion);
		contentValues.put("geo_place_id", geo_place_id);
		contentValues.put("geo_referance", geo_referance);
		contentValues.put("geo_photo_referance", geo_photo_referance);
		contentValues.put("datetime", date_time);
		contentValues.put("customgame", customgame_name);
		db.insert("NEXTSCHEDUEDGAME", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO NEXTSCHEDULEDGAME TABLE
	 */

	public boolean insertAllScheduledGame(String game_id, String title,
			String sport, String address, String time, String date,
			String skillevel, String note, String game_type, int player_count,
			String starttime, String endtime, String miles,
			String game_category, String game_visibility, String game_enddate,
			String game_creator, int game_maxplayercount,
			int game_messagecount, String location, String geo_place_id,
			String geo_referance, String geo_photo_referance, String date_time) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("game_id", game_id);
		contentValues.put("title", title);
		contentValues.put("sport", sport);
		contentValues.put("address", address);
		contentValues.put("time", time);
		contentValues.put("date", date);
		contentValues.put("skillevel", skillevel);
		contentValues.put("note", note);
		contentValues.put("game_type", game_type);
		contentValues.put("player_count", player_count);
		contentValues.put("starttime", starttime);
		contentValues.put("endtime", endtime);
		contentValues.put("miles", miles);
		contentValues.put("game_category", game_category);
		contentValues.put("visibility", game_visibility);
		contentValues.put("end_date", game_enddate);
		contentValues.put("created_by", game_creator);
		contentValues.put("message_count", game_maxplayercount);
		contentValues.put("max_player", game_messagecount);
		contentValues.put("location", location);
		contentValues.put("geo_place_id", geo_place_id);
		contentValues.put("geo_referance", geo_referance);
		contentValues.put("geo_photo_referance", geo_photo_referance);
		contentValues.put("date_time", date_time);
		db.insert("ALLSCHEDUEDGAME", null, contentValues);
		return true;
	}

	/*
	 * INSERTING DATA INTO NEXTSCHEDULEDGAME TABLE
	 */

	public boolean insertGameHistory(String username, String game_id,
			String title, String sport, String address, String time,
			String date, String skillevel, String note, String game_type,
			int player_count, String starttime, String endtime,
			String latitude, String longitude, String miles) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("username", username);
		contentValues.put("game_id", game_id);
		contentValues.put("title", title);
		contentValues.put("sport", sport);
		contentValues.put("address", address);
		contentValues.put("time", time);
		contentValues.put("date", date);
		contentValues.put("skillevel", skillevel);
		contentValues.put("note", note);
		contentValues.put("game_type", game_type);
		contentValues.put("player_count", player_count);
		contentValues.put("latitude", latitude);
		contentValues.put("longitude", longitude);
		contentValues.put("miles", miles);
		db.insert("GAMEHISTORY", null, contentValues);
		return true;
	}

	// INSERTING DATA INTO NEXTSCHEDULEDGAME TABLE
	public boolean insertPendingRequest(String request_id, String game_id,
			String title, String sport, String address, String starttime,
			String endtime, String date, String skillevel, String invited_by,
			int player_count, String visibility, String initial_note,
			String request_status, String joining_date, int max_player,
			String latitude, String longitude, String game_type,
			String game_category, int message_count, String geo_place_id,
			String geo_referance, String geo_photo_referance) {

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("request_id", request_id);
		contentValues.put("game_id", game_id);
		contentValues.put("title", title);
		contentValues.put("sport", sport);
		contentValues.put("address", address);
		contentValues.put("starttime", starttime);
		contentValues.put("endtime", endtime);
		contentValues.put("date", date);
		contentValues.put("skillevel", skillevel);
		contentValues.put("invited_by_username", invited_by);
		contentValues.put("player_count", player_count);
		contentValues.put("visibility", visibility);
		contentValues.put("initial_note", initial_note);
		contentValues.put("request_status", request_status);
		contentValues.put("joining_date", joining_date);
		contentValues.put("max_player", max_player);
		contentValues.put("latitude", latitude);
		contentValues.put("longitude", longitude);
		contentValues.put("game_type", game_type);
		contentValues.put("game_category", game_category);
		contentValues.put("message_count", message_count);
		contentValues.put("geo_place_id", geo_place_id);
		contentValues.put("geo_referance", geo_referance);
		contentValues.put("geo_photo_referance", geo_photo_referance);
		db.insert("PENDINGREQUEST", null, contentValues);
		return true;
	}

	// INSERTING DATA INTO COMMENT TABLE
	public boolean insertLocationDetails(String name, String vicinity,
			String icon, String miles, String place_id, String referance,
			String photo_referance, String latitude, String longitude,
			String latlng, String place_id_db) {

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("name", name);
		contentValues.put("vicinity", vicinity);
		contentValues.put("icon", icon);
		contentValues.put("miles", miles);
		contentValues.put("place_id", place_id);
		contentValues.put("referance", referance);
		contentValues.put("photo_referance", photo_referance);
		contentValues.put("latitude", latitude);
		contentValues.put("longitude", longitude);
		contentValues.put("latlng", latlng);
		contentValues.put("place_id_db", place_id_db);
		db.insert("LOCATION_DETAILS", null, contentValues);
		return true;
	}

	// GETTING INSERTED ROW CREATED AT AND UPDATED AT
	public String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		Date date = new Date();
		return dateFormat.format(date);
	}

	// GET ALL SPORTS
	public Cursor getAllFriendsFromDb() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM SPORT", null);

		return c1;

	}

	// GETTING ReG ID FROM GCM TABLE BY DEVICE_TOKEN
	public String getRegId(String device_token) {
		String selected_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT register_id FROM GCM where device_token = '"
						+ device_token + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			selected_id = (c1.getString(c1.getColumnIndex("register_id")));
			c1.moveToNext();
		}
		System.out.println(" register_id FROM DB" + selected_id);
		return selected_id;

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public String getPushStatus(String device_token) {
		String selected_status = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT push_status FROM GCM where device_token = '"
						+ device_token + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			selected_status = (c1.getString(c1.getColumnIndex("push_status")));
			c1.moveToNext();
		}
		System.out.println(" selected_status FROM DB" + selected_status);
		return selected_status;

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updatePushStatus(String push_status, String device_token) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c = db.rawQuery("UPDATE GCM SET push_status = '" + push_status
				+ "'" + " where device_token = '" + device_token + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount() UPDATE*****************"
				+ c.getCount());

	}

	// FILTERING FRIEND BY SPORT NAME
	public Cursor getcurrentsportfrienlist(String SportName, String auth_token) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String block_status = "unblock";
		if (SportName.equalsIgnoreCase("All")) {
			return db
					.rawQuery(
							"SELECT friend_name,sport,friend_id,user_status,game_title,game_id FROM FRIEND where auth_id ='"
									+ auth_token
									+ "'"
									+ " AND block_status ='"
									+ block_status + "'", null);
		} else {
			return db
					.rawQuery(
							"SELECT friend_name,sport,friend_id,user_status,game_title,game_id FROM FRIEND where sport ='"
									+ SportName
									+ "'"
									+ " AND auth_id ='"
									+ auth_token
									+ "'"
									+ " AND block_status ='"
									+ block_status + "'", null);
		}
	}


	// UPDATE STATUS FRIEND
	public void updateStausfrienlist(String SportName, String FriendName) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";
		Cursor c;
		if (SportName.equalsIgnoreCase("All")) {
			c = db.rawQuery("UPDATE FRIEND SET status = '" + status + "'"
					+ " where friend_name = '" + FriendName + "'", null);
		} else {
			c = db.rawQuery("UPDATE FRIEND SET status = '" + status + "'"
					+ " where sport = '" + SportName + "'"
					+ " AND friend_name = '" + FriendName + "'", null);
		}
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// UPDATE inviteTeammate_Status FRIEND
	public void updateStausfrienlistForInviteTeammate(String FriendName) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String inviteTeammate_Status = "1";
		Cursor c;

		c = db.rawQuery("UPDATE FRIEND SET inviteTeammate_Status = '"
				+ inviteTeammate_Status + "'" + " where friend_name = '"
				+ FriendName + "'", null);
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// UPDATE inviteTeammate_Status FRIEND
	public void updateStausfrienlistForInviteTeammate1(String FriendName) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String inviteTeammate_Status = "0";
		Cursor c;

		c = db.rawQuery("UPDATE FRIEND SET inviteTeammate_Status = '"
				+ inviteTeammate_Status + "'" + " where friend_name = '"
				+ FriendName + "'", null);
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// UPDATE inviteTeammate_Status FRIEND
	public void updateStausteammate(String player_name) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "0";
		Cursor c;

		c = db.rawQuery("UPDATE TEAMMATE SET status = '" + status + "'"
				+ " where player_name = '" + player_name + "'", null);
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// FILTERING MYTEAM BY SPORT ID
	public Cursor getcurrentsportMyTeam(String sportName, String auth_token) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String sportId = getcurrentSportId(sportName);
		Cursor c1 = null;

		if (sportName.equalsIgnoreCase("All")) {
			c1 = db.rawQuery(
					"SELECT team_name,team_sport_id,team_custom_sport_name,team_id FROM MYTEAM where team_sport_id = '"
							+ sportName + "'", null);
		} else {
			c1 = db.rawQuery(
					"SELECT team_name,team_sport_id,team_custom_sport_name,team_id FROM MYTEAM where team_sport_id ='"
							+ sportId + "'", null);
		}

		return c1;

	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public String getcurrentSportId(String sportName) {
		String selected_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT sport_id FROM SPORT where sport_name = '" + sportName
						+ "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			selected_id = (c1.getString(c1.getColumnIndex("sport_id")));
			c1.moveToNext();
		}

		// System.out.println(" selected_frdId FROM DB" + selected_id);
		return selected_id;

	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public String getcurrentSportName(String sportId) {
		String selected_sportName = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT sport_name FROM SPORT where sport_id = '" + sportId
						+ "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			selected_sportName = (c1.getString(c1.getColumnIndex("sport_name")));
			c1.moveToNext();
		}
		System.out.println(" selected_sportName FROM DB" + selected_sportName);
		return selected_sportName;

	}

	// DELETE FRIEND TABLE ROW BY THE HELP OF FRIEND ID
	public boolean deleteFriend(String friend_id) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		try {
			db.delete(FRIEND, "friend_id = ?", new String[] { friend_id });
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.close();
		}
		return false;
	}

	// UPDATE FRIEND TABLE ROW FOR BLOCK FRIEND ID
	public void blockFriend(String friend_id) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		String block_status = "block";
		Cursor c = db.rawQuery("UPDATE FRIEND SET block_status ='"
				+ block_status + "'" + " where friend_id ='" + friend_id + "'",
				null);
		c.moveToNext();
	}

	// UPDATE FRIEND TABLE ROW FOR UNBLOCK FRIEND ID
	public void unblockFriend(String friend_id) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		String block_status = "unblock";
		Cursor c = db.rawQuery("UPDATE FRIEND SET block_status ='"
				+ block_status + "'" + " where friend_id ='" + friend_id + "'",
				null);
		c.moveToNext();
	}

	// GETTING ALL BLOCK FRIEND LIST FROM FRIEND TABLE
	public Cursor blockFriendlist() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		return db.rawQuery("SELECT * FROM BLOCK_FRIEND ", null);

	}

	// GETTING MYTEAM TABLE LATEST UPDATED COLUMN
	public String getlatestupdateMyTeam(String auth_token) {
		String selected_date = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM " + MYTEAM + " where auth_id= '"
				+ auth_token + "' ORDER BY updated_at DESC LIMIT 1", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				selected_date = (c1.getString(c1.getColumnIndex("updated_at")));
				c1.moveToNext();
			}

		}
		System.out.println("PULL NEW MY TEAM selected_date FROM DB"
				+ selected_date);
		return selected_date;
	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public String getcurrentfriendId(String FriendName) {
		String selected_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT user_id FROM " + FRIEND
				+ " where username = '" + FriendName + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				selected_id = (c1.getString(c1.getColumnIndex("user_id")));
				c1.moveToNext();
			}
		}
		return selected_id;
	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public String getcurrentfriendName(String FriendId) {
		String selected_name = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT friend_name FROM FRIEND where friend_id ='" + FriendId
						+ "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				selected_name = (c1.getString(c1.getColumnIndex("friend_name")));
				c1.moveToNext();
			}

		}
		System.out.println(" selected_frdName FROM DB" + selected_name);
		return selected_name;

	}

	// GETTING TEAM ID FROM MYTEAM TABLE BY TEAM NAME
	public String getCurrentTeamId(String TeamName) {
		String selected_team_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT team_id FROM " + MYTEAM
				+ " where name = '" + TeamName + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				selected_team_id = (c1.getString(c1.getColumnIndex("team_id")));
				c1.moveToNext();
			}

		}
		System.out.println(" selected_teamId FROM DB----" + selected_team_id);
		return selected_team_id;

	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<String> getTeamPlayerName(String TeamName) {
		ArrayList<String> allPlayerName = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";
		String block_status = "unblock";
		Cursor c1 = db.rawQuery("SELECT team_player_name FROM " + TEAMPLAYER
				+ " where team_name = '" + TeamName + "'" /*
														 * + " AND status = '" +
														 * status + "'"
														 */, null);
		System.out.println("c1.getCount()-->" + c1.getCount());
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				String team_player_name = c1.getString(c1
						.getColumnIndex("team_player_name"));

				Cursor c2 = db.rawQuery(
						"SELECT friend_name FROM FRIEND where friend_name='"
								+ team_player_name + "'"
								+ "AND block_status ='" + block_status + "'",
						null);
				System.out.println("c2.getCount()-->" + c2.getCount());
				c2.moveToFirst();
				if (c2.getCount() != 0) {
					for (int j = 0; j < c2.getCount(); j++) {
						allPlayerName.add(c2.getString(c2
								.getColumnIndex("friend_name")));
						c2.moveToNext();
					}
				}
				c2.close();
				c1.moveToNext();
			}

		}
		System.out.println(" selected_allPlyername FROM TEAMPLAYER DB----"
				+ allPlayerName);
		c1.close();

		return allPlayerName;

	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<String> getTeammateName(String Game_Id) {
		ArrayList<String> player_status_pair = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";

		Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
				+ " where game_id = '" + Game_Id + "'" + " AND status = '"
				+ status + "'", null);
		System.out.println("c1.getCount()-->" + c1.getCount());
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				String player_name = c1.getString(c1
						.getColumnIndex("player_name"));

				player_status_pair.add(player_name);
				c1.moveToNext();
			}

		}

		return player_status_pair;

	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<String> getAllNewTeammates() {
		ArrayList<String> player_names = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String player_status = "new";
		// String block_status="unblock";
		Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
				+ " where  player_status ='" + player_status + "'", null);
		System.out.println("c1.getCount()-->" + c1.getCount());
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				String player_name = c1.getString(c1
						.getColumnIndex("player_name"));

				player_names.add(player_name);
				c1.moveToNext();
			}

		}

		return player_names;

	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<String> getIndividualplayername(String team_name) {
		ArrayList<String> player_names = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMPLAYER
				+ " where  team_name ='" + team_name + "'", null);
		System.out.println("c1.getCount()-->" + c1.getCount());
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				String player_name = c1.getString(c1
						.getColumnIndex("team_player_name"));

				player_names.add(player_name);
				c1.moveToNext();
			}

		}

		return player_names;

	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<String> getTeammateNameStatus(String Game_Id) {
		ArrayList<String> player_status_pair = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";

		Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
				+ " where game_id = '" + Game_Id + "'" + " AND status = '"
				+ status + "'", null);
		System.out.println("c1.getCount()-->" + c1.getCount());
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				// String
				// player_name=c1.getString(c1.getColumnIndex("player_name"));
				String player_status = c1.getString(c1
						.getColumnIndex("request_status"));
				player_status_pair.add(player_status);
				c1.moveToNext();
			}

		}

		return player_status_pair;

	}

	// UPDATE STATUS TEAM PLAYER
	public void updateStatusTeamPlayer(String FriendName, String TeamName) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "0";
		Cursor c = db.rawQuery("UPDATE TEAMPLAYER SET status = '" + status
				+ "'" + " where team_name = '" + TeamName + "'"
				+ " AND team_player_name = '" + FriendName + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// UPDATE STATUS FRIEND
	public void updateStatusPlayer(String FriendId) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";
		Cursor c = db.rawQuery("UPDATE FRIEND SET status = '" + status + "'"
				+ " where friend_id = '" + FriendId + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// UPDATE STATUS FRIEND
	public void updateStatusPlayer1(String FriendId) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "0";
		Cursor c = db.rawQuery("UPDATE FRIEND SET status = '" + status + "'"
				+ " where friend_id = '" + FriendId + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// UPDATE STATUS FRIEND
	public void updateAllFriendStatusTeammate() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		// String status="0";
		ArrayList<String> upadteId = new ArrayList<String>();
		Cursor c1 = db.rawQuery("SELECT *FROM FRIEND", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				upadteId.add((c1.getString(c1.getColumnIndex("friend_id"))));
				c1.moveToNext();
			}

		}
		System.out.println("selected_id" + upadteId);
		for (int j = 0; j < upadteId.size(); j++) {
			String selectedId = upadteId.get(j).toString();
			System.out.println("selected_id" + selectedId);
			updateStatusPlayerTeammate1(selectedId);
		}

	}

	// UPDATE STATUS FRIEND
	public void updateAllFriendStatus() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		// String status="0";
		ArrayList<String> upadteId = new ArrayList<String>();
		Cursor c1 = db.rawQuery("SELECT *FROM FRIEND", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				upadteId.add((c1.getString(c1.getColumnIndex("friend_id"))));

				c1.moveToNext();
			}

		}
		System.out.println("selected_id" + upadteId);
		for (int j = 0; j < upadteId.size(); j++) {
			String selectedId = upadteId.get(j).toString();
			System.out.println("selected_id" + selectedId);
			updateStatusPlayer1(selectedId);
			// db.rawQuery("UPDATE FRIEND SET status = '"+status+"'"+" where friend_id = '"+selectedId+"'",
			// null);

		}

	}

	// UPDATE STATUS FRIEND
	public String getAllFriendIdByTeam(String teamname) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String selected_id = null;
		Cursor c1 = db.rawQuery("SELECT players FROM MYTEAM where name='"
				+ teamname + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				selected_id = (c1.getString(c1.getColumnIndex("players")));
				c1.moveToNext();
			}

		}

		if (c1.getCount() == 0) {
			return null;
		} else {
			return selected_id;
		}

	}

	// UPDATE STATUS FRIEND WHEN REMOVE FROM DROPPED LIST
	public void updateStatusFrienlistRemoveFromDroplist(String SportName,
			String FriendName) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "0";
		Cursor c;
		if (SportName.equalsIgnoreCase("All")) {

			c = db.rawQuery("UPDATE FRIEND SET status = '" + status + "'"
					+ " where friend_name = '" + FriendName + "'", null);
		} else {
			c = db.rawQuery("UPDATE FRIEND SET status = '" + status + "'"
					+ " where sport = '" + SportName + "'"
					+ " AND friend_name = '" + FriendName + "'", null);
		}
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());
	}

	// GETTING FRIEND TABLE LATEST UPDATED COLUMN
	public ArrayList<String> getlatestupdateFriendId(String auth_token) {
		ArrayList<String> stringArrayList = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		String block_status = "unblock";
		Cursor c1 = db.rawQuery("SELECT * FROM " + FRIEND
				+ " where auth_id = '" + auth_token + "'"
				+ " AND block_status ='" + block_status + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				String selected_friend_id = (c1.getString(c1
						.getColumnIndex("friend_id")));
				stringArrayList.add(selected_friend_id);
				c1.moveToNext();
			}

		}
		System.out.println("PULL NEW FRIEND selected_friend_id FROM DB"
				+ stringArrayList);
		return stringArrayList;
	}

	// TO CHECK TEAM ID EXISTANCE
	public void toCheckTeamExistance(String teamId, String user_id,
			String auth_id, String teamName, String players,
			String custom_sport_name, String sport_id, String created_at,
			String updated_at) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c = null;
		try {
			c = db.rawQuery("select team_id from MYTEAM where team_id = ?",
					new String[] { String.valueOf(teamId) });
			System.out.println("COUNT***************** " + c.getCount());
			if (c.getCount() != 0) {
				deleteTeam(teamId);
				System.out.println("DELETE***************** " + c.getCount());
			}
			System.out.println("INSERT***************** " + c.getCount());
			insertMyTeam(teamId, user_id, sport_id, auth_id, teamName, players,
					custom_sport_name, created_at, updated_at);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
		}
	}

	// TO CHECK TEAM EXISTANCE
	public Boolean toCheckTeamNameExistance(String teamName) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean selected_TeamName = false;
		Cursor c = null;
		try {
			c = db.rawQuery("select name from MYTEAM where name = ?",
					new String[] { String.valueOf(teamName) });
			c.moveToFirst();
			if (c.getCount() != 0) {

				selected_TeamName = true;
				c.moveToNext();
			}

			System.out.println("selected_TeamName FROM DB" + selected_TeamName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return selected_TeamName;
	}

	// TO CHECK TEAM EXISTANCE
	public Boolean toCheckSportNameExistance(String sportName) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean selected_TeamName = false;
		Cursor c = null;
		try {
			c = db.rawQuery(
					"select sport_name from SPORT where sport_name = ?",
					new String[] { String.valueOf(sportName) });
			c.moveToFirst();
			if (c.getCount() != 0) {

				selected_TeamName = true;
				c.moveToNext();
			}

			System.out.println("selected_TeamName FROM DB" + selected_TeamName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return selected_TeamName;
	}

	// TO CHECK TEAM PLAYER NAME EXISTANCE
	public void toCheckTeamPlayerNameExistance(String auth_id, String teamId,
			String teamName, String frd_Id, String team_Player_Name) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c = null;
		try {
			c = db.rawQuery(
					"select team_player_name from TEAMPLAYER where team_player_name = ?",
					new String[] { String.valueOf(team_Player_Name) });
			if (c.getCount() != 0) {
				deleteTeamPlayer(teamId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
		}
	}





	public void deleteAllData(Context context) {
		DatabaseHelper helper = new DatabaseHelper(context);
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + CREDENTIALS);
        db.execSQL("DROP TABLE IF EXISTS " + GAMES);
        db.execSQL("DROP TABLE IF EXISTS " + CARD_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + FRIEND);
        db.execSQL("DROP TABLE IF EXISTS " + MYTEAM);
        db.execSQL("DROP TABLE IF EXISTS " + TEAMPLAYER);
        db.execSQL("DROP TABLE IF EXISTS " + CURRENT_USER);
		db.execSQL("DROP TABLE IF EXISTS " + USER_RATING);
		helper.onCreate(db);
	}

	// FILTERING MYTEAM BY SPORT ID
	public Cursor getpushNotificationStatus() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM PUSHNOTIFICATIONSTATUS", null);

	}

	public void deleteActivityfeedsData(Context context) {
		db.execSQL("delete FROM " + ACTIVITY_FEEDS);
		db.close();

	}

	public void deleteChatInfo(Context context) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + CHAT_INFO);
		db.close();

	}

	public void deleteChatConversation(Context context) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + USER_CONVERSATION);
		db.close();

	}

	public void deleteGroupChatDetails(Context context) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + GROUP_CHAT_TEAM_DETAILS);
		db.close();

	}

	public void deleteSkilllevelData(Context context) {
		db.execSQL("delete FROM " + SKILL_LEVEL);
	}

	public void deleteUserprofileData(Context context) {
		db.execSQL("delete FROM " + USERPROFILE);

	}

	public void deleteAllscheduledGames(Context context) {
		db.execSQL("delete FROM " + ALLSCHEDUEDGAME);

	}

	public void deleteAllLocalgames() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + " " + LOCALGAME_LIST);
	}

	public void deleteAllSports() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + SPORT);
	}

	public void deleteAllNextScheduledgames(Context context) {
		db.execSQL("delete FROM " + NEXTSCHEDUEDGAME);
	}

	public void deleteAllScheduledgames(Context context) {
		db.execSQL("delete FROM " + ALLSCHEDUEDGAME);
	}

	public void deleteAllFriendList(Context context) {
		db.execSQL("delete FROM " + FRIEND);
	}

	public void deleteTeamPlayerData(Context context) {
		db.execSQL("delete FROM " + TEAMPLAYER);
		// db.close();

	}

	public void deleteTeamMateData(Context context) {
		db.execSQL("delete FROM " + TEAMMATE);
	}

	public void deleteFriendsInfo(Context context) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM " + FRIEND);
	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void deleteTeammateDataIFExit() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT *FROM TEAMMATE", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			deleteTeammateData();
			c1.moveToNext();
		}
	}

	public void deleteInvitationInfo(Context context) {
		db.execSQL("delete FROM " + INVITATION_INFO);
	}

	public void deleteGamehistorydata(Context context) {
		db.execSQL("delete FROM " + GAMEHISTORY);
	}

	public void deleteTeammateData() {
		db.execSQL("delete FROM " + TEAMMATE);
	}

	public void deleteGameTeammateData(String game_id) {
		db.execSQL("delete *FROM GAMEPLAYER where game_id='" + game_id + "'",
				null);

	}

	public void deleteFromUserProfile() {
		db.execSQL("delete FROM " + USERPROFILE);

	}

	public void deleteCommentData() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + " " + COMMENTS);

	}

	public void deleteGameplayer() {
		db.execSQL("delete FROM " + GAMEPLAYER);
	}

	public void deleteGameInvitation() {
		db.execSQL("delete FROM " + PENDINGREQUEST);
	}

	// FILTERING MYTEAM BY SPORT ID
	public Cursor getAllLocalGames_Map() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db
				.rawQuery(
						"SELECT * FROM LOCALGAME_LIST order by game_date DESC,CAST(miles AS REAL) ASC",
						null);
	}

	// FILTERING MYTEAM BY SPORT ID
	public Cursor getAllLocalGames() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db
				.rawQuery(
						"SELECT * FROM LOCALGAME_LIST order by game_date ASC,CAST(miles AS REAL) ASC",
						null);
	}

	// FILTERING MYTEAM BY SPORT ID
	public Cursor getUserProfile() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM USERPROFILE", null);

	}

	public Cursor getAllActivityFeeds() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM ACTIVITY_FEEDS ORDER BY date DESC",
				null);

	}

	public Cursor getSelectedLocalGame(String title) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM LOCALGAME_LIST where game_id ='"
				+ title + "' ", null);

	}

	public String getSelectedUserProfile(String user_id) {
		String username = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT name FROM USERPROFILE where user_id='"
				+ user_id + "' ", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			username = c1.getString(c1.getColumnIndex("name"));
			c1.moveToNext();
		}
		return username;
	}

	public Cursor getSelectedLocalGameLocation(String latlng) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM LOCALGAME_LIST where latlng='"
				+ latlng + "' ", null);
	}

	// Getting all the Latitude and longitude from the LOCALGAME_LIST table
	public ArrayList<String> getAllLocationLocalGame() {
		ArrayList<String> loc_arr = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT latitude,longitude FROM LOCALGAME_LIST", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				String lat = (c1.getString(c1.getColumnIndex("latitude")));
				String lon = (c1.getString(c1.getColumnIndex("longitude")));
				String fin_loc = lat + lon;
				fin_loc = fin_loc.replace(".", "");
				loc_arr.add(fin_loc);
				c1.moveToNext();
			}
		}
		return loc_arr;
	}

	public String getSelectedLocalGameGameid(String title) {
		String game_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery(
				"SELECT game_id FROM LOCALGAME_LIST where title='" + title
						+ "' ", null);

		c.moveToFirst();
		if (c.getCount() != 0) {
			game_id = (c.getString(c.getColumnIndex("game_id")));
			c.moveToNext();
		}
		return game_id;

	}

	public Cursor getAllComments(String msg_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM COMMENTS where msg_id='" + msg_id
				+ "'", null);
	}

	public Cursor getAllTitleFromDb() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM LOCALGAME_LIST", null);
	}

	public Cursor getAllGameInvitation() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM PENDINGREQUEST", null);

	}

	// Getting the Request_id from PENDINGREQUEST
	public String getRequestId(String game_id) {
		String request_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT request_id FROM PENDINGREQUEST where game_id = '"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			request_id = (c1.getString(c1.getColumnIndex("request_id")));
			c1.moveToNext();
		}
		return request_id;
	}

	public void deleteGameInvitation(String game_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM PENDINGREQUEST WHERE game_id='" + game_id + "'");

	}

	/*
	 * public Cursor getAllScheduledGames(String limit) { SQLiteDatabase db =
	 * this.dBHelper.getReadableDatabase();
	 * 
	 * return db.rawQuery(
	 * "SELECT * FROM ALLSCHEDUEDGAME ORDER BY date_time DESC LIMIT '" + limit +
	 * "'", null);
	 * 
	 * }
	 */

	public Cursor getAllScheduledGamesFromDB(String limit) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery(
				"SELECT * FROM ALLSCHEDUEDGAME ORDER BY date_time DESC", null);
	}

	public Cursor getAllGameHistory(String name) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT *FROM GAMEHISTORY where username='" + name
				+ "' order by date,starttime ASC", null);
	}

	public Cursor getAllScheduledSelectedGames(String title) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT *FROM ALLSCHEDUEDGAME where title='" + title
				+ "'", null);
	}

	public Cursor getUsername() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT user_name from user", null);
	}

	public Cursor getAllLocalGamesFortoday(String date) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where game_date='"
				+ date + "'", null);
	}

	public String getUserIdForUser(String user_id) {
		String user_name = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT friend_name FROM FRIEND where friend_id='" + user_id
						+ "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			user_name = (c1.getString(c1.getColumnIndex("friend_name")));
			c1.moveToNext();
		}
		return user_name;
	}

	public Cursor getAllLocalGamesFortomorrow(String date) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where game_date='"
				+ date + "'", null);
	}

	public Cursor getAllLocalGamesForBaseball(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);
	}

	public Cursor getAllLocalGamesForBasketball(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);

	}

	public Cursor getAllLocalGamesForCricket(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);

	}

	public Cursor getAllLocalGamesForSoccer(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);

	}

	public Cursor getAllLocalGamesForFootball(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);

	}

	public Cursor getAllLocalGamesForTennis(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);

	}

	public Cursor getAllLocalGamesForVolleyball(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);

	}

	public Cursor getAllLocalGamesForGolfball(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);

	}

	public Cursor getAllLocalGamesForRunningball(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);

	}

	public Cursor getAllLocalGamesForBikingball(String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCALGAME_LIST where sport='" + sport
				+ "'", null);

	}

	public Cursor getAllLocalGamesForCustom(String value) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		return db.rawQuery("SELECT * FROM LOCALGAME_LIST WHERE is_custom='"
				+ value + "'", null);
	}

	public Cursor getDetailslocation(String name) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * from LOCATION_DETAILS where latlng='"
				+ name + "'", null);

	}

	public Cursor getAlllocation() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db
				.rawQuery(
						"SELECT * from LOCATION_DETAILS ORDER by CAST(miles AS REAL) ASC",
						null);

	}

	public String getPlaceid(String title) {
		String place_id = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT place_id from LOCATION_DETAILS where latlng='" + title
						+ "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			place_id = (c1.getString(c1.getColumnIndex("place_id")));
			c1.moveToNext();
		}
		return place_id;

	}

	public String getPlaceDetails_name(String title) {
		String name = null, vicinity = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT name,vicinity from LOCATION_DETAILS where latlng='"
						+ title + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			name = (c1.getString(c1.getColumnIndex("name")));
			vicinity = (c1.getString(c1.getColumnIndex("vicinity")));
			c1.moveToNext();
		}
		return name + vicinity;

	}

	public String getPlaceid_db(String title) {
		String plave_id = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT place_id_db from LOCATION_DETAILS where latlng='"
						+ title + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			plave_id = (c1.getString(c1.getColumnIndex("place_id_db")));
			c1.moveToNext();
		}
		return plave_id;

	}

	public Cursor getAllFriends() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT *FROM FRIEND", null);

	}

	public String getUserid(String username) {
		String user_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT user_id FROM USER where user_name = '"
				+ username + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			user_id = (c1.getString(c1.getColumnIndex("user_id")));
			c1.moveToNext();
		}

		return user_id;

	}

	public String getFriendName(String friend_id) {
		String friend_name = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT friend_name FROM FRIEND where friend_id = '"
						+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			friend_name = (c1.getString(c1.getColumnIndex("friend_name")));
			c1.moveToNext();
		}

		return friend_name;

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public String getComments(String msg_id) {
		String comment = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT comment FROM COMMENTS where msg_id = '"
				+ msg_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			comment = (c1.getString(c1.getColumnIndex("comment")));
			c1.moveToNext();
		}

		return comment;

	}

	// TO CHECK TEAM PLAYER NAME EXISTANCE
	public void insertAllTeamPlayer(String auth_token) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT *FROM MYTEAM", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {

				String selected_friend_ids = (c1.getString(c1
						.getColumnIndex("players")));
				String selected_team_id = (c1.getString(c1
						.getColumnIndex("team_id")));
				String selected_team_name = (c1.getString(c1
						.getColumnIndex("name")));

				if (selected_friend_ids != null
						&& !selected_friend_ids.equalsIgnoreCase("null")) {
					insertTeamPlayerForInviteTeammate(auth_token,
							selected_team_id, selected_team_name,
							selected_friend_ids);
				}

				c1.moveToNext();
			}
		}

	}

	public void insertTeamPlayerForInviteTeammate(String auth_token,
			String selected_team_id, String selected_team_name,
			String selected_friend_ids) {
		ArrayList<String> allAddedPlayerId = new ArrayList<String>();

		selected_friend_ids = selected_friend_ids.trim();
		for (String retval : selected_friend_ids.split(",")) {
			retval = retval.trim();
			allAddedPlayerId.add(retval);
			updateStatusPlayerTeammate(retval);
			System.out.println(retval);
		}

		for (int i1 = 0; i1 < allAddedPlayerId.size(); i1++) {
			String frd_id = allAddedPlayerId.get(i1).toString();
			System.out.println("frd_id" + frd_id);
			frd_id = frd_id.trim();
			String frd_name = getcurrentfriendName(frd_id);

		}

	}

	// UPDATE STATUS TEAM PLAYER
	public void updateStatusTeamPlayerForInviteTeammate(String friendId) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "0";
		Cursor c = db.rawQuery("UPDATE TEAMPLAYER SET status = '" + status
				+ "'" + " where team_player_id = '" + friendId + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<String> getTeammateName(String Game_Id,
			String visibilty_status) {
		ArrayList<String> player_status_pair = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";
		String req_status = "accepted";

		if (visibilty_status.equalsIgnoreCase("public")) {
			Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
					+ " where game_id = '" + Game_Id + "'" + " AND status = '"
					+ status + "'" + " AND request_status = '" + req_status
					+ "'", null);
			System.out.println("c1.getCount()-->" + c1.getCount());
			c1.moveToFirst();
			if (c1.getCount() != 0) {

				for (int i = 0; i < c1.getCount(); i++) {
					String player_name = c1.getString(c1
							.getColumnIndex("player_name"));

					player_status_pair.add(player_name);
					c1.moveToNext();
				}

			}
		} else {
			Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
					+ " where game_id = '" + Game_Id + "'" + " AND status = '"
					+ status + "'", null);
			System.out.println("c1.getCount()-->" + c1.getCount());
			c1.moveToFirst();
			if (c1.getCount() != 0) {

				for (int i = 0; i < c1.getCount(); i++) {
					String player_name = c1.getString(c1
							.getColumnIndex("player_name"));

					player_status_pair.add(player_name);
					c1.moveToNext();
				}

			}

		}
		return player_status_pair;

	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<String> getTeammateNameStatus(String Game_Id,
			String visibilty_status) {
		ArrayList<String> player_status_pair = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";
		String req_status = "accepted";

		if (visibilty_status.equalsIgnoreCase("public")) {
			Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
					+ " where game_id = '" + Game_Id + "'" + " AND status = '"
					+ status + "'" + "AND request_status = '" + req_status
					+ "'", null);
			System.out.println("c1.getCount()-->" + c1.getCount());
			c1.moveToFirst();
			if (c1.getCount() != 0) {

				for (int i = 0; i < c1.getCount(); i++) {

					String player_status = c1.getString(c1
							.getColumnIndex("request_status"));
					player_status_pair.add(player_status);
					c1.moveToNext();
				}

			}
		} else {
			Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
					+ " where game_id = '" + Game_Id + "'" + " AND status = '"
					+ status + "'", null);
			System.out.println("c1.getCount()-->" + c1.getCount());
			c1.moveToFirst();
			if (c1.getCount() != 0) {

				for (int i = 0; i < c1.getCount(); i++) {

					String player_status = c1.getString(c1
							.getColumnIndex("request_status"));
					player_status_pair.add(player_status);
					c1.moveToNext();
				}

			}

		}
		return player_status_pair;

	}

	// UPDATE STATUS TEAM PLAYER
	public void updateStatusTeamPlayerByName(String playerName) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "0";
		Cursor c = db.rawQuery("UPDATE TEAMPLAYER SET status = '" + status
				+ "'" + " where team_player_name = '" + playerName + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// UPDATE STATUS TEAM PLAYER
	public Boolean checkDroplistUserRequestStatus(String playerName,
			String loginuser_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_request_status = false;
		Cursor c1 = db.rawQuery("SELECT *FROM " + TEAMMATE
				+ " where player_name = '" + playerName + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {

				String player_status = c1.getString(c1
						.getColumnIndex("request_status"));

				String invited_by = c1.getString(c1
						.getColumnIndex("invited_by"));

				if (player_status.equalsIgnoreCase("accepted")
						|| player_status.equalsIgnoreCase("creator")) {
					check_request_status = true;
				}
				if (!invited_by.equalsIgnoreCase(loginuser_id)
						&& player_status.equalsIgnoreCase("pending")) {
					check_request_status = true;
				}
				c1.moveToNext();
			}

		}
		return check_request_status;

	}

	public String getProfilePic(String auth_token) {
		String player_status = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT profile_picture from user where auth_id= '"
						+ auth_token + "'", null);
		System.out.println("image uel");
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {

				player_status = c1.getString(c1
						.getColumnIndex("profile_picture"));

				c1.moveToNext();
			}

		}

		return player_status;

	}

	// CHECK TEAM PLAYER EXISTANCE
	public Boolean checkDroplistUserManageTeam(String team_player_name) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_user_exitance = false;
		Cursor c1 = db.rawQuery("SELECT team_player_name FROM " + TEAMPLAYER
				+ " where team_player_name = '" + team_player_name + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_user_exitance = true;
			c1.moveToNext();
		}
		return check_user_exitance;
	}

	// CHECK Game EXISTANCE
	public Boolean checkGameExistance(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_game_exitance = false;
		Cursor c1 = db.rawQuery("SELECT title FROM " + LOCALGAME_LIST
				+ " where game_id = '" + game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_game_exitance = true;
			updateGameStatus(game_id);
			c1.moveToNext();
		}
		return check_game_exitance;
	}

	// UPDATE STATUS & Attending
	public void updateGameStatus(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		boolean status = true;
		String game_status = "accepted";
		System.out.println("UPadte updateGameStatus" + game_id);
		Cursor c = db.rawQuery("UPDATE LOCALGAME_LIST SET gamestatus = '"
				+ game_status + "' , is_attending='" + status
				+ "' where game_id = '" + game_id + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// CHECK TEAMMATE PLAYER EXISTANCE
	public Boolean checkDroplistUserInviteTeammate(String player_name) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_user_exitance = false;
		String status = "1";
		Cursor c1 = db.rawQuery("SELECT player_name FROM " + TEAMMATE
				+ " where player_name = '" + player_name + "'"
				+ "AND status = '" + status + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_user_exitance = true;
			c1.moveToNext();
		}
		return check_user_exitance;
	}

	// CHECK Game_id EXISTANCE
	public Boolean checkGameidExistance(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_gameid_exitance = false;

		Cursor c1 = db.rawQuery(
				"SELECT game_id FROM NEXTSCHEDUEDGAME where game_id = '"
						+ game_id + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_gameid_exitance = true;
			c1.moveToNext();
		}
		return check_gameid_exitance;
	}

	public void updateProfilePicture(String auth_token, String img) {
		System.out.println("updated img");
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c = db.rawQuery("UPDATE USER SET profile_picture = '" + img
				+ "'" + " where auth_id = '" + auth_token + "'", null);
		System.out.println("updated img c.getCount()-->" + c.getCount());
		c.moveToNext();
	}

	public void updateUserDetails(String auth_token, String username) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c = db.rawQuery("UPDATE USER SET user_name = '" + username
				+ "' where auth_id = '" + auth_token + "'", null);
		System.out.println("updated img c.getCount()-->" + c.getCount());
		c.moveToNext();
	}

	public Cursor getAllNestScheduledGames(String limit) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery(
				"SELECT * FROM NEXTSCHEDUEDGAME  ORDER BY datetime ASC LIMIT '"
						+ limit + "'", null);
	}

	public Cursor getMostResentScheduledGames() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		return db.rawQuery(
				"SELECT * FROM NEXTSCHEDUEDGAME ORDER BY datetime ASC limit 1",
				null);
	}

	public Cursor getGameDetails() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		return db.rawQuery("SELECT * FROM  GAME_DETAIL", null);
	}

	public Cursor getPlyerName() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM GAMEPLAYER", null);
	}

	public Cursor getPlayerName() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM TEAMMATE", null);

	}

	public boolean gameidExistance(String game_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery(
				"SELECT  game_id FROM GAMEPLAYER where game_id='" + game_id
						+ "'", null);

		if (c.getCount() != 0) {
			return true;
		} else {
			return false;
		}

	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<String> getTeammateName1(String Game_Id,
			String loginuser_Id) {
		ArrayList<String> player_status_pair = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";

		Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
				+ " where game_id = '" + Game_Id + "'" + " AND status = '"
				+ status + "'", null);
		System.out.println("c1.getCount()-->" + c1.getCount());
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				String player_status = c1.getString(c1
						.getColumnIndex("request_status"));
				if (player_status.equalsIgnoreCase("accepted")
						|| player_status.equalsIgnoreCase("Pending")
						|| player_status.equalsIgnoreCase("creator")) {
					String player_name = c1.getString(c1
							.getColumnIndex("player_name"));

					String invited_by = c1.getString(c1
							.getColumnIndex("invited_by"));
					String connection_status = c1.getString(c1
							.getColumnIndex("connection_status"));
					player_status_pair.add(player_name);
				}

				c1.moveToNext();
			}

		}
		Collections.reverse(player_status_pair);
		return player_status_pair;

	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<String> getTeammateNameStatus1(String Game_Id,
			String loginuser_Id) {
		ArrayList<String> player_status_pair = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";
		Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
				+ " where game_id = '" + Game_Id + "'" + " AND status = '"
				+ status + "'", null);
		System.out.println("c1.getCount()-->" + c1.getCount());
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				String player_status = c1.getString(c1
						.getColumnIndex("request_status"));
				if (player_status.equalsIgnoreCase("accepted")
						|| player_status.equalsIgnoreCase("Pending")
						|| player_status.equalsIgnoreCase("creator")) {
					String player_name = c1.getString(c1
							.getColumnIndex("player_name"));

					String invited_by = c1.getString(c1
							.getColumnIndex("invited_by"));
					String connection_status = c1.getString(c1
							.getColumnIndex("connection_status"));
					player_status_pair.add(player_status);
				}
				c1.moveToNext();
			}

		}
		Collections.reverse(player_status_pair);
		return player_status_pair;

	}

	// UPDATE STATUS FRIEND
	public void updateStatusPlayerTeammate(String FriendId) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";
		Cursor c = db.rawQuery("UPDATE FRIEND SET inviteTeammate_Status = '"
				+ status + "'" + " where friend_id = '" + FriendId + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// UPDATE STATUS FRIEND
	public void updateStatusPlayerTeammate1(String FriendId) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "0";
		Cursor c = db.rawQuery("UPDATE FRIEND SET inviteTeammate_Status = '"
				+ status + "'" + " where friend_id = '" + FriendId + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());

	}

	// GETTING Team Or Fiend
	public String getTeamOrFriend(String playername) {
		String selected_name = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT teamorfriend FROM TEAMMATE where player_name = '"
						+ playername + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			selected_name = (c1.getString(c1.getColumnIndex("teamorfriend")));
			c1.moveToNext();
		}
		System.out.println(" selected_frdId FROM DB" + selected_name);
		return selected_name;

	}

	// GETTING Invitation Request id
	public String getRequestid(String game_id) {
		String selected_requestid = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT request_id FROM PENDINGREQUEST where game_id = '"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			selected_requestid = (c1.getString(c1.getColumnIndex("request_id")));
			c1.moveToNext();
		}
		return selected_requestid;
	}

	// GETTING Team Or Friend
	public boolean getTeamNameExistance(String teamname) {
		boolean team_existance = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT  name  FROM MYTEAM where name = '"
				+ teamname + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			team_existance = true;
			// c1.moveToNext();
		}
		System.out.println(" team_existance FROM DB" + team_existance);
		return team_existance;

	}

	// GETTING Team Or Friend
	public String getFriendInviteTeamStatus(String friendname) {
		String exitance_frdname = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT inviteTeammate_Status FROM FRIEND where friend_name = '"
						+ friendname + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			exitance_frdname = (c1.getString(c1
					.getColumnIndex("inviteTeammate_Status")));
			c1.moveToNext();
		}
		System.out.println(" exitance_frdname FROM DB" + exitance_frdname);
		return exitance_frdname;
	}

	// UPDATE TEAM PLAYER STATUS
	public void updateStatusTeamPlayerByName1(String playerName) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String status = "1";
		Cursor c = db.rawQuery("UPDATE TEAMPLAYER SET status = '" + status
				+ "'" + " where team_player_name = '" + playerName + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount()*****************" + c.getCount());
	}

	// Deleting cancelled Game from NEXTSCHEDUEDGAME Table
	public void deletecancellGameFromUpcominggames(String game_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM NEXTSCHEDUEDGAME WHERE game_id='" + game_id
				+ "'");
	}

	// Removing the team from MYTEAM Table
	public void deleteMyTeam(String team_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM MYTEAM WHERE team_id='" + team_id + "'");

	}

	// Deleting cancelled Game from Allscheduledgames Table
	public void deletecancellGameFromAllscheduledgames(String game_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM ALLSCHEDUEDGAME WHERE game_id='" + game_id
				+ "'");
	}

	// GETTING TEAM ID FROM MYTEAM TABLE BY TEAM NAME
	public String getInvitationInfo() {
		String selected_game_id = null, selected_request_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM INVITATION_INFO", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				selected_game_id = (c1.getString(c1.getColumnIndex("game_id")));
				selected_request_id = (c1.getString(c1
						.getColumnIndex("request_id")));
				c1.moveToNext();
			}

		}
		System.out.println(" selected_teamId FROM DB----" + selected_game_id
				+ "," + selected_request_id);
		return selected_game_id + "," + selected_request_id;

	}

	// To Check Friend name Existance
	public Boolean toCheckFriendNameExistance(String username) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean selected_friendName = false;
		Cursor c = null;
		try {
			c = db.rawQuery(
					"select friend_name from FRIEND where friend_name = ?",
					new String[] { String.valueOf(username) });
			c.moveToFirst();
			if (c.getCount() != 0) {
				selected_friendName = true;
				c.moveToNext();
			}

			System.out.println("selected_TeamName FROM DB"
					+ selected_friendName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return selected_friendName;
	}

	// GETTING Team Or Friend
	public ArrayList<String> getAllNextScheduledGametitles() {
		ArrayList<String> game_title = new ArrayList<String>();
		ArrayList<String> game_ids = new ArrayList<String>();

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT title FROM NEXTSCHEDUEDGAME", null);
		c1.moveToFirst();
		System.out.println("title count izzzzzz" + c1.getCount());
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				String gametitle = (c1.getString(c1.getColumnIndex("title")));
				System.out.println("gametitle izzzzz" + gametitle);
				game_title.add(gametitle);
				c1.moveToNext();
			}

		}
		System.out.println(" exitance_frdname FROM DB" + game_title);
		return game_title;
	}

	// GETTING Team Or Friend
	public String getGameid(String gametitle) {
		String game_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT game_id FROM NEXTSCHEDUEDGAME where title = '"
						+ gametitle + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			game_id = (c1.getString(c1.getColumnIndex("game_id")));
			c1.moveToNext();
		}
		System.out.println(" exitance_frdname FROM DB" + game_id);
		return game_id;
	}

	// Getting the user selected radius in the MAP
	public String getUserRadius(String username) {
		String radius = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT radius FROM GAMERADIUS where user_name='" + username
						+ "' ", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			radius = c1.getString(c1.getColumnIndex("radius"));
			c1.moveToNext();
		}
		return radius;
	}

	// Deleting all the game radius data
	public void deleteGameRadiusData(String username) {

		String user = username;
		System.out.println("Username izzz" + user);
		db.execSQL(
				"delete *FROM GAMERADIUS where user_name='" + username + "'",
				null);

	}

	// TO CHECK TEAM EXISTANCE
	public Boolean toCheckUserNameExistance(String username) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean selected_username = false;
		Cursor c = null;
		try {
			c = db.rawQuery(
					"select user_name from GAMERADIUS where user_name = ?",
					new String[] { String.valueOf(username) });
			c.moveToFirst();
			if (c.getCount() != 0) {

				selected_username = true;
				c.moveToNext();
			}

			System.out.println("selected_TeamName FROM DB" + selected_username);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return selected_username;
	}

	// GETTING Team Or Friend
	public String getPushNotificationStatus(String username) {
		String plyer_request = null, game_invitation = null, nearby_games = null, message_board = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT * FROM PUSHNOTIFICATIONSTATUS where user_name = '"
						+ username + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			plyer_request = (c1.getString(c1.getColumnIndex("player_request")));
			game_invitation = (c1.getString(c1
					.getColumnIndex("game_invitation")));
			nearby_games = (c1.getString(c1.getColumnIndex("nearby_games")));
			message_board = (c1.getString(c1.getColumnIndex("message_board")));
			c1.moveToNext();
		}

		return plyer_request + "#" + game_invitation + "#" + nearby_games + "#"
				+ message_board;
	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updatePushStatus_playerrequest(String username,
			String update_val) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c = db.rawQuery(
				"UPDATE PUSHNOTIFICATIONSTATUS SET player_request = '"
						+ update_val + "'" + " where user_name = '" + username
						+ "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount() UPDATE*****************"
				+ c.getCount());

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updatePushStatus_gameinvitation(String username,
			String update_val) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c = db.rawQuery(
				"UPDATE PUSHNOTIFICATIONSTATUS SET game_invitation = '"
						+ update_val + "'" + " where user_name = '" + username
						+ "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount() UPDATE*****************"
				+ c.getCount());

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updatePushStatus_nearbygames(String username, String update_val) {
		db.execSQL("UPDATE PUSHNOTIFICATIONSTATUS SET nearby_games = '"
				+ update_val + "'" + " where user_name = '" + username + "'");

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updatePushStatus_message(String username, String update_val) {
		db.execSQL("UPDATE PUSHNOTIFICATIONSTATUS SET message_board = '"
				+ update_val + "'" + " where user_name = '" + username + "'");
	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updateRadius(String username, String update_val) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c = db.rawQuery("UPDATE GAMERADIUS SET radius = '" + update_val
				+ "'" + " where user_name = '" + username + "'", null);
		c.moveToNext();
		System.out.println("cursor.getCount() UPDATE*****************"
				+ c.getCount());

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updateAllstatus(String username, String player_request) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery(
				"UPDATE PUSHNOTIFICATIONSTATUS SET player_request = '"
						+ player_request + "'" + " where user_name = '"
						+ username + "'", null);
		c.moveToNext();

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updateGameInvitation(String username, String player_request) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery(
				"UPDATE PUSHNOTIFICATIONSTATUS SET game_invitation = '"
						+ player_request + "'" + " where user_name = '"
						+ username + "'", null);
		c.moveToNext();

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updatenearbygames(String username, String player_request) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery(
				"UPDATE PUSHNOTIFICATIONSTATUS SET nearby_games = '"
						+ player_request + "'" + " where user_name = '"
						+ username + "'", null);
		c.moveToNext();

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updatemessageboard(String username, String player_request) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery(
				"UPDATE PUSHNOTIFICATIONSTATUS SET message_board = '"
						+ player_request + "'" + " where user_name = '"
						+ username + "'", null);
		c.moveToNext();

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updateNearByGamesOption(String username, String option) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery(
				"UPDATE PUSHNOTIFICATIONSTATUS SET nearby_games_option = '"
						+ option + "'" + " where user_name = '" + username
						+ "'", null);
		c.moveToNext();

	}

	public void deleteUserpushDate(Context context) {
		db.execSQL("delete FROM " + PUSHNOTIFICATIONSTATUS);

	}

	public void deleteActivityFeeds() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + " " + ACTIVITY_FEEDS);
	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public ArrayList<String> getUserProfileImage() {
		String image = null, current_status = null, username = null, user_id = null, firstname = null, lastname = null;
		ArrayList<String> user_info = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db
				.rawQuery(
						"SELECT profile_picture,current_status,user_name,user_id,firstname,lastname FROM USER",
						null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			image = (c1.getString(c1.getColumnIndex("profile_picture")));
			current_status = (c1.getString(c1.getColumnIndex("current_status")));
			username = (c1.getString(c1.getColumnIndex("user_name")));
			user_id = (c1.getString(c1.getColumnIndex("user_id")));
			firstname = (c1.getString(c1.getColumnIndex("firstname")));
			lastname = (c1.getString(c1.getColumnIndex("lastname")));
			user_info.add(image);
			user_info.add(current_status);
			user_info.add(username);
			user_info.add(user_id);
			user_info.add(firstname);
			user_info.add(lastname);
			c1.moveToNext();
		}

		return user_info;

	}

	public Cursor getAllFriends(String sport) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM FRIEND where sport = '" + sport
				+ "'", null);

		return c1;

	}

	public Cursor getSelectedFriend(String sport) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM FRIEND where sport = '" + sport
				+ "'", null);

		return c1;

	}

	public Cursor getcurrentsportfrienlistRoster(String SportName,
			String auth_token) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		return db.rawQuery("SELECT * FROM FRIEND", null);
	}

	public void dropGameDetails() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM" + " " + GAME_DETAIL);
	}

	public void deletePlayerinfo() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM" + " " + TEAMMATE);
	}

	public void deleteAllTeammates() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM" + " " + GAMEPLAYER);
	}

	public void deleteinvitationinfo() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM" + " " + INVITATION_INFO);
	}

	public Cursor getAllSport() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		return db.rawQuery("SELECT *  FROM SPORT", null);

	}

	public void deleteAllLocationDetails() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM" + " " + LOCATION_DETAILS);
	}

	public String getUserName(String user_id) {
		String radius = null;
		ArrayList<String> friend_name = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT friend_name FROM FRIEND where friend_id='" + user_id
						+ "' ", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			radius = c1.getString(c1.getColumnIndex("friend_name"));
			friend_name.add(radius);
			c1.moveToNext();
		}
		return radius;
	}

	public String getNonFriendUserName(String user_id) {
		String user_name = null;
		ArrayList<String> friend_name = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT username FROM NONFRIEND_CHAT_DETAILS where user_id='"
						+ user_id + "' ", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			user_name = c1.getString(c1.getColumnIndex("username"));
			c1.moveToNext();
		}
		return user_name;
	}

	public ArrayList<String> getAllstarttimeanddatefromDB(String date) {
		String game_id = null;
		String start_time = null, end_time = null;
		ArrayList<String> games = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM NEXTSCHEDUEDGAME where date ='"
				+ date + "' ", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			game_id = c1.getString(c1.getColumnIndex("game_id"));
			start_time = c1.getString(c1.getColumnIndex("starttime"));
			end_time = c1.getString(c1.getColumnIndex("endtime"));
			games.add(game_id + "#" + start_time + "#" + end_time);
			c1.moveToNext();
		}

		return games;

	}

	public String getfriend_id(String user_name) {
		String radius = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT friend_id FROM FRIEND where friend_name='" + user_name
						+ "' ", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			radius = c1.getString(c1.getColumnIndex("friend_id"));

			c1.moveToNext();
		}
		return radius;
	}

	public String getGameCreator(String Game_Id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String player_name = null;

		Cursor c1 = db.rawQuery("SELECT * FROM " + GAME_DETAIL
				+ " where game_id = '" + Game_Id + "'", null);
		System.out.println("c1.getCount()-->" + c1.getCount());
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				player_name = c1.getString(c1.getColumnIndex("created_by"));
				c1.moveToNext();
			}

		}

		return player_name;

	}

	public String getGameCreator_GameDetails(String Game_Id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String player_name = null;

		Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMMATE
				+ " where game_id = '" + Game_Id + "'", null);
		System.out.println("c1.getCount()-->" + c1.getCount());
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				String player_status = c1.getString(c1
						.getColumnIndex("request_status"));
				if (player_status.equalsIgnoreCase("creator")) {
					player_name = c1
							.getString(c1.getColumnIndex("player_name"));

				}

				c1.moveToNext();
			}

		}

		return player_name;

	}

	// GETTING Team Or Friend
	public String getGameTItle(String game_id) {
		String game_title = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT game_title FROM GAME_DETAIL where game_id = '"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			game_id = (c1.getString(c1.getColumnIndex("game_title")));
			c1.moveToNext();
		}

		return game_title;
	}

	/*
	 * public String getGameCreator(String game_id) { SQLiteDatabase db =
	 * this.dBHelper.getReadableDatabase();
	 * 
	 * Cursor c1 = db.rawQuery(
	 * "SELECT name,sport_id,custom_sport_name,team_id FROM MYTEAM where sport_id ='"
	 * + sportId + "'" + "AND auth_id = '" + auth_token + "'", null);
	 * c1.moveToFirst(); if (c1.getCount() != 0) { game_id =
	 * (c1.getString(c1.getColumnIndex("game_title"))); c1.moveToNext(); }
	 * 
	 * return game_title; }
	 */

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public String getInvitedFriendID(String FriendName) {
		String selected_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT player_id FROM " + TEAMMATE
				+ " where player_name = '" + FriendName + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				selected_id = (c1.getString(c1.getColumnIndex("player_id")));
				c1.moveToNext();
			}

		}
		System.out.println(" selected_frdId FROM DB" + selected_id);
		return selected_id;

	}

	public boolean updateGamestatus(String game_id, String status) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE GAME_DETAIL SET gamestatus = '" + status
				+ "'" + " where game_id = '" + game_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updateGamePlayerCount(String game_id, int player_count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE NEXTSCHEDUEDGAME SET player_count = '"
				+ player_count + "'" + " where game_id = '" + game_id + "'",
				null);
		c.moveToNext();

		return true;
	}

	public boolean updateLocalGamePlayerCount(String game_id, int player_count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE LOCALGAME_LIST SET player_count = '"
				+ player_count + "'" + " where game_id = '" + game_id + "'",
				null);
		c.moveToNext();

		return true;
	}

	public boolean updateGamePlayerCountGamedetails(String game_id,
			int player_count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE GAME_DETAIL SET player_count = '"
				+ player_count + "'" + " where game_id = '" + game_id + "'",
				null);
		c.moveToNext();

		return true;
	}

	public boolean updateGamestatus_afterwithdraw(String game_id, String status) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE GAME_DETAIL SET gamestatus = '" + status
				+ "'" + " where game_id = '" + game_id + "'", null);
		c.moveToNext();

		return true;
	}

	// Deleting cancelled Game from NEXTSCHEDUEDGAME Table
	public void deleteGameplayer(String playername) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM TEAMMATE WHERE player_name='" + playername
				+ "'");

	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public String getRequest_id(String game_id) {
		String request_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT request_id FROM " + PENDINGREQUEST
				+ " where game_id = '" + game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				request_id = (c1.getString(c1.getColumnIndex("request_id")));
				c1.moveToNext();
			}

		}

		return request_id;

	}

	public Cursor getCountDetails() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db.rawQuery("SELECT * FROM COUNTS", null);

	}

	public Cursor getGameHistory() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		return db
				.rawQuery("SELECT * FROM GAMEHISTORY ORDER BY date DESC", null);

	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public int getPrifileExistance() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT *  FROM USERPROFILE", null);

		int count = c1.getCount();

		return count;

	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public Cursor getProfileData() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT *  FROM USERPROFILE", null);

		return c1;

	}

	public boolean updateFirstname(String user_id, String firstname) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USERPROFILE SET name = '" + firstname
				+ "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updateEmail(String user_id, String email) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USERPROFILE SET user_email = '" + email
				+ "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updatePhone(String user_id, String phone) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USERPROFILE SET phone_number = '"
				+ phone + "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updatePhoneNumber(String user_id, String phone) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USER SET phone_number = '" + phone + "'"
				+ " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updatAboutme(String user_id, String aboutme) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USERPROFILE SET user_aboutme = '"
				+ aboutme + "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updateProfilepic(String user_id, String image) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USERPROFILE SET user_profileimage = '"
				+ image + "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updateProfilepicture(String user_id, String image) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USER SET profile_picture = '" + image
				+ "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updateProfilepicinSignup(String user_id, String image) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USER SET profile_picture = '" + image
				+ "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public String getUserProfileImage(String user_id) {
		String profile_image = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT user_profileimage FROM " + USERPROFILE
				+ " where user_id = '" + user_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				profile_image = (c1.getString(c1
						.getColumnIndex("user_profileimage")));
				c1.moveToNext();
			}

		}

		return profile_image;

	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public String getPrimarySport(String user_id) {
		String sport = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT sport FROM " + CURRENT_USER
				+ " where user_id = '" + user_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				sport = (c1.getString(c1.getColumnIndex("sport")));
				c1.moveToNext();
			}

		}
		return sport;

	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public int getSkilllevelcount() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT *  FROM SKILL_LEVEL", null);

		int count = c1.getCount();
		return count;

	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public Cursor getSkilllevelData() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT *  FROM SKILL_LEVEL", null);

		return c1;

	}

	public boolean updatePrimarySport(String user_id, String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USER SET sport = '" + sport + "'"
				+ " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updateUserProfilePrimarySport(String user_id, String sport) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USERPROFILE SET user_sport = '" + sport
				+ "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updatPrimarysportskilllevel(String user_id, String skilllevel) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db
				.rawQuery("UPDATE USERPROFILE SET expertlevel = '" + skilllevel
						+ "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updatRewardpoints(String user_id, int reward_point) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USERPROFILE SET rewards = '"
				+ reward_point + "'" + " where user_id = '" + user_id + "'",
				null);
		c.moveToNext();

		return true;
	}

	public boolean updateAboutMe(String user_id, String aboutme) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USERPROFILE SET user_aboutme = '"
				+ aboutme + "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public int getHistoryCount() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM GAMEHISTORY", null);

		int get_count = c1.getCount();

		return get_count;

	}

	// CHECK Game_id EXISTANCE
	public String getGame_id(String title) {
		String game_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT game_id FROM NEXTSCHEDUEDGAME where title = '" + title
						+ "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			game_id = (c1.getString(c1.getColumnIndex("game_id")));
			c1.moveToNext();
		}
		return game_id;
	}

	public ArrayList<String> getPlacereferance(String title) {
		String place_referance = null, place_photo_referance = null;
		ArrayList<String> place_image_referance = new ArrayList<String>();

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT referance,photo_referance from LOCATION_DETAILS where latlng ='"
						+ title + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			place_referance = (c1.getString(c1.getColumnIndex("referance")));
			place_image_referance.add(place_referance);
			place_photo_referance = (c1.getString(c1
					.getColumnIndex("photo_referance")));
			place_image_referance.add(place_photo_referance);
			c1.moveToNext();
		}
		return place_image_referance;

	}

	public boolean updateFriendRequestCount(String username, int count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE COUNTS SET player_request_count = '"
				+ count + "'" + " where user_name = '" + username + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updateGameInvitation(String username, int count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE COUNTS SET game_invitation_count = '"
				+ count + "'" + " where user_name = '" + username + "'", null);
		c.moveToNext();

		return true;
	}


	public String getGameCreatorImage(String req_status) {
		String profile_pic = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT profile_image FROM TEAMMATE where request_status = '"
						+ req_status + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			profile_pic = (c1.getString(c1.getColumnIndex("image_url")));
			c1.moveToNext();
		}
		return profile_pic;
	}

	public int getFriendsCount() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM FRIEND", null);

		int get_count = c1.getCount();

		return get_count;

	}

	public String getProfilePicture(String user_id) {

		String profile_pic = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT profile_picture FROM USER where user_id = '" + user_id
						+ "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			profile_pic = (c1.getString(c1.getColumnIndex("profile_picture")));
			c1.moveToNext();
		}
		return profile_pic;
	}

	public boolean updateGameOwner(String userid, String creator) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE TEAMMATE SET request_status = '"
				+ creator + "'" + " where player_id = '" + userid + "'", null);
		c.moveToNext();

		return true;
	}

	public String getNewCreatorName(String player_id) {

		String player_name = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT player_name FROM TEAMMATE where player_id = '"
						+ player_id + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			player_name = (c1.getString(c1.getColumnIndex("player_name")));
			c1.moveToNext();
		}
		return player_name;
	}

	public boolean updateNewOwner(String game_id, String created_by) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db
				.rawQuery("UPDATE GAME_DETAIL SET created_by = '" + created_by
						+ "'" + " where game_id = '" + game_id + "'", null);
		c.moveToNext();

		return true;
	}

	// Get Next Scheduled game count
	public int getNextScheduledGameCount() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM NEXTSCHEDUEDGAME", null);

		int get_count = c1.getCount();

		return get_count;
	}

	// Get Local games count
	public int getLocalGamesCount() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM LOCALGAME_LIST", null);

		int get_count = c1.getCount();

		return get_count;

	}

	public Cursor getUpcomingGameDetails(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT * FROM NEXTSCHEDUEDGAME where game_id = '" + game_id
						+ "'", null);

		return c1;
	}

	public Cursor getPastGameDetails(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT * FROM ALLSCHEDUEDGAME where game_id = '" + game_id
						+ "'", null);

		return c1;
	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public Cursor getGameDetails(String game_id) {
		String sport = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM " + LOCALGAME_LIST
				+ " where game_id = '" + game_id + "'", null);

		return c1;

	}

	public String getGameManagerImage(String req_status) {
		String profile_pic = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT image_url FROM TEAMMATE where request_status = '"
						+ req_status + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			profile_pic = (c1.getString(c1.getColumnIndex("image_url")));
			c1.moveToNext();
		}
		return profile_pic;
	}

	public String getGameCreatorName(String req_status) {
		String profile_pic = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT player_name FROM TEAMMATE where request_status = '"
						+ req_status + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			profile_pic = (c1.getString(c1.getColumnIndex("player_name")));
			c1.moveToNext();
		}
		return profile_pic;
	}

	// CHECK Game EXISTANCE in the LOCALGAME_LIST
	public Boolean checkLocalGameExistance(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_game_exitance = false;
		Cursor c1 = db.rawQuery("SELECT game_id FROM " + LOCALGAME_LIST
				+ " where game_id = '" + game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_game_exitance = true;
			c1.moveToNext();
		}
		return check_game_exitance;
	}

	// Getting the row count of the Game invitation table
	public int getInvitationRowCount() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM INVITATION_INFO", null);

		int row_count = c1.getCount();

		return row_count;
	}

	// Deleting cancelled Game from NEXTSCHEDUEDGAME Table
	public void deleteGameinvitation(String game_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM INVITATION_INFO WHERE game_id ='" + game_id
				+ "'");

	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public int getGameDetailscount(String game_id) {
		String sport = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM " + LOCALGAME_LIST
				+ " where game_id = '" + game_id + "'", null);

		return c1.getCount();

	}

	// CHECK Game EXISTANCE in the LOCALGAME_LIST
	public Boolean checkLocalGameinNextScheduledgame(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_game_exitance = false;
		Cursor c1 = db.rawQuery("SELECT game_id FROM " + NEXTSCHEDUEDGAME
				+ " where game_id = '" + game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_game_exitance = true;
			c1.moveToNext();
		}
		return check_game_exitance;
	}

	public Cursor getSkillelevleData() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM SKILL_LEVEL", null);
		return c1;
	}

	// Deleting the game from the local game table when the game id matches
	public void deleteLocalgame(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM LOCALGAME_LIST WHERE game_id='" + game_id + "'");

	}

	/*
	 * INSERTING DATA INTO CHECKIN_GAME TABLE
	 */
	public boolean insertChechkinGameId(String game_id) {

		System.out.println("ENTER DATA INTO CHECKIN_GAME TABLE");
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("game_id", game_id);
		db.insert("CHECKIN_GAME", null, contentValues);
		return true;
	}

	// GETTING SUCCESSFULL CHECKIN GAMEID FROM CHECKIN_GAME TABLE
	public ArrayList<String> getChechkinGameId() {
		ArrayList<String> gameId_list = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM " + CHECKIN_GAME, null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				String game_id = c1.getString(c1.getColumnIndex("game_id"));
				gameId_list.add(game_id);

				c1.moveToNext();
			}

		}

		return gameId_list;

	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public String getUserEmail(String username) {
		String email = null;
		ArrayList<String> user_info = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT email FROM USER where user_name = '"
				+ username + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			email = (c1.getString(c1.getColumnIndex("email")));

			c1.moveToNext();
		}

		return email;

	}

	public String getLatlng(String place_id) {
		String latitude = null, longitude = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT latitude,longitude FROM LOCATION_DETAILS where latlng = '"
						+ place_id + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			latitude = (c1.getString(c1.getColumnIndex("latitude")));
			longitude = (c1.getString(c1.getColumnIndex("longitude")));

			c1.moveToNext();
		}
		return latitude + " " + longitude;
	}

	// CHECK Game_id EXISTANCE
	public String getGCMID(String device_token) {
		String gcm_registration_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT registration_id FROM GCM_ID where device_token = '"
						+ device_token + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			gcm_registration_id = (c1.getString(c1
					.getColumnIndex("registration_id")));
			c1.moveToNext();
		}
		return gcm_registration_id;
	}

	// Getting friend sport by giving friend id
	public String getFriendId(String friend_name) {
		String friend_id = "null";
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT friend_id FROM FRIEND where friend_name = '"
						+ friend_name + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			friend_id = (c1.getString(c1.getColumnIndex("friend_id")));
			c1.moveToNext();
		}

		return friend_id;

	}

	// Getting friend sport by giving friend id
	public String getFriendSport(String friend_id) {
		String friend_sport = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT sport FROM FRIEND where friend_id = '"
				+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			friend_sport = (c1.getString(c1.getColumnIndex("sport")));
			c1.moveToNext();
		}

		return friend_sport;

	}

	// getting game creator name sport from USER table

	public String getCreatorSport(String creator_name) {
		String creator_sport = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT sport FROM USER where user_name = '"
				+ creator_name + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			creator_sport = (c1.getString(c1.getColumnIndex("sport")));
			c1.moveToNext();
		}

		return creator_sport;

	}

	public String getMyPhoneNumber() {
		String myphone_number = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT phone_number FROM USER", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			myphone_number = (c1.getString(c1.getColumnIndex("phone_number")));
			c1.moveToNext();
		}

		return myphone_number;

	}

	/*
	 * Get all Pending FriendInvites
	 */
	public Cursor getAllPendingFriendinvites() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		return db.rawQuery("SELECT * FROM PENDINGFRIEND_INVITES", null);

	}

	/*
	 * Get the pending friend invitation count
	 */

	public int getAllFriendRequestCount() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM PENDINGFRIEND_INVITES", null);
		return c1.getCount();
	}

	/*
	 * Delete all pending invitationss
	 */

	public void deletePendingInvitationeData(Context context) {
		db.execSQL("delete FROM " + PENDINGFRIEND_INVITES);

	}

	// Deleting the game from the local game table when the game id matches
	public void deleteFriendinvite(String user_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM PENDINGFRIEND_INVITES WHERE user_id='"
				+ user_id + "'");

	}

	// CHECK Game EXISTANCE in the LOCALGAME_LIST
	public int GetMessageCount_NextGame(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		int msg_exitance = 0;
		Cursor c1 = db.rawQuery("SELECT message_count FROM " + NEXTSCHEDUEDGAME
				+ " where game_id = '" + game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			msg_exitance = (c1.getInt(c1.getColumnIndex("message_count")));
			c1.moveToNext();
		}
		return msg_exitance;
	}

	public boolean updateGameMessageCount(String game_id, int player_count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE NEXTSCHEDUEDGAME SET message_count = '"
				+ player_count + "'" + " where game_id = '" + game_id + "'",
				null);
		c.moveToNext();

		return true;
	}

	public boolean updateGameMessageCountGamedetails(String game_id,
			int player_count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE GAME_DETAIL SET message_count = '"
				+ player_count + "'" + " where game_id = '" + game_id + "'",
				null);
		c.moveToNext();

		return true;
	}

	// CHECK Game EXISTANCE in the LOCALGAME_LIST
	public int GetMessageCount_Localgames(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		int msg_exitance = 0;
		Cursor c1 = db.rawQuery("SELECT message_count FROM " + LOCALGAME_LIST
				+ " where game_id = '" + game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			msg_exitance = (c1.getInt(c1.getColumnIndex("message_count")));
			c1.moveToNext();
		}
		return msg_exitance;
	}

	public boolean updateGameMessageCount_local(String game_id, int player_count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("UPDATE LOCALGAME_LIST SET message_count = '" + player_count
				+ "'" + " where game_id = '" + game_id + "'");

		return true;
	}

	// db.execSQL("delete FROM " + ACTIVITY_FEEDS);

	// CHECK Game EXISTANCE in the LOCALGAME_LIST
	public int GetMessageCount_Gamedetails(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		int msg_exitance = 0;
		Cursor c1 = db.rawQuery("SELECT message_count FROM " + GAME_DETAIL
				+ " where game_id = '" + game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			msg_exitance = (c1.getInt(c1.getColumnIndex("message_count")));
			c1.moveToNext();
		}
		return msg_exitance;
	}

	public boolean updateLocalGameplayer_count(String game_id,
			String player_count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE LOCALGAME_LIST SET player_count = '"
				+ player_count + "'" + " where game_id = '" + game_id + "'",
				null);
		c.moveToNext();

		return true;
	}

	// CHECK Game Created by in the LOCALGAME_LIST
	public String getCreatedby_Localgames(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String get_creator = null;
		Cursor c1 = db.rawQuery("SELECT created_by FROM " + LOCALGAME_LIST
				+ " where game_id = '" + game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			get_creator = (c1.getString(c1.getColumnIndex("created_by")));
			c1.moveToNext();
		}
		return get_creator;
	}

	/*
	 * Insert all message count of particular GAME ID
	 */
	public boolean insertMessageCount(String game_id, String message_count) {

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("game_id", game_id);
		contentValues.put("msg_count", message_count);
		db.insert("MESSAGE_COUNT", null, contentValues);
		return true;
	}

	/*
	 * First Check the game Id exitance .....if game id is already there then
	 * update the msg count else insert new row
	 */
	public void checkGameIdExitance(String game_id, String message_count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT game_id FROM MESSAGE_COUNT where game_id = '" + game_id
						+ "'", null);
		if (c1.getCount() != 0) {
			Cursor c = db.rawQuery("UPDATE MESSAGE_COUNT SET msg_count = '"
					+ message_count + "'" + " where game_id = '" + game_id
					+ "'", null);
			c.moveToNext();
		} else {
			insertMessageCount(game_id, message_count);

		}

	}

	/*
	 * Get the message count of particular GAME ID
	 */
	public String getGameMessageCount(String game_id) {
		String message_count = "0";
		Cursor c1 = db.rawQuery(
				"SELECT msg_count FROM MESSAGE_COUNT where game_id = '"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			message_count = (c1.getString(c1.getColumnIndex("msg_count")));
			c1.moveToNext();
		}
		return message_count;
	}

	// Deleting cancelled Game from NEXTSCHEDUEDGAME Table
	public void deletecancellGameFromLocalGamess(String game_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM LOCALGAME_LIST WHERE game_id='" + game_id + "'");
	}

	// UPDATE inviteTeammate_Status FRIEND
	public void deletePlayeerInvitation_status(String player_name) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c;

		db.execSQL("delete FROM TEAMMATE WHERE player_name='" + player_name
				+ "'");

	}

	/*
	 * Get the pending friend invitation count
	 */

	public int getNearBygamesCount(String date1, String date2) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM NEARBY_GAMES where date = '"
				+ date1 + "' ", null);

		Cursor c2 = db.rawQuery("SELECT * FROM NEARBY_GAMES where date = '"
				+ date2 + "' ", null);

		return c1.getCount() + c2.getCount();
	}

	// UPDATE inviteTeammate_Status FRIEND
	public void deleteNearbyGames(String date1, String date2) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM NEARBY_GAMES WHERE date='" + date1
				+ "' OR date='" + date2 + "'");

	}

	public ArrayList<String> getNextScheduledgames_Count() {
		String game_id = null;
		ArrayList<String> game_id_array = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM NEXTSCHEDUEDGAME", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				game_id = (c1.getString(c1.getColumnIndex("game_id")));
				game_id_array.add(game_id);
				c1.moveToNext();
			}

		}
		return game_id_array;
	}

	// GET PLAYER COUNT OF A GAME
	public int getGamePlayerCount(String game_id) {
		int count = 0;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT player_count FROM NEXTSCHEDUEDGAME where game_id = '"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			count = (c1.getInt(c1.getColumnIndex("player_count")));
			c1.moveToNext();
		}

		return count;

	}

	public void deleteBlockedfriend(String user_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM BLOCK_FRIEND WHERE user_id='" + user_id + "'");
	}

	public String getFriendDetails(String user_id) {

		String name = null, sport = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM FRIEND WHERE friend_id='"
				+ user_id + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			name = (c1.getString(c1.getColumnIndex("friend_name")));
			sport = (c1.getString(c1.getColumnIndex("sport")));
			c1.moveToNext();
		}

		return name + "#" + sport;

	}

	// TO CHECK Recurring games
	public Boolean toCheckRecurringGamesExistance(String game_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean is_recurring = false;
		Cursor c = null;
		try {
			c = db.rawQuery(
					"select game_id from RECURRING_GAMES where game_id = ?",
					new String[] { String.valueOf(game_id) });
			c.moveToFirst();
			if (c.getCount() != 0) {

				is_recurring = true;
				c.moveToNext();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return is_recurring;
	}

	// TO CHECK Recurring games
	public String getRecurringGamesType(String game_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String recurring_type = null;
		Cursor c = null;
		try {
			c = db.rawQuery(
					"select recurring_type from RECURRING_GAMES where game_id = ?",
					new String[] { String.valueOf(game_id) });
			c.moveToFirst();
			if (c.getCount() != 0) {

				recurring_type = (c.getString(c
						.getColumnIndex("recurring_type")));
				c.moveToNext();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recurring_type;
	}

	// CHECK Game_id EXISTANCE
	public String getGameTime(String game_id) {
		String game_title = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT datetime FROM NEXTSCHEDUEDGAME where game_id = '"
						+ game_id + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			game_title = (c1.getString(c1.getColumnIndex("datetime")));
			c1.moveToNext();
		}
		return game_title;
	}

	// Deleting the game from the local game table when the game id matches
	public void deleteAllUpcomingGames(String title, String game_time) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM NEXTSCHEDUEDGAME WHERE title='" + title + "'"
				+ " AND datetime >='" + game_time + "'");

	}

	public boolean updateUserFirstname(String user_id, String firstname) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USER SET firstname = '" + firstname
				+ "'" + " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public boolean updateUserLastname(String user_id, String lastname) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE USER SET lastname = '" + lastname + "'"
				+ " where user_id = '" + user_id + "'", null);
		c.moveToNext();

		return true;
	}

	public int getAllGamesCountAtaLocation(String latlng) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM LOCALGAME_LIST where latlng = '"
				+ latlng + "'", null);
		return c1.getCount();
	}

	public Cursor getAllGamesAtaLocation(String latlng) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT * FROM LOCALGAME_LIST  where latlng = '" + latlng
						+ "' order by game_date,converted_time ASC", null);
		return c1;
	}

	public ArrayList<String> getAllFriendNames() {
		String user_name = null;
		ArrayList<String> allfriends = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT friend_name FROM FRIEND", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			user_name = (c1.getString(c1.getColumnIndex("friend_name")));
			allfriends.add(user_name);
			c1.moveToNext();
		}
		return allfriends;
	}

	// Get user login status
	public String getUserLoginStatus() {
		String user_status = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT user_login_status FROM USER_LOGIN_STATUS", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			user_status = c1.getString(c1.getColumnIndex("user_login_status"));
			c1.moveToNext();
		}
		return user_status;
	}

	// Get user login status
	public int getVersionCOde() {
		int user_status = 0;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT version_code FROM USER_LOGIN_STATUS",
				null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			user_status = c1.getInt(c1.getColumnIndex("version_code"));
			c1.moveToNext();
		}
		return user_status;
	}

	public void deleteUserStatus(Context context) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + USER_LOGIN_STATUS);
	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public int getPendingIntentID(String game_id) {
		int pendingintent_id = 0;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT checkin_id FROM CANCELL_GAME_CHECKIN where game_id ='"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			pendingintent_id = c1.getInt(c1.getColumnIndex("checkin_id"));
			c1.moveToNext();
		}
		return pendingintent_id;
	}

	// UPDATE STATUS FRIEND
	public void updateCheckinGames(int checkin_id_update, String game_id_update) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c = db.rawQuery("UPDATE CANCELL_GAME_CHECKIN SET checkin_id = '"
				+ checkin_id_update + "'" + " where game_id = '"
				+ game_id_update + "'", null);
		c.moveToNext();
	}

	// GETTING FRD ID FROM FRIEND TABLE BY FRIEND NAME
	public String getPendingIntentauth_token(String game_id) {
		String pendingintent_auth_token = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT auth_token FROM CANCELL_GAME_CHECKIN where game_id ='"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			pendingintent_auth_token = c1.getString(c1
					.getColumnIndex("auth_token"));
			c1.moveToNext();
		}
		return pendingintent_auth_token;
	}

	// Get all friend ids from database
	public ArrayList<String> getAllFriendIds() {
		String user_name = null;
		ArrayList<String> allfriends = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT friend_id FROM FRIEND", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {

			for (int i = 0; i < c1.getCount(); i++) {
				user_name = (c1.getString(c1.getColumnIndex("friend_id")));
				allfriends.add(user_name);
				c1.moveToNext();
			}

		}
		return allfriends;
	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public String getcustomSportId(String sportName) {
		String selected_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT sport_id FROM SPORT where sport_name = '" + sportName
						+ "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			selected_id = (c1.getString(c1.getColumnIndex("sport_id")));
			c1.moveToNext();
		}
		return selected_id;
	}

	// GETTING TEAMMATE IMAGE FROM FRIEND TABLE BY USERID
	public String getTeammateImage(String friend_id) {
		String image_url = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT image FROM FRIEND where friend_id = '"
				+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			image_url = (c1.getString(c1.getColumnIndex("image")));
			c1.moveToNext();
		}
		return image_url;
	}

	public String getTeammateUsername(String friend_id) {
		String username = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT friend_name FROM FRIEND where friend_id = '"
						+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			username = (c1.getString(c1.getColumnIndex("friend_name")));
			c1.moveToNext();
		}
		return username;
	}

	public String getTeammateFirstname(String friend_id) {
		String firstname = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT first_name FROM FRIEND where friend_id = '" + friend_id
						+ "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			firstname = (c1.getString(c1.getColumnIndex("first_name")));
			c1.moveToNext();
		}
		return firstname;
	}

	public String getTeammateLastname(String friend_id) {
		String lastname = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT last_name FROM FRIEND where friend_id = '" + friend_id
						+ "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			lastname = (c1.getString(c1.getColumnIndex("last_name")));
			c1.moveToNext();
		}
		return lastname;
	}

	// Get All chat history from USER_CONVERSATION table by channel_id
	public Cursor getAllMessagesForChat(String channel_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT * FROM USER_CONVERSATION where channel_id = '"
						+ channel_id + "'", null);
		return c1;
	}

	// Get All chat history from USER_CONVERSATION table by channel_id
	public Cursor getAllRecipentsForChat(String sender_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT * FROM USER_CONVERSATION where sender_id = '"
						+ sender_id + "'", null);
		return c1;
	}

	// Get channel_id from USER_CONVERSATION table by sender_id and revicer_id
	public String getChannelId(String sender_id, String revicer_id) {
		String channel_id = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT channel_id FROM USER_CONVERSATION where sender_id = '"
						+ sender_id + "'" + " AND revicer_id = '" + revicer_id
						+ "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			channel_id = (c1.getString(c1.getColumnIndex("channel_id")));
			c1.moveToNext();
		}
		return channel_id;
	}

	// Get last message from USER_CONVERSATION table by channel_id
	public String getLastMessage(String channel_id) {
		String message = null;

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT message FROM USER_CONVERSATION where channel_id = '"
						+ channel_id + "'", null);
		/*
		 * Cursor c1 =
		 * db.rawQuery("SELECT message FROM USER_CONVERSATION where sender_id = '"
		 * + sender_id + "'"+" AND revicer_id = '" + revicer_id + "'", null);
		 */
		c1.moveToLast();
		if (c1.getCount() != 0) {
			message = (c1.getString(c1.getColumnIndex("message")));
			// c1.moveToNext();
		}
		return message;
	}

	/*** Check if friend_id exits in db **/
	public boolean CheckIsFriendorNot(String friend_id) {
		SQLiteDatabase sqldb = this.dBHelper.getReadableDatabase();
		String Query = "Select * FROM FRIEND where friend_id =?" + friend_id;
		Cursor cursor = sqldb.rawQuery(Query, null);
		if (cursor.getCount() <= 0) {
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}

	public int getSportTablerowcount() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM SPORT", null);
		return c1.getCount();
	}

	/*** get all chat info details ***/
	/*
	 * public Cursor getChatInfoData(String userid) { SQLiteDatabase db =
	 * this.dBHelper.getReadableDatabase(); Cursor c1 =
	 * db.rawQuery("SELECT * FROM CHAT_INFO where user_id = '" + userid + "'",
	 * null); return c1; }
	 */

	public ArrayList<String> getAllFriendNamesFromDB() {
		String user_name = null;
		ArrayList<String> allfriends = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT friend_name FROM FRIEND", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				user_name = (c1.getString(c1.getColumnIndex("friend_name")));
				allfriends.add(user_name);
				c1.moveToNext();
			}
		}
		return allfriends;
	}

	/*** get all chat info details ***/
	/*
	 * public Cursor getChatInfoData(String userid, String channel_id) {
	 * SQLiteDatabase db = this.dBHelper.getReadableDatabase(); Cursor c1 =
	 * db.rawQuery("SELECT * FROM CHAT_INFO where user_id = '" + userid +
	 * "' AND channel_id = '" + channel_id + "'", null); return c1; }
	 */

	/*** get all chat info details ***/
	public Cursor getChatInfoData(String userid, String channel_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM CHAT_INFO where channel_id = '"
				+ channel_id + "'", null);
		return c1;
	}

	/*** get all chat info details ***/
	/*
	 * public Cursor getChatInfoData() { SQLiteDatabase db =
	 * this.dBHelper.getReadableDatabase(); Cursor c1 =
	 * db.rawQuery("SELECT * FROM CHAT_INFO", null); return c1; }
	 */

	/**
	 * get All channel id for filter latest message
	 * 
	 */
	public ArrayList<String> getAllChannelId() {
		ArrayList<String> all_id_arr = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT * FROM USER_CONVERSATION ORDER BY rowid DESC", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				String channel_id = (c1.getString(c1
						.getColumnIndex("channel_id")));
				if (!all_id_arr.contains(channel_id)) {
					all_id_arr.add(channel_id);
				}
				c1.moveToNext();
			}
		}
		return all_id_arr;
	}

	/**
	 * get All channel id for filter latest message
	 * 
	 */
	public ArrayList<String> getAllChannelIdFromChatInfo() {
		ArrayList<String> all_id_arr = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery(
				"SELECT * FROM CHAT_INFO ORDER BY time_stamp DESC", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				String channel_id = (c1.getString(c1
						.getColumnIndex("channel_id")));
				if (!all_id_arr.contains(channel_id)) {
					all_id_arr.add(channel_id);
				}
				c1.moveToNext();
			}
		}
		return all_id_arr;
	}

	// Getting the provider from the user table
	public String getProvider(String username) {
		String email = null;
		ArrayList<String> user_info = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT provider FROM USER where user_name = '"
				+ username + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			email = (c1.getString(c1.getColumnIndex("provider")));
			c1.moveToNext();
		}
		return email;
	}

	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public String getUSerFacebookID(String username) {
		String facebook_id = null;
		ArrayList<String> user_info = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT fb_id FROM USER where user_name = '"
				+ username + "'", null);

		c1.moveToFirst();
		if (c1.getCount() != 0) {
			facebook_id = (c1.getString(c1.getColumnIndex("fb_id")));

			c1.moveToNext();
		}

		return facebook_id;

	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public boolean getChannelIdStatus(String channel_id) {
		String channel_id_db = null;
		boolean isChatExist = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT channel_id FROM USER_CONVERSATION where channel_id = '"
						+ channel_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			channel_id_db = (c1.getString(c1.getColumnIndex("channel_id")));
			isChatExist = true;
			c1.moveToNext();
		}
		// System.out.println(" selected_frdId FROM DB" + selected_id);
		return isChatExist;
	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public boolean getChannelIdExistance(String channel_id) {
		String channel_id_db = null;
		boolean isChatExist = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT channel_id FROM CHAT_INFO where channel_id = '"
						+ channel_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			channel_id_db = (c1.getString(c1.getColumnIndex("channel_id")));
			isChatExist = true;
			c1.moveToNext();
		}
		return isChatExist;
	}

	// GETTING TEAM FROM SPORT TABLE BY SPORT NAME
	public boolean getGroupChatTeamExistance(String team_id) {
		String team_id_db = null;
		boolean isChatExist = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT team_id FROM Group_Chat_Team_Status where team_id = '"
						+ team_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			team_id_db = (c1.getString(c1.getColumnIndex("team_id")));
			isChatExist = true;
			c1.moveToNext();
		}
		return isChatExist;
	}

	// DELETE FRIEND TABLE ROW BY THE HELP OF FRIEND ID
	public boolean deleteTeamID(String team_id) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		try {
			db.delete(Group_Chat_Team_Status, "team_id = ?",
					new String[] { team_id });
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.close();
		}
		return false;
	}

	// GETTING TEAM FROM SPORT TABLE BY SPORT NAME
	public String getGroupName(String channelid) {
		String team_name = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT teammate_name FROM CHAT_INFO where channel_id = '"
						+ channelid + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			team_name = (c1.getString(c1.getColumnIndex("teammate_name")));
			c1.moveToNext();
		}
		return team_name;
	}

	/*** get all chat info row count ***/
	public int getChatInfoRowCount() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM CHAT_INFO", null);
		return c1.getCount();
	}

	// UPDATE user last message
	public void updateLastMessage(String channel_id, String message) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c;

		db.execSQL("UPDATE CHAT_INFO SET chat_message = '" + message + "'"
				+ " where channel_id = '" + channel_id + "'");

		// c.moveToNext();

	}

	// Delete the channel ID
	public boolean deleteChannelID(String channelid) {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		try {
			db.delete(CHAT_INFO, "channel_id = ?", new String[] { channelid });
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.close();
		}
		return false;
	}

	// UPDATE user Team name with channel id
	public void updateTeamName(String channel_id, String message) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c;

		db.execSQL("UPDATE CHAT_INFO SET teammate_name = '" + message + "'"
				+ " where channel_id = '" + channel_id + "'");

		// c.moveToNext();

	}

	// UPDATE user last message
	public void updateTeamNameForTeam(String team_id, String teamname) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("UPDATE CHAT_INFO SET teammate_name = '" + teamname + "'"
				+ " where teammate_id = '" + team_id + "'");

		// c.moveToNext();

	}

	// GETTING MYTEAM TABLE LATEST UPDATED COLUMN
	public String getLastTTimeStamp(String channel_id) {
		String time_stamp = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM " + USER_CONVERSATION
				+ " where channel_id= '" + channel_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				time_stamp = (c1.getString(c1.getColumnIndex("time_stamp")));
				c1.moveToNext();
			}

		}

		return time_stamp;
	}

	/**
	 * Delete all the chat dasa for the given channel ID
	 * 
	 * @param channel_id
	 */
	public void deleteChatData(String channel_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM USER_CONVERSATION where channel_id='"
				+ channel_id + "'");
	}

	/**
	 * Delete data for the given channel ID
	 * 
	 * @param channel_id
	 */

	public void deleteChatHistoryData(String channel_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM CHAT_INFO where channel_id='" + channel_id
				+ "'");

	}

	/**
	 * Delete the withdrawn user from the TEAMMATE Table
	 * 
	 * @param gameid
	 * @param usarname
	 */
	public void deleteWithdrawUser(String gameid, String usarname) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM TEAMMATE where game_id='" + gameid
				+ "' AND player_name='" + usarname + "'");

	}

	// GETTING the message message existance
	public boolean getMessageExistance(String message_id) {
		String message_id_db = null;
		boolean isChatExist = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT message_id FROM USER_CONVERSATION where message_id = '"
						+ message_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			message_id_db = (c1.getString(c1.getColumnIndex("message_id")));
			isChatExist = true;
			c1.moveToNext();
		}
		return isChatExist;
	}

	/*** get all PENDING REQUEST count ***/
	public int getGameInvitationCOunt() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM PENDINGREQUEST", null);
		return c1.getCount();
	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public boolean getPendingRequestIdExistance(String game_id) {
		String game_id_db = null;
		boolean isChatExist = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT game_id FROM PENDINGREQUEST where game_id = '"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			game_id_db = (c1.getString(c1.getColumnIndex("game_id")));
			isChatExist = true;
			c1.moveToNext();
		}
		return isChatExist;
	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public String getPendingRequestId(String game_id) {
		String request_id_db = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT request_id FROM PENDINGREQUEST where game_id = '"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			request_id_db = (c1.getString(c1.getColumnIndex("request_id")));
			c1.moveToNext();
		}
		return request_id_db;
	}

	// UPDATE user last message
	public void updateDeleteStatus(String channel_id, int status) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("UPDATE USER_CONVERSATION SET isDeleted = '" + status + "'"
				+ " where channel_id = '" + channel_id + "'");

		// c.moveToNext();

	}

	// UPDATE user last message
	public void updatefriendName(String friend_id, String friend_name) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("UPDATE CHAT_INFO SET teammate_name = '" + friend_name + "'"
				+ " where teammate_id = '" + friend_id + "'");

		// c.moveToNext();

	}

	// GETTING the message message existance
	public boolean getGroupeExistance(String group_id) {
		String group_id_db = null;
		boolean isChatExist = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT teammate_id FROM CHAT_INFO where teammate_id = '"
						+ group_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			group_id_db = (c1.getString(c1.getColumnIndex("teammate_id")));
			isChatExist = true;
			c1.moveToNext();
		}
		return isChatExist;
	}

	public int getunreadMessageCount() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM CHAT_UNREAD_MESSAGES", null);

		return c1.getCount();
	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public boolean getChannelIdExistanceUnreadMessages(String channel_id) {
		String channel_id_db = null;
		boolean isChatExist = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT channel_id FROM CHAT_UNREAD_MESSAGES where channel_id = '"
						+ channel_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			channel_id_db = (c1.getString(c1.getColumnIndex("channel_id")));
			isChatExist = true;
			c1.moveToNext();
		}
		return isChatExist;
	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public int getPendingUnreadMessageCount(String channel_id) {
		int unreadMessageCount = 0;
		boolean isChatExist = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT unread_messages FROM CHAT_UNREAD_MESSAGES where channel_id = '"
						+ channel_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			unreadMessageCount = (c1.getInt(c1
					.getColumnIndex("unread_messages")));
			c1.moveToNext();
		}
		return unreadMessageCount;
	}

	// UPDATE user last message
	public void updateUnreadMessageCount(String channel_id, int updated_count) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("UPDATE CHAT_UNREAD_MESSAGES SET unread_messages = '"
				+ updated_count + "'" + " where channel_id = '" + channel_id
				+ "'");

	}

	public void deleteChatReadMessages(String channel_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM CHAT_UNREAD_MESSAGES where channel_id='"
				+ channel_id + "' ");

	}

	public int getUserConversationRowCount() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM CHAT_INFO", null);

		return c1.getCount();
	}

	// GETTING SPORT ID FROM SPORT TABLE BY SPORT NAME
	public String getLastTimeStamp() {
		String last_time_stamp = null;
		boolean isChatExist = false;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT * FROM CHAT_INFO ORDER BY time_stamp DESC", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			last_time_stamp = (c1.getString(c1.getColumnIndex("time_stamp")));
			c1.moveToNext();
		}
		return last_time_stamp;
	}

	public void deleteUserunreadChatMessages(Context context) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + CHAT_UNREAD_MESSAGES);

	}

	public void deleteUserhatInfo(Context context) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + CHAT_INFO);

	}

	// GETTING TEAMMATE IMAGE FROM FRIEND TABLE BY USERID
	public String getGroupChatTeammateImage(String friend_id) {
		String image_url = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT player_image FROM GROUP_CHAT_TEAM_DETAILS where player_id = '"
						+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			image_url = (c1.getString(c1.getColumnIndex("player_image")));
			c1.moveToNext();
		}
		return image_url;
	}

	// GETTING TEAMMATE IMAGE FROM FRIEND TABLE BY USERID
	public String getGroupChatTeammateFirstName(String friend_id) {
		String image_url = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT plyaer_firstname FROM GROUP_CHAT_TEAM_DETAILS where player_id = '"
						+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			image_url = (c1.getString(c1.getColumnIndex("plyaer_firstname")));
			c1.moveToNext();
		}
		return image_url;
	}

	// GETTING TEAMMATE IMAGE FROM FRIEND TABLE BY USERID
	public String getGroupChatTeammateLastName(String friend_id) {
		String image_url = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT player_lastname FROM GROUP_CHAT_TEAM_DETAILS where player_id = '"
						+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			image_url = (c1.getString(c1.getColumnIndex("player_lastname")));
			c1.moveToNext();
		}
		return image_url;
	}

	// GETTING TEAMMATE IMAGE FROM FRIEND TABLE BY USERID
	public String getGroupChatTeammateUsername(String friend_id) {
		String image_url = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT player_name FROM GROUP_CHAT_TEAM_DETAILS where player_id = '"
						+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			image_url = (c1.getString(c1.getColumnIndex("player_name")));
			c1.moveToNext();
		}
		return image_url;
	}

	// CHECK TEAM PLAYER EXISTANCE
	public Boolean checkUserExistancefriendTable(String user_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_user_exitance = false;
		Cursor c1 = db.rawQuery("SELECT friend_id FROM " + FRIEND
				+ " where friend_id = '" + user_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_user_exitance = true;
			c1.moveToNext();
		}
		return check_user_exitance;
	}

	// CHECK TEAM PLAYER EXISTANCE
	public Boolean checkUserExistanceNonfriendTable(String user_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_user_exitance = false;
		Cursor c1 = db.rawQuery("SELECT user_id FROM " + NONFRIEND_CHAT_DETAILS
				+ " where user_id = '" + user_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_user_exitance = true;
			c1.moveToNext();
		}
		return check_user_exitance;
	}

	public String getNonFriendFirstname(String friend_id) {
		String firstname = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT first_name FROM NONFRIEND_CHAT_DETAILS where user_id = '"
						+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			firstname = (c1.getString(c1.getColumnIndex("first_name")));
			c1.moveToNext();
		}
		return firstname;
	}

	public String getNonFriendLastname(String friend_id) {
		String lastname = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT last_name FROM NONFRIEND_CHAT_DETAILS where user_id = '"
						+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			lastname = (c1.getString(c1.getColumnIndex("last_name")));
			c1.moveToNext();
		}
		return lastname;
	}

	public String getNonFriendImage(String friend_id) {
		String image = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT image FROM NONFRIEND_CHAT_DETAILS where user_id = '"
						+ friend_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			image = (c1.getString(c1.getColumnIndex("image")));
			c1.moveToNext();
		}
		return image;
	}

	public String getFriendUserID(String frienduser_name) {
		String image = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT friend_id FROM FRIEND where friend_name = '"
						+ frienduser_name + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			image = (c1.getString(c1.getColumnIndex("friend_id")));
			c1.moveToNext();
		}
		return image;
	}

	// CHECK TEAM PLAYER EXISTANCE
	public int getNonfriendChatdetailsTableRowCount() {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM NONFRIEND_CHAT_DETAILS", null);
		return c1.getCount();
	}

	/*
	 * // UPDATE user last message public void updateChatInfo(String channel_id,
	 * String username,String firstname,String lastname, String image) {
	 * 
	 * SQLiteDatabase db = this.dBHelper.getReadableDatabase();
	 * 
	 * db.execSQL("UPDATE USER_CONVERSATION SET unread_messages = '" +
	 * updated_count + "'" + " where channel_id = '" + channel_id + "'");
	 * 
	 * }
	 */

	/*
	 * // CHECK TEAM PLAYER EXISTANCE public Boolean
	 * checkUserExistanceNonfriendTable(String user_id) {
	 * 
	 * SQLiteDatabase db = this.dBHelper.getReadableDatabase(); Boolean
	 * check_user_exitance = false; Cursor c1 =
	 * db.rawQuery("SELECT user_id FROM " + NONFRIEND_CHAT_DETAILS +
	 * " where user_id = '" + user_id + "'", null); c1.moveToFirst(); if
	 * (c1.getCount() != 0) { check_user_exitance = true; c1.moveToNext(); }
	 * return check_user_exitance; }
	 */

	// CHECK Game EXISTANCE in the LOCALGAME_LIST
	public Boolean checkNextScheduledGame(String game_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_game_exitance = false;
		Cursor c1 = db.rawQuery("SELECT game_id FROM " + NEXTSCHEDUEDGAME
				+ " where game_id = '" + game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_game_exitance = true;
			c1.moveToNext();
		}
		return check_game_exitance;
	}

	// GET PLAYER COUNT OF A GAME
	public int getGamePlayerCountLocalGames(String game_id) {
		int count = 0;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery(
				"SELECT player_count FROM LOCALGAME_LIST where game_id = '"
						+ game_id + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			count = (c1.getInt(c1.getColumnIndex("player_count")));
			c1.moveToNext();
		}

		return count;

	}

	// GET PLAYER COUNT OF A GAME
	public ArrayList<String> getAllLocalGameIds() {
		String game_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		ArrayList<String> gameid_arr = new ArrayList<String>();

		Cursor c1 = db.rawQuery("SELECT * FROM LOCALGAME_LIST", null);
		c1.moveToFirst();

		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				game_id = (c1.getString(c1.getColumnIndex("game_id")));
				gameid_arr.add(game_id);
				c1.moveToNext();
			}
		}

		return gameid_arr;

	}

	// GETTING MYTEAM TABLE LATEST UPDATED COLUMN
	public String getLatestLocalGame() {
		String selected_date = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		Cursor c1 = db.rawQuery("SELECT * FROM " + LOCALGAME_LIST
				+ " ORDER BY updated_at DESC LIMIT 1", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			// for (int i = 0; i < c1.getCount(); i++) {
			selected_date = (c1.getString(c1.getColumnIndex("updated_at")));
			c1.moveToNext();
			// }

		}
		return selected_date;
	}

	// CHECK Game EXISTANCE in the LOCALGAME_LIST
	public Boolean checkTeammateExistance(String username) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Boolean check_teammmate_exitance = false;
		Cursor c1 = db.rawQuery("SELECT player_name FROM " + TEAMMATE
				+ " where player_name = '" + username + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			check_teammmate_exitance = true;
			c1.moveToNext();
		}
		return check_teammmate_exitance;
	}

	public void deleteCancelGameCheckin(String gameid) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM CANCELL_GAME_CHECKIN where game_id='" + gameid
				+ "' ");

	}

    // UPDATE user last message
    public void updateteammateStatus(String plyaer_id, String status) {

        SQLiteDatabase db = this.dBHelper.getReadableDatabase();

        db.execSQL("UPDATE TEAMMATE SET request_status = '" + status + "'"
                + " where player_id = '" + plyaer_id + "'");

    }

	/**
	 * Get all the group chat friends
	 * @param groupid
	 * @return
     */
	public ArrayList<String> getChatGroupTeammates(String groupid, String loggedinusername) {
		String player_name = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		ArrayList<String> player_name_arr = new ArrayList<String>();

		Cursor c1 = db.rawQuery("SELECT player_name FROM GROUP_CHAT_TEAM_DETAILS where group_id = '" + groupid + "'", null);
		c1.moveToFirst();

		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				player_name = (c1.getString(c1.getColumnIndex("player_name")));

                if(!player_name_arr.contains(player_name) && !player_name.equalsIgnoreCase(loggedinusername)){
                    player_name_arr.add(player_name);
                }

				c1.moveToNext();
			}
		}
		return player_name_arr;

	}


	/**
	 * Get player of a team
	 * @param groupid
     * @return
     */
	public String getTeamPlayers(String groupid) {
		String players = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT players FROM MYTEAM where team_id = '" + groupid + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
            players = (c1.getString(c1.getColumnIndex("players")));
            c1.moveToNext();
		}
		return players;
	}


    public void getFriendDetailsTeam(String user_id,String group_id) {

        String name = null, sport = null;
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();

        Cursor c1 = db.rawQuery("SELECT * FROM FRIEND WHERE friend_id='"
                + user_id + "'", null);

        c1.moveToFirst();
        if (c1.getCount() != 0) {
           String  username = (c1.getString(c1.getColumnIndex("friend_name")));
           String firatsname = (c1.getString(c1.getColumnIndex("first_name")));
            String last_name = (c1.getString(c1.getColumnIndex("last_name")));
            String image = (c1.getString(c1.getColumnIndex("image")));
            insertgroupChatDetaiils(user_id,username,firatsname,last_name,image,group_id);
            c1.moveToNext();
        }
    }

	// UPDATE user last message
	public void updateTeamNameMySquadz(String team_name, String team_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("UPDATE MYTEAM SET name = '"
				+ team_name + "'" + " where team_id = '" + team_id
				+ "'");
	}


	public void deleteMySquadzTeammate(String player_id, String gropp_id) {

		SQLiteDatabase db = this.dBHelper.getReadableDatabase();

		db.execSQL("delete FROM GROUP_CHAT_TEAM_DETAILS WHERE player_id='" + player_id + "'"
				+ " AND group_id = '" + gropp_id + "'");
	}

	// GET ALL SPORTS
	public Cursor getFriendsFromDB() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM FRIEND", null);
		return c1;
	}

	// Getting all sports name
	public Cursor getSportsName() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM SPORT", null);
		return c1;
	}



    // Getting all sports name
    public int getCredentialsCount(){
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT * FROM credentials", null);
        return c1.getCount();
    }


    // Getting all sports name
    public Cursor getAllCredentials() {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT * FROM credentials", null);
        return c1;
    }

    // Getting all sports name
    public int getSavedCardsCount() {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT * FROM CARD_DETAILS", null);
        return c1.getCount();
    }

    // Getting all sports name
    public Cursor getSavedCards() {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT * FROM CARD_DETAILS ORDER BY default_card DESC", null);
        return c1;
    }

    // Getting all sports name
    public Cursor getAllFriendsDeomDb() {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT * FROM FRIEND", null);
        return c1;
    }
	//Delete Card details before inserting new caeds
	public void deleteCardDetail() {
		SQLiteDatabase db = this.dBHelper.getWritableDatabase();
		db.execSQL("delete FROM " + CARD_DETAILS);
	}


	// Getting all sports name
	public int getFriendTableCount() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM FRIEND", null);
		return c1.getCount();
	}


    // FILTERING FRIEND BY SPORT NAME
    public Cursor getFilteredFriendList(String SportName) {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        return db.rawQuery("SELECT * FROM FRIEND where sport ='"+ SportName + "'", null);
    }

    // FILTERING FRIEND BY SPORT NAME
    public Cursor getAllTeams(String SportId) {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        return db.rawQuery("SELECT * FROM MYTEAM where team_sport_id ='"+ SportId + "'", null);
    }

	// FILTERING FRIEND BY SPORT NAME
	public Cursor getTeamsForAll() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		return db.rawQuery("SELECT * FROM MYTEAM", null);
	}

    // FILTERING FRIEND BY SPORT NAME
    public Cursor getAllTeamPlayers(String teamId) {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        return db.rawQuery("SELECT * FROM TEAMPLAYER where team_id ='"+ teamId + "'", null);
    }

    // DELETE FRIEND TABLE ROW BY THE HELP OF FRIEND ID
    public boolean deleteFriendFromDb(String friend_id) {
        SQLiteDatabase db = this.dBHelper.getWritableDatabase();
        try {
            db.delete(FRIEND, "user_id = ?", new String[] { friend_id });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return false;
    }

    // DELETE FRIEND TABLE ROW BY THE HELP OF FRIEND ID
    public boolean deleteTeam(String team_id) {
        SQLiteDatabase db = this.dBHelper.getWritableDatabase();
        try {
            db.delete(MYTEAM, "team_id = ?", new String[] { team_id });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return false;
    }


    // DELETE FRIEND TABLE ROW BY THE HELP OF FRIEND ID
    public boolean deleteTeamPlayer(String team_id) {
        SQLiteDatabase db = this.dBHelper.getWritableDatabase();
        try {
            db.delete(TEAMPLAYER, "team_id = ?", new String[] { team_id });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return false;
    }



    // UPDATE user last message
    public void updateUserPrimaryCard(String card_id, int status) {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        db.execSQL("UPDATE CARD_DETAILS SET default_card = '"
                + status + "'" + " where card_id = '" + card_id
                + "'");
    }


    // UPDATE user last message
    public void updateUserPrimaryCardstatus(String card_id, int status) {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        db.execSQL("UPDATE CARD_DETAILS SET default_card = '"
                + status + "'" + " where card_id = '" + card_id
                + "'");
    }


	/**
	 * Get primary card
	 * @return
	 */
	public String getPrimaryPaymentCard(int default_card_status) {
		String card_id = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT card_id FROM CARD_DETAILS where default_card = '" + default_card_status + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
            card_id = (c1.getString(c1.getColumnIndex("card_id")));
			c1.moveToNext();
		}
		return card_id;
	}
	/**
	 * Getting Sport ID on behalf of Sports name from SPORT table
	 */
	public String getSportId(String sport_name)
	{
		String sport_id = "";
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT sport_id FROM SPORT where sport_name = '" + sport_name + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			sport_id = (c1.getString(c1.getColumnIndex("sport_id")));
			c1.moveToNext();
		}
		return sport_id;
	}

	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public int getTeamPlayerCount(String team_id) {
		ArrayList<String> player_names = new ArrayList<String>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMPLAYER
				+ " where  team_id ='" + team_id + "'", null);
		return c1.getCount();

	}



	// GETTING TEAM PLYER NAME FROM TEAMPLAYER TABLE BY TEAM NAME
	public ArrayList<FirebaseGroupChatParticipantModel> getAllteamPlayers(String teamId) {
        ArrayList<FirebaseGroupChatParticipantModel> allChatUserData = new ArrayList<>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM " + TEAMPLAYER
				+ " where  team_id ='" + teamId + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {

				String player_name = c1.getString(c1
						.getColumnIndex("player_user_name"));
                String player_id = c1.getString(c1
                        .getColumnIndex("player_id"));
                String player_image = c1.getString(c1
                        .getColumnIndex("player_profile_image"));

                FirebaseGroupChatParticipantModel firebaseCHatModel =
                        new FirebaseGroupChatParticipantModel();
				firebaseCHatModel.setUser_id(player_id);
				firebaseCHatModel.setUsername(player_name);
				firebaseCHatModel.setImage(player_image);
                allChatUserData.add(firebaseCHatModel);
				c1.moveToNext();
			}
		}
		return allChatUserData;
	}
	/**
	 * Getting player count based on the Sports
	 */
	public int getPlayerCount(String sport_name) {
		int player_count = 0;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT username FROM FRIEND where sport = '" + sport_name + "'", null);
		c1.moveToFirst();

		if (c1.getCount() != 0) {
			player_count = c1.getCount();
		}

		return player_count;

	}

	// Getting all Friends
	public int getAllPlayerCount() {
		int player_count = 0;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM FRIEND", null);
		c1.moveToFirst();

		if (c1.getCount() != 0) {
			player_count = c1.getCount();
		}

		return player_count;
	}

	// Getting players based on the Team ID
	public ArrayList<String> getPlayers(String team_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		ArrayList<String> players_username = new ArrayList<>();
		Cursor c1 = db.rawQuery("SELECT player_user_name FROM TEAMPLAYER where team_id ='"+ team_id + "'", null);

		c1.moveToFirst();
		for (int i = 0; i < c1.getCount(); i++) {

			players_username.add(c1.getString(c1
					.getColumnIndex("player_user_name")));

			c1.moveToNext();
		}
		return players_username;
	}

	/**
	 * Delete Team & players before inserting new records
	 */
	public void deleteTeamPlayer() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + MYTEAM);
		db.execSQL("delete FROM " + TEAMPLAYER);
		db.close();
	}

	/**
	 * Delete Friend before inserting new records
	 */
	public void deleteFriend() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + FRIEND);
		db.close();
	}


	// GETTING PUSH STATUS FROM GCM TABLE BY DEVICE_TOKEN
	public void updateTeam(String teamId,String teamName,String teamSport,
						   String teamCustomSport, String teamCreatorId,
						   String teamCreatorName,String teamCreatorImage,
						   String teamCreatedAt,String teamUpdatedAt) {

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();

		ContentValues contentValues = new ContentValues();
		contentValues.put("team_id", teamId);
		contentValues.put("team_name", teamName);
		contentValues.put("team_sport_id", teamSport);
		contentValues.put("team_custom_sport_name", teamCustomSport);
		contentValues.put("team_creator_user_id", teamCreatorId);
		contentValues.put("team_creator_user_name", teamCreatorName);
		contentValues.put("team_creator_profile_image", teamCreatorImage);
		contentValues.put("team_created_at", teamCreatedAt);
		contentValues.put("team_updated_at", teamUpdatedAt);

		db.update("MYTEAM", contentValues, "team_id="+teamId, null);

		//c.moveToNext();

//		Cursor c = db.rawQuery("UPDATE MYTEAM SET team_id = '" + push_status
//				+ "'" + " where device_token = '" + device_token + "'", null);
//		c.moveToNext();

	}

	public void updatePlayer(String teamId,String player_id,String player_user_name,
						   String player_first_name,String player_last_name,
						   String player_image) {

		SQLiteDatabase db = this.dBHelper.getWritableDatabase();

		ContentValues contentValues = new ContentValues();
		contentValues.put("team_id", teamId);
		contentValues.put("player_id", player_id);
		contentValues.put("player_user_name", player_user_name);
		contentValues.put("player_first_name", player_first_name);
		contentValues.put("player_last_name", player_last_name);
		contentValues.put("player_profile_image", player_image);

		db.update("TEAMPLAYER", contentValues, "team_id="+teamId, null);

		//c.moveToNext();

//		Cursor c = db.rawQuery("UPDATE MYTEAM SET team_id = '" + push_status
//				+ "'" + " where device_token = '" + device_token + "'", null);
//		c.moveToNext();

	}

	// Getting players Ids based on the Team ID
	public ArrayList<String> getPlayersId(String team_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		ArrayList<String> player_id_array = new ArrayList<>();
		Cursor c1 = db.rawQuery("SELECT player_id FROM TEAMPLAYER where team_id ='"+ team_id + "'", null);

		c1.moveToFirst();
		for (int i = 0; i < c1.getCount(); i++) {

			player_id_array.add(c1.getString(c1
					.getColumnIndex("player_id")));

			c1.moveToNext();
		}
		return player_id_array;
	}

	// Getting Team Ids based on the Player ID
	public ArrayList<String> getTeamId(String player_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		ArrayList<String> team_id_array = new ArrayList<>();
		Cursor c1 = db.rawQuery("SELECT team_id FROM TEAMPLAYER where player_id ='"+ player_id + "'", null);

		c1.moveToFirst();
		for (int i = 0; i < c1.getCount(); i++) {

			team_id_array.add(c1.getString(c1
					.getColumnIndex("team_id")));

			c1.moveToNext();
		}
		return team_id_array;
	}

	// Getting Team Ids based on the Player ID
	public String getUserNameImage(String player_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String player_user_name = "";
		String player_user_image = "";
		Cursor c1 = db.rawQuery("SELECT player_user_name,player_profile_image FROM TEAMPLAYER where player_id ='"+ player_id + "'", null);

		c1.moveToFirst();
		for (int i = 0; i < c1.getCount(); i++) {

			player_user_name = c1.getString(c1
					.getColumnIndex("player_user_name"));
			player_user_image = c1.getString(c1
					.getColumnIndex("player_profile_image"));


			c1.moveToNext();
		}
		return player_user_name+"@@@@"+player_user_image;
	}

	// Getting Team Ids based on the Player ID
	public String getUserNameImageFromFriend(String player_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		String player_user_name = "";
		String player_user_image = "";
		Cursor c1 = db.rawQuery("SELECT username,image FROM FRIEND where user_id ='"+ player_id + "'", null);

		c1.moveToFirst();
		for (int i = 0; i < c1.getCount(); i++) {

			player_user_name = c1.getString(c1
					.getColumnIndex("username"));
			player_user_image = c1.getString(c1
					.getColumnIndex("image"));


			c1.moveToNext();
		}
		return player_user_name+"@@@@"+player_user_image;
	}

	// Getting friend username from DB
	public String getcurrentfriendUserName(String FriendId) {
		String selected_username = null;
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT username FROM " + FRIEND
				+ " where user_id = '" + FriendId + "'", null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				selected_username = (c1.getString(c1.getColumnIndex("username")));
				c1.moveToNext();
			}
		}
		return selected_username;
	}

    // Getting user profile image from DB
    public String getcurrentfriendProfileImage(String FriendId) {
        String selected_user_image = null;
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT image FROM " + FRIEND
                + " where user_id = '" + FriendId + "'", null);
        c1.moveToFirst();
        if (c1.getCount() != 0) {
            for (int i = 0; i < c1.getCount(); i++) {
                selected_user_image = (c1.getString(c1.getColumnIndex("image")));
                c1.moveToNext();
            }
        }
        return selected_user_image;
    }

    // Getting all Friends
    public Profile getCurrentUserDetails() {
        Profile userProfileData = null;
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT * FROM CURRENT_USER", null);
        c1.moveToFirst();

        if (c1.getCount() != 0) {
           String user_id = (c1.getString(c1.getColumnIndex("user_id")));
           String username = (c1.getString(c1.getColumnIndex("username")));
           String phone_number = (c1.getString(c1.getColumnIndex("phone_number")));
           String first_name = (c1.getString(c1.getColumnIndex("first_name")));
           String last_name = (c1.getString(c1.getColumnIndex("last_name")));
           String email = (c1.getString(c1.getColumnIndex("email")));
           String birth_date = (c1.getString(c1.getColumnIndex("birth_date")));
           String image = (c1.getString(c1.getColumnIndex("image")));
           String about_me = (c1.getString(c1.getColumnIndex("about_me")));
           String sport = (c1.getString(c1.getColumnIndex("sport")));
           String skill_level = (c1.getString(c1.getColumnIndex("skill_level")));
           String secondary_sports = (c1.getString(c1.getColumnIndex("secondary_sports")));
           String rewards = (c1.getString(c1.getColumnIndex("rewards")));
           String street_name = (c1.getString(c1.getColumnIndex("street_name")));
           String locality = (c1.getString(c1.getColumnIndex("locality")));
           String province_abbreviation = (c1.getString(c1.getColumnIndex("province_abbreviation")));
           String postal_code = (c1.getString(c1.getColumnIndex("postal_code")));
           String country = (c1.getString(c1.getColumnIndex("country")));

           userProfileData = new Profile(user_id, username, phone_number,
                    null, first_name, last_name,
                    email, birth_date, image,
                    about_me, sport, skill_level,
                    Integer.valueOf(rewards), street_name,
                    locality, province_abbreviation,
                    postal_code, country);
        }

        return userProfileData;
    }


    // Getting all Friends
    public int getCurrentUserStatus() {
        int player_count = 0;
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT * FROM CURRENT_USER", null);
        return c1.getCount();
    }

    public void deleteCurrentUserData(Context context) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        db.execSQL("delete FROM " + CURRENT_USER);
    }


    /**
     * Function responsible for retriving the primary card
     * @param cardId card id to be checked
     * @return
     */
	public int isPrimaryCard(String cardId){
        int defaultCardStatus = 0;
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT default_card FROM " + CARD_DETAILS
                + " where card_id = '" + cardId + "'", null);
        c1.moveToFirst();
        if (c1.getCount() != 0) {
            for (int i = 0; i < c1.getCount(); i++) {
                defaultCardStatus = (c1.getInt(c1.getColumnIndex("default_card")));
                c1.moveToNext();
            }
        }
        return defaultCardStatus;

	}


    /**
     * Function responsible to get the row count from the CARD_DETAILS table
     * @return
     */
    public int getNumberOfcards(){
        int defaultCardStatus = 0;
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        Cursor c1 = db.rawQuery("SELECT * FROM " + CARD_DETAILS, null);
        return c1.getCount();
    }


   /* // Deleting all the game radius data
    public void deleteUserCard(String card_id) {
        SQLiteDatabase db = this.dBHelper.getReadableDatabase();
        db.execSQL("delete FROM CARD_DETAILS where card_id ='" + card_id + "'", null);

    }*/

	/*//---deletes a particular title---
	public boolean deleteUserCard(String card_id) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		return db.delete(CARD_DETAILS, "card_id" + "=" + card_id, null) > 0;
	}*/

    /**
     * Remove a contact from database by title
     */
    public void deleteUserCard(String card_id) {
        //Open the database
        SQLiteDatabase database = this.dBHelper.getWritableDatabase();

        //Execute sql query to remove from database
        //NOTE: When removing by String in SQL, value must be enclosed with ''
        database.execSQL("DELETE FROM " + CARD_DETAILS + " WHERE " + "card_id" + "= '" + card_id + "'");

        //Close the database
        database.close();
    }

	/**
	 * Fetch list/event info for review
	 */
	public Cursor getGameInfoForReview(String user_id, String date) {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM USER_RATING where user_id ='"+ user_id + "' AND date ='"
				+ date + "' ORDER BY game_end_time DESC LIMIT 1", null);
		return cursor;
	}

	/**
	 * Remove records from USER_RATING table
	 */
	public void deleteRatingsInfo() {
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		db.execSQL("delete FROM " + USER_RATING);
	}


	// Getting friend username from DB
	public ArrayList<String> getAllUserids() {
        String selected_username = null;
		ArrayList<String> friendIdsarray = new ArrayList<>();
		SQLiteDatabase db = this.dBHelper.getReadableDatabase();
		Cursor c1 = db.rawQuery("SELECT * FROM " + FRIEND, null);
		c1.moveToFirst();
		if (c1.getCount() != 0) {
			for (int i = 0; i < c1.getCount(); i++) {
				selected_username = (c1.getString(c1.getColumnIndex("user_id")));
                friendIdsarray.add(selected_username);
				c1.moveToNext();
			}
		}
		return friendIdsarray;
	}
}