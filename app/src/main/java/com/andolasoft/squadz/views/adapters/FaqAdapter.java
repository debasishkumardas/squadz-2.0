package com.andolasoft.squadz.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andolasoft.squadz.R;

import java.util.ArrayList;

/**
 * Created by SpNayak on 1/3/2017.
 */

public class FaqAdapter extends BaseAdapter {
    private ArrayList<String> faq_questionary_array = new ArrayList<String>();
    private LayoutInflater layoutInflater;
    private Context context;

    public FaqAdapter(Context context, ArrayList<String> faq_questionary_array) {
        this.faq_questionary_array = faq_questionary_array;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return faq_questionary_array.size();
    }

    @Override
    public Object getItem(int position) {
        return faq_questionary_array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_faq_layout, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        String[] split_arrayList = faq_questionary_array.get(position).split("####");
        String question = split_arrayList[0];
        String answer = split_arrayList[1];

        int pos = position +1;
        mViewHolder.faq_question_text.setText(String.valueOf(pos)+". "+question);
        mViewHolder.faq_answer_text.setText(answer);

        return convertView;
    }

    private class MyViewHolder {
        TextView faq_question_text,
                 faq_answer_text;

        public MyViewHolder(View item) {
            faq_question_text = (TextView) item.findViewById(R.id.faq_question_text);
            faq_answer_text = (TextView) item.findViewById(R.id.faq_answer_text);

        }
    }
}

