package com.andolasoft.squadz.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.TeamsModel;

import java.util.ArrayList;

/**
 * Created by Debasish Kumar Das on 2/8/2017.
 */

public class TeamsAdapter extends BaseAdapter {
    Context context;
    ArrayList<TeamsModel> teamDetailsarray;
    LayoutInflater inflter;

    public TeamsAdapter(Context applicationContext, ArrayList<TeamsModel> teams) {
        this.context = applicationContext;
        this.teamDetailsarray = teams;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return teamDetailsarray.size();
    }

    @Override
    public Object getItem(int i) {
        return teamDetailsarray.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        //view = inflter.inflate(R.layout.layout_team_item, null);
        final MyViewHolder mViewHolder;
        if (view == null) {
            view = inflter.inflate(R.layout.layout_team_item, viewGroup, false);
            mViewHolder = new MyViewHolder(view);
            view.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) view.getTag();
        }

        String player_count = teamDetailsarray.get(position).getTeamPlayerCount();
        mViewHolder.teamName.setText(teamDetailsarray.get(position).getTeamName() + " ("+player_count+") ");
        mViewHolder.teamSport.setText(teamDetailsarray.get(position).getTeamSport());

        return view;
    }
    private class MyViewHolder {
        TextView teamName,teamSport;

        public MyViewHolder(View view) {
            teamName = (TextView) view.findViewById(R.id.tvTeamName);
            teamSport = (TextView) view.findViewById(R.id.tvTeamsport);
        }
    }
}
