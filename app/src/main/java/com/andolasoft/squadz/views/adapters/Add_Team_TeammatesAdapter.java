package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.AddTeammates;
import com.andolasoft.squadz.fragments.TabFragment;
import com.andolasoft.squadz.fragments.UpcomingPastGamesFragment;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.FriendsModel;
import com.andolasoft.squadz.models.InviteTeammatesModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class Add_Team_TeammatesAdapter extends
        RecyclerView.Adapter<Add_Team_TeammatesAdapter.MyViewHolder> {

    private Activity context;
    ArrayList<FriendsModel> friendsModelArrayList;
    DatabaseAdapter db;

    public Add_Team_TeammatesAdapter(Activity context, ArrayList<FriendsModel> friendsModelArrayList) {
        this.context = context;
        this.friendsModelArrayList = friendsModelArrayList;
        this.db = new DatabaseAdapter(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_add_team_teammates_list_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return friendsModelArrayList.size();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final String userFirstName = friendsModelArrayList.get(position).getFirstName();
        String lastName = friendsModelArrayList.get(position).getLastName();
        String user_id = friendsModelArrayList.get(position).getUserId();

        /**Checking participant ids for show/hide teammates in Add Teammate page*/
        if(AppConstants.userIdArray != null
                &&AppConstants.userIdArray.size() > 0
                && AppConstants.userIdArray.contains(user_id))
        {
            holder.profile_image_layout.setVisibility(View.GONE);
            holder.view_add_teammate.setVisibility(View.GONE);
        }
        else{
            holder.profile_image_layout.setVisibility(View.VISIBLE);
            holder.view_add_teammate.setVisibility(View.VISIBLE);

            if(!TextUtils.isEmpty(userFirstName) ||
                    !TextUtils.isEmpty(lastName) && !lastName.equalsIgnoreCase("null")){
                holder.userFullname_add_teammates.setText(userFirstName+" "+ lastName);
            }
            else if(!TextUtils.isEmpty(userFirstName) && !userFirstName.equalsIgnoreCase("null"))
            {
                holder.userFullname_add_teammates.setText(userFirstName);
            }
            else{
                holder.userFullname_add_teammates.setText(friendsModelArrayList.get(position).getUserName());
            }

            String userProfileImage = friendsModelArrayList.get(position).getImage();
            if(!TextUtils.isEmpty(userProfileImage) &&
                    !userProfileImage.equalsIgnoreCase("")) {
                if(!userProfileImage.equalsIgnoreCase("null") &&
                        !TextUtils.isEmpty(userProfileImage) &&
                        userProfileImage != null){
                    Glide.with(context).load(userProfileImage).into(holder.profile_image_teammates);
                }
            }

            /**It will auto select on the checkbox if this array contains the teammates id
             * This array stores the data in while any team is clicked & that team has any teammates*/
            if(Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.contains(user_id))
            {
                holder.add_teammates_checkbox.setChecked(true);
            }
            else{
                holder.add_teammates_checkbox.setChecked(false);
            }
        }

        //holder.add_teammates_checkbox.setTag(position); // This line is important.
        /**Check box click listener*/
        holder.add_teammates_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_id = friendsModelArrayList.get(position).getUserId();
                String user_Name = friendsModelArrayList.get(position).getUserName();
                if(holder.add_teammates_checkbox.isChecked()) {
                    Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.add(user_id);
                } else {
                    String team_id = "";
                    //String user_id = friendsModelArrayList.get(position).getUserId();
                    if(db.getTeamId(user_id).size() > 0) {
                        Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
                        for (int i = 0; i < db.getTeamId(user_id).size(); i++) {
                            Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.add(db.getTeamId(user_id).get(i));
                        }
                        for (int i = 0; i < Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.size() ; i++) {
                            team_id = Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.get(i);
                        }
                        for (int i = 0; i < db.getPlayersId(team_id).size(); i++) {
                            Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.remove(db.getPlayersId(team_id).get(i));
                        }
                        Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.remove(user_id);
                        AddTeammates.addTeammatesAdapter.notifyDataSetChanged();
                    } else {
                        Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.remove(user_id);
                    }
                }
            }
        });

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView profile_image_teammates;
        TextView userFullname_add_teammates;
        CheckBox add_teammates_checkbox;
        RelativeLayout profile_image_layout;
        View view_add_teammate;
        public MyViewHolder(View view) {
            super(view);
            profile_image_teammates = (ImageView) view.findViewById(R.id.profile_image_teammates);
            userFullname_add_teammates = (TextView) view.findViewById(R.id.userFullname_add_teammates);
            add_teammates_checkbox = (CheckBox) view.findViewById(R.id.add_teammates_checkbox);
            profile_image_layout = (RelativeLayout) view.findViewById(R.id.profile_image_layout);
            view_add_teammate = (View) view.findViewById(R.id.view_add_teammate);
        }
    }
}