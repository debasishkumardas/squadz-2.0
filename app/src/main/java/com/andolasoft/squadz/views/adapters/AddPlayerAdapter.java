package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.AddNewTeam;
import com.andolasoft.squadz.activities.Profile;
import com.andolasoft.squadz.activities.TeamPlayerPicker;
import com.andolasoft.squadz.models.AddPlayerModel;
import com.andolasoft.squadz.models.FriendsModel;
import com.andolasoft.squadz.models.ProfileOtherViewModel;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddPlayerAdapter extends
        RecyclerView.Adapter<AddPlayerAdapter.MyViewHolder> {

    public static String userId,auth_token;
    SnackBar snackBar;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    private Activity context;
    ArrayList<AddPlayerModel> userModelArray;
    String sportName = "";
    boolean isFirstTime = true;
    boolean isChecked = false;

    public AddPlayerAdapter(Activity context, ArrayList<AddPlayerModel> userarray, String sportName) {
        this.context = context;
        snackBar = new SnackBar(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs = context.getSharedPreferences("AddPlayerPrefs", Context.MODE_PRIVATE);
        editor = prefs.edit();
        this.userModelArray = userarray;
        this.sportName = sportName;
        Constants.ADD_PLAYER_ARRAYLIST = new ArrayList<>();
        Constants.ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_player_list_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return userModelArray.size();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final String userFirstName = userModelArray.get(position).getFirstName();
        final String lastName = userModelArray.get(position).getLastName();
        final String image = userModelArray.get(position).getImage();
        final String player_id = userModelArray.get(position).getUserId();
        final String userName = userModelArray.get(position).getUserName();
        final boolean selected = userModelArray.get(position).isCheckboxSelected();

        if(!TextUtils.isEmpty(userFirstName) ||
                !TextUtils.isEmpty(lastName) && !lastName.equalsIgnoreCase("null")){
            holder.tv_userFullname.setText(userFirstName+" "+ lastName);
        } else if(!TextUtils.isEmpty(userFirstName) && !userFirstName.equalsIgnoreCase("null")) {
            holder.tv_userFullname.setText(userFirstName);
        } else{
            holder.tv_userFullname.setText(userModelArray.get(position).getUserName());
        }

        holder.add_player_checkbox.setTag(position);

        String userProfileImage = userModelArray.get(position).getImage();
        if(!TextUtils.isEmpty(userProfileImage) &&
                !userProfileImage.equalsIgnoreCase("")) {
            if(!userProfileImage.equalsIgnoreCase("null") &&
                    !TextUtils.isEmpty(userProfileImage) &&
                    userProfileImage != null){
                Glide.with(context).load(userProfileImage).into(holder.userProfileImage);
            }
        }

        /**
         * Check existing data if selected
         */
        Set<String> set = null;
        if(Constants.IS_EDIT_BUTTON_CLICKED) {
            set = prefs.getStringSet(Constants.EDIT_NAME_SPORT_NAME, null);
        } else{
            set = prefs.getStringSet(sportName, null);
        }
        holder.add_player_checkbox.setTag(position); // This line is important.

        if (set != null) {
            Constants.ADD_PLAYER_ARRAYLIST = new ArrayList<String>(set);

            if (Constants.ADD_PLAYER_ARRAYLIST.contains(userName)) {
                holder.add_player_checkbox.setChecked(true);
                isFirstTime = false;
            } else{
                holder.add_player_checkbox.setChecked(false);
            }
        }


        if(isFirstTime) {
            if(userModelArray.get(position).isCheckboxSelected()) {
                holder.add_player_checkbox.setChecked(userModelArray.get(position).isCheckboxSelected());
            } else{
                holder.add_player_checkbox.setChecked(userModelArray.get(position).isCheckboxSelected());
            }
        }

//        holder.add_player_checkbox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(selected)
//                {
//                    isChecked = false;
//                    userModelArray.get(position).setCheckboxSelected(false);
//                    holder.add_player_checkbox.setChecked(false);
//                    //userModelArray.get(pos).setCheckboxSelected(true);
//                    Constants.ADD_PLAYER_ARRAYLIST.remove(userName);
//                    TeamPlayerPicker.team_name_count.setText("("+Constants.ADD_PLAYER_ARRAYLIST.size()+")");
//
//                    /**Keeping records for store in database*/
//                    Constants.ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST.remove(player_id+"@@@@"+userName+"@@@@"+userFirstName+"@@@@"+lastName+"@@@@"+image);
//
//                }
//                else{
//                    isChecked = true;
//                    userModelArray.get(position).setCheckboxSelected(true);
//                    holder.add_player_checkbox.setChecked(true);
//                    //userModelArray.get(pos).setCheckboxSelected(false);
//                    Constants.ADD_PLAYER_ARRAYLIST.add(userName);
//                    TeamPlayerPicker.team_name_count.setText("("+Constants.ADD_PLAYER_ARRAYLIST.size()+")");
//
//                    /**Keeping records for store in database*/
//                    Constants.ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST.add(player_id+"@@@@"+userName+"@@@@"+userFirstName+"@@@@"+lastName+"@@@@"+image);
//
//                }
//            }
//        });
        holder.add_player_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                int getPosition = (Integer) buttonView.getTag();
                userModelArray.get(getPosition).setCheckboxSelected(buttonView.isChecked());

                if(isChecked) {
                    //userModelArray.get(position).setCheckboxSelected(true);
                    Constants.ADD_PLAYER_ARRAYLIST.add(userName);
                    TeamPlayerPicker.team_name_count.setText("("+Constants.ADD_PLAYER_ARRAYLIST.size()+")");

                    /**Keeping records for store in database*/
                    Constants.ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST.add(player_id+"@@@@"+userName+"@@@@"+userFirstName+"@@@@"+lastName+"@@@@"+image);
                }
                else{
                    //userModelArray.get(position).setCheckboxSelected(false);
                    Constants.ADD_PLAYER_ARRAYLIST.remove(userName);
                    TeamPlayerPicker.team_name_count.setText("("+Constants.ADD_PLAYER_ARRAYLIST.size()+")");

                    /**Keeping records for store in database*/
                    Constants.ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST.remove(player_id+"@@@@"+userName+"@@@@"+userFirstName+"@@@@"+lastName+"@@@@"+image);
                }
            }
        });
        /**Checkbox click*/

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView userProfileImage;
        TextView tv_userFullname;
        CheckBox add_player_checkbox;
        public MyViewHolder(View view) {
            super(view);
            userProfileImage = (ImageView) view.findViewById(R.id.profile_image_add_player);
            tv_userFullname = (TextView) view.findViewById(R.id.userFullname_add_player);
            add_player_checkbox = (CheckBox) view.findViewById(R.id.add_player_checkbox);
        }
    }
}