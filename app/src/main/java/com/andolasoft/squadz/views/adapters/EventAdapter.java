package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.EventDetail;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.models.VenueDetailModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by SpNayak on 1/2/2017.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> implements Filterable {

    public List<EventListingModel> filteredList;
    public List<EventListingModel> previouslist;
    private ArrayList<EventListingModel> EventListingModelList;
    private Activity context;
    private CustomFilter mFilter;
    private ProgressDialog dialog;
    public EventListingModel eventListingModel;
    public static ArrayList<EventListingModel> eventListingModelArrayList;
    SnackBar snackBar;
    SharedPreferences prefs;
    String event_creator_image;
    boolean isAddedToFavorite = false;
    String userId,status, event_startTime, event_endtime;
    String image;
    //public static ArrayList<UserMolel> userModelArary = new ArrayList<>();
    //public static ArrayList<String> userIdArray = new ArrayList<>();
    double currentLatitude, currentLongitude;
    public static GPSTracker gpsTracker;
    public static String event_Id="";



    public EventAdapter(Activity context, ArrayList<EventListingModel> EventListingModelList) {
        this.EventListingModelList = EventListingModelList;
        this.context = context;
        this.previouslist = EventListingModelList;
        filteredList = new ArrayList<EventListingModel>();
        filteredList.addAll(previouslist);
        mFilter = new CustomFilter(EventAdapter.this);
        snackBar = new SnackBar(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        event_creator_image = prefs.getString("image", null);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        userId = prefs.getString("loginuser_id", null);
        getCurrentLocation();
        gpsTracker = new GPSTracker(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_layout_event_listing, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return EventListingModelList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final EventListingModel eventListingModel = EventListingModelList.get(position);
        String price = eventListingModel.getEvent_Price();
        String minimum_price = eventListingModel.getEvent_Minimum_Price();
        String eventName = eventListingModel.getEvent_Name();
        String ratings = eventListingModel.getEvent_Ratings();
        String reviews = eventListingModel.getEvent_Reviews();
        String skillLevel = eventListingModel.getSkill_level();
        String date = eventListingModel.getEventDate();
        String eventStartTime = eventListingModel.getEventStartTime();
        String eventEndTime = eventListingModel.getEventEndTime();
        String creatorUserName = eventListingModel.getCreator_name();
        String totalNumberOfPlayers =  eventListingModel.getCapacity();
        String totalNumberOfParticipants =  eventListingModel.getEvent_Total_Participants();
        String creator_profile_image =  eventListingModel.getEvent_Creator_Image();
        String event_sport_type =  eventListingModel.getEvent_Sports_Type();
        String object_Type =  eventListingModel.getObject_Type();
        boolean isFavorite = eventListingModel.isFavorite();
        String total_price = eventListingModel.getEvent_Total_Price();
        String list_Id = eventListingModel.getList_id();
        String venue_name = eventListingModel.getVenue_name();

        ArrayList<String> imageArray = eventListingModel.getImages_array();

        if(imageArray.size() > 0){
            image = imageArray.get(0);
        }

        if(!TextUtils.isEmpty(skillLevel)){
            holder.tv_eventSkillLevel.setText(skillLevel);
        }

        if(!TextUtils.isEmpty(date)){
            holder.tv_eventDatetime.setText(ApplicationUtility.formattedDate(date) +" - "+eventStartTime +" to " + eventEndTime);
        }

        /**Displaying Price & As low as cost according to the Object type*/
        if(TextUtils.isEmpty(list_Id) && (!object_Type.equalsIgnoreCase("Event") || !object_Type.equalsIgnoreCase("event"))) {
            holder.tv_minimum_price.setVisibility(View.INVISIBLE);
            holder.event_price.setVisibility(View.GONE);
            holder.rating_venue.setVisibility(View.INVISIBLE);
            holder.rating_event_reviews.setVisibility(View.INVISIBLE);
            holder.tv_game_venue_name.setVisibility(View.GONE);
        }
        else if( (object_Type.equalsIgnoreCase("Game") || object_Type.equalsIgnoreCase("game"))) {
            holder.tv_game_venue_name.setVisibility(View.VISIBLE);
            holder.tv_game_venue_name.setText(venue_name);

            if(!TextUtils.isEmpty(total_price)) {
                holder.tv_minimum_price.setVisibility(View.VISIBLE);
                if(!total_price.equalsIgnoreCase("0")){
                    holder.event_price.setText("$ "+total_price+" / Player");
                    holder.tv_minimum_price.setText("As low as $" + ApplicationUtility.twoDigitAfterDecimal(Double.valueOf(minimum_price)));
                }else{
                    holder.event_price.setText("FREE");
                    // holder.tv_minimum_price.setText("FREE");
                    holder.tv_minimum_price.setVisibility(View.GONE);
                }
            } else {
                holder.event_price.setTextColor(Color.parseColor("#000000"));
                holder.event_price.setText("FREE");
            }
        }
        else if(object_Type.equalsIgnoreCase("Event") || object_Type.equalsIgnoreCase("event")) {
            holder.tv_minimum_price.setVisibility(View.VISIBLE);
            holder.tv_game_venue_name.setVisibility(View.VISIBLE);
            holder.tv_game_venue_name.setText(venue_name);

            if (!TextUtils.isEmpty(total_price)) {
                if (!total_price.equalsIgnoreCase("0")) {
                    holder.event_price.setText("$ " + total_price + " / Player");
                } else {
                    holder.event_price.setText("FREE");
                }
            } else {
                holder.event_price.setTextColor(Color.parseColor("#000000"));
                holder.event_price.setText("FREE");
            }
        }

        /*****************************************************************/


        if(!TextUtils.isEmpty(ratings)) {
            holder.rating_venue.setRating(Float.valueOf(ratings));
        } else{
            holder.rating_venue.setRating(0);
        }

        if(!TextUtils.isEmpty(reviews)) {
            holder.rating_event_reviews.setText(reviews + " Reviews");
        } else{
            holder.rating_event_reviews.setText("0 Review");
        }
        holder.tv_totalnoparticiapnts.setText(totalNumberOfParticipants /*+ " of " + totalNumberOfPlayers*/);

        if(!TextUtils.isEmpty(totalNumberOfParticipants) && !TextUtils.isEmpty(totalNumberOfPlayers) && !totalNumberOfPlayers.equalsIgnoreCase("No maximum player capacity")) {
            int leftSpots = Integer.valueOf(totalNumberOfPlayers) - Integer.valueOf(totalNumberOfParticipants);
            holder.tv_eventspots.setText(String.valueOf(leftSpots) + " spots remaining");
        } else{
            holder.tv_eventspots.setText("0 spot remaining");
        }

        //Displaying venue courts image
        if(!TextUtils.isEmpty(image)){
            Glide.with(context).load(image).into(holder.event_listing_image);
        }else{
            //holder.event_listing_image.setBackgroundResource(R.mipmap.ic_account_gray);
            int getVenueImage = SportsImagePicker.getDefaultSportImage(event_sport_type);
            holder.event_listing_image.setBackgroundResource(getVenueImage);
        }

        //Displaying the Event Name
        if(!TextUtils.isEmpty(eventName)){
            holder.tv_event_name.setText(eventName);
        }

        //Displaying the creator name
        if(!TextUtils.isEmpty(creatorUserName)){
            holder.tv_event_creator_name.setText(creatorUserName);
        }else{
            holder.tv_event_creator_name.setText("Name Display");
        }

        //Displaying the Event creator Image
        if(!TextUtils.isEmpty(creator_profile_image.trim())){
            Glide.with(context).load(creator_profile_image).into(holder.imagv_event_creator_image);
        }


        if(isFavorite){
            holder.favorite_button.setFavorite(true);
        }else{
            holder.favorite_button.setFavorite(false);
        }

        double current_loc_latitude = gpsTracker.getLatitude();
        double current_loc_longitude = gpsTracker.getLongitude();


        if(!TextUtils.isEmpty(eventListingModel.getLatitude())) {
            double dest_loc_latitude = Double.valueOf(eventListingModel.getLatitude());
            double dest_loc_longitude = Double.valueOf(eventListingModel.getLongitude());

            double distance = ApplicationUtility.distance(currentLatitude, currentLongitude,
                    dest_loc_latitude, dest_loc_longitude);

            holder.tv_event_miles.setText(String.valueOf(distance) + " miles");
        }

        if(!TextUtils.isEmpty(object_Type)
                && ( object_Type.equalsIgnoreCase("event")
                || object_Type.equalsIgnoreCase("Event"))) {
            holder.tv_minimum_price.setVisibility(View.GONE);
        } else{
            holder.tv_minimum_price.setVisibility(View.VISIBLE);
        }


        holder.favorite_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String eventId = EventListingModelList.get(position).getEvent_Id();
                boolean favorite = EventListingModelList.get(position).isFavorite();
                AppConstants.OBJECT_TYPE_GAME = EventListingModelList.get(position).getObject_Type();
                if (favorite) {
                    makeListFavorite(userId,eventId,false,EventListingModelList,position,holder);
                    //holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_orange);
                } else {
                    makeListFavorite(userId,eventId,true,EventListingModelList,position,holder);
                    //holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_gray);
                }
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.EVENT_FILTER_BTN_CLICK = false;
                Constants.VENUE_IMAGE = eventListingModel.getEvent_Image();
                event_Id = EventListingModelList.get(position).getEvent_Id();
                getVenuesCourtsDetailsInfor(EventListingModelList.get(position).getEvent_Id(), userId);
            }
        });

        image = "";
    }

    /**
     * Function to get Venue courts from server
     */
    public void makeListFavorite(String user_id, String event_id, final boolean isFavorite,
                                 final ArrayList<EventListingModel> eventArray,
                                 final int position, final MyViewHolder holder) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, event_id, isFavorite);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(context, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    isAddedToFavorite = responseObject.getBoolean("isFavorite");
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(context, false);
                        Constants.FAVOURITE_CLICKED = true;
                        if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            if(isAddedToFavorite){
                                snackBar.setSnackBarMessage("Successfully Added to WishList");
                                eventArray.get(position).setFavorite(true);
                                holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_orange);
                                notifyDataSetChanged();
                            }else{
                                snackBar.setSnackBarMessage("Successfully Removed from WishList");
                                eventArray.get(position).setFavorite(false);
                                holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_gray);
                                notifyDataSetChanged();
                            }
                        }else if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                            snackBar.setSnackBarMessage("Failed to Add to WishList");
                        }else{
                            snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE_ERR);
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String userId, String objectId, boolean favoriteStatus) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "add_to_favorite").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("object_id", objectId);

        if(!TextUtils.isEmpty(AppConstants.OBJECT_TYPE_GAME)
                && ( AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")))
        {
            urlBuilder.addQueryParameter("object_type", "event");
        }
        else{
            urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_EVENT);
        }

        if(favoriteStatus){
            urlBuilder.addQueryParameter("is_favorite", Boolean.TRUE.toString());
        }else{
            urlBuilder.addQueryParameter("is_favorite", Boolean.FALSE.toString());
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public RatingBar rating_venue;
        public MaterialFavoriteButton favorite_button;
        public CardView cardView;
        public TextView event_price,rating_event_reviews,
                tv_event_name, tv_eventSkillLevel, tv_eventDatetime,tv_eventspots,
                tv_totalnoparticiapnts, tv_event_creator_name, tv_event_miles,
                tv_minimum_price,tv_game_venue_name;

        public ImageView event_listing_image,imagv_event_participants;
        public CircleImageView imagv_event_creator_image ;

        public MyViewHolder(View view) {
            super(view);

            rating_venue = (RatingBar) view.findViewById(R.id.rating_event_detail);
            favorite_button = (MaterialFavoriteButton) view.findViewById(R.id.event_favorite_button);
            cardView = (CardView) view.findViewById(R.id.card_view_event_list);
            event_price = (TextView) view.findViewById(R.id.event_price);
            rating_event_reviews = (TextView) view.findViewById(R.id.rating_event_reviews);
            event_listing_image = (ImageView) view.findViewById(R.id.event_listing_image);
            imagv_event_creator_image = (CircleImageView) view.findViewById(R.id.event_creator_image);
            imagv_event_participants = (ImageView)view.findViewById(R.id.event_participants_image);
            tv_event_name = (TextView) view.findViewById(R.id.venue_name);
            tv_eventSkillLevel = (TextView) view.findViewById(R.id.event_sport_name);
            tv_eventDatetime = (TextView) view.findViewById(R.id.event_date_time);
            tv_eventspots = (TextView) view.findViewById(R.id.event_spots);
            tv_totalnoparticiapnts = (TextView) view.findViewById(R.id.event_participants);
            tv_event_creator_name = (TextView) view.findViewById(R.id.event_creator_name);
            tv_event_miles = (TextView) view.findViewById(R.id.event_miles);
            tv_minimum_price = (TextView) view.findViewById(R.id.event_Minimum_Amount_Text);
            tv_game_venue_name = (TextView) view.findViewById(R.id.tv_game_venue_name);
        }
    }

    /*********************
     * CLASS FOR CUSTOM FILTER
     *********************************************/
    public class CustomFilter extends Filter {

        private EventAdapter mAdapter;

        private CustomFilter(EventAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(previouslist);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final EventListingModel list : previouslist) {
                    // if (list.getTaskName().toLowerCase().startsWith(filterPattern)) {
                    if (list.getEvent_Name().toLowerCase().contains(filterPattern)) {
                        filteredList.add(list);
                    }
                }
            }
            EventListingModelList = (ArrayList<EventListingModel>) filteredList;
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            notifyDataSetChanged();

            if (((List<EventListingModel>) results.values).size() == 0) {

                snackBar.setSnackBarMessage("No events found");
                notifyDataSetChanged();
            } else {
//                text_contacts.setVisibility(View.GONE);
//                stickyList.setVisibility(View.VISIBLE);
            }
        }

    }

    /**
     * Function to get Venue Courts detail from server
     */
    public void getVenuesCourtsDetailsInfor(String court_Id, String userId) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(court_Id, userId);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, final IOException e) {

                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LoaderUtility.handleLoader(context, false);
                        Toast.makeText(context,""+e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "",venue_id ="",capacity = "", name = "",
                        latitude = "", longitude = "",review_rating = "",
                        review_count = "",court_type = "",description = "",
                        price = "",address = "",city = "",state = "",
                        country = "", total_number_players = "",
                        game_date = "", startTIme = "", endTime = "",
                        skillLevel = "", no_of_participants = "",
                        eventSportName = "", eventCourtName = "",
                        event_creator_name = "", event_creator_image = "",
                        event_creator_user_id = "", minimum_price = "",
                        actual_price = "",visibility = "",note = "",list_id = "",
                        cancel_policy_type = "",cancellation_point = "",object_type = "",
                        venue_name = "",minimum_age = "", space_type = "", space_desc = "",
                        phone = "", website = "", rules = "";
                boolean isFavorite = false, isEquipmenmtAvailable = false;
                ArrayList<String> space_desc_array = new ArrayList<String>();
                ArrayList<String> equipments_array = new ArrayList<String>();
                ArrayList<String> amenities_array = new ArrayList<String>();

                try {
                    //JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    eventListingModelArrayList = new ArrayList<EventListingModel>();
                    eventListingModel = new EventListingModel();
                    ArrayList<String> images_arraylist = new ArrayList<String>();

                    if(jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if(jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if(jsonObject.has("list_id") && !jsonObject.isNull("list_id")) {
                        list_id = jsonObject.getString("list_id");
                    }

                    if(jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                        Constants.EVENT_NAME_MAP = name;
                    }
                    if(jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                        Constants.EVENT_LATITUDE = Double.parseDouble(latitude);
                    }
                    if(jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                        Constants.EVENT_LONGITUDE = Double.parseDouble(longitude);
                    }
                    if(jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }

                    if(jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }

                    if(jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if(jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }
                    if(jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if(jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if(jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }

                    if(jsonObject.has("no_of_player") && !jsonObject.isNull("no_of_player")){
                        total_number_players = jsonObject.getString("no_of_player");
                    }

                    if(jsonObject.has("no_of_partitipants") && !jsonObject.isNull("no_of_partitipants")){
                        no_of_participants = jsonObject.getString("no_of_partitipants");
                    }

                    if(jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")){
                        isFavorite = jsonObject.getBoolean("is_favorite");
                    }
                    if(jsonObject.has("court_capacity") && !jsonObject.isNull("court_capacity")) {
                        capacity = jsonObject.getString("court_capacity");
                    }

                    if(jsonObject.has("user_review")&& !jsonObject.isNull("user_review")) {
                        JSONObject user_review_object = jsonObject.getJSONObject("user_review");
                        VenueAdapter.user_review_arrayList = new ArrayList<String>();
                        if(user_review_object.length() > 0) {
                            String user_rating = user_review_object.getString("rating");
                            String user_review = user_review_object.getString("review");
                            VenueAdapter.user_review_arrayList.add(user_rating + "@@@@" + user_review);
                        }
                    }

                    if(jsonObject.has("price") && !jsonObject.isNull("price")) {
                        try{
                            price = jsonObject.getString("price");
                            int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                            float price_pre_player = Float.parseFloat(price) / (float)totalPlayer;
                            actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                            float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                            minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                        }catch(Exception ex){
                            ex.printStackTrace();;
                        }
                    }

                    if(jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")){
                        eventSportName = jsonObject.getString("sport_name");
                    }

                    if(jsonObject.has("game_date") && !jsonObject.isNull("game_date")){
                        game_date = jsonObject.getString("game_date");
                    }

                    if(jsonObject.has("skill_level") && !jsonObject.isNull("skill_level")){
                        skillLevel = jsonObject.getString("skill_level");
                    }

                    if(jsonObject.has("list_name") && !jsonObject.isNull("list_name")){
                        eventCourtName = jsonObject.getString("list_name");
                    }

                    if(jsonObject.has("user_name") && !jsonObject.isNull("user_name")){
                        event_creator_name = jsonObject.getString("user_name");
                    }

                    if(jsonObject.has("user_id") && !jsonObject.isNull("user_id")){
                        event_creator_user_id = jsonObject.getString("user_id");
                    }

                    if(jsonObject.has("user_profile_image") && !jsonObject.isNull("user_profile_image")){
                        event_creator_image = jsonObject.getString("user_profile_image");
                    }
                    if(jsonObject.has("visibility") && !jsonObject.isNull("visibility")){
                        visibility = jsonObject.getString("visibility");
                    }
                    if(jsonObject.has("note") && !jsonObject.isNull("note")) {
                        note = jsonObject.getString("note");
                    }

                    if(jsonObject.has("cancel_policy_type")&& !jsonObject.isNull("cancel_policy_type"))
                    {
                        cancel_policy_type = jsonObject.getString("cancel_policy_type");
                    }
                    if(jsonObject.has("cancellation_point")&& !jsonObject.isNull("cancellation_point"))
                    {
                        cancellation_point = jsonObject.getString("cancellation_point");
                    }
                    if(jsonObject.has("object_type")&& !jsonObject.isNull("object_type"))
                    {
                        object_type = jsonObject.getString("object_type");
                    }
                    if(jsonObject.has("venue_name")&& !jsonObject.isNull("venue_name"))
                    {
                        venue_name = jsonObject.getString("venue_name");
                    }

                    if(jsonObject.has("equipments") && !jsonObject.isNull("equipments")){
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");

                        if(equipmentArray.length() > 0){
                            isEquipmenmtAvailable = true;
                        }
                    }

                    if(jsonObject.has("time_slot")&& !jsonObject.isNull("time_slot")) {
                        String start_time = null;
                        JSONArray time_slot_array_json_array = jsonObject.getJSONArray("time_slot");
                        for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                            JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                            if(j == 0){
                                startTIme = getValues.getString("start_time");
                            }
                            endTime = getValues.getString("end_time");
                        }
                    }


                    if(jsonObject.has("images")&& !jsonObject.isNull("images")) {
                        ArrayList<String> images_array = new ArrayList<String>();
                        JSONArray images_json_array = jsonObject.getJSONArray("images");
                        for (int j = 0; j < images_json_array.length(); j++) {
                            images_array.add(images_json_array.getString(j));
                        }
                        eventListingModel.setImages_array(images_array);
                    }


                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        JSONObject add_info_json_object = jsonObject.getJSONObject("additional_info");

                        JSONArray jsonArray = add_info_json_object.getJSONArray("space_desc");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String value = jsonArray.get(i).toString();
                            space_desc_array.add(value);
                        }

                        if(add_info_json_object.has("equipments")&& !add_info_json_object.isNull("equipments")) {

                            JSONArray equipments_json_array = add_info_json_object.getJSONArray("equipments");
                            for (int i = 0; i < equipments_json_array.length(); i++) {
                                String value = equipments_json_array.get(i).toString();
                                equipments_array.add(value);
                            }

                        }
                        if(add_info_json_object.has("amenities")&& !add_info_json_object.isNull("amenities")) {

                            JSONArray amenities_json_array = add_info_json_object.getJSONArray("amenities");
                            for (int i = 0; i < amenities_json_array.length(); i++) {
                                String value = amenities_json_array.get(i).toString();
                                amenities_array.add(value);
                            }
                        }
                        if(add_info_json_object.has("phone")&& !add_info_json_object.isNull("phone")) {
                            phone = add_info_json_object.getString("phone");
                        }
                        if(add_info_json_object.has("website")&& !add_info_json_object.isNull("website")) {
                            website = add_info_json_object.getString("website");
                        }
                        if(add_info_json_object.has("rules")&& !add_info_json_object.isNull("rules")) {
                            rules = add_info_json_object.getString("rules");
                        }

                        eventListingModel.setAdditional_space_desc_array(space_desc_array);
                        eventListingModel.setAdditional_equipmentes_array(equipments_array);
                        eventListingModel.setAdditional_amenities_array(amenities_array);
                        eventListingModel.setAdditional_phone_number(phone);
                        eventListingModel.setAdditional_website(website);
                        eventListingModel.setAdditional_venue_rules(rules);
                    }


                    if(jsonObject.has("minimum_age")&& !jsonObject.isNull("minimum_age")) {
                        minimum_age = jsonObject.getString("minimum_age");
                        if(minimum_age != null && !minimum_age.equalsIgnoreCase("No minimum age required")){
                            minimum_age = minimum_age + " + ";
                        }
                    }

                    if(jsonObject.has("space_type")&& !jsonObject.isNull("space_type")) {
                        space_type = jsonObject.getString("space_type");
                    }

                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        String space = "";
                        JSONObject jsonObject_additional_info = jsonObject.getJSONObject("additional_info");

                        if(jsonObject_additional_info.has("space_desc") && !jsonObject_additional_info.isNull("space_desc")){
                            JSONArray spaceArray = jsonObject_additional_info.getJSONArray("space_desc");
                            for (int i = 0; i < spaceArray.length(); i++) {

                                // if(!TextUtils.isEmpty(space)){
                                space = space + "," + spaceArray.get(i);
                                // }
                            }

                            if(TextUtils.isEmpty(space)){
                                space_desc = "No Space Available";
                            }else {
                                if (space != null
                                        && space
                                        .length() > 1) {
                                    space_desc = space.substring(1,space.length());
                                }
                                //space_desc = space;
                            }
                        }
                    }


                    if(jsonObject.has("game_partitipants")&& !jsonObject.isNull("game_partitipants")){

                        try{
                            JSONArray particiapntsArray = jsonObject.getJSONArray("game_partitipants");

                            AppConstants.userModelArary = new ArrayList<UserMolel>();
                            AppConstants.userIdArray = new ArrayList<String>();
                            if(particiapntsArray.length() > 0){
                                for(int i = 0; i < particiapntsArray.length(); i++){
                                    UserMolel model = new UserMolel();
                                    JSONObject participantsObj = particiapntsArray.getJSONObject(i);
                                    String userName = participantsObj.getString("user_name");
                                    String userId = participantsObj.getString("user_id");
                                    String user_profile_image = participantsObj.getString("user_profile_image");
                                    model.setUserFullName(userName);
                                    model.setUserId(userId);
                                    model.setUserProfileImage(user_profile_image);
                                    AppConstants.userIdArray.add(userId);
                                    AppConstants.userModelArary.add(model);
                                }
                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }



                    eventListingModel.setEvent_Id(id);
                    eventListingModel.setEvent_Name(name);
                    eventListingModel.setList_id(list_id);
                    eventListingModel.setLatitude(latitude);
                    eventListingModel.setLongitude(longitude);
                    eventListingModel.setEvent_Ratings(review_rating);
                    eventListingModel.setEvent_Reviews(review_count);
                    eventListingModel.setEvent_Price(actual_price);
                    eventListingModel.setCourt_type(court_type);
                    eventListingModel.setDescription(description);
                    eventListingModel.setAddress(address);
                    eventListingModel.setVisibility(visibility);
                    eventListingModel.setNote(note);
                    eventListingModel.setCancel_policy_type(cancel_policy_type);
                    eventListingModel.setCancellation_point(cancellation_point);
                    eventListingModel.setCity(city);
                    eventListingModel.setState(state);
                    eventListingModel.setCountry(country);
                    eventListingModel.setCapacity(capacity);
                    eventListingModel.setEvent_Total_Participants(total_number_players);
                    eventListingModel.setEventDate(game_date);
                    eventListingModel.setEventStartTime(startTIme);
                    eventListingModel.setEventEndTime(endTime);
                    eventListingModel.setSkill_level(skillLevel);
                    eventListingModel.setEvent_accepted_Participants(no_of_participants);
                    eventListingModel.setEvent_Sports_Type(eventSportName);
                    eventListingModel.setEvent_court_name(eventCourtName);
                    eventListingModel.setEvent_Creator_Image(event_creator_image);
                    eventListingModel.setCreator_name(event_creator_name);
                    eventListingModel.setCreator_user_id(event_creator_user_id);
                    eventListingModel.setEvent_Minimum_Price(minimum_price);
                    eventListingModel.setFavorite(isFavorite);
                    eventListingModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    eventListingModel.setObject_Type(object_type);
                    eventListingModel.setEvent_Total_Price(price);
                    eventListingModel.setCourt_min_players(minimum_age);
                    eventListingModel.setCourt_space_desc(space_desc);
                    eventListingModel.setSpace_type(space_type);
                    eventListingModel.setVenue_name(venue_name);
                    eventListingModelArrayList.add(eventListingModel);

                }catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(context, false);

                if(isSuccessStatus) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            context.finish();
                            Intent intent = new Intent(context, EventDetail.class);
                            context.startActivity(intent);
                        }
                    });
                }
                else
                {
                    snackBar.setSnackBarMessage("Network error! Please try after sometime");
                }


            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String court_Id, String userId) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_game_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.EVENT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", userId);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(context, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    public void getCurrentLocation(){
        GPSTracker gps = new GPSTracker(context);

        if(gps.canGetLocation()){
            currentLatitude = gps.getLatitude();
            currentLongitude = gps.getLongitude();
        }
    }

}
