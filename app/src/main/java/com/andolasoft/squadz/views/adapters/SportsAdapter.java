package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.SportsImagePicker;

import java.util.ArrayList;

/**
 * Created by SpNayak on 1/3/2017.
 */

public class SportsAdapter extends BaseAdapter {
    private ArrayList<String> sport_array = new ArrayList<String>();
    private LayoutInflater layoutInflater;
    private Context context;
    SportsImagePicker sportsImagePicker;

    public SportsAdapter(Context context, ArrayList<String> sport_array) {
        this.sport_array = sport_array;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
        sportsImagePicker = new SportsImagePicker();
    }

    @Override
    public int getCount() {
        return sport_array.size();
    }

    @Override
    public Object getItem(int position) {
        return sport_array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_sports_category, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        String[] split_sport = sport_array.get(position).split("###");
        final String sport_name = split_sport[0].trim();
        String sport_id = split_sport[1].trim();

        if(sport_name.equals(Constants.EVENT_FILTER_SPORT_NAME))
        {
            mViewHolder.txt_sports_name.setText(sport_name);
            mViewHolder.txt_sports_name.setTextColor(context.getResources().getColor(R.color.orange));
        }
        else{
            mViewHolder.txt_sports_name.setText(sport_name);
            mViewHolder.txt_sports_name.setTextColor(context.getResources().getColor(R.color.gray_new));
        }

        mViewHolder.ic_sports.setBackgroundResource(sportsImagePicker.getSportImage(sport_name));

        mViewHolder.sports_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.EVENT_FILTER_SPORT_NAME = mViewHolder.txt_sports_name.getText().toString().trim();
                ((Activity)context).finish();

            }
        });

        return convertView;
    }

    private class MyViewHolder {
        TextView txt_sports_name;
        ImageView ic_sports;
        RelativeLayout sports_layout;

        public MyViewHolder(View item) {
            txt_sports_name = (TextView) item.findViewById(R.id.sports_name);
            ic_sports = (ImageView) item.findViewById(R.id.sports_image);
            sports_layout = (RelativeLayout) item.findViewById(R.id.sports_layout);

        }
    }
}

