package com.andolasoft.squadz.views.actionbar;

import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

import com.andolasoft.squadz.R;

/**
 * Created by Debasish Kumar Das on 1/11/2017.
 */
public class StatusBarUtils {

    /*
     * Function to handle the Change StatusBar Color
     */
    public static void changeStatusBarColor(Activity ctx) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = ctx.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ctx.getResources().getColor(R.color.gray3));
        }
    }
}
