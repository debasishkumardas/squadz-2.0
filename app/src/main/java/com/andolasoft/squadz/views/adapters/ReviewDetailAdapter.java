package com.andolasoft.squadz.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.ReviewDetailModel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by SpNayak on 1/3/2017.
 */

public class ReviewDetailAdapter extends RecyclerView.Adapter<ReviewDetailAdapter.MyViewHolder> {
    String image = "";
    private ArrayList<ReviewDetailModel> reviewDetailModelArrayList = new ArrayList<ReviewDetailModel>();
    private LayoutInflater layoutInflater;
    private Context context;

    public ReviewDetailAdapter(Context context, ArrayList<ReviewDetailModel> reviewDetailModelArrayList) {
        this.reviewDetailModelArrayList = reviewDetailModelArrayList;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
    }

//    @Override
//    public int getCount() {
//        return reviewDetailModelArrayList.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return reviewDetailModelArrayList.get(position);
//    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_review_detail_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder mViewHolder, int position) {

        image = reviewDetailModelArrayList.get(position).getReviewed_by_creator_image();
        if (!TextUtils.isEmpty(image)
                && !image.equalsIgnoreCase("null")
                && image != null) {
            Glide.with(context)
                    .load(image)
                    //.placeholder(R.mipmap.loading_placeholder_icon)
                    .into(mViewHolder.reviewed_by_creator_image);
        }
        else{
            mViewHolder.reviewed_by_creator_image.setBackgroundResource(R.mipmap.ic_account_gray);
        }

        String date = reviewDetailModelArrayList.get(position).getReviewed_by_date();
        mViewHolder.reviewed_by_creator_name.setText(reviewDetailModelArrayList.get(position).getReviewed_by_creator_name());
        mViewHolder.reviewed_by_date.setText(ApplicationUtility.customReviewDateFormat(date));
        mViewHolder.reviewed_by_creator_description.setText(reviewDetailModelArrayList.get(position).getReviewed_by_creator_description());

        String ratings = reviewDetailModelArrayList.get(position).getReviewed_by_ratings();
        if (!TextUtils.isEmpty(ratings)) {
            mViewHolder.rating_list.setRating(Float.valueOf(reviewDetailModelArrayList.get(position).getReviewed_by_ratings()));
        } else {
            mViewHolder.rating_list.setRating(0);
        }

        image = null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return reviewDetailModelArrayList.size();
    }

//    public View getView(final int position, View convertView, ViewGroup parent) {
//
//        MyViewHolder mViewHolder;
//        if (convertView == null) {
//            convertView = layoutInflater.inflate(R.layout.adapter_review_detail_layout, parent, false);
//            mViewHolder = new MyViewHolder(convertView);
//            convertView.setTag(mViewHolder);
//        } else {
//            mViewHolder = (MyViewHolder) convertView.getTag();
//        }
//
//        image = reviewDetailModelArrayList.get(position).getReviewed_by_creator_image();
//        if(!TextUtils.isEmpty(image)
//                && !image.equalsIgnoreCase("null")
//                && image != null)
//        {
//            Glide.with(context)
//                    .load(image)
//                    .placeholder(R.mipmap.loading_placeholder_icon)
//                    .into(mViewHolder.reviewed_by_creator_image);
//        }
////        else{
////            mViewHolder.reviewed_by_creator_image.setBackgroundResource(R.mipmap.ic_account_gray);
////        }
//
//        String date = reviewDetailModelArrayList.get(position).getReviewed_by_date();
//        mViewHolder.reviewed_by_creator_name.setText(reviewDetailModelArrayList.get(position).getReviewed_by_creator_name());
//        mViewHolder.reviewed_by_date.setText(ApplicationUtility.customReviewDateFormat(date));
//        mViewHolder.reviewed_by_creator_description.setText(reviewDetailModelArrayList.get(position).getReviewed_by_creator_description());
//
//        String ratings = reviewDetailModelArrayList.get(position).getReviewed_by_ratings();
//        if(!TextUtils.isEmpty(ratings)) {
//            mViewHolder.rating_list.setRating(Float.valueOf(reviewDetailModelArrayList.get(position).getReviewed_by_ratings()));
//        }
//        else{
//            mViewHolder.rating_list.setRating(0);
//        }
//
//        return convertView;
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView reviewed_by_creator_image;
        TextView reviewed_by_creator_name,
                reviewed_by_date,
                reviewed_by_creator_description;
        RatingBar rating_list;

        public MyViewHolder(View view) {
            super(view);
            reviewed_by_creator_image = (CircleImageView) view.findViewById(R.id.reviewed_by_creator_image);
            reviewed_by_creator_name = (TextView) view.findViewById(R.id.reviewed_by_creator_name);
            reviewed_by_date = (TextView) view.findViewById(R.id.reviewed_by_date);
            reviewed_by_creator_description = (TextView) view.findViewById(R.id.reviewed_by_creator_description);
            rating_list = (RatingBar) view.findViewById(R.id.rating_list);
        }
    }
}

