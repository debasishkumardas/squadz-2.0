package com.andolasoft.squadz.views.adapters;

/**
 * Created by SpNayak on 1/15/2017.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.VenueDetails;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class ImageSlideAdapter extends PagerAdapter {
    // Declare Variables
    Context context;

    ArrayList<String> images_array;
    LayoutInflater inflater;


    public ImageSlideAdapter(Context context, ArrayList<String> images_array) {
        this.context = context;
        this.images_array = images_array;

    }

    @Override
    public int getCount() {
        return images_array.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // Declare Variables

        ImageView venue_image_infowindow;
        View itemView;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        itemView = inflater.inflate(R.layout.image_slide, container,
                false);


        venue_image_infowindow = (ImageView) itemView.findViewById(R.id.venue_image_pager);
        //rightNav = (ImageButt,on) itemView.findViewById(R.id.right_nav);

        if(images_array.get(position) != null) {
            Glide.with(context).load(images_array.get(position))
                    .placeholder(R.mipmap.loading_placeholder_icon).into(venue_image_infowindow);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayImagePopUp(context,images_array);
            }
        });
        //venue_image_infowindow.setImageResource(flag[position]);

        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);

    }

    /**
     * Display Menu pop up window
     */
    public void displayImagePopUp(Context context,ArrayList<String> image_array)
    {
        final Dialog dialog = new Dialog(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.venue_detail_image_popup);

        ViewPager viewpager_popup = (ViewPager) dialog.findViewById(R.id.viewpager_popup);
        Button close_btn = (Button) dialog.findViewById(R.id.close_btn);

        ImageSlideAdapter imageSlideAdapter = new ImageSlideAdapter(context,image_array);
        viewpager_popup.setAdapter(imageSlideAdapter);

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}

