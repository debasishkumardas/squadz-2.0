package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.andolasoft.squadz.R;

public class NavigationDrawerAdapter extends BaseAdapter {

    String[] str_item_arry;
    int[] after_login_icon_array = new int[]{R.mipmap.ic_home_gray,R.mipmap.ic_mysport, R.mipmap.ic_group_white_36dp,
            R.mipmap.ic_billing, R.mipmap.ic_radius_gray, R.mipmap.ic_favorite_border_gray,
            R.mipmap.ic_privecy, R.mipmap.ic_lock_gray, R.drawable.ic_settings_black_48dp, R.mipmap.ic_faq_gray,
            R.mipmap.ic_terms, R.mipmap.ic_logout};

    int[] facebook_login_icon_array = new int[]{R.mipmap.ic_home_gray,R.mipmap.ic_mysport, R.mipmap.ic_group_white_36dp,
            R.mipmap.ic_billing, R.mipmap.ic_radius_gray, R.mipmap.ic_favorite_border_gray,
            R.mipmap.ic_privecy,R.drawable.ic_settings_black_48dp, R.mipmap.ic_faq_gray,
            R.mipmap.ic_terms, R.mipmap.ic_logout};

    String login_Status;
    LayoutInflater mInflater;
    private Context context;
    String radius_count,cards_count,wish_lists_count;

    public NavigationDrawerAdapter(Context context, String[] str_item_arry, String login_Status, String radius_count,String cards_count,String wish_lists_count) {
        this.context = context;
        this.str_item_arry = str_item_arry;
        this.login_Status = login_Status;
        this.radius_count = radius_count;
        this.cards_count = cards_count;
        this.wish_lists_count = wish_lists_count;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return str_item_arry.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = null;
        if (convertView == null) {
//            mInflater = (LayoutInflater)
//                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
            TextView text_item = (TextView) convertView.findViewById(R.id.text_item);
            TextView count_item = (TextView) convertView.findViewById(R.id.count_item);
            ImageView menu_icon = (ImageView) convertView.findViewById(R.id.menu_icon);
            View divider_view = (View) convertView.findViewById(R.id.divider_view);

            text_item.setText(str_item_arry[position]);
            if(login_Status.equalsIgnoreCase("FACEBOOK_LOGIN"))
            {
                menu_icon.setBackgroundResource(facebook_login_icon_array[position]);
            }
            else {
                menu_icon.setBackgroundResource(after_login_icon_array[position]);
            }

            if (position == 2) {
                divider_view.setVisibility(View.VISIBLE);
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) divider_view.getLayoutParams();
                marginParams.setMargins(0, 60, 0, 0);
            }
            if (position == 5) {
                divider_view.setVisibility(View.VISIBLE);
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) divider_view.getLayoutParams();
                marginParams.setMargins(0, 70, 0, 0);
            }
            if (position == 6 || position == 7) {
                divider_view.setVisibility(View.VISIBLE);
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) divider_view.getLayoutParams();
                marginParams.setMargins(0, 6, 0, 0);
            }
            if (position == 3 ) {
                if(!cards_count.equalsIgnoreCase("0"))
                {
                    count_item.setVisibility(View.VISIBLE);
                    count_item.setText(cards_count);
                }
                else{
                    count_item.setVisibility(View.INVISIBLE);
                }

            }
            if(position == 5)
            {
                if(!wish_lists_count.equalsIgnoreCase("0"))
                {
                    count_item.setVisibility(View.VISIBLE);
                    count_item.setText(wish_lists_count);
                }
                else{
                    count_item.setVisibility(View.INVISIBLE);
                }

            }
            if (position == 4) {
                if(!radius_count.equalsIgnoreCase("0"))
                {
                    count_item.setVisibility(View.VISIBLE);
                    count_item.setText(radius_count + " Miles");
                }
                else{
                    count_item.setVisibility(View.INVISIBLE);
                }

            }
            if(login_Status.equalsIgnoreCase("FACEBOOK_LOGIN"))
            {
                if (position == 6 || position == 7 ||
                        position == 8 || position == 9 ||
                        position == 10  ) {
                    text_item.setTextColor(context.getResources().getColor(R.color.gray));
                    text_item.setTextSize(13);
                }
            }
            else {
                if (position == 6 || position == 7 ||
                        position == 8 || position == 9 ||
                        position == 10 || position == 11 ) {
                    text_item.setTextColor(context.getResources().getColor(R.color.gray));
                    text_item.setTextSize(13);
                }
            }


        }

        return convertView;
    }
}
