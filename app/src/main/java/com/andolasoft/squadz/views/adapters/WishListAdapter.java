package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.EventDetail;
import com.andolasoft.squadz.activities.VenueDetails;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.models.VenueDetailModel;
import com.andolasoft.squadz.models.VenueModel;
import com.andolasoft.squadz.models.WishListModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by SpNayak on 1/2/2017.
 */

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.MyViewHolder> implements Filterable {

    public static WishListAdapter venueAdapter;
    public List<WishListModel> filteredList;
    public List<WishListModel> previouslist;
    private Activity context;
    private CustomFilter mFilter;
    private ProgressDialog dialog;
    public VenueDetailModel venueDetailModel;
    SnackBar snackBar;
    SharedPreferences prefs;
    public static String userId;
    boolean isAddedToFavorite = false;
    String status;
    public static String venueId;
    public static ArrayList<String> images_array;
    public static double court_latitude, court_longitude;
    public static GPSTracker gpsTracker;
    private ArrayList<WishListModel> wishListModelsList;
    String image;
    double currentLatitude, currentLongitude;
    public static ArrayList<VenueDetailModel> venueDetailModelArrayList;
    public static ArrayList<EventListingModel> eventListingModelArrayList;
    EventListingModel eventListingModel;
    public static ArrayList<String> userIdArray = new ArrayList<>();
    public static ArrayList<UserMolel> userModelArary = new ArrayList<>();


    public WishListAdapter(Activity context, ArrayList<WishListModel> wishListModelsList) {
        this.wishListModelsList = wishListModelsList;
        this.context = context;
        this.previouslist = wishListModelsList;
        filteredList = new ArrayList<WishListModel>();
        filteredList.addAll(previouslist);
        mFilter = new CustomFilter(WishListAdapter.this);
        snackBar = new SnackBar(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        userId = prefs.getString("loginuser_id", null);
        venueAdapter = this;
        gpsTracker = new GPSTracker(context);
        getCurrentLocation();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_layout_wishlist, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return wishListModelsList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final WishListModel wishListModel = wishListModelsList.get(position);
        String object_type = wishListModel.getObject_type();
        holder.card_view_venue_list.setVisibility(View.GONE);
        holder.cardView.setVisibility(View.GONE);

        if(object_type.equalsIgnoreCase("List")) {
            holder.card_view_venue_list.setVisibility(View.VISIBLE);
            //Set Court data
            setListData(holder, position, wishListModel,wishListModelsList);
        }
        else{
            holder.cardView.setVisibility(View.VISIBLE);
            //Set Event data
            setEventData(holder, position, wishListModel,wishListModelsList);
        }

    }

    /**
     * Set Court data
     */
    public void setListData(final MyViewHolder holder, final int position,
                            final WishListModel wishListModel,
                            final ArrayList<WishListModel> wishListModelsList)
    {
        boolean isFavourite = wishListModel.isFavorite();
        if(isFavourite) {
            holder.favorite_button.setFavorite(true);
            holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_orange);
        }
        else{
            holder.favorite_button.setFavorite(false);
            holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_gray);
        }

        String price = wishListModel.getPrice();
        if(!TextUtils.isEmpty(price) &&
                !price.equalsIgnoreCase("0")) {
            holder.venue_price.setText("$ "+wishListModel.getPrice());
        }
        else{
            holder.venue_price.setTextColor(Color.parseColor("#000000"));
            holder.venue_price.setText("FREE");
        }
        holder.venue_name.setText(wishListModel.getName());
        holder.rating_venue_reviews.setText(wishListModel.getReview_count()+" Reviews");
        holder.rating_venue.setRating(Float.parseFloat(wishListModel.getRatings()));

        //Displaying venue courts image
        //String venueImage = venueModel.getVenueImage();

        images_array = wishListModel.getImages_array();
        if(images_array.size() > 0) {
            if(!TextUtils.isEmpty(images_array.get(0))){
                Glide.with(context).load(images_array.get(0)).placeholder(R.mipmap.loading_placeholder_icon).into(holder.venue_image);
            }
        }
        else{
            //This image will display if no image will be found
            holder.venue_image.setBackgroundResource(R.mipmap.ic_no_image_found);
        }

        //Calculating distance from current to destination
        /*double current_loc_latitude = HomeFragment.currLocLatitude;
        double current_loc_longitude = HomeFragment.currLocLongitude;*/
        double current_loc_latitude = gpsTracker.getLatitude();
        double current_loc_longitude = gpsTracker.getLongitude();

        if(!TextUtils.isEmpty(wishListModel.getLatitude())) {
            double dest_loc_latitude = Double.valueOf(wishListModel.getLatitude());
            double dest_loc_longitude = Double.valueOf(wishListModel.getLongitude());

            double distance = ApplicationUtility.distance(current_loc_latitude, current_loc_longitude,
                    dest_loc_latitude, dest_loc_longitude);

            holder.text_venue_miles.setText(String.valueOf(distance) + " miles");
        }
        else{
            holder.card_view_venue_list.setVisibility(View.GONE);
        }

        holder.card_view_venue_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.VENUE_FILTER_BTN_CLICK = false;
                Constants.UNCHECK_SPEC = false;

                court_latitude = Double.valueOf(wishListModel.getLatitude());
                court_longitude = Double.valueOf(wishListModel.getLongitude());
                /*court_latitude = gpsTracker.getLatitude();
                court_longitude = gpsTracker.getLongitude();*/

                images_array = wishListModel.getImages_array();
                venueId = wishListModel.getId();
                String venue_name = wishListModel.getName();
                Constants.VENUE_NAME = venue_name;

                String price = wishListModel.getPrice();
                if(!TextUtils.isEmpty(price)&&
                        !price.equalsIgnoreCase("0")) {
                    Constants.VENUE_PRICE = price;
                }
                else{
                    Constants.VENUE_PRICE = "FREE";
                }
                String venueImage = wishListModel.getImage().toString();
                Constants.VENUE_IMAGE = venueImage;
                VenueAdapter.venueId = wishListModel.getId();
                getVenuesCourtsDetailsInfor(wishListModel.getId());

            }
        });

        holder.favorite_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                venueId = wishListModel.getId();
                boolean isFavourite =  wishListModel.isFavorite();
                AppConstants.OBJECT_TYPE_GAME = "";
                if(isFavourite) {
                    makeListFavorite(userId, venueId, false,holder,wishListModelsList,position,"list");
                }
                else{
                   // makeListFavorite(userId, venueId, true, holder,wishListModelsList,position);
                }
            }
        });
    }

    /**
     * Set Event data
     */
    public void setEventData(final MyViewHolder holder, final int position,
                             final WishListModel wishListModel, final ArrayList<WishListModel> wishListModelsList) {
        //final wishListModel wishListModel = wishListModelList.get(position);
        String price = wishListModel.getPrice();
        String minimum_price = wishListModel.getMinimum_Price();
        String eventName = wishListModel.getName();
        String ratings = wishListModel.getRatings();
        String reviews = wishListModel.getReview_count();
        String skillLevel = wishListModel.getSkill_level();
        String date = wishListModel.getEventDate();
        String eventStartTime = wishListModel.getEventStartTime();
        String eventEndTime = wishListModel.getEventEndTime();
        String creatorUserName = wishListModel.getCreator_name();
        String totalNumberOfPlayers =  wishListModel.getCapacity();
        String totalNumberOfParticipants =  wishListModel.getTotal_Participants();
        String creator_profile_image =  wishListModel.getCreator_Image();
        String sportName = wishListModel.getSports_Type();
        boolean isFavorite = wishListModel.isFavorite();
        String object_Type = wishListModel.getObject_type();
        String total_price = wishListModel.getEvent_Total_Price();
        String list_Id = wishListModel.getList_id();

        ArrayList<String> imageArray = wishListModel.getImages_array();

        if(imageArray.size() > 0){
            image = imageArray.get(0);
        }

        if(!TextUtils.isEmpty(skillLevel)){
            holder.tv_eventSkillLevel.setText(skillLevel);
        }

        if(!TextUtils.isEmpty(date)){
            holder.tv_eventDatetime.setText(ApplicationUtility.formattedDate(date) +" - "+eventStartTime +" to " + eventEndTime);
        }

        /*if(!TextUtils.isEmpty(total_price) && !total_price.equalsIgnoreCase("0.0")) {
            if(!total_price.equalsIgnoreCase("0") ){
                holder.event_price.setText("$ "+total_price+" / Player");
                holder.tv_minimum_price.setText("As low as $" + minimum_price);
            }else{
                holder.event_price.setText("FREE");
                holder.tv_minimum_price.setText("FREE");
            }
        }
        else{
            holder.event_price.setTextColor(Color.parseColor("#000000"));
            holder.event_price.setText("FREE");
        }*/

        /**Displaying Price & As low as cost according to the Object type*/
        if(TextUtils.isEmpty(list_Id) && (!object_Type.equalsIgnoreCase("Event") || !object_Type.equalsIgnoreCase("event")))
        {
            holder.tv_minimum_price.setVisibility(View.INVISIBLE);
            holder.event_price.setVisibility(View.GONE);
            holder.event_rating_venue.setVisibility(View.INVISIBLE);
            holder.rating_event_reviews.setVisibility(View.INVISIBLE);
        }
        else if( (object_Type.equalsIgnoreCase("Game") || object_Type.equalsIgnoreCase("game"))) {
            if(!TextUtils.isEmpty(total_price)) {
                holder.tv_minimum_price.setVisibility(View.VISIBLE);
                if(!total_price.equalsIgnoreCase("0")){
                    holder.event_price.setText("$ "+total_price+" / Player");
                    holder.tv_minimum_price.setText("As low as $" + ApplicationUtility.twoDigitAfterDecimal(Double.valueOf(minimum_price)));
                }else{
                    holder.event_price.setText("FREE");
                    // holder.tv_minimum_price.setText("FREE");
                    holder.tv_minimum_price.setVisibility(View.GONE);
                }
            }
            else
            {
                holder.event_price.setTextColor(Color.parseColor("#000000"));
                holder.event_price.setText("FREE");
            }
        }
        else if(object_Type.equalsIgnoreCase("Event") || object_Type.equalsIgnoreCase("event"))
        {
            holder.tv_minimum_price.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(total_price)) {
                if (!total_price.equalsIgnoreCase("0")) {
                    holder.event_price.setText("$ " + total_price + " / Player");
                } else {
                    holder.event_price.setText("FREE");
                }
            } else {
                holder.event_price.setTextColor(Color.parseColor("#000000"));
                holder.event_price.setText("FREE");
            }
        }

        /*****************************************************************/

        holder.event_rating_venue.setRating(Float.valueOf(ratings));
        holder.rating_event_reviews.setText(reviews+" Reviews");
        holder.tv_totalnoparticiapnts.setText(totalNumberOfParticipants/* + " of " + totalNumberOfPlayers*/);

        if(!TextUtils.isEmpty(totalNumberOfPlayers)) {
            int leftSpots = Integer.valueOf(totalNumberOfPlayers) - Integer.valueOf(totalNumberOfParticipants);
            holder.tv_eventspots.setText(String.valueOf(leftSpots) + " spots remaining");
        }
        else{
            holder.tv_eventspots.setText(" 0 spot remaining");
        }

        //Displaying venue courts image
        if(!TextUtils.isEmpty(image)){
            Glide.with(context).load(image).into(holder.event_listing_image);
        }else{
            int getVenueImage = SportsImagePicker.getDefaultSportImage(sportName);
            holder.event_listing_image.setBackgroundResource(getVenueImage);
        }

        //Displaying the Event Name
        if(!TextUtils.isEmpty(eventName)){
            holder.tv_event_name.setText(eventName);
        }

        //Displaying the creator name
        if(!TextUtils.isEmpty(creatorUserName)){
            holder.tv_event_creator_name.setText(creatorUserName);
        }else{
            holder.tv_event_creator_name.setText("Name Display");
        }

        //Displaying the Event creator Image
        if(!TextUtils.isEmpty(creator_profile_image) &&
                creator_profile_image.equalsIgnoreCase("null") &&
                creator_profile_image != null){
            Glide.with(context).load(creator_profile_image).into(holder.imagv_event_creator_image);
        }


        if(isFavorite){
            holder.event_favorite_button.setFavorite(true);
        }else{
            holder.event_favorite_button.setFavorite(false);
        }

        //Calculating distance from current to destination
        if(!TextUtils.isEmpty(wishListModel.getLatitude())) {
            double dest_loc_latitude = Double.valueOf(wishListModel.getLatitude());
            double dest_loc_longitude = Double.valueOf(wishListModel.getLongitude());

            double distance = ApplicationUtility.distance(currentLatitude, currentLongitude,
                    dest_loc_latitude, dest_loc_longitude);

            holder.tv_event_miles.setText(String.valueOf(distance) + " miles");
        }

        if(!TextUtils.isEmpty(object_Type)
                && ( object_Type.equalsIgnoreCase("event")
                || object_Type.equalsIgnoreCase("Event")))
        {
            holder.tv_minimum_price.setVisibility(View.GONE);
        }
        else{
            holder.tv_minimum_price.setVisibility(View.VISIBLE);
        }

        holder.event_favorite_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String eventId = wishListModelsList.get(position).getId();
                boolean favorite = wishListModelsList.get(position).isFavorite();
                AppConstants.OBJECT_TYPE_GAME = wishListModelsList.get(position).getObject_type();

                if (favorite) {
                    makeListFavorite(userId, eventId, false,holder,wishListModelsList,position,"game");
                } else {
                    //makeListFavorite(userId, venueId, true,holder,wishListModelsList,position);
                }
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.VENUE_IMAGE = wishListModel.getImage();

                venueId = wishListModel.getId();
                EventAdapter.event_Id = venueId;
                getVenuesEventDetailsInfor(wishListModel.getId(), userId);
            }
        });

        image = "";
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        //Court References

        public TextView venue_name, text_venue_miles, rating_venue_reviews, venue_price;
        public RatingBar rating_venue;
        public ImageView venue_image, icon_venue_favourite;
        public CardView card_view_venue_list;
        public MaterialFavoriteButton favorite_button;

        //Event References
        public RatingBar event_rating_venue;
        public MaterialFavoriteButton event_favorite_button;
        public CardView cardView;
        public TextView event_price,rating_event_reviews,
                tv_event_name, tv_eventSkillLevel, tv_eventDatetime,tv_eventspots,
                tv_totalnoparticiapnts, tv_event_creator_name, tv_event_miles, tv_minimum_price;

        public ImageView event_listing_image,imagv_event_participants;
        public CircleImageView imagv_event_creator_image ;

        public MyViewHolder(View view) {
            super(view);

            //Court References
            text_venue_miles = (TextView) view.findViewById(R.id.text_venue_miles);
            rating_venue_reviews = (TextView) view.findViewById(R.id.rating_venue_reviews);
            venue_price = (TextView) view.findViewById(R.id.venue_price);
            venue_name = (TextView) view.findViewById(R.id.venue_name);
            rating_venue = (RatingBar) view.findViewById(R.id.rating_venue);
            venue_image = (ImageView) view.findViewById(R.id.venue_image);
            icon_venue_favourite = (ImageView) view.findViewById(R.id.icon_venue_favourite);
            card_view_venue_list = (CardView) view.findViewById(R.id.card_view_venue_list);
            favorite_button = (MaterialFavoriteButton) view.findViewById(R.id.favorite_button);

            //Event References

            event_rating_venue = (RatingBar) view.findViewById(R.id.rating_event_detail);
            event_favorite_button = (MaterialFavoriteButton) view.findViewById(R.id.event_favorite_button_wish);
            cardView = (CardView) view.findViewById(R.id.card_view_event_list);
            event_price = (TextView) view.findViewById(R.id.event_price);
            rating_event_reviews = (TextView) view.findViewById(R.id.rating_event_reviews);
            event_listing_image = (ImageView) view.findViewById(R.id.event_listing_image);
            imagv_event_creator_image = (CircleImageView) view.findViewById(R.id.event_creator_image);
            imagv_event_participants = (ImageView)view.findViewById(R.id.event_participants_image);
            tv_event_name = (TextView) view.findViewById(R.id.venue_name_event);
            tv_eventSkillLevel = (TextView) view.findViewById(R.id.event_sport_name);
            tv_eventDatetime = (TextView) view.findViewById(R.id.event_date_time);
            tv_eventspots = (TextView) view.findViewById(R.id.event_spots);
            tv_totalnoparticiapnts = (TextView) view.findViewById(R.id.event_participants);
            tv_event_creator_name = (TextView) view.findViewById(R.id.event_creator_name);
            tv_event_miles = (TextView) view.findViewById(R.id.event_miles);
            tv_minimum_price = (TextView) view.findViewById(R.id.event_Minimum_Amount_Text);
        }
    }

    /*********************
     * CLASS FOR CUSTOM FILTER
     *********************************************/
    public class CustomFilter extends Filter {

        private WishListAdapter mAdapter;

        private CustomFilter(WishListAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(previouslist);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final WishListModel list : previouslist) {
                    // if (list.getTaskName().toLowerCase().startsWith(filterPattern)) {
                    if (list.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(list);
                    }
                }
            }
            wishListModelsList = (ArrayList<WishListModel>) filteredList;
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            notifyDataSetChanged();

            if (((List<VenueModel>) results.values).size() == 0) {

                snackBar.setSnackBarMessage("No courts found");
                //Toast.makeText(context, "No events found", Toast.LENGTH_LONG).show();
                notifyDataSetChanged();
            } else {
//                text_contacts.setVisibility(View.GONE);
//                stickyList.setVisibility(View.VISIBLE);
            }
        }

    }

    /**
     * Function to get Venue Courts detail from server
     */
    public void getVenuesCourtsDetailsInfor(String court_Id) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(court_Id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(context, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "",venue_id ="",capacity = "", name = "",
                        latitude = "", longitude = "",review_rating = "",
                        review_count = "",court_type = "",description = "",
                        price = "",address = "",city = "",state = "",country = "",
                        eventSportName = "", objectType = "",cancel_policy_type = "",
                        cancellation_point = "", minimum_age = "",
                        space_type = "", space_desc = "",
                        phone = "", website = "", rules = "";
                boolean favourite_status = false,isEquipmenmtAvailable = false;;

                ArrayList<String> images_arraylist = new ArrayList<String>();
                ArrayList<String> specs_arraylist = new ArrayList<String>();
                ArrayList<String> space_desc_array = new ArrayList<String>();
                ArrayList<String> equipments_array = new ArrayList<String>();
                ArrayList<String> amenities_array = new ArrayList<String>();

                try
                {
                    //JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    venueDetailModelArrayList = new ArrayList<VenueDetailModel>();
                    venueDetailModel = new VenueDetailModel();

                    if(jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if(jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if(jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                    }
                    if(jsonObject.has("capacity") && !jsonObject.isNull("capacity")) {
                        capacity = jsonObject.getString("capacity");
                    }
                    if(jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                    }
                    if(jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                    }
                    if(jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }
                    if(jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }
                    if(jsonObject.has("price") && !jsonObject.isNull("price"))
                    {
                        price = jsonObject.getString("price");
                    }
                    if(jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if(jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }
                    if(jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if(jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if(jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }
                    if(jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")) {
                        favourite_status = jsonObject.getBoolean("is_favorite");
                    }

                    if(jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")){
                        eventSportName = jsonObject.getString("sport_name");
                    }

                    if(jsonObject.has("object_type") && !jsonObject.isNull("object_type")){
                        objectType = jsonObject.getString("object_type");
                    }
                    if(jsonObject.has("cancel_policy_type")&& !jsonObject.isNull("cancel_policy_type"))
                    {
                        cancel_policy_type = jsonObject.getString("cancel_policy_type");
                    }
                    if(jsonObject.has("cancellation_point")&& !jsonObject.isNull("cancellation_point"))
                    {
                        cancellation_point = jsonObject.getString("cancellation_point");
                    }

                    if(jsonObject.has("user_review")&& !jsonObject.isNull("user_review")) {
                        JSONObject user_review_object = jsonObject.getJSONObject("user_review");
                        VenueAdapter.user_review_arrayList = new ArrayList<String>();
                        if(user_review_object.length() > 0) {
                            String user_rating = user_review_object.getString("rating");
                            String user_review = user_review_object.getString("review");
                            VenueAdapter.user_review_arrayList.add(user_rating + "@@@@" + user_review);
                        }
                    }


                    if(jsonObject.has("equipments") && !jsonObject.isNull("equipments")){
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");

                        if(equipmentArray.length() > 0){
                            isEquipmenmtAvailable = true;
                        }
                    }

                    if(jsonObject.has("images")&& !jsonObject.isNull("images")) {
                        JSONArray images_json_array = jsonObject.getJSONArray("images");

                        for (int i = 0; i < images_json_array.length(); i++) {

                            images_arraylist.add(images_json_array.getString(i));
                        }
                        venueDetailModel.setImages_array(images_arraylist);
                    }


                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        JSONObject add_info_json_object = jsonObject.getJSONObject("additional_info");

                        JSONArray jsonArray = add_info_json_object.getJSONArray("space_desc");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String value = jsonArray.get(i).toString();
                            space_desc_array.add(value);
                        }

                        if(add_info_json_object.has("equipments")&& !add_info_json_object.isNull("equipments")) {

                            JSONArray equipments_json_array = add_info_json_object.getJSONArray("equipments");
                            for (int i = 0; i < equipments_json_array.length(); i++) {
                                String value = equipments_json_array.get(i).toString();
                                equipments_array.add(value);
                            }

                        }
                        if(add_info_json_object.has("amenities")&& !add_info_json_object.isNull("amenities")) {

                            JSONArray amenities_json_array = add_info_json_object.getJSONArray("amenities");
                            for (int i = 0; i < amenities_json_array.length(); i++) {
                                String value = amenities_json_array.get(i).toString();
                                amenities_array.add(value);
                            }
                        }
                        if(add_info_json_object.has("phone")&& !add_info_json_object.isNull("phone")) {
                            phone = add_info_json_object.getString("phone");
                        }
                        if(add_info_json_object.has("website")&& !add_info_json_object.isNull("website")) {
                            website = add_info_json_object.getString("website");
                        }
                        if(add_info_json_object.has("rules")&& !add_info_json_object.isNull("rules")) {
                            rules = add_info_json_object.getString("rules");
                        }

                        venueDetailModel.setAdditional_space_desc_array(space_desc_array);
                        venueDetailModel.setAdditional_equipmentes_array(equipments_array);
                        venueDetailModel.setAdditional_amenities_array(amenities_array);
                        venueDetailModel.setAdditional_phone_number(phone);
                        venueDetailModel.setAdditional_website(website);
                        venueDetailModel.setAdditional_venue_rules(rules);
                    }

                    if(jsonObject.has("minimum_age")&& !jsonObject.isNull("minimum_age")) {
                        minimum_age = jsonObject.getString("minimum_age");
                        if(minimum_age != null && !minimum_age.equalsIgnoreCase("No minimum age required")){
                            minimum_age = minimum_age + " + ";
                        }
                    }

                    if(jsonObject.has("space_type")&& !jsonObject.isNull("space_type")) {
                        space_type = jsonObject.getString("space_type");
                    }

                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        String space = "";
                        JSONObject jsonObject_additional_info = jsonObject.getJSONObject("additional_info");

                        if(jsonObject_additional_info.has("space_desc") && !jsonObject_additional_info.isNull("space_desc")){
                            JSONArray spaceArray = jsonObject_additional_info.getJSONArray("space_desc");
                            for (int i = 0; i < spaceArray.length(); i++) {

                                // if(!TextUtils.isEmpty(space)){
                                space = space + "," + spaceArray.get(i);
                                // }
                            }

                            if(TextUtils.isEmpty(space)){
                                space_desc = "No Space Available";
                            }else {
                                if (space != null
                                        && space
                                        .length() > 1) {
                                    space_desc = space.substring(1,space.length());
                                }
                                //space_desc = space;
                            }
                        }
                    }


                    venueDetailModel.setId(id);
                    venueDetailModel.setVenue_id(venue_id);
                    venueDetailModel.setName(name);
                    venueDetailModel.setCapacity(capacity);
                    venueDetailModel.setLatitude(latitude);
                    venueDetailModel.setLongitude(longitude);
                    venueDetailModel.setReview_rating(review_rating);
                    venueDetailModel.setReview_count(review_count);
                    venueDetailModel.setPrice(price);
                    venueDetailModel.setCourt_type(court_type);
                    venueDetailModel.setDescription(description);
                    venueDetailModel.setAddress(address);
                    venueDetailModel.setCity(city);
                    venueDetailModel.setState(state);
                    venueDetailModel.setCountry(country);
                    venueDetailModel.setFavourite(favourite_status);
                    venueDetailModel.setEvent_Sports_Type(eventSportName);
                    venueDetailModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    venueDetailModel.setObjectType(objectType);
                    venueDetailModel.setCancel_policy_type(cancel_policy_type);
                    venueDetailModel.setCancellation_point(cancellation_point);
                    venueDetailModel.setSpace_type(space_type);
                    venueDetailModel.setCourt_min_players(minimum_age);
                    venueDetailModel.setCourt_space_desc(space_desc);
                    venueDetailModelArrayList.add(venueDetailModel);
                }catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(context, false);

                if(isSuccessStatus) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            ((Activity)context).finish();
                            VenueAdapter.venueDetailModelArrayList = venueDetailModelArrayList;
                            Constants.FROM_WISHLIST = true;
                            Intent intent = new Intent(context, VenueDetails.class);
                            context.startActivity(intent);
                        }
                    });
                }
                else {
                    snackBar.setSnackBarMessage("Network error! Please try after sometime");
                }
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String court_Id) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_court_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.VENUE_COURT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", userId);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(context, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }


    /**
     * Function to get Venue courts from server
     */
    public void makeListFavorite(String user_id, String list_id, boolean isFavorite, final MyViewHolder holder, final ArrayList<WishListModel> modelArray, final int position, String object_type) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, list_id, isFavorite,object_type);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(context, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    isAddedToFavorite = responseObject.getBoolean("isFavorite");

                }catch(Exception ex){
                    ex.printStackTrace();
                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(context, false);
                        Constants.FAVOURITE_CLICKED = true;
                        if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            if(isAddedToFavorite){
                                snackBar.setSnackBarMessage("Successfully Added to WishList");
                                holder.favorite_button.setFavorite(true);
                                holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_orange);
                                modelArray.get(position).setFavorite(isAddedToFavorite);
                                venueAdapter.notifyDataSetChanged();
                            }else{
                                snackBar.setSnackBarMessage("Successfully Removed from WishList");
                                holder.favorite_button.setFavorite(false);
                                holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_gray);
                                //modelArray.get(position).setFavorite(isAddedToFavorite);
                                modelArray.remove(position);
                                venueAdapter.notifyDataSetChanged();
                            }

                        }else if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                            snackBar.setSnackBarMessage("Failed to Add to WishList");
                            notifyDataSetChanged();
                        }else{
                            snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE_ERR);
                            notifyDataSetChanged();
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String userId, String objectId, boolean favoriteStatus, String object_type) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "add_to_favorite").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("object_id", objectId);

        if(!TextUtils.isEmpty(AppConstants.OBJECT_TYPE_GAME)
                && ( AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")))
        {
            urlBuilder.addQueryParameter("object_type", "event");
        }
        else{
            if(object_type.equalsIgnoreCase(AppConstants.OBJECT_TYPE_LIST)) {
                urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_LIST);
            }
            else{
                urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_EVENT);
            }
        }


        if(favoriteStatus){
            urlBuilder.addQueryParameter("is_favorite", Boolean.TRUE.toString());
        }else{
            urlBuilder.addQueryParameter("is_favorite", Boolean.FALSE.toString());
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }


    /**
     * Function to get Venue Courts detail from server
     */
    public void getVenuesEventDetailsInfor(String court_Id, String userId) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(court_Id, userId);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(context, false);
                Toast.makeText(context,e.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "",venue_id ="",capacity = "", name = "",
                        latitude = "", longitude = "",review_rating = "",
                        review_count = "",court_type = "",description = "",
                        price = "",address = "",city = "",state = "",
                        country = "", total_number_players = "",
                        game_date = "", startTIme = "", endTime = "",
                        skillLevel = "", no_of_participants = "",
                        eventSportName = "", eventCourtName = "",
                        event_creator_name = "", event_creator_image = "",
                        event_creator_user_id = "", minimum_price = "",
                        actual_price = "",list_id = "",visibility = "",note = "",
                        cancel_policy_type = "",cancellation_point = "", object_type = "",
                        minimum_age = "", space_type = "", space_desc = "",
                        phone = "", website = "", rules = "";
                boolean isFavorite = false, isEquipmenmtAvailable = false;
                ArrayList<String> space_desc_array = new ArrayList<String>();
                ArrayList<String> equipments_array = new ArrayList<String>();
                ArrayList<String> amenities_array = new ArrayList<String>();

                try
                {
                    //JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    eventListingModelArrayList = new ArrayList<EventListingModel>();
                    eventListingModel = new EventListingModel();
                    ArrayList<String> images_arraylist = new ArrayList<String>();

                    if(jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if(jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if(jsonObject.has("list_id") && !jsonObject.isNull("list_id")) {
                        list_id = jsonObject.getString("list_id");
                    }
                    if(jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                        Constants.EVENT_NAME_MAP = name;
                    }
                    if(jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                        Constants.EVENT_LATITUDE = Double.parseDouble(latitude);
                    }
                    if(jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                        Constants.EVENT_LONGITUDE = Double.parseDouble(longitude);
                    }
                    if(jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }

                    if(jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }

                    if(jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if(jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }
                    if(jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if(jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if(jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }

                    if(jsonObject.has("no_of_player") && !jsonObject.isNull("no_of_player")){
                        total_number_players = jsonObject.getString("no_of_player");
                    }

                    if(jsonObject.has("no_of_partitipants") && !jsonObject.isNull("no_of_partitipants")){
                        no_of_participants = jsonObject.getString("no_of_partitipants");
                    }

                    if(jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")){
                        isFavorite = jsonObject.getBoolean("is_favorite");
                    }

                    if(jsonObject.has("price") && !jsonObject.isNull("price")) {
                        try{
                            price = jsonObject.getString("price");
                            int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                            float price_pre_player = Float.parseFloat(price) / (float)totalPlayer;
                            actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                            float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                            minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                        }catch(Exception ex){
                            ex.printStackTrace();;
                        }
                    }

                    if(jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")){
                        eventSportName = jsonObject.getString("sport_name");
                    }

                    if(jsonObject.has("game_date") && !jsonObject.isNull("game_date")){
                        game_date = jsonObject.getString("game_date");
                    }

                    if(jsonObject.has("skill_level") && !jsonObject.isNull("skill_level")){
                        skillLevel = jsonObject.getString("skill_level");
                    }

                    if(jsonObject.has("list_name") && !jsonObject.isNull("list_name")){
                        eventCourtName = jsonObject.getString("list_name");
                    }

                    if(jsonObject.has("user_name") && !jsonObject.isNull("user_name")){
                        event_creator_name = jsonObject.getString("user_name");
                    }

                    if(jsonObject.has("user_id") && !jsonObject.isNull("user_id")){
                        event_creator_user_id = jsonObject.getString("user_id");
                    }

                    if(jsonObject.has("user_profile_image") && !jsonObject.isNull("user_profile_image")){
                        event_creator_image = jsonObject.getString("user_profile_image");
                    }
                    if(jsonObject.has("visibility") && !jsonObject.isNull("visibility")){
                        visibility = jsonObject.getString("visibility");
                    }
                    if(jsonObject.has("note") && !jsonObject.isNull("note")) {
                        note = jsonObject.getString("note");
                    }
                    if(jsonObject.has("cancel_policy_type")&& !jsonObject.isNull("cancel_policy_type"))
                    {
                        cancel_policy_type = jsonObject.getString("cancel_policy_type");
                    }
                    if(jsonObject.has("cancellation_point")&& !jsonObject.isNull("cancellation_point"))
                    {
                        cancellation_point = jsonObject.getString("cancellation_point");
                    }
                    if(jsonObject.has("court_capacity") && !jsonObject.isNull("court_capacity")) {
                        capacity = jsonObject.getString("court_capacity");
                    }

                    if(jsonObject.has("equipments") && !jsonObject.isNull("equipments")){
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");

                        if(equipmentArray.length() > 0){
                            isEquipmenmtAvailable = true;
                        }
                    }

                    if(jsonObject.has("time_slots")&& !jsonObject.isNull("time_slots")) {
                        String start_time = null;
                        JSONArray time_slot_array_json_array = jsonObject.getJSONArray("time_slots");
                        for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                            JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                            if(j == 0){
                                startTIme = getValues.getString("start_time");
                            }
                            endTime = getValues.getString("end_time");
                        }
                    }
                    else if(jsonObject.has("time_slot")&& !jsonObject.isNull("time_slot")) {
                        String start_time = null;
                        JSONArray time_slot_array_json_array = jsonObject.getJSONArray("time_slot");
                        for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                            JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                            if(j == 0){
                                startTIme = getValues.getString("start_time");
                            }
                            endTime = getValues.getString("end_time");
                        }
                    }


                    if(jsonObject.has("images")&& !jsonObject.isNull("images")) {
                        ArrayList<String> images_array = new ArrayList<String>();
                        JSONArray images_json_array = jsonObject.getJSONArray("images");
                        for (int j = 0; j < images_json_array.length(); j++) {
                            images_array.add(images_json_array.getString(j));
                        }
                        eventListingModel.setImages_array(images_array);
                    }
                    if(jsonObject.has("object_type")&& !jsonObject.isNull("object_type")) {
                        object_type = jsonObject.getString("object_type");
                    }


                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        JSONObject add_info_json_object = jsonObject.getJSONObject("additional_info");

                        JSONArray jsonArray = add_info_json_object.getJSONArray("space_desc");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String value = jsonArray.get(i).toString();
                            space_desc_array.add(value);
                        }

                        if(add_info_json_object.has("equipments")&& !add_info_json_object.isNull("equipments")) {

                            JSONArray equipments_json_array = add_info_json_object.getJSONArray("equipments");
                            for (int i = 0; i < equipments_json_array.length(); i++) {
                                String value = equipments_json_array.get(i).toString();
                                equipments_array.add(value);
                            }

                        }
                        if(add_info_json_object.has("amenities")&& !add_info_json_object.isNull("amenities")) {

                            JSONArray amenities_json_array = add_info_json_object.getJSONArray("amenities");
                            for (int i = 0; i < amenities_json_array.length(); i++) {
                                String value = amenities_json_array.get(i).toString();
                                amenities_array.add(value);
                            }
                        }
                        if(add_info_json_object.has("phone")&& !add_info_json_object.isNull("phone")) {
                            phone = add_info_json_object.getString("phone");
                        }
                        if(add_info_json_object.has("website")&& !add_info_json_object.isNull("website")) {
                            website = add_info_json_object.getString("website");
                        }
                        if(add_info_json_object.has("rules")&& !add_info_json_object.isNull("rules")) {
                            rules = add_info_json_object.getString("rules");
                        }

                        eventListingModel.setAdditional_space_desc_array(space_desc_array);
                        eventListingModel.setAdditional_equipmentes_array(equipments_array);
                        eventListingModel.setAdditional_amenities_array(amenities_array);
                        eventListingModel.setAdditional_phone_number(phone);
                        eventListingModel.setAdditional_website(website);
                        eventListingModel.setAdditional_venue_rules(rules);
                    }


                    if(jsonObject.has("minimum_age")&& !jsonObject.isNull("minimum_age")) {
                        minimum_age = jsonObject.getString("minimum_age");
                        if(minimum_age != null && !minimum_age.equalsIgnoreCase("No minimum age required")){
                            minimum_age = minimum_age + " + ";
                        }
                    }

                    if(jsonObject.has("space_type")&& !jsonObject.isNull("space_type")) {
                        space_type = jsonObject.getString("space_type");
                    }

                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        String space = "";
                        JSONObject jsonObject_additional_info = jsonObject.getJSONObject("additional_info");

                        if(jsonObject_additional_info.has("space_desc") && !jsonObject_additional_info.isNull("space_desc")){
                            JSONArray spaceArray = jsonObject_additional_info.getJSONArray("space_desc");
                            for (int i = 0; i < spaceArray.length(); i++) {

                                // if(!TextUtils.isEmpty(space)){
                                space = space + "," + spaceArray.get(i);
                                // }
                            }

                            if(TextUtils.isEmpty(space)){
                                space_desc = "No Space Available";
                            }else {
                                if (space != null
                                        && space
                                        .length() > 1) {
                                    space_desc = space.substring(1,space.length());
                                }
                                //space_desc = space;
                            }
                        }
                    }

                    if(jsonObject.has("game_partitipants")&& !jsonObject.isNull("game_partitipants")){

                        try{
                            JSONArray particiapntsArray = jsonObject.getJSONArray("game_partitipants");
                            AppConstants.userModelArary = new ArrayList<UserMolel>();
                            AppConstants.userIdArray = new ArrayList<String>();
                            if(particiapntsArray.length() > 0){
                                for(int i = 0; i < particiapntsArray.length(); i++){
                                    UserMolel model = new UserMolel();
                                    JSONObject participantsObj = particiapntsArray.getJSONObject(i);
                                    String userName = participantsObj.getString("user_name");
                                    String userId = participantsObj.getString("user_id");
                                    String user_profile_image = participantsObj.getString("user_profile_image");
                                    model.setUserFullName(userName);
                                    model.setUserId(userId);
                                    model.setUserProfileImage(user_profile_image);
                                    AppConstants.userIdArray.add(userId);
                                    AppConstants.userModelArary.add(model);
                                }
                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }

                    eventListingModel.setEvent_Id(id);
                    eventListingModel.setList_id(list_id);
                    eventListingModel.setEvent_Name(name);
                    eventListingModel.setLatitude(latitude);
                    eventListingModel.setLongitude(longitude);
                    eventListingModel.setEvent_Ratings(review_rating);
                    eventListingModel.setEvent_Reviews(review_count);
                    eventListingModel.setEvent_Price(actual_price);
                    eventListingModel.setCourt_type(court_type);
                    eventListingModel.setDescription(description);
                    eventListingModel.setAddress(address);
                    eventListingModel.setVisibility(visibility);
                    eventListingModel.setNote(note);
                    eventListingModel.setCancel_policy_type(cancel_policy_type);
                    eventListingModel.setCancellation_point(cancellation_point);
                    eventListingModel.setCity(city);
                    eventListingModel.setState(state);
                    eventListingModel.setCountry(country);
                    eventListingModel.setCapacity(capacity);
                    eventListingModel.setEvent_Total_Participants(total_number_players);
                    eventListingModel.setEventDate(game_date);
                    eventListingModel.setEventStartTime(startTIme);
                    eventListingModel.setEventEndTime(endTime);
                    eventListingModel.setSkill_level(skillLevel);
                    eventListingModel.setEvent_accepted_Participants(no_of_participants);
                    eventListingModel.setEvent_Sports_Type(eventSportName);
                    eventListingModel.setEvent_court_name(eventCourtName);
                    eventListingModel.setEvent_Creator_Image(event_creator_image);
                    eventListingModel.setCreator_name(event_creator_name);
                    eventListingModel.setCreator_user_id(event_creator_user_id);
                    eventListingModel.setEvent_Minimum_Price(minimum_price);
                    eventListingModel.setFavorite(isFavorite);
                    eventListingModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    eventListingModel.setObject_Type(object_type);
                    eventListingModel.setEvent_Total_Price(price);
                    eventListingModel.setCourt_min_players(minimum_age);
                    eventListingModel.setCourt_space_desc(space_desc);
                    eventListingModel.setSpace_type(space_type);
                    eventListingModelArrayList.add(eventListingModel);

                }catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(context, false);

                if(isSuccessStatus) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            context.finish();
                            EventAdapter.eventListingModelArrayList = eventListingModelArrayList;
                            Constants.FROM_WISHLIST = true;
                            Intent intent = new Intent(context, EventDetail.class);
                            context.startActivity(intent);
                        }
                    });
                }
                else
                {
                    snackBar.setSnackBarMessage("Network error! Please try after sometime");
                }


            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String court_Id, String userId) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_game_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.EVENT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", userId);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    public void getCurrentLocation(){
        GPSTracker gps = new GPSTracker(context);

        if(gps.canGetLocation()){
            currentLatitude = gps.getLatitude();
            currentLongitude = gps.getLongitude();
        }
    }



}
