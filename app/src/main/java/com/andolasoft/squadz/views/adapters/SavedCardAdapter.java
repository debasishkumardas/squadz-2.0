package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.SplashScreen;
import com.andolasoft.squadz.activities.StoredPaymentCardsScreen;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.SavedPaymentCardsModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Debasish Kumar Das on 2/6/2017.
 */
public class SavedCardAdapter extends RecyclerView.Adapter<SavedCardAdapter.MyViewHolder> {

    private ArrayList<SavedPaymentCardsModel> cardListingArray;
    private Activity context;
    ProgressDialog dialog;
    String status;
    DatabaseAdapter db;
    int pos;
    MaterialDialog mMaterialDialog;
    SharedPreferences pref;
    String logged_in_userid;


    public SavedCardAdapter(Activity context, ArrayList<SavedPaymentCardsModel>
            EventListingModelList) {
        this.cardListingArray = EventListingModelList;
        this.context = context;
        db = new DatabaseAdapter(context);
        pref = PreferenceManager.getDefaultSharedPreferences(this.context);
        getSenderData();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_creditcard_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return cardListingArray.size();
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final SavedPaymentCardsModel cardList = cardListingArray.get(position);
        String card_type = cardList.getCardBrand();
        if (card_type.equalsIgnoreCase("American Express")) {
            holder.ivCard.setImageResource(R.mipmap.ic_american_express);
        } else if (card_type.equalsIgnoreCase("Visa")) {
            holder.ivCard.setImageResource(R.mipmap.ic_visa);
        } else if (card_type.equalsIgnoreCase("Mastercard")) {
            holder.ivCard.setImageResource(R.mipmap.ic_mastercard);
        } else if (card_type.equalsIgnoreCase("Discover")) {
            holder.ivCard.setImageResource(R.mipmap.ic_discover_card);
        } else if (card_type.equalsIgnoreCase("JCB")) {
            holder.ivCard.setImageResource(R.mipmap.ic_jcb_card);
        } else {
            holder.ivCard.setImageResource(R.mipmap.ic_diners_club);
        }

        holder.CardNumber.setText("**** **** **** " + cardList.getLastfour());

        if (cardList.getPrimary_card_status() == 1) {
            holder.iv_primary_card.setVisibility(View.VISIBLE);
        }

        holder.v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                pos = position;
                String cardId = cardList.getCardId();
                String custId = cardList.getCustId();
                String card_acc_number = cardList.getLastfour();
                displayDialog("**** **** **** " + card_acc_number,cardId,custId);
                return true;
            }
        });

        /*holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
    }

    /**
     * Getting user ids
     */
    public void getSenderData(){
        logged_in_userid = pref.getString("loginuser_id", null);
    }

    public void updateUserPrimaryCard(final String card_id, String cust_id) {
        //Displaying loader
        LoaderUtility.handleLoader(context, true);
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBody(card_id,cust_id);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                SplashScreen.displayMessageInUiThread(context, e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    String message = responseObject.getString("message");
                    if (message != null &&
                            message.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                        //Getting the default card fron Db
                        String default_card = db.getPrimaryPaymentCard(1);
                        //Updating the stored default card staus
                        db.updateUserPrimaryCardstatus(default_card, 0);
                        //Updating the default card
                        db.updateUserPrimaryCard(card_id,1);
                        //Displaying the messages
                        SplashScreen.displayMessageInUiThread(context, "Card Successfully Added As Primary");
                        //Refreshing the list view items
                        context.runOnUiThread(new Runnable() {
                            public void run() {
                                StoredPaymentCardsScreen.refreshView();
                            }
                        });
                    } else if (message != null &&
                            message.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)) {
                        //Displaying the messages
                        SplashScreen.displayMessageInUiThread(context, message);
                    } else {
                        SplashScreen.displayMessageInUiThread(context, message);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    SplashScreen.displayMessageInUiThread(context, ex.toString());
                }
                LoaderUtility.handleLoader(context, false); 
            }
        });
    }

    /**
     * Dispaly ing the message
     *
     * @param message status message
     */
    public void displayMessage(final String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(context, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String card_id, String cust_id){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE+"make_card_primary?").newBuilder();
        urlBuilder.addQueryParameter("card_id", card_id);
        urlBuilder.addQueryParameter("cust_id", cust_id);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public View v;
        ImageView ivCard, iv_primary_card;
        TextView CardNumber;

        public MyViewHolder(View view) {
            super(view);
            this.v = view;
            ivCard = (ImageView) view.findViewById(R.id.cardImage);
            CardNumber = (TextView) view.findViewById(R.id.cardNumber);
            iv_primary_card = (ImageView) view.findViewById(R.id.iv_primary);
            iv_primary_card.setVisibility(View.GONE);
        }
    }

    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayDialog(String card_bumber, final String card_id, final String cust_id){
        mMaterialDialog = new MaterialDialog(context);
        View view = LayoutInflater.from(context)
                .inflate(R.layout.layout_update_card_status,
                        null);
        TextView tv_card_numner = (TextView) view.findViewById(R.id.tv_card_number);
        TextView tv_card_option_primary_card = (TextView) view.findViewById(R.id.tv_make_card_primary);
        TextView tv_card_option_delete_card = (TextView) view.findViewById(R.id.tv_delete_card);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_card_numner.setText(card_bumber);
        tv_card_option_primary_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                updateUserPrimaryCard(card_id,cust_id);
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_card_option_delete_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                int getPrimaryCardStatus = db.isPrimaryCard(card_id);
                int getNumberOfCards = db.getNumberOfcards();

                if(getNumberOfCards > 1 &&
                        getPrimaryCardStatus == 1){
                    //Imposing restriction for deleting the user primary card
                    displayDialog_MakeCardPrimary();
                }else{
                    //Function call to remove user card from the server
                    deleteUserCard(card_id,cust_id);
                }

            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }



    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayDialog_MakeCardPrimary(){
        mMaterialDialog = new MaterialDialog(context);
        View view = LayoutInflater.from(context)
                .inflate(R.layout.layout_update_card_status,
                        null);
        TextView tv_card_numner = (TextView) view.findViewById(R.id.tv_card_number);
        tv_card_numner.setText("Make Primary Card");
        TextView tv_card_option_primary_card = (TextView) view.findViewById(R.id.tv_make_card_primary);
        tv_card_option_primary_card.setText("This is your primary card. Please select another card as your primary card before removing this card.");
        TextView tv_card_option_delete_card = (TextView) view.findViewById(R.id.tv_delete_card);
        tv_card_option_delete_card.setVisibility(View.GONE);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_cancel.setText("OK");

        tv_card_option_primary_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_card_option_delete_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();

            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }


    /**
     * Fuhction responsible for removing user saved payment cards
     * @param card_id The User Payment Card Id
     * @param cust_id The customer Id
     */
    public void deleteUserCard(final String card_id, String cust_id) {
        //Displaying loader
        LoaderUtility.handleLoader(context, true);
        // should be a singleton
        //OkHttpClient client = new OkHttpClient();
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the request
        Request request = buildApiRequestBody_DeleteCard(card_id,cust_id);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                SplashScreen.displayMessageInUiThread(context, e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    String status = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if (status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                        //Removing user card
                        db.deleteUserCard(card_id);
                        //Displaying the messages
                        SplashScreen.displayMessageInUiThread(context, message);
                        //Refreshing the list view items
                        context.runOnUiThread(new Runnable() {
                            public void run() {
                                StoredPaymentCardsScreen.refreshView();
                            }
                        });
                    } else if (status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)) {
                        //Displaying the messages
                        SplashScreen.displayMessageInUiThread(context, message);
                    } else {
                        SplashScreen.displayMessageInUiThread(context, message);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    SplashScreen.displayMessageInUiThread(context, ex.toString());
                }
                LoaderUtility.handleLoader(context, false); 
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody_DeleteCard(String card_id, String cust_id){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE+"delete_card").newBuilder();
        urlBuilder.addQueryParameter("card_no", card_id);
        urlBuilder.addQueryParameter("user_id", logged_in_userid);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }
}


