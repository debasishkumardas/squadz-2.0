package com.andolasoft.squadz.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.Profile;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.ContactBean;
import com.andolasoft.squadz.models.ProfileOtherViewModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.UserProfileManager;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MyContactAdapter extends BaseAdapter {
	private Activity activity;
	private List<ContactBean> items;
	private int row;
	private ContactBean objBean;
	private String user_id, auth_token;
	Context context;
	String getusername;
	SharedPreferences preferences;
	Handler handler = new Handler();
	LayoutInflater inflater;
	HashMap<String, Integer> alphaIndexer;
	String[] sections;
	String status, message;
    ProgressDialog dialog;
	ArrayList<String> frd_status;
    ProfileOtherViewModel profileOtherViewModel;
    SnackBar snackBar;
	Context mContext;


	public MyContactAdapter(Activity act, List<ContactBean> items, Context context) {
		this.activity = act;
		this.items = items;
		this.mContext = context;
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		getusername = preferences.getString("USERNAME", null);
		auth_token = preferences.getString("auth_token", null);
        snackBar = new SnackBar(activity);
		inflater = LayoutInflater.from(act);
		frd_status = new ArrayList<String>();
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View view = convertView;
		try {
			final ViewHolder holder;
			convertView = null;
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.alluser_row, null);
				holder = new ViewHolder();
				holder.tvname = (TextView) view.findViewById(R.id.tvname);

				holder.tvPhoneNo = (TextView) view.findViewById(R.id.tvphone);

				holder.invite_to_squadz = (ImageView) view
						.findViewById(R.id.sendmessage);

				holder.addressbook_lay = (RelativeLayout) view
						.findViewById(R.id.addressbook_main_layout);

				holder.user_profile_pic = (ImageView) view
						.findViewById(R.id.user_pic);

				holder.add_friend = (ImageView) view
						.findViewById(R.id.add_friends);

				holder.invite_to_squadz.setVisibility(View.GONE);

				holder.add_friend.setVisibility(View.GONE);
				view.setTag(holder);
			} else {
				view = convertView;
				holder = (ViewHolder) view.getTag();
			}

			objBean = items.get(position);

			if (holder.tvname != null && null != objBean.getName()
					&& objBean.getName().trim().length() > 0) {
				holder.tvname.setText(Html.fromHtml(objBean.getName()));
			}

			if (objBean.getMatch()) {
				if (objBean.gefriendship_status()) {
					String user_image = objBean.getFacebookImageUrl();
					if (!TextUtils.isEmpty(user_image)) {
                        Glide.with(activity).load(user_image).into(holder.user_profile_pic);
					}
					holder.tvPhoneNo.setText(objBean.geusername()
							+ " is already your friend");
					holder.invite_to_squadz.setVisibility(View.GONE);
				} else {
					String user_image = objBean.getFacebookImageUrl();
                    String user_id = objBean.getUserid();

					if (!TextUtils.isEmpty(user_image)) {
                        Glide.with(activity).load(user_image).into(holder.user_profile_pic);
					}
					holder.tvPhoneNo.setText("Known as " + objBean.geusername()
							+ " on Squadz");

                    if(!frd_status.contains(user_id)){
                        holder.add_friend.setVisibility(View.VISIBLE);
                    }
				}
			} else if (holder.tvPhoneNo != null && null != objBean.getPhoneNo()
					&& objBean.getPhoneNo().trim().length() > 0) {
				holder.tvPhoneNo.setText(Html.fromHtml("Invite to Squadz"));
				holder.invite_to_squadz.setVisibility(View.VISIBLE);
				// holder.add_friend.setImageResource(R.drawable.new_messages_red);
			}

			holder.invite_to_squadz.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					int pos = position;

					boolean status = items.get(pos).gefriendship_status();

					if (status) {

						Toast.makeText(activity, "You are already friend",
								Toast.LENGTH_SHORT).show();

					} else {
						String phn_no = items.get(pos).getPhoneNo();
						Intent i = new Intent(
								Intent.ACTION_VIEW);
						i.putExtra("address", phn_no);
						/*
						 * i.putExtra("sms_body", "Add me on Squadz" + "!" + " "
						 * + "Username:" + " " + getusername
						 * +" https://www.squadz.com");
						 */
						i.putExtra(
								"sms_body",
								"Add me on Squadz"
										+ "!"
										+ " "
										+ "Username:"
										+ " "
										+ getusername
										+ " iOS: http://apple.co/1PnoqcZ and Android: https://goo.gl/q5Vntz For more information, visit www.squadz.com");
						i.setType("vnd.android-dir/mms-sms");
						// i.setType("vnd.android-dir/mms-sms");
						activity.startActivity(i);
					}

				}
			});

			holder.add_friend.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					int pos = position;
					user_id = items.get(pos).getUserid();
                    Addfriend(user_id, pos, holder.add_friend);
				}
			});

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = position;
                    boolean getMatch = items.get(position).getMatch();
                    user_id = items.get(pos).getUserid();

                    if(getMatch){
                        //Getting user profile info
                        //getTeammatesInfo(auth_token, user_id);
						UserProfileManager userProfileManager = new UserProfileManager();
						userProfileManager.getUserProfileInfo(mContext, activity, auth_token, user_id);
                    }else{
                        snackBar.setSnackBarMessage("This user does not exist");
                    }
                }
            });

			/*holder.addressbook_lay.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					DatabaseAdapter db = new DatabaseAdapter(activity);
					int pos = position;
					user_id = items.get(pos).getUserid();
					if (!TextUtils.isEmpty(user_id)) {
						*//**
						 * Getting the user details from the model object while
						 * user clicking on the list
						 *//*
						String nearbyfriend_user_id = items.get(pos)
								.getUserid();
						String username = items.get(pos).geusername();
						String image = items.get(pos).getFacebookImageUrl();
						String fullName = items.get(pos).getName();
						String[] name = fullName.split(" ");
						*//**
						 * Inserting user details to the local DB frrom the
						 * model object
						 *//*
						db.insertNonFriendDetails(nearbyfriend_user_id,
								username, name[0], name[1], image);

						*//**
						 * Showing the Dialog with two options View profile,
						 * Chat
						 *//*
						showDialog(username, nearbyfriend_user_id, true);

						*//*
						 * handler.postDelayed(new Runnable() {
						 * 
						 * @SuppressLint("NewApi")
						 * 
						 * @Override public void run() { try { Intent intent =
						 * new Intent(activity, UserProfileActivity.class);
						 * Bundle bndlanimation = ActivityOptions
						 * .makeCustomAnimation(activity, R.anim.pageanimation1,
						 * R.anim.pageanimation2) .toBundle();
						 * intent.putExtra("friend_id", user_id);
						 * intent.putExtra("intent", "sync_addressbook");
						 * intent.putExtra("intent_extra", "address_book");
						 * 
						 * activity.startActivity(intent, bndlanimation);
						 * activity.finish(); handler.removeCallbacks(this);
						 * 
						 * } catch (Exception e) { // TODO: handle exception
						 * e.printStackTrace(); } } }, 100);
						 *//*

					} else {
						Toast.makeText(activity,
								"User does not exist on Squadz",
								Toast.LENGTH_SHORT).show();
					}

				}
			});
*/


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return view;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return items.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*private class Addfriend extends AsyncTask<String, Void, Boolean> {
		String status, message;
		ImageView invite_friend;

		public Addfriend(ImageView invited_frnd) {
			// TODO Auto-generated constructor stub
			this.invite_friend = invited_frnd;
		}

		protected void onPreExecute() {
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			if (status.equalsIgnoreCase("success")) {
				invite_friend.setVisibility(View.GONE);
				Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
			} else if (status.equalsIgnoreCase("error")) {
				Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
			}
		}

		protected Boolean doInBackground(final String... args) {
			try {
				Jsoncordinator jParser1 = new Jsoncordinator();

				// http://54.164.124.188:3003/api/v1/add_friend?auth_token=&friend_id=
				JSONObject json2 = jParser1
						.getJSONFromUrl1(Api_Constants.addfrienddURL
								+ "auth_token=" + auth_token + "&friend_id="
								+ user_id);

				System.out.println("GETTING JSON IN ACTIVITY" + json2);
				status = json2.getString("status");

				message = json2.getString("message");

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}
	}*/

	private class ViewHolder {
		TextView tvname, tvPhoneNo;
		ImageView invite_to_squadz;
		ImageView add_friend;
		ImageView user_profile_pic;
		RelativeLayout addressbook_lay;

	}

/*	private void showDialog(final String username, final String userid,
			final boolean showChat) {

		final Dialog dialog = new Dialog(activity);

		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		dialog.setContentView(R.layout.popup_view);

		Button view_profile = (Button) dialog.findViewById(R.id.view_pro_btn);

		Button chat_btn = (Button) dialog.findViewById(R.id.chat_btn);

		Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);

		TextView username_txt = (TextView) dialog
				.findViewById(R.id.username_txt);

		username_txt.setText(username);

		dialog.setCancelable(true);

		dialog.show();

		// chat_btn.setVisibility(showChat ? View.VISIBLE : View.GONE);

		// chat_btn.setVisibility(View.GONE);

		view_profile.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// String name = creator_name.getText().toString();
				// viewProfile(userid, showChat);

				handler.postDelayed(new Runnable() {
					@SuppressLint("NewApi")
					@Override
					public void run() {
						try {
							Intent intent = new Intent(activity,
									UserProfileActivity.class);
							Bundle bndlanimation = ActivityOptions
									.makeCustomAnimation(activity,
											R.anim.pageanimation1,
											R.anim.pageanimation2).toBundle();
							intent.putExtra("friend_id", userid);
							intent.putExtra("intent", "sync_addressbook");
							intent.putExtra("intent_extra", "address_book");

							activity.startActivity(intent, bndlanimation);
							activity.finish();
							handler.removeCallbacks(this);
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
				}, 100);

			}
		});

		chat_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();

				if (cd.isConnectingToInternet()) {

					Api_Constants.RECIPIENT_USER_ID = userid;

					String user_name = username;

					// String first_name = db
					// .getTeammateFirstname(Api_Constants.RECIPIENT_USER_ID);
					// String last_name = db
					// .getTeammateLastname(Api_Constants.RECIPIENT_USER_ID);

					// Api_Constants.TEAMMATE_FULLNAME = first_name + " "
					// + last_name;

					new GetChatCHannel(Api_Constants.AUTH_TOKEN,
							Api_Constants.LOGGEDIN_USER_ID,
							Api_Constants.RECIPIENT_USER_ID, user_name)
							.execute();

				} else {
					Toast.makeText(activity,
							"Please connect to a working internet connection",
							Toast.LENGTH_LONG).show();
				}

			}
		});

		cancel_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

	}*/

	/**
	 * Creating the Channel for initialting the Chat
	 * 
	 * @author Bibhu
	 * 
	 */

	/*private class GetChatCHannel extends AsyncTask<String, String, String> {
		String getChannelid_API = null;
		String status, channel_id;;
		boolean is_avail;
		ProgressDialog dialog;
		String auth_token, logginuser_id, recipient_user_id, username;

		public GetChatCHannel(String loggedinuser_auth_token,
				String loggedinuser_id, String recipient_user_id,
				String username) {
			// TODO Auto-generated constructor stub
			this.auth_token = loggedinuser_auth_token;
			this.logginuser_id = loggedinuser_id;
			this.recipient_user_id = recipient_user_id;
			this.username = username;

			getChannelid_API = Api_Constants.baseURL_LIVE + "chats/"
					+ this.auth_token + "/" + this.logginuser_id + "/"
					+ this.recipient_user_id
					+ "/create_channel?type=individual";
		}

		@Override
		protected void onPreExecute() {

			customProgessDialog
					.showProgressDialog("Please wait while initializing chat seassion......");

		}

		protected String doInBackground(String... args) {
			System.out.println(getChannelid_API);

			try {
				Jsoncordinator jParser2 = new Jsoncordinator();

				// get JSON data from URL
				JSONObject json2 = jParser2.getJSONFromUrl1(getChannelid_API);

				status = json2.getString("status");

				if (status != null && status.equalsIgnoreCase("success")) {
					Api_Constants.CHAT_CHANNEL_ID = json2
							.getString("channel_id");
				}

			} catch (Exception e1) {

				e1.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String name1) {

			customProgessDialog.dismissDialog();
			if (status != null && status.equalsIgnoreCase("success")) {

				Intent intent = new Intent(activity, ChatActivity.class);
				intent.putExtra("to_name", username);
				intent.putExtra("type", "individual");

				activity.startActivity(intent);
			} else {
				Toast.makeText(activity, Api_Constants.CONNECTION_TIMEOUT_MSG,
						Toast.LENGTH_SHORT).show();

			}

		}

	}*/


	/**
	 * Calling the API to getting the sports
	 */

	public void Addfriend(final String user_id,
						final int position,
						final ImageView friendRequestIcon){
		//Displaying loader
		LoaderUtility.handleLoader(mContext, true);
		// should be a singleton
		OkHttpClient client = Singleton.getInstance().getClient();

		Request request = buildApiRequestBody(user_id);

		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				//Displying the error message to the User
				if(!ApplicationUtility.isConnected(activity)){
					ApplicationUtility.displayNoInternetMessage(activity);
				}else{
					ApplicationUtility.displayErrorMessage(activity, e.getMessage().toString());
				}

			}

			@Override
			public void onResponse(Call call, final Response response) throws IOException {
				String responseData = response.body().string();
				try{
					JSONObject responseObject = new JSONObject(responseData);
					status = responseObject.getString("status");
					message = responseObject.getString("message");
					if(status != null &&
							status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
						//Displaying the messages
						displayMessage(message);
					}else if(status != null &&
							status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
						//Displaying the messages
						displayMessage(message);
					}else{
						displayMessage("Some thing went wrong");
					}

				}catch(Exception ex){
					ex.printStackTrace();
					displayMessage("Some thing went wrong");
				}

				//Handling the loader state
                activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(status != null &&
								status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
							Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
							friendRequestIcon.setVisibility(View.GONE);
                            frd_status.add(user_id);
						}
					}
				});

				LoaderUtility.handleLoader(mContext, false);
			}
		});
	}

	/**
	 * Dispaly ing the message
	 * @param message status message
	 */
	public void displayMessage(final String message){
        activity.runOnUiThread(new Runnable() {
			public void run() {

				//Displaying the success message after successful sign up
				//Toast.makeText(ForgotPasswordScreen.this,message, Toast.LENGTH_LONG).show();
				// snackbar.setSnackBarMessage(message);
				Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
			}
		});
	}

	/**
	 * Function to handle the progress loader
	 * @param status true/false
	 */
	public void handleLoader(boolean status){
		if(status){
			dialog = ProgressDialog
					.show(activity, "", "Please wait ...");
			dialog.getWindow().setGravity(Gravity.CENTER);
			dialog.setCancelable(false);
		}else{
			dialog.dismiss();
		}
	}

	/**
	 * Function to build the Login APi request parameters
	 * @return APi request
	 */
	public Request buildApiRequestBody(String user_id){
//		HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"add_friend?").newBuilder();
		HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"add_friend?").newBuilder();
		urlBuilder.addQueryParameter("auth_token", auth_token);
		urlBuilder.addQueryParameter("friend_id", user_id);
		String url = urlBuilder.build().toString();
		Request request = new Request.Builder()
				.url(url)
				.method("POST", RequestBody.create(null, new byte[0]))
				.build();
		return request;
	}


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyProfile(String auth_token, String user_id) {

        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL +"users/" + auth_token + "/profile").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }



    /**
     * Getting teammates information
     */
    public void getTeammatesInfo(String auth_token, String user_id) {
        //Handling the loader state
		LoaderUtility.handleLoader(mContext, true);

        //Getting the OkHttpClient
		OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyProfile(auth_token, user_id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
				LoaderUtility.handleLoader(mContext, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String user_id = "",
                        username = "",phone_number = "",
                        name = "",email = "",
                        birth_date = "",image = "",
                        about_me = "",sport = "",
                        skill_level = "",rewards = "",
                        street_name = "",locality = "",
                        province_abbreviation = "",postal_code = "",
                        country = "";

                // Read data on the worker thread
                String responseData = response.body().string();

                profileOtherViewModel = new ProfileOtherViewModel();
                FriendsAdapter.profileOtherViewModelArrayList = new ArrayList<ProfileOtherViewModel>();
                ArrayList<String> secondary_aports_array = new ArrayList<String>();
                String is_profile_visible = null;

                try {
                    JSONObject jsonObject = new JSONObject(responseData);

                    if(jsonObject.has("status") && !jsonObject.isNull("status")) {
                        status = jsonObject.getString("status");
                    }
                    if(jsonObject.has("message") && !jsonObject.isNull("message")) {
                        message = jsonObject.getString("message");
                    }

                    if(jsonObject.has("is_profile_visible") && !jsonObject.isNull("is_profile_visible")) {
                        is_profile_visible = jsonObject.getString("is_profile_visible");
                    }

                    if(jsonObject.has("profile") && !jsonObject.isNull("profile")) {
                        JSONObject profile_jsonObject = jsonObject.getJSONObject("profile");

                        if(profile_jsonObject.has("user_id") && !profile_jsonObject.isNull("user_id")) {
                            user_id = profile_jsonObject.getString("user_id");
                        }
                        if(profile_jsonObject.has("username") && !profile_jsonObject.isNull("username")) {
                            username = profile_jsonObject.getString("username");
                        }
                        if(profile_jsonObject.has("phone_number") && !profile_jsonObject.isNull("phone_number"))
                        {
                            phone_number = profile_jsonObject.getString("phone_number");
                        }
                        if(profile_jsonObject.has("name") && !profile_jsonObject.isNull("name")) {
                            name = profile_jsonObject.getString("name");
                        }
                        if(profile_jsonObject.has("email") && !profile_jsonObject.isNull("email")) {
                            email = profile_jsonObject.getString("email");
                        }
                        if(profile_jsonObject.has("birth_date") && !profile_jsonObject.isNull("birth_date")) {
                            birth_date = profile_jsonObject.getString("birth_date");
                        }
                        if(profile_jsonObject.has("image") && !profile_jsonObject.isNull("image")) {
                            image = profile_jsonObject.getString("image");
                        }
                        if(profile_jsonObject.has("about_me") && !profile_jsonObject.isNull("about_me")) {
                            about_me = profile_jsonObject.getString("about_me");
                        }
                        if(profile_jsonObject.has("sport") && !profile_jsonObject.isNull("sport")) {
                            sport = profile_jsonObject.getString("sport");
                        }
                        if(profile_jsonObject.has("skill_level") && !profile_jsonObject.isNull("skill_level")) {
                            skill_level = profile_jsonObject.getString("skill_level");
                        }
                        if(profile_jsonObject.has("rewards") && !profile_jsonObject.isNull("rewards")) {
                            rewards = profile_jsonObject.getString("rewards");
                        }
                        if(profile_jsonObject.has("street_name") && !profile_jsonObject.isNull("street_name")) {
                            street_name = profile_jsonObject.getString("street_name");
                        }
                        if(profile_jsonObject.has("locality") && !profile_jsonObject.isNull("locality")) {
                            locality = profile_jsonObject.getString("locality");
                        }
                        if(profile_jsonObject.has("province_abbreviation") && !profile_jsonObject.isNull("province_abbreviation")) {
                            province_abbreviation = profile_jsonObject.getString("province_abbreviation");
                        }
                        if(profile_jsonObject.has("postal_code") && !profile_jsonObject.isNull("postal_code")) {
                            postal_code = profile_jsonObject.getString("postal_code");
                        }
                        if(profile_jsonObject.has("country") && !profile_jsonObject.isNull("country")) {
                            country = profile_jsonObject.getString("country");
                        }
                        if(profile_jsonObject.has("secondary_sports") && !profile_jsonObject.isNull("secondary_sports")) {
                            JSONArray secondary_sports_jsonArray = profile_jsonObject.getJSONArray("secondary_sports");
                            for (int i = 0; i < secondary_sports_jsonArray.length(); i++) {

                                JSONObject jsonObject1 = secondary_sports_jsonArray.getJSONObject(i);
                                String sport_id = jsonObject1.getString("sport_id");
                                String skillLevel = jsonObject1.getString("skill_level");

                                secondary_aports_array.add(sport_id);
                            }
                        }

                    }

                    profileOtherViewModel.setStatus(status);
                    profileOtherViewModel.setUser_id(user_id);
                    profileOtherViewModel.setUsername(username);
                    profileOtherViewModel.setPhone_number(phone_number);
                    profileOtherViewModel.setName(name);
                    profileOtherViewModel.setEmail(email);
                    profileOtherViewModel.setBirth_date(birth_date);
                    profileOtherViewModel.setImage(image);
                    profileOtherViewModel.setAbout_me(about_me);
                    profileOtherViewModel.setSport(sport);
                    profileOtherViewModel.setSkill_level(skill_level);
                    profileOtherViewModel.setRewards(rewards);
                    profileOtherViewModel.setStreet_name(street_name);
                    profileOtherViewModel.setLocality(locality);
                    profileOtherViewModel.setProvince_abbreviation(province_abbreviation);
                    profileOtherViewModel.setPostal_code(postal_code);
                    profileOtherViewModel.setCountry(country);
                    profileOtherViewModel.setSecondary_sports_array(secondary_aports_array);
                    profileOtherViewModel.setProfile_visibility(is_profile_visible);
                    FriendsAdapter.profileOtherViewModelArrayList.add(profileOtherViewModel);
                }catch (Exception exp) {
                    exp.printStackTrace();
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
						LoaderUtility.handleLoader(mContext, false);
                        if(status.equalsIgnoreCase("success")) {
                            FriendsAdapter.isFromFriendAdapter = true;
                            Intent intent = new Intent(activity, Profile.class);
                            activity.startActivity(intent);
                        } else if(!TextUtils.isEmpty(message)) {
                            snackBar.setSnackBarMessage(message);
                        } else{
                            snackBar.setSnackBarMessage("Network error, please try after sometime");
                        }
                    }
                });

            }
        });
    }

}
