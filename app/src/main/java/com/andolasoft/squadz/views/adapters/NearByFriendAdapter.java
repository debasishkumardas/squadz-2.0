package com.andolasoft.squadz.views.adapters;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.SplashScreen;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.UserProfileManager;
import com.bumptech.glide.Glide;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class NearByFriendAdapter extends BaseAdapter {
	Activity mContext;
	LayoutInflater inflater;
	private ArrayList<UserMolel> arraylist;
	SharedPreferences preferences;
	String auth_token, user_id;
	ArrayList<String> frd_status;
	DatabaseAdapter db;
    ProgressDialog dialog;
    String status, message;
    Context pageContext;

	public NearByFriendAdapter(Activity mContext, ArrayList<UserMolel> sort, Context context) {
		this.mContext = mContext;
		this.arraylist = sort;
        this.pageContext = context;
		inflater = LayoutInflater.from(mContext);
		preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		auth_token = preferences.getString("auth_token", null);
		frd_status = new ArrayList<String>();
		db = new DatabaseAdapter(this.mContext);
	}

	@Override
	public int getCount() {
		return arraylist.size();
	}

	@Override
	public UserMolel getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;

	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		convertView = null;
		convertView = inflater.inflate(R.layout.layout_facebook_sync, null);
		final TextView user_name = (TextView) convertView
				.findViewById(R.id.friendship_status);
		final TextView full_name = (TextView) convertView
				.findViewById(R.id.player_teamate__name);
		final ImageView user_img = (ImageView) convertView
				.findViewById(R.id.user_pic);
		final ImageView frd_req_icon = (ImageView) convertView
				.findViewById(R.id.invited_frnd);
        final CardView cv_row = (CardView) convertView.findViewById(R.id.card_view_event_list);
		View view_players = (View) convertView.findViewById(R.id.view_players);
		view_players.setVisibility(View.GONE);

		// Getting the user profile image
		String img_url = arraylist.get(position).getUserProfileImage();

		// Loading the profile image
		if (!TextUtils.isEmpty(img_url.trim())
				&& img_url != null
				&& !img_url.equalsIgnoreCase("null")) {
            Glide.with(mContext).load(img_url).into(user_img);
		}

		// Setting user ful name
		full_name.setText(arraylist.get(position).getUserFullName());

		user_name.setText("Known as " + arraylist.get(position).getUserName()
				+ " on Squadz");
		// Getting the User id
		user_id = arraylist.get(position).getUserId();

		// Checking condition for the friend request icon
		if (frd_status.contains(user_id)) {
			frd_req_icon.setVisibility(View.GONE);
		} else {
			if (arraylist.get(position).getUserStatus().length() == 0) {
				frd_req_icon.setVisibility(View.VISIBLE);
			} else {
				frd_req_icon.setVisibility(View.GONE);
			}
		}

		// Implementing the click event on the friend request icon
		frd_req_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				int pos = position;

				user_id = arraylist.get(pos).getUserId();

				Adduser(user_id,position, frd_req_icon);
			}
		});

		// Implementing the click event on each row of the near by friend list
		// view
        cv_row.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				/**
				 * Getting the user details from the model object while user
				 * clicking on the list
				 */
				int pos = position;

				String nearbyfriend_user_id = arraylist.get(pos).getUserId();

				String username = arraylist.get(pos).getUserName();

				String image = arraylist.get(pos).getUserProfileImage();

				String fullName = arraylist.get(pos).getUserFullName();

				String[] name = fullName.split(" ");

				/*db.insertNonFriendDetails(nearbyfriend_user_id, username,
						name[0], name[1], image);*/

                //Class call to get user profile information
                UserProfileManager  userProfileManager = new UserProfileManager();
                //Function call to handle the Profile API call and handling of the response
                userProfileManager.getUserProfileInfo(pageContext,
                        mContext,auth_token, nearbyfriend_user_id);
			}
		});

		return convertView;
	}

	/*// APi call for the friend request
	private class Addfriend extends AsyncTask<String, Void, Boolean> {
		ProgressDialog ALERT;
		String status, message;
		ImageView invite_friend;

		public Addfriend(ImageView invited_frnd) {
			// TODO Auto-generated constructor stub
			this.invite_friend = invited_frnd;
		}

		protected void onPreExecute() {

			customProgessDialog
					.showProgressDialog("Please wait while sending friend request ......");

		}

		@Override
		protected void onPostExecute(final Boolean success) {

			customProgessDialog.dismissDialog();
			if (status.equalsIgnoreCase("success")) {
				Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
				invite_friend.setVisibility(View.GONE);
				frd_status.add(user_id);

			} else if (status.equalsIgnoreCase("error")) {
				invite_friend.setVisibility(View.VISIBLE);
				Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
			}
		}

		protected Boolean doInBackground(final String... args) {
			try {
				Jsoncordinator jParser1 = new Jsoncordinator();

				JSONObject json2 = jParser1
						.getJSONFromUrl1(Api_Constants.addfrienddURL
								+ "auth_token=" + auth_token + "&friend_id="
								+ user_id);

				System.out.println("GETTING JSON IN ACTIVITY" + json2);
				status = json2.getString("status");

				message = json2.getString("message");

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}
	}*/



    /**
     * Calling the API to getting the sports
     */

    public void Adduser(final String user_id,
						final int position,
						final ImageView friendRequestIcon){
        //Displaying loader
		LoaderUtility.handleLoader(mContext, true);
        // should be a singleton
		OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBody(user_id);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
				if(!ApplicationUtility.isConnected(mContext)){
					ApplicationUtility.displayNoInternetMessage(mContext);
				}else{
                    ApplicationUtility.displayErrorMessage(mContext, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages
                        displayMessage(message);
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                            friendRequestIcon.setVisibility(View.GONE);
                            frd_status.add(user_id);
                        }
                    }
                });

				LoaderUtility.handleLoader(mContext, false);
            }
        });
    }

    /**
     * Dispaly ing the message
     * @param message status message
     */
    public void displayMessage(final String message){
        mContext.runOnUiThread(new Runnable() {
            public void run() {

                //Displaying the success message after successful sign up
                //Toast.makeText(ForgotPasswordScreen.this,message, Toast.LENGTH_LONG).show();
                // snackbar.setSnackBarMessage(message);
                Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(mContext, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }

    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String user_id){
        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"add_friend?").newBuilder();
		HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"add_friend?").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("friend_id", user_id);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }



/*
	private void showDialog(final String username, final String userid,
							final boolean showChat) {

		final Dialog dialog = new Dialog(mContext);

		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		dialog.setContentView(R.layout.popup_view);

		Button view_profile = (Button) dialog.findViewById(R.id.view_pro_btn);

		Button chat_btn = (Button) dialog.findViewById(R.id.chat_btn);

		Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);

		TextView username_txt = (TextView) dialog
				.findViewById(R.id.username_txt);

		username_txt.setText(username);

		dialog.setCancelable(true);

		dialog.show();

		// chat_btn.setVisibility(showChat ? View.VISIBLE : View.GONE);

		// chat_btn.setVisibility(View.GONE);

		view_profile.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// String name = creator_name.getText().toString();
				// viewProfile(userid, showChat);

				if (!TextUtils.isEmpty(userid)) {
					Intent intent = new Intent(mContext,
							UserProfileActivity.class);
					Bundle bndlanimation = ActivityOptions.makeCustomAnimation(
							mContext, R.anim.pageanimation1,
							R.anim.pageanimation2).toBundle();
					intent.putExtra("friend_id", userid);
					intent.putExtra("intent", "Nearbyfriend");
					intent.putExtra("intent_extra", "NearbygameFriend");

					mContext.startActivity(intent, bndlanimation);

				} else {
					Toast.makeText(mContext, "User does not exist on Squadz",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		chat_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();

				if (cd.isConnectingToInternet()) {

					Api_Constants.RECIPIENT_USER_ID = userid;

					String user_name = username;

					// String first_name = db
					// .getTeammateFirstname(Api_Constants.RECIPIENT_USER_ID);
					// String last_name = db
					// .getTeammateLastname(Api_Constants.RECIPIENT_USER_ID);

					// Api_Constants.TEAMMATE_FULLNAME = first_name + " "
					// + last_name;

					new GetChatCHannel(Api_Constants.AUTH_TOKEN,
							Api_Constants.LOGGEDIN_USER_ID,
							Api_Constants.RECIPIENT_USER_ID, user_name)
							.execute();

				} else {
					Toast.makeText(mContext,
							"Please connect to a working internet connection",
							Toast.LENGTH_LONG).show();
				}

			}
		});

		cancel_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

	}

	private class GetChatCHannel extends AsyncTask<String, String, String> {
		String getChannelid_API = null;
		String status, channel_id;;
		boolean is_avail;
		ProgressDialog dialog;
		String auth_token, logginuser_id, recipient_user_id, username;

		public GetChatCHannel(String loggedinuser_auth_token,
							  String loggedinuser_id, String recipient_user_id,
							  String username) {
			// TODO Auto-generated constructor stub
			this.auth_token = loggedinuser_auth_token;
			this.logginuser_id = loggedinuser_id;
			this.recipient_user_id = recipient_user_id;
			this.username = username;

			getChannelid_API = Api_Constants.baseURL_LIVE + "chats/"
					+ this.auth_token + "/" + this.logginuser_id + "/"
					+ this.recipient_user_id
					+ "/create_channel?type=individual";
		}

		@Override
		protected void onPreExecute() {

			customProgessDialog
					.showProgressDialog("Please wait while initializing chat seassion......");

		}

		protected String doInBackground(String... args) {
			System.out.println(getChannelid_API);

			try {
				Jsoncordinator jParser2 = new Jsoncordinator();
				// get JSON data from URL

				JSONObject json2 = jParser2.getJSONFromUrl1(getChannelid_API);

				status = json2.getString("status");

				if (status != null && status.equalsIgnoreCase("success")) {
					Api_Constants.CHAT_CHANNEL_ID = json2
							.getString("channel_id");
				}

			} catch (Exception e1) {

				e1.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String name1) {

			customProgessDialog.dismissDialog();
			if (status != null && status.equalsIgnoreCase("success")) {

				Intent intent = new Intent(mContext, ChatActivity.class);
				intent.putExtra("to_name", username);
				intent.putExtra("type", "individual");

				mContext.startActivity(intent);
			} else {
				Toast.makeText(mContext, Api_Constants.CONNECTION_TIMEOUT_MSG,
						Toast.LENGTH_SHORT).show();

			}

		}

	}*/

}
