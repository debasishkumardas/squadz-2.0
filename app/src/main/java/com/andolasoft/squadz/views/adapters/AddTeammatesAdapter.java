package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.AddTeammates;
import com.andolasoft.squadz.activities.TeamPlayerPicker;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.AddPlayerModel;
import com.andolasoft.squadz.models.TeamsModel;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class AddTeammatesAdapter extends
        RecyclerView.Adapter<AddTeammatesAdapter.MyViewHolder> {

    public static String userId,auth_token;
    private Activity context;
    ArrayList<TeamsModel> teamsModelArrayList;
    DatabaseAdapter db;

    public AddTeammatesAdapter(Activity context, ArrayList<TeamsModel> teamsModelArrayList) {
        this.context = context;
        this.teamsModelArrayList = teamsModelArrayList;
        this.db = new DatabaseAdapter(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_add_teammates_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return teamsModelArrayList.size();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final String teamName = teamsModelArrayList.get(position).getTeamName();
        holder.team_name_addTeam.setText(teamName);
        String teamId = teamsModelArrayList.get(position).getTeamId();

        if(Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.contains(teamId)) {
            holder.addTeam_team_checkbox.setChecked(false);
        } else{
            holder.addTeam_team_checkbox.setChecked(teamsModelArrayList.get(position).isCheckboxSelected());
        }

        holder.addTeam_team_checkbox.setTag(position);
        holder.addTeam_team_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int getPosition = (Integer) v.getTag();
                if(holder.addTeam_team_checkbox.isChecked()) {
                    teamsModelArrayList.get(getPosition).setCheckboxSelected(true);
                    String teamId = teamsModelArrayList.get(position).getTeamId();
                    /**Fetching players ids based on the team id to display selected checkbox in the teammates list view*/

                    if(db.getPlayersId(teamId).size() > 0) {
                        for (int i = 0; i < db.getPlayersId(teamId).size(); i++) {
                            Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.add(db.getPlayersId(teamId).get(i));
                            Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.add(db.getPlayersId(teamId).get(i));
                        }
                        HashSet<String> hashSet = new HashSet<String>();
                        hashSet.addAll(Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES);
                        Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.clear();
                        Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.addAll(hashSet);
                    }
                    AddTeammates.teamTeammatesAdapter.notifyDataSetChanged();
                } else{
                    teamsModelArrayList.get(getPosition).setCheckboxSelected(false);

                    String teamId = teamsModelArrayList.get(position).getTeamId();
                    for (int i = 0; i < db.getPlayersId(teamId).size(); i++) {
                        Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.remove(db.getPlayersId(teamId).get(i));
                        Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.remove(db.getPlayersId(teamId).get(i));

                        HashSet<String> hashSet = new HashSet<String>();
                        hashSet.addAll(Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES);
                        Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.clear();
                        Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.addAll(hashSet);
                    }
                    AddTeammates.teamTeammatesAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView team_name_addTeam;
        CheckBox addTeam_team_checkbox;
        public MyViewHolder(View view) {
            super(view);
            team_name_addTeam = (TextView) view.findViewById(R.id.team_name_addTeam);
            addTeam_team_checkbox = (CheckBox) view.findViewById(R.id.addTeam_team_checkbox);
        }
    }
}