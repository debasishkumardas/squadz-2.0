package com.andolasoft.squadz.views.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;


import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.Constants;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by SpNayak on 1/3/2017.
 */

public class FilterVenuesSpecificationAdapter extends BaseAdapter {
    ArrayList<String> checkbox_array = new ArrayList<>();
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    private ArrayList<String> venuesNameList = new ArrayList<String>();
    private LayoutInflater layoutInflater;
    private Context context;

    public FilterVenuesSpecificationAdapter(Context context, ArrayList<String> venuesNameList) {
        this.venuesNameList = venuesNameList;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
        //prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs = context.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    @Override
    public int getCount() {
        return venuesNameList.size();
    }

    @Override
    public Object getItem(int position) {
        return venuesNameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_filter_venues_layout, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        mViewHolder.venues_spec_name.setText(venuesNameList.get(position));

        //Checking whether any check boxes selection are exist to display the check mark in Checkbox
        Set<String> set = prefs.getStringSet("VENUES_SPECIFICATIONS", null);

        if (set != null) {
            Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY = new ArrayList<String>(set);

            if (Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY.contains(venuesNameList.get(position))) {
                mViewHolder.venues_spec_checkbox.setChecked(true);
            }

        }
        mViewHolder.venues_spec_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean selected) {

                if (selected) {
                    Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY.add(venuesNameList.get(position));
                } else {
                    Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY.remove(venuesNameList.get(position));
                    Constants.UNCHECK_SPEC = true;
                }
            }
        });

        return convertView;
    }

    private class MyViewHolder {
        TextView venues_spec_name;
        CheckBox venues_spec_checkbox;

        public MyViewHolder(View item) {
            venues_spec_name = (TextView) item.findViewById(R.id.venues_spec_name);
            venues_spec_checkbox = (CheckBox) item.findViewById(R.id.venues_spec_checkbox);

        }
    }
}

