package com.andolasoft.squadz.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.AddFriendToChat;
import com.andolasoft.squadz.activities.ChatActivity;
import com.andolasoft.squadz.activities.TeamPlayerPicker;
import com.andolasoft.squadz.fragments.ChatFragmnet;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.ChatConversationModel;
import com.andolasoft.squadz.models.Model_Scheduler;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.bumptech.glide.Glide;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class TeammateListAdapter extends BaseAdapter implements Filterable {
	Context mContext;
	LayoutInflater inflater;
	ArrayList<Model_Scheduler> team_items;
	private ArrayList<Model_Scheduler> filteredList;
	private List<Model_Scheduler> previouslist;
	CustomFilter filter;/* = new CustomFilter(TeammateListAdapter.this); */
	DatabaseAdapter db;
    public static boolean isChannelCreated = false;
    ProgressDialog dialog;
    String loggedin_user_id;
    boolean isChildFound = false;
    int pageRedirectionCount = 0;
    public static ArrayList<String> userIds;
    public static ArrayList<String> userIds_duplicate;

	public TeammateListAdapter(Context mContext,
							   ArrayList<Model_Scheduler> team_items) {

		this.previouslist = new ArrayList<Model_Scheduler>();
		this.mContext = mContext;
		this.team_items = team_items;
		this.previouslist = team_items;
		inflater = LayoutInflater.from(mContext);
		db = new DatabaseAdapter(mContext);
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
        loggedin_user_id = preferences.getString("loginuser_id", null);
		filteredList = new ArrayList<Model_Scheduler>();
		filteredList.addAll(previouslist);
		// filteredList.addAll(team_items);
		filter = new CustomFilter(TeammateListAdapter.this);
        if(userIds == null){
            userIds = new ArrayList<>();
            userIds_duplicate = new ArrayList<>();
        }
	}

	@Override
	public int getCount() {
		return team_items.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;

	}

	@SuppressLint({ "InflateParams", "ViewHolder" })
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		convertView = null;
		convertView = inflater.inflate(R.layout.teammate_items, null);
		TextView full_name_txt = (TextView) convertView
				.findViewById(R.id.textView_name);
		TextView user_name_txt = (TextView) convertView
				.findViewById(R.id.textView_username);
		TextView textView_message = (TextView) convertView
				.findViewById(R.id.textView_message);
        CheckBox add_player_checkbox = (CheckBox) convertView.
                findViewById(R.id.add_player_checkbox);
        add_player_checkbox.setTag(position);
		ImageView userImage  = (ImageView) convertView.findViewById(R.id.user_icon);
		final String user_name = team_items.get(position).getFriendusername();
		final String first_name = team_items.get(position).getFirstname();
		final String last_name = team_items.get(position).getLastName();
		final String image = team_items.get(position).getImageUrl();
        final String user_id = team_items.get(position).getfriendid();

        if(userIds.contains(user_id)){
            add_player_checkbox.setChecked(true);
        }

		if(!TextUtils.isEmpty(image.trim())){
			Glide.with(mContext).load(image).into(userImage);
		}else{
            userImage.setBackgroundResource(R.mipmap.ic_account_gray);
		}
		// String[] tempItems =
		// team_items.get(position).toString().split("###");
		full_name_txt.setText(user_name);
		user_name_txt.setText(first_name + " " + last_name);

		/*convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				int pos = position;
				String userName = team_items.get(pos).getFriendusername();

                String imageURL = team_items.get(pos).getImageUrl();
                String userId = team_items.get(pos).getfriendid();

                //Function call to get all the data for creating a team
				//createChannel(userName,channel_id,imageURL,userId);

			}
		});*/


        add_player_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                int getPosition = (Integer) buttonView.getTag();
                String userId = team_items.get(getPosition).getfriendid();

                if(isChecked) {
                    if(!userIds.contains(userId)){
                        //userModelArray.get(position).setCheckboxSelected(true);
                        userIds.add(userId);
                    }
                } else{
                    //userModelArray.get(position).setCheckboxSelected(false);
                    userIds.remove(userId);
                }
            }
        });

		AddFriendToChat.btnDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseAdapter db = new DatabaseAdapter(mContext);

                String getButtonText = AddFriendToChat.btnDone.getText().toString();

                if(getButtonText != null && getButtonText.equalsIgnoreCase("Done")){
                    if(userIds.size() == 1){
                        String channel_id = ApplicationUtility.getChannelId();
                        AppConstants.isChannedCreated = true;
                        String userName = db.getcurrentfriendUserName(userIds.get(0));
                        String userImage = db.getcurrentfriendProfileImage(userIds.get(0));
                        fetchValue(userIds.get(0),userImage,userName);
                        //handlePageRedirection();
                    }else if(userIds.size() > 1){
                        AppConstants.isChannedCreated = true;
                        AppConstants.USER_SELECTED_CHAT_TYPE = "group";
                        String channel_id = ApplicationUtility.getChannelId();
                        AppConstants.CHAT_CHANNEL_ID = channel_id;
                        isChannelCreated = true;
                        handlePageRedirection();
                    }else if(userIds.size() == 0){
                        Toast.makeText(mContext, "Please select a friend to start the conversation", Toast.LENGTH_SHORT).show();
                    }
                }else if(getButtonText != null && getButtonText.equalsIgnoreCase("Update")){
                        AppConstants.isGroupUpdated = true;
                        AppConstants.isUserGointToUpdateTheGroup = false;
                        //Redirecting the page
                        //Intent intent = new Intent(mContext, ChatActivity.class);
                        //mContext.startActivity(intent);
                        ((Activity) mContext).finish();
                }

            }
        });

		return convertView;
	}

    /**
     * Function responsible for handling the page redirections
     * @param userName
     * @param channelId
     * @param imageUrl
     * @param userId
     */
	public void createChannel(String userName, String channelId,
							  String imageUrl, String userId){
        pageRedirectionCount = pageRedirectionCount + 1;

        if(pageRedirectionCount > 1){

        }else{
            AppConstants.USER_SELECTED_CHAT_TYPE = "individual";
            AppConstants.CHAT_USER_NAME = userName;
            AppConstants.CHAT_CHANNEL_ID = channelId;
            AppConstants.CHAT_USER_IMAGE_URL = imageUrl;
            AppConstants.CHAT_USER_ID = userId;
            isChannelCreated = true;
            Intent intent = new Intent(mContext, ChatActivity.class);
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        }

	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return filter;
	}

    /**
     * Function to handle the page redirections
     */
    public  void handlePageRedirection(){
        pageRedirectionCount = pageRedirectionCount + 1;

        if(pageRedirectionCount > 1){

        }else{
            Intent intent = new Intent(mContext, ChatActivity.class);
            mContext.startActivity(intent);((Activity) mContext).finish();
        }

    }

	/*
	 * public class filter_here extends Filter {
	 * 
	 * @SuppressLint("DefaultLocale")
	 * 
	 * @Override protected FilterResults performFiltering(CharSequence
	 * constraint) { // TODO Auto-generated method stub
	 * 
	 * FilterResults Result = new FilterResults(); // if constraint is empty
	 * return the original names if (constraint.length() == 0) { Result.values =
	 * team_items; Result.count = team_items.size(); return Result; }
	 * 
	 * 
	 * else { Result.values = team_items; Result.count = team_items.size(); }
	 * 
	 * 
	 * ArrayList<Model_Scheduler> Filtered_Names = new
	 * ArrayList<Model_Scheduler>(); String filterString =
	 * constraint.toString().toLowerCase(); String filterableString;
	 * 
	 * 
	 * final String filterPattern = constraint.toString().toLowerCase().trim();
	 * for (final TaskList list : previouslist) { if
	 * (list.getTaskName().toLowerCase().startsWith(filterPattern)) {
	 * filteredList.add(list); } }
	 * 
	 * 
	 * for (int i = 0; i < team_items.size(); i++) { filterableString =
	 * team_items.get(i).getFriendusername(); if
	 * (filterableString.toLowerCase().startsWith(filterString)) { if
	 * (!Filtered_Names.contains(team_items.get(i)))
	 * Filtered_Names.add(team_items.get(i)); }
	 * 
	 * filterableString = team_items.get(i).getFirstname(); if
	 * (filterableString.toLowerCase().startsWith(filterString)) { if
	 * (!Filtered_Names.contains(team_items.get(i)))
	 * Filtered_Names.add(team_items.get(i)); }
	 * 
	 * filterableString = team_items.get(i).getLastName(); if
	 * (filterableString.toLowerCase().startsWith(filterString)) { if
	 * (!Filtered_Names.contains(team_items.get(i)))
	 * Filtered_Names.add(team_items.get(i)); } }
	 * 
	 * Result.values = Filtered_Names; Result.count = Filtered_Names.size();
	 * 
	 * return Result; }
	 * 
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override protected void publishResults(CharSequence constraint,
	 * FilterResults results) { // TODO Auto-generated method stub team_items =
	 * (ArrayList<Model_Scheduler>) results.values; notifyDataSetChanged();
	 * ListFilter.updateListviewHeight();
	 * 
	 * } }
	 */

	public class CustomFilter extends Filter {

		private TeammateListAdapter mAdapter;

		private CustomFilter(TeammateListAdapter mAdapter) {
			super();
			this.mAdapter = mAdapter;
		}

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			filteredList.clear();
			// ArrayList<Model_Scheduler> Filtered_Names = new
			// ArrayList<Model_Scheduler>();
			final FilterResults results = new FilterResults();
			if (constraint.length() == 0) {
				filteredList.addAll(previouslist);
			} else {
				final String filterPattern = constraint.toString()
						.toLowerCase().trim();
				for (final Model_Scheduler list : previouslist) {
					if (list.getFriendusername().toLowerCase()
							.startsWith(filterPattern)) {
						filteredList.add(list);
					}

					else if (list.getFirstname().toLowerCase()
							.startsWith(filterPattern)) {
						filteredList.add(list);
					}

					else if (list.getLastName().toLowerCase()
							.startsWith(filterPattern)) {
						filteredList.add(list);
					}

				}
			}

			System.out.println("Count Number " + filteredList.size());
			team_items = filteredList;
			results.values = filteredList;
			results.count = filteredList.size();
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// System.out.println("Count Number 2 " + ((List<Model_Scheduler>)
			// results.values).size());

			// team_items = (ArrayList<Model_Scheduler>) results.values;
			this.mAdapter.notifyDataSetChanged();
			AddFriendToChat.updateListviewHeight();

		}
	}

/*	private class GetChatCHannel extends AsyncTask<String, String, String> {
		String getChannelid_API = null;
		String status, channel_id;;
		boolean is_avail;
		ProgressDialog dialog;
		String auth_token, logginuser_id, recipient_user_id, username,
				user_image;

		public GetChatCHannel(String loggedinuser_auth_token,
				String loggedinuser_id, String recipient_user_id,
				String username, String user_image) {
			// TODO Auto-generated constructor stub
			this.auth_token = loggedinuser_auth_token;
			this.logginuser_id = loggedinuser_id;
			this.recipient_user_id = recipient_user_id;
			this.username = username;
			this.user_image = user_image;

			getChannelid_API = Api_Constants.baseURL_LIVE + "chats/"
					+ this.auth_token + "/" + this.logginuser_id + "/"
					+ this.recipient_user_id
					+ "/create_channel?type=individual";
		}

		@Override
		protected void onPreExecute() {

			customProgessDialog
					.showProgressDialog("Please wait while initializing chat seassion......");

		}

		protected String doInBackground(String... args) {
			System.out.println(getChannelid_API);

			try {
				Jsoncordinator jParser2 = new Jsoncordinator();
				// get JSON data from URL

				JSONObject json2 = jParser2.getJSONFromUrl1(getChannelid_API);

				status = json2.getString("status");
				Api_Constants.CHAT_CHANNEL_ID = json2.getString("channel_id");

			} catch (Exception e1) {

				e1.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String name1) {

			customProgessDialog.dismissDialog();
			if (status != null && status.equalsIgnoreCase("success")) {
				db.insertChatInfoData(Api_Constants.CHAT_CHANNEL_ID,
						logginuser_id, recipient_user_id, username, user_image,
						"individual", Api_Constants.TEAMMATE_FULLNAME, null, "");

				Intent intent = new Intent(mContext, ChatActivity.class);
				intent.putExtra("to_name", username);
				intent.putExtra("type", "individual");
				mContext.startActivity(intent);
			} else {
				Toast.makeText(mContext, Api_Constants.CONNECTION_TIMEOUT_MSG,
						Toast.LENGTH_SHORT).show();

			}

		}


	}*/


    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(mContext, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }


    /**
     * Fetching conversation data from firebase
     */
    public void fetchValue(final String userId,
                           final String user_image_url,
                           final String user_name){

        //Handling the loader
        LoaderUtility.handleLoader(mContext, true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(mContext);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(loggedin_user_id);

        //callback to handle the user conversation data
        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {

                if(AppConstants.isChannedCreated){
                Log.d("TAG", dataSnapshot.toString());
                //gatting all the conversation data and assigning them to the model class
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String message_sender_id = (String) postSnapshot.child("senderId").getValue();
                    String message_receiver_id = (String) postSnapshot.child("recieverID").getValue();

                    if(message_sender_id != null &&
                            message_receiver_id != null){
                        if(userId.equalsIgnoreCase(message_sender_id) ||
                                userId.equalsIgnoreCase(message_receiver_id)){

                            isChildFound = true;

                            //Fetching all the data from data snap shot object
                            String channel_id = (String) postSnapshot.child("channelId").getValue();
                            String message_sender_image_url = (String) postSnapshot.child("imageUrl").getValue();
                            String message_username = (String) postSnapshot.child("chatUserName").getValue();

                            //Assigning the values to the
                            AppConstants.CHAT_USER_NAME = message_username;
                            AppConstants.CHAT_CHANNEL_ID = channel_id;
                            AppConstants.CHAT_USER_IMAGE_URL = message_sender_image_url;
                            AppConstants.CHAT_USER_ID = userId;
                            AppConstants.CHAT_TYPE = "individual";
                            isChannelCreated = false;

                            //Handling the page redirection
                            handlePageRedirection();
							break;
                        }
                    }
                }

                if(!isChildFound){
                    //Creating a random number for channel id
                    String channel_id = ApplicationUtility.getChannelId();
                    createChannel(user_name,channel_id,user_image_url,userId);
                    isChildFound = false;
                    return;
                }
                }

                //Handling the loader
                LoaderUtility.handleLoader(mContext, false);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

}
