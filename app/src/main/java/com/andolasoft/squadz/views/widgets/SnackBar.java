package com.andolasoft.squadz.views.widgets;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.nispok.snackbar.listeners.ActionClickListener;

/**
 * Created by Manas on 13-Jul-16.
 */
public class SnackBar {
    Context ctx;

    public SnackBar(Context ctx) {
        this.ctx = ctx;
    }

    public void setSnackBarMessage(String message) {
        //Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/proxima_nova_bold.ttf");
        SnackbarManager.show(
                Snackbar.with(this.ctx) // context
                        .text(message) // text to display
                        //.textTypeface(typeface)
                        .actionLabel("OK") // action button label
                        //.actionLabelTypeface(typeface)
                        .color(Color.parseColor("#19212C"))//Change the background color
                        .actionColor(Color.RED)
                        .duration(Snackbar.SnackbarDuration.LENGTH_LONG)
                        .type(SnackbarType.MULTI_LINE)
                        .swipeToDismiss(true)
                        .actionListener(new ActionClickListener() {
                            @Override
                            public void onActionClicked(Snackbar snackbar) {

                            }
                        }) // action button's ActionClickListener
                , (Activity) this.ctx);
    }

}
