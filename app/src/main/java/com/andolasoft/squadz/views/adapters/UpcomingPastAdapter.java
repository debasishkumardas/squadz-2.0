package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.EventDetail;
import com.andolasoft.squadz.activities.NavigationDrawerActivity;
import com.andolasoft.squadz.activities.UpcomingPastDetail;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.models.UpcomingPastModel;
import com.andolasoft.squadz.models.UpcomingPastModel;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by SpNayak on 1/2/2017.
 */

public class UpcomingPastAdapter extends RecyclerView.Adapter<UpcomingPastAdapter.MyViewHolder> implements Filterable {

    public List<UpcomingPastModel> filteredList;
    public List<UpcomingPastModel> previouslist;
    private ArrayList<UpcomingPastModel> upcomingPastModelList;
    private Activity context;
    private CustomFilter mFilter;
    private ProgressDialog dialog;
    public UpcomingPastModel UpcomingPastModel;
    public static ArrayList<UpcomingPastModel> upcomingPastModelArrayList;
    SnackBar snackBar;
    SharedPreferences prefs;
    String event_creator_image;
    boolean isAddedToFavorite = false;
    public static String userId,status, event_startTime, event_endtime;
    String image;
    //public static ArrayList<UserMolel> userModelArary = new ArrayList<>();
    //public static ArrayList<String> userIdArray = new ArrayList<>();
    double currentLatitude, currentLongitude;
    public static GPSTracker gpsTracker;
    String event_type = "";
    public EventListingModel eventListingModel;
    public static ArrayList<EventListingModel> eventListingModelArrayList;
    public static String event_id;


    public UpcomingPastAdapter(Activity context, ArrayList<UpcomingPastModel> upcomingPastModelList, String event_type) {
        this.upcomingPastModelList = upcomingPastModelList;
        this.context = context;
        this.previouslist = upcomingPastModelList;
        filteredList = new ArrayList<UpcomingPastModel>();
        filteredList.addAll(previouslist);
        mFilter = new CustomFilter(UpcomingPastAdapter.this);
        snackBar = new SnackBar(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        event_creator_image = prefs.getString("image", null);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        userId = prefs.getString("loginuser_id", null);
        getCurrentLocation();
        gpsTracker = new GPSTracker(context);
        this.event_type = event_type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_layout_upcoming_past, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return upcomingPastModelList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final UpcomingPastModel UpcomingPastModel = upcomingPastModelList.get(position);
        String price = UpcomingPastModel.getEvent_Price();
        String minimum_price = UpcomingPastModel.getEvent_Minimum_Price();
        String eventName = UpcomingPastModel.getEvent_Name();
        String ratings = UpcomingPastModel.getEvent_Ratings();
        String reviews = UpcomingPastModel.getEvent_Reviews();
        String skillLevel = UpcomingPastModel.getSkill_level();
        String date = UpcomingPastModel.getEventDate();
        String eventStartTime = UpcomingPastModel.getEventStartTime();
        String eventEndTime = UpcomingPastModel.getEventEndTime();
        String creatorUserName = UpcomingPastModel.getCreator_name();
        String totalNumberOfPlayers =  UpcomingPastModel.getCapacity();
        String totalNumberOfParticipants =  UpcomingPastModel.getEvent_Total_Participants();
        String creator_profile_image =  UpcomingPastModel.getEvent_Creator_Image();
        String event_sport_type =  UpcomingPastModel.getEvent_Sports_Type();
        String address =  UpcomingPastModel.getAddress();
        boolean isFavorite = UpcomingPastModel.isFavorite();
        String object_Type = UpcomingPastModel.getObject_Type();
        String total_price = UpcomingPastModel.getEvent_Total_Price();
        String lis_Id = UpcomingPastModel.getList_Id();

        if(!event_type.equalsIgnoreCase("UPCOMING"))
        {
            /**Hiding projected cost field for past games*/
            holder.tv_minimum_price.setVisibility(View.GONE);
            holder.tv_eventspots.setVisibility(View.GONE);
            holder.rating_venue.setVisibility(View.INVISIBLE);
            holder.rating_event_reviews.setVisibility(View.INVISIBLE);
        }
        else{
            /**Hiding projected cost field for past games*/
            holder.tv_minimum_price.setVisibility(View.VISIBLE);
            holder.tv_eventspots.setVisibility(View.VISIBLE);
        }

        ArrayList<String> imageArray = UpcomingPastModel.getImages_array();

        if(imageArray.size() > 0){
            image = imageArray.get(0);
            Glide.with(context).load(image).into(holder.event_listing_image);
        }
        else if(TextUtils.isEmpty(lis_Id)){
            //holder.event_listing_image.setBackgroundResource(R.mipmap.ic_account_gray);
            int getVenueImage = SportsImagePicker.getDefaultSportImage(event_sport_type);
            holder.event_listing_image.setBackgroundResource(getVenueImage);
        }
        else{
            int image = UpcomingPastModel.getImage();
            holder.event_listing_image.setBackgroundResource(image);
        }

        if(!TextUtils.isEmpty(skillLevel)){
            holder.tv_eventSkillLevel.setText(skillLevel);
        }

        if(!TextUtils.isEmpty(date)){
            holder.tv_eventDatetime.setText(ApplicationUtility.formattedDate(date) +" - "+
                    eventStartTime +" to " +
                    eventEndTime);
        }
        holder.upcoming_event_address_listing_name.setText(address);


        if(TextUtils.isEmpty(lis_Id) && (!object_Type.equalsIgnoreCase("Event") || !object_Type.equalsIgnoreCase("event")))
        {
            holder.tv_minimum_price.setVisibility(View.INVISIBLE);
            holder.event_price.setVisibility(View.GONE);
        }
        else if(!TextUtils.isEmpty(lis_Id) &&
                (object_Type.equalsIgnoreCase("Game") ||
                        object_Type.equalsIgnoreCase("game"))) {
            if (!TextUtils.isEmpty(total_price)) {
                //holder.tv_minimum_price.setVisibility(View.VISIBLE);
                if (!total_price.equalsIgnoreCase("0")) {
                    holder.event_price.setText("$ " + total_price + " / Player");
                    //holder.tv_minimum_price.setText("As low as $" + minimum_price);
                    double total_projected_cost = Double.valueOf(total_price) / Double.valueOf(totalNumberOfParticipants);
                    holder.tv_minimum_price.setText("Projected cost: $" + ApplicationUtility.twoDigitAfterDecimal(total_projected_cost));
                } else {
                    holder.event_price.setText("FREE");
                    holder.tv_minimum_price.setText("FREE");
                }
            } else {
                holder.event_price.setTextColor(Color.parseColor("#000000"));
                holder.event_price.setText("FREE");
            }
        }
        else if(object_Type.equalsIgnoreCase("Event") || object_Type.equalsIgnoreCase("event"))
        {
            holder.tv_minimum_price.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(total_price)) {
                if (!total_price.equalsIgnoreCase("0")) {
                    holder.event_price.setText("$ " + total_price + " / Player");
                } else {
                    holder.event_price.setText("FREE");
                }
            } else {
                holder.event_price.setTextColor(Color.parseColor("#000000"));
                holder.event_price.setText("FREE");
            }
        }


       /* if(!TextUtils.isEmpty(price)) {
            if(!price.equalsIgnoreCase("0")){
                holder.event_price.setText("$ "+price+" / Player");
                holder.tv_minimum_price.setText("As low as $" + minimum_price);
            }else{
                holder.event_price.setText("FREE");
                holder.tv_minimum_price.setText("FREE");
            }
        }
        else{
            holder.event_price.setTextColor(Color.parseColor("#000000"));
            holder.event_price.setText("FREE");
        }*/

        holder.rating_venue.setRating(Float.valueOf(ratings));
        holder.rating_event_reviews.setText(reviews+" Reviews");
        holder.tv_totalnoparticiapnts.setText(totalNumberOfParticipants/* + " of " + totalNumberOfPlayers*/);

        if(!TextUtils.isEmpty(totalNumberOfPlayers) && !TextUtils.isEmpty(totalNumberOfParticipants)) {
            int leftSpots = Integer.valueOf(totalNumberOfPlayers) - Integer.valueOf(totalNumberOfParticipants);
            holder.tv_eventspots.setText(String.valueOf(leftSpots) + " spots remaining");
        }
        else{
            holder.tv_eventspots.setText("0 spot remaining");
        }

        /*//Displaying venue courts image
        if(!TextUtils.isEmpty(image)){
            Glide.with(context).load(image).into(holder.event_listing_image);
        }else{
           // holder.event_listing_image.setBackgroundResource(R.mipmap.ic_account_gray);
            int getVenueImage = SportsImagePicker.getDefaultSportImage(event_sport_type);
            holder.event_listing_image.setBackgroundResource(getVenueImage);
        }*/

        //Displaying the Event Name
        if(!TextUtils.isEmpty(eventName)){
            holder.tv_event_name.setText(eventName);
        }

        //Displaying the creator name
        if(!TextUtils.isEmpty(creatorUserName)){
            holder.tv_event_creator_name.setText(creatorUserName);
        }else{
            holder.tv_event_creator_name.setText("Name Display");
        }

        //Displaying the Event creator Image
        if(!TextUtils.isEmpty(creator_profile_image) &&
                creator_profile_image.equalsIgnoreCase("null") &&
                creator_profile_image != null){
            Glide.with(context).load(creator_profile_image).into(holder.imagv_event_creator_image);
        }

        if(isFavorite){
            holder.favorite_button.setFavorite(true);
        }else{
            holder.favorite_button.setFavorite(false);
        }

        //Calculating distance from current to destination
        /*double current_loc_latitude = Blank2Fragment.currLocLatitude;
        double current_loc_longitude = Blank2Fragment.currLocLongitude;*/
        double current_loc_latitude = gpsTracker.getLatitude();
        double current_loc_longitude = gpsTracker.getLongitude();


        if(!TextUtils.isEmpty(UpcomingPastModel.getLatitude())) {
            double dest_loc_latitude = Double.valueOf(UpcomingPastModel.getLatitude());
            double dest_loc_longitude = Double.valueOf(UpcomingPastModel.getLongitude());

            double distance = ApplicationUtility.distance(currentLatitude, currentLongitude,
                    dest_loc_latitude, dest_loc_longitude);

            holder.tv_event_miles.setText(String.valueOf(distance) + " miles");
        }



        /**Favourite an be made only for Upcoming events not for Past game*/
        if(event_type.equalsIgnoreCase("UPCOMING")) {
            holder.favorite_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String eventId = upcomingPastModelList.get(position).getEvent_Id();
                    boolean favorite = upcomingPastModelList.get(position).isFavorite();
                    AppConstants.OBJECT_TYPE_GAME = upcomingPastModelList.get(position).getCourt_type();

                    if (favorite) {
                        makeListFavorite(userId, eventId, false, upcomingPastModelList, position, holder);
                    } else {
                        makeListFavorite(userId, eventId, true, upcomingPastModelList, position, holder);
                    }
                }
            });
        }
        else{
            holder.favorite_button.setEnabled(false);
            holder.favorite_button.setClickable(false);
        }

        if(!TextUtils.isEmpty(object_Type)
                && ( object_Type.equalsIgnoreCase("event")
                || object_Type.equalsIgnoreCase("Event")))
        {
            holder.tv_minimum_price.setVisibility(View.GONE);
        }
        else{
            if(event_type.equalsIgnoreCase("UPCOMING")) {
                holder.tv_minimum_price.setVisibility(View.VISIBLE);
            }
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.VENUE_IMAGE = UpcomingPastModel.getEvent_Image();
                Constants.VISIBILITY = UpcomingPastModel.getVisibility();
                event_id = UpcomingPastModel.getEvent_Id();

                if(event_type.equalsIgnoreCase("PAST")) {
                    Constants.UPCOMING_PAST_TYPE = "PAST";
                    Constants.UPCOMING_PAST_CLICKED = false;
                } else{
                    Constants.UPCOMING_PAST_TYPE = "UPCOMING";
                    Constants.UPCOMING_PAST_CLICKED = true;
                }
                getUpcomingEventDetailsInfo(event_id, userId);

            }
        });


    }

    /**
     * Function to get Venue courts from server
     */
    public void makeListFavorite(String user_id, String event_id, final boolean isFavorite,
                                 final ArrayList<UpcomingPastModel> eventArray,
                                 final int position, final MyViewHolder holder) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);


        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, event_id, isFavorite);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(context, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    isAddedToFavorite = responseObject.getBoolean("isFavorite");
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(context, false);
                        Constants.FAVOURITE_CLICKED = true;
                        if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            if(isAddedToFavorite){
                                snackBar.setSnackBarMessage("Successfully Added to WishList");
                                eventArray.get(position).setFavorite(true);
                                holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_orange);
                                notifyDataSetChanged();

                                getCountAPI(NavigationDrawerActivity.user_id, NavigationDrawerActivity.auth_token);
                            }else{
                                snackBar.setSnackBarMessage("Successfully Removed from WishList");
                                eventArray.get(position).setFavorite(false);
                                holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_gray);
                                notifyDataSetChanged();

                                getCountAPI(NavigationDrawerActivity.user_id, NavigationDrawerActivity.auth_token);
                            }
                        }else if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                            snackBar.setSnackBarMessage("Failed to Add to WishList");
                        }else{
                            snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE_ERR);
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String userId, String objectId, boolean favoriteStatus) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "add_to_favorite").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("object_id", objectId);

        if(!TextUtils.isEmpty(AppConstants.OBJECT_TYPE_GAME)
                && ( AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")))
        {
            urlBuilder.addQueryParameter("object_type", "event");
        }
        else{
            urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_EVENT);
        }
        if(favoriteStatus){
            urlBuilder.addQueryParameter("is_favorite", Boolean.TRUE.toString());
        }else{
            urlBuilder.addQueryParameter("is_favorite", Boolean.FALSE.toString());
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public RatingBar rating_venue;
        public MaterialFavoriteButton favorite_button;
        public CardView cardView;
        public TextView event_price,rating_event_reviews,
                tv_event_name, tv_eventSkillLevel, tv_eventDatetime,tv_eventspots,
                tv_totalnoparticiapnts, tv_event_creator_name, tv_event_miles,
                tv_minimum_price,upcoming_event_address_listing_name,projected_cost;

        public ImageView event_listing_image,imagv_event_participants;
        public CircleImageView imagv_event_creator_image ;

        public MyViewHolder(View view) {
            super(view);

            rating_venue = (RatingBar) view.findViewById(R.id.rating_event_detail);
            favorite_button = (MaterialFavoriteButton) view.findViewById(R.id.event_favorite_button);
            cardView = (CardView) view.findViewById(R.id.card_view_event_list);
            event_price = (TextView) view.findViewById(R.id.event_price);
            rating_event_reviews = (TextView) view.findViewById(R.id.rating_event_reviews);
            event_listing_image = (ImageView) view.findViewById(R.id.event_listing_image);
            imagv_event_creator_image = (CircleImageView) view.findViewById(R.id.event_creator_image);
            imagv_event_participants = (ImageView)view.findViewById(R.id.event_participants_image);
            tv_event_name = (TextView) view.findViewById(R.id.venue_name);
            tv_eventSkillLevel = (TextView) view.findViewById(R.id.event_sport_name);
            tv_eventDatetime = (TextView) view.findViewById(R.id.event_date_time);
            tv_eventspots = (TextView) view.findViewById(R.id.event_spots);
            tv_totalnoparticiapnts = (TextView) view.findViewById(R.id.event_participants);
            tv_event_creator_name = (TextView) view.findViewById(R.id.event_creator_name);
            tv_event_miles = (TextView) view.findViewById(R.id.event_miles);
            tv_minimum_price = (TextView) view.findViewById(R.id.event_Minimum_Amount_Text);
            upcoming_event_address_listing_name = (TextView) view.findViewById(R.id.upcoming_event_address_listing_name);
            projected_cost = (TextView) view.findViewById(R.id.projected_cost);
        }
    }

    /*********************
     * CLASS FOR CUSTOM FILTER
     *********************************************/
    public class CustomFilter extends Filter {

        private UpcomingPastAdapter mAdapter;

        private CustomFilter(UpcomingPastAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(previouslist);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final UpcomingPastModel list : previouslist) {
                    // if (list.getTaskName().toLowerCase().startsWith(filterPattern)) {
                    if (list.getEvent_Name().toLowerCase().contains(filterPattern)) {
                        filteredList.add(list);
                    }
                }
            }
            upcomingPastModelList = (ArrayList<UpcomingPastModel>) filteredList;
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            notifyDataSetChanged();

            if (((List<UpcomingPastModel>) results.values).size() == 0) {

                snackBar.setSnackBarMessage("No events found");
                notifyDataSetChanged();
            } else {
//                text_contacts.setVisibility(View.GONE);
//                stickyList.setVisibility(View.VISIBLE);
            }
        }

    }
    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(context, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    public void getCurrentLocation(){
        GPSTracker gps = new GPSTracker(context);

        if(gps.canGetLocation()){
            currentLatitude = gps.getLatitude();
            currentLongitude = gps.getLongitude();
        }
    }

    /**
     * Function to get Upcoming detail from server
     */
    public void getUpcomingEventDetailsInfo(String court_Id, String userId) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForDetail(court_Id, userId);

        client.newCall(request).enqueue(new Callback() {

            @Override
            //Toast.makeText(context,e.toString(), Toast.LENGTH_LONG).show();
            public void onFailure(Call call, final IOException e) {

                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LoaderUtility.handleLoader(context, false);
                        Toast.makeText(context,""+e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "",venue_id ="",capacity = "", name = "",
                        latitude = "", longitude = "",review_rating = "",
                        review_count = "",court_type = "",description = "",
                        price = "",address = "",city = "",state = "",
                        country = "", total_number_players = "",
                        game_date = "", startTIme = "", endTime = "",
                        skillLevel = "", no_of_participants = "",
                        eventSportName = "", eventCourtName = "",
                        event_creator_name = "", event_creator_image = "",
                        event_creator_user_id = "", minimum_price = "",
                        actual_price = "",visibility = "",note = "",list_id = "",
                        cancel_policy_type = "",cancellation_point = "",object_type = "";
                boolean isFavorite = false, isEquipmenmtAvailable = false;

                try {
                    //JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    eventListingModelArrayList = new ArrayList<EventListingModel>();
                    eventListingModel = new EventListingModel();
                    ArrayList<String> images_arraylist = new ArrayList<String>();

                    if(jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if(jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if(jsonObject.has("list_id") && !jsonObject.isNull("list_id")) {
                        list_id = jsonObject.getString("list_id");
                    }
                    if(jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                        Constants.EVENT_NAME_MAP = name;
                    }
                    if(jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                        Constants.EVENT_LATITUDE = Double.parseDouble(latitude);
                    }
                    if(jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                        Constants.EVENT_LONGITUDE = Double.parseDouble(longitude);
                    }
                    if(jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }

                    if(jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }

                    if(jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if(jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }
                    if(jsonObject.has("note") && !jsonObject.isNull("note")) {
                        note = jsonObject.getString("note");
                    }
                    if(jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if(jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if(jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }

                    if(jsonObject.has("no_of_player") && !jsonObject.isNull("no_of_player")){
                        total_number_players = jsonObject.getString("no_of_player");
                    }

                    if(jsonObject.has("no_of_partitipants") && !jsonObject.isNull("no_of_partitipants")){
                        no_of_participants = jsonObject.getString("no_of_partitipants");
                    }

                    if(jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")){
                        isFavorite = jsonObject.getBoolean("is_favorite");
                    }
                    if(jsonObject.has("cancel_policy_type")&& !jsonObject.isNull("cancel_policy_type"))
                    {
                        cancel_policy_type = jsonObject.getString("cancel_policy_type");
                    }
                    if(jsonObject.has("cancellation_point")&& !jsonObject.isNull("cancellation_point"))
                    {
                        cancellation_point = jsonObject.getString("cancellation_point");
                    }
                    if (jsonObject.has("object_type") && !jsonObject.isNull("object_type")) {
                        object_type = jsonObject.getString("object_type");
                    }
                    if(jsonObject.has("court_capacity") && !jsonObject.isNull("court_capacity")) {
                        capacity = jsonObject.getString("court_capacity");
                    }

                    if(jsonObject.has("price") && !jsonObject.isNull("price")) {
                        try{
                            price = jsonObject.getString("price");
                            int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                            float price_pre_player = Float.parseFloat(price) / (float)totalPlayer;
                            actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                            float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                            minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                        }catch(Exception ex){
                            ex.printStackTrace();;
                        }
                    }

                    if(jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")){
                        eventSportName = jsonObject.getString("sport_name");
                    }

                    if(jsonObject.has("game_date") && !jsonObject.isNull("game_date")){
                        game_date = jsonObject.getString("game_date");
                    }

                    if(jsonObject.has("skill_level") && !jsonObject.isNull("skill_level")){
                        skillLevel = jsonObject.getString("skill_level");
                    }

                    if(jsonObject.has("list_name") && !jsonObject.isNull("list_name")){
                        eventCourtName = jsonObject.getString("list_name");
                    }

                    if(jsonObject.has("user_name") && !jsonObject.isNull("user_name")){
                        event_creator_name = jsonObject.getString("user_name");
                    }

                    if(jsonObject.has("user_id") && !jsonObject.isNull("user_id")){
                        event_creator_user_id = jsonObject.getString("user_id");
                    }

                    if(jsonObject.has("user_profile_image") && !jsonObject.isNull("user_profile_image")){
                        event_creator_image = jsonObject.getString("user_profile_image");
                    }
                    if(jsonObject.has("visibility") && !jsonObject.isNull("visibility")){
                        visibility = jsonObject.getString("visibility");
                    }

                    if(jsonObject.has("equipments") && !jsonObject.isNull("equipments")){
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");

                        if(equipmentArray.length() > 0){
                            isEquipmenmtAvailable = true;
                        }
                    }

                    if(jsonObject.has("user_review")&& !jsonObject.isNull("user_review")) {
                        JSONObject user_review_object = jsonObject.getJSONObject("user_review");
                        VenueAdapter.user_review_arrayList = new ArrayList<String>();
                        if(user_review_object.length() > 0) {
                            String user_rating = user_review_object.getString("rating");
                            String user_review = user_review_object.getString("review");
                            VenueAdapter.user_review_arrayList.add(user_rating + "@@@@" + user_review);
                        }
                    }

                    if(jsonObject.has("time_slot")&& !jsonObject.isNull("time_slot")) {
                        String start_time = null;
                        JSONArray time_slot_array_json_array = jsonObject.getJSONArray("time_slot");
                        for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                            JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                            if(j == 0){
                                startTIme = getValues.getString("start_time");
                            }
                            endTime = getValues.getString("end_time");
                        }
                    }


                    if(jsonObject.has("images")&& !jsonObject.isNull("images")) {
                        ArrayList<String> images_array = new ArrayList<String>();
                        JSONArray images_json_array = jsonObject.getJSONArray("images");
                        for (int j = 0; j < images_json_array.length(); j++) {
                            images_array.add(images_json_array.getString(j));
                        }
                        eventListingModel.setImages_array(images_array);
                    }


                    if(jsonObject.has("game_partitipants")&& !jsonObject.isNull("game_partitipants")){

                        try{
                            JSONArray particiapntsArray = jsonObject.getJSONArray("game_partitipants");
                            AppConstants.userModelArary = new ArrayList<UserMolel>();
                            AppConstants.userIdArray = new ArrayList<String>();
                            if(particiapntsArray.length() > 0){
                                for(int i = 0; i < particiapntsArray.length(); i++){
                                    UserMolel model = new UserMolel();
                                    JSONObject participantsObj = particiapntsArray.getJSONObject(i);
                                    String userName = participantsObj.getString("user_name");
                                    String userId = participantsObj.getString("user_id");
                                    String user_profile_image = participantsObj.getString("user_profile_image");
                                    model.setUserFullName(userName);
                                    model.setUserId(userId);
                                    model.setUserProfileImage(user_profile_image);
                                    AppConstants.userIdArray.add(userId);
                                    AppConstants.userModelArary.add(model);
                                }
                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }

                    if (jsonObject.has("note") && !jsonObject.isNull("note")) {
                        note = jsonObject.getString("note");
                    }

                    eventListingModel.setEvent_Id(id);
                    eventListingModel.setList_id(list_id);
                    eventListingModel.setEvent_Name(name);
                    eventListingModel.setLatitude(latitude);
                    eventListingModel.setLongitude(longitude);
                    eventListingModel.setEvent_Ratings(review_rating);
                    eventListingModel.setEvent_Reviews(review_count);
                    eventListingModel.setEvent_Price(actual_price);
                    eventListingModel.setCourt_type(court_type);
                    eventListingModel.setDescription(description);
                    eventListingModel.setAddress(address);
                    eventListingModel.setVisibility(visibility);
                    eventListingModel.setNote(note);
                    eventListingModel.setCancel_policy_type(cancel_policy_type);
                    eventListingModel.setCancellation_point(cancellation_point);
                    eventListingModel.setCity(city);
                    eventListingModel.setState(state);
                    eventListingModel.setCountry(country);
                    eventListingModel.setCapacity(capacity);
                    eventListingModel.setEvent_Total_Participants(total_number_players);
                    eventListingModel.setEventDate(game_date);
                    eventListingModel.setEventStartTime(startTIme);
                    eventListingModel.setEventEndTime(endTime);
                    eventListingModel.setSkill_level(skillLevel);
                    eventListingModel.setEvent_accepted_Participants(no_of_participants);
                    eventListingModel.setEvent_Sports_Type(eventSportName);
                    eventListingModel.setEvent_court_name(eventCourtName);
                    eventListingModel.setEvent_Creator_Image(event_creator_image);
                    eventListingModel.setCreator_name(event_creator_name);
                    eventListingModel.setCreator_user_id(event_creator_user_id);
                    eventListingModel.setEvent_Minimum_Price(minimum_price);
                    eventListingModel.setEvent_Price(actual_price);
                    eventListingModel.setFavorite(isFavorite);
                    eventListingModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    eventListingModel.setObject_Type(object_type);
                    eventListingModel.setEvent_Total_Price(price);
                    eventListingModelArrayList.add(eventListingModel);

                }catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(context, false);

                if(isSuccessStatus) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //context.finish();
                            Intent intent = new Intent(context, UpcomingPastDetail.class);
                            context.startActivity(intent);
                        }
                    });
                }
                else
                {
                    snackBar.setSnackBarMessage("Network error! Please try after sometime");
                }
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForDetail(String court_Id, String userId) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_game_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.EVENT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", userId);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }


    /**
     * Fetching Radius,wishlist & Card count
     */
    /**
     * API integration for all counts
     */
    public void getCountAPI(String user_id, String auth_token) {
        //Handling the loader state
        //handleLoader(true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(user_id,auth_token);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                //handleLoader(false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();

                String radius = "0",cards = "0",wish_lists_count = "0";
                try {

                    JSONObject radius_jsonObject = new JSONObject(responseData);
                    JSONObject user_json_object = radius_jsonObject.getJSONObject("user_info");

                    if(user_json_object.has("radius") && !user_json_object.isNull("radius")) {
                        radius = user_json_object.getString("radius");
                    }
                    if(user_json_object.has("cards") && !user_json_object.isNull("cards")) {
                        cards = user_json_object.getString("cards");
                    }
                    if(user_json_object.has("wish_lists_count") && !user_json_object.isNull("wish_lists_count")) {
                        wish_lists_count = user_json_object.getString("wish_lists_count");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalRadius = radius;
                final String finalCards = cards;
                final String finalWish_lists_count = wish_lists_count;
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        //handleLoader(false);
                        if(finalRadius.equalsIgnoreCase("0")) {
                            NavigationDrawerAdapter baseActivityAdapter = new NavigationDrawerAdapter(context, NavigationDrawerActivity.mPlanetTitles, "LOGGED_IN", "10", finalCards, finalWish_lists_count);
                            NavigationDrawerActivity.mDrawerList.setAdapter(baseActivityAdapter);
                        } else{
                            NavigationDrawerAdapter baseActivityAdapter = new NavigationDrawerAdapter(context, NavigationDrawerActivity.mPlanetTitles, "LOGGED_IN", finalRadius, finalCards, finalWish_lists_count);
                            NavigationDrawerActivity.mDrawerList.setAdapter(baseActivityAdapter);
                        }

                        //Store counts for Radius, Billings (Card listing) & Wishlist
                        setCountDatainPreferences(finalRadius, finalCards, finalWish_lists_count);

                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String user_id, String auth_token) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_profile_info").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("user_id", user_id);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Store counts for Radius, Billings (Card listing) & Wishlist
     */
    public void setCountDatainPreferences(String radius_count, String billing_count,
                                          String wishlist_count) {
        if(radius_count.equalsIgnoreCase("0")) {
            NavigationDrawerActivity.profileEditor.putString("RADIUS_COUNT","10");
        } else{
            NavigationDrawerActivity.profileEditor.putString("RADIUS_COUNT",radius_count);
        }
        NavigationDrawerActivity.profileEditor.putString("BILLING_COUNT",billing_count);
        NavigationDrawerActivity.profileEditor.putString("WISHLIST_COUNT",wishlist_count);
        NavigationDrawerActivity.profileEditor.apply();
    }

}
