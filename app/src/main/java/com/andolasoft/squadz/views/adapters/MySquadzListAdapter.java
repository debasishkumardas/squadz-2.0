package com.andolasoft.squadz.views.adapters;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;


import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.AddFriendToChat;
import com.andolasoft.squadz.activities.ChatActivity;
import com.andolasoft.squadz.managers.DatabaseAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MySquadzListAdapter extends BaseAdapter implements Filterable {
	Context mContext;
	LayoutInflater inflater;
	ArrayList<String> squad_items;
	DatabaseAdapter db;
	private ArrayList<String> filteredList;
	private List<String> previouslist;

	CustomFilter filter;/* = new CustomFilter();*/

	public MySquadzListAdapter(Context mContext, ArrayList<String> squadz_items) {
		
		filter = new CustomFilter(MySquadzListAdapter.this);
		this.previouslist = new ArrayList<String>();
		this.mContext = mContext;
		this.squad_items = squadz_items;
		db = new DatabaseAdapter(mContext);
		inflater = LayoutInflater.from(mContext);
		this.previouslist = squad_items;
		filteredList = new ArrayList<String>();
		filteredList.addAll(previouslist);
	}

	@Override
	public int getCount() {
		return squad_items.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;

	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		convertView = null;

		convertView = inflater.inflate(R.layout.my_squadz_items, null);
		TextView full_name_txt = (TextView) convertView
				.findViewById(R.id.textView_name);
		TextView user_name_txt = (TextView) convertView
				.findViewById(R.id.textView_username);

		String[] tempItems = squad_items.get(position).toString().split("###");
		full_name_txt.setText(tempItems[0]);
		user_name_txt.setText(tempItems[1]);
		final String team_name = tempItems[0];
		final String sport_id = tempItems[2];
		final String team_id = tempItems[3];

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				/*ConnectionDetector cd = new ConnectionDetector(mContext);

				if (cd.isConnectingToInternet()) {

					Api_Constants.TEAMMATE_FULLNAME = team_name;

					Api_Constants.RECIPIENT_USER_ID = team_id;

					new GetChatCHannel(Api_Constants.AUTH_TOKEN,
							Api_Constants.LOGGEDIN_USER_ID,
							Api_Constants.RECIPIENT_USER_ID, team_name, "")
							.execute();
				} else {
					Toast.makeText(mContext,
							"Please connect to a working internet connection",
							Toast.LENGTH_LONG).show();
				}*/

				//String userName =
				/*db.insertChatInfoData(Api_Constants.CHAT_CHANNEL_ID,
						logginuser_id, recipient_user_id, username, user_image,
						"group", username, null,"");
				Intent intent = new Intent(mContext, ChatActivity.class);
				intent.putExtra("to_name", username);
				intent.putExtra("type", "group");
				mContext.startActivity(intent);*/

			}
		});

		return convertView;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return filter;
	}

	public class filter_here extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub

			FilterResults Result = new FilterResults();
			// if constraint is empty return the original names
			if (constraint.length() == 0) {
				Result.values = squad_items;
				Result.count = squad_items.size();
				return Result;
			}

			ArrayList<String> Filtered_Names = new ArrayList<String>();
			String filterString = constraint.toString().toLowerCase();
			String filterableString;

			for (int i = 0; i < squad_items.size(); i++) {
				filterableString = squad_items.get(i);
				if (filterableString.toLowerCase().contains(filterString)) {
					Filtered_Names.add(filterableString);
				}
			}
			Result.values = Filtered_Names;
			Result.count = Filtered_Names.size();

			return Result;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// TODO Auto-generated method stub
			squad_items = (ArrayList<String>) results.values;
			// notifyDataSetChanged();
			AddFriendToChat.updateListviewHeight();

		}

	}

	/*private class GetChatCHannel extends AsyncTask<String, String, String> {
		String getChannelid_API = null;
		String status, channel_id;;
		boolean is_avail;
		ProgressDialog dialog;
		String auth_token, logginuser_id, recipient_user_id, username,
				user_image;

		public GetChatCHannel(String loggedinuser_auth_token,
				String loggedinuser_id, String recipient_user_id,
				String username, String user_image) {
			// TODO Auto-generated constructor stub
			this.auth_token = loggedinuser_auth_token;
			this.logginuser_id = loggedinuser_id;
			this.recipient_user_id = recipient_user_id;
			this.username = username;
			this.user_image = user_image;

			getChannelid_API = Api_Constants.baseURL_LIVE + "chats/"
					+ this.auth_token + "/" + this.logginuser_id + "/"
					+ this.recipient_user_id + "/create_channel?type=group";
		}

		@Override
		protected void onPreExecute() {

			customProgessDialog
					.showProgressDialog("Please wait while initializing chat seassion......");

		}

		protected String doInBackground(String... args) {
			System.out.println(getChannelid_API);

			try {
				Jsoncordinator jParser2 = new Jsoncordinator();
				// get JSON data from URL

				JSONObject json2 = jParser2.getJSONFromUrl1(getChannelid_API);

				status = json2.getString("status");
				
				Api_Constants.CHAT_CHANNEL_ID = json2.getString("channel_id");

			} catch (Exception e1) {

				e1.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String name1) {

			customProgessDialog.dismissDialog();
			if (status != null && status.equalsIgnoreCase("success")) {

				db.insertChatInfoData(Api_Constants.CHAT_CHANNEL_ID,
						logginuser_id, recipient_user_id, username, user_image,
						"group", username, null,"");
				Intent intent = new Intent(mContext, ChatActivity.class);
				intent.putExtra("to_name", username);
				intent.putExtra("type", "group");
				mContext.startActivity(intent);
			} else {
				Toast.makeText(mContext, Api_Constants.CONNECTION_TIMEOUT_MSG,
						Toast.LENGTH_SHORT).show();

			}

		}

	}*/
	
	

	public class CustomFilter extends Filter {

		private MySquadzListAdapter mAdapter;

		private CustomFilter(MySquadzListAdapter mAdapter) {
			super();
			this.mAdapter = mAdapter;
		}

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			filteredList.clear();
			// ArrayList<Model_Scheduler> Filtered_Names = new
			// ArrayList<Model_Scheduler>();
			final FilterResults results = new FilterResults();
			if (constraint.length() == 0) {
				filteredList.addAll(previouslist);
			} else {
				final String filterPattern = constraint.toString()
						.toLowerCase().trim();
				//for (final String list : previouslist) {
					
					/*fotr(int i =0; i < previouslist.)
					if (list.get(i).toLowerCase()
							.startsWith(filterPattern)) {
						filteredList.add(list);
					}*/

					/*if (list.getFirstname().toLowerCase()
							.startsWith(filterPattern)) {
						filteredList.add(list);
					}

					if (list.getLastName().toLowerCase()
							.startsWith(filterPattern)) {
						filteredList.add(list);
					}*/

				//}
				
				for(int  i = 0; i < previouslist.size(); i++)
				{
					if(previouslist.get(i).toLowerCase()
							.startsWith(filterPattern))
					{
						filteredList.add(previouslist.get(i));
					}
				}
			}

			//System.out.println("Count Number " + filteredList.size());
			squad_items = filteredList;
			results.values = filteredList;
			results.count = filteredList.size();
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// System.out.println("Count Number 2 " + ((List<Model_Scheduler>)
			// results.values).size());

			// team_items = (ArrayList<Model_Scheduler>) results.values;
			this.mAdapter.notifyDataSetChanged();
			AddFriendToChat.updateListviewHeight();
			// ListFilter.updateListviewHeight();

		}
	}

}
