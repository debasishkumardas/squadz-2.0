package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.TeamPlayerPicker;
import com.andolasoft.squadz.models.AddPlayerModel;
import com.andolasoft.squadz.models.InviteTeammatesModel;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Set;

public class InviteTeammatesAdapter extends
        RecyclerView.Adapter<InviteTeammatesAdapter.MyViewHolder> {

    public static String userId,auth_token;
    SnackBar snackBar;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    private Activity context;
    ArrayList<InviteTeammatesModel> userModelArray;

    public InviteTeammatesAdapter(Activity context, ArrayList<InviteTeammatesModel> userarray) {
        this.context = context;
        snackBar = new SnackBar(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs = context.getSharedPreferences("AddPlayerPrefs", Context.MODE_PRIVATE);
        editor = prefs.edit();
        this.userModelArray = userarray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invite_teammates_list_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return userModelArray.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final String userFirstName = userModelArray.get(position).getFirstName();
        String lastName = userModelArray.get(position).getLastName();

        if(!TextUtils.isEmpty(userFirstName) ||
                !TextUtils.isEmpty(lastName) && !lastName.equalsIgnoreCase("null")){
            holder.tv_userFullname.setText(userFirstName+" "+ lastName);
        }
        else if(!TextUtils.isEmpty(userFirstName) && !userFirstName.equalsIgnoreCase("null"))
        {
            holder.tv_userFullname.setText(userFirstName);
        }
        else{
            holder.tv_userFullname.setText(userModelArray.get(position).getUserName());
        }

        String userProfileImage = userModelArray.get(position).getImage();
        if(!TextUtils.isEmpty(userProfileImage) &&
                !userProfileImage.equalsIgnoreCase("")) {
            if(!userProfileImage.equalsIgnoreCase("null") &&
                    !TextUtils.isEmpty(userProfileImage) &&
                    userProfileImage != null){
                Glide.with(context).load(userProfileImage).into(holder.userProfileImage);
            }
        }

        /**Checkbox click*/
        holder.add_teammates_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Constants.ADD_TEAMMATES_IDS_ARRAY.add(userModelArray.get(position).getUserId());
                    Constants.ADD_TEAMMATES_IMAGES_ARRAY.add(userModelArray.get(position).getImage());
                    Constants.ADD_TEAMMATES_NAMES_ARRAY.add(userModelArray.get(position).getUserName());
                }
                else{
                    Constants.ADD_TEAMMATES_IDS_ARRAY.remove(userModelArray.get(position).getUserId());
                    Constants.ADD_TEAMMATES_IMAGES_ARRAY.remove(userModelArray.get(position).getImage());
                    Constants.ADD_TEAMMATES_NAMES_ARRAY.remove(userModelArray.get(position).getUserName());
                }
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView userProfileImage;
        TextView tv_userFullname;
        CheckBox add_teammates_checkbox;
        public MyViewHolder(View view) {
            super(view);
            userProfileImage = (ImageView) view.findViewById(R.id.profile_image_add_player);
            tv_userFullname = (TextView) view.findViewById(R.id.userFullname_add_player);
            add_teammates_checkbox = (CheckBox) view.findViewById(R.id.add_player_checkbox);
        }
    }
}