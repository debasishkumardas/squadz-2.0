package com.andolasoft.squadz.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.LocationUnregisteredVenueModel;
import com.andolasoft.squadz.models.LocationUnregisteredVenueModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SpNayak on 1/3/2017.
 */

public class LocationUnregisteredVenueAdapter extends ArrayAdapter<LocationUnregisteredVenueModel>{
    private ArrayList<LocationUnregisteredVenueModel> LocationUnregisteredVenueModelArrayList = new ArrayList<LocationUnregisteredVenueModel>();
    private LayoutInflater layoutInflater;
    private Context context;
    boolean is_Unregistred_Venues_Available;
    boolean is_page_Active = true;
    boolean is_page_Active_status = true;

    public LocationUnregisteredVenueAdapter(Context context, ArrayList<LocationUnregisteredVenueModel> LocationUnregisteredVenueModelArrayList) {
        super(context, 0, LocationUnregisteredVenueModelArrayList);
        this.LocationUnregisteredVenueModelArrayList = LocationUnregisteredVenueModelArrayList;
        this.context = context;
        this.is_Unregistred_Venues_Available = is_Unregistred_Venues_Available;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return LocationUnregisteredVenueModelArrayList.size();
    }

   /* @Override
    public Object getItem(int position) {
        return LocationUnregisteredVenueModelArrayList.get(position);
    }*/
   @Override
   public Filter getFilter() {
       return new DogsFilter(this, LocationUnregisteredVenueModelArrayList);
   }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_location_address_layout, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        mViewHolder.linear_registered.setVisibility(View.GONE);
        mViewHolder.linear_unregistered.setVisibility(View.GONE);
        String venue_Id = LocationUnregisteredVenueModelArrayList.get(position).get_Venue_Id();

        /*if(!TextUtils.isEmpty(venue_Id) && position == 0 )
        {
            is_page_Active_status = false;
            View view;
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.registered_venue_layout, null);

            mViewHolder.linear_unregistered.setVisibility(View.GONE);
            mViewHolder.linear_registered.setVisibility(View.VISIBLE);
            mViewHolder.linear_registered.addView(view);
        }
        if(TextUtils.isEmpty(venue_Id) && is_Unregistred_Venues_Available && is_page_Active)
        {
            is_page_Active = false;
            View view;
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.unregistered_venue_layout, null);

            mViewHolder.linear_registered.setVisibility(View.GONE);
            mViewHolder.linear_unregistered.setVisibility(View.VISIBLE);
            mViewHolder.linear_unregistered.addView(view);
        }*/

        mViewHolder.short_address.setText(LocationUnregisteredVenueModelArrayList.get(position).get_Venue_Short_address());
        mViewHolder.full_address.setText(LocationUnregisteredVenueModelArrayList.get(position).get_Venue_Full_address());

        return convertView;
    }

    private class MyViewHolder {
        TextView short_address,
                full_address;
        LinearLayout linear_registered,linear_unregistered;

        public MyViewHolder(View item) {
            short_address = (TextView) item.findViewById(R.id.short_address);
            full_address = (TextView) item.findViewById(R.id.full_address);
            linear_registered =  (LinearLayout) item.findViewById(R.id.linear_registered);
            linear_unregistered = (LinearLayout) item.findViewById(R.id.linear_unregistered);

        }
    }

    class DogsFilter extends Filter {

        LocationUnregisteredVenueAdapter adapter;
        List<LocationUnregisteredVenueModel> originalList;
        List<LocationUnregisteredVenueModel> filteredList;

        public DogsFilter(LocationUnregisteredVenueAdapter adapter, List<LocationUnregisteredVenueModel> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = originalList;
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                // Your filtering logic goes in here
                for (final LocationUnregisteredVenueModel dog : originalList) {
                    if (dog.get_Venue_Short_address().toLowerCase().contains(filterPattern)) {
                        filteredList.add(dog);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            try {
                adapter.LocationUnregisteredVenueModelArrayList.clear();
                adapter.LocationUnregisteredVenueModelArrayList.addAll((List) results.values);
                adapter.notifyDataSetChanged();
            }catch (Exception exp)
            {
                exp.printStackTrace();
            }
        }
    }
}

