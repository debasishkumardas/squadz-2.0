package com.andolasoft.squadz.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.ChatModel;
import com.bumptech.glide.Glide;

import java.util.List;

public class MessagesListAdapter extends BaseAdapter {

	private Context context;
	private List<ChatModel> messagesItems;
	int shownPosition = -1, previousPosition = -1;
    SharedPreferences pref;
    String loggedinuserId = null;
    boolean isSelf;

	public MessagesListAdapter(Context context, List<ChatModel> navDrawerItems) {
		this.context = context;
		this.messagesItems = navDrawerItems;
        pref = pref = PreferenceManager.getDefaultSharedPreferences(context);
        loggedinuserId = pref.getString("loginuser_id", null);
	}

	@Override
	public int getCount() {
		return messagesItems.size();
	}

	@Override
	public Object getItem(int position) {
		return messagesItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        LayoutInflater  mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        /**
         * The following list not implemented reusable list items as list items
         * are showing incorrect data Add the solution if you have one
         * */

        final ChatModel m = messagesItems.get(position);

		/*LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);*/
        //convertView = null;

        boolean isSelf = m.isSelf();
        //String senderId = m.getSenderId();

        /*if(senderId.equalsIgnoreCase(loggedinuserId)){
            isSelf = true;
        }
*/
        // Identifying the message owner
        if (isSelf) {
            // message belongs to you, so load the right aligned layout
            convertView = mInflater.inflate(R.layout.list_item_message_right,
                    null);
            ImageView senderImage = (ImageView) convertView
                    .findViewById(R.id.user_image_change);
            String image_url_sender = m.getSenderImageUrl();

            if(!TextUtils.isEmpty(image_url_sender.trim()) &&
					image_url_sender != null &&
					!image_url_sender.equalsIgnoreCase("null")){
                Glide.with(context).load(image_url_sender).into(senderImage);
            }else{
                senderImage.setBackgroundResource(R.mipmap.ic_account_gray);
            }

        } else {
            // message belongs to other person, load the left aligned layout
            convertView = mInflater.inflate(R.layout.list_item_message_left,
                    null);
            ImageView lblFrom = (ImageView) convertView
                    .findViewById(R.id.user_image);
            String image_url = m.getImageUrl();

            if(!TextUtils.isEmpty(image_url.trim()) &&
                    image_url != null &&
                    !image_url.equalsIgnoreCase("null")){
                Glide.with(context).load(image_url).into(lblFrom);
            }else{
                lblFrom.setBackgroundResource(R.mipmap.ic_account_gray);
            }

        }

		/*if (convertView == null) {
            holder = new ViewHolder();
			holder.lblFrom = (ImageView) convertView
					.findViewById(R.id.user_image);
			holder.txtMsg = (TextView) convertView.findViewById(R.id.txtMsg);

			holder.time = (TextView) convertView
					.findViewById(R.id.txt_time);

			holder.time.setVisibility(View.GONE);

			holder.textLetter = (TextView) convertView
					.findViewById(R.id.textLetter);

			holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);
			convertView.setTag(holder);
		}

        convertView = null;*/

		TextView txtMsg = (TextView) convertView.findViewById(R.id.txtMsg);

		final TextView time = (TextView) convertView
				.findViewById(R.id.txt_time);

		//time.setVisibility(View.GONE);

		TextView textLetter = (TextView) convertView
				.findViewById(R.id.textLetter);

		TextView txt_name = (TextView) convertView.findViewById(R.id.txt_name);
		txt_name.setVisibility(View.GONE);





		/*if (isSelf) {
            txt_name.setVisibility(View.GONE);
            lblFrom.setVisibility(View.GONE);
            textLetter.setVisibility(View.GONE);
		}*/

        time.setText(m.getMsgTimeStamp());

        txtMsg.setText(m.getText());
        txt_name.setText(m.getChatUserName());


		//boolean timeShownStatus = messagesItems.get(position).isTimeShown();



		/*
		 * if (time.getVisibility() == View.VISIBLE) { // Its visible
		 * time.setVisibility(View.VISIBLE); } else { // Either gone or
		 * invisible time.setVisibility(View.GONE); }
		 */

		/*convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (shownPosition != -1) {
					previousPosition = shownPosition;
				}
				// previousPosition = shownPosition;

				shownPosition = position;

				refreshListView(shownPosition, previousPosition);
				
				*//**
				 * Scroll the list view to bottom when user click on the lict item of the list view
				 *//*
				if(position == messagesItems.size() - 1){
					ChatActivity.scrollMyListViewToBottom();
				}

				*//**
				 * Getting the visibility of the time text view and setting the
				 * view accrodingly
				 *//*
				*//*
				 * if (time.getVisibility() == View.VISIBLE) { // Its visible
				 * time.setVisibility(View.GONE); } else { // Either gone or
				 * invisible // time.setVisibility(View.GONE);
				 * 
				 * time.setVisibility(View.VISIBLE); time.setText(m.getDate() +
				 * ", " + m.getTime()); }
				 *//*

			}
		});*/

		return convertView;
	}

	final class ViewHolder {
		ImageView lblFrom;
		TextView txtMsg;
		TextView time;
		TextView textLetter;
		TextView txt_name;
	}



	private String getFirstLetterOfUsername(String username) {

        // String test = "StackOverflow";

        String cropedString = null;

        if (!TextUtils.isEmpty(username)) {
            char first = username.charAt(0);

            cropedString = Character.toString(first);
        }

        return cropedString.toUpperCase();

    }

/*	private String getFirstLetters(String name) {

		String newText[] = name.split("(?<=[\\S])[\\S]*\\s*");
		// 3. Using StringBuffer.append method
		StringBuffer sbf = new StringBuffer();

		if (newText.length > 0) {

			sbf.append(newText[0]);
			for (int i = 1; i < newText.length; i++) {
				sbf.append(newText[i]);
			}

		}

		return sbf.toString().toUpperCase();
	}

	private String getFirstLetterOfUsername(String username) {

		// String test = "StackOverflow";

		String cropedString = null;

		if (!TextUtils.isEmpty(username)) {
			char first = username.charAt(0);

			cropedString = Character.toString(first);
		}

		return cropedString.toUpperCase();

	}

	public void refreshListView(int position, int prev_position) {

		*//*
		 * // boolean displayStatus = messagesItems.get(position).isTimeShown();
		 * 
		 * if (position == prev_position) {
		 * messagesItems.get(position).setTimeShown(false); } else
		 * 
		 * 
		 * if (messagesItems.get(position).isTimeShown()) {
		 * messagesItems.get(position).setTimeShown(false); } else {
		 * messagesItems.get(position).setTimeShown(true); }
		 * 
		 * if (messagesItems.get(prev_position).isTimeShown()) {
		 * messagesItems.get(prev_position).setTimeShown(false); } else {
		 * messagesItems.get(prev_position).setTimeShown(true); }
		 * 
		 * 
		 * //}
		 * 
		 * 
		 * if (displayStatus) { messagesItems.get(position).setTimeShown(false);
		 * } else { messagesItems.get(position).setTimeShown(true); }
		 *//*

		if (position == prev_position) {

			if (messagesItems.get(position).isTimeShown()) {
				messagesItems.get(position).setTimeShown(false);
			} else {
				messagesItems.get(position).setTimeShown(true);
			}
		}

		else {
			
			if(prev_position != -1)
			{
				if(messagesItems.get(prev_position).isTimeShown()){
					messagesItems.get(prev_position).setTimeShown(false);
				}
			}

			
			messagesItems.get(position).setTimeShown(true);

		}

		notifyDataSetChanged();

	}*/
}
