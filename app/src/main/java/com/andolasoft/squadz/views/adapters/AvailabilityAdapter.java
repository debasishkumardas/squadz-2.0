package com.andolasoft.squadz.views.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.AvailabilityTimeSlotsModel;
import com.andolasoft.squadz.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by SpNayak on 1/3/2017.
 */

public class AvailabilityAdapter extends BaseAdapter {
    private ArrayList<AvailabilityTimeSlotsModel> venuesNameList = new ArrayList<AvailabilityTimeSlotsModel>();
    private LayoutInflater layoutInflater;
    private Context context;
    int selectedPosition = 0;
    boolean ischecked = false;
    ArrayList<String> start_time_array = new ArrayList<>();
    ArrayList<String> end_time_array = new ArrayList<>();
    ArrayList<String> new_time_array;
    ArrayList<String> time_array;
    boolean is_FirstTime = true;

    public AvailabilityAdapter(Context context, ArrayList<AvailabilityTimeSlotsModel> venuesNameList) {
        this.venuesNameList = venuesNameList;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
        new_time_array = new ArrayList<>();
        time_array = new ArrayList<>();
        Constants.AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return venuesNameList.size();
    }

    @Override
    public Object getItem(int position) {
        return venuesNameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_availability_layout, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

//        final int time_position = venuesNameList.get(position).getTime_position();
//
//        //Checking if time slots are available or not
//        boolean is_available_time = venuesNameList.get(position).is_available();
//        if(!is_available_time)
//        {
//            mViewHolder.availability_Time.setText(venuesNameList.get(position).getStart_end_time());
//            mViewHolder.availability_Time.setPaintFlags(mViewHolder.availability_Time.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//            mViewHolder.avail_timeslot_checkbox.setVisibility(View.GONE);
//        }
//        else{
//            mViewHolder.availability_Time.setText(venuesNameList.get(position).getStart_end_time());
//            mViewHolder.avail_timeslot_checkbox.setVisibility(View.VISIBLE);
//        }

        mViewHolder.availability_Time.setText(venuesNameList.get(position).getStart_end_time());
        mViewHolder.avail_timeslot_checkbox.setVisibility(View.VISIBLE);

        mViewHolder.avail_timeslot_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String item = "";
                boolean strike_out_previous_timing = true,strike_out_after_timing = true;
                if(new_time_array.size() > 0) {
                    item = new_time_array.get(new_time_array.size() - 1);
                }

                if(position + 1 != venuesNameList.size()) {
                    if (position != 0 && position <= venuesNameList.size()) {

                        String previuos_time = venuesNameList.get(position - 1).getStart_end_time();
                        String next_time = venuesNameList.get(position + 1).getStart_end_time();

                        strike_out_previous_timing = venuesNameList.get(position - 1).is_available();
                        strike_out_after_timing = venuesNameList.get(position + 1).is_available();
                    } else if (position == 0) {
                        strike_out_after_timing = venuesNameList.get(position + 1).is_available();
                    }
                }
//                else if(position == (venuesNameList.size() - 1))
//                {
//                    strike_out_previous_timing = venuesNameList.get(position - 1).is_available();
//                }
                if(isChecked)
                {
                    if(is_FirstTime)
                    {
                        is_FirstTime = false;
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                        mViewHolder.avail_timeslot_checkbox.setChecked(true);
                        new_time_array.add(String.valueOf(position));

                        time_array.add(venuesNameList.get(position).getStart_end_time());
                    }

                    ///////For Strike out
                    else if( position == ((Integer.valueOf(item)) + 1) && !strike_out_previous_timing && !strike_out_after_timing )
                    {
                        mViewHolder.avail_timeslot_checkbox.setChecked(true);
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                        new_time_array.add(String.valueOf(position));

                        time_array.add(venuesNameList.get(position).getStart_end_time());
                    }
                    else if( position == ((Integer.valueOf(item)) - 1) && !strike_out_previous_timing && !strike_out_after_timing )
                    {
                        mViewHolder.avail_timeslot_checkbox.setChecked(true);
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                        new_time_array.add(String.valueOf(position));

                        time_array.add(venuesNameList.get(position).getStart_end_time());
                    }

                    else if( position == (Integer.valueOf(new_time_array.get(0)) + 1) && strike_out_previous_timing && strike_out_after_timing )
                    {
                        mViewHolder.avail_timeslot_checkbox.setChecked(true);
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                        new_time_array.add(String.valueOf(position));

                        time_array.add(venuesNameList.get(position).getStart_end_time());
                    }

                    else if( position == (Integer.valueOf(new_time_array.get(0)) - 1) && strike_out_previous_timing && strike_out_after_timing )
                    {
                        mViewHolder.avail_timeslot_checkbox.setChecked(true);
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                        new_time_array.add(String.valueOf(position));

                        time_array.add(venuesNameList.get(position).getStart_end_time());
                    }
                    ////////////For Strike out

                    else if( position == (Integer.valueOf(item)) + 1)
                    {
                        mViewHolder.avail_timeslot_checkbox.setChecked(true);
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                        new_time_array.add(String.valueOf(position));

                        time_array.add(venuesNameList.get(position).getStart_end_time());
                    }
                    else if( position == (Integer.valueOf(item)) - 1)
                    {
                        mViewHolder.avail_timeslot_checkbox.setChecked(true);
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                        new_time_array.add(String.valueOf(position));

                        time_array.add(venuesNameList.get(position).getStart_end_time());
                    }
                    else{
                        mViewHolder.avail_timeslot_checkbox.setChecked(false);
                    }
                }
                else {

                    if(position + 1 != venuesNameList.size()) {
                        if (position != 0 && position <= venuesNameList.size()) {
                            String get_Time = venuesNameList.get(position).getStart_end_time();

                            String previuos_time = venuesNameList.get(position - 1).getStart_end_time();
                            String next_time = venuesNameList.get(position + 1).getStart_end_time();

                            if (time_array.contains(previuos_time) && time_array.contains(next_time)) {
                                mViewHolder.avail_timeslot_checkbox.setChecked(true);
                            } else {
                                mViewHolder.avail_timeslot_checkbox.setChecked(false);
                                new_time_array.remove(String.valueOf(position));
                                time_array.remove(venuesNameList.get(position).getStart_end_time());
                                Constants.AVAILABILITY_TIME_SLOT_ARRAY.remove(venuesNameList.get(position).getStart_end_time());
                                if (new_time_array.size() == 0) {
                                    new_time_array = new ArrayList<String>();
                                    is_FirstTime = true;
                                    time_array = new ArrayList<String>();
                                }
                                if (time_array.size() == 0) {
                                    new_time_array = new ArrayList<String>();
                                    is_FirstTime = true;
                                    time_array = new ArrayList<String>();
                                }
                            }
                        }
                        else if(position == 0)
                        {
                            mViewHolder.avail_timeslot_checkbox.setChecked(false);
                            new_time_array.remove(String.valueOf(position));
                            time_array.remove(venuesNameList.get(position).getStart_end_time());
                            Constants.AVAILABILITY_TIME_SLOT_ARRAY.remove(venuesNameList.get(position).getStart_end_time());
                            if (new_time_array.size() == 0) {
                                new_time_array = new ArrayList<String>();
                                is_FirstTime = true;
                                time_array = new ArrayList<String>();
                            }
                            if (time_array.size() == 0) {
                                new_time_array = new ArrayList<String>();
                                is_FirstTime = true;
                                time_array = new ArrayList<String>();
                            }
                        }
                    }
                    else {
                        mViewHolder.avail_timeslot_checkbox.setChecked(false);
                        new_time_array.remove(String.valueOf(position));
                        time_array.remove(venuesNameList.get(position).getStart_end_time());
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.remove(venuesNameList.get(position).getStart_end_time());
                        if (new_time_array.size() == 0) {
                            new_time_array = new ArrayList<String>();
                            is_FirstTime = true;
                            time_array = new ArrayList<String>();
                        }
                        if (time_array.size() == 0) {
                            new_time_array = new ArrayList<String>();
                            is_FirstTime = true;
                            time_array = new ArrayList<String>();
                        }
                    }
                }
            }
        });

        /*mViewHolder.avail_timeslot_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int next_position = position;

                if(isChecked)
                {
                    Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                }
                else{
                    Constants.AVAILABILITY_TIME_SLOT_ARRAY.remove(venuesNameList.get(position).getStart_end_time());
                }
            }
        });*/



        /*mViewHolder.avail_timeslot_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                String getTimeSlots = mViewHolder.availability_Time.getText().toString();

                String[] str_timeslots = getTimeSlots.split(" - ");
                String start_time = str_timeslots[0];
                String end_time = str_timeslots[1];

               if(isChecked)
                {
                    if(new_time_array.contains(start_time))
                    {
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                        mViewHolder.avail_timeslot_checkbox.setChecked(true);
                        ischecked = true;
                        new_time_array.add(start_time);
                    }
                    else if(!ischecked)
                    {
                        new_time_array.add(end_time);
                        Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(venuesNameList.get(position).getStart_end_time());
                        mViewHolder.avail_timeslot_checkbox.setChecked(true);
                        ischecked = true;
                    }
                    else{

                        mViewHolder.avail_timeslot_checkbox.setChecked(false);
                        Toast.makeText(context, "Please select consecutive time slots", Toast.LENGTH_SHORT).show();
                    }

                }
                else{
                   ischecked = false;
                   Constants.AVAILABILITY_TIME_SLOT_ARRAY.remove(venuesNameList.get(position).getStart_end_time());
                }
            }
        });*/

        /*RadioButton r = (RadioButton)convertView.findViewById(R.id.avail_radio_button);
        if(ischecked) {
            r.setChecked(position == selectedPosition);
            if(position == selectedPosition) {
                mViewHolder.availability_Time.setTextColor(context.getResources().getColor(R.color.orange));
            }
            else{
                mViewHolder.availability_Time.setTextColor(context.getResources().getColor(R.color.gray_new));
            }
        }
        else{
            mViewHolder.availability_Time.setTextColor(context.getResources().getColor(R.color.gray_new));
        }
        r.setTag(position);
        r.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                ischecked = true;
                selectedPosition = (Integer)view.getTag();
                //Toast.makeText(context, ""+venuesNameList.get(position), Toast.LENGTH_SHORT).show();
                Constants.AVAILABILITY_TIME_SLOT = venuesNameList.get(position);
                notifyDataSetChanged();
            }
        });*/

        return convertView;
    }

    private class MyViewHolder {
        TextView availability_Time;
        //RadioButton avail_radio_button;
        CheckBox avail_timeslot_checkbox;

        public MyViewHolder(View item) {
            availability_Time = (TextView) item.findViewById(R.id.availability_Time);
           // avail_radio_button = (RadioButton) item.findViewById(R.id.avail_radio_button);
            avail_timeslot_checkbox = (CheckBox) item.findViewById(R.id.avail_timeslot_checkbox);

        }
    }
}

