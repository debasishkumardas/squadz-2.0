package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.EventDetail;
import com.andolasoft.squadz.activities.Sports;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.models.InviteTeammatesModel;
import com.andolasoft.squadz.models.NotificationModel;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NotificationAdapter extends
        RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    public static String userId;
    SnackBar snackBar;
    private Activity context;
    ArrayList<NotificationModel> notificationModelArrayList;
    ProgressDialog dialog;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String auth_token = "", user_id, type = "";
    public EventListingModel eventListingModel;
    public static ArrayList<EventListingModel> eventListingModelArrayList;
    DatabaseAdapter db;

    public NotificationAdapter(Activity context, ArrayList<NotificationModel> notificationModelArrayList) {
        this.context = context;
        snackBar = new SnackBar(context);
        this.notificationModelArrayList = notificationModelArrayList;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        editor = prefs.edit();
        auth_token = prefs.getString("auth_token","");
        user_id = prefs.getString("loginuser_id","");
        db = new DatabaseAdapter(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_notificatin_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return notificationModelArrayList.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final String notificationType = notificationModelArrayList.get(position).getNotification_type();

        holder.create_game_layout_cardview.setVisibility(View.GONE);
        holder.joined_game_layout_cardview.setVisibility(View.GONE);
        holder.invited_game_layout_cardview.setVisibility(View.GONE);
        holder.friend_invitation_layout_cardview.setVisibility(View.GONE);
        holder.friend_invitation_accepted_layout_cardview.setVisibility(View.GONE);
        holder.payment_layout_cardview.setVisibility(View.GONE);

        if(notificationType.equalsIgnoreCase("new_game")) {
            holder.create_game_layout_cardview.setVisibility(View.VISIBLE);
            holder.new_game_name.setText("Upcoming "+notificationModelArrayList.get(position).getGame_name()+" Game Nearby!");
            holder.notification_game_creator_name.setText(notificationModelArrayList.get(position).getUser_name());

            String game_date = notificationModelArrayList.get(position).getGame_Date();
            String date = ApplicationUtility.dateTimeFormatForNearByGameNotification(notificationModelArrayList.get(position).getGame_Date());
            String game_time = notificationModelArrayList.get(position).getGame_time();
            String court_name = notificationModelArrayList.get(position).getCourt_name();

            /**Comaparing date to display today & tomorrow*/
            double current_date_mili = ApplicationUtility.getCurrentDateInMili();
            double notofication_date_mili = ApplicationUtility.getNotificationDateInMili(game_date);
            if(notofication_date_mili==current_date_mili) {
                holder.new_game_description.setText("Today"+","+game_time + " at "+court_name);
            } else if(game_date.equalsIgnoreCase(ApplicationUtility.getNextDayDateFromCurrentDate())) {
                holder.new_game_description.setText("Tomorrow"+","+game_time + " at "+court_name);
            } else{
                holder.new_game_description.setText(date+","+game_time + " at "+court_name);
            }

            String time = ApplicationUtility.getTimeAgo(ApplicationUtility.UTCTimeFormat(notificationModelArrayList.get(position).getCreated_at()));
            holder.new_game_name_time_ago.setText(time);

            holder.new_game_type.setText(notificationModelArrayList.get(position).getVisibility());
            holder.new_game_total_participants.setText(notificationModelArrayList.get(position).getPartitipants() +" participants");

            String image = notificationModelArrayList.get(position).getUser_profile_image();
            if(!TextUtils.isEmpty(image)
                    && !image.equalsIgnoreCase("null")
                    && !image.equalsIgnoreCase(" ")
                    && image != null) {
                Glide.with(context).load(image).into(holder.notification_game_creator_image);
            }

            String game_type = notificationModelArrayList.get(position).getVisibility();
            if(!TextUtils.isEmpty(game_type) && game_type.equalsIgnoreCase("Public")) {
                holder.new_game_type_icon.setImageResource(R.mipmap.ic_public_gray);
            } else if(!TextUtils.isEmpty(game_type) && game_type.equalsIgnoreCase("Public")) {
                holder.new_game_type_icon.setImageResource(R.mipmap.ic_private_gray);
            } else{
                holder.new_game_type_icon.setImageResource(R.mipmap.ic_public_gray);
            }

            String sport = notificationModelArrayList.get(position).getGame_name();
            holder.new_game_sport_icon.setImageResource(SportsImagePicker.getOrangeSportImage(sport));

            /**
             * Game Created card view layout click
             */
            holder.create_game_layout_cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String game_Id = notificationModelArrayList.get(position).getGame_id();

                    /** Get game details API */
                    getVenuesCourtsDetailsInfor(game_Id, user_id);
                }
            });
        }
        if(notificationType.equalsIgnoreCase("joined")) {
            holder.joined_game_layout_cardview.setVisibility(View.VISIBLE);
            String sport_Name = notificationModelArrayList.get(position).getGame_name();//Get name is Sport name
            holder.join_user_name.setText(notificationModelArrayList.get(position).getUser_name() +" has joined "+ sport_Name);

            String time = ApplicationUtility.getTimeAgo(ApplicationUtility.UTCTimeFormat(notificationModelArrayList.get(position).getCreated_at()));
            holder.join_game_time_ago.setText(time);

            String image = notificationModelArrayList.get(position).getUser_profile_image();
            if(!TextUtils.isEmpty(image)
                    && !image.equalsIgnoreCase("null")
                    && !image.equalsIgnoreCase(" ")
                    && image != null) {
                Glide.with(context).load(image).into(holder.join_game_creator_image);
            }

            String game_Name = notificationModelArrayList.get(position).getGame_final_name();
            String no_of_participants = notificationModelArrayList.get(position).getPartitipants();
            String price_per_player = notificationModelArrayList.get(position).getPer_player_price();
            String object_type = notificationModelArrayList.get(position).getObject_type();
            boolean is_registered_event = notificationModelArrayList.get(position).is_registered_event();

            if(is_registered_event && (object_type.equalsIgnoreCase("Game")
                    || object_type.equalsIgnoreCase("game"))) {
                holder.join_game_description.setText(game_Name+" has "
                        +no_of_participants
                        + " participants and now costs $"
                        +ApplicationUtility.twoDigitAfterDecimal(Double.valueOf(price_per_player))+"/player");
            } else{

                holder.join_game_description.setText(game_Name+" has "
                        +no_of_participants
                        + " participants");
            }
            /**
             * Game Joined card view layout click
             */
            holder.joined_game_layout_cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String game_Id = notificationModelArrayList.get(position).getGame_id();

                    /** Get game details API */
                    getVenuesCourtsDetailsInfor(game_Id, user_id);
                }
            });
        }
        if(notificationType.equalsIgnoreCase("invited")) {
            holder.invited_game_layout_cardview.setVisibility(View.VISIBLE);
            //holder.invited_game_name.setText(notificationModelArrayList.get(position).getGame_name());
            String sport_name = notificationModelArrayList.get(position).getGame_name();
            holder.invited_sender_descripetion.setText(notificationModelArrayList.get(position).getUser_name()+" has invited you to play "+sport_name);

            String time = ApplicationUtility.getTimeAgo(ApplicationUtility.UTCTimeFormat(notificationModelArrayList.get(position).getCreated_at()));
            holder.invited_game_name_time_ago.setText(time);
            String image = notificationModelArrayList.get(position).getUser_profile_image();
            if(!TextUtils.isEmpty(image)
                && !image.equalsIgnoreCase("null")
                && !image.equalsIgnoreCase(" ")
                && image != null) {
                Glide.with(context).load(image).into(holder.invited_user_image);
            }

            String sport = notificationModelArrayList.get(position).getGame_name();
            holder.invited_game_sport_icon.setImageResource(SportsImagePicker.getOrangeSportImage(sport));
            /**
             * Game Invited card view layout click
             */
            holder.invited_game_layout_cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String game_Id = notificationModelArrayList.get(position).getGame_id();

                    /** Get game details API */
                    getVenuesCourtsDetailsInfor(game_Id, user_id);
                }
            });
        }

        if(notificationType.equalsIgnoreCase("friend_request_invited")) {
            holder.friend_invitation_layout_cardview.setVisibility(View.VISIBLE);
            holder.friend_invitation_descripetion.setText(notificationModelArrayList.get(position).getUser_name() +" wants to be your teammate.");

            String image = notificationModelArrayList.get(position).getUser_profile_image();
            if(!TextUtils.isEmpty(image)
                    && !image.equalsIgnoreCase("null")
                    && !image.equalsIgnoreCase(" ")
                    && image != null) {
                Glide.with(context).load(image).into(holder.friend_invitation_user_image);
            }

            /**Accept button click*/
            holder.friend_invitation_accept_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    type = "Accept";
                    int item_position = position;
                    String userId = notificationModelArrayList.get(position).getUser_id();
                    invitationAcceptAPI(auth_token, userId,type,item_position);
                }
            });

            /**Reject button click*/
            holder.friend_invitation_reject_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    type = "Reject";
                    int item_position = position;
                    String userId = notificationModelArrayList.get(position).getUser_id();
                    invitationAcceptAPI(auth_token, userId,type,item_position);
                }
            });

        }

        if(notificationType.equalsIgnoreCase("BecameFriends")) {
            holder.friend_invitation_accepted_layout_cardview.setVisibility(View.VISIBLE);
            String accepted_user_name = notificationModelArrayList.get(position).getUser_name();
            holder.friend_invitation_accpted_descripetion.setText("You and "+accepted_user_name +" are now teammates!");

            String image = notificationModelArrayList.get(position).getUser_profile_image();
            if(!TextUtils.isEmpty(image)
                    && !image.equalsIgnoreCase("null")
                    && !image.equalsIgnoreCase(" ")
                    && image != null) {
                Glide.with(context).load(image).into(holder.friend_invitation_accepted_user_image);
            }
            String time = ApplicationUtility.getTimeAgo(ApplicationUtility.UTCTimeFormat(notificationModelArrayList.get(position).getCreated_at()));
            holder.friend_invitation_accepted_time_ago.setText(time);
        }


       /* if(notificationType.equalsIgnoreCase("game_transaction"))
        {
            holder.payment_layout_cardview.setVisibility(View.VISIBLE);
            holder.payment_user_name.setText("Yo received payment from "+notificationModelArrayList.get(position).getUser_name());
            String image = notificationModelArrayList.get(position).getUser_profile_image();
            if(!TextUtils.isEmpty(image)
                    && !image.equalsIgnoreCase("null")
                    && !image.equalsIgnoreCase(" ")
                    && image != null)
            {
                Glide.with(context).load(image).into(holder.payment_creator_image);
            }
        }*/

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView create_game_layout_cardview,
                joined_game_layout_cardview,
                invited_game_layout_cardview,
                payment_layout_cardview,
                friend_invitation_layout_cardview,
                friend_invitation_accepted_layout_cardview;

        /**Create game Reference*/
        TextView new_game_name,notification_game_creator_name,
                new_game_type,new_game_total_participants,
                new_game_name_time_ago;
        CircleImageView notification_game_creator_image;
        ImageView new_game_type_icon,new_game_sport_icon;


        /**Join game Reference*/
        TextView join_user_name,new_game_description,
                join_game_time_ago,join_game_description;
        CircleImageView join_game_creator_image;

        /**Invite game Reference*/
        TextView invited_game_name,invited_sender_descripetion,
                invited_game_name_time_ago;
        CircleImageView invited_user_image;
        ImageView accept_icon,reject_icon,invited_game_sport_icon;

        /**Friend request Reference*/
        TextView invited_friend_username,
                friend_invitation_descripetion,
                friend_invitation_time_ago;
        CircleImageView friend_invitation_user_image;
        ImageView friend_invitation_accept_icon,
                friend_invitation_reject_icon;

        /**Friend request accept Reference*/
        ImageView friend_invitation_accepted_user_image;
        TextView friend_invitation_accpted_descripetion,
                friend_invitation_accepted_time_ago;

        /**Payment game Reference*/
        TextView payment_user_name;
        CircleImageView payment_creator_image;

        public MyViewHolder(View view) {
            super(view);
            create_game_layout_cardview = (CardView) view.findViewById(R.id.create_game_layout_cardview);
            joined_game_layout_cardview = (CardView) view.findViewById(R.id.joined_game_layout_cardview);
            invited_game_layout_cardview = (CardView) view.findViewById(R.id.invited_game_layout_cardview);
            friend_invitation_layout_cardview = (CardView) view.findViewById(R.id.friend_invitation_layout_cardview);
            friend_invitation_accepted_layout_cardview = (CardView) view.findViewById(R.id.friend_invitation_accepted_layout_cardview);
            payment_layout_cardview = (CardView) view.findViewById(R.id.payment_layout_cardview);

            /**Create game Reference*/
            new_game_name = (TextView) view.findViewById(R.id.new_game_name);
            notification_game_creator_name = (TextView) view.findViewById(R.id.notification_game_creator_name);
            new_game_type = (TextView) view.findViewById(R.id.new_game_type);
            new_game_total_participants = (TextView) view.findViewById(R.id.new_game_total_participants);
            new_game_description = (TextView) view.findViewById(R.id.new_game_description);
            notification_game_creator_image = (CircleImageView) view.findViewById(R.id.notification_game_creator_image);
            new_game_name_time_ago = (TextView) view.findViewById(R.id.new_game_name_time_ago);
            new_game_type_icon = (ImageView) view.findViewById(R.id.new_game_type_icon);
            new_game_sport_icon = (ImageView) view.findViewById(R.id.new_game_sport_icon);

            /**Join game Reference*/
            join_user_name = (TextView) view.findViewById(R.id.join_user_name);
            join_game_time_ago = (TextView) view.findViewById(R.id.join_game_time_ago);
            join_game_description = (TextView) view.findViewById(R.id.join_game_description);
            join_game_creator_image = (CircleImageView) view.findViewById(R.id.join_game_creator_image);

            /**Invite game Reference*/
            invited_game_name = (TextView) view.findViewById(R.id.invited_game_name);
            invited_sender_descripetion = (TextView) view.findViewById(R.id.invited_sender_descripetion);
            invited_game_name_time_ago = (TextView) view.findViewById(R.id.invited_game_name_time_ago);
            invited_user_image = (CircleImageView) view.findViewById(R.id.invited_user_image);
            accept_icon = (ImageView) view.findViewById(R.id.accept_icon);
            reject_icon = (ImageView) view.findViewById(R.id.reject_icon);
            invited_game_sport_icon = (ImageView) view.findViewById(R.id.invited_game_sport_icon);

            /**Friend request Reference*/
            invited_friend_username = (TextView) view.findViewById(R.id.invited_friend_username);
            friend_invitation_descripetion = (TextView) view.findViewById(R.id.friend_invitation_descripetion);
            friend_invitation_time_ago = (TextView) view.findViewById(R.id.friend_invitation_time_ago);
            friend_invitation_user_image = (CircleImageView) view.findViewById(R.id.friend_invitation_user_image);
            friend_invitation_accept_icon = (ImageView) view.findViewById(R.id.friend_invitation_accept_icon);
            friend_invitation_reject_icon = (ImageView) view.findViewById(R.id.friend_invitation_reject_icon);

            /**Friend request accept Reference*/
            friend_invitation_accepted_user_image = (ImageView) view.findViewById(R.id.friend_invitation_accepted_user_image);
            friend_invitation_accpted_descripetion = (TextView) view.findViewById(R.id.friend_invitation_accpted_descripetion);
            friend_invitation_accepted_time_ago =  (TextView) view.findViewById(R.id.friend_invitation_accepted_time_ago);

            /**Payement game Reference*/
            payment_user_name = (TextView) view.findViewById(R.id.payment_user_name);
            payment_creator_image = (CircleImageView) view.findViewById(R.id.payment_creator_image);
        }
    }

    /**
     * Invited Accept API
     */
    public void invitationAcceptAPI(String auth_token, String friend_id, String type, final int item_position) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForAddPlayers(auth_token,friend_id,type, item_position);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(context, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject player_jsonObject = new JSONObject(responseData);

                    if(player_jsonObject.has("status") && !player_jsonObject.isNull("status"))
                    {
                        status = player_jsonObject.getString("status");
                    }
                    if(player_jsonObject.has("message") && !player_jsonObject.isNull("message"))
                    {
                        message = player_jsonObject.getString("message");
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalStatus = status;
                final String finalMessage = message;
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(context, false);

                        if(finalStatus.equalsIgnoreCase("success"))
                        {
                            /**Save records inside local database*/
                            saveInvitedUserRecords(notificationModelArrayList,item_position);

                            notificationModelArrayList.remove(item_position);
                            notifyDataSetChanged();
                            snackBar.setSnackBarMessage(finalMessage);
                        }
                        else
                        {
                            snackBar.setSnackBarMessage(finalMessage);
                        }
                    }
                });
            }
        });
    }
    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForAddPlayers(String auth_token,
                                                    String friend_id,
                                                    String type,
                                                    int item_position) {

        HttpUrl.Builder urlBuilder = null;
        if(type.equalsIgnoreCase("Accept")) {
            //urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION + "accept_request").newBuilder();
            urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "accept_request").newBuilder();
            urlBuilder.addQueryParameter("accepted_at", ApplicationUtility.getCurrentDateTime());
            //urlBuilder.addQueryParameter("object_type", "user");
        } else {
            //urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION + "reject_request").newBuilder();
            urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "reject_request").newBuilder();
        }

        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("friend_id", friend_id);


        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }



    /**
     * Function to get Venue Courts detail from server
     */
    public void getVenuesCourtsDetailsInfor(String court_Id, String userId) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(court_Id, userId);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(context, false);
                Toast.makeText(context,e.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "",venue_id ="",list_id = "",capacity = "", name = "",
                        latitude = "", longitude = "",review_rating = "",
                        review_count = "",court_type = "",description = "",
                        price = "",address = "",city = "",state = "",
                        country = "", total_number_players = "",
                        game_date = "", startTIme = "", endTime = "",
                        skillLevel = "", no_of_participants = "",
                        eventSportName = "", eventCourtName = "",
                        event_creator_name = "", event_creator_image = "",
                        event_creator_user_id = "", minimum_price = "",
                        actual_price = "", cancel_policy_type = "",cancellation_point = "",
                        object_type = "",minimum_age = "", space_type = "", space_desc = "",
                        phone = "", website = "", rules = "" ;
                boolean isFavorite = false, isEquipmenmtAvailable = false;
                ArrayList<String> space_desc_array = new ArrayList<String>();
                ArrayList<String> equipments_array = new ArrayList<String>();
                ArrayList<String> amenities_array = new ArrayList<String>();

                try {
                    //JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    eventListingModelArrayList = new ArrayList<EventListingModel>();
                    eventListingModel = new EventListingModel();
                    ArrayList<String> images_arraylist = new ArrayList<String>();

                    if(jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if(jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if(jsonObject.has("list_id") && !jsonObject.isNull("list_id")) {
                        list_id = jsonObject.getString("list_id");
                    }
                    if(jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                        Constants.EVENT_NAME_MAP = name;
                    }
                    if(jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                        Constants.EVENT_LATITUDE = Double.parseDouble(latitude);
                    }
                    if(jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                        Constants.EVENT_LONGITUDE = Double.parseDouble(longitude);
                    }
                    if(jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }

                    if(jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }

                    if(jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if(jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }
                    if(jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if(jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if(jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }

                    if(jsonObject.has("no_of_player") && !jsonObject.isNull("no_of_player")){
                        total_number_players = jsonObject.getString("no_of_player");
                    }

                    if(jsonObject.has("no_of_partitipants") && !jsonObject.isNull("no_of_partitipants")){
                        no_of_participants = jsonObject.getString("no_of_partitipants");
                    }

                    if(jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")){
                        isFavorite = jsonObject.getBoolean("is_favorite");
                    }

                    if(jsonObject.has("price") && !jsonObject.isNull("price")) {
                        try{
                            price = jsonObject.getString("price");
                            int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                            float price_pre_player = Float.parseFloat(price) / (float)totalPlayer;
                            actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                            float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                            minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                        }catch(Exception ex){
                            ex.printStackTrace();;
                        }
                    }

                    if(jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")){
                        eventSportName = jsonObject.getString("sport_name");
                    }

                    if(jsonObject.has("game_date") && !jsonObject.isNull("game_date")){
                        game_date = jsonObject.getString("game_date");
                    }

                    if(jsonObject.has("skill_level") && !jsonObject.isNull("skill_level")){
                        skillLevel = jsonObject.getString("skill_level");
                    }

                    if(jsonObject.has("list_name") && !jsonObject.isNull("list_name")){
                        eventCourtName = jsonObject.getString("list_name");
                    }

                    if(jsonObject.has("user_name") && !jsonObject.isNull("user_name")){
                        event_creator_name = jsonObject.getString("user_name");
                    }

                    if(jsonObject.has("user_id") && !jsonObject.isNull("user_id")){
                        event_creator_user_id = jsonObject.getString("user_id");
                    }

                    if(jsonObject.has("user_profile_image") && !jsonObject.isNull("user_profile_image")){
                        event_creator_image = jsonObject.getString("user_profile_image");
                    }
                    if(jsonObject.has("cancel_policy_type")&& !jsonObject.isNull("cancel_policy_type"))
                    {
                        cancel_policy_type = jsonObject.getString("cancel_policy_type");
                    }
                    if(jsonObject.has("cancellation_point")&& !jsonObject.isNull("cancellation_point"))
                    {
                        cancellation_point = jsonObject.getString("cancellation_point");
                    }
                    if(jsonObject.has("object_type")&& !jsonObject.isNull("object_type"))
                    {
                        object_type = jsonObject.getString("object_type");
                    }

                    if(jsonObject.has("equipments") && !jsonObject.isNull("equipments")){
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");

                        if(equipmentArray.length() > 0){
                            isEquipmenmtAvailable = true;
                        }
                    }

                    /*if(jsonObject.has("time_slot")&& !jsonObject.isNull("time_slot")) {
                        ArrayList<String> timeslot_array = new ArrayList<String>();
                        JSONArray images_json_array = jsonObject.getJSONArray("time_slot");
                        for (int j = 0; j < images_json_array.length(); j++) {
                            JSONArray getValues = images_json_array.getJSONArray(j);
                            for(int x = 0; x < getValues.length(); x++){
                                if(j == 0){
                                    startTIme = getValues.getString(1);
                                }else{
                                    endTime = getValues.getString(1);
                                }
                            }
                        }
                    }*/


                    if(jsonObject.has("time_slot")&& !jsonObject.isNull("time_slot")) {
                        String start_time = null;
                        JSONArray time_slot_array_json_array = jsonObject.getJSONArray("time_slot");
                        for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                            JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                            if(j == 0){
                                startTIme = getValues.getString("start_time");
                            }
                            endTime = getValues.getString("end_time");
                        }
                    }


                    if(jsonObject.has("images")&& !jsonObject.isNull("images")) {
                        ArrayList<String> images_array = new ArrayList<String>();
                        JSONArray images_json_array = jsonObject.getJSONArray("images");
                        for (int j = 0; j < images_json_array.length(); j++) {
                            images_array.add(images_json_array.getString(j));
                        }
                        eventListingModel.setImages_array(images_array);
                    }


                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        JSONObject add_info_json_object = jsonObject.getJSONObject("additional_info");

                        JSONArray jsonArray = add_info_json_object.getJSONArray("space_desc");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String value = jsonArray.get(i).toString();
                            space_desc_array.add(value);
                        }

                        if(add_info_json_object.has("equipments")&& !add_info_json_object.isNull("equipments")) {

                            JSONArray equipments_json_array = add_info_json_object.getJSONArray("equipments");
                            for (int i = 0; i < equipments_json_array.length(); i++) {
                                String value = equipments_json_array.get(i).toString();
                                equipments_array.add(value);
                            }

                        }
                        if(add_info_json_object.has("amenities")&& !add_info_json_object.isNull("amenities")) {

                            JSONArray amenities_json_array = add_info_json_object.getJSONArray("amenities");
                            for (int i = 0; i < amenities_json_array.length(); i++) {
                                String value = amenities_json_array.get(i).toString();
                                amenities_array.add(value);
                            }
                        }
                        if(add_info_json_object.has("phone")&& !add_info_json_object.isNull("phone")) {
                            phone = add_info_json_object.getString("phone");
                        }
                        if(add_info_json_object.has("website")&& !add_info_json_object.isNull("website")) {
                            website = add_info_json_object.getString("website");
                        }
                        if(add_info_json_object.has("rules")&& !add_info_json_object.isNull("rules")) {
                            rules = add_info_json_object.getString("rules");
                        }

                        eventListingModel.setAdditional_space_desc_array(space_desc_array);
                        eventListingModel.setAdditional_equipmentes_array(equipments_array);
                        eventListingModel.setAdditional_amenities_array(amenities_array);
                        eventListingModel.setAdditional_phone_number(phone);
                        eventListingModel.setAdditional_website(website);
                        eventListingModel.setAdditional_venue_rules(rules);
                    }


                    if(jsonObject.has("minimum_age")&& !jsonObject.isNull("minimum_age")) {
                        minimum_age = jsonObject.getString("minimum_age");
                        if(minimum_age != null && !minimum_age.equalsIgnoreCase("No minimum age required")){
                            minimum_age = minimum_age + " + ";
                        }
                    }

                    if(jsonObject.has("space_type")&& !jsonObject.isNull("space_type")) {
                        space_type = jsonObject.getString("space_type");
                    }

                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        String space = "";
                        JSONObject jsonObject_additional_info = jsonObject.getJSONObject("additional_info");

                        if(jsonObject_additional_info.has("space_desc") && !jsonObject_additional_info.isNull("space_desc")){
                            JSONArray spaceArray = jsonObject_additional_info.getJSONArray("space_desc");
                            for (int i = 0; i < spaceArray.length(); i++) {

                                // if(!TextUtils.isEmpty(space)){
                                space = space + "," + spaceArray.get(i);
                                // }
                            }

                            if(TextUtils.isEmpty(space)){
                                space_desc = "No Space Available";
                            }else {
                                if (space != null
                                        && space
                                        .length() > 1) {
                                    space_desc = space.substring(1,space.length());
                                }
                                //space_desc = space;
                            }
                        }
                    }


                    if(jsonObject.has("game_partitipants")&& !jsonObject.isNull("game_partitipants")){

                        try{
                            JSONArray particiapntsArray = jsonObject.getJSONArray("game_partitipants");
                            AppConstants.userModelArary = new ArrayList<UserMolel>();
                            AppConstants.userIdArray = new ArrayList<String>();
                            if(particiapntsArray.length() > 0){
                                for(int i = 0; i < particiapntsArray.length(); i++){
                                    UserMolel model = new UserMolel();
                                    JSONObject participantsObj = particiapntsArray.getJSONObject(i);
                                    String userName = participantsObj.getString("user_name");
                                    String userId = participantsObj.getString("user_id");
                                    String user_profile_image = participantsObj.getString("user_profile_image");
                                    model.setUserFullName(userName);
                                    model.setUserId(userId);
                                    model.setUserProfileImage(user_profile_image);
                                    AppConstants.userIdArray.add(userId);
                                    AppConstants.userModelArary.add(model);
                                }
                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }

                    eventListingModel.setEvent_Id(id);
                    eventListingModel.setList_id(list_id);
                    eventListingModel.setEvent_Name(name);
                    eventListingModel.setLatitude(latitude);
                    eventListingModel.setLongitude(longitude);
                    eventListingModel.setEvent_Ratings(review_rating);
                    eventListingModel.setEvent_Reviews(review_count);
                    eventListingModel.setEvent_Price(actual_price);
                    eventListingModel.setCourt_type(court_type);
                    eventListingModel.setDescription(description);
                    eventListingModel.setAddress(address);
                    eventListingModel.setCity(city);
                    eventListingModel.setState(state);
                    eventListingModel.setCountry(country);
                    eventListingModel.setCapacity(capacity);
                    eventListingModel.setEvent_Total_Participants(total_number_players);
                    eventListingModel.setEventDate(game_date);
                    eventListingModel.setEventStartTime(startTIme);
                    eventListingModel.setEventEndTime(endTime);
                    eventListingModel.setSkill_level(skillLevel);
                    eventListingModel.setEvent_accepted_Participants(no_of_participants);
                    eventListingModel.setEvent_Sports_Type(eventSportName);
                    eventListingModel.setEvent_court_name(eventCourtName);
                    eventListingModel.setEvent_Creator_Image(event_creator_image);
                    eventListingModel.setCreator_name(event_creator_name);
                    eventListingModel.setCreator_user_id(event_creator_user_id);
                    eventListingModel.setEvent_Minimum_Price(minimum_price);
                    eventListingModel.setFavorite(isFavorite);
                    eventListingModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    eventListingModel.setEvent_Total_Price(price);
                    eventListingModel.setCancel_policy_type(cancel_policy_type);
                    eventListingModel.setCancellation_point(cancellation_point);
                    eventListingModel.setObject_Type(object_type);
                    eventListingModel.setCourt_min_players(minimum_age);
                    eventListingModel.setCourt_space_desc(space_desc);
                    eventListingModel.setSpace_type(space_type);
                    eventListingModelArrayList.add(eventListingModel);

                }catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(context, false);

                if(isSuccessStatus) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //context.finish();
                            Constants.FROM_NOTIFICATION = true;
                            EventAdapter.eventListingModelArrayList = eventListingModelArrayList;
                            Intent intent = new Intent(context, EventDetail.class);
                            context.startActivity(intent);
                        }
                    });
                }
                else
                {
                    snackBar.setSnackBarMessage("Network error! Please try after sometime");
                }
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String court_Id, String userId) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_game_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.EVENT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", userId);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(context, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Fetching selected invited user details & store inside local database
     */
    public void saveInvitedUserRecords(ArrayList<NotificationModel> notificationModelArrayList, int item_position)
    {
        String userId = notificationModelArrayList.get(item_position).getUser_id();
        String userName = notificationModelArrayList.get(item_position).getUser_name();
        String firstName = notificationModelArrayList.get(item_position).getFirst_name();
        String lastName = notificationModelArrayList.get(item_position).getLast_name();
        String image = notificationModelArrayList.get(item_position).getUser_profile_image();
        String sport = notificationModelArrayList.get(item_position).getGame_name();
        String phone_number = notificationModelArrayList.get(item_position).getPhone_number();
        String status = notificationModelArrayList.get(item_position).getStatus();
        String rewards = notificationModelArrayList.get(item_position).getRewards();

        db.insertfriend(userId,userName,firstName,lastName,
                image,sport,phone_number,status,rewards);
    }
}