package com.andolasoft.squadz.views.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.Constants;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by SpNayak on 1/3/2017.
 */

public class EventSportsAdapter extends BaseAdapter {
    ArrayList<String> checkbox_array = new ArrayList<>();
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    private ArrayList<String> sportsArray = new ArrayList<String>();
    private LayoutInflater layoutInflater;
    private Context context;

    public EventSportsAdapter(Context context, ArrayList<String> sportsArray) {
        this.sportsArray = sportsArray;
        this.context = context;
        layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return sportsArray.size();
    }

    @Override
    public Object getItem(int position) {
        return sportsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.event_sports_layout, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        mViewHolder.sports_name.setText(sportsArray.get(position));
        //mViewHolder.sport_icon.setBackgroundResource();

        return convertView;
    }

    private class MyViewHolder {
        TextView sports_name;
        ImageView sport_icon;

        public MyViewHolder(View item) {
            sports_name = (TextView) item.findViewById(R.id.sports_name);
            sport_icon = (ImageView) item.findViewById(R.id.sport_icon);

        }
    }
}

