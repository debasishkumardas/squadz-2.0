package com.andolasoft.squadz.views.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.VenueDetails;
import com.andolasoft.squadz.models.VenueDetailModel;
import com.andolasoft.squadz.models.VenueModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by SpNayak on 1/2/2017.
 */

public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.MyViewHolder> implements Filterable {

    public static VenueAdapter venueAdapter;
    public List<VenueModel> filteredList;
    public List<VenueModel> previouslist;
    private ArrayList<VenueModel> venueModelList;
    private Activity context;
    private CustomFilter mFilter;
    private ProgressDialog dialog;
    public VenueDetailModel venueDetailModel;
    public static ArrayList<VenueDetailModel> venueDetailModelArrayList;
    SnackBar snackBar;
    SharedPreferences prefs;
    public static String userId;
    boolean isAddedToFavorite = false;
    String status;
    public static String venueId;
    public static ArrayList<String> images_array;
    public static double court_latitude, court_longitude;
    public static GPSTracker gpsTracker;
    public static ArrayList<String> user_review_arrayList;

    public VenueAdapter(Activity context, ArrayList<VenueModel> venueModelList) {
        this.venueModelList = venueModelList;
        this.context = context;
        this.previouslist = venueModelList;
        filteredList = new ArrayList<VenueModel>();
        filteredList.addAll(previouslist);
        mFilter = new CustomFilter(VenueAdapter.this);
        snackBar = new SnackBar(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        userId = prefs.getString("loginuser_id", null);
        venueAdapter = this;
        gpsTracker = new GPSTracker(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_layout_venue_listing, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return venueModelList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final VenueModel venueModel = venueModelList.get(position);
        boolean isFavourite = venueModel.isFavourited();
        String venueName = venueModel.getVenueName_listing();

        if(isFavourite) {
            holder.favorite_button.setFavorite(true);
            holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_orange);
        }
        else{
            holder.favorite_button.setFavorite(false);
            holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_gray);
        }

        String price = venueModel.getVenuePrice();
        if(!TextUtils.isEmpty(price) &&
                !price.equalsIgnoreCase("0") &&
                !price.equalsIgnoreCase("0.0")) {
            holder.venue_price.setText("$ "+venueModel.getVenuePrice()+" / hour");
        }
        else{
            holder.venue_price.setTextColor(Color.parseColor("#000000"));
            holder.venue_price.setText("FREE");
        }
        holder.venue_name.setText(venueModel.getVenueName());
        holder.rating_venue_reviews.setText(venueModel.getReview_count()+" Reviews");
        holder.rating_venue.setRating(Float.parseFloat(venueModel.getVenueRatings()));
        holder.venueName.setText(venueName);

        //Displaying venue courts image
        //String venueImage = venueModel.getVenueImage();

        images_array = venueModelList.get(position).getImages_array();
        if(images_array.size() > 0) {
            if(!TextUtils.isEmpty(images_array.get(0))){
                Glide.with(context).load(images_array.get(0)).placeholder(R.mipmap.loading_placeholder_icon).into(holder.venue_image);
            }
        }
        else{
            //This image will display if no image will be found
            holder.venue_image.setBackgroundResource(R.mipmap.ic_no_image_found);
        }

        //Calculating distance from current to destination
        /*double current_loc_latitude = HomeFragment.currLocLatitude;
        double current_loc_longitude = HomeFragment.currLocLongitude;*/
        double current_loc_latitude = gpsTracker.getLatitude();
        double current_loc_longitude = gpsTracker.getLongitude();

        if(!TextUtils.isEmpty(venueModel.getLatitude())) {
            double dest_loc_latitude = Double.valueOf(venueModel.getLatitude());
            double dest_loc_longitude = Double.valueOf(venueModel.getLongitude());

            double distance = ApplicationUtility.distance(current_loc_latitude, current_loc_longitude,
                    dest_loc_latitude, dest_loc_longitude);

            holder.text_venue_miles.setText(String.valueOf(distance) + " miles");
        }
        else{
            holder.card_view_venue_list.setVisibility(View.GONE);
        }

        holder.card_view_venue_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.VENUE_FILTER_BTN_CLICK = false;
                Constants.UNCHECK_SPEC = false;

                court_latitude = Double.valueOf(venueModel.getLatitude());
                court_longitude = Double.valueOf(venueModel.getLongitude());
                /*court_latitude = gpsTracker.getLatitude();
                court_longitude = gpsTracker.getLongitude();*/

                images_array = venueModelList.get(position).getImages_array();
                venueId = venueModelList.get(position).getVenueId();
                String venue_name = venueModel.getVenueName();
                Constants.VENUE_NAME = venue_name;

                String price = venueModel.getVenuePrice();
                if(!TextUtils.isEmpty(price)&&
                        !price.equalsIgnoreCase("0")) {
                    Constants.VENUE_PRICE = price;
                }
                else{
                    Constants.VENUE_PRICE = "FREE";
                }
                String venueImage = venueModelList.get(position).getVenueImage().toString();
                Constants.VENUE_IMAGE = venueImage;
                getVenuesCourtsDetailsInfor(venueModelList.get(position).getVenueId());

            }
        });

        holder.favorite_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                venueId = venueModelList.get(position).getVenueId();
                boolean isFavourite =  venueModelList.get(position).isFavourited();
                if(isFavourite) {
                    makeListFavorite(userId, venueId, false,holder,venueModelList,position);
                }
                else{
                    makeListFavorite(userId, venueId, true, holder,venueModelList,position);
                }
            }
        });

        /*holder.favorite_button.setOnFavoriteAnimationEndListener(
                new MaterialFavoriteButton.OnFavoriteAnimationEndListener() {
                    @Override
                    public void onAnimationEnd(MaterialFavoriteButton buttonView, boolean favorite) {//

                        venueId = venueModelList.get(position).getVenueId();

                        if (favorite) {
                            makeListFavorite(userId,venueId,favorite);
                        } else {
                            makeListFavorite(userId,venueId,favorite);
                        }
                    }
                });*/
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView venue_name, text_venue_miles, rating_venue_reviews, venue_price, venueName;
        public RatingBar rating_venue;
        public ImageView venue_image, icon_venue_favourite;
        public CardView card_view_venue_list;
        public MaterialFavoriteButton favorite_button;

        public MyViewHolder(View view) {
            super(view);

            text_venue_miles = (TextView) view.findViewById(R.id.text_venue_miles);
            rating_venue_reviews = (TextView) view.findViewById(R.id.rating_venue_reviews);
            venue_price = (TextView) view.findViewById(R.id.venue_price);
            venue_name = (TextView) view.findViewById(R.id.venue_name);
            rating_venue = (RatingBar) view.findViewById(R.id.rating_venue);
            venue_image = (ImageView) view.findViewById(R.id.venue_image);
            icon_venue_favourite = (ImageView) view.findViewById(R.id.icon_venue_favourite);
            card_view_venue_list = (CardView) view.findViewById(R.id.card_view_venue_list);
            favorite_button = (MaterialFavoriteButton) view.findViewById(R.id.favorite_button);
            venueName = (TextView) view.findViewById(R.id.tvVenueName);
        }
    }

    /*********************
     * CLASS FOR CUSTOM FILTER
     *********************************************/
    public class CustomFilter extends Filter {

        private VenueAdapter mAdapter;

        private CustomFilter(VenueAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(previouslist);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final VenueModel list : previouslist) {
                    // if (list.getTaskName().toLowerCase().startsWith(filterPattern)) {
                    if (list.getVenueName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(list);
                    }
                }
            }
            venueModelList = (ArrayList<VenueModel>) filteredList;
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            notifyDataSetChanged();

            if (((List<VenueModel>) results.values).size() == 0) {

                snackBar.setSnackBarMessage("No courts found");
                //Toast.makeText(context, "No events found", Toast.LENGTH_LONG).show();
                notifyDataSetChanged();
            } else {
//                text_contacts.setVisibility(View.GONE);
//                stickyList.setVisibility(View.VISIBLE);
            }
        }

    }

    /**
     * Function to get Venue Courts detail from server
     */
    public void getVenuesCourtsDetailsInfor(String court_Id) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(court_Id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(context, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "",venue_id ="",capacity = "", name = "",
                        latitude = "", longitude = "",review_rating = "",
                        review_count = "",court_type = "",description = "",
                        price = "",address = "",city = "",state = "",country = "",
                        eventSportName = "",cancel_policy_type = "",cancellation_point = "", minimum_age = "",
                        space_type = "", space_desc = "",
                        phone = "", website = "", rules = "", venueName = "";
                boolean favourite_status = false,isEquipmenmtAvailable = false;;

                ArrayList<String> images_arraylist = new ArrayList<String>();
                ArrayList<String> specs_arraylist = new ArrayList<String>();
                ArrayList<String> space_desc_array = new ArrayList<String>();
                ArrayList<String> equipments_array = new ArrayList<String>();
                ArrayList<String> amenities_array = new ArrayList<String>();
                user_review_arrayList = new ArrayList<String>();

                try {
                    //JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    venueDetailModelArrayList = new ArrayList<VenueDetailModel>();
                    venueDetailModel = new VenueDetailModel();

                    if(jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if(jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if(jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                    }
                    if(jsonObject.has("capacity") && !jsonObject.isNull("capacity")) {
                        capacity = jsonObject.getString("capacity");
                    }
                    if(jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                    }
                    if(jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                    }
                    if(jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }
                    if(jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }
                    if(jsonObject.has("price") && !jsonObject.isNull("price"))
                    {
                        price = jsonObject.getString("price");
                    }
                    if(jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if(jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }
                    if(jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if(jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if(jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }
                    if(jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")) {
                        favourite_status = jsonObject.getBoolean("is_favorite");
                    }

                    if(jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")){
                        eventSportName = jsonObject.getString("sport_name");
                    }
                    if(jsonObject.has("equipments") && !jsonObject.isNull("equipments")){
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");
                        if(equipmentArray.length() > 0){
                            isEquipmenmtAvailable = true;
                        }
                    }

                    if(jsonObject.has("images")&& !jsonObject.isNull("images")) {
                        JSONArray images_json_array = jsonObject.getJSONArray("images");
                        for (int i = 0; i < images_json_array.length(); i++) {
                            images_arraylist.add(images_json_array.getString(i));
                        }
                        venueDetailModel.setImages_array(images_arraylist);
                    }

                    /**Fetching user review & save in a arraylist
                     * for prefilling in the Add review page for again updating the review*/
                    if(jsonObject.has("user_review")&& !jsonObject.isNull("user_review")) {
                        JSONObject user_review_object = jsonObject.getJSONObject("user_review");
                        if(user_review_object.length() > 0) {
                            String user_rating = user_review_object.getString("rating");
                            String user_review = user_review_object.getString("review");
                            user_review_arrayList.add(user_rating + "@@@@" + user_review);
                        }
                    }

                    if(jsonObject.has("cancel_policy_type")&& !jsonObject.isNull("cancel_policy_type")) {
                        cancel_policy_type = jsonObject.getString("cancel_policy_type");
                    }

                    if(jsonObject.has("cancellation_point")&& !jsonObject.isNull("cancellation_point")) {
                        cancellation_point = jsonObject.getString("cancellation_point");
                    }
                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        JSONObject add_info_json_object = jsonObject.getJSONObject("additional_info");

                        JSONArray jsonArray = add_info_json_object.getJSONArray("space_desc");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String value = jsonArray.get(i).toString();
                            space_desc_array.add(value);
                        }

                        if(add_info_json_object.has("equipments")&& !add_info_json_object.isNull("equipments")) {

                            JSONArray equipments_json_array = add_info_json_object.getJSONArray("equipments");
                            for (int i = 0; i < equipments_json_array.length(); i++) {
                                String value = equipments_json_array.get(i).toString();
                                equipments_array.add(value);
                            }

                        }
                        if(add_info_json_object.has("amenities")&& !add_info_json_object.isNull("amenities")) {

                            JSONArray amenities_json_array = add_info_json_object.getJSONArray("amenities");
                            for (int i = 0; i < amenities_json_array.length(); i++) {
                                String value = amenities_json_array.get(i).toString();
                                amenities_array.add(value);
                            }
                        }
                        if(add_info_json_object.has("phone")&& !add_info_json_object.isNull("phone")) {
                            phone = add_info_json_object.getString("phone");
                        }
                        if(add_info_json_object.has("website")&& !add_info_json_object.isNull("website")) {
                            website = add_info_json_object.getString("website");
                        }
                        if(add_info_json_object.has("rules")&& !add_info_json_object.isNull("rules")) {
                            rules = add_info_json_object.getString("rules");
                        }

                        venueDetailModel.setAdditional_space_desc_array(space_desc_array);
                        venueDetailModel.setAdditional_equipmentes_array(equipments_array);
                        venueDetailModel.setAdditional_amenities_array(amenities_array);
                        venueDetailModel.setAdditional_phone_number(phone);
                        venueDetailModel.setAdditional_website(website);
                        venueDetailModel.setAdditional_venue_rules(rules);
                    }

                    if(jsonObject.has("minimum_age")&& !jsonObject.isNull("minimum_age")) {
                        minimum_age = jsonObject.getString("minimum_age");
                        if(minimum_age != null && !minimum_age.equalsIgnoreCase("No minimum age required")){
                            minimum_age = minimum_age + " + ";
                        }
                    }

                    if(jsonObject.has("space_type")&& !jsonObject.isNull("space_type")) {
                        space_type = jsonObject.getString("space_type");
                    }

                    if(jsonObject.has("venue_name")&& !jsonObject.isNull("venue_name")) {
                        venueName = jsonObject.getString("venue_name");
                    }

                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        String space = "";
                        JSONObject jsonObject_additional_info = jsonObject.getJSONObject("additional_info");

                        if(jsonObject_additional_info.has("space_desc") && !jsonObject_additional_info.isNull("space_desc")){
                            JSONArray spaceArray = jsonObject_additional_info.getJSONArray("space_desc");
                            for (int i = 0; i < spaceArray.length(); i++) {

                               // if(!TextUtils.isEmpty(space)){
                                    space = space + "," + spaceArray.get(i);
                               // }
                            }

                            if(TextUtils.isEmpty(space)){
                                space_desc = "No Space Available";
                            }else {
                                if (space != null
                                        && space
                                        .length() > 1) {
                                    space_desc = space.substring(1,space.length());
                                }
                                //space_desc = space;
                            }
                        }
                    }

                    venueDetailModel.setId(id);
                    venueDetailModel.setVenue_id(venue_id);
                    venueDetailModel.setName(name);
                    venueDetailModel.setCapacity(capacity);
                    venueDetailModel.setLatitude(latitude);
                    venueDetailModel.setLongitude(longitude);
                    venueDetailModel.setReview_rating(review_rating);
                    venueDetailModel.setReview_count(review_count);
                    venueDetailModel.setPrice(price);
                    venueDetailModel.setCourt_type(court_type);
                    venueDetailModel.setDescription(description);
                    venueDetailModel.setAddress(address);
                    venueDetailModel.setCity(city);
                    venueDetailModel.setState(state);
                    venueDetailModel.setCountry(country);
                    venueDetailModel.setFavourite(favourite_status);
                    venueDetailModel.setEvent_Sports_Type(eventSportName);
                    venueDetailModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    venueDetailModel.setCancel_policy_type(cancel_policy_type);
                    venueDetailModel.setCancellation_point(cancellation_point);
                    venueDetailModel.setSpace_type(space_type);
                    venueDetailModel.setCourt_min_players(minimum_age);
                    venueDetailModel.setCourt_space_desc(space_desc);
                    venueDetailModel.setVenueName(venueName);
                    venueDetailModelArrayList.add(venueDetailModel);
                }catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(context, false);

                if(isSuccessStatus) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            ((Activity)context).finish();
                            AppConstants.OBJECT_TYPE_GAME = "list";
                            Intent intent = new Intent(context, VenueDetails.class);
                            context.startActivity(intent);
                        }
                    });
                }
                else {
                    snackBar.setSnackBarMessage("Network error! Please try after sometime");
                }
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String court_Id) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_court_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.VENUE_COURT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", userId);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(context, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }


    /**
     * Function to get Venue courts from server
     */
    public void makeListFavorite(String user_id, String list_id, boolean isFavorite, final MyViewHolder holder, final ArrayList<VenueModel> modelArray, final int position) {
        //Handling the loader state
        LoaderUtility.handleLoader(context, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, list_id, isFavorite);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(context, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    isAddedToFavorite = responseObject.getBoolean("isFavorite");

                }catch(Exception ex){
                    ex.printStackTrace();
                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(context, false);
                        Constants.FAVOURITE_CLICKED = true;

                        if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            if(isAddedToFavorite){
                                snackBar.setSnackBarMessage("Successfully Added to WishList");
                                holder.favorite_button.setFavorite(true);
                                holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_orange);
                                modelArray.get(position).setFavourited(isAddedToFavorite);
                                venueAdapter.notifyDataSetChanged();
                            }else{
                                snackBar.setSnackBarMessage("Successfully Removed from WishList");
                                holder.favorite_button.setFavorite(false);
                                holder.favorite_button.setFavoriteResource(R.mipmap.ic_favorite_gray);
                                modelArray.get(position).setFavourited(isAddedToFavorite);
                                venueAdapter.notifyDataSetChanged();
                            }

                        }else if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                            snackBar.setSnackBarMessage("Failed to Add to WidhList");
                            notifyDataSetChanged();
                        }else{
                            snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE_ERR);
                            notifyDataSetChanged();
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String userId, String objectId, boolean favoriteStatus) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "add_to_favorite").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("object_id", objectId);
        urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_LIST);
        if(favoriteStatus){
            urlBuilder.addQueryParameter("is_favorite", Boolean.TRUE.toString());
        }else{
            urlBuilder.addQueryParameter("is_favorite", Boolean.FALSE.toString());
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }

}
