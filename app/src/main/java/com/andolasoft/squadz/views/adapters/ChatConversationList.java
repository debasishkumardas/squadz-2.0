package com.andolasoft.squadz.views.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.activities.ChatActivity;
import com.andolasoft.squadz.activities.SplashScreen;
import com.andolasoft.squadz.fragments.TabFragment;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.ChatConversationModel;
import com.andolasoft.squadz.models.FirebaseGroupChatParticipantModel;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.EndPoints;
import com.bumptech.glide.Glide;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class ChatConversationList extends BaseAdapter {
	Activity mContext;
	LayoutInflater inflater;
	private ArrayList<ChatConversationModel> arraylist;
	SharedPreferences preferences;
	String auth_token, user_id,userId;
	ArrayList<String> frd_status;
	DatabaseAdapter db;
    ProgressDialog dialog;
    String status, message;
    MaterialDialog mMaterialDialog;
    Firebase postRef_delete_group;
    int badgeCount = 0;

	public ChatConversationList(Activity mContext, ArrayList<ChatConversationModel> sort) {
		this.mContext = mContext;
		this.arraylist = sort;
		inflater = LayoutInflater.from(mContext);
		preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		auth_token = preferences.getString("auth_token", null);
        userId = preferences.getString("loginuser_id", null);
		frd_status = new ArrayList<String>();
		db = new DatabaseAdapter(this.mContext);
	}

	@Override
	public int getCount() {
		return arraylist.size();
	}

	@Override
	public ChatConversationModel getItem(int arg0) {
		return arraylist.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		return position;

	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		//convertView = null;
		convertView = inflater.inflate(R.layout.layout_chat_list, null);
		final TextView user_name = (TextView) convertView
				.findViewById(R.id.friendship_status);
		final TextView full_name = (TextView) convertView
				.findViewById(R.id.player_teamate__name);
		final ImageView user_img = (ImageView) convertView
				.findViewById(R.id.user_pic);
		final ImageView frd_req_icon = (ImageView) convertView
				.findViewById(R.id.invited_frnd);
		frd_req_icon.setVisibility(View.GONE);
		View view_players = (View) convertView.findViewById(R.id.view_players);
		view_players.setVisibility(View.GONE);

        String userName = arraylist.get(position).getChatUserName();
        String user_profile_pic = arraylist.get(position).getImageUrl();
        String chat_Llast_message = arraylist.get(position).getLastMessage();
		int unreadMessageCount = arraylist.get(position).getReadMessageCount();


        if(unreadMessageCount > 0){
            full_name.setTextColor(Color.parseColor("#F97503"));
            full_name.setText(userName + " (" + String.valueOf(unreadMessageCount) + ") ");
            //badgeCount = badgeCount + unreadMessageCount;
            //SplashScreen.messageBadgeCount = badgeCount;
            //TabFragment.refreshChatBadgeCount();
        }else{
            full_name.setText(userName);
        }


        if(!TextUtils.isEmpty(chat_Llast_message)){
            user_name.setText(chat_Llast_message);
        }else{
            user_name.setText("(No message)");
        }
        if(!TextUtils.isEmpty(user_profile_pic.trim()) &&
                user_profile_pic != null &&
                !user_profile_pic.equalsIgnoreCase("null")){
            Glide.with(mContext).load(user_profile_pic).into(user_img);
        }

        convertView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = position;
                AppConstants.CHAT_USER_NAME = arraylist.get(pos).getChatUserName();
                AppConstants.CHAT_CHANNEL_ID = arraylist.get(pos).getChannelId();
                AppConstants.CHAT_USER_IMAGE_URL = arraylist.get(pos).getImageUrl();
                AppConstants.CHAT_USER_ID = arraylist.get(pos).getRecieverID();
				AppConstants.CHAT_TYPE = arraylist.get(pos).getChatType();
                AppConstants.CHAT_THREAD_UNREAD_MESSAGE_COUNT = arraylist.get(pos).getReadMessageCount();

                if(AppConstants.CHAT_TYPE.equalsIgnoreCase("group")){
                    ArrayList<FirebaseGroupChatParticipantModel> chatParticipantModel = arraylist.get(pos).getChatMembers();

                    if(chatParticipantModel.size() > 0){
                        for(int i = 0; i < chatParticipantModel.size(); i++){
                            String participantsName = chatParticipantModel.get(i).getUsername();
                            AppConstants.groupParticipantsName.add(participantsName);

                            String participantsIds = chatParticipantModel.get(i).getUser_id();
                            AppConstants.groupParticipantsIds.add(participantsIds);
                        }
                    }
                }
                Intent intent = new Intent(mContext, ChatActivity.class);
                mContext.startActivity(intent);
            }
        });

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                int pos = position;
                String getChatType = arraylist.get(pos).getChatType();

                if(getChatType != null &&
                        getChatType.equalsIgnoreCase("individual")){
                    String ChannelId = arraylist.get(pos).getChannelId();
                    String chat_user_id = arraylist.get(pos).getRecieverID();
                    displayDialog(ChannelId,chat_user_id,arraylist, pos, getChatType);
                }else{
                    String ChannelId = arraylist.get(pos).getChannelId();
                    String chat_user_id = arraylist.get(pos).getRecieverID();
                    displayDialog(ChannelId,chat_user_id,arraylist, pos, getChatType);
                }

                return false;
            }
        });
		return convertView;
	}


    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayDialog(final String ChannelId, final String chatUserId ,
                              final ArrayList<ChatConversationModel> arrayList,
                              final int position, final String chatType){
        mMaterialDialog = new MaterialDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_delete_conversation, null);
        TextView tv_card_numner = (TextView) view.findViewById(R.id.tv_card_number);
        TextView tv_card_option_primary_card = (TextView) view.findViewById(R.id.tv_make_card_primary);
        tv_card_option_primary_card.setVisibility(View.GONE);
        TextView tv_card_option_delete_card = (TextView) view.findViewById(R.id.tv_delete_card);
        tv_card_option_delete_card.setText("Are you sure want to delete this conversation?");
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        TextView tv_confirm = (TextView) view.findViewById(R.id.tv_confirm);
        tv_card_numner.setText("Delete Chat");
        tv_card_option_primary_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();

                if(chatType != null && chatType.equalsIgnoreCase("individual")){
                    deleteCOnversation(ChannelId, chatUserId, arrayList, position);
                }else{
                    deleteGroupConversation(ChannelId, chatUserId, arrayList, position);
                }

            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }


    public void deleteCOnversation(final String channelId, final String chatUserId,
                                   ArrayList<ChatConversationModel> arrayList, int position){
        //Initializing the firebase instance
        Firebase.setAndroidContext(mContext);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Initializing the child
        postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(userId).child(channelId);
        //Removing the channel from the logged in user id
        postRef.removeValue();


        //Initializing the firebase instance
        Firebase postRef_receiver = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Initializing the child
        postRef_receiver = postRef_receiver.child(AppConstants.CONVERSATION_NODE).child(chatUserId).child(channelId);
        //Removing the channel from the chat user id
        postRef_receiver.removeValue();

        //Removing the value from the array
        arrayList.remove(position);

        //Notifying the data changed
        notifyDataSetChanged();

    }

    /**
     * Function responsible for deleting a group
     * @param channelId Chat Channel Id
     * @param chatUserId Group Id
     * @param arrayList The Listview Array
     * @param position The selected position
     */

    public void deleteGroupConversation(final String channelId, final String chatUserId,
                                   final ArrayList<ChatConversationModel> arrayList, final int position){
        //Initializing the firebase instance
        Firebase.setAndroidContext(mContext);
        postRef_delete_group = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Initializing the child
        postRef_delete_group = postRef_delete_group.child(AppConstants.CONVERSATION_NODE).child(userId).child("groupIds");

        // Attach a listener to read the data at our posts reference
        postRef_delete_group.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                for(com.firebase.client.DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    String message_group_id_key = (String) postSnapshot.getKey();
                    String message_group_id = (String) postSnapshot.getValue();
                    if(message_group_id.equalsIgnoreCase(chatUserId)){
                        postRef_delete_group.child(message_group_id_key).removeValue();
                        if(arrayList.size() > position){
                            //Removing the value from the array
                            arrayList.remove(position);
                            AppConstants.isgroupDeleted = true;
                        }
                        //Notifying the data changed
                        notifyDataSetChanged();
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(mContext, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }


}
