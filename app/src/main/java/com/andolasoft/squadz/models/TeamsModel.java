package com.andolasoft.squadz.models;

/**
 * Created by Debasish Kumar Das on 2/8/2017.
 */
public class TeamsModel {

    String teamId;
    String teamName;
    String teamSport;
    String teamCustomSport;
    String teamCreatorId;
    String teamCreatorName;
    String teamCreatorImage;
    String teamPlayerCount;
    boolean isCheckboxSelected = false;

    public TeamsModel(String teamId, String teamName, String teamSport,
                      String teamCustomSport, String teamCreatorId,
                      String teamCreatorName, String teamCreatorImage, String player_count) {
        this.teamId = teamId;
        this.teamName = teamName;
        this.teamSport = teamSport;
        this.teamCustomSport = teamCustomSport;
        this.teamCreatorId = teamCreatorId;
        this.teamCreatorName = teamCreatorName;
        this.teamCreatorImage = teamCreatorImage;
        this.teamPlayerCount = player_count;
    }


    public String getTeamId() {
        return teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getTeamSport() {
        return teamSport;
    }

    public String getTeamCustomSport() {
        return teamCustomSport;
    }

    public String getTeamCreatorId() {
        return teamCreatorId;
    }

    public String getTeamCreatorName() {
        return teamCreatorName;
    }

    public String getTeamCreatorImage() {
        return teamCreatorImage;
    }

    public String getTeamPlayerCount() {
        return teamPlayerCount;
    }

    public boolean isCheckboxSelected() {
        return isCheckboxSelected;
    }

    public void setCheckboxSelected(boolean checkboxSelected) {
        isCheckboxSelected = checkboxSelected;
    }
}
