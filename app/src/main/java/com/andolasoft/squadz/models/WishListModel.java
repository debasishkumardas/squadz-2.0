package com.andolasoft.squadz.models;

import java.util.ArrayList;

/**
 * Created by SpNayak on 1/2/2017.
 */

public class WishListModel {

    private String Id;
    private String Image;
    private String Name;
    private String Creator_Name;
    private String Creator_Image;
    private String Sports_Type;
    private String Sports_Type_Image;
    private String Ratings;
    private String Reviews;
    private String Miles;
    private String Price;
    private String Minimum_Price;
    private String Total_Participants;
    private String Spots;
    private String Minimum_Amount_Text;
    private String Date;
    private String Time;
    private boolean is_Favourited = false;
    private String description;
    private String address;
    private String city;
    private String state;
    private String country;
    private String capacity;
    private String skill_level;
    private String eventDate;
    private ArrayList<String> images_array = new ArrayList<>();
    private String eventStartTime;
    private String eventEndTime;
    private String creator_name;
    private String creator_user_id;
    private String accepted_Participants;
    private String court_name;
    private boolean isFavorite;
    private boolean isEquipmentAvailable;
    private String latitude;
    private String longitude;
    private String court_type;
    private String object_type;
    private String review_count;
    private String event_total_price;
    private String list_id;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCreator_Name() {
        return Creator_Name;
    }

    public void setCreator_Name(String creator_Name) {
        Creator_Name = creator_Name;
    }

    public String getCreator_Image() {
        return Creator_Image;
    }

    public void setCreator_Image(String creator_Image) {
        Creator_Image = creator_Image;
    }

    public String getSports_Type() {
        return Sports_Type;
    }

    public void setSports_Type(String sports_Type) {
        Sports_Type = sports_Type;
    }

    public String getSports_Type_Image() {
        return Sports_Type_Image;
    }

    public void setSports_Type_Image(String sports_Type_Image) {
        Sports_Type_Image = sports_Type_Image;
    }

    public String getRatings() {
        return Ratings;
    }

    public void setRatings(String ratings) {
        Ratings = ratings;
    }

    public String getReviews() {
        return Reviews;
    }

    public void setReviews(String reviews) {
        Reviews = reviews;
    }

    public String getMiles() {
        return Miles;
    }

    public void setMiles(String miles) {
        Miles = miles;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getMinimum_Price() {
        return Minimum_Price;
    }

    public void setMinimum_Price(String minimum_Price) {
        Minimum_Price = minimum_Price;
    }

    public String getTotal_Participants() {
        return Total_Participants;
    }

    public void setTotal_Participants(String total_Participants) {
        Total_Participants = total_Participants;
    }

    public String getSpots() {
        return Spots;
    }

    public void setSpots(String spots) {
        Spots = spots;
    }

    public String getMinimum_Amount_Text() {
        return Minimum_Amount_Text;
    }

    public void setMinimum_Amount_Text(String minimum_Amount_Text) {
        Minimum_Amount_Text = minimum_Amount_Text;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public boolean is_Favourited() {
        return is_Favourited;
    }

    public void setIs_Favourited(boolean is_Favourited) {
        this.is_Favourited = is_Favourited;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getSkill_level() {
        return skill_level;
    }

    public void setSkill_level(String skill_level) {
        this.skill_level = skill_level;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public ArrayList<String> getImages_array() {
        return images_array;
    }

    public void setImages_array(ArrayList<String> images_array) {
        this.images_array = images_array;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getCreator_name() {
        return creator_name;
    }

    public void setCreator_name(String creator_name) {
        this.creator_name = creator_name;
    }

    public String getCreator_user_id() {
        return creator_user_id;
    }

    public void setCreator_user_id(String creator_user_id) {
        this.creator_user_id = creator_user_id;
    }

    public String getAccepted_Participants() {
        return accepted_Participants;
    }

    public void setAccepted_Participants(String accepted_Participants) {
        this.accepted_Participants = accepted_Participants;
    }

    public String getCourt_name() {
        return court_name;
    }

    public void setCourt_name(String court_name) {
        this.court_name = court_name;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isEquipmentAvailable() {
        return isEquipmentAvailable;
    }

    public void setEquipmentAvailable(boolean equipmentAvailable) {
        isEquipmentAvailable = equipmentAvailable;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCourt_type() {
        return court_type;
    }

    public void setCourt_type(String court_type) {
        this.court_type = court_type;
    }

    public String getObject_type() {
        return object_type;
    }

    public void setObject_type(String object_type) {
        this.object_type = object_type;
    }

    public String getReview_count() {
        return review_count;
    }

    public void setReview_count(String review_count) {
        this.review_count = review_count;
    }

    public String getEvent_Total_Price() {
        return event_total_price;
    }
    public void setEvent_Total_Price(String event_total_price) {
        this.event_total_price = event_total_price;
    }
    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }
}
