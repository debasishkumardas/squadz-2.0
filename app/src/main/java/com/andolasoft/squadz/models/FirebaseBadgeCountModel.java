package com.andolasoft.squadz.models;

/**
 * Created by Debasish Kumar Das on 4/26/2017.
 */
public class FirebaseBadgeCountModel {
    long badgeCount;

    public long getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(long badgeCount) {
        this.badgeCount = badgeCount;
    }
}
