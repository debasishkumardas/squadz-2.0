package com.andolasoft.squadz.models;

import java.util.ArrayList;

/**
 * Created by Debasish Kumar Das on 3/3/2017.
 */
public class ChatConversationModel {

    String channelId;
    String chatUserName;
    int date;
    String imageUrl;
    String recieverID;
    String senderId;
    String chatType;
    String lastMessage;
    ArrayList<FirebaseGroupChatParticipantModel> chatMembers;
    int readMessageCount;
    boolean isOnline;

    public ChatConversationModel() {
    }

    public ChatConversationModel(String channelId, String chatUserName,
                                 int date, String imageUrl,
                                 String recieverID, String senderId, String chatType) {
        this.channelId = channelId;
        this.chatUserName = chatUserName;
        this.date = date;
        this.imageUrl = imageUrl;
        this.recieverID = recieverID;
        this.senderId = senderId;
        this.chatType = chatType;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getChatUserName() {
        return chatUserName;
    }

    public int getDate() {
        return date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getRecieverID() {
        return recieverID;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setRecieverID(String recieverID) {
        this.recieverID = recieverID;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public ArrayList<FirebaseGroupChatParticipantModel> getChatMembers() {
        return chatMembers;
    }

    public void setChatMembers(ArrayList<FirebaseGroupChatParticipantModel> chatMembers) {
        this.chatMembers = chatMembers;
    }

    public int getReadMessageCount() {
        return readMessageCount;
    }

    public void setReadMessageCount(int readMessageCount) {
        this.readMessageCount = readMessageCount;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }
}
