package com.andolasoft.squadz.models;

/**
 * Created by Debasish Kumar Das on 2/7/2017.
 */
public class InviteTeammatesModel {
    String userId;
    String userName;
    String firstName;
    String lastName;
    String image;
    String sport;
    String phoneNumber;
    String status;
    String reward;

    public InviteTeammatesModel(String userId, String userName, String firstName,
                                String lastName, String image,
                                String sport, String phoneNumber, String status, String reward) {
        this.userId = userId;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.image = image;
        this.sport = sport;
        this.phoneNumber = phoneNumber;
        this.status = status;
        this.reward = reward;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }
}
