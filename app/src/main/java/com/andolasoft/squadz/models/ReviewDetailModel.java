package com.andolasoft.squadz.models;

/**
 * Created by SpNayak on 3/14/2017.
 */

public class ReviewDetailModel
{
    private String reviewed_by_creator_id;
    private String reviewed_by_creator_image;
    private String reviewed_by_creator_name;
    private String reviewed_by_date;
    private String reviewed_by_ratings;
    private String reviewed_by_creator_description;

    public String getReviewed_by_creator_id() {
        return reviewed_by_creator_id;
    }

    public void setReviewed_by_creator_id(String reviewed_by_creator_id) {
        this.reviewed_by_creator_id = reviewed_by_creator_id;
    }

    public String getReviewed_by_creator_image() {
        return reviewed_by_creator_image;
    }

    public void setReviewed_by_creator_image(String reviewed_by_creator_image) {
        this.reviewed_by_creator_image = reviewed_by_creator_image;
    }

    public String getReviewed_by_creator_name() {
        return reviewed_by_creator_name;
    }

    public void setReviewed_by_creator_name(String reviewed_by_creator_name) {
        this.reviewed_by_creator_name = reviewed_by_creator_name;
    }

    public String getReviewed_by_date() {
        return reviewed_by_date;
    }

    public void setReviewed_by_date(String reviewed_by_date) {
        this.reviewed_by_date = reviewed_by_date;
    }

    public String getReviewed_by_ratings() {
        return reviewed_by_ratings;
    }

    public void setReviewed_by_ratings(String reviewed_by_ratings) {
        this.reviewed_by_ratings = reviewed_by_ratings;
    }

    public String getReviewed_by_creator_description() {
        return reviewed_by_creator_description;
    }

    public void setReviewed_by_creator_description(String reviewed_by_creator_description) {
        this.reviewed_by_creator_description = reviewed_by_creator_description;
    }
}
