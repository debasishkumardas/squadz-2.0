package com.andolasoft.squadz.models;

import java.util.ArrayList;

/**
 * Created by SpNayak on 2/2/2017.
 */

public class ProfileOtherViewModel {

    private String status = "", user_id = "",
            username = "", phone_number = "",
            name = "", email = "",
            birth_date = "", image = "",
            about_me = "", sport = "",
            skill_level = "", rewards = "",
            street_name = "", locality = "",
            province_abbreviation = "", postal_code = "",
            country = "", profile_visibility = "";
    ArrayList<String> secondary_sports_array = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAbout_me() {
        return about_me;
    }

    public void setAbout_me(String about_me) {
        this.about_me = about_me;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getSkill_level() {
        return skill_level;
    }

    public void setSkill_level(String skill_level) {
        this.skill_level = skill_level;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getProvince_abbreviation() {
        return province_abbreviation;
    }

    public void setProvince_abbreviation(String province_abbreviation) {
        this.province_abbreviation = province_abbreviation;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public ArrayList<String> getSecondary_sports_array() {
        return secondary_sports_array;
    }

    public void setSecondary_sports_array(ArrayList<String> secondary_sports_array) {
        this.secondary_sports_array = secondary_sports_array;
    }

    public String getProfile_visibility() {
        return profile_visibility;
    }

    public void setProfile_visibility(String profile_visibility) {
        this.profile_visibility = profile_visibility;
    }
}
