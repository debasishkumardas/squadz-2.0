package com.andolasoft.squadz.models;

import java.util.ArrayList;

/**
 * Created by SpNayak on 1/2/2017.
 */

public class VenueDetailModel {

    private String id;
    private String objectType;
    private String venue_id;
    private String name;
    private String capacity;
    private String latitude;
    private String longitude;
    private String review_rating;
    private String review_count;
    private String price;
    private String court_type;
    private String description;
    private String address;
    private String city;
    private String state;
    private String country;
    private ArrayList<String> images_array = new ArrayList<>();
    private ArrayList<String> specs_array = new ArrayList<>();
    private ArrayList<String> equipmentes_array = new ArrayList<>();
    private boolean isFavourite = false;
    private boolean isEquipmentAvailable;
    private String event_Sports_Type;
    private String cancel_policy_type;
    private String cancellation_point;
    private String space_type;
    private String court_min_players;
    private String court_space_desc;
    private ArrayList<String> additional_space_desc_array = new ArrayList<>();
    private ArrayList<String> additional_equipmentes_array = new ArrayList<>();
    private ArrayList<String> additional_amenities_array = new ArrayList<>();
    private String additional_website;
    private String additional_phone_number;
    private String additional_venue_rules;
    private String venueName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(String venue_id) {
        this.venue_id = venue_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getReview_rating() {
        return review_rating;
    }

    public void setReview_rating(String review_rating) {
        this.review_rating = review_rating;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getReview_count() {
        return review_count;
    }

    public void setReview_count(String review_count) {
        this.review_count = review_count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCourt_type() {
        return court_type;
    }

    public void setCourt_type(String court_type) {
        this.court_type = court_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<String> getImages_array() {
        return images_array;
    }

    public void setImages_array(ArrayList<String> images_array) {
        this.images_array = images_array;
    }

    public ArrayList<String> getEquipmentes_array() {
        return equipmentes_array;
    }

    public void setEquipmentes_array(ArrayList<String> equipmentes_array) {
        this.equipmentes_array = equipmentes_array;
    }

    public ArrayList<String> getSpecs_array() {
        return specs_array;
    }

    public void setSpecs_array(ArrayList<String> specs_array) {
        this.specs_array = specs_array;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public boolean isEquipmentAvailable() {
        return isEquipmentAvailable;
    }

    public void setEquipmentAvailable(boolean equipmentAvailable) {
        isEquipmentAvailable = equipmentAvailable;
    }

    public String getEvent_Sports_Type() {
        return event_Sports_Type;
    }

    public void setEvent_Sports_Type(String event_Sports_Type) {
        this.event_Sports_Type = event_Sports_Type;
    }

    public String getCancel_policy_type() {
        return cancel_policy_type;
    }

    public void setCancel_policy_type(String cancel_policy_type) {
        this.cancel_policy_type = cancel_policy_type;
    }

    public String getCancellation_point() {
        return cancellation_point;
    }

    public void setCancellation_point(String cancellation_point) {
        this.cancellation_point = cancellation_point;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getSpace_type() {
        return space_type;
    }

    public void setSpace_type(String space_type) {
        this.space_type = space_type;
    }

    public String getCourt_min_players() {
        return court_min_players;
    }

    public void setCourt_min_players(String court_min_players) {
        this.court_min_players = court_min_players;
    }

    public String getCourt_space_desc() {
        return court_space_desc;
    }

    public void setCourt_space_desc(String court_space_desc) {
        this.court_space_desc = court_space_desc;
    }
    public ArrayList<String> getAdditional_space_desc_array() {
        return additional_space_desc_array;
    }

    public void setAdditional_space_desc_array(ArrayList<String> additional_space_desc_array) {
        this.additional_space_desc_array = additional_space_desc_array;
    }

    public ArrayList<String> getAdditional_equipmentes_array() {
        return additional_equipmentes_array;
    }

    public void setAdditional_equipmentes_array(ArrayList<String> additional_equipmentes_array) {
        this.additional_equipmentes_array = additional_equipmentes_array;
    }

    public ArrayList<String> getAdditional_amenities_array() {
        return additional_amenities_array;
    }

    public void setAdditional_amenities_array(ArrayList<String> additional_amenities_array) {
        this.additional_amenities_array = additional_amenities_array;
    }

    public String getAdditional_website() {
        return additional_website;
    }

    public void setAdditional_website(String additional_website) {
        this.additional_website = additional_website;
    }

    public String getAdditional_phone_number() {
        return additional_phone_number;
    }

    public void setAdditional_phone_number(String additional_phone_number) {
        this.additional_phone_number = additional_phone_number;
    }

    public String getAdditional_venue_rules() {
        return additional_venue_rules;
    }

    public void setAdditional_venue_rules(String additional_venue_rules) {
        this.additional_venue_rules = additional_venue_rules;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }
}
