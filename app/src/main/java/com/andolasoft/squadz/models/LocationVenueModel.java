package com.andolasoft.squadz.models;

/**
 * Created by SpNayak on 4/20/2017.
 */

public class LocationVenueModel {
    String venue_id;
    String venue_name;
    String venue_short_address;
    String venue_full_address;
    String latitude;
    String longitude;
    boolean venue_registered_status = false;

    public String get_Venue_Id() {
        return venue_id;
    }
    public void setVenue_Id(String venue_id) {
        this.venue_id = venue_id;
    }

    public String get_Venue_Name() {
        return venue_name;
    }
    public void setVenue_Name(String venue_name) {
        this.venue_name = venue_name;
    }

    public String get_Venue_Short_address() {
        return venue_short_address;
    }
    public void setVenue_Short_Address(String venue_short_address) {
        this.venue_short_address = venue_short_address;
    }

    public String get_Venue_Full_address() {
        return venue_full_address;
    }
    public void setVenue_Full_Address(String venue_full_address) {
        this.venue_full_address = venue_full_address;
    }

    public String get_Venue_Latitude() {
        return latitude;
    }
    public void setVenue_Latitude(String latitude) {
        this.latitude = latitude;
    }

    public String get_Venue_Longitude() {
        return longitude;
    }
    public void setVenue_Longitude(String longitude) {
        this.longitude = longitude;
    }

    public boolean get_Venue_Registered_status() {
        return venue_registered_status;
    }
    public void setVenue_Registered_status(boolean venue_registered_status) {
        this.venue_registered_status = venue_registered_status;
    }

}
