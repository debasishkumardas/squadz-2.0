package com.andolasoft.squadz.models;

import java.util.ArrayList;

/**
 * Created by SpNayak on 1/2/2017.
 */

public class VenueModel {

    private String venueId;
    private String venueName;
    private String venueImage;
    private String venueMiles;
    private String venueRatings;
    private String venueReviews;
    private String venuePrice;
    private String latitude;
    private String longitude;
    private String review_count;
    private String court_type;
    private String object_type;
    private String venueName_listing;

    private double miles;
    private boolean isFavourited = false;
    private ArrayList<String> images_array = new ArrayList<>();

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueImage() {
        return venueImage;
    }

    public void setVenueImage(String venueImage) {
        this.venueImage = venueImage;
    }

    public String getVenueMiles() {
        return venueMiles;
    }

    public void setVenueMiles(String venueMiles) {
        this.venueMiles = venueMiles;
    }

    public String getVenueRatings() {
        return venueRatings;
    }

    public void setVenueRatings(String venueRatings) {
        this.venueRatings = venueRatings;
    }

    public String getVenueReviews() {
        return venueReviews;
    }

    public void setVenueReviews(String venueReviews) {
        this.venueReviews = venueReviews;
    }

    public String getVenuePrice() {
        return venuePrice;
    }

    public void setVenuePrice(String venuePrice) {
        this.venuePrice = venuePrice;
    }

    public boolean isFavourited() {
        return isFavourited;
    }

    public void setFavourited(boolean favourited) {
        isFavourited = favourited;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getReview_count() {
        return review_count;
    }

    public void setReview_count(String review_count) {
        this.review_count = review_count;
    }

    public String getCourt_type() {
        return court_type;
    }

    public void setCourt_type(String court_type) {
        this.court_type = court_type;
    }

    public ArrayList<String> getImages_array() {
        return images_array;
    }

    public void setImages_array(ArrayList<String> images_array) {
        this.images_array = images_array;
    }

    public String getObject_type() {
        return object_type;
    }

    public void setObject_type(String object_type) {
        this.object_type = object_type;
    }

    public double getMiles() {
        return miles;
    }

    public void setMiles(double miles) {
        this.miles = miles;
    }

    public String getVenueName_listing() {
        return venueName_listing;
    }

    public void setVenueName_listing(String venueName_listing) {
        this.venueName_listing = venueName_listing;
    }
}
