package com.andolasoft.squadz.models;

import java.util.ArrayList;
import java.util.Comparator;

public class Model_Scheduler {

	private String pickup;
	private String sport;
	private String location;
	private String date;
	private String time;
	private String skilllevel;
	private int player_count;
	private String notes;
	private String gametype;
	private String latitude;
	private String longitude;
	private String address;
	private String start_time;
	private String end_time;
	private String miles;
	private String game_id;
	private String username;
	private String message;
	private String game_date;
	private String msg_id;
	private int favorite_count;
	private int comment_count;
	private boolean my_favorite;
	private int is_attending,max_player_nextgame;
	private String user_comment;
	private String comment_username;
	private String gamestatus;
	private String created_by;
	private String created_at;
	private String image;
	private String friend_id;
	private String friend_name;
	private String user_id;
	private String user_name;
	private String play_date,activity_type;
	private String max_player;
	private String first_name;
	private String last_name,game_date_attending;
	private String game_title;
	
	
	public Model_Scheduler() {
		
	}
	
	public String getPickup()
	{
		return pickup;
	}

	public void setPickup(String pickup) 
	{
		this.pickup = pickup;
	}

	public String getSport()
	{
		return sport;
	}

	public void setSport(String sport)
	{
		this.sport= sport;
	}
	public String getLocation()
	{	
		return location;
	}

	public void setLocation(String location) 
	{
		this.location= location;
	}
	public void setDate(String date)  
	{
		String str = null;
		
		if(date!=null && date.length()>1)
		{
			str=date.substring(0, date.length()-15);
		}
		this.date= str;
	}
		
	
	public String getDate()
	{	
		return date;
	}
	public void setTime(String time) 
	{
		this.time= time;
	}
	public String getTime()
	{	
		return time;
	}
	public void setSkilllevel(String skilllevel) 
	{
		this.skilllevel= skilllevel;
	}
	public String getSkilllevel()
	{	
		return skilllevel;
	}
	public void setPlayercount(int player_count) 
	{
		this.player_count= player_count;
	}
	public int getPlayercount()
	{	
		return player_count;
	}
	public void setNotes(String notes) 
	{
		this.notes= notes;
	}
	public String getNotes()
	{	
		return notes;
	}
	public void setGametype(String gametype) 
	{
		this.gametype= gametype;
	}
	public String getGametype()
	{	
		return gametype;
	}
	public void setLatitude(String latitude) 
	{
		this.latitude= latitude;
	}
	public String getLatitude()
	{	
		return latitude;
	}
	public void setLongitude(String longitude) 
	{
		this.longitude= longitude;
	}
	public String getLongitude()
	{	
		return longitude;
	}
	public void setAddress(String address) 
	{
		this.address= address;
	}
	public String getAddress()
	{	
		return address;
	}
	public void setStarttime(String start_time) 
	{
		this.start_time= start_time;
	}
	public String getStart_time()
	{	
		return start_time;
	}
	public void setEndtime(String end_time) 
	{
		this.end_time= end_time;
	}
	public String getEnd_time()
	{	
		return end_time;
	}
	public void setMiles(String miles) 
	{
		this.miles= miles;
	}
	public String getMiles()
	{	
		return miles;
	}
	public void setGame_id(String game_id) 
	{
		this.game_id= game_id;
		System.out.println("game id izzz"+game_id);
	}
	public String getGame_id()
	{	
		return game_id;
	}
	
	public void setUsername(String username) 
	{
		this.username= username;
		
	}
	public String getUsername()
	{	
		return username;
	}
	public void setMessage(String message) 
	{
		this.message= message;
		
	}
	public String getMessage()
	{	
		return message;
	}
	public void setGameDate(String game_date) 
	{
		String str = null;
		if(game_date!=null && game_date.length()>1)
		{
			str=game_date.substring(0, game_date.length()-10);
		}
		this.game_date= str;
		
	}
	public String getGameDate()
	{	
		return game_date;
	}
	
	public void setCommentcount(int  comment_count) 
	{
		this.comment_count= comment_count;
	}
	public int getCommentcount()
	{	
		return comment_count;
	}
	public void setFavoritecount(int  favorite_count) 
	{
		this.favorite_count= favorite_count;
		
	}
	public int getFavoritecount()
	{	
		return favorite_count;
	}
	public void setMessageId(String  msg_id) 
	{
		this.msg_id= msg_id;
		
	}
	public String getMessageId()
	{	
		return msg_id;
	}
	public void setMyfavorite(boolean  my_favorite) 
	{
		this.my_favorite= my_favorite;
		
	}
	public boolean getMyfavorite()
	{	
		return my_favorite;
	}
	public void setUsercomment(String  user_comment) 
	{
		this.user_comment= user_comment;
		
	}
	public String getUsercomment()
	{	
		return user_comment;
	}
	public void setCommentUsername(String  comment_username) 
	{
		this.comment_username= comment_username;
		
	}
	public String getCommentUsername()
	{	
		return comment_username;
	}
	
	public void setGame_Date(String date)  
	{
		/*if(date!=null && date.length()>1)
		{
			date=date.substring(0, date.length()-15);
			this.date= date;
		}*/
		
		this.date= date;
	}
	public String getGame_Date()  
	{
		return date;
	}
	public void setAttening(int  is_attending) 
	{
		this.is_attending= is_attending;
	}
	public int getAttending()
	{	
		return is_attending;
	}
	
	public void setHistoryGameDate(String game_date) 
	{
		this.game_date= game_date;
	}
	public String getHistoryGameDate()
	{	
		return game_date;
	}
	
	
	public void setGamestatus(String gamestatus) 
	{
		this.gamestatus= gamestatus;
	}
	public String getGamestatus()
	{	
		return gamestatus;
	}
	
	public void setCreated_by(String created_by) 
	{
		this.created_by= created_by;
	}
	public String getCreated_by()
	{	
		return created_by;
	}
	
	public void setCreated_at(String created_at) 
	{
		this.created_at= created_at;
	}
	public String getCreated_at()
	{	
		return created_at;
	}
	public void setImageUrl(String image) 
	{
		this.image = image;
	}
	public String getImageUrl()
	{	
		return image;
	}
	
	public void setFeedDate(String date)  
	{
		this.date= date;
	}
	
	public String getFeedDate()
	{	
		return date;
	}
	
	public void setFriendid(String friend_id)  
	{
		this.friend_id = friend_id;
	}
	
	public String getfriendid()
	{	
		return friend_id;
	}
	
	public void setUser_name(String user_name)  
	{
		this.user_name = user_name;
	}
	
	public String getUser_name()
	{	
		return user_name;
	}
	
	public void setUser_id(String user_id)  
	{
		this.user_id = user_id;
	}
	
	public String getUserid()
	{	
		return user_id;
	}
	
	public void setFriend_username(String friend_name)  
	{
		this.friend_name = friend_name;
	}
	
	public String getFriendusername()
	{	
		return friend_name;
	}
	
	public void setPlaydate(String play_date)  
	{
		System.out.println("Pay date izzzzpppp"+play_date);
		this.play_date = play_date;
	}
	
	public String getPlaydate()
	{	
		return play_date;
	}
	
	
	public void setActivitytype(String activity_type)  
	{
		this.activity_type = activity_type;
	}
	
	public String getActivitytype()
	{	
		return activity_type;
	}
	
	
	public void setMaxPlayer(String max_player)  
	{
		this.max_player = max_player;
	}
	
	public String getMaxPlayer()
	{	
		return max_player;
	}
	
	public void setFirstName(String firstname)  
	{
		this.first_name = firstname;
	}
	
	public String getFirstname()
	{	
		return first_name;
	}
	
	
	public void setLastName(String lastname)  
	{
		this.last_name = lastname;
	}
	
	public String getLastName()
	{	
		return last_name;
	}
	
	public void setActivityDate_attending(String game_date_attending) 
	{
		
		this.game_date_attending= game_date_attending;
		
	}
	public String getActivityDate_attending()
	{	
		return game_date_attending;
	}

	
	
	public static class CompareUserName implements Comparator< Model_Scheduler >
    {
        @Override
        public int compare( Model_Scheduler o1,  Model_Scheduler o2 )
        {
            return o1.friend_name.compareTo( o2.friend_name );
        }
    }

    
    public void setActivityDate(String game_date) 
	{
		
		this.game_date= game_date;
		
	}
	public String getActivityDate()
	{	
		return game_date;
	}
	
	public void setMaxPlayer_nextgame(int max_player)  
	{
		this.max_player_nextgame = max_player;
	}
	
	public int getMaxPlayer_nextgame()
	{	
		return max_player_nextgame;
	}

	public void setGameTitle(String game_title)  
	{
		this.game_title = game_title;
	}
	
	public String getGame_title()
	{	
		return game_title;
	}

}
