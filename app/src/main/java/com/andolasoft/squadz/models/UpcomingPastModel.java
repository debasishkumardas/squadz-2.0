package com.andolasoft.squadz.models;

import java.util.ArrayList;

/**
 * Created by SpNayak on 1/2/2017.
 */

public class UpcomingPastModel {

    private String event_Id;
    private String event_Image;
    private String event_Name;
    private String event_Creator_Name;
    private String event_Creator_Image;
    private String event_Sports_Type;
    private String event_Sports_Type_Image;
    private String event_Ratings;
    private String event_Reviews;
    private String event_Miles;
    private String event_Price;
    private String event_Minimum_Price;
    private String event_Total_Participants;
    private String event_Spots;
    private String event_Minimum_Amount_Text;
    private String event_Date;
    private String event_Time;
    private String latitude;
    private String longitude;
    private boolean is_Event_Favourited = false;
    private String court_type;
    private String description;
    private String address;
    private String city;
    private String state;
    private String country;
    private String capacity;
    private String skill_level;
    private String eventDate;
    private ArrayList<String> images_array = new ArrayList<>();
    private String eventStartTime;
    private String eventEndTime;
    private String creator_name;
    private String creator_user_id;
    private String event_accepted_Participants;
    private String event_court_name;
    private String visibility;
    private String venue_id;
    private boolean isFavorite;
    private boolean isEquipmentAvailable;
    private int image;
    private String object_Type;
    private String event_total_price;
    private String note;
    private String list_Id;


    public String getEvent_Id() {
        return event_Id;
    }

    public void setEvent_Id(String event_Id) {
        this.event_Id = event_Id;
    }

    public String getEvent_Image() {
        return event_Image;
    }

    public void setEvent_Image(String event_Image) {
        this.event_Image = event_Image;
    }

    public String getEvent_Name() {
        return event_Name;
    }

    public void setEvent_Name(String event_Name) {
        this.event_Name = event_Name;
    }

    public String getEvent_Creator_Name() {
        return event_Creator_Name;
    }

    public void setEvent_Creator_Name(String event_Creator_Name) {
        this.event_Creator_Name = event_Creator_Name;
    }

    public String getEvent_Creator_Image() {
        return event_Creator_Image;
    }

    public void setEvent_Creator_Image(String event_Creator_Image) {
        this.event_Creator_Image = event_Creator_Image;
    }

    public String getEvent_Sports_Type() {
        return event_Sports_Type;
    }

    public void setEvent_Sports_Type(String event_Sports_Type) {
        this.event_Sports_Type = event_Sports_Type;
    }

    public String getEvent_Sports_Type_Image() {
        return event_Sports_Type_Image;
    }

    public void setEvent_Sports_Type_Image(String event_Sports_Type_Image) {
        this.event_Sports_Type_Image = event_Sports_Type_Image;
    }

    public String getEvent_Ratings() {
        return event_Ratings;
    }

    public void setEvent_Ratings(String event_Ratings) {
        this.event_Ratings = event_Ratings;
    }

    public String getEvent_Reviews() {
        return event_Reviews;
    }

    public void setEvent_Reviews(String event_Reviews) {
        this.event_Reviews = event_Reviews;
    }

    public String getEvent_Miles() {
        return event_Miles;
    }

    public void setEvent_Miles(String event_Miles) {
        this.event_Miles = event_Miles;
    }

    public String getEvent_Price() {
        return event_Price;
    }

    public void setEvent_Price(String event_Price) {
        this.event_Price = event_Price;
    }

    public String getEvent_Total_Participants() {
        return event_Total_Participants;
    }

    public void setEvent_Total_Participants(String event_Total_Participants) {
        this.event_Total_Participants = event_Total_Participants;
    }

    public String getEvent_Spots() {
        return event_Spots;
    }

    public void setEvent_Spots(String event_Spots) {
        this.event_Spots = event_Spots;
    }

    public String getEvent_Minimum_Amount_Text() {
        return event_Minimum_Amount_Text;
    }

    public void setEvent_Minimum_Amount_Text(String event_Minimum_Amount_Text) {
        this.event_Minimum_Amount_Text = event_Minimum_Amount_Text;
    }

    public String getEvent_Date() {
        return event_Date;
    }

    public void setEvent_Date(String event_Date) {
        this.event_Date = event_Date;
    }

    public String getEvent_Time() {
        return event_Time;
    }

    public void setEvent_Time(String event_Time) {
        this.event_Time = event_Time;
    }

    public boolean is_Event_Favourited() {
        return is_Event_Favourited;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setIs_Event_Favourited(boolean is_Event_Favourited) {
        this.is_Event_Favourited = is_Event_Favourited;
    }

    public String getCourt_type() {
        return court_type;
    }

    public void setCourt_type(String court_type) {
        this.court_type = court_type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }


    public ArrayList<String> getImages_array() {
        return images_array;
    }

    public void setImages_array(ArrayList<String> images_array) {
        this.images_array = images_array;
    }

    public void setSkill_level(String skill_level) {
        this.skill_level = skill_level;
    }

    public String getSkill_level() {

        return skill_level;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getCreator_name() {
        return creator_name;
    }

    public void setCreator_name(String creator_name) {
        this.creator_name = creator_name;
    }

    public String getCreator_user_id() {
        return creator_user_id;
    }

    public void setCreator_user_id(String creator_user_id) {
        this.creator_user_id = creator_user_id;
    }

    public String getEvent_accepted_Participants() {
        return event_accepted_Participants;
    }

    public void setEvent_accepted_Participants(String event_accepted_Participants) {
        this.event_accepted_Participants = event_accepted_Participants;
    }

    public String getEvent_court_name() {
        return event_court_name;
    }

    public void setEvent_court_name(String event_court_name) {
        this.event_court_name = event_court_name;
    }


    public String getEvent_Minimum_Price() {
        return event_Minimum_Price;
    }

    public void setEvent_Minimum_Price(String event_Minimum_Price) {
        this.event_Minimum_Price = event_Minimum_Price;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isEquipmentAvailable() {
        return isEquipmentAvailable;
    }

    public void setEquipmentAvailable(boolean equipmentAvailable) {
        isEquipmentAvailable = equipmentAvailable;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(String venue_id) {
        this.venue_id = venue_id;
    }
    public String getObject_Type() {
        return object_Type;
    }

    public void setObject_Type(String object_Type) {
        this.object_Type = object_Type;
    }
    public String getEvent_Total_Price() {
        return event_total_price;
    }

    public void setEvent_Total_Price(String event_total_price) {
        this.event_total_price = event_total_price;
    }
    public String getList_Id() {
        return list_Id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;

    }
    public void setList_Id(String list_Id) {
        this.list_Id = list_Id;
    }
}
