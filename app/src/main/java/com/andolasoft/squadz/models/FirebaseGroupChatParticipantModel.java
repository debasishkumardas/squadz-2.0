package com.andolasoft.squadz.models;

/**
 * Created by Debasish Kumar Das on 3/6/2017.
 */
public class FirebaseGroupChatParticipantModel {

    String user_id;
    String username;
    String image;

    public FirebaseGroupChatParticipantModel(){

    }

    public FirebaseGroupChatParticipantModel(String userId,
                                             String userName,
                                             String imageURL) {
        this.user_id = userId;
        this.username = userName;
        this.image = imageURL;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
