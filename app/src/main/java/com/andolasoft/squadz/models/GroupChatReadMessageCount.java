package com.andolasoft.squadz.models;

/**
 * Created by Debasish Kumar Das on 4/8/2017.
 */
public class GroupChatReadMessageCount {

    int readMessageCount = 0;

    public GroupChatReadMessageCount() {
    }


    public GroupChatReadMessageCount(int readMessageCount) {
        this.readMessageCount = readMessageCount;
    }

    public int getReadMessageCount() {
        return readMessageCount;
    }

    public void setReadMessageCount(int readMessageCount) {
        this.readMessageCount = readMessageCount;
    }
}
