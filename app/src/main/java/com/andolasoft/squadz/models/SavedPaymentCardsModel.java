package com.andolasoft.squadz.models;

/**
 * Created by Debasish Kumar Das on 2/6/2017.
 */
public class SavedPaymentCardsModel {

    String cardBrand;
    String country;
    String funding;
    String lastfour;
    String custId;
    String cardId;
    int primary_card_status;

    public SavedPaymentCardsModel(String cardBrand, String country,
                                  String funding, String lastfour,
                                  String custId, String cardId, int pimary_card_status) {
        this.cardBrand = cardBrand;
        this.country = country;
        this.funding = funding;
        this.lastfour = lastfour;
        this.custId = custId;
        this.cardId = cardId;
        this.primary_card_status = pimary_card_status;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFunding() {
        return funding;
    }

    public void setFunding(String funding) {
        this.funding = funding;
    }

    public String getLastfour() {
        return lastfour;
    }

    public void setLastfour(String lastfour) {
        this.lastfour = lastfour;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public int getPrimary_card_status() {
        return primary_card_status;
    }
}
