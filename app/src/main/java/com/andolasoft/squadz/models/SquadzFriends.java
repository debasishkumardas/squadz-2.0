package com.andolasoft.squadz.models;

public class SquadzFriends {
	private String name;
	private String phoneNo;
	private String username, userid, facebook_user_id, facebook_image_url,
			squadz_image_url, request_status, facebook_friendship_status;
	private boolean friendship_status;
	private boolean isMatch;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String geusername() {
		return username;
	}

	public void setusername(String username) {
		this.username = username;
	}

	public boolean gefriendship_status() {
		return friendship_status;
	}

	public void setfriendship_status(boolean friendship_status) {
		this.friendship_status = friendship_status;
	}

	public boolean getMatch() {
		return isMatch;
	}

	public void setMatch(boolean isMatch) {
		this.isMatch = isMatch;
	}

	public String getUserid() {
		return userid;
	}

	public void setuserId(String userid) {
		this.userid = userid;
	}

	public String getFacebookUserid() {
		return facebook_user_id;
	}

	public void setFacebookuserId(String facebook_user_id) {
		this.facebook_user_id = facebook_user_id;
	}

	public String getFacebookImageUrl() {
		return facebook_image_url;
	}

	public void setFacebookImageUrl(String facebook_image_url) {
		this.facebook_image_url = facebook_image_url;
	}

	public String getSquadzImageUrl() {
		return squadz_image_url;
	}

	public void setSquadzImageUrl(String squadz_image_url) {
		this.squadz_image_url = squadz_image_url;
	}

	public String getRequest_status() {
		return request_status;
	}

	public void setRequest_status(String request_status) {
		this.request_status = request_status;
	}

	public String getFacebookfriendship_status() {
		return facebook_friendship_status;
	}

	public void setFacebookfriendship_status(String friendship_status) {
		this.facebook_friendship_status = friendship_status;
	}

}
