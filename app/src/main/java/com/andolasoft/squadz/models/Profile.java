package com.andolasoft.squadz.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Profile {

    private String userId;
    private String username;
    private String phoneNumber;
    private String name;
    private String firstName;
    private String lastName;
    private String email;
    private String birthDate;
    private String image;
    private String aboutMe;
    private String sport;
    private String skillLevel;
   // private List<SecondarySport> secondarySports = null;
    private Integer rewards;
    private String streetName;
    private String locality;
    private String provinceAbbreviation;
    private String postalCode;
    private String country;
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public Profile(){

    }

    public Profile(String userId, String username, String phoneNumber,
                   String name, String firstName, String lastName,
                   String email, String birthDate, String image,
                   String aboutMe, String sport, String skillLevel,
                   Integer rewards, String streetName,
                   String locality, String provinceAbbreviation,
                   String postalCode, String country) {
        this.userId = userId;
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
        this.image = image;
        this.aboutMe = aboutMe;
        this.sport = sport;
        this.skillLevel = skillLevel;
        this.rewards = rewards;
        this.streetName = streetName;
        this.locality = locality;
        this.provinceAbbreviation = provinceAbbreviation;
        this.postalCode = postalCode;
        this.country = country;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getSkillLevel() {
        return skillLevel;
    }

    public void setSkillLevel(String skillLevel) {
        this.skillLevel = skillLevel;
    }

   /* public List<SecondarySport> getSecondarySports() {
        return secondarySports;
    }

    public void setSecondarySports(List<SecondarySport> secondarySports) {
        this.secondarySports = secondarySports;
    }*/

    public Integer getRewards() {
        return rewards;
    }

    public void setRewards(Integer rewards) {
        this.rewards = rewards;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getProvinceAbbreviation() {
        return provinceAbbreviation;
    }

    public void setProvinceAbbreviation(String provinceAbbreviation) {
        this.provinceAbbreviation = provinceAbbreviation;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

   /* public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }*/

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}