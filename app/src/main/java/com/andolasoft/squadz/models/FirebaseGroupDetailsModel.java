package com.andolasoft.squadz.models;

import java.util.ArrayList;

/**
 * Created by Debasish Kumar Das on 3/6/2017.
 */
public class FirebaseGroupDetailsModel {

    String channelId;
    int date;
    String groupId;
    String groupName;
    String imageUrl;
    int readMessageCount;
    ArrayList<FirebaseGroupChatParticipantModel> participants;
    ArrayList<String> offLineUsers;

    public FirebaseGroupDetailsModel(){

    }


    public FirebaseGroupDetailsModel(String channelId, int date, String groupId,
                                     String groupName,
                                     String imageUrl,
                                     ArrayList<FirebaseGroupChatParticipantModel> participants) {
        this.channelId = channelId;
        this.date = date;
        this.groupId = groupId;
        this.groupName = groupName;
        this.imageUrl = imageUrl;
        this.participants = participants;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<FirebaseGroupChatParticipantModel> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<FirebaseGroupChatParticipantModel> participants) {
        this.participants = participants;
    }

    public int getReadMessageCount() {
        return readMessageCount;
    }

    public void setReadMessageCount(int readMessageCount) {
        this.readMessageCount = readMessageCount;
    }

    public ArrayList<String> getOffLineUsers() {
        return offLineUsers;
    }

    public void setOffLineUsers(ArrayList<String> offLineUsers) {
        this.offLineUsers = offLineUsers;
    }
}
