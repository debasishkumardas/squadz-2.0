package com.andolasoft.squadz.models;

/**
 * Created by Debasish Kumar Das on 3/3/2017.
 */
public class ChatModel {
    String chatUserName;
    String senderId;
    String recieverID;
    String channelId;
    String text;
    String msgTimeStamp;
    String imageUrl;
    String senderImageUrl;
    String chatType;
    int date;
    boolean isSelf;

    public ChatModel(){

    }

    public ChatModel(String chatUserName, String senderId,
                     String recieverID, String channelId, String text,
                     String msgTimeStamp, String imageUrl, String chatType,
                     int date) {
        this.chatUserName = chatUserName;
        this.senderId = senderId;
        this.recieverID = recieverID;
        this.channelId = channelId;
        this.text = text;
        this.msgTimeStamp = msgTimeStamp;
        this.imageUrl = imageUrl;
        this.chatType = chatType;
        this.date = date;
    }


    public String getChatUserName() {
        return chatUserName;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getRecieverID() {
        return recieverID;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getText() {
        return text;
    }

    public String getMsgTimeStamp() {
        return msgTimeStamp;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getChatType() {
        return chatType;
    }

    public int getDate() {
        return date;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public void setRecieverID(String recieverID) {
        this.recieverID = recieverID;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setMsgTimeStamp(String msgTimeStamp) {
        this.msgTimeStamp = msgTimeStamp;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public void setDate(int date) {
        this.date = date;
    }


    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean self) {
        isSelf = self;
    }

    public String getSenderImageUrl() {
        return senderImageUrl;
    }

    public void setSenderImageUrl(String senderImageUrl) {
        this.senderImageUrl = senderImageUrl;
    }
}




