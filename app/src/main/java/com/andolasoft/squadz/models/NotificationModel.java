package com.andolasoft.squadz.models;

/**
 * Created by SpNayak on 3/3/2017.
 */

public class NotificationModel {

    String user_id;
    String user_name;
    String user_profile_image;
    String notification_type;
    String game_name;
    String created_at;
    String partitipants;
    String visibility;
    String game_id;
    String created_at_time_milisecond;
    String first_name;
    String last_name;
    String status="";
    String rewards;
    String phone_number;
    String game_date;
    String per_player_price;
    String game_final_name;
    String game_time;
    String court_name;
    String object_type;
    boolean is_registered_event = true;
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }

    public String getGame_name() {
        return game_name;
    }

    public void setGame_name(String game_name) {
        this.game_name = game_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getPartitipants() {
        return partitipants;
    }

    public void setPartitipants(String partitipants) {
        this.partitipants = partitipants;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getGame_id() {
        return game_id;
    }

    public void setGame_id(String game_id) {
        this.game_id = game_id;
    }

    public String getCreated_at_time_milisecond() {
        return created_at_time_milisecond;
    }

    public void setCreated_at_time_milisecond(String created_at_time_milisecond) {
        this.created_at_time_milisecond = created_at_time_milisecond;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
    public String getGame_Date() {
        return game_date;
    }

    public void setGame_Date(String game_date) {
        this.game_date = game_date;
    }

    public String getPer_player_price() {
        return per_player_price;
    }

    public void setPer_player_price(String per_player_price) {
        this.per_player_price = per_player_price;
    }

    public String getGame_final_name() {
        return game_final_name;
    }

    public void setGame_final_name(String game_final_name) {
        this.game_final_name = game_final_name;
    }

    public String getGame_time() {
        return game_time;
    }

    public void setGame_time(String game_time) {
        this.game_time = game_time;
    }

    public boolean is_registered_event() {
        return is_registered_event;
    }

    public void setIs_registered_event(boolean is_registered_event) {
        this.is_registered_event = is_registered_event;
    }

    public String getCourt_name() {
        return court_name;
    }

    public void setCourt_name(String court_name) {
        this.court_name = court_name;
    }

    public String getObject_type() {
        return object_type;
    }

    public void setObject_type(String object_type) {
        this.object_type = object_type;
    }
}
