package com.andolasoft.squadz.models;

/**
 * Created by SpNayak on 2/15/2017.
 */

public class AvailabilityTimeSlotsModel {
    String start_end_time;
    boolean is_available = true;
    int time_position = 0;

    public String getStart_end_time() {
        return start_end_time;
    }

    public void setStart_end_time(String start_end_time) {
        this.start_end_time = start_end_time;
    }

    public boolean is_available() {
        return is_available;
    }

    public void setIs_available(boolean is_available) {
        this.is_available = is_available;
    }

    public int getTime_position() {
        return time_position;
    }

    public void setTime_position(int time_position) {
        this.time_position = time_position;
    }
}
