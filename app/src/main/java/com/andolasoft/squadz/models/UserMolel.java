package com.andolasoft.squadz.models;

/**
 * Created by Debasish Kumar Das on 1/23/2017.
 */
public class UserMolel {

    private String userName;
    private String userFullName;
    private String userFirstName;
    private String userLastName;
    private String userProfileImage;
    private String userId;
    private String userPhoneNumber;
    private String userSport;
    private String userStatus;


    public UserMolel(){}
    public UserMolel(String userName, String userFullName,
                     String userFirstName, String userLastName,
                     String userProfileImage, String userId,
                     String userPhoneNumber, String userSport,
                     String userStatus) {
        this.userName = userName;
        this.userFullName = userFullName;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userProfileImage = userProfileImage;
        this.userId = userId;
        this.userPhoneNumber = userPhoneNumber;
        this.userSport = userSport;
        this.userStatus = userStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getUserSport() {
        return userSport;
    }

    public void setUserSport(String userSport) {
        this.userSport = userSport;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }
}
