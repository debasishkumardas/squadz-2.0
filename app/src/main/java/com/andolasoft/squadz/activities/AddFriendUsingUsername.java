package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.SeatchPlayerAdaper;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddFriendUsingUsername extends AppCompatActivity {

    @InjectView(R.id.titletxt)
    TextView tv_pageTitle;
    @InjectView(R.id.back_icon_summary)
    ImageView iv_back;
    @InjectView(R.id.et_search)
    EditText et_search_player;
    @InjectView(R.id.searchicon)
    ImageView iv_search;
    @InjectView(R.id.list)
    ListView friends_list;
    @InjectView(R.id.nosearchresult)
    TextView tv_no_friends;
    @InjectView(R.id.progressBar)
    ProgressBar mProgressLoader;
    ProgressDialog dialog;
    SharedPreferences preferences;
    String auth_token, search_query;
    SeatchPlayerAdaper searchPlayerAdapter;
    ArrayList<UserMolel> searchPlayerArray;
    SnackBar snackbar = new SnackBar(AddFriendUsingUsername.this);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend_using_username);
        ButterKnife.inject(this);

        //Handle the progress loader state
        setProgressDialogVisibility(false);

        //Setting the page title
        setPageTitle();

        //Getting the auth token
        getAuthToken();

        //Initializing the array
        initializeArray();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddFriendUsingUsername.this.finish();
            }
        });

        et_search_player.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                search_query = s.toString();
                searchPlayerArray.clear();
                if(s.toString().length() == 0){
                    int size = searchPlayerArray.size();
                    Log.d("Size",String.valueOf(size));
                    searchPlayerAdapter = new SeatchPlayerAdaper(
                            AddFriendUsingUsername.this, searchPlayerArray);
                    friends_list.setAdapter(searchPlayerAdapter);
                    searchPlayerAdapter.notifyDataSetChanged();
                    tv_no_friends.setVisibility(View.VISIBLE);
                }else{
                    getNearByFriends(s.toString());
                }



            }
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });
    }


    public void setPageTitle(){
        tv_pageTitle.setText("Search Player");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AddFriendUsingUsername.this.finish();
    }

    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String auth_token, String search_query){
       //http://54.164.124.188/api/v1/search?94341605c7a2df4b1776d08478eb69e0&q=har
//        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"/search?").newBuilder();
        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"/search?").newBuilder();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"search?").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("q", search_query);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(AddFriendUsingUsername.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }


    public void getNearByFriends(String searchQuery) {
        //Handling the loader state
        setProgressDialogVisibility(true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(auth_token,searchQuery);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(AddFriendUsingUsername.this, false);
                if(!ApplicationUtility.isConnected(AddFriendUsingUsername.this)){
                    ApplicationUtility.displayNoInternetMessage(AddFriendUsingUsername.this);
                }else{
                    ApplicationUtility.displayErrorMessage(AddFriendUsingUsername.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    searchPlayerArray = new ArrayList<UserMolel>();
                    JSONObject nearby_jsonObject = new JSONObject(responseData);
                    JSONArray jsonArray = nearby_jsonObject.getJSONArray("all_users");
                    if (jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                JSONObject nearbyJson = jsonArray.getJSONObject(i);
                                String user_id = nearbyJson.getString("user_id");
                                String userName = nearbyJson.getString("username");
                                String firstName = nearbyJson.getString("first_name");
                                String lastName = nearbyJson.getString("last_name");
                                String usersport =nearbyJson.getString("sport");
                                String userImage =nearbyJson.getString("image");

                                //Setting all the data in the model
                                UserMolel userModel = new UserMolel(userName,"",firstName,lastName,
                                        userImage, user_id, "", usersport, "");
                                searchPlayerArray.add(userModel);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        setProgressDialogVisibility(false);
                        if (searchPlayerArray.size() != 0 &&
                                search_query.length() != 0) {
                            //searchPlayerAdapter.notifyDataSetChanged();
                            searchPlayerAdapter = new SeatchPlayerAdaper(
                                    AddFriendUsingUsername.this, searchPlayerArray);
                            friends_list.setAdapter(searchPlayerAdapter);
                            searchPlayerAdapter.notifyDataSetChanged();
                           // notifyAdapter();
                            tv_no_friends.setVisibility(View.GONE);
                        } else{
                            searchPlayerArray.clear();
                            searchPlayerAdapter = new SeatchPlayerAdaper(
                                    AddFriendUsingUsername.this, searchPlayerArray);
                            friends_list.setAdapter(searchPlayerAdapter);
                            searchPlayerAdapter.notifyDataSetChanged();
                            tv_no_friends.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });
    }

    /**
     * Function to set the Progress dialog visibility
     *
     * @param status
     */
    public void setProgressDialogVisibility(boolean status) {

        if (status) {
            mProgressLoader.setVisibility(View.VISIBLE);
        } else {
            mProgressLoader.setVisibility(View.GONE);
        }
    }

    /**
     * Getting the auth token
     */
    public void getAuthToken() {
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        auth_token = preferences.getString("auth_token", "");
    }

    //Initializing the array
    public void initializeArray(){
        searchPlayerArray = new ArrayList<>();
    }

    private void notifyAdapter()  {
        AddFriendUsingUsername.this.runOnUiThread(new Runnable()  {
            public void run() {
                //friends_list.setAdapter(null);
                if(searchPlayerAdapter != null) {
                    searchPlayerAdapter.notifyDataSetChanged();
                }
            }
        });
    }
}
