package com.andolasoft.squadz.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.Model_Scheduler;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ListUtils;
import com.andolasoft.squadz.views.adapters.MySquadzListAdapter;
import com.andolasoft.squadz.views.adapters.TeammateListAdapter;

import java.util.ArrayList;

public class AddFriendToChat extends Activity {

	static ListView squadz_list;
	static ListView team_list;
	ArrayList<String> squadz_list_arr, teammates_list_arr;
	MySquadzListAdapter my_squadz_list_adapter;
	static TeammateListAdapter teammate_adapter;
	EditText search_edt;
	View mCustomView;
	ImageView back_btn;
	DatabaseAdapter db;
	String auth_token, login_user_id;
	ArrayList<Model_Scheduler> allfriend_list;
    public static TextView btnDone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_listfilter);
		AppConstants.isChannedCreated = false;
		squadz_list = (ListView) findViewById(R.id.mysquadz_view);
		team_list = (ListView) findViewById(R.id.teammate_view);
		back_btn = (ImageView) findViewById(R.id.nav_icon);
		search_edt = (EditText) findViewById(R.id.search_edt);
        btnDone = (TextView) findViewById(R.id.done_btn);
        if(AppConstants.isUserGointToUpdateTheGroup){
            btnDone.setText("Update");
        }

		// GETTING AUTH TOKEN FROM SHARED PREF
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(AddFriendToChat.this);
		auth_token = preferences.getString("auth_token", null);
		login_user_id = preferences.getString("loginuser_id", null);
		db = new DatabaseAdapter(AddFriendToChat.this);
		search_edt.requestFocus();
		/**
		 * Set My Squadz list Items
		 */
		setMySquadzListData();

		getSelectedFriendFromDB();

		search_edt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				my_squadz_list_adapter.getFilter().filter(s.toString());
				squadz_list.setTextFilterEnabled(true);

				my_squadz_list_adapter.notifyDataSetChanged();

				teammate_adapter.getFilter().filter(s.toString());
				team_list.setTextFilterEnabled(true);

				teammate_adapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				if (arg0.length() == 0) {
					my_squadz_list_adapter = new MySquadzListAdapter(
							AddFriendToChat.this, squadz_list_arr);
					squadz_list.setAdapter(my_squadz_list_adapter);
                    ListUtils.setDynamicHeight(squadz_list);

					teammate_adapter = new TeammateListAdapter(AddFriendToChat.this,
							allfriend_list);
					team_list.setAdapter(teammate_adapter);
                    ListUtils.setDynamicHeight(team_list);
				}
			}
		});

		back_btn.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
                handleBackButtonPress();
                AppConstants.isUserGointToUpdateTheGroup = false;
				finish();

			}
		});

	}


    public void redirectUser(){
        Intent intent = new Intent(AddFriendToChat.this, ChatActivity.class);
        startActivity(intent);
        AddFriendToChat.this.finish();
    }

    /**
     * Function responsible for handling the dynamic height of the list view
     */
	public static void updateListviewHeight() {
		ListUtils.setDynamicHeight(squadz_list);
        ListUtils.setDynamicHeight(team_list);

	}

    /**
     * Function responsible for handling My squadz data
     */
	public void setMySquadzListData() {
		db.open();
		squadz_list_arr = new ArrayList<String>();
		Cursor getAllSportsCursor = db.getcurrentsportMyTeam("ALl", auth_token);
		getAllSportsCursor.moveToFirst();
		if (getAllSportsCursor.getCount() != 0) {
			for (int i = 0; i < getAllSportsCursor.getCount(); i++) {
				String selected_team_name = (getAllSportsCursor
						.getString(getAllSportsCursor.getColumnIndex("name")));
				String selected_sport_id = (getAllSportsCursor
						.getString(getAllSportsCursor
								.getColumnIndex("sport_id")));
				String selected_custom_sport_name = (getAllSportsCursor
						.getString(getAllSportsCursor
								.getColumnIndex("custom_sport_name")));
				String selected_team_id = (getAllSportsCursor
						.getString(getAllSportsCursor.getColumnIndex("team_id")));
				String name = db.getcurrentSportName(selected_sport_id);
				if (selected_custom_sport_name.equalsIgnoreCase("null")
						&& name != null) {
					squadz_list_arr.add(selected_team_name + "###" + name
							+ "###" + selected_sport_id + "###"
							+ selected_team_id);
				} else {
					squadz_list_arr.add(selected_team_name + "###"
							+ selected_custom_sport_name + "###"
							+ selected_sport_id + "###" + selected_team_id);
				}
				getAllSportsCursor.moveToNext();
			}
		}

        //Setting the adapter
		my_squadz_list_adapter = new MySquadzListAdapter(AddFriendToChat.this,
				squadz_list_arr);
		squadz_list.setAdapter(my_squadz_list_adapter);
        ListUtils.setDynamicHeight(squadz_list);
	}

    /**
     * Getting all the friends from database and setting in the list view
     */
	public void getSelectedFriendFromDB() {
		db.open();
		allfriend_list = new ArrayList<Model_Scheduler>();
		Model_Scheduler scheduler;
		Cursor cursor = db.getcurrentsportfrienlistRoster("All", auth_token);
		cursor.moveToFirst();
		if (cursor.getCount() != 0) {
			do {
				scheduler = new Model_Scheduler();
				scheduler.setFriendid(cursor.getString(cursor
						.getColumnIndex("user_id")));
				scheduler.setFriend_username(cursor.getString(cursor
						.getColumnIndex("username")));
				scheduler.setSport(cursor.getString(cursor
						.getColumnIndex("sport")));
				scheduler.setFirstName(cursor.getString(cursor
						.getColumnIndex("firstname")));
				scheduler.setLastName(cursor.getString(cursor
						.getColumnIndex("lastname")));
				scheduler.setImageUrl(cursor.getString(cursor
						.getColumnIndex("image")));
				allfriend_list.add(scheduler);
			} while (cursor.moveToNext());
		}

		teammate_adapter = new TeammateListAdapter(AddFriendToChat.this,
				allfriend_list);
		team_list.setAdapter(teammate_adapter);
        ListUtils.setDynamicHeight(team_list);

		// deactivate and closing cursor
		cursor.deactivate();
		cursor.close();

		// Closing db connection
		db.close();
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		AppConstants.isUserGointToUpdateTheGroup = false;
        handleBackButtonPress();
		AddFriendToChat.this.finish();
	}

    /**
     * Function responsible for handling the back button presses
     */
    public void handleBackButtonPress(){
        if(AddFriendToChat.btnDone.getText().toString() != null &&
                AddFriendToChat.btnDone.getText().toString().equalsIgnoreCase("Update")){
            AppConstants.isAnyThingUpdated = true;
        }
    }
}
