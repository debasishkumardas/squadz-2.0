package com.andolasoft.squadz.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.Manifest;
import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.ChatFragmnet;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.ChatConversationModel;
import com.andolasoft.squadz.models.ChatModel;
import com.andolasoft.squadz.models.FirebaseBadgeCountModel;
import com.andolasoft.squadz.models.FirebaseGroupChatParticipantModel;
import com.andolasoft.squadz.models.FirebaseGroupDetailsModel;
import com.andolasoft.squadz.models.FriendsModel;
import com.andolasoft.squadz.models.GroupChatReadMessageCount;
import com.andolasoft.squadz.models.TeamsModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.FriendsAdapter;
import com.andolasoft.squadz.views.adapters.TeamsAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TeamMates extends AppCompatActivity {

    @InjectView(R.id.back_icon_venue_mapview_layout)
    RelativeLayout layBack;
    @InjectView(R.id.spinner_teammates_map_list)
    Spinner sportPicker;
    @InjectView(R.id.book_icon_venue)
    ImageView ivAddPlayer;
    @InjectView(R.id.show_myteams)
    Button btnMyTeammates;
    @InjectView(R.id.show_FriendRequest)
    Button btnMySquadz;
    @InjectView(R.id.friend_request_list)
    RecyclerView friendsList;
    @InjectView(R.id.mySquadz_list)
    SwipeMenuListView teamList;
    @InjectView(R.id.swipe_refresh_layout_teammates)
    SwipeRefreshLayout swipe_refresh_layout_teammates;
    DatabaseAdapter db;
    ArrayList<String> sports_name_array, sports_id_array;
    HashMap<String, String> map_sports_name;
    ArrayList<FriendsModel> friendArray;
    ArrayList<TeamsModel> teamsArray;
    FriendsAdapter friendsAdapter;
    public boolean isSquadzButtonClicked = false;
    String sportName;
    TeamsAdapter teamAdapter;
    Dialog dialog;
    SharedPreferences pref;
    String auth_token, apiStatus;
    public static String SELECTED_SPORT_NAME;
    MaterialDialog mMaterialDialog;
    String type = "My_Teammates", logged_in_user_id = null;
    ArrayList<String> groupIdsArray = null;
    public static boolean isGroupChaneelIdCreated = false;
    int pageredirectionsCount = 0;
    String err_msg = null;
    String msg = null;
    SnackBar snackBar;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_mates);
        ButterKnife.inject(this);

        //Added Font to the Screen
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        snackBar = new SnackBar(this);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        auth_token = pref.getString("auth_token", "");
        logged_in_user_id = pref.getString("loginuser_id", "");

        //Initializing all the necessary classes
        initializeClass();

        //Handling button background and color
        changeButtonBackground(btnMyTeammates, btnMySquadz,type);

        //Setting up the sports in te spinner
        getSportsName();

        btnMyTeammates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSquadzButtonClicked = false;

                //Kept the click type
                type = "My_Teammates";
                //Setting the add button Image
                setImageBackGround(isSquadzButtonClicked);

                //Handling the list views visibility
                handleViewVisibility(true);

                //Clearing the array content
                if (friendArray != null) {
                    friendArray.clear();
                }
                if (sportName != null &&
                        sportName.equalsIgnoreCase("All")) {
                    //Get all friends from database
                    getAllFriendFromDB();
                } else {
                    //Get all Filtered friends from database
                    getAllFriendFromDBWithFilter(sportName);
                }

                //Refreshing the List view
                setDatainListVIew();


                //Handling the Button Colors
                changeButtonBackground(btnMyTeammates, btnMySquadz, type);

            }
        });

        btnMySquadz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSquadzButtonClicked = true;
                //Kept the click type
                type = "My_Squadz";
                //Setting the add button Image
                setImageBackGround(isSquadzButtonClicked);
                //Handling the list views visibility
                handleViewVisibility(false);
                //Handling the Button Colors
                changeButtonBackground(btnMySquadz, btnMyTeammates,type);
                teamsArray = new ArrayList<TeamsModel>();

                //Initializing the swipe view in the list view
                setSwipeMenuCreator();

                if (sportName != null &&
                        sportName.equalsIgnoreCase("All")) {
                    //Get all Teams from database
                    getAllTeams();
                } else {
                    //getting all the friends from Database
                    getAllTeamsWithFilter(sportName);
                }

                //Clearing the array content
                if(teamsArray.size() > 0){
                    setListData();
                }else {
                    Toast.makeText(TeamMates.this,"No Teams Found", Toast.LENGTH_LONG).show();
                    setListData();
                }
            }
        });

        layBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.IS_NEWLY_ADDED_TEAM = false;
                TeamMates.this.finish();
            }
        });

        ivAddPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Handling the click event on the add button click
                if(isSquadzButtonClicked){
                    Constants.IS_EDIT_BUTTON_CLICKED = false;
                    startActivity(new Intent(TeamMates.this, AddNewTeam.class));
                }else{
                    displayDialog();
                }
            }
        });

        sportPicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Counter check for not calling getVenuesFilterListing() for first time
                // as it was called for first time automatically
                sportName = sportPicker.getSelectedItem().toString();
                SELECTED_SPORT_NAME = sportName;
                int pos = HomeFragment.getCategoryPos(sports_name_array, sportName);
                sportPicker.setSelection(pos);

                if (friendArray != null) {
                    friendArray.clear();
                }

                if(!isSquadzButtonClicked){
                    if (db.getFriendTableCount() > 0) {
                        if (sportName != null &&
                                sportName.equalsIgnoreCase("All")) {
                            //Get all friends from database
                            getAllFriendFromDB();
                        } else {
                            //Get all Filtered friends from database
                            getAllFriendFromDBWithFilter(sportName);
                        }
                    }
                    //Refreshing the List view
                    setDatainListVIew();
                }else{
                    teamsArray = new ArrayList<TeamsModel>();
                    setSwipeMenuCreator();
                    if (sportName != null &&
                            sportName.equalsIgnoreCase("All")) {
                        //Get all Teams from database
                        getAllTeams();
                    } else {
                        //getting all the friends from Database
                        getAllTeamsWithFilter(sportName);
                    }
                    if(teamsArray.size() > 0){
                        setListData();
                    }else {
                        Toast.makeText(TeamMates.this,"No Teams Found", Toast.LENGTH_LONG).show();
                        setListData();
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Function call to refresh the UI
        refreshUi();

        teamList.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                teamList.smoothOpenMenu(position);
            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });


        // step 2. listener item click event
        teamList.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        //Toast.makeText(TeamMates.this,"1st page", Toast.LENGTH_LONG).show();
                        //Getting the teamId
                        Constants.TEAM_ID = teamsArray.get(position).getTeamId();
                        Constants.TEAM_NAME = teamsArray.get(position).getTeamName();
                        Constants.EDIT_NAME_SPORT_NAME = teamsArray.get(position).getTeamSport();
                        Constants.IS_EDIT_BUTTON_CLICKED = true;

                        Intent intent_addTeam = new Intent(TeamMates.this,AddNewTeam.class);
                        startActivity(intent_addTeam);
                        break;
                    case 1:
                        DatabaseAdapter db = new DatabaseAdapter(TeamMates.this);

                        ArrayList<String> participantsIds = new ArrayList<String>();

                        //Adding the logged in user id to the array
                        participantsIds.add(logged_in_user_id);

                        //Getting the teamId
                        String chatting_getteamId = teamsArray.get(position).getTeamId();
                        String chatting_teamName = teamsArray.get(position).getTeamName();

                        //Getting all friends friends from Database
                        ArrayList<FirebaseGroupChatParticipantModel> chatParticiapntmodel =
                                db.getAllteamPlayers(chatting_getteamId);

                        if(chatParticiapntmodel.size() > 0){
                            //Getting all the participants ids
                            for(int i = 0; i < chatParticiapntmodel.size(); i++){
                                String getParticiapntId = chatParticiapntmodel.get(i).getUser_id();
                                participantsIds.add(getParticiapntId);
                            }
                            //Fetch Values
                            fetchValue(chatting_getteamId, chatting_teamName,
                                    participantsIds, chatParticiapntmodel);
                        }else{
                            Toast.makeText(TeamMates.this, "Unable to create chat seassion as, there is no teammate found in the group.", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case 2:

                        //Getting the teamId
                        String getteamId = teamsArray.get(position).getTeamId();
                        String teamName = teamsArray.get(position).getTeamName();

                        //Displaying the Delete team Dialog
                        showdeleteTeamDialog(getteamId, teamName);

                        //API call to remove the team
                        //RemoveTeam(getteamId);
                        break;
                }
                return false;
            }
        });

        /**
         * Pull to refresh functionality
         */
        swipe_refresh_layout_teammates.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_layout_teammates.setRefreshing(false);
                if(type.equalsIgnoreCase("My_Teammates")) {
                    getUserFriends(auth_token);
                } else{
                    getUserTeams(auth_token);
                }
            }
        });
    }


    /**
     * Function responsible for refreshing the message badge count
     */
    public void refreshBadgeCount(String userId){
        //Initializing the model
        FirebaseBadgeCountModel firebaseModel = new FirebaseBadgeCountModel();
        firebaseModel.setBadgeCount(0);

        //Refreshing the badge count for the reciver
        Firebase.setAndroidContext(TeamMates.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child("messageBadgeCount");
        postRef.child(userId).setValue(firebaseModel);
    }

    /**
     * Function responsible for creating the channel ID and saving the group info in firebase
     * @param groupName The Group/ Team name
     * @param groupId The Group/ Team id
     * @param groupModel The Group/ Team participant Model
     */
    public void createGroupChannelId(String groupName,
                                     String groupId,
                                     ArrayList<FirebaseGroupChatParticipantModel> groupModel,
                                     ArrayList<String> player_ids){
        //String channel Id
        String channel_id = ApplicationUtility.getChannelId();
        int getCurrentDate = ChatActivity.getCurrentDate();

        //Initializing the firebase Client
        Firebase.setAndroidContext(TeamMates.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child(AppConstants.GROUP_NODE).
                child(groupId);

        //Adding the
        ArrayList<String> onLineUserArray = new ArrayList<>();
        for(int i = 0; i < player_ids.size();  i++){
            onLineUserArray.add(player_ids.get(i));
            //Inserting the badge count for the group members
            refreshBadgeCount(player_ids.get(i));
        }

        if(onLineUserArray.contains(logged_in_user_id)){
            onLineUserArray.remove(logged_in_user_id);
        }

        //Creating the model to push firebase
        FirebaseGroupDetailsModel groupDetailsModel = new FirebaseGroupDetailsModel();

        //Setting the data in model
        groupDetailsModel.setChannelId(channel_id);
        groupDetailsModel.setDate(getCurrentDate);
        groupDetailsModel.setGroupId(groupId);
        groupDetailsModel.setGroupName(groupName);
        groupDetailsModel.setImageUrl("");
        groupDetailsModel.setParticipants(groupModel);
        groupDetailsModel.setParticipants(groupModel);
        groupDetailsModel.setReadMessageCount(0);
        groupDetailsModel.setOffLineUsers(onLineUserArray);

        //Pushing the model to the firebase
        postRef.setValue(groupDetailsModel);

        Firebase postRef_unreadmessage_count = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef_unreadmessage_count = postRef_unreadmessage_count.child("groupmessagecount").child(groupId);

        //Saving the data in the all the participant
        for(int i = 0; i < player_ids.size();  i++){
            //Initializing the firebase Client
            Firebase.setAndroidContext(TeamMates.this);
            Firebase postRef_teammates = new Firebase(EndPoints.BASE_URL_FIREBASE);
            postRef_teammates = postRef_teammates.child(AppConstants.CONVERSATION_NODE).
                    child(player_ids.get(i)).child("groupIds");
            AppConstants.groupParticipantsIds.add(player_ids.get(i));
            postRef_teammates.push().setValue(groupId);

            String playerId = player_ids.get(i);

            //postRef_unreadmessage_count.child(playerId);
            GroupChatReadMessageCount unreadMessages = new GroupChatReadMessageCount();
            unreadMessages.setReadMessageCount(0);
            postRef_unreadmessage_count.child(playerId).setValue(unreadMessages);
        }

        if(AppConstants.groupParticipantsIds.contains(logged_in_user_id)){
            AppConstants.groupParticipantsIds.remove(logged_in_user_id);
        }

        isGroupChaneelIdCreated = true;

        //Asigning the values to the chat
        assignChatValues(channel_id, groupName, "", groupId);

        //Redirecting the user to the chat board
        redirectUser();
    }

    /**
     * Fetching conversation data from firebase
     */
    public void fetchValue(final String groupId, final String groupName,
                           final ArrayList<String> player_ids,
                           final ArrayList<FirebaseGroupChatParticipantModel> groupModel){

        //Handling the loader
        LoaderUtility.handleLoader(TeamMates.this, true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(TeamMates.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(logged_in_user_id).child("groupIds");

        //callback to handle the user conversation data
        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                if(!isGroupChaneelIdCreated && !AppConstants.isgroupDeleted){
                AppConstants.isgroupDeleted = false;
                groupIdsArray = new ArrayList<String>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String message_group_id = (String) postSnapshot.getValue();
                    if(!groupIdsArray.contains(message_group_id)){
                        groupIdsArray.add(message_group_id);
                    }
                }
                    if(groupIdsArray.contains(groupId)){
                        fetchGroupValue(groupId);
                    }else{
                        AppConstants.isgroupDeleted = false;
                        isGroupChaneelIdCreated = false;
                        createGroupChannelId(groupName, groupId, groupModel,player_ids);
                    }

                }
                Log.d("Data Snapshot", dataSnapshot.toString());
                //Handling the loader
                LoaderUtility.handleLoader(TeamMates.this, false);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }



    /**
     * Fetching conversation data from firebase
     */
    public void fetchGroupValue(String groupId){
        //Handling the loader
        // handleLoader(true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(TeamMates.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.GROUP_NODE);

        //callback to handle the user conversation data
        postRef.child(groupId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                Log.d("TAG", dataSnapshot.toString());
                ChatConversationModel chatConversationModel = new ChatConversationModel();
                String channelId = (String) dataSnapshot.child("channelId").getValue();
                long date = (long) dataSnapshot.child("date").getValue();
                String recieverID = (String) dataSnapshot.child("groupId").getValue();
                String chatUserName = (String) dataSnapshot.child("groupName").getValue();
                String imageUrl = (String) dataSnapshot.child("imageUrl").getValue();


                //Asigning the values to the chat
                assignChatValues(channelId, chatUserName, "", recieverID);

                //Redirecting the user to the chat board
                redirectUser();

              /*  chatConversationModel.setChannelId(channelId);
                chatConversationModel.setDate(date);
                chatConversationModel.setRecieverID(recieverID);
                chatConversationModel.setChatUserName(chatUserName);
                chatConversationModel.setImageUrl(imageUrl);
                chatConversationModel.setSenderId(null);
                chatConversationModel.setChatType("group");*/
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

        // handleLoader(false);
    }


    /**
     * Function to handle the page redirections
     */
    public void redirectUser(){
        pageredirectionsCount = pageredirectionsCount + 1;
        if(pageredirectionsCount == 1){
            isGroupChaneelIdCreated = false;
            Intent intent = new Intent(TeamMates.this, ChatActivity.class);
            startActivity(intent);
        }


    }

    /**
     * Function to assign chat values
     */
    public void assignChatValues(String channelId, String groupName,
                                 String imageUrl, String groupId){
        AppConstants.CHAT_CHANNEL_ID = channelId;
        AppConstants.CHAT_USER_NAME = groupName;
        AppConstants.CHAT_USER_IMAGE_URL = imageUrl;
        AppConstants.CHAT_USER_ID = groupId;
        AppConstants.CHAT_TYPE = "group";
    }

    public void handleViewVisibility(boolean status) {
        if (status) {
            teamList.setVisibility(View.GONE);
            friendsList.setVisibility(View.VISIBLE);
        } else {
            friendsList.setVisibility(View.GONE);
            teamList.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Function responsible for updating the UI
     */
    public void refreshUi() {
        friendArray = new ArrayList<>();
        //Checking the FRIEND table insert COunt
        if (db.getFriendTableCount() > 0) {
            if (Constants.SPORTS_NAME != null &&
                    Constants.SPORTS_NAME.equalsIgnoreCase("All")) {
                //Getting all friends from DB
                getAllFriendFromDB();
            } else {
                //Getting all the friends from the Db
                getAllFriendFromDBWithFilter(Constants.SPORTS_NAME);
            }
        }
        //Refreshing the List view
        setDatainListVIew();
    }

    public void setDatainListVIew() {
        if (friendArray.size() != 0) {
            //refreshing the UI
            friendsAdapter = new FriendsAdapter(TeamMates.this, friendArray);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            friendsList.setLayoutManager(mLayoutManager);
            friendsList.setItemAnimator(new DefaultItemAnimator());
            friendsList.setAdapter(friendsAdapter);
            friendsAdapter.notifyDataSetChanged();
        } else {
            Toast.makeText(TeamMates.this, "No Teammates Found", Toast.LENGTH_LONG).show();
            friendsAdapter = new FriendsAdapter(TeamMates.this, friendArray);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            friendsList.setLayoutManager(mLayoutManager);
            friendsList.setItemAnimator(new DefaultItemAnimator());
            friendsList.setAdapter(friendsAdapter);
            friendsAdapter.notifyDataSetChanged();
        }
    }


    public void initializeClass() {
        db = new DatabaseAdapter(TeamMates.this);
        //Change the Swipe layout color
        swipe_refresh_layout_teammates.setColorSchemeResources(R.color.orange,
                R.color.orange,
                R.color.orange);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void changeButtonBackground(Button clicked, Button notClcike, String type) {

        if(type.equalsIgnoreCase("My_Teammates")) {
            clicked.setTextColor(Color.parseColor("#000000"));
            notClcike.setTextColor(Color.parseColor("#CCCCCC"));
            btnMySquadz.setBackgroundColor(Color.parseColor("#FFFFFF"));
            btnMyTeammates.setBackgroundResource(R.drawable.myroster_backgroung);
        }
        else{
            notClcike.setTextColor(Color.parseColor("#CCCCCC"));
            clicked.setTextColor(Color.parseColor("#000000"));
            btnMyTeammates.setBackground(null);
            btnMySquadz.setBackgroundResource(R.drawable.myroster_backgroung);
        }
    }

    //Getting Sports name from database
    public void getSportsName() {
        Cursor cursor = db.getSportsName();
        sports_name_array = new ArrayList<>();
        sports_id_array = new ArrayList<>();
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String sport_Name = cursor.getString(cursor
                            .getColumnIndex("sport_name"));
                    String sport_id = cursor.getString(cursor
                            .getColumnIndex("sport_id"));
                    sports_name_array.add(sport_Name);
                    sports_id_array.add(sport_id);

                } while (cursor.moveToNext());
            }
            //Sending sports name with sports id to the spinner
            setVenueSportsName(sports_name_array, sports_id_array);
        }
    }

    /**
     * Keeping sports name inside spinner
     */
    public void setVenueSportsName(ArrayList<String> sports_name_array, ArrayList<String> sports_id_array) {

        //Getting venue name & id as a array to display inside the spinner
        String[] spinnerArray = new String[sports_id_array.size()];
        map_sports_name = new HashMap<String, String>();

        for (int i = 0; i < sports_id_array.size(); i++) {
            map_sports_name.put(sports_name_array.get(i), sports_id_array.get(i));
            spinnerArray[i] = sports_name_array.get(i);
        }

        //Sending sports name as a String[]
        displaySpinnerInfo(spinnerArray);
    }

    /**
     * Displaying name inside spinner
     *
     * @param spinnerArray
     */
    public void displaySpinnerInfo(String[] spinnerArray) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(TeamMates.this, android.R.layout.simple_spinner_item, spinnerArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sportPicker.setAdapter(dataAdapter);
        int pos = getCategoryPos(sports_name_array, Constants.SPORTS_NAME);
        sportPicker.setSelection(pos);
    }

    /**
     * getting the position os a item in the arrayalist
     *
     * @param SportsArray
     * @param category
     * @return
     */
    private int getCategoryPos(ArrayList<String> SportsArray, String category) {
        return SportsArray.indexOf(category);
    }

    /**
     * Function to pull all the friends from Local DB
     */

    public void getAllFriendFromDB() {
        Cursor cursor = db.getAllFriendsDeomDb();
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String user_id = cursor.getString(cursor
                            .getColumnIndex("user_id"));
                    String username = cursor.getString(cursor
                            .getColumnIndex("username"));
                    String firstname = cursor.getString(cursor
                            .getColumnIndex("firstname"));
                    String lastname = cursor.getString(cursor
                            .getColumnIndex("lastname"));
                    String image = cursor.getString(cursor
                            .getColumnIndex("image"));
                    String sport = cursor.getString(cursor
                            .getColumnIndex("sport"));
                    String phonenumber = cursor.getString(cursor
                            .getColumnIndex("phonenumber"));
                    String status = cursor.getString(cursor
                            .getColumnIndex("status"));
                    String reward = cursor.getString(cursor
                            .getColumnIndex("reward"));
                    FriendsModel friendsModel = new FriendsModel(user_id, username, firstname,
                            lastname, image, sport, phonenumber, status, reward);
                    friendArray.add(friendsModel);
                } while (cursor.moveToNext());
            }
        }

    }

    /**
     * Function to pull all the friends from Local DB
     */

    public void getAllFriendFromDBWithFilter(String sportName) {
        Cursor cursor = db.getFilteredFriendList(sportName);
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String user_id = cursor.getString(cursor
                            .getColumnIndex("user_id"));
                    String username = cursor.getString(cursor
                            .getColumnIndex("username"));
                    String firstname = cursor.getString(cursor
                            .getColumnIndex("firstname"));
                    String lastname = cursor.getString(cursor
                            .getColumnIndex("lastname"));
                    String image = cursor.getString(cursor
                            .getColumnIndex("image"));
                    String sport = cursor.getString(cursor
                            .getColumnIndex("sport"));
                    String phonenumber = cursor.getString(cursor
                            .getColumnIndex("phonenumber"));
                    String status = cursor.getString(cursor
                            .getColumnIndex("status"));
                    String reward = cursor.getString(cursor
                            .getColumnIndex("reward"));
                    FriendsModel friendsModel = new FriendsModel(user_id, username, firstname,
                            lastname, image, sport, phonenumber, status, reward);
                    friendArray.add(friendsModel);
                } while (cursor.moveToNext());
            }
        }

    }


    /*public void displayDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(TeamMates.this);
        builderSingle.setTitle("Add teammates");

        final ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(TeamMates.this,
                        android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Username");
        arrayAdapter.add("Address book");
        arrayAdapter.add("Facebook");
        arrayAdapter.add("Nearby");

        builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);

                //Calling the function to handle the page redirection
                redirectUserToAddFriend(strName);
                dialog.dismiss();
            }
        });
        builderSingle.show();
    }
*/

    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayDialog(){
        mMaterialDialog = new MaterialDialog(TeamMates.this);
        View view = LayoutInflater.from(TeamMates.this)
                .inflate(R.layout.layout_add_teammates,
                        null);
        TextView tv_add_teammates_username = (TextView) view.findViewById(R.id.tv_add_friend_username);
        TextView tv_add_teammates_addressbook = (TextView) view.findViewById(R.id.tv_add_friend_addressbook);
        TextView tv_add_teammates_facebook = (TextView) view.findViewById(R.id.tv_add_friend_facebook);
        TextView tv_add_teammates_nearby = (TextView) view.findViewById(R.id.tv_add_friend_nearby);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel_teammates);
        tv_add_teammates_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                startActivity(new Intent(TeamMates.this, AddFriendUsingUsername.class));
            }
        });

        tv_add_teammates_addressbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                /**RedirectToAddressBook*/
                redirectToAddressBook();

            }
        });

        tv_add_teammates_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                startActivity(new Intent(TeamMates.this, AddFriendUsingFacebook.class));
            }
        });

        tv_add_teammates_nearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                startActivity(new Intent(TeamMates.this, AddFriendUsingNearBy.class));
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();

            }
        });

        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }


    /**
     * Handling the page redirection for adding friends
     * @param pageRedirect
     */
    public void redirectUserToAddFriend(String pageRedirect){
        if(pageRedirect != null && pageRedirect.equalsIgnoreCase("Username")){
            startActivity(new Intent(TeamMates.this, AddFriendUsingUsername.class));
        }else if(pageRedirect != null && pageRedirect.equalsIgnoreCase("Address book")){
            startActivity(new Intent(TeamMates.this, AddFriendUsingAddressbook.class));
        }else if(pageRedirect != null && pageRedirect.equalsIgnoreCase("Facebook")){
            startActivity(new Intent(TeamMates.this, AddFriendUsingFacebook.class));
        }else if(pageRedirect != null && pageRedirect.equalsIgnoreCase("Nearby")){
            startActivity(new Intent(TeamMates.this, AddFriendUsingNearBy.class));
        }
    }


    public void getAllTeamsWithFilter(String sportName) {
        Cursor cursor = db.getAllTeams(sportName);
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String team_id = cursor.getString(cursor
                            .getColumnIndex("team_id"));
                    String team_name = cursor.getString(cursor
                            .getColumnIndex("team_name"));
                    String team_sport = cursor.getString(cursor
                            .getColumnIndex("team_sport_id"));
                    String team_custom_sport = cursor.getString(cursor
                            .getColumnIndex("team_custom_sport_name"));
                    String team_creator_id = cursor.getString(cursor
                            .getColumnIndex("team_creator_user_id"));
                    String team_creator_name = cursor.getString(cursor
                            .getColumnIndex("team_creator_user_name"));
                    String team_creator_image = cursor.getString(cursor
                            .getColumnIndex("team_creator_profile_image"));
                    int team_player_count = db.getTeamPlayerCount(team_id);
                    TeamsModel teamModel = new TeamsModel(team_id, team_name, team_sport,
                            team_custom_sport, team_creator_id, team_creator_name,
                            team_creator_image, String.valueOf(team_player_count));
                    teamsArray.add(teamModel);
                } while (cursor.moveToNext());
            }
        }

    }



    public void getAllTeams() {
        Cursor cursor = db.getTeamsForAll();
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String team_id = cursor.getString(cursor
                            .getColumnIndex("team_id"));
                    String team_name = cursor.getString(cursor
                            .getColumnIndex("team_name"));
                    String team_sport = cursor.getString(cursor
                            .getColumnIndex("team_sport_id"));
                    String team_custom_sport = cursor.getString(cursor
                            .getColumnIndex("team_custom_sport_name"));
                    String team_creator_id = cursor.getString(cursor
                            .getColumnIndex("team_creator_user_id"));
                    String team_creator_name = cursor.getString(cursor
                            .getColumnIndex("team_creator_user_name"));
                    String team_creator_image = cursor.getString(cursor
                            .getColumnIndex("team_creator_profile_image"));
                    int team_player_count = db.getTeamPlayerCount(team_id);
                    TeamsModel teamModel = new TeamsModel(team_id, team_name, team_sport,
                            team_custom_sport, team_creator_id, team_creator_name,
                            team_creator_image, String.valueOf(team_player_count));
                    teamsArray.add(teamModel);
                } while (cursor.moveToNext());
            }
        }

    }



    private void setSwipeMenuCreator() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                switch (menu.getViewType()) {
                    case 0:
                        createMenu3(menu);
                        break;
                }
            }

            private void createMenu3(SwipeMenu menu) {
                SwipeMenuItem item1 = new SwipeMenuItem(
                        getApplicationContext());
                item1.setBackground(new ColorDrawable(Color.rgb(255, 115,
                        0)));
                item1.setWidth(dp2px(50));
                item1.setIcon(R.mipmap.ic_mode_edit_white_144);
                menu.addMenuItem(item1);

                SwipeMenuItem item2 = new SwipeMenuItem(getApplicationContext());
                item2.setBackground(new ColorDrawable((Color.rgb(255, 115, 0))));
                //   item2.setBackground(getResources().getColor(R.color.status_inprogress));
                item2.setWidth(dp2px(50));
                item2.setIcon(R.mipmap.ic_message_white_144);
                menu.addMenuItem(item2);

                SwipeMenuItem item3 = new SwipeMenuItem(
                        getApplicationContext());
                item3.setBackground(new ColorDrawable(Color.rgb(255, 115, 0)));
                // item2.setBackground(getResources().getColor(R.color.status_resolved));
                item3.setWidth(dp2px(50));
                item3.setIcon(R.mipmap.ic_delete_white_144);
                menu.addMenuItem(item3);
            }
        };
        // if(!is_client.matches("1") && !task_rest.matches("1")){
        // set creator
        teamList.setMenuCreator(creator);
        //}
        // Close Interpolator
        teamList.setCloseInterpolator(new AccelerateInterpolator());
    }

    /*** convert dp to pixels ***/
    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    /**
     * Set Team data
     */

    public void setListData(){
        teamAdapter = new TeamsAdapter(TeamMates.this, teamsArray);
        teamList.setAdapter(teamAdapter);
        teamAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Handling the list view if the friend ship has been removed
        if(Profile.isFriendremoved){
            if (friendArray != null) {
                friendArray.clear();
            }
            //Get all Filtered friends from database
            if (sportName != null &&
                    sportName.equalsIgnoreCase("All")) {
                //Get all friends from database
                getAllFriendFromDB();
            } else {
                //Get all Filtered friends from database
                getAllFriendFromDBWithFilter(sportName);
            }
            //Refreshing the List view
            setDatainListVIew();
            Profile.isFriendremoved = false;
        }
        if(Constants.IS_NEWLY_ADDED_TEAM)
        {
            Constants.IS_NEWLY_ADDED_TEAM = false;
            getAllTeamsAfterNewlyAdded();
        }
        if(Constants.IS_UPDATED_TEAM)
        {
            Constants.IS_UPDATED_TEAM = false;
            getUserTeams(auth_token);
        }
        if(Constants.FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY)
        {
            Constants.FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY = false;
            getUserFriends(auth_token);
        }
    }

    public void setImageBackGround(boolean clickStatus){
        if(clickStatus){
            ivAddPlayer.setImageResource(R.drawable.ic_add_team_orange_36dp);
        }else{
            ivAddPlayer.setImageResource(R.mipmap.ic_adduser_orange_144dp);
        }

    }


    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(TeamMates.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildTeamremoveApiRequestBody(String team_id){
       // HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"teams/"+auth_token+"/delete?").newBuilder();
        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"teams/"+auth_token+"/delete?").newBuilder();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"/delete_team").newBuilder();

        urlBuilder.addQueryParameter(Constants.PARAM_TEAM_ID, team_id);
        //urlBuilder.addQueryParameter(Constants.PARAM_USER_ID, logged_in_user_id);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Calling the API to getting the sports
     */

    public void RemoveTeam(final String teamId){

        //Displaying loader
        LoaderUtility.handleLoader(TeamMates.this, true);
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildTeamremoveApiRequestBody(teamId);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                SplashScreen.displayMessageInUiThread(TeamMates.this,e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    apiStatus = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if(apiStatus != null &&
                            apiStatus.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages
                        displayMessage(message);
                    }else if(apiStatus != null &&
                            apiStatus.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                TeamMates.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(apiStatus != null &&
                                apiStatus.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){

                            //Removing the team from Database
                            db.deleteTeam(teamId);

                            //Removing the team players from Database
                            db.deleteTeamPlayer(teamId);

                            //Refreshing the team List UI
                            refreshTeamListUI();
                        }
                    }
                });
                LoaderUtility.handleLoader(TeamMates.this, false);
            }
        });
    }


    /**
     * Dispaly ing the message
     * @param message status message
     */
    public void displayMessage(final String message){
        TeamMates.this.runOnUiThread(new Runnable() {
            public void run() {

                //Displaying the success message after successful sign up
                //Toast.makeText(ForgotPasswordScreen.this,message, Toast.LENGTH_LONG).show();
                // snackbar.setSnackBarMessage(message);
                Toast.makeText(TeamMates.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }


    /**
     * Function handling the UI for the team List
     */
    public void refreshTeamListUI(){

        teamsArray = new ArrayList<TeamsModel>();
        //getting all the friends from Database
        //getAllTeamsWithFilter(sportName);

        if (sportName != null &&
                sportName.equalsIgnoreCase("All")) {
            //Get all Teams from database
            getAllTeams();
        } else {
            //getting all the friends from Database
            getAllTeamsWithFilter(sportName);
        }

        //Clearing the array content
        if(teamsArray.size() > 0){
            setListData();
        }else {
            Toast.makeText(TeamMates.this,"No Teams Found", Toast.LENGTH_LONG).show();
            setListData();
        }
    }


    private void showdeleteTeamDialog(final String team_id, String team_name) {
        AlertDialog.Builder builder = new AlertDialog.Builder(TeamMates.this);
        builder.setTitle("Delete Team - "+ team_name);
        builder.setMessage("Are you sure you want to delete this team?");

        String positiveText = getString(android.R.string.ok);

        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        dialog.dismiss();
                        RemoveTeam(team_id);
                    }
                });

        String negativeText = getString(android.R.string.cancel);

        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    /**
     * Getting all teams from local database
     */
    public void getAllTeamsAfterNewlyAdded()
    {
        isSquadzButtonClicked = true;
        //Kept the click type
        type = "My_Squadz";
        //Setting the add button Image
        //Setting the add button Image
        setImageBackGround(isSquadzButtonClicked);
        //Handling the list views visibility
        handleViewVisibility(false);
        //Handling the Button Colors
        changeButtonBackground(btnMySquadz, btnMyTeammates,type);
        teamsArray = new ArrayList<TeamsModel>();

        //Initializing the swipe view in the list view
        setSwipeMenuCreator();

        if (sportName != null &&
                sportName.equalsIgnoreCase("All")) {
            //Get all Teams from database
            getAllTeams();
        } else {
            //getting all the friends from Database
            getAllTeamsWithFilter(sportName);
        }

        //Clearing the array content
        if(teamsArray.size() > 0){
            setListData();
        }else {
            Toast.makeText(TeamMates.this,"No Teams Found", Toast.LENGTH_LONG).show();
            setListData();
        }
    }

    /**
     * All friends API
     * @param authToken
     */
    public void getUserFriends(final String authToken){

        final DatabaseAdapter db = new DatabaseAdapter(TeamMates.this);
        //Handling the loader state
        LoaderUtility.handleLoader(TeamMates.this, true);


        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildFriendsApiRequestBody(authToken);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(TeamMates.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    err_msg = responseObject.getString("status");
                    if (err_msg.equalsIgnoreCase("success")) {
                        db.deleteFriend();
                        JSONArray friendArray = responseObject.getJSONArray("all_users");
                        for(int i = 0; i < friendArray.length(); i++){
                            JSONObject frienddetailsObject = friendArray.getJSONObject(i);
                            String userId = frienddetailsObject.getString("user_id");
                            String userName = frienddetailsObject.getString("username");
                            String firstName = frienddetailsObject.getString("first_name");
                            String lastName = frienddetailsObject.getString("last_name");
                            String image = frienddetailsObject.getString("image");
                            String sport = frienddetailsObject.getString("sport");
                            String phone_number = frienddetailsObject.getString("phone_number");
                            //String status = frienddetailsObject.getString("status");
                           // String rewards = frienddetailsObject.getString("rewards");
                            db.insertfriend(userId,userName,firstName,lastName,
                                    image,sport,phone_number,null,"0");
                        }
                    } else {
                        msg = responseObject.getString("message");
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }


                TeamMates.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Hiding the soft key board after successful login
                        //hideSoftKeyboard();
                        LoaderUtility.handleLoader(TeamMates.this, false);
                        if(err_msg != null && err_msg.equalsIgnoreCase("success")){
                            fetchTeammates();
                        }else{
                            snackBar.setSnackBarMessage(msg);
                        }
                    }
                });
            }
        });
    }
    /**
     * All Teams API
     * @param authToken
     */

    public void getUserTeams(final String authToken){

        final DatabaseAdapter db = new DatabaseAdapter(TeamMates.this);
        //Handling the loader state
        LoaderUtility.handleLoader(TeamMates.this, true);


        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildTeamsApiRequestBody(authToken);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(TeamMates.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    err_msg = responseObject.getString("status");
                    if (err_msg.equalsIgnoreCase("success")) {

                        db.deleteTeamPlayer();
                        JSONArray friendArray = responseObject.getJSONArray("all_teams");
                        for(int i = 0; i < friendArray.length(); i++){
                            JSONObject frienddetailsObject = friendArray.getJSONObject(i);
                            String teamId = frienddetailsObject.getString("_id");
                            String teamName = frienddetailsObject.getString("name");
                            String teamSport = frienddetailsObject.getString("sport_id");
                            String sportName = db.getcurrentSportName(teamSport);
                            String teamCustomSport = frienddetailsObject.getString("custom_sport_name");
                            String teamCreatorId = frienddetailsObject.getString("user_id");
                            String teamCreatorName = frienddetailsObject.getString("user_name");
                            String teamCreatorImage = frienddetailsObject.getString("image");
                            String teamCreatedAt = frienddetailsObject.getString("created_at");
                            String teamUpdatedAt = frienddetailsObject.getString("updated_at");

                            //Inserting the teams data in Local DB
                            db.insertMyTeam(teamId,teamName,sportName,teamCustomSport,
                                    teamCreatorId,teamCreatorName,teamCreatorImage,teamCreatedAt,teamUpdatedAt);

                            if(frienddetailsObject.has("players") && !frienddetailsObject.isNull("players"))
                            {
                                JSONArray teamPlayersArray = frienddetailsObject.getJSONArray("players");
                                for (int j = 0; j < teamPlayersArray.length(); j++) {
                                    JSONObject teamPlayersObj = teamPlayersArray.getJSONObject(j);
                                    String player_id = teamPlayersObj.getString("player_id");
                                    String player_user_name = teamPlayersObj.getString("player_name");
                                    String player_first_name = teamPlayersObj.getString("first_name");
                                    String player_last_name = teamPlayersObj.getString("last_name");
                                    String player_image = teamPlayersObj.getString("image");

                                    //Inserting the teammayes data in Local DB
                                    db.insertTeamPlayer(teamId, player_id, player_user_name,
                                            player_first_name, player_last_name, player_image);
                                }
                            }
                        }
                    } else {
                        msg = responseObject.getString("message");
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }


                TeamMates.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Hiding the soft key board after successful login
                        //hideSoftKeyboard();
                        LoaderUtility.handleLoader(TeamMates.this, false);
                        if(err_msg != null && err_msg.equalsIgnoreCase("success")){
                            //Handling the loader state
                            getAllTeamsAfterNewlyAdded();

                        }else{
                            snackBar.setSnackBarMessage(msg);
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildTeamsApiRequestBody(String authTOken){

        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"teams/"+authTOken).newBuilder();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"teams").newBuilder();
        urlBuilder.addQueryParameter("user_id",logged_in_user_id);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildFriendsApiRequestBody(String authTOken){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"friend_list").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_AUTHTOKEN, authTOken);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Getting all teammates from local storage
     */
    public void fetchTeammates()
    {
        isSquadzButtonClicked = false;

        //Kept the click type
        type = "My_Teammates";
        //Setting the add button Image
        setImageBackGround(isSquadzButtonClicked);

        //Handling the list views visibility
        handleViewVisibility(true);

        //Clearing the array content
        if (friendArray != null) {
            friendArray.clear();
        }
        if (sportName != null &&
                sportName.equalsIgnoreCase("All")) {
            //Get all friends from database
            getAllFriendFromDB();
        } else {
            //Get all Filtered friends from database
            getAllFriendFromDBWithFilter(sportName);
        }
        //Refreshing the List view
        setDatainListVIew();
        //Handling the Button Colors
        changeButtonBackground(btnMyTeammates, btnMySquadz, type);
    }

    /**
     * RedirectToAddressBook
     */
    public void redirectToAddressBook() {
        int MyVersion = Build.VERSION.SDK_INT;

        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
            else{
                startActivity(new Intent(TeamMates.this, AddFriendUsingAddressbook.class));
            }
        } else{
            startActivity(new Intent(TeamMates.this, AddFriendUsingAddressbook.class));
        }
    }

    /**
     * Request persmission for Post lollipop devices
     */
    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(TeamMates.this,
                new String[]{android.Manifest.permission.READ_CONTACTS},
                1);
    }

    /**
     * Check permission
     * @return
     */
    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Getting Contacts from Phone book
                    startActivity(new Intent(TeamMates.this, AddFriendUsingAddressbook.class));

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // Toast.makeText(Profile.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.IS_NEWLY_ADDED_TEAM = false;
        TeamMates.this.finish();
    }
}
