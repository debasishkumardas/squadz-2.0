package com.andolasoft.squadz.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.Manifest;
import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.adapters.EventParticipantsAdapter;
import com.andolasoft.squadz.views.adapters.FriendsAdapter;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SpNayak on 1/31/2017.
 */

public class Profile extends AppCompatActivity {

    @InjectView(R.id.profile_back_layout)
    RelativeLayout rel_back;
    @InjectView(R.id.profile_menu_layout)
    RelativeLayout rel_profile_menu;
    @InjectView(R.id.games_played_layout)
    RelativeLayout games_played_layout;
    @InjectView(R.id.phone_layout)
    RelativeLayout phone_layout;
    @InjectView(R.id.chat_layout)
    RelativeLayout chat_layout;
    @InjectView(R.id.message_layout)
    RelativeLayout message_layout;
    @InjectView(R.id.horizontal_scrollview_profile)
    HorizontalScrollView horizontal_scrollview_profile;
    @InjectView(R.id.interest_linear_layout)
    LinearLayout interest_linear_layout;
    @InjectView(R.id.user_profile_name)
    TextView user_profile_name;
    @InjectView(R.id.profile_user_image)
    CircleImageView profile_user_image;
    @InjectView(R.id.view1)
    View profileView_divider;
    @InjectView(R.id.view2)
    View profileview_divider2;
    @InjectView(R.id.view3)
    View interest_divider;
    @InjectView(R.id.interest_title)
    TextView tv_interest;
    @InjectView(R.id.lay_private_profile)
    RelativeLayout lay_private_profiles;
    @InjectView(R.id.call_info_layout)
    LinearLayout lay_contactInfo;
    ProgressDialog dialog;
    SharedPreferences preferences;
    SharedPreferences.Editor profileEditor;
    String auth_token, user_id, username;
    String primary_sport="",phone_number = "", userId = null, userEmail = null;
    boolean isStatus = false;
    SportsImagePicker sportsImagePicker;
    String status, message;
    public static boolean isFriendremoved = false;
    DatabaseAdapter db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_profile);

        //Initializing the Database
        db = new DatabaseAdapter(Profile.this);

        /**
         * Initializing views
         */
        initViews();

        /**
         * Adding Click events on te views
         */
        setClickEvents();

        /**
         * Display profile data
         */
        displayProfileInfo();

    }

    /**
     * Adding Click events on te views
     */
    public void setClickEvents() {
        //Back button click
        rel_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendsAdapter.isFromFriendAdapter = false;
                finish();
            }
        });

        //Menu icon click
        rel_profile_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Display Menu pop up window
                 */
                displayMenuPopUp();
            }
        });

        phone_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Making phone call
                makePhoneCall();

            }
        });

        chat_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Send SMS
                sendSMS();
            }
        });

        message_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open Gmail app
                openGmail();
    }
        });

        games_played_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    /**
     * Request persmission for Post lollipop devices
     */
    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(Profile.this,
                new String[]{android.Manifest.permission.CALL_PHONE},
                1);
    }

    /**
     * Check permission
     * @return
     */
    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Making call to the specific number
     */
    public void makePhoneCall() {
        int MyVersion = Build.VERSION.SDK_INT;

        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
            else{
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+phone_number));
                startActivity(callIntent);
            }
        }
        else{
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+phone_number));
            startActivity(callIntent);
        }
    }

    /**
     * Display profile data
     */
    public void displayProfileInfo() {
        if(!FriendsAdapter.isFromFriendAdapter){
            //Getting the user profile visibility status
            String is_prolfie_visible = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getProfile_visibility();
            String image = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getImage();
            if(is_prolfie_visible != null &&
                    is_prolfie_visible.equalsIgnoreCase("public")){
                lay_private_profiles.setVisibility(View.GONE);
                //Getting user Details
                String name = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getName();
                //image = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getImage();
                String userName = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getUsername();
                userId = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getUser_id();
                phone_number = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getPhone_number();
                primary_sport = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getSport();
                userEmail = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getEmail();
                isStatus = true;
                ArrayList<String> secondary_Sports_Array = EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getSecondary_sports_array();
                ArrayList<String> sports_arrayList = new ArrayList<>();

                //Fetching sports from sport id
                if(secondary_Sports_Array.size() > 0) {
                    if(!TextUtils.isEmpty(primary_sport)) {
                        sports_arrayList.add(primary_sport);
                    }
                    for (int i = 0; i < secondary_Sports_Array.size(); i++) {
                        String sport_name = db.getcurrentSportName(secondary_Sports_Array.get(i));
                        sports_arrayList.add(sport_name);
                    }
                } else{
                    if(!TextUtils.isEmpty(primary_sport)) {
                        sports_arrayList.add(primary_sport);
                    }
                }

                if(!TextUtils.isEmpty(name)) {
                    user_profile_name.setText(EventParticipantsAdapter.profileOtherViewModelArrayList.get(0).getName());
                }else{
                    user_profile_name.setText(userName);
                }

                if(!TextUtils.isEmpty(image)) {
                    Glide.with(Profile.this).load(image).into(profile_user_image);
                }

                /**
                 * Set Interest data inside Horizontal scroll view
                 */
                if(sports_arrayList.size() > 0) {
                    setInterestInfo(sports_arrayList);
                } else{
                    Toast.makeText(this, "No sports available", Toast.LENGTH_SHORT).show();
                }

                //Handling the contact info page visibilirt
                handleContactLayoutVIsibility(phone_number, userEmail);
            }else{

                if (!TextUtils.isEmpty(image)) {
                    Glide.with(Profile.this).load(image).into(profile_user_image);
                }
                //Refreshing UI for the Privare Profiles
                refreshUIforPrivateProfiles(true);
            }

        }else{
            //Getting the user profile visibility status
            String is_prolfie_visible = FriendsAdapter.profileOtherViewModelArrayList.get(0).getProfile_visibility();
            userId = FriendsAdapter.profileOtherViewModelArrayList.get(0).getUser_id();
            String image = FriendsAdapter.profileOtherViewModelArrayList.get(0).getImage();
            if(is_prolfie_visible != null && is_prolfie_visible.equalsIgnoreCase("public")) {
                lay_private_profiles.setVisibility(View.GONE);
                //Getting user Details
                String name = FriendsAdapter.profileOtherViewModelArrayList.get(0).getName();
                String userName = FriendsAdapter.profileOtherViewModelArrayList.get(0).getUsername();
                userId = FriendsAdapter.profileOtherViewModelArrayList.get(0).getUser_id();
                phone_number = FriendsAdapter.profileOtherViewModelArrayList.get(0).getPhone_number();
                primary_sport = FriendsAdapter.profileOtherViewModelArrayList.get(0).getSport();
                userEmail = FriendsAdapter.profileOtherViewModelArrayList.get(0).getEmail();
                isStatus = true;
                ArrayList<String> secondary_Sports_Array = FriendsAdapter.profileOtherViewModelArrayList.get(0).getSecondary_sports_array();
                ArrayList<String> sports_arrayList = new ArrayList<>();

                //Fetching sports from sport id
                if (secondary_Sports_Array.size() > 0) {
                    if (!TextUtils.isEmpty(primary_sport)) {
                        sports_arrayList.add(primary_sport);
                    }
                    for (int i = 0; i < secondary_Sports_Array.size(); i++) {
                        String sport_name = db.getcurrentSportName(secondary_Sports_Array.get(i));
                        sports_arrayList.add(sport_name);
                    }
                } else {
                    if (!TextUtils.isEmpty(primary_sport)) {
                        sports_arrayList.add(primary_sport);
                    }
                }
                if (!TextUtils.isEmpty(name)) {
                    user_profile_name.setText(FriendsAdapter.profileOtherViewModelArrayList.get(0).getName());
                } else {
                    user_profile_name.setText(userName);
                }
                if (!TextUtils.isEmpty(image)) {
                    Glide.with(Profile.this).load(image).into(profile_user_image);
                }
                /**
                 * Set Interest data inside Horizontal scroll view
                 */
                if (sports_arrayList.size() > 0) {
                    setInterestInfo(sports_arrayList);
                } else {
                    Toast.makeText(this, "No sports available", Toast.LENGTH_SHORT).show();
                }

                //Handling the contact info page visibilirt
                handleContactLayoutVIsibility(phone_number, userEmail);
            }else{

                if (!TextUtils.isEmpty(image)) {
                    Glide.with(Profile.this).load(image).into(profile_user_image);
                }
                //Refreshing UI for the Privare Profiles
                refreshUIforPrivateProfiles(true);
            }
        }
    }

    /**
     * Set Interest data inside Horizontal scroll view
     */
    public void setInterestInfo(ArrayList<String> secondary_sports_Array) {

        if(secondary_sports_Array.size() == 1) {
            for (int i = 0; i < secondary_sports_Array.size(); i++) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View inflate_View = inflater.inflate(R.layout.profile_interest_layout, null, false);
                TextView sport_name_profile_interest = (TextView) inflate_View.findViewById(R.id.sport_name_profile_interest);
                TextView primary_sport_profile_interest = (TextView) inflate_View.findViewById(R.id.primary_sport_profile_interest);
                ImageView primaryImageView = (ImageView) inflate_View.findViewById(R.id.sport_icon_profile_interest);
                sport_name_profile_interest.setText(secondary_sports_Array.get(i));
                SportsImagePicker sportImagepicker = new SportsImagePicker();
                primaryImageView.setBackgroundResource(sportImagepicker.getSportImage(secondary_sports_Array.get(i)));
                primary_sport_profile_interest.setVisibility(View.VISIBLE);
                interest_linear_layout.addView(inflate_View);
            }
        }
        else{
            for (int i = 0; i < secondary_sports_Array.size(); i++) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View inflate_View = inflater.inflate(R.layout.profile_interest_layout, null, false);
                TextView sport_name_profile_interest = (TextView) inflate_View.findViewById(R.id.sport_name_profile_interest);
                TextView primary_sport_profile_interest = (TextView) inflate_View.findViewById(R.id.primary_sport_profile_interest);
                ImageView sport_icon_profile_interest = (ImageView) inflate_View.findViewById(R.id.sport_icon_profile_interest);
                sport_icon_profile_interest.setImageResource(sportsImagePicker.getSportImage(secondary_sports_Array.get(i)));
                if(isStatus) {
                    if (secondary_sports_Array.contains(primary_sport)) {
                        sport_name_profile_interest.setText(secondary_sports_Array.get(i));
                        primary_sport_profile_interest.setVisibility(View.VISIBLE);
                        isStatus = false;
                    }
                }
                else{
                    sport_name_profile_interest.setText(secondary_sports_Array.get(i));
                    primary_sport_profile_interest.setVisibility(View.INVISIBLE);
                }
                interest_linear_layout.addView(inflate_View);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Making phone call
                    makePhoneCall();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                   // Toast.makeText(Profile.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    /**
     * Open Gmail app
     */
    public void openGmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/html");
        final PackageManager pm = this.getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
        String className = null;
        for (final ResolveInfo info : matches) {
            if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                className = info.activityInfo.name;
                if(className != null && !className.isEmpty()){
                    break;
                }
            }
        }
        emailIntent.setClassName("com.google.android.gm", className);
        //emailIntent.setData(Uri.parse("test@gmail.com"));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { userEmail });
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Squadz");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivityForResult(emailIntent,5);
    }

    /**
     * Send SMS
     */
    public void sendSMS() {
        String phn_number = phone_number;
       /* Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.putExtra("address", phn_number);
        smsIntent.setType("vnd.android-dir/mms-sms");
        //smsIntent.putExtra("sms_body", "This app is really good for a Bar/Club reservation. Play store link to download.\n" + Constant.PLAY_STORE_URL+"\n"
//                +"\nInstall the app and see Bar/Club around you.\n");
        startActivity(smsIntent);*/


        Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+phn_number));
        intentsms.putExtra("sms_body", "");
        startActivity(intentsms);
    }

    /**
     * Display Menu pop up window
     */
    public void displayMenuPopUp() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.profile_menu_info_popup);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        wlp.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        final TextView tv_remove_friendShip = (TextView)dialog.findViewById(R.id.removefriendship);
        TextView tv_cancel = (TextView)dialog.findViewById(R.id.cancel);

        //Getting all the User ids from database
        ArrayList<String> allUserIdsArray = db.getAllUserids();

        //Condition Implementation to display appropiate option as per the user
        if(user_id != null &&
                user_id.equalsIgnoreCase(userId)) {
            tv_remove_friendShip.setVisibility(View.GONE);
        }else if(userId != null &&
                !allUserIdsArray.contains(userId)){
            tv_remove_friendShip.setText("Add as Teammate");
        }else if(userId != null &&
                allUserIdsArray.contains(userId)){
            tv_remove_friendShip.setText("Remove from Teammates");
        }

        tv_remove_friendShip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                //Getting the text from the text view
                String getText = tv_remove_friendShip.getText().toString();

                //Implementation of the condition to call the add or remove team mate API
                if(getText != null && getText.equalsIgnoreCase("Add as Teammate")){
                    //API call to send team mate Request
                    Adduser(userId);
                }else if(getText != null && getText.equalsIgnoreCase("Remove from Teammates")){
                    //API call to remove teammate
                    RemoveUser(userId);
                }
            }
        });


        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * Initializing views
     */
    public void initViews() {
        ButterKnife.inject(this);
        sportsImagePicker = new SportsImagePicker();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        profileEditor = this.preferences.edit();

        if (preferences.contains("auth_token")) {
            auth_token = preferences.getString("auth_token", "");
            user_id = preferences.getString("loginuser_id", "");
            username = preferences.getString("USERNAME", "");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            //Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_LONG).show();
        }
        else{
            //Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FriendsAdapter.isFromFriendAdapter = false;
        finish();
    }


    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(Profile.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }



    /**
     * Calling the API to getting the sports
     */

    public void RemoveUser(final String userId){
        //Displaying loader
        LoaderUtility.handleLoader(Profile.this, true);
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBody(userId);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                if(!ApplicationUtility.isConnected(Profile.this)){
                    ApplicationUtility.displayNoInternetMessage(Profile.this);
                }else{
                    ApplicationUtility.displayErrorMessage(Profile.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages
                        displayMessage(message);
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                Profile.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            //Removing the friend from Database
                            db.deleteFriendFromDb(userId);
                            isFriendremoved = true;
                            Profile.this.finish();
                        }
                    }
                });

                LoaderUtility.handleLoader(Profile.this, false); 
            }
        });
    }


    /**
     * Dispaly ing the message
     * @param message status message
     */
    public void displayMessage(final String message){
        Profile.this.runOnUiThread(new Runnable() {
            public void run() {

                //Displaying the success message after successful sign up
                //Toast.makeText(ForgotPasswordScreen.this,message, Toast.LENGTH_LONG).show();
                // snackbar.setSnackBarMessage(message);
                Toast.makeText(Profile.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String userId){
//        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"remove_friend?").newBuilder();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"remove_friend?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_FRIEND_ID, userId);
        urlBuilder.addQueryParameter(Constants.PARAM_AUTHTOKEN, auth_token);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyAddFriend(String user_id){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"add_friend?").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("friend_id", user_id);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to handle the contact info layout visibility
     * @param userPhoneNumber
     * @param userEmail
     */
    public void handleContactLayoutVIsibility(String userPhoneNumber, String userEmail){
        try{
            if((userPhoneNumber.equalsIgnoreCase("NA") ||
                    userPhoneNumber.length() == 0) &&
                    userEmail.length() == 0){
                    lay_contactInfo.setVisibility(View.GONE);
                    profileView_divider.setVisibility(View.GONE);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * Function responsible for handling the private profiles
     */
    public void refreshUIforPrivateProfiles(boolean status){

        if(status){
            user_profile_name.setVisibility(View.INVISIBLE);
            lay_contactInfo.setVisibility(View.INVISIBLE);
            profileView_divider.setVisibility(View.INVISIBLE);
            profileview_divider2.setVisibility(View.INVISIBLE);
            horizontal_scrollview_profile.setVisibility(View.INVISIBLE);
            games_played_layout.setVisibility(View.INVISIBLE);
            interest_divider.setVisibility(View.INVISIBLE);
            tv_interest.setVisibility(View.INVISIBLE);
        }
    }


    /**
     * Calling the API to getting the sports
     */

    public void Adduser(final String user_id){
        //Displaying loader
        LoaderUtility.handleLoader(Profile.this, true);
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBodyAddFriend(user_id);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                if(!ApplicationUtility.isConnected(Profile.this)){
                    ApplicationUtility.displayNoInternetMessage(Profile.this);
                }else{
                    ApplicationUtility.displayErrorMessage(Profile.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages
                        displayMessage(message);
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                Profile.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            Toast.makeText(Profile.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                LoaderUtility.handleLoader(Profile.this, false); 
            }
        });
    }

}
