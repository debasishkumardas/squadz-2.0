package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.models.VenueModel;
import com.andolasoft.squadz.models.WishListModel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.WishListAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SpNayak on 1/31/2017.
 */

public class MyWishList extends AppCompatActivity {

    public static MyWishList myWishList;
    @InjectView(R.id.back_icon_wishlist_layout)
    RelativeLayout back_icon_wishlist_layout;
    @InjectView(R.id.wishlist_recycler_view)
    RecyclerView wishlist_recycler_view;
    @InjectView(R.id.swipe_refresh_layout_wishlist)
    SwipeRefreshLayout swipe_refresh_layout_wishlist;
    @InjectView(R.id.wishlist_search_icon)
    ImageView wishlist_search_icon;
    @InjectView(R.id.wishlist_search_layout)
    RelativeLayout wishlist_search_layout;
    @InjectView(R.id.back_icon_layout_search_wishlist)
    RelativeLayout back_icon_layout_search_wishlist;
    @InjectView(R.id.wishlist_search_edit_field)
    EditText wishlist_search_edit_field;
    @InjectView(R.id.wishlist_toolbar_layout)
    Toolbar wishlist_toolbar_layout;
    @InjectView(R.id.wishlist_location_icon)
    ImageView wishlist_location_icon;
    ProgressDialog dialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    SnackBar snackBar;
    String user_id = "",auth_token = "";
    WishListAdapter wishListAdapter;
//    private VenueModel venueModel;
//    public static ArrayList<VenueModel> venueModelArrayList;
    ArrayList<String> images_arraylist;

//    public static ArrayList<EventListingModel> eventListingModelArrayList;
//    EventListingModel eventListingModel;
    String userId = null;

    WishListModel wishListModel;
    public static ArrayList<WishListModel> wishListModelArrayList;
    boolean isSoftKeyboardDisplay = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_mywishlist_layout);

        myWishList = this;

        /**
         * Initializing views
         */
        initViews();

        /**
         * Adding Click events on te views
         */
        setClickEvents();

    }

    /**
     * Adding Click events on te views
     */
    public void setClickEvents() {
        //Back button click
        back_icon_wishlist_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        swipe_refresh_layout_wishlist.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                myWishListAPI();
                swipe_refresh_layout_wishlist.setRefreshing(false);
            }
        });

        //Search icon click
        wishlist_search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wishlist_search_layout.setVisibility(View.VISIBLE);
                wishlist_toolbar_layout.setVisibility(View.INVISIBLE);
                wishlist_search_edit_field.setText("");
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        });

        //Search layout back icon click
        back_icon_layout_search_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view2 = getCurrentFocus();
                if (view2 != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view2.getWindowToken(), 0);
                }
                wishlist_search_layout.setVisibility(View.INVISIBLE);
                wishlist_toolbar_layout.setVisibility(View.VISIBLE);
            }
        });

        /**
         * CHECKING IF SOFT KEYBOARD IS UP OR DOWN
         */
        wishlist_search_edit_field.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (keyboardShown(wishlist_search_edit_field.getRootView())) {
                    isSoftKeyboardDisplay = true;
                }
            }
        });

        wishlist_search_edit_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                wishListAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    setDateIntoRecyclerView();
                }
            }
        });

        wishlist_location_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyWishList.this, WishListMapView.class);
                startActivity(intent);
            }
        });
    }

    /**
     * API integration for displaying my wish list data
     */
    public void myWishListAPI() {
        //Handling the loader state
        LoaderUtility.handleLoader(MyWishList.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(MyWishList.this, false);
                if(!ApplicationUtility.isConnected(MyWishList.this)){
                    ApplicationUtility.displayNoInternetMessage(MyWishList.this);
                }else{
                    ApplicationUtility.displayErrorMessage(MyWishList.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread

                //Venue listing
                String id = "", image = "", name = "", latitude = "", longitude = "",
                        review_rating = "", review_count = "", price = "", court_type = "";
                boolean favourite_status = false;

                //Event list
                String skill_level = "",
                        eventDate = "", no_of_participants = "", total_number_players = "", startTime = "", endTime = "",
                        creator_username = "", creator_user_id = "", creator_profile_image = "",
                        actual_price = "", minimum_price = "", eventVisibility = "", event_sport="",list_id = "";
                boolean isfavorite = false;
                ArrayList<String> participantsArray = null;


                String responseData = response.body().string();

                try {
                    //venueModelArrayList = new ArrayList<>();
                    wishListModelArrayList = new ArrayList<WishListModel>();

                    JSONObject wishlistjsonObject = new JSONObject(responseData);
                    JSONArray wishlist_json_Array = wishlistjsonObject.getJSONArray("wish_lists");

                    for (int i = 0; i < wishlist_json_Array.length(); i++) {
                        JSONObject wishlist_jsonObject = wishlist_json_Array.getJSONObject(i);

                        if(wishlist_jsonObject.has("object_type") &&
                                !wishlist_jsonObject.isNull("object_type")) {
                            String object_type = wishlist_jsonObject.getString("object_type");
                            if(object_type != null &&
                                    object_type.equalsIgnoreCase("List")) {
                                //Venue listing
                                images_arraylist = new ArrayList<String>();
                                //venueModel = new VenueModel();
                                wishListModel = new WishListModel();
                                if (wishlist_jsonObject.has("id") && !wishlist_jsonObject.isNull("id")) {
                                    id = wishlist_jsonObject.getString("id");
                                }
                                if (wishlist_jsonObject.has("name") &&
                                        !wishlist_jsonObject.isNull("name")) {
                                    name = wishlist_jsonObject.getString("name");
                                }
                                if (wishlist_jsonObject.has("latitude") &&
                                        !wishlist_jsonObject.isNull("latitude")) {
                                    latitude = wishlist_jsonObject.getString("latitude");
                                }
                                if (wishlist_jsonObject.has("longitude") &&
                                        !wishlist_jsonObject.isNull("longitude")) {
                                    longitude = wishlist_jsonObject.getString("longitude");
                                }
                                if (wishlist_jsonObject.has("review_rating") &&
                                        !wishlist_jsonObject.isNull("review_rating")) {
                                    review_rating = wishlist_jsonObject.getString("review_rating");
                                }
                                if (wishlist_jsonObject.has("review_count") &&
                                        !wishlist_jsonObject.isNull("review_count")) {
                                    review_count = wishlist_jsonObject.getString("review_count");
                                }
                                if (wishlist_jsonObject.has("price") &&
                                        !wishlist_jsonObject.isNull("price")) {
                                    price = wishlist_jsonObject.getString("price");
                                }
                                if(wishlist_jsonObject.has("is_favorite") &&
                                        !wishlist_jsonObject.isNull("is_favorite")) {
                                    favourite_status = wishlist_jsonObject.getBoolean("is_favorite");
                                }

                                if(wishlist_jsonObject.has("image")&&
                                        !wishlist_jsonObject.isNull("image")) {
                                    JSONArray images_json_array = wishlist_jsonObject.getJSONArray("image");
                                    for (int j = 0; j < images_json_array.length(); j++) {
                                        images_arraylist.add(images_json_array.getString(j));
                                    }
                                    wishListModel.setImages_array(images_arraylist);
                                }

                                wishListModel.setId(id);
                                wishListModel.setImage(image);
                                wishListModel.setName(name);
                                wishListModel.setLatitude(latitude);
                                wishListModel.setLongitude(longitude);
                                wishListModel.setRatings(review_rating);
                                wishListModel.setReview_count(review_count);
                                wishListModel.setPrice(price);
                                wishListModel.setFavorite(favourite_status);
                                wishListModel.setObject_type(object_type);
                                wishListModelArrayList.add(wishListModel);
                            } else if(object_type.equalsIgnoreCase("Event")
                                    || object_type.equalsIgnoreCase("game")
                                    || object_type.equalsIgnoreCase("Game")) {

                                wishListModel = new WishListModel();
                                if (wishlist_jsonObject.has("id") &&
                                        !wishlist_jsonObject.isNull("id")) {
                                    id = wishlist_jsonObject.getString("id");
                                }
                                if (wishlist_jsonObject.has("image") &&
                                        !wishlist_jsonObject.isNull("image")) {
                                    image = wishlist_jsonObject.getString("image");
                                }
                                if (wishlist_jsonObject.has("name") &&
                                        !wishlist_jsonObject.isNull("name")) {
                                    name = wishlist_jsonObject.getString("name");
                                }
                                if (wishlist_jsonObject.has("latitude") &&
                                        !wishlist_jsonObject.isNull("latitude")) {
                                    latitude = wishlist_jsonObject.getString("latitude");
                                }
                                if (wishlist_jsonObject.has("longitude") &&
                                        !wishlist_jsonObject.isNull("longitude")) {
                                    longitude = wishlist_jsonObject.getString("longitude");
                                }
                                if (wishlist_jsonObject.has("review_rating") &&
                                        !wishlist_jsonObject.isNull("review_rating")) {
                                    review_rating = wishlist_jsonObject.getString("review_rating");
                                }
                                if (wishlist_jsonObject.has("review_count") &&
                                        !wishlist_jsonObject.isNull("review_count")) {
                                    review_count = wishlist_jsonObject.getString("review_count");
                                }

                                if (wishlist_jsonObject.has("no_of_player") &&
                                        !wishlist_jsonObject.isNull("no_of_player")) {
                                    total_number_players = wishlist_jsonObject.getString("no_of_player");
                                }

                                if (wishlist_jsonObject.has("no_of_partitipants") &&
                                        !wishlist_jsonObject.isNull("no_of_partitipants")) {
                                    no_of_participants = wishlist_jsonObject.getString("no_of_partitipants");
                                }

                                if (wishlist_jsonObject.has("sport_name") &&
                                        !wishlist_jsonObject.isNull("sport_name")) {
                                    event_sport = wishlist_jsonObject.getString("sport_name");
                                }

                                if (wishlist_jsonObject.has("price") && !wishlist_jsonObject.isNull("price")) {

                                if (wishlist_jsonObject.has("price") &&
                                        !wishlist_jsonObject.isNull("price")) {
                                    price = wishlist_jsonObject.getString("price");
                                    try {
                                        int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                                        float price_pre_player = Float.parseFloat(price) / (float) totalPlayer;
                                        actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                                        float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                                        minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                                    } catch (Exception ex) {
                                        ex.printStackTrace();

                                    }
                                }

                                if (wishlist_jsonObject.has("skill_level") && !wishlist_jsonObject.isNull("skill_level")) {
                                    skill_level = wishlist_jsonObject.getString("skill_level");
                                }

                                if (wishlist_jsonObject.has("date") && !wishlist_jsonObject.isNull("date")) {
                                    eventDate = wishlist_jsonObject.getString("date");
                                }


                                if (wishlist_jsonObject.has("user_name") && !wishlist_jsonObject.isNull("user_name")) {
                                    creator_username = wishlist_jsonObject.getString("user_name");
                                }

                                if (wishlist_jsonObject.has("user_id") && !wishlist_jsonObject.isNull("user_id")) {
                                    creator_user_id = wishlist_jsonObject.getString("user_id");
                                }

                                if (wishlist_jsonObject.has("user_profile_image") && !wishlist_jsonObject.isNull("user_profile_image")) {
                                    creator_profile_image = wishlist_jsonObject.getString("user_profile_image");
                                }

                                if (wishlist_jsonObject.has("user_profile_image") && !wishlist_jsonObject.isNull("user_profile_image")) {
                                    creator_profile_image = wishlist_jsonObject.getString("user_profile_image");
                                }

                                if (wishlist_jsonObject.has("is_favorite") && !wishlist_jsonObject.isNull("is_favorite")) {
                                    isfavorite = wishlist_jsonObject.getBoolean("is_favorite");
                                }
                                    if (wishlist_jsonObject.has("list_id") && !wishlist_jsonObject.isNull("list_id")) {
                                        list_id = wishlist_jsonObject.getString("list_id");
                                    }


                                if(wishlist_jsonObject.has("time_slots")&& !wishlist_jsonObject.isNull("time_slots")) {
                                    String start_time = null;
                                    JSONArray time_slot_array_json_array = wishlist_jsonObject.getJSONArray("time_slots");
                                    for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                                        JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                                        if(j == 0){
                                            startTime = getValues.getString("start_time");
                                        }
                                        endTime = getValues.getString("end_time");
                                    }
                                }

                                if (wishlist_jsonObject.has("image") && !wishlist_jsonObject.isNull("image")) {
                                    ArrayList<String> images_arraylist = new ArrayList<String>();
                                    JSONArray images_json_array = wishlist_jsonObject.getJSONArray("image");
                                    for (int j = 0; j < images_json_array.length(); j++) {
                                        images_arraylist.add(images_json_array.getString(j));
                                    }
                                    wishListModel.setImages_array(images_arraylist);
                                }


                                if (wishlist_jsonObject.has("visibility") && !wishlist_jsonObject.isNull("visibility")) {
                                    eventVisibility = wishlist_jsonObject.getString("visibility");
                                }

                                if (wishlist_jsonObject.has("partitipant_ids") && !wishlist_jsonObject.isNull("partitipant_ids")) {
                                    participantsArray = new ArrayList<String>();
                                    JSONArray images_json_array = wishlist_jsonObject.getJSONArray("partitipant_ids");
                                    for (int j = 0; j < images_json_array.length(); j++) {
                                        participantsArray.add(images_json_array.getString(j));
                                    }
                                    //eventListingModel.setImages_array(participantsArray);
                                }

                                wishListModel.setId(id);
                                wishListModel.setImage(image);
                                wishListModel.setName(name);
                                wishListModel.setLatitude(latitude);
                                wishListModel.setLongitude(longitude);
                                wishListModel.setRatings(review_rating);
                                wishListModel.setReview_count(review_count);
                                wishListModel.setPrice(actual_price);
                                wishListModel.setSkill_level(skill_level);
                                wishListModel.setEventDate(eventDate);
                                wishListModel.setTotal_Participants(no_of_participants);
                                wishListModel.setEventStartTime(startTime);
                                wishListModel.setEventEndTime(endTime);
                                wishListModel.setCapacity(total_number_players);
                                wishListModel.setCreator_name(creator_username);
                                wishListModel.setCreator_user_id(creator_user_id);
                                wishListModel.setCreator_Image(creator_profile_image);
                                wishListModel.setFavorite(isfavorite);
                                wishListModel.setMinimum_Price(minimum_price);
                                wishListModel.setObject_type(object_type);
                                wishListModel.setSports_Type(event_sport);
                                wishListModel.setEvent_Total_Price(price);
                                wishListModel.setList_id(list_id);
                                wishListModelArrayList.add(wishListModel);
                            }
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(MyWishList.this, false);
                        Constants.FAVOURITE_CLICKED = false;
                        if(wishListModelArrayList.size() > 0) {
                            enableViews(true);
                            /**Storing wish count in Shared preferences*/
                            saveWishListCount();

                            wishListAdapter = new WishListAdapter(MyWishList.this,wishListModelArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            wishlist_recycler_view.setLayoutManager(mLayoutManager);
                            wishlist_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            wishlist_recycler_view.setAdapter(wishListAdapter);
                        } else{
                            enableViews(false);
                            /**Storing wish count in Shared preferences*/
                            saveWishListCount();

                            snackBar.setSnackBarMessage("No wish list available");
                        }
                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_wish_lists").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("user_id", userId);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(MyWishList.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Initializing views
     */
    public void initViews()
    {
        ButterKnife.inject(this);
        snackBar = new SnackBar(this);
        myWishList = this;
        preferences = PreferenceManager
                .getDefaultSharedPreferences(MyWishList.this);
        editor = preferences.edit();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //Change the Swipe layout color
        swipe_refresh_layout_wishlist.setColorSchemeResources(R.color.orange,
                R.color.orange,
                R.color.orange);

        if(preferences.contains("loginuser_id"))
        {
            userId = preferences.getString("loginuser_id","");
            auth_token = preferences.getString("auth_token","");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        //My Wishlist API
        myWishListAPI();
    }

    private boolean keyboardShown(View rootView) {

        final int softKeyboardHeight = 100;
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
        int heightDiff = rootView.getBottom() - r.bottom;
        return heightDiff > softKeyboardHeight * dm.density;
    }

    /**
     * Set adapter
     */
    public void setDateIntoRecyclerView()
    {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        wishlist_recycler_view.setLayoutManager(mLayoutManager);
        wishlist_recycler_view.setItemAnimator(new DefaultItemAnimator());
        wishlist_recycler_view.setAdapter(wishListAdapter);
        wishListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if(isSoftKeyboardDisplay) {
            isSoftKeyboardDisplay = false;
            wishlist_search_layout.setVisibility(View.INVISIBLE);
            wishlist_toolbar_layout.setVisibility(View.VISIBLE);

            //Setting Adapter to Recycler view
            setDateIntoRecyclerView();
        }
        else {
            super.onBackPressed();
            finish();
        }
    }

    /**
     * Function to handle the clicks on the toolbarbuttons
     *
     * @param status
     */
    public void enableViews(boolean status) {
        if (status) {
            wishlist_search_icon.setFocusable(true);
            wishlist_search_icon.setClickable(true);
        } else {
            wishlist_search_icon.setFocusable(false);
            wishlist_search_icon.setClickable(false);
        }
    }
    /**
     * Storing wish count in Shared preferences
     */
    public void saveWishListCount()
    {
        editor.putString("WISHLIST_COUNT",String.valueOf(wishListModelArrayList.size()));
        editor.apply();
    }
}
