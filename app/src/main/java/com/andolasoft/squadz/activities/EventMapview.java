package com.andolasoft.squadz.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.managers.LocationPickerManager;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EventMapview extends AppCompatActivity implements OnMapReadyCallback {

    @InjectView(R.id.spinner_event_map_list)
    Spinner spinner_venue_map_list;
    @InjectView(R.id.back_icon_venue_mapview_layout)
    RelativeLayout ic_back_layout;
    @InjectView(R.id.book_icon_venue)
    ImageView ic_back_list;
    @InjectView(R.id.venue_filter_icon)
    ImageView imgv_filter;
    private boolean gps_enabled = false;
    private GPSTracker gpsTracker;
    private Double current_lat, current_long;
    private View rootView;
    private MapView mapView;
    Marker venueMark;
    List<Marker> markers = new ArrayList<Marker>();
    Dialog dialog;
    SnackBar snackBar = new SnackBar(EventMapview.this);
    private GoogleMap mMap;
    String venueImage, venueReviews, venueAmount, venueReviewsCount, venueName;
    double venueMiles;
    View v;
    ArrayList<String> currLocItems;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    JSONArray event_listing_array = null;
    public static ArrayList<EventListingModel> eventListingModelArrayList;
    EventListingModel eventListingModel;
    String userId = null;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_event_mapview);
        ButterKnife.inject(this);


        pref = PreferenceManager.getDefaultSharedPreferences(EventMapview.this);
        userId = pref.getString("loginuser_id", null);
        //Setting the address in the view
        setVenueAppletonType();

        /**
         * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
         */
        isLocationEnabled();

        initMapGps();

        //Registering the Clcik events
        setOnClickEvents();
    }

    /**
     * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
     *
     * @return
     */
    public boolean isLocationEnabled() {
        gps_enabled = false;
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gps_enabled;
    }


    /**
     * INITIALIZING MAP WITH GPS TRACKER
     */
    public void initMapGps() {
        gpsTracker = new GPSTracker(this);

        if (!gps_enabled) {
            Toast.makeText(EventMapview.this, "please enable your location service", Toast.LENGTH_SHORT).show();
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
        } else {
            if (gpsTracker.canGetLocation()) {
                current_lat = gpsTracker.getLatitude();
                current_long = gpsTracker.getLongitude();
            }
        }
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (gps_enabled) {
            LatLng lat_lng = new LatLng(current_lat, current_long);
            // mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory
            //   .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)).position(lat_lng).title(current_lat.toString() + "," + current_long.toString()));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat_lng, 15));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.isMyLocationEnabled();
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.clear();
            //Placing the markers in the map view
            setMarkers();
            //mMap.setMyLocationEnabled(true);

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @SuppressLint("InflateParams")
                public View getInfoContents(final Marker arg0) {
                    v = getLayoutInflater().inflate(
                            R.layout.map_info_window, null);
                    try{

                        //Initializing the Views
                        ImageView imgv_venue_image = (ImageView) v.findViewById(R.id.venue_image_infowindow);
                        TextView tv_venueMile = (TextView) v.findViewById(R.id.text_venue_miles);
                        TextView tv_venuerating = (TextView) v.findViewById(R.id.rating_venue_reviews);
                        TextView tv_venuePrice = (TextView) v.findViewById(R.id.venue_price);
                        TextView tv_Venue_name = (TextView) v.findViewById(R.id.venue_name);
                        RatingBar ratReviewBar = (RatingBar) v.findViewById(R.id.rating_venue);

                        //tv_venueMile.setText("0.4 miles");
                        tv_venuerating.setText(venueReviewsCount+" Reviews");
                        tv_Venue_name.setText(venueName);
                        tv_venueMile.setText(String.valueOf(venueMiles + " miles away"));
                        if(!TextUtils.isEmpty(venueAmount) &&
                                !venueAmount.equalsIgnoreCase("FREE") &&
                                !venueAmount.equalsIgnoreCase("0")){
                            tv_venuePrice.setText("$ "+venueAmount+"/ Player");
                        }else{
                            tv_venuePrice.setText("FREE");
                        }
                        ratReviewBar.setRating(Float.valueOf(venueReviews));

                        if(!TextUtils.isEmpty(venueImage)
                                && !venueImage.equalsIgnoreCase("")){
                            Glide.with(EventMapview.this).load(venueImage).asBitmap().override(200,150).listener(new RequestListener<String, Bitmap>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                    e.printStackTrace();
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    if(!isFromMemoryCache) arg0.showInfoWindow();
                                    return false;
                                }
                            }).into(imgv_venue_image);
                        }
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }

                    return v;
                }
            });


            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String title = marker.getTitle();
                    setMarketInfoWindow(title);
                    return false;
                }
            });

            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    String title = marker.getTitle();
                   // getVenuesCourtsDetailsInfor(title);
                }
            });

            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    //mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 15));
                    // mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

                    if (markers.size() != 0) {
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (Marker marker : markers) {
                            builder.include(marker.getPosition());
                        }
                        LatLngBounds bounds = builder.build();
                        int padding = 150; // offset from edges of the map in
                        // pixels
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(
                                bounds, padding);
                        mMap.moveCamera(cu);
                        markers.clear();
                    }
                }
            });
        }
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(EventMapview.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }


    public void setMarkers(){
        for(int i = 0; i < EventListing.eventListingModelArrayList.size(); i++){
            String venuelat = EventListing.eventListingModelArrayList.get(i).getLatitude();
            String venuelon = EventListing.eventListingModelArrayList.get(i).getLongitude();
            String venueId = EventListing.eventListingModelArrayList.get(i).getEvent_Id();

            if(!TextUtils.isEmpty(venuelat) ||
                    !TextUtils.isEmpty(venuelon)){
                LatLng pinLocation = new LatLng(Double.valueOf(venuelat),Double.valueOf(venuelon));

                venueMark = mMap
                        .addMarker(new MarkerOptions()
                                .position(pinLocation)
                                .title(venueId)
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                markers.add(venueMark);
            }
        }
    }

    /**
     * Added click events on the views
     */
    public void setOnClickEvents() {

        //Action bar back icon click
        ic_back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //VenueListing.isApiCalled = false;
                finish();
                //Intent intent_availability = new Intent(EventMapview.this, VenueListing.class);
                //startActivity(intent_availability);
            }
        });

        ic_back_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // VenueListing.isApiCalled = false;
                EventMapview.this.finish();
            }
        });

        imgv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_availability = new Intent(EventMapview.this, Filter.class);
                startActivity(intent_availability);
            }
        });

        spinner_venue_map_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(VenueListing.this, "Catch it!", Toast.LENGTH_SHORT).show();
                    LocationPickerManager locationPickerManager
                            = new LocationPickerManager(EventMapview.this);
                }
                return true;
            }
        });


    }

    /**
     * KEEPING VENUE APPLETON NAME INSIDE THE SPINNER
     */
    public void setVenueAppletonType() {

       /* //Getting venue name & id as a array to display inside the spinner
        spinnerArray = new String[venue_spinner_id_array.size()];
        map_venue_name_spiner = new HashMap<String, String>();

        for (int i = 0; i < venue_spinner_id_array.size(); i++) {
            map_venue_name_spiner.put(venue_spinner_name_array.get(i), venue_spinner_id_array.get(i));
            spinnerArray[i] = venue_spinner_name_array.get(i);
        }*/

        currLocItems = new ArrayList<>();
        currLocItems.add(HomeFragment.current_city);
        //Displaying name inside spinner
        displaySpinnerInfo(currLocItems);
    }

    /**
     * Displaying name inside spinner
     */
    public void displaySpinnerInfo(ArrayList<String> spinnerArray) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(EventMapview.this, android.R.layout.simple_spinner_item, spinnerArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_venue_map_list.setAdapter(dataAdapter);
        spinner_venue_map_list.setSelection(0);
    }


    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Google", "Place Selected: " + place.getName());
                HomeFragment.address = place.getAddress().toString();
                String latLng = place.getLatLng().toString();
                String[] latlngArr = latLng.split(" ");
                String latlngVal = latlngArr[1].substring(1, latlngArr[1].length()-1);
                String[] finalArray =  latlngVal.split(",");
                HomeFragment.currLocLatitude = Double.parseDouble(finalArray[0].toString());
                HomeFragment.currLocLongitude = Double.parseDouble(finalArray[1].toString());
                LocationPickerManager.getCurrentCity(EventMapview.this);
                setVenueAppletonType();
                getEventListing();
                // Display attributions if required.
                CharSequence attributions = place.getAttributions();
                if (!TextUtils.isEmpty(attributions)) {
                    //mPlaceAttribution.setText(Html.fromHtml(attributions.toString()));
                } else {
                    //mPlaceAttribution.setText("");
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("Google", "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }
    }


    /**
     * Function to get Event listing from server
     */
    public void getEventListing() {
        //Handling the loader state
        LoaderUtility.handleLoader(EventMapview.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(EventMapview.this, false);
                if(!ApplicationUtility.isConnected(EventMapview.this)){
                    ApplicationUtility.displayNoInternetMessage(EventMapview.this);
                }else{
                    ApplicationUtility.displayErrorMessage(EventMapview.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", image = "", name = "", latitude = "", longitude = "",
                        review_rating = "", review_count = "", price = "", court_type = "", eventVisibility = "";
                ArrayList<String> participantsArray = null;

                try {
                    eventListingModelArrayList = new ArrayList<>();
                    event_listing_array = new JSONArray(responseData);

                    for (int i = 0; i < event_listing_array.length(); i++) {
                        eventListingModel = new EventListingModel();

                        JSONObject responseObject = event_listing_array.getJSONObject(i);

                        if (responseObject.has("id") && !responseObject.isNull("id")) {
                            id = responseObject.getString("id");
                        }
                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            image = responseObject.getString("image");
                        }
                        if (responseObject.has("name") && !responseObject.isNull("name")) {
                            name = responseObject.getString("name");
                        }
                        if (responseObject.has("latitude") && !responseObject.isNull("latitude")) {
                            latitude = responseObject.getString("latitude");
                        }
                        if (responseObject.has("longitude") && !responseObject.isNull("longitude")) {
                            longitude = responseObject.getString("longitude");
                        }
                        if (responseObject.has("review_rating") && !responseObject.isNull("review_rating")) {
                            review_rating = responseObject.getString("review_rating");
                        }
                        if (responseObject.has("review_count") && !responseObject.isNull("review_count")) {
                            review_count = responseObject.getString("review_count");
                        }
                        if (responseObject.has("price") && !responseObject.isNull("price")) {
                            price = responseObject.getString("price");
                        }

                        if (responseObject.has("visibility") && !responseObject.isNull("visibility")) {
                            eventVisibility = responseObject.getString("visibility");
                        }

                        if (responseObject.has("partitipant_ids") && !responseObject.isNull("partitipant_ids")) {
                            participantsArray = new ArrayList<String>();
                            JSONArray images_json_array = responseObject.getJSONArray("partitipant_ids");
                            for (int j = 0; j < images_json_array.length(); j++) {
                                participantsArray.add(images_json_array.getString(j));
                            }
                            //eventListingModel.setImages_array(participantsArray);
                        }

                        if(eventVisibility != null &&
                                eventVisibility.equalsIgnoreCase("Private") &&
                                participantsArray.contains(userId)){
                            eventListingModel.setEvent_Id(id);
                            eventListingModel.setEvent_Image(image);
                            eventListingModel.setEvent_Name(name);
                            eventListingModel.setLatitude(latitude);
                            eventListingModel.setLongitude(longitude);
                            eventListingModel.setEvent_Ratings(review_rating);
                            eventListingModel.setEvent_Reviews(review_count);
                            eventListingModel.setEvent_Price(price);

                            eventListingModelArrayList.add(eventListingModel);
                        }else if(eventVisibility != null &&
                                eventVisibility.equalsIgnoreCase("Public")){
                            eventListingModel.setEvent_Id(id);
                            eventListingModel.setEvent_Image(image);
                            eventListingModel.setEvent_Name(name);
                            eventListingModel.setLatitude(latitude);
                            eventListingModel.setLongitude(longitude);
                            eventListingModel.setEvent_Ratings(review_rating);
                            eventListingModel.setEvent_Reviews(review_count);
                            eventListingModel.setEvent_Price(price);
                        }

                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                EventMapview.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(EventMapview.this, false);

                        //Keeping vebue names inside spinner
                        //setVenueAppletonType();

                        if(eventListingModelArrayList != null &&
                                eventListingModelArrayList.size() != 0) {
                            if (event_listing_array.length() > 0) {
                                handleClicks(true);
                                mMap.clear();
                                //Placing the markers in the map view
                                setMarkers();
                            }else{
                                handleClicks(false);
                                mMap.clear();
                                snackBar.setSnackBarMessage("No courts found");
                            }
                        }
                        else {
                            handleClicks(false);
                            mMap.clear();
                            snackBar.setSnackBarMessage("No Events found");
                        }
                    }
                });


            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_venue_games").newBuilder();
        urlBuilder.addQueryParameter("location", String.valueOf(HomeFragment.currLocLatitude+","+ HomeFragment.currLocLatitude));
        urlBuilder.addQueryParameter("sportid", Constants.SPORTS_ID);
        urlBuilder.addQueryParameter("sportname", Constants.SPORTS_NAME);
        urlBuilder.addQueryParameter("current_city", HomeFragment.current_city);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }

    /**
     *
     * @param courtId
     */
    public void setMarketInfoWindow(String courtId){
        try{
            for(int i = 0; i < EventListing.eventListingModelArrayList.size(); i++){
                String venueId = EventListing.eventListingModelArrayList.get(i).getEvent_Id();
                if(venueId.equalsIgnoreCase(courtId)){
                    ArrayList<String> image_array = EventListing.eventListingModelArrayList.
                            get(i).getImages_array();
                    venueImage = image_array.get(0);
                    venueReviews = EventListing.eventListingModelArrayList.get(i).getEvent_Reviews();
                    venueAmount = EventListing.eventListingModelArrayList.get(i).getEvent_Price();

                    String latitude = EventListing.eventListingModelArrayList.get(i).getLatitude();
                    String longitude = EventListing.eventListingModelArrayList.get(i).getLongitude();

                    venueMiles = ApplicationUtility.distance(HomeFragment.currLocLatitude, HomeFragment.
                            currLocLongitude,Double.parseDouble(latitude), Double.parseDouble(longitude));
                    venueReviewsCount = EventListing.eventListingModelArrayList.get(i).getEvent_Reviews();
                    venueName = EventListing.eventListingModelArrayList.get(i).getEvent_Name();
                }
            }
        }catch(Exception ex){
            snackBar.setSnackBarMessage("Unable to get Event data");
        }

    }

    public void handleClicks(boolean status){
        if(status){
            imgv_filter.setFocusable(true);
            imgv_filter.setClickable(true);
        }else{
            imgv_filter.setFocusable(false);
            imgv_filter.setClickable(false);
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



}
