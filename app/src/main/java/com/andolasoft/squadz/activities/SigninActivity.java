package com.andolasoft.squadz.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import bolts.AppLinks;
import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SigninActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {
    @InjectView(R.id.username) EditText et_username;
    @InjectView(R.id.password) EditText et_password;
    @InjectView(R.id.login_btn)Button btn_login;
    @InjectView(R.id.loginwithfacebook) LoginButton btn_fblogin;
    @InjectView(R.id.forgot_pass) TextView tv_forgotpassword;
    @InjectView(R.id.signup)TextView btn_signup;
    ProgressDialog dialog = null;
    private ProfileTracker profileTracker;
    CallbackManager callbackManager ;
    private AccessTokenTracker accessTokenTracker;
    public static String android_device_id = null;
    public static String fb_user_id, fb_user_name, fb_user_email, fb_user_image,
            fb_user_gender, fb_user_firstname, fb_user_lastname;
    public static final String FACEBOOK_API_CALL_PERMISSIONs = "id,email,gender,name, first_name, last_name, birthday";
    SnackBar snackbar = new SnackBar(SigninActivity.this);
    protected LocationRequest locationRequest;
    int REQUEST_CHECK_SETTINGS = 100;
    GPSTracker gpsTracer;
    protected GoogleApiClient mGoogleApiClient;
    private static final int PERMISSION_REQUEST_CODE = 1;
    int deviceAndroidVerion = 0, badgeCount = 0;
    public static double currLocLatitude, currLocLongitude;
    SharedPreferences preferences;;
    public static String FCM_REGISTRATION_ID = null;
    String err_msg = null;
    String msg = null;
    public static SigninActivity mcontext;
    String logInType = "",login_userId = "";
    String auth_token = null;
    String called_time = null, user_id = null, userId = null;
    GPSTracker gpsTracker;


    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken.getCurrentAccessToken().getToken();
            AccessToken accessToken = loginResult.getAccessToken();
           /* Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);*/

            if(Profile.getCurrentProfile() == null) {
                profileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                        // profile2 is the new profile
                        Log.v("facebook - profile", profile2.getFirstName());
                        profileTracker.stopTracking();
                    }
                };
            }
            else {
                Profile profile = Profile.getCurrentProfile();
            }
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        FacebookSdk.sdkInitialize(getApplicationContext());

        Uri targetUrl =
                AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
        if (targetUrl != null) {
            Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
        }
        setContentView(R.layout.activity_signin);

        ButterKnife.inject(this);

        //Getting the application context
        mcontext = this;

        //Hiding the ActionBar
        hideActionBar();

        //Hiding the SoftKeyboard
        hideSoftKeyboard();

        //Configring the Facebook permissions
        configureFacebookLoginButton();

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(SigninActivity.this);

        gpsTracker = new GPSTracker(SigninActivity.this);

        //Getting the android Version from the device
        deviceAndroidVerion = ApplicationUtility.getDeviceInstalledAndroidVersion();

        //requesting permission for Android 6.0
        if(deviceAndroidVerion >= 23){
            boolean value = checkLocationPermission();
            if (!checkLocationPermission()) {
                //Requesting permission fop Location
                requestPermissionAccessLocation();
            }else{
                //Initializing the Google API client
                initializeGoogleClient();
            }
        }else{
            //Initializing the Google API client
            initializeGoogleClient();
        }


        //Click event for the login button
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Getting the internet connection status
                boolean isConnected = ApplicationUtility.isConnectingToInternet(SigninActivity.this);
                
                if(isConnected){
                    // Validating user entered data
                    validateUserData();    
                }else{
                    Toast.makeText(SigninActivity.this, "Please connect to a working internet connection and try again", Toast.LENGTH_SHORT).show();
                }
                
            }
        });

        //Click evet for the forgotPassword text view
        tv_forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SigninActivity.this, ForgotPasswordScreen.class);
                startActivity(intent);
            }
        });

        //Integrating click event on the keyboard done button
        et_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    // Validating user entered data
                    validateUserData();
                }
                return false;
            }
        });


        //Integrating the click event to the sign up button
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SigninActivity.this, NormalRegistrationStepOne.class);
                startActivity(intent);
            }
        });


        //Function to handle the facebook callbacks
        facebookStratTracking();

        //Function Call to get the Fcm Id
        getFcmId();

    }

    /**
     * Function to get the permissions statud for Android 6.0 Onwards
     *
     * @return
     */
    private boolean checkLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function to request Permission
     *
     * @return
     */
    private void requestPermissionAccessLocation() {
        final String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION};
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("To Display events and games around you. You need to give permission to access your location");
            builder.setTitle("Access Location");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(SigninActivity.this, permissions, PERMISSION_REQUEST_CODE);
                }
            });
            builder.show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }



    /**
     * Handling the permission Status
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Checking for the location Permissions
                    if (!checkLocationPermission()) {
                        //Requesting Location Permissions
                        //requestPermissionAccessLocation();
                    }  else {
                        //Initializing the Google API client
                        initializeGoogleClient();
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }




    /**
     * Validing the User inputted data
     */
    public void validateUserData(){
        SnackBar snackBar = new SnackBar(SigninActivity.this);
        String mUsername = et_username.getText().toString().trim();
        String mPassword = et_password.getText().toString().trim();

        //Checking the validation for the user entered data
        if(TextUtils.isEmpty(mUsername)){
            snackBar.setSnackBarMessage("Please enter Username");
        }else if(TextUtils.isEmpty(mPassword)){
            snackBar.setSnackBarMessage("Please enter Password");
        }else{
            userLoginTask(mUsername,mPassword);
        }
    }

    /**
     * Function to handle the facebook callbacks
     */
    public void facebookStratTracking(){
        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
                AccessToken accessToken = newToken;
                Profile profile = Profile.getCurrentProfile();
                displayMessage(profile);
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
    }

    /**
     * Hiding the ActionBar
     */
    public void hideActionBar(){
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.hide();
    }

    /**
     * Hiding the Key board
     */
    public void hideSoftKeyboard() {
        SigninActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    /**
     * Function to format the Mobile Number
     * @param mob_num Mobile Number to be formatted
     * @return Formatted Mobile Number
     */
    public String formatMobileNumber(String mob_num){
        String mobNumber = null;
        if(mob_num.contains("-")){
            String []mob_num_arr = mob_num.split("-");
            mobNumber = mob_num_arr[1].replace(" ","");
        }else{
            mobNumber = mob_num;
        }
        return mobNumber;
    }


    public void sportApiRequest(){
       final ProgressDialog dialog = ProgressDialog
                .show(SigninActivity.this, "", "Please wait ...");
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setCancelable(false);

        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = new Request.Builder()
                .url("http://54.164.124.188/api/v1/get_sports").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE_MIGRATION+"get_sports").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialog.dismiss();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // ... check for failure using `isSuccessful` before proceeding
                dialog.dismiss();
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    Log.d("Response",responseObject.toString());
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                SigninActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(SigninActivity.this, "Got the response", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    /**
     * Function to handle the user login task
     * @param userName
     * @param passWord
     */
    public void userLoginTask(String userName, String passWord){

        //Handling the loader state
        LoaderUtility.handleLoader(SigninActivity.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(userName,passWord);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(SigninActivity.this, false); 
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    err_msg = responseObject.getString("status");
                    if (err_msg != null && err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                        //Parsing the data from the JSON object
                        auth_token = responseObject.getString("auth_token");
                        String device_info_id = responseObject.getString("device_info_id");
                        String user_id = responseObject.getString("user_id");
                        login_userId = user_id;
                        userId = user_id;
                        String phone_number = responseObject.getString("phone_number");
                        String firstname = responseObject.getString("first_name");
                        String lastname = responseObject.getString("last_name");
                        //String current_status = responseObject.getString("current_status");
                        String user_email = responseObject.getString("email");
                        String birth_day = responseObject.getString("birth_date");
                        String image = responseObject.getString("image");
                        String user_sport = responseObject.getString("sport");
                        String userName = responseObject.getString("username");
                        boolean privacy_show_full_name = responseObject.getBoolean("show_fullname");
                        boolean privacy_show_contacts = responseObject.getBoolean("show_contact");
                        boolean privacy_show_public = responseObject.getBoolean("show_public");
                        /**
                         * Comment for api migration (Sp nayak 13-05-2017)
                         */
                        /*int game_invitation_count = responseObject
                                .getInt("game_invitation_count");
                        int friend_invitation_count = responseObject
                                .getInt("friend_invitation_count");
                        boolean recive_push_notification = (Boolean) responseObject
                                .get("receive_push_notification");
                        JSONObject array = responseObject.getJSONObject("notifications");
                        boolean player_request = (Boolean) array.get("player_request");
                        boolean game_invitation = (Boolean) array.get("game_invitation");
                        boolean nearby_games = (Boolean) array.get("nearby_game");
                        boolean nearby_games_all = (Boolean) array.get("nearby_game_all");
                        boolean nearby_games_primary = (Boolean) array
                                .get("nearby_game_primary");
                        boolean message_board = (Boolean) array.get("message_board");
                        boolean game_update = (Boolean) array.get("game_update");
                        boolean comment_on_post = (Boolean) array.get("comment_on_post");
                        boolean game_cancellation = (Boolean) array.get("game_cancel");
                        boolean game_edit = (Boolean) array.get("game_edit");
                        boolean game_join_leave = (Boolean) array.get("game_join_leave");*/

                        //Keeping the login status either from Facebook or Normal login
                        logInType = "NORMAL_LOGIN";

                        //Storing the data in the Shatred Preferance
                        storeDatainSharedPreferance(auth_token,userName,device_info_id,
                                user_id, firstname,lastname, image, user_email,phone_number,
                                user_sport, null);

                        //Storing the user privacy data in Shared Preferance
                        storeDatainSharedPreferance(privacy_show_full_name, privacy_show_contacts,
                                privacy_show_public);

                        /**
                         * Comment for api migration (Sp nayak 13-05-2017)
                         */
                       /* //Store Notification data in shared preferences
                        storeNotificationDataInSharedPref(player_request,
                                nearby_games, game_invitation, game_update,
                                message_board, comment_on_post, game_cancellation,
                                game_edit, game_join_leave, nearby_games_primary,
                                nearby_games_all);*/

                        //Stroing the user info for quick access
                        userInfoQuickAccess(auth_token,user_id,userName);

                    } else {
                        msg = responseObject.getString("message");
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                SigninActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Hiding the soft key board after successful login
                         hideSoftKeyboard();

                        if(err_msg != null &&
                                err_msg.equalsIgnoreCase("success")){
                            //Toast.makeText(SigninActivity.this, "Login Successful", Toast.LENGTH_LONG).show();
                            //Handling the page redirection
                            getUserFriends(auth_token);
                        }else{
                            snackbar.setSnackBarMessage(msg);
                            //Handling the loader state
                            LoaderUtility.handleLoader(SigninActivity.this, false); 
                        }
                    }
                });
            }
        });
    }

    /**
     * Redirect the user to the next screen
     */
    public void redirectUser(){
        SigninActivity.this.finish();
        Intent intent = new Intent(SigninActivity.this,NavigationDrawerActivity.class);
        startActivity(intent);
    }

    /**
     * Storing the user details to get a quick access
     * @param auth_token Logged in user auth token
     * @param user_id Logged in User user id
     * @param user_name logged in user user name
     */
    public void userInfoQuickAccess(String auth_token, String user_id,
                                     String user_name){
        AppConstants.AUTH_TOKEN = auth_token;
        AppConstants.USERNAME = user_name;
        AppConstants.LOGGEDIN_USERID = user_id;
    }

    /**
     * Keeping the user data in the shared preferance after successful login
     * @param auth_token user auth_token
     * @param mUsername user Username
     * @param device_info_id user device infoid
     * @param user_id user user_id
     * @param firstname user firstname
     * @param lastname user lastname
     * @param image user Profile image
     */

    public void storeDatainSharedPreferance(String auth_token,String mUsername,
                                            String device_info_id, String user_id,
                                            String firstname, String lastname, String image,
                                            String userEmail, String userPhone, String userSport,
                                            String facebook_id){
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(SigninActivity.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("auth_token", auth_token);
        editor.putString("USERNAME", mUsername);
        editor.putString("device_info_id", device_info_id);
        editor.putString("loginuser_id", user_id);
        editor.putString("username", mUsername);
        editor.putString("firstname", firstname);
        editor.putString("lastname", lastname);
        editor.putString("image", image);
        editor.putString("user_email", userEmail);
        editor.putString("user_phone", userPhone);
        editor.putString("user_sport", userSport);
        editor.putString("LOGIN_TYPE", logInType);
        editor.putString("Facebook_Id", facebook_id);
        editor.commit();
    }


    public void storeDatainSharedPreferance(boolean privacy_show_fullname,
                                            boolean show_contact_info,
                                            boolean show_profile){
        SharedPreferences sharedpreferences = getSharedPreferences("Privacy_Data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorPrivacy = getSharedPreferences("Privacy_Data", MODE_PRIVATE).edit();
        editorPrivacy.putBoolean("Privacy_show_fullname", privacy_show_fullname);
        editorPrivacy.putBoolean("Privacy_show_contact_info", show_contact_info);
        editorPrivacy.putBoolean("Privacy_show_Profile", show_profile);
        editorPrivacy.commit();
    }

    /**
     * Store Notification data in shared preferences
     */
    public void storeNotificationDataInSharedPref(boolean player_request, boolean nearby_game, boolean game_invitation,
                                                  boolean game_update, boolean message_board, boolean comment_on_post,
                                                  boolean game_cancel, boolean game_edit, boolean game_join_leave,
                                                  boolean nearby_game_primary, boolean nearby_game_all) {

        SharedPreferences preferences = getSharedPreferences("Settings_Data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("auth_token", auth_token);
        editor.putBoolean("PLAYER_REQUEST", player_request);
        editor.putBoolean("NEARBY_GAME", nearby_game);
        editor.putBoolean("GAME_INVITATION", game_invitation);
        editor.putBoolean("GAME_UPDATE", game_update);
        editor.putBoolean("MESSAGE_BOARD", message_board);
        editor.putBoolean("COMMENT_ON_POST", comment_on_post);
        editor.putBoolean("GAME_CANCEL", game_cancel);
        editor.putBoolean("GAME_EDIT", game_edit);
        editor.putBoolean("GAME_JOIN_LEAVE", game_join_leave);
        editor.putBoolean("ONLY_PRIMARY", nearby_game_primary);
        editor.putBoolean("ALL_GAMES", nearby_game_all);

        editor.apply();
    }


    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(SigninActivity.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String user_name, String password){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"login").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, user_name);
        //urlBuilder.addQueryParameter(Constants.PARAM_EMAIL, user_name);
        urlBuilder.addQueryParameter(Constants.PARAM_PASSWORD, password);
        //urlBuilder.addQueryParameter(Constants.PARAM_FCMID, FCM_REGISTRATION_ID);
        //urlBuilder.addQueryParameter(Constants.PARAM_DEVICETOKEN, getDeviceToken(SigninActivity.this));
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETOKEN, FCM_REGISTRATION_ID);
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETYPE, Constants.DEVICE_TYPE);
        //urlBuilder.addQueryParameter(Constants.PARAM_DEVICETYPE, FCM_REGISTRATION_ID);
        urlBuilder.addQueryParameter(Constants.PARAM_LOCATION, gpsTracker.getLatitude()+ "," + gpsTracker.getLongitude());
        urlBuilder.addQueryParameter(Constants.PARAM_TIMEZONE, getDeviceTimeZone());
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildFriendsApiRequestBody(String authTOken){
//        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"friend_list").newBuilder();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"friend_list").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_AUTHTOKEN, authTOken);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Getting the device current time zone
     * @return device timezone
     */
    public static String getDeviceTimeZone(){
        String device_timezone = null;
        //Getting the current time zone
        try {
            /** get device timezone **/
            TimeZone tz = TimeZone.getDefault();
            String displayname = tz.getDisplayName(false, TimeZone.SHORT);
            device_timezone= tz.getID();
        } catch (Exception e) {

            e.printStackTrace();
        }
        return device_timezone;
    }

    /**
     * Function to get teh Device Token
     * @return Android device Token
     */
    public static String getDeviceToken(Context mContext){
        /** get android device Id **/
        android_device_id = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_device_id;
    }


    /**
     * Configuring all the permissions for the facebook user data retrival
     */
    public void configureFacebookLoginButton(){
        //btn_fblogin.setBackgroundResource(0);
        btn_fblogin.setCompoundDrawablesWithIntrinsicBounds(null, null,
                null, null);
        btn_fblogin.setText("Login with Facebook");
        btn_fblogin.setCompoundDrawablePadding(0);
        btn_fblogin.setReadPermissions("public_profile email");
        callbackManager = CallbackManager.Factory.create();
        btn_fblogin.registerCallback(callbackManager, callback);

    }

    /**
         * Getting data from Facebook
         * @param profile User facebook Profile Object
         */

    private void displayMessage(final Profile profile){
        if(profile != null){
            Bundle params = new Bundle();
            params.putString("fields", FACEBOOK_API_CALL_PERMISSIONs);
            new GraphRequest(AccessToken.getCurrentAccessToken(), "me", params, HttpMethod.GET,
                    new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse response) {
                            if (response != null) {
                                try {
                                    JSONObject data = response.getJSONObject();

                                    //Getting user_id
                                    fb_user_id = data.getString("id");

                                    //Getting Email
                                    if(data.has("email")){
                                        fb_user_email = data.getString("email");
                                    }else{
                                        fb_user_email   = null;
                                    }

                                    //Getting Fullname
                                    fb_user_name = data.getString("name");

                                    //Getting gender
                                    fb_user_gender = data.getString("gender");

                                    //Getting firstname
                                    fb_user_firstname = data.getString("first_name");

                                    //Getting lastname
                                    fb_user_lastname = data.getString("last_name");


                                    //Building the Facebook user Profile Picture
                                    fb_user_image = AppConstants.FACEBOOK_USER_IMAGE_BASE_URL+ fb_user_id
                                            +AppConstants.FACEBOOK_USER_IMAGE_OFFSET_URL;

                                    //Logging out from the instance
                                    LoginManager.getInstance().logOut();

                                    //Calling the API for Facebook Login
                                    facebookUserLoginTask(fb_user_id, fb_user_name, fb_user_image, fb_user_email,
                                            fb_user_firstname,fb_user_lastname);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).executeAsync();
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(), "GPS enabled", Toast.LENGTH_LONG).show();
                //Getting the Current Location
                getCurrentLocation();
            } else {
                Toast.makeText(getApplicationContext(), "GPS is not enabled", Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == PERMISSION_REQUEST_CODE){
            Toast.makeText(SigninActivity.this, "Permission granted", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Function to build the Login APi request parameters
     * @return API request
     */
    public Request buildFacebookLoginApiRequestBody(String fb_userId, String fb_userName,
                                                            String fb_userImage, String fb_userEmail,
                                                            String fb_userfirstname,String fb_userlastname){
        //Building the API with all the required Parameters
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"fblogin?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_UID, fb_userId);
        urlBuilder.addQueryParameter(Constants.PARAM_FIRST_NAME, fb_userfirstname);
        urlBuilder.addQueryParameter(Constants.PARAM_LAST_NAME, fb_userlastname);
        urlBuilder.addQueryParameter(Constants.PARAM_FCMID, FCM_REGISTRATION_ID);
        urlBuilder.addQueryParameter(Constants.PARAM_EMAIL, fb_userEmail);
        urlBuilder.addQueryParameter(Constants.PARAM_PROVIDER, Constants.VALUE_SOCIAL_USER_DATA_PROVIDER);
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETOKEN, FCM_REGISTRATION_ID);
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETYPE, Constants.DEVICE_TYPE);
        urlBuilder.addQueryParameter(Constants.PARAM_LOCATION, gpsTracker.getLatitude()+ "," + gpsTracker.getLongitude());
        urlBuilder.addQueryParameter(Constants.PARAM_TIMEZONE, getDeviceTimeZone());
        urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, fb_userImage);
        urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE_NAME, Constants.IMAGE_CONTENT_NAME);
        urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE_CONTENT_TYPE, Constants.IMAGE_CONTENT_TYPE);
        String url = urlBuilder.build().toString();

        //Building the request
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }


    /**
     *
     * Facebook User Login task
     * @param fb_userId_ Facebook User id
     * @param fb_userName Facebook User full name
     * @param fb_userImage Facebook User Image
     * @param fb_userEmail Facebook User Email
     * @param fb_userfirstname Facebook User FirstName
     * @param fb_userlastname Facebook User LastName
     */
    public void facebookUserLoginTask(String fb_userId_, String fb_userName,
                                      String fb_userImage, String fb_userEmail,
                                      String fb_userfirstname,
                                      String fb_userlastname){
        String message = null;

        //Handling the loader state
        LoaderUtility.handleLoader(SigninActivity.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildFacebookLoginApiRequestBody(fb_userId_, fb_userName, fb_userImage,
                fb_userEmail,fb_userfirstname,fb_userlastname);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(SigninActivity.this, false); 
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();

                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    String err_msg = responseObject.getString("status");

                    //String message = responseObject.getString("message");
                    if (err_msg != null &&
                            err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                        //Parsing the data from the JSON object
                        auth_token = responseObject.getString("auth_token");
                        String facebook_id = responseObject.getString("fb_id");
                        String device_info_id = responseObject.getString("device_info_id");
                        String user_id = responseObject.getString("user_id");
                        String phone_number = responseObject.getString("phone_number");
                        String firstname = responseObject.getString("first_name");
                        String lastname = responseObject.getString("last_name");
                        //String current_status = responseObject.getString("current_status");
                        String user_email = responseObject.getString("email");
                        String birth_day = responseObject.getString("birth_date");
                        String image = responseObject.getString("image");
                        String user_sport = responseObject.getString("sport");
                        String userName = responseObject.getString("username");
                        /*int game_invitation_count = responseObject
                                .getInt("game_invitation_count");
                        int friend_invitation_count = responseObject
                                .getInt("friend_invitation_count");
                        boolean recive_push_notification = (Boolean) responseObject
                                .get("receive_push_notification");
                        JSONObject array = responseObject.getJSONObject("notifications");
                        boolean player_request = (Boolean) array.get("player_request");
                        boolean game_invitation = (Boolean) array.get("game_invitation");
                        boolean nearby_games = (Boolean) array.get("nearby_game");
                        boolean nearby_games_all = (Boolean) array.get("nearby_game_all");
                        boolean nearby_games_primary = (Boolean) array
                                .get("nearby_game_primary");
                        boolean message_board = (Boolean) array.get("message_board");
                        boolean game_update = (Boolean) array.get("game_update");
                        boolean comment_on_post = (Boolean) array.get("comment_on_post");
                        boolean game_cancellation = (Boolean) array.get("game_cancel");
                        boolean game_edit = (Boolean) array.get("game_edit");
                        boolean game_join_leave = (Boolean) array.get("game_join_leave");*/

                        //Keeping the login status either from Facebook or Normal login
                        logInType = "FACEBOOK_LOGIN";

                        //Storing the data in the Shared Preference
                        storeDatainSharedPreferance(auth_token,userName,device_info_id,
                                user_id, firstname,lastname, image, user_email, phone_number,
                                user_sport, facebook_id);

                        //Storing the user info for quick access
                        userInfoQuickAccess(auth_token,user_id,userName);

                        //Releasing all the facebook values
                        releaseValues();

                        //Redirecting the user to the Home Screen
                        //redirectUserHomePage();
                        getUserFriends(auth_token);

                        LoaderUtility.handleLoader(SigninActivity.this, false);
                    } else if(err_msg != null &&
                            err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_CREATE_FAILED)) {
                        //Treating this user as a new user in the APP.
                        // Redirecting the user to the sign up screen
                        LoaderUtility.handleLoader(SigninActivity.this, false);
                        redirectUserSignup();
                    }else if(err_msg != null &&
                            err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Handling the loader state
                        LoaderUtility.handleLoader(SigninActivity.this, false);
                        if(responseObject != null && responseObject.has("error_ident")){
                            String error_identity = responseObject.getString("error_ident");
                            if(error_identity != null && error_identity.equalsIgnoreCase("create_failed")){
                                redirectUserSignup();
                            }
                        }else{
                            //Displaying the Error Message
                            String message = responseObject.getString("message");
                            snackbar.setSnackBarMessage(message);
                        }
                    }else{
                        //Handling the loader state
                        LoaderUtility.handleLoader(SigninActivity.this, false); 
                        snackbar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE);
                    }
                }catch(Exception ex){
                    //Handling the loader state
                    LoaderUtility.handleLoader(SigninActivity.this, false);
                    ex.printStackTrace();
                }

            }
        });
    }


    /**
     * Releasing all the facebook hold values
     */
    public static void releaseValues(){
        fb_user_firstname = null;
        fb_user_lastname = null;
        fb_user_name = null;
        fb_user_email = null;
        fb_user_image = null;
        fb_user_id = null;
    }


    /**
     * Redirecting the user to the signup screen
     */
    public void redirectUserSignup(){
        //Sign in Activity.this.finish();
        Intent intent = new Intent(SigninActivity.this,
                FacebookUserRegistrationStepOne.class);
        startActivity(intent);
    }

    /**
     * Redirecting the user to the signup screen
     */
    public void redirectUserHomePage(){
        SigninActivity.this.finish();
        Intent intent = new Intent(SigninActivity.this,
                NavigationDrawerActivity.class);
        startActivity(intent);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );

        result.setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // NO need to show the dialog;
                getCurrentLocation();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult(SigninActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    /**
     * Function to initialize the Google API clent
     */
    public void initializeGoogleClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
    }

    /**
     * Function Responsible for proving the current location latitude and longitude
     */
    public void getCurrentLocation(){
        this.gpsTracer = new GPSTracker(SigninActivity.this);
        if(this.gpsTracer.canGetLocation()) {
            this.currLocLatitude = this.gpsTracer.getLatitude();
            this.currLocLongitude = this.gpsTracer.getLongitude();
        } else{
            snackbar.setSnackBarMessage("Can't fetch your location");
        }
    }

    /**
     * Fucntion to get the Fcm Id
     */
    public void getFcmId(){
        SharedPreferences firebase_token_preferences = getSharedPreferences("Firebase_Token", Context.MODE_PRIVATE);
        SharedPreferences.Editor firebase_editor = firebase_token_preferences.edit();
        FCM_REGISTRATION_ID = firebase_token_preferences.getString("Fcm_id", (String)null);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



    public void getUserFriends(final String authToken){

        final DatabaseAdapter db = new DatabaseAdapter(SigninActivity.this);
        //Handling the loader state
       // handleLoader(true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildFriendsApiRequestBody(authToken);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(SigninActivity.this, false); 
                redirectUser();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    err_msg = responseObject.getString("status");
                    if (err_msg.equalsIgnoreCase("success")) {
                        JSONArray friendArray = responseObject.getJSONArray("all_users");
                        for(int i = 0; i < friendArray.length(); i++){
                            JSONObject frienddetailsObject = friendArray.getJSONObject(i);
                            String userId = frienddetailsObject.getString("user_id");
                            String userName = frienddetailsObject.getString("username");
                            String firstName = frienddetailsObject.getString("first_name");
                            String lastName = frienddetailsObject.getString("last_name");
                            String image = frienddetailsObject.getString("image");
                            String sport = frienddetailsObject.getString("sport");
                            String phone_number = frienddetailsObject.getString("phone_number");
                            String status = frienddetailsObject.getString("status");
                            String rewards = frienddetailsObject.getString("rewards");
                            db.insertfriend(userId,userName,firstName,lastName,
                                    image,sport,phone_number,status,rewards);
                        }
                    } else {
                        msg = responseObject.getString("message");
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }


                SigninActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Hiding the soft key board after successful login
                        hideSoftKeyboard();

                        if(err_msg != null && err_msg.equalsIgnoreCase("success")){
                            //Toast.makeText(SigninActivity.this, "", Toast.LENGTH_LONG).show();
                            getUserTeams(userId);
                            //Handling the page redirection
                           // redirectUser();
                        }else{
                            //Handling the loader state
                            LoaderUtility.handleLoader(SigninActivity.this, false); 
                            snackbar.setSnackBarMessage(msg);
                            redirectUser();
                        }
                    }
                });
            }
        });
    }


    public void getUserTeams(final String user_id){

        final DatabaseAdapter db = new DatabaseAdapter(SigninActivity.this);
        //Handling the loader state
       // handleLoader(true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildTeamsApiRequestBody(user_id);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(SigninActivity.this, false); 
                redirectUser();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    err_msg = responseObject.getString("status");
                    if (err_msg.equalsIgnoreCase("success")) {
                        JSONArray friendArray = responseObject.getJSONArray("all_teams");
                        for(int i = 0; i < friendArray.length(); i++){
                            JSONObject frienddetailsObject = friendArray.getJSONObject(i);
                            String teamId = frienddetailsObject.getString("_id");
                            String teamName = frienddetailsObject.getString("name");
                            String teamSport = frienddetailsObject.getString("sport_id");
                            String sportName = db.getcurrentSportName(teamSport);
                            String teamCustomSport = frienddetailsObject.getString("custom_sport_name");
                            String teamCreatorId = frienddetailsObject.getString("user_id");
                            String teamCreatorName = frienddetailsObject.getString("user_name");
                            String teamCreatorImage = frienddetailsObject.getString("image");
                            String teamCreatedAt = frienddetailsObject.getString("created_at");
                            String teamUpdatedAt = frienddetailsObject.getString("updated_at");

                            //Inserting the teams data in Local DB
                            db.insertMyTeam(teamId,teamName,sportName,teamCustomSport,
                                    teamCreatorId,teamCreatorName,teamCreatorImage,teamCreatedAt,teamUpdatedAt);
                            JSONArray teamPlayersArray = frienddetailsObject.getJSONArray("players");
                            for(int j = 0; j < teamPlayersArray.length(); j++){
                                JSONObject teamPlayersObj = teamPlayersArray.getJSONObject(j);
                                String player_id = teamPlayersObj.getString("player_id");
                                String player_user_name = teamPlayersObj.getString("player_name");
                                String player_first_name = teamPlayersObj.getString("first_name");
                                String player_last_name = teamPlayersObj.getString("last_name");
                                String player_image = teamPlayersObj.getString("image");

                                //Inserting the teammayes data in Local DB
                                db.insertTeamPlayer(teamId, player_id, player_user_name,
                                        player_first_name, player_last_name, player_image );
                            }
                        }
                    } else {
                        msg = responseObject.getString("message");
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                SigninActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Hiding the soft key board after successful login
                        hideSoftKeyboard();

                        if(err_msg != null && err_msg.equalsIgnoreCase("success")){
                            //Handling the loader state
                            LoaderUtility.handleLoader(SigninActivity.this, false); 
                            //Handling the page redirection
                           // redirectUser();
                            getBadgeCount(null);
                        }else{
                            //Handling the loader state
                            LoaderUtility.handleLoader(SigninActivity.this, false); 
                            snackbar.setSnackBarMessage(msg);
                            redirectUser();
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildTeamsApiRequestBody(String authTOken){

        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"teams").newBuilder();
        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"teams/"+authTOken).newBuilder();
        urlBuilder.addQueryParameter("user_id",login_userId);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }




    /**
     * Function responsible for
     * updateing the notification badge count
     */
    public void getBadgeCount(final String timeStamp) {
        //Handling the loader state
        LoaderUtility.handleLoader(SigninActivity.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildBadgeCountApiRequestBody(timeStamp);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(SigninActivity.this, false); 
                //Handling the page redirection
                redirectUser();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    badgeCount = responseObject.getInt("badge_count");
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                SigninActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(SigninActivity.this, false); 
                        //Keeping the last API called time
                        keepTheBadgeCountApiCalledTime(SigninActivity.this, called_time);
                        //Assigning the value after the API get called
                        if(badgeCount > 0){
                            Constants.PUSH_NOTIFICATION_BADGE_COUNT = badgeCount;
                        }
                        Toast.makeText(SigninActivity.this, "Login Successful", Toast.LENGTH_LONG).show();
                        //Handling the page redirection
                        redirectUser();
                    }
                });
            }
        });
    }


    /**
     * Function responsible for keeping the badge count APi called time
     * @param context
     * @param time
     */
    public static void keepTheBadgeCountApiCalledTime(Context context, String time){
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.BADGE_COUNT_API_CALLED_TIME, time);
        editor.commit();
    }


    /**
     * Function to build the Badge count API request
     *
     * @return APi request
     */
    public Request buildBadgeCountApiRequestBody(String time) {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(SigninActivity.this);
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_badge_count").newBuilder();
        user_id = preferences.getString("loginuser_id", null);
        urlBuilder.addQueryParameter("user_id", user_id);
        if(!TextUtils.isEmpty(time)){
            urlBuilder.addQueryParameter("prev_seen_at", time);
        }else{
            time = ApplicationUtility.getCurrentDateTime();
            urlBuilder.addQueryParameter("prev_seen_at", time);
        }
        called_time = ApplicationUtility.getCurrentDateTime();
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    @Override
    protected void onResume() {
        super.onResume();
        et_username.setText(null);
        et_password.setText(null);
    }
}
