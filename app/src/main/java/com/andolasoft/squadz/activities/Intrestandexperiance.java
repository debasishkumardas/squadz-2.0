package com.andolasoft.squadz.activities;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.Jsoncordinator;
import com.andolasoft.squadz.utils.LoaderUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Intrestandexperiance extends Activity implements
		OnSeekBarChangeListener {

	TextView save_profile, profile_title, primarySport,
			primarySportSeekbarText, basketballSeekbarText,
			baseballSeekbarText, footballSeekbarText, soccerSeekbarText,
			tennisSeekbarText, volleyballSeekbarText, golfSeekbarText,
			runningSeekbarText, cricketSeekbarText, bikingSeekbarText,
			rugbySeekBarText, frisbeeseekbarText;
	ImageView back_editprofile, primarySportPlus, primarySportMinus,
			basketballPlus, basketballMinus, baseballPlus, baseballMinus,
			footballPlus, footballMinus, soccerPlus, soccerMinus, tennisPlus,
			tennisMinus, volleyballPlus, volleyballMinus, golfPlus, golfMinus,
			runningPlus, runningMinus, cricketPlus, cricketMinus, bikingPlus,
			bikingMinus, rugby_plus, rugby_minus, frisbee_plus, frisbee_minus;
	DatabaseAdapter db;
	String primary_sport, auth_token, device_info_id, loginuser_id,
			getusername, login_user_id;
	SeekBar primary_seekbar, baseball_seekbar, basketball_seekbar,
			football_seekbar, soccer_seekbar, tennis_seekbar,
			volleyball_seekbar, golf_seekbar, running_seekbar, cricket_seekbar,
			biking_seekbar, rugbySeekbar, frisbeeSeekBar;
	RelativeLayout sportBasketball, sportBaseball, sportFootball, sportSoccer,
			sportTennis, sportVolleyball, sportGolf, sportRunning,
			sportCricket, sportBiking, sportRugby, sportfreebie;
	View viewBasketball, viewBaseball, viewFootball, viewSoccer, viewTennis,
			viewVolleyball, viewGolf, viewRunning, viewCricket;
	static int progressChanged = 0;
	static int progressChangedPrimary = 0;
	static int progressChangedBasketball = 0;
	static int progressChangedBaseball = 0;
	static int progressChangedFootball = 0;
	static int progressChangedSoccer = 0;
	static int progressChangedTennis = 0;
	static int progressChangedVolleyball = 0;
	static int progressChangedGolf = 0;
	static int progressChangedRunning = 0;
	static int progressChangedCricket = 0;
	static int progressChangedBiking = 0;
	static int progressChangedRugby = 0;
	static int progressChangedFrisbee = 0;
	static int count = 0;

	TextView edit_interest_exp_save;
	ImageView backbtn_interest_exp;
	View mCustomVIew;
	ArrayList<Integer> skill_data;
	SharedPreferences sp;
	SharedPreferences.Editor edit;
	String[] sportlist = { "Basketball", "Baseball", "Football", "Soccer",
			"Tennis", "Volleyball", "Golf", "Running" };
	ArrayAdapter<String> sportsadapter;
	ImageView selectSport;
	String name, user_email, phone_number, user_aboutme, friend_id,
			user_birthday, from_intent, intent_extra;
	boolean isconnectrdtointernet;
	Handler handler = new Handler();
	ProgressDialog dialog;
    ImageView ivBack;
    TextView tvDone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intrestandexperiance);

		// Initializing the Shared preferences
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());

		// Getting the auth_token from Shared preferences
		auth_token = preferences.getString("auth_token", null);

		// Getting the device info id from Shared preferences
		device_info_id = preferences.getString("device_info_id", null);

		// Getting the login user id from Shared preferences
		loginuser_id = preferences.getString("loginuser_id", null);

		// Getting the user name from Shared preferences
		getusername = preferences.getString("USERNAME", null);

		login_user_id = preferences.getString("loginuser_id", null);

		db = new DatabaseAdapter(getApplicationContext());

		Intent intent = getIntent();
		primary_sport = intent.getStringExtra("primary_sport");

		if (TextUtils.isEmpty(primary_sport)) {
			primary_sport = db.getPrimarySport(login_user_id);
		}
		// primary_sport = db.getPrimarySport(login_user_id);

		skill_data = new ArrayList<Integer>();

		sp = getSharedPreferences("ProgressState", MODE_PRIVATE);
		edit = sp.edit();

		Intent profile_intent = getIntent();

		friend_id = profile_intent.getStringExtra("friend_id");
		from_intent = profile_intent.getStringExtra("intent");
		intent_extra = profile_intent.getStringExtra("intent_extra");

		//getProfileData();

		selectSport = (ImageView) findViewById(R.id.selectSport);
		selectSport.setVisibility(View.GONE);
		primary_seekbar = (SeekBar) findViewById(R.id.primarySeekBar);
		basketball_seekbar = (SeekBar) findViewById(R.id.basketball_seekBar);
		baseball_seekbar = (SeekBar) findViewById(R.id.baseball_seekBar);
		football_seekbar = (SeekBar) findViewById(R.id.football_seekBar);
		soccer_seekbar = (SeekBar) findViewById(R.id.soccer_seekBar);
		tennis_seekbar = (SeekBar) findViewById(R.id.tennis_seekBar);
		volleyball_seekbar = (SeekBar) findViewById(R.id.volleyball_seekBar);
		golf_seekbar = (SeekBar) findViewById(R.id.golf_seekBar);
		running_seekbar = (SeekBar) findViewById(R.id.running_seekBar);
		cricket_seekbar = (SeekBar) findViewById(R.id.cricket_seekBar);
		biking_seekbar = (SeekBar) findViewById(R.id.biking_seekBar);
		rugbySeekbar = (SeekBar) findViewById(R.id.rugby_seekBar);
		frisbeeSeekBar = (SeekBar) findViewById(R.id.ultimate_fribees_seekBar);
        ivBack = (ImageView) findViewById(R.id.btn_back);
        tvDone = (TextView) findViewById(R.id.tv_done);

		primarySportSeekbarText = (TextView) findViewById(R.id.primarySportSeekbarText);
		basketballSeekbarText = (TextView) findViewById(R.id.basketballSeekbarText);
		baseballSeekbarText = (TextView) findViewById(R.id.baseballSeekbarText);
		footballSeekbarText = (TextView) findViewById(R.id.footballSeekbarText);
		soccerSeekbarText = (TextView) findViewById(R.id.soccerSeekbarText);
		tennisSeekbarText = (TextView) findViewById(R.id.tennisSeekbarText);
		volleyballSeekbarText = (TextView) findViewById(R.id.volleyballSeekbarText);
		golfSeekbarText = (TextView) findViewById(R.id.golfSeekbarText);
		runningSeekbarText = (TextView) findViewById(R.id.runningSeekbarText);
		cricketSeekbarText = (TextView) findViewById(R.id.cricketSeekbarText);
		bikingSeekbarText = (TextView) findViewById(R.id.bikingSeekbarText);
		rugbySeekBarText = (TextView) findViewById(R.id.rugbySeekbarText);
		frisbeeseekbarText = (TextView) findViewById(R.id.ultimate_fribeesSeekbarText);

		sportBasketball = (RelativeLayout) findViewById(R.id.basketball_others_layout_main_layout);
		sportBaseball = (RelativeLayout) findViewById(R.id.basetball_primary_layout_main_layout);
		sportFootball = (RelativeLayout) findViewById(R.id.football_primary_layout_main_layout);
		sportSoccer = (RelativeLayout) findViewById(R.id.soccer_primary_layout_main_layout);
		sportTennis = (RelativeLayout) findViewById(R.id.tennis_primary_layout_main_layout);
		sportVolleyball = (RelativeLayout) findViewById(R.id.volleyball_primary_layout_main_layout);
		sportGolf = (RelativeLayout) findViewById(R.id.golf_primary_layout_main_layout);
		sportRunning = (RelativeLayout) findViewById(R.id.running_primary_layout_main_layout);
		sportCricket = (RelativeLayout) findViewById(R.id.cricket_primary_layout_main_layout);
		sportBiking = (RelativeLayout) findViewById(R.id.biking_primary_layout_main_layout);
		sportRugby = (RelativeLayout) findViewById(R.id.rugby_primary_layout_main_layout);
		sportfreebie = (RelativeLayout) findViewById(R.id.ultimate_fribees_primary_layout_main_layout);

		viewBasketball = (View) findViewById(R.id.basketball_view);
		viewBaseball = (View) findViewById(R.id.edit_profile_view_interest);
		viewFootball = (View) findViewById(R.id.soccer_edit_profile_view_interest);
		viewSoccer = (View) findViewById(R.id.tennis_edit_profile_view_interest);
		viewTennis = (View) findViewById(R.id.volley_edit_profile_view_interest);
		viewVolleyball = (View) findViewById(R.id.golf_edit_profile_view_interest);
		viewGolf = (View) findViewById(R.id.running_edit_profile_view_interest);
		viewRunning = (View) findViewById(R.id.cricket_edit_profile_view_interest);
		viewCricket = (View) findViewById(R.id.biking_edit_profile_view_interest);

		primarySport = (TextView) findViewById(R.id.basketball_primary_lay);

		primarySportPlus = (ImageView) findViewById(R.id.primarySportPlus);
		primarySportMinus = (ImageView) findViewById(R.id.primarySportMinus);
		basketballPlus = (ImageView) findViewById(R.id.basketballPlus);
		basketballMinus = (ImageView) findViewById(R.id.basketballMinus);
		baseballPlus = (ImageView) findViewById(R.id.baseballPlus);
		baseballMinus = (ImageView) findViewById(R.id.baseballMinus);
		footballPlus = (ImageView) findViewById(R.id.footballPlus);
		footballMinus = (ImageView) findViewById(R.id.footballMinus);
		soccerPlus = (ImageView) findViewById(R.id.soccerPlus);
		soccerMinus = (ImageView) findViewById(R.id.soccerMinus);
		tennisPlus = (ImageView) findViewById(R.id.tennisPlus);
		tennisMinus = (ImageView) findViewById(R.id.tennisMinus);
		volleyballPlus = (ImageView) findViewById(R.id.volleyballPlus);
		volleyballMinus = (ImageView) findViewById(R.id.volleyballMinus);
		golfPlus = (ImageView) findViewById(R.id.golfPlus);
		golfMinus = (ImageView) findViewById(R.id.golfMinus);
		runningPlus = (ImageView) findViewById(R.id.runningPlus);
		runningMinus = (ImageView) findViewById(R.id.runningMinus);
		cricketPlus = (ImageView) findViewById(R.id.cricketPlus);
		cricketMinus = (ImageView) findViewById(R.id.cricketMinus);
		bikingPlus = (ImageView) findViewById(R.id.bikingPlus);
		bikingMinus = (ImageView) findViewById(R.id.bikingMinus);

		rugby_plus = (ImageView) findViewById(R.id.rugbyPlus);
		rugby_minus = (ImageView) findViewById(R.id.rugbyMinus);
		frisbee_plus = (ImageView) findViewById(R.id.ultimate_fribeesPlus);
		frisbee_minus = (ImageView) findViewById(R.id.ultimate_fribeesMinus);

		if (!TextUtils.isEmpty(primary_sport)) {

			primarySport.setText(primary_sport);
			updateView(primary_sport);
		}

		/*ActionBar mActionBar = getActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater
				.from(Intrestandexperiance.this);*/

		tvDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// save data
				try {
					count = count + 1;
					saveData();
					Intrestandexperiance.this.finish();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});

        ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				handler.postDelayed(new Runnable() {
					@SuppressLint("NewApi")
					@Override
					public void run() {
						Intrestandexperiance.this.finish();
                        handler.removeCallbacks(this);
					}
				}, 100);

			}
		});

		if (count > 0) {
			displayRestoredData();
		} else {
			new UserProfile().execute();
		}

		/*selectSport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// sportDialog();
				count = 1;
				handler.postDelayed(new Runnable() {
					@SuppressLint("NewApi")
					@Override
					public void run() {
						Intent intent = new Intent(Intrestandexperiance.this,
								SelectSportActivity.class);
						intent.putExtra("friend_id", friend_id);
						intent.putExtra("intent", from_intent);
						intent.putExtra("intent_extra", intent_extra);
						Intrestandexperiance.this.startActivity(intent,
								bndlanimation);
						// Intrestandexperiance.this.finish();
						handler.removeCallbacks(this);
					}
				}, 100);
			}
		});*/

		primary_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						primarySportSeekbarText
								.setText(setSeekbarText(progressChangedPrimary));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedPrimary = progress;

					}
				});

		basketball_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						basketballSeekbarText
								.setText(setSeekbarText(progressChangedBasketball));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedBasketball = progress;

					}
				});

		baseball_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						baseballSeekbarText
								.setText(setSeekbarText(progressChangedBaseball));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedBaseball = progress;

					}
				});

		football_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						footballSeekbarText
								.setText(setSeekbarText(progressChangedFootball));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedFootball = progress;

					}
				});

		soccer_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						soccerSeekbarText
								.setText(setSeekbarText(progressChangedSoccer));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedSoccer = progress;

					}
				});

		tennis_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						tennisSeekbarText
								.setText(setSeekbarText(progressChangedTennis));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedTennis = progress;

					}
				});

		volleyball_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						volleyballSeekbarText
								.setText(setSeekbarText(progressChangedVolleyball));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedVolleyball = progress;

					}
				});

		golf_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

				golfSeekbarText.setText(setSeekbarText(progressChangedGolf));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				progressChangedGolf = progress;

			}
		});

		running_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						runningSeekbarText
								.setText(setSeekbarText(progressChangedRunning));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedRunning = progress;

					}
				});

		cricket_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						cricketSeekbarText
								.setText(setSeekbarText(progressChangedCricket));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedCricket = progress;

					}
				});

		biking_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						bikingSeekbarText
								.setText(setSeekbarText(progressChangedBiking));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedBiking = progress;

					}
				});

		rugbySeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

				rugbySeekBarText.setText(setSeekbarText(progressChangedRugby));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				progressChangedRugby = progress;

			}
		});

		frisbeeSeekBar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {

						frisbeeseekbarText
								.setText(setSeekbarText(progressChangedFrisbee));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						// TODO Auto-generated method stub
						progressChangedFrisbee = progress;

					}
				});

		primarySportPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedPrimary);
				primary_seekbar.setProgress(pos);
				progressChangedPrimary = pos;
				primarySportSeekbarText
						.setText(setSeekbarText(progressChangedPrimary));
			}
		});

		primarySportMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedPrimary);
				primary_seekbar.setProgress(pos);
				progressChangedPrimary = pos;
				primarySportSeekbarText
						.setText(setSeekbarText(progressChangedPrimary));

			}
		});

		basketballPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedBasketball);
				basketball_seekbar.setProgress(pos);
				progressChangedBasketball = pos;
				basketballSeekbarText
						.setText(setSeekbarText(progressChangedBasketball));

			}
		});

		basketballMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedBaseball);
				basketball_seekbar.setProgress(pos);
				progressChangedBasketball = pos;
				basketballSeekbarText
						.setText(setSeekbarText(progressChangedBasketball));

			}
		});

		baseballPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedBaseball);
				baseball_seekbar.setProgress(pos);
				progressChangedBaseball = pos;
				baseballSeekbarText
						.setText(setSeekbarText(progressChangedBaseball));

			}
		});

		baseballMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedBaseball);
				baseball_seekbar.setProgress(pos);
				progressChangedBaseball = pos;
				baseballSeekbarText
						.setText(setSeekbarText(progressChangedBaseball));
			}
		});

		footballPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedFootball);
				football_seekbar.setProgress(pos);
				progressChangedFootball = pos;
				footballSeekbarText
						.setText(setSeekbarText(progressChangedFootball));
			}
		});

		footballMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedFootball);
				football_seekbar.setProgress(pos);
				progressChangedFootball = pos;
				footballSeekbarText
						.setText(setSeekbarText(progressChangedFootball));
			}
		});

		soccerPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedSoccer);
				soccer_seekbar.setProgress(pos);
				progressChangedSoccer = pos;
				soccerSeekbarText
						.setText(setSeekbarText(progressChangedSoccer));

			}
		});

		soccerMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedSoccer);
				soccer_seekbar.setProgress(pos);
				progressChangedSoccer = pos;
				soccerSeekbarText
						.setText(setSeekbarText(progressChangedSoccer));

			}
		});
		tennisPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedTennis);
				tennis_seekbar.setProgress(pos);
				progressChangedTennis = pos;
				tennisSeekbarText
						.setText(setSeekbarText(progressChangedTennis));
			}
		});

		tennisMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedTennis);
				tennis_seekbar.setProgress(pos);
				progressChangedTennis = pos;
				tennisSeekbarText
						.setText(setSeekbarText(progressChangedTennis));

			}
		});

		volleyballPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedVolleyball);
				volleyball_seekbar.setProgress(pos);
				progressChangedVolleyball = pos;
				volleyballSeekbarText
						.setText(setSeekbarText(progressChangedVolleyball));

			}
		});

		volleyballMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedVolleyball);
				volleyball_seekbar.setProgress(pos);
				progressChangedVolleyball = pos;
				volleyballSeekbarText
						.setText(setSeekbarText(progressChangedVolleyball));

			}
		});

		golfPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedGolf);
				golf_seekbar.setProgress(pos);
				progressChangedGolf = pos;
				golfSeekbarText.setText(setSeekbarText(progressChangedGolf));

			}
		});

		golfMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedGolf);
				golf_seekbar.setProgress(pos);
				progressChangedGolf = pos;
				golfSeekbarText.setText(setSeekbarText(progressChangedGolf));

			}
		});

		runningPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedRunning);
				running_seekbar.setProgress(pos);
				progressChangedRunning = pos;
				runningSeekbarText
						.setText(setSeekbarText(progressChangedRunning));
			}
		});

		runningMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedRunning);
				running_seekbar.setProgress(pos);
				progressChangedRunning = pos;
				runningSeekbarText
						.setText(setSeekbarText(progressChangedRunning));
			}
		});

		cricketPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedCricket);
				cricket_seekbar.setProgress(pos);
				progressChangedCricket = pos;
				cricketSeekbarText
						.setText(setSeekbarText(progressChangedCricket));
			}
		});

		cricketMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedCricket);
				cricket_seekbar.setProgress(pos);
				progressChangedCricket = pos;
				cricketSeekbarText
						.setText(setSeekbarText(progressChangedCricket));
			}
		});

		bikingPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedCricket);
				biking_seekbar.setProgress(pos);
				progressChangedBiking = pos;
				bikingSeekbarText
						.setText(setSeekbarText(progressChangedBiking));

			}
		});

		bikingMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedBiking);
				biking_seekbar.setProgress(pos);
				progressChangedBiking = pos;
				bikingSeekbarText
						.setText(setSeekbarText(progressChangedBiking));

			}
		});

		rugby_plus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedRugby);
				rugbySeekbar.setProgress(pos);
				progressChangedRugby = pos;
				rugbySeekBarText.setText(setSeekbarText(progressChangedRugby));

			}
		});

		rugby_minus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedRugby);
				rugbySeekbar.setProgress(pos);
				progressChangedRugby = pos;
				rugbySeekBarText.setText(setSeekbarText(progressChangedRugby));

			}
		});

		frisbee_plus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarPlusPosition(progressChangedFrisbee);
				frisbeeSeekBar.setProgress(pos);
				progressChangedFrisbee = pos;
				frisbeeseekbarText
						.setText(setSeekbarText(progressChangedFrisbee));

			}
		});

		frisbee_minus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int pos = setSeekbarMinusPosition(progressChangedFrisbee);
				frisbeeSeekBar.setProgress(pos);
				progressChangedFrisbee = pos;
				frisbeeseekbarText
						.setText(setSeekbarText(progressChangedFrisbee));

			}
		});

	}

	/**
	 * Set text value according to seekbar position
	 */
	public int setSeekbarPosition(String skillLevel) {
		int pos = 0;

		if (skillLevel.contentEquals("Recreational Player")) {
			pos = 15;
		} else if (skillLevel.contentEquals("High School Athlete")
				|| skillLevel.contentEquals("High School Athlet")) {
			pos = 45;
		} else if (skillLevel.contentEquals("Collegiate Athlete")
				|| skillLevel.contentEquals("Collegiate Athlet")) {
			pos = 65;
		} else if (skillLevel.contentEquals("Professional Athlete")
				|| skillLevel.contentEquals("Professional Athlet")) {
			pos = 85;
		} else {
			pos = 0;
		}

		return pos;
	}

	/**
	 * Set text value according to seekbar position
	 * 
	 * @param num
	 * @return
	 */
	public String setSeekbarText(int num) {
		String text;

		if (isBetween(num, 1, 25)) {
			text = "Recreational Player";
		} else if (isBetween(num, 26, 50)) {
			text = "High School Athlete";
		} else if (isBetween(num, 51, 75)) {
			text = "Collegiate Athlete";
		} else if (isBetween(num, 76, 100)) {
			text = "Professional Athlete";
		} else {

			text = "No Experience";
		}

		/*
		 * switch (num) {
		 * 
		 * case 0: text = "No Experience"; break; case 20:
		 * 
		 * break; case 40: text = "High School Athlete"; break; case 60: text =
		 * "Collegiate Athlete"; break; case 80: text = "Professional Athlete";
		 * break; case 100: text = "Former Athlete"; break;
		 * 
		 * default: text = "No Experience"; break; }
		 */

		return text;
	}

	/**
	 * set seekbar position for plus button click
	 * 
	 * @param num
	 * @return
	 */
	public int setSeekbarPlusPosition(int num) {
		int num1 = 0;

		if (isBetween(num, 1, 25)) {
			num1 = 41;
		} else if (isBetween(num, 26, 50)) {
			num1 = 61;
		} else if (isBetween(num, 51, 75)) {
			num1 = 81;
		} else if (isBetween(num, 76, 100)) {
			num1 = 91;
		} else {

			num1 = 10;
		}

		return num1;
	}

	/**
	 * set seekbar position for minus button click
	 * 
	 * @param num
	 * @return
	 */
	public int setSeekbarMinusPosition(int num) {
		int num1 = 0;

		if (isBetween(num, 1, 25)) {
			num1 = 0;
		} else if (isBetween(num, 26, 50)) {
			num1 = 10;
		} else if (isBetween(num, 51, 75)) {
			num1 = 41;
		} else if (isBetween(num, 76, 100)) {
			num1 = 61;
		} else {

			num1 = 0;
		}

		return num1;
	}

	/**
	 * Set Relativelayout and view visibility
	 * 
	 * @param sport
	 */
	public void updateView(String sport) {

		if (sport.contentEquals("Basketball")) {

			sportBasketball.setVisibility(View.GONE);
			viewBasketball.setVisibility(View.GONE);
		} else if (sport.contentEquals("Baseball")) {

			sportBaseball.setVisibility(View.GONE);
			viewBaseball.setVisibility(View.GONE);
		} else if (sport.contentEquals("Football")) {

			sportFootball.setVisibility(View.GONE);
			viewFootball.setVisibility(View.GONE);
		} else if (sport.contentEquals("Soccer")) {

			sportSoccer.setVisibility(View.GONE);
			viewSoccer.setVisibility(View.GONE);
		} else if (sport.contentEquals("Tennis")) {

			sportTennis.setVisibility(View.GONE);
			viewTennis.setVisibility(View.GONE);
		} else if (sport.contentEquals("Volleyball")) {

			sportVolleyball.setVisibility(View.GONE);
			viewVolleyball.setVisibility(View.GONE);
		} else if (sport.contentEquals("Golf")) {

			sportGolf.setVisibility(View.GONE);
			viewGolf.setVisibility(View.GONE);
		} else if (sport.contentEquals("Running")) {

			sportRunning.setVisibility(View.GONE);
			viewRunning.setVisibility(View.GONE);
		} else if (sport.contentEquals("Cricket")) {
			sportCricket.setVisibility(View.GONE);
			viewCricket.setVisibility(View.GONE);
		} else if (sport.contentEquals("Biking")) {
			sportBiking.setVisibility(View.GONE);
		} else if (sport.contentEquals("Rugby")) {
			sportRugby.setVisibility(View.GONE);
			// viewCricket.setVisibility(View.GONE);
		} else if (sport.contentEquals("Ultimate Frisbee")) {
			sportfreebie.setVisibility(View.GONE);
			// viewCricket.setVisibility(View.GONE);
		}

	}

	/**
	 * Setting the progress of the time of progress
	 */

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {

		switch (seekBar.getId()) {

		case R.id.primarySeekBar:
			progressChanged = progress;
			primarySportSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.basketball_seekBar:
			progressChanged = progress;
			basketballSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.baseball_seekBar:
			progressChanged = progress;
			baseballSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.football_seekBar:
			progressChanged = progress;
			footballSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.soccer_seekBar:
			progressChanged = progress;
			soccerSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.tennis_seekBar:
			progressChanged = progress;
			tennisSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.volleyball_seekBar:
			progressChanged = progress;
			volleyballSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.golfSeekbarText:
			progressChanged = progress;
			golfSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.running_seekBar:
			progressChanged = progress;
			runningSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.cricket_seekBar:
			progressChanged = progress;
			cricketSeekbarText.setText(setSeekbarText(progress));
			break;
		case R.id.rugby_seekBar:
			progressChanged = progress;
			rugbySeekBarText.setText(setSeekbarText(progress));
			break;
		case R.id.ultimate_fribees_seekBar:
			progressChanged = progress;
			frisbeeseekbarText.setText(setSeekbarText(progress));
			break;

		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	/**
	 * Setting the progress of the time of changing the progress
	 */
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		switch (seekBar.getId()) {

		case R.id.primarySeekBar:

			primarySportSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.basketball_seekBar:

			basketballSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.baseball_seekBar:

			baseballSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.football_seekBar:

			footballSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.soccer_seekBar:

			soccerSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.tennis_seekBar:

			tennisSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.volleyball_seekBar:

			volleyballSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.golfSeekbarText:

			golfSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.running_seekBar:

			runningSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.cricket_seekBar:

			cricketSeekbarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.rugby_seekBar:
			// progressChanged = progress;
			rugbySeekBarText.setText(setSeekbarText(progressChanged));
			break;
		case R.id.ultimate_fribees_seekBar:
			// progressChanged = progress;
			frisbeeseekbarText.setText(setSeekbarText(progressChanged));
			break;

		}

	}

	/**
	 * Change seekbar postion according to prevoius data
	 */
	public void displayRestoredData() {
		// set seekbar position
		primary_seekbar.setProgress(sp.getInt("primarySport", 0));
		basketball_seekbar.setProgress(sp.getInt("Basketball", 0));
		baseball_seekbar.setProgress(sp.getInt("Baseball", 0));
		football_seekbar.setProgress(sp.getInt("Football", 0));
		soccer_seekbar.setProgress(sp.getInt("Soccer", 0));
		tennis_seekbar.setProgress(sp.getInt("Tennis", 0));
		volleyball_seekbar.setProgress(sp.getInt("Volleyball", 0));
		golf_seekbar.setProgress(sp.getInt("Golf", 0));
		running_seekbar.setProgress(sp.getInt("Running", 0));
		cricket_seekbar.setProgress(sp.getInt("Cricket", 0));
		biking_seekbar.setProgress(sp.getInt("Biking", 0));
		rugbySeekbar.setProgress(sp.getInt("Rugby", 0));
		frisbeeSeekBar.setProgress(sp.getInt("Ultimate Frisbee", 0));

		// set text according to seekbar position
		primarySportSeekbarText.setText(setSeekbarText(sp.getInt(
				"primarySport", 0)));
		basketballSeekbarText
				.setText(setSeekbarText(sp.getInt("Basketball", 0)));
		baseballSeekbarText.setText(setSeekbarText(sp.getInt("Baseball", 0)));
		footballSeekbarText.setText(setSeekbarText(sp.getInt("Football", 0)));
		soccerSeekbarText.setText(setSeekbarText(sp.getInt("Soccer", 0)));
		tennisSeekbarText.setText(setSeekbarText(sp.getInt("Tennis", 0)));
		volleyballSeekbarText
				.setText(setSeekbarText(sp.getInt("Volleyball", 0)));
		golfSeekbarText.setText(setSeekbarText(sp.getInt("Golf", 0)));
		runningSeekbarText.setText(setSeekbarText(sp.getInt("Running", 0)));
		cricketSeekbarText.setText(setSeekbarText(sp.getInt("Cricket", 0)));
		bikingSeekbarText.setText(setSeekbarText(sp.getInt("Biking", 0)));
		rugbySeekBarText.setText(setSeekbarText(sp.getInt("Rugby", 0)));
		frisbeeseekbarText.setText(setSeekbarText(sp.getInt("Ultimate Frisbee",
				0)));

		progressChangedPrimary = sp.getInt("primarySport", 0);
		progressChangedBasketball = sp.getInt("Basketball", 0);
		progressChangedBaseball = sp.getInt("Baseball", 0);
		progressChangedFootball = sp.getInt("Football", 0);
		progressChangedSoccer = sp.getInt("Soccer", 0);
		progressChangedTennis = sp.getInt("Golf", 0);
		progressChangedVolleyball = sp.getInt("Volleyball", 0);
		progressChangedGolf = sp.getInt("Golf", 0);
		progressChangedRunning = sp.getInt("Running", 0);
		progressChangedCricket = sp.getInt("Cricket", 0);
		progressChangedBiking = sp.getInt("Biking", 0);
		progressChangedRugby = sp.getInt("Rugby", 0);
		progressChangedFrisbee = sp.getInt("Ultimate Frisbee", 0);

	}

	/**
	 * check whether the num is between x
	 * 
	 * @param x
	 * @param lower
	 * @param upper
	 * @return
	 */
	public static boolean isBetween(int x, int lower, int upper) {
		return lower <= x && x <= upper;
	}

	/**
	 * Store data in shared preference
	 */
	public void saveData() {
		//db.updatePrimarySport(login_user_id, primary_sport);
		//db.updateUserProfilePrimarySport(login_user_id, primary_sport);
		edit.putInt("primarySport", progressChangedPrimary);
		edit.putInt("Basketball", progressChangedBasketball);
		edit.putInt("Baseball", progressChangedBaseball);
		edit.putInt("Football", progressChangedFootball);
		edit.putInt("Soccer", progressChangedSoccer);
		edit.putInt("Tennis", progressChangedTennis);
		edit.putInt("Volleyball", progressChangedVolleyball);
		edit.putInt("Golf", progressChangedGolf);
		edit.putInt("Running", progressChangedRunning);
		edit.putInt("Cricket", progressChangedCricket);
		edit.putInt("Biking", progressChangedBiking);
		edit.putInt("Rugby", progressChangedRugby);
		edit.putInt("Ultimate Frisbee", progressChangedFrisbee);
		edit.commit();
		Log.d("Interest and Experience", String.valueOf(progressChangedPrimary));
	}

	public void getProfileData() {

		db.open();
		Cursor cursor = db.getProfileData();
		cursor.moveToFirst();
		if (cursor.getCount() != 0) {
			do {

				name = cursor.getString(cursor.getColumnIndex("name"));

				phone_number = cursor.getString(cursor
						.getColumnIndex("phone_number"));

				user_email = cursor.getString(cursor
						.getColumnIndex("user_email"));

				user_aboutme = cursor.getString(cursor
						.getColumnIndex("user_aboutme"));

				user_birthday = cursor.getString(cursor
						.getColumnIndex("birth_date"));

			} while (cursor.moveToNext());
		}

	}

	/*
	 * Getting the User Profile and the Skill level
	 */
	private class UserProfile extends AsyncTask<String, Void, Boolean> {
		// UserProfile API
		String userprofilespi = EndPoints.BASE_URL + "users/"+ auth_token
				+ "/profile?" + "user_id=" + friend_id;
		String status, primary_skillLevel;
		JSONObject user_profile;
		JSONObject user_data;
		ProgressDialog dialog;
		JSONArray secondarySports;

		@Override
		protected void onPreExecute() {
			LoaderUtility.handleLoader(Intrestandexperiance.this, true);
		}

		protected Boolean doInBackground(final String... args) {
			Jsoncordinator jParser1 = new Jsoncordinator();
			// get JSON data from URL

			System.out.println("User Profile URL izzz" + userprofilespi);
			user_data = jParser1.getJSONFromUrl1(userprofilespi);
			System.out.println("user_data izzzzz" + user_data);

			try {
				// Getting the status from the API
				status = user_data.getString("status");

				if (status.equalsIgnoreCase("success")) {
					JSONObject profile = user_data.getJSONObject("profile");
					primary_sport = profile.getString("sport");
					primary_skillLevel = profile.getString("skill_level");
					secondarySports = profile.getJSONArray("secondary_sports");

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			if (status != null && status.equalsIgnoreCase("success")) {
				// Setting the values for the Text views
				try {
					int pos = setSeekbarPosition(primary_skillLevel);
					primary_seekbar.setProgress(pos);
					progressChangedPrimary = pos;
					primarySportSeekbarText
							.setText(setSeekbarText(progressChangedPrimary));

					for (int i = 0; i < secondarySports.length(); i++) {
						String sport_id = (String) secondarySports
								.getJSONObject(i).get("sport_id");
						String skill_level = (String) secondarySports
								.getJSONObject(i).get("skill_level");
						setOtherSportDetails(sport_id, skill_level);
					}

					LoaderUtility.handleLoader(Intrestandexperiance.this, false);


				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			} else if (status != null && status.equalsIgnoreCase("error")) {
				// Showing the Toast after getting the error message
				Toast.makeText(getApplicationContext(), "Problem in loading",
						Toast.LENGTH_LONG).show();

				LoaderUtility.handleLoader(Intrestandexperiance.this, true);

			} else {
				Toast.makeText(getApplicationContext(),
						AppConstants.API_RESPONSE_ERROR_MESSAGE, Toast.LENGTH_LONG)
						.show();
			}
		}

	}

	/**
	 * Function to handle the progress loader
	 * @param status true/false
	 */
	public void handleLoader(boolean status){
		if(status){
			dialog = ProgressDialog
					.show(this, "", "Please wait ...");
			dialog.getWindow().setGravity(Gravity.CENTER);
			dialog.setCancelable(false);
		}else{
			dialog.dismiss();
		}
	}

	/**
	 * Set progress and skillLevel from API data
	 * 
	 * @param sportId
	 *            Selected sport id
	 * @param skillLevel
	 *            Selected Skill level
	 */
	public void setOtherSportDetails(String sportId, String skillLevel) {
		String sport_name = db.getcurrentSportName(sportId);
		int pos = setSeekbarPosition(skillLevel);
		if (sport_name.contentEquals("Basketball")) {
			basketball_seekbar.setProgress(pos);
			progressChangedBasketball = pos;
			basketballSeekbarText.setText(skillLevel);
		}
		if (sport_name.contentEquals("Baseball")) {

			baseball_seekbar.setProgress(pos);
			progressChangedBaseball = pos;
			baseballSeekbarText.setText(skillLevel);
		}
		if (sport_name.contentEquals("Football")) {

			football_seekbar.setProgress(pos);
			progressChangedFootball = pos;
			footballSeekbarText.setText(skillLevel);

		}
		if (sport_name.contentEquals("Soccer")) {

			soccer_seekbar.setProgress(pos);
			progressChangedSoccer = pos;
			soccerSeekbarText.setText(skillLevel);
		}
		if (sport_name.contentEquals("Tennis")) {

			tennis_seekbar.setProgress(pos);
			progressChangedTennis = pos;
			tennisSeekbarText.setText(skillLevel);

		}
		if (sport_name.contentEquals("Volleyball")) {

			volleyball_seekbar.setProgress(pos);
			progressChangedVolleyball = pos;
			volleyballSeekbarText.setText(skillLevel);

		}
		if (sport_name.contentEquals("Golf")) {

			golf_seekbar.setProgress(pos);
			progressChangedGolf = pos;
			golfSeekbarText.setText(skillLevel);
		}
		if (sport_name.contentEquals("Cricket")) {

			cricket_seekbar.setProgress(pos);
			progressChangedCricket = pos;
			cricketSeekbarText.setText(skillLevel);
		}
		if (sport_name.contentEquals("Biking")) {

			biking_seekbar.setProgress(pos);
			progressChangedBiking = pos;
			bikingSeekbarText.setText(skillLevel);
		}

		if (sport_name.contentEquals("Rugby")) {

			rugbySeekbar.setProgress(pos);
			progressChangedRugby = pos;
			rugbySeekBarText.setText(skillLevel);
		}

		if (sport_name.contentEquals("Ultimate Frisbee")) {

			frisbeeSeekBar.setProgress(pos);
			progressChangedFrisbee = pos;
			frisbeeseekbarText.setText(skillLevel);
		}

	}

	/**
	 * Function to handle the device back button
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intrestandexperiance.this.finish();
		Intent intent = new Intent(Intrestandexperiance.this, EditUserProfile.class);
		intent.putExtra("name", name);
		intent.putExtra("email", user_email);
		intent.putExtra("phone", phone_number);
		intent.putExtra("aboutme", user_aboutme);
		intent.putExtra("birth_date", user_birthday);
		intent.putExtra("friend_id", friend_id);
		intent.putExtra("intent", from_intent);
		intent.putExtra("intent_extra", intent_extra);
		startActivity(intent);
	}

}
