package com.andolasoft.squadz.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EventFilter extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener{

    public static EventFilter eventFilter;
    private RelativeLayout back_icon_event_filter_layout,
            selectDate_layout;
    private long selected_date_milisecond;
    private TextView select_date_text;
    int minteger = 0;
    int players = 0;
    SnackBar snackBar;
    @InjectView(R.id.event_filter_apply_btn)
    Button event_filter_apply_btn;
    @InjectView(R.id.event_filter_radio_low_high)
    RadioButton event_filter_radio_low_high;
    @InjectView(R.id.event_filter_radio_high_low)
    RadioButton event_filter_radio_high_low;
    @InjectView(R.id.event_filter_mapRadius_miles)
    TextView event_filter_mapRadius_miles;
    @InjectView(R.id.event_plus_btn)
    Button event_plus_btn;
    @InjectView(R.id.event_minus_btn)
    Button event_minus_btn;
    @InjectView(R.id.max_player_count_event_filter)
    TextView max_player_count_event_filter;
    @InjectView(R.id.max_player_minus_btn)
    Button max_player_minus_btn;
    @InjectView(R.id.max_player_plus_btn)
    Button max_player_plus_btn;
    @InjectView(R.id.sports_layout_event_filter)
    RelativeLayout sports_layout_event_filter;
    @InjectView(R.id.event_filter_sports_name)
    TextView event_filter_sports_name;
    @InjectView(R.id.event_filter_sports_icon)
    ImageView ic_event_sport;
    SportsImagePicker sportsImagePicker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_event_filter);
        ButterKnife.inject(this);

        /**
         * Initializing all views
         */
        initReferences();

        /**
         * Adding click events
         */
        setClickEvents();

        /**
         * Checking value existance coming from Evevnt Listing page
         */
        checkValueExistanceAfterFilter();

    }

    /**
     * Adding click events
     */
    public void setClickEvents()
    {
        back_icon_event_filter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.EVENT_FILTER_BTN_CLICK = false;
                finish();
            }
        });

        selectDate_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        EventFilter.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.setAccentColor(getResources().getColor(R.color.deep_orange));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        //Low to high Radio button click & stored the click status
        event_filter_radio_low_high.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Constants.EVENT_FILTER_PRICE = "lowtohigh";
                }
            }
        });

        //Low to high Radio button click & stored the click status
        event_filter_radio_high_low.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Constants.EVENT_FILTER_PRICE = "hightolow";
                }
            }
        });

        /**
         * Miles plus button click
         */
        event_plus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT != 0) {
                    Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT = Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT + 5;
                    display(Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT);
                } else {
                    minteger = minteger + 5;
                    display(minteger);
                    Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT = minteger;
                }

            }
        });

        /**
         * Miles minus button click
         */
        event_minus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT != 0) {
                        Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT = Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT - 5;
                        display(Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT);
                } else {
                    if (minteger != 0) {
                        minteger = minteger - 5;
                        display(minteger);
                        Constants.EVENT_FILTER_MAP_RADIUS_MILES_COUNT = minteger;
                    }
                }


            }
        });

        max_player_minus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                players = Integer.valueOf(max_player_count_event_filter.getText().toString().trim());

                if(players == 1)
                {
                    snackBar.setSnackBarMessage("Court Minimum player exceed");
                }
                else {
                    players --;
                    max_player_count_event_filter.setText(String.valueOf(players));
                    Constants.NO_OF_PLAYER = max_player_count_event_filter.getText().toString().trim();
                }
            }
        });

        max_player_plus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                players = Integer.valueOf(max_player_count_event_filter.getText().toString().trim());

                if(players == Constants.EVENT_MAX_PLAYER_)
                {
                    snackBar.setSnackBarMessage("Court Maximum player exceed");
                }
                else {
                    players ++;
                    max_player_count_event_filter.setText(String.valueOf(players));
                    Constants.NO_OF_PLAYER = max_player_count_event_filter.getText().toString().trim();
                }
            }
        });

        //Sports field click event
        sports_layout_event_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_sports = new Intent(EventFilter.this,Sports.class);
                startActivity(intent_sports);
            }
        });
        
        //Apply Button Click
        event_filter_apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(Constants.EVENT_FILTER_PRICE)) {
                    Constants.EVENT_FILTER_BTN_CLICK = true;
                    finish();
                }
                else if(!TextUtils.isEmpty(Constants.NO_OF_PLAYER)) {
                    Constants.EVENT_FILTER_BTN_CLICK = true;
                    finish();
                }
                else if(!TextUtils.isEmpty(Constants.EVENT_FILTER_SELECT_DATE)) {
                    Constants.EVENT_FILTER_BTN_CLICK = true;
                    finish();
                }
                else if(!TextUtils.isEmpty(Constants.EVENT_FILTER_MAP_RADIUS_MILES)) {
                    Constants.EVENT_FILTER_BTN_CLICK = true;
                    finish();
                }
                else{
                    finish();
                    Constants.EVENT_FILTER_BTN_CLICK = false;
                }
            }
        });
    }

    /**
     * DISPLAY RADIUS COUNT
     * @param number
     */

    private void display(int number) {

        event_filter_mapRadius_miles.setText("" + number + " Miles");
        Constants.EVENT_FILTER_MAP_RADIUS_MILES = String.valueOf(number);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = (++monthOfYear)+ "/"+dayOfMonth + "/" + year;
        String finalOutputDate = formattedDate(date);
        String OutputDate_For_Display = formattedDateForDisplay(date);

        //Getting current date from Calender & Converting into in milisecond
        long current_date_milisecond = currentDateInMilisecond();

        if (selected_date_milisecond < current_date_milisecond) {
            Toast.makeText(this, "Selected date should not be less from current date", Toast.LENGTH_LONG).show();
        } else {
            //select_date_text.setText(finalOutputDate);
            select_date_text.setText(OutputDate_For_Display);
            //Constants.EVENT_FILTER_SELECT_DATE = finalOutputDate;
            Constants.EVENT_FILTER_SELECT_DATE = OutputDate_For_Display;
        }
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public String formattedDate(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MM/dd/yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

            selected_date_milisecond = date1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }
    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public String formattedDateForDisplay(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMMM dd, yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * Getting current date from Calender & Converting into in milisecond
     */
    public long currentDateInMilisecond() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(c.getTime());

        return convertToMilliseconds(formattedDate);
    }
    /**
     * Return date into milisecond
     *
     * @param date
     * @return
     */
    public static long convertToMilliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();

            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Checking value existance coming from Evevnt Listing page
     */
    public void checkValueExistanceAfterFilter()
    {
        if(!TextUtils.isEmpty(Constants.EVENT_FILTER_PRICE))
        {
            if(Constants.EVENT_FILTER_PRICE.equalsIgnoreCase("lowtohigh"))
            {
                event_filter_radio_low_high.setChecked(true);
                Constants.EVENT_FILTER_PRICE = "lowtohigh";
            }
            else if(Constants.EVENT_FILTER_PRICE.equalsIgnoreCase("hightolow"))
            {
                event_filter_radio_high_low.setChecked(true);
                Constants.EVENT_FILTER_PRICE = "hightolow";
            }
        }
        /**
         * Check map radius existance
         */
        if (!TextUtils.isEmpty(Constants.EVENT_FILTER_MAP_RADIUS_MILES)) {
            event_filter_mapRadius_miles.setText("" + Constants.EVENT_FILTER_MAP_RADIUS_MILES + " Miles");
        }
        /**
         * Check existance for Select date
         */
        if (!TextUtils.isEmpty(Constants.EVENT_FILTER_SELECT_DATE)) {
            select_date_text.setText(Constants.EVENT_FILTER_SELECT_DATE);
        }
        /**
         * Check existance for Number of Player
         */
        if (!TextUtils.isEmpty(Constants.NO_OF_PLAYER)) {
            max_player_count_event_filter.setText(Constants.NO_OF_PLAYER);
        }

    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {
        back_icon_event_filter_layout = (RelativeLayout) findViewById(R.id.back_icon_event_filter_layout);
        selectDate_layout = (RelativeLayout) findViewById(R.id.selectDate_layout);
        select_date_text = (TextView) findViewById(R.id.select_date_text);

        eventFilter = this;
        snackBar = new SnackBar(this);
        sportsImagePicker = new SportsImagePicker();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Getting Sport name from Sport page
        if(!TextUtils.isEmpty(Constants.EVENT_FILTER_SPORT_NAME))
        {
            event_filter_sports_name.setText(Constants.EVENT_FILTER_SPORT_NAME);
            ic_event_sport.setImageResource(sportsImagePicker.getSportImage(Constants.EVENT_FILTER_SPORT_NAME));
        }
        //Getting Sport name from Home page
        else if(!TextUtils.isEmpty(Constants.SPORTS_NAME))
        {
            Constants.EVENT_FILTER_SPORT_NAME = Constants.SPORTS_NAME;
            event_filter_sports_name.setText(Constants.SPORTS_NAME);
            ic_event_sport.setImageResource(sportsImagePicker.getSportImage(Constants.SPORTS_NAME));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.EVENT_FILTER_BTN_CLICK = false;
        finish();
    }

}
