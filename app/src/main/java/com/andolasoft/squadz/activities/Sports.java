package com.andolasoft.squadz.activities;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.views.adapters.SportsAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Sports extends AppCompatActivity {

    @InjectView(R.id.listView_sports)
    ListView listView_sports;
    @InjectView(R.id.back_icon_sports_layout)
    RelativeLayout back_icon_sports_layout;

    SportsAdapter sportsAdapter;
    DatabaseAdapter db;
    ArrayList<String> sports_name_array,sports_id_array;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_sports);
        ButterKnife.inject(this);

        /**
         * Initializing References
         */
        initRef();

        /**
         * Getting Sports name from database
         */
        getSportsName();

        /**
         * Click events on the views
         */
        addClickEvents();

    }

    /**
     * Click events on the views
     */
    public void addClickEvents() {

        back_icon_sports_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * Initializing References
     */
    public void initRef()
    {
        db = new DatabaseAdapter(this);
    }

    //Getting Sports name from database
    public void getSportsName() {
        Cursor cursor = db.getSportsName();

        sports_name_array = new ArrayList<>();
        //sports_id_array = new ArrayList<>();

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String sport_Name = cursor.getString(cursor
                            .getColumnIndex("sport_name"));
                    String sport_id = cursor.getString(cursor
                            .getColumnIndex("sport_id"));

                    sports_name_array.add(sport_Name+"###"+sport_id);
                    //sports_id_array.add(sport_id);

                } while (cursor.moveToNext());
            }

            sportsAdapter = new SportsAdapter(Sports.this,sports_name_array);
            listView_sports.setAdapter(sportsAdapter);
            sportsAdapter.notifyDataSetChanged();

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
