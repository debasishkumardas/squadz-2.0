package com.andolasoft.squadz.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class ListingAdditionalInfo extends AppCompatActivity {

    @InjectView(R.id.back_icon_add_info_layout)
    RelativeLayout back_icon_add_info_layout;
    @InjectView(R.id.space_descrp_layout)
    LinearLayout space_descrp_layout;
    @InjectView(R.id.equipment_provided_layout)
    LinearLayout equipment_provided_layout;
    @InjectView(R.id.player_have_access_amenities_layout)
    LinearLayout player_have_access_amenities_layout;
    @InjectView(R.id.website_name)
    TextView website_host_name;
    @InjectView(R.id.phone_number_title)
    TextView phone_number_title;
    @InjectView(R.id.venue_rules_desc)
    TextView venue_rules_desc;
    SnackBar snackBar;
    private static final String BULLET_SYMBOL = "&#8226";
    String venue_website = "",venue_phone_number = "",
            formatted_phone_number = "",from_page = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_listing_additional_info);

        /*** Initializing all views belongs to this layout*/
        initReferences();

        /*** Adding click events on the views*/
        addClickEvents();

        /*** Set data to the views*/
        displayAdditionalInfo();

    }

    /**
     * Adding click events on the views
     */
    public void addClickEvents() {
        back_icon_add_info_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /**Website clicked*/
        website_host_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ListingAdditionalInfo.this,Website_Webview_Additional_Info.class);
                intent.putExtra("WEBSITE",venue_website);
                startActivity(intent);
            }
        });
        /**Phone number clicked*/
        phone_number_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Making phone call
                if(!phone_number_title.getText().toString().trim().equalsIgnoreCase("Not Available"))
                {
                    makePhoneCall();
                }
            }
        });

    }
    /**
     * Set data to the views
     */
    public void displayAdditionalInfo()
    {
        ArrayList<String> venue_space_descp_array = null, venue_equipments_array = null, venue_players_amenities_array = null;
        String venue_rules = "";

        if(!TextUtils.isEmpty(from_page) && from_page.equalsIgnoreCase("GAME"))
        {
            venue_phone_number = EventAdapter.eventListingModelArrayList.get(0).getAdditional_phone_number();
            venue_website = EventAdapter.eventListingModelArrayList.get(0).getAdditional_website();
            venue_rules = EventAdapter.eventListingModelArrayList.get(0).getAdditional_venue_rules();
            venue_space_descp_array = EventAdapter.eventListingModelArrayList.get(0).getAdditional_space_desc_array();
            venue_equipments_array = EventAdapter.eventListingModelArrayList.get(0).getAdditional_equipmentes_array();
            venue_players_amenities_array = EventAdapter.eventListingModelArrayList.get(0).getAdditional_amenities_array();
        }

       else {
            venue_phone_number = VenueAdapter.venueDetailModelArrayList.get(0).getAdditional_phone_number();
            venue_website = VenueAdapter.venueDetailModelArrayList.get(0).getAdditional_website();
            venue_rules = VenueAdapter.venueDetailModelArrayList.get(0).getAdditional_venue_rules();
            venue_space_descp_array = VenueAdapter.venueDetailModelArrayList.get(0).getAdditional_space_desc_array();
            venue_equipments_array = VenueAdapter.venueDetailModelArrayList.get(0).getAdditional_equipmentes_array();
            venue_players_amenities_array = VenueAdapter.venueDetailModelArrayList.get(0).getAdditional_amenities_array();
        }

        if(!TextUtils.isEmpty(venue_phone_number))
        {
            phone_number_title.setText(venue_phone_number);
            formatted_phone_number = venue_phone_number.replaceAll("[-+.^:, ()]","");
        }
        else{
            phone_number_title.setText("Not Available");
        }

        if(!TextUtils.isEmpty(venue_rules))
        {
            venue_rules_desc.setText(venue_rules);
        }
        else{
            venue_rules_desc.setText("Not Available");
        }
        if(!TextUtils.isEmpty(venue_website))
        {
            website_host_name.setText(venue_website);
            website_host_name.setPaintFlags(website_host_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
        else{
            website_host_name.setText("Not Available");
        }

        //Linkify.addLinks(website_host_name, Linkify.ALL);
        //website_host_name.setMovementMethod(LinkMovementMethod.getInstance());

        if(venue_space_descp_array.size() == 0)
        {
            LayoutInflater inflater = LayoutInflater.from(this);
            View view = inflater.inflate(R.layout.add_info_description_layout, null);
            TextView textView = (TextView) view.findViewById(R.id.amenities_title);
            textView.setText("Not Available");
            space_descrp_layout.addView(view);
        }
        else {
            if (venue_space_descp_array.size() > 0) {
                for (int i = 0; i < venue_space_descp_array.size(); i++) {
                    LayoutInflater inflater = LayoutInflater.from(this);
                    View view = inflater.inflate(R.layout.add_info_description_layout, null);
                    String amenities = venue_space_descp_array.get(i);
                    TextView textView = (TextView) view.findViewById(R.id.amenities_title);
                    textView.setText(Html.fromHtml(BULLET_SYMBOL + " " + amenities));
                    space_descrp_layout.addView(view);
                }
            }
        }

        if(venue_equipments_array.size() == 0)
        {
            LayoutInflater inflater = LayoutInflater.from(this);
            View view = inflater.inflate(R.layout.add_info_description_layout, null);
            TextView textView = (TextView) view.findViewById(R.id.amenities_title);
            textView.setText("Not Available");
            equipment_provided_layout.addView(view);
        }
        else {
            if (venue_equipments_array.size() > 0) {
                for (int i = 0; i < venue_equipments_array.size(); i++) {
                    LayoutInflater inflater = LayoutInflater.from(this);
                    View view = inflater.inflate(R.layout.add_info_description_layout, null);
                    String amenities = venue_equipments_array.get(i);
                    TextView textView = (TextView) view.findViewById(R.id.amenities_title);
                    textView.setText(Html.fromHtml(BULLET_SYMBOL + " " + amenities));
                    equipment_provided_layout.addView(view);
                }
            }
        }

        if(venue_players_amenities_array.size() == 0)
        {
            LayoutInflater inflater = LayoutInflater.from(this);
            View view = inflater.inflate(R.layout.add_info_description_layout, null);
            TextView textView = (TextView) view.findViewById(R.id.amenities_title);
            textView.setText("Not Available");
            player_have_access_amenities_layout.addView(view);
        }
        else {
            if (venue_players_amenities_array.size() > 0) {
                for (int i = 0; i < venue_players_amenities_array.size(); i++) {
                    LayoutInflater inflater = LayoutInflater.from(this);
                    View view = inflater.inflate(R.layout.add_info_description_layout, null);
                    String amenities = venue_players_amenities_array.get(i);
                    TextView textView = (TextView) view.findViewById(R.id.amenities_title);
                    textView.setText(Html.fromHtml(BULLET_SYMBOL + " " + amenities));
                    player_have_access_amenities_layout.addView(view);
                }
            }
        }

    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {

        ButterKnife.inject(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        snackBar = new SnackBar(this);
        from_page = getIntent().getStringExtra("FROM_PAGE");

    }

    /**
     * Making call to the specific number
     */
    public void makePhoneCall() {
        int MyVersion = Build.VERSION.SDK_INT;

        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
            else{
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+formatted_phone_number));
                startActivity(callIntent);
            }
        }
        else{
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+formatted_phone_number));
            startActivity(callIntent);
        }
    }
    /**
     * Request persmission for Post lollipop devices
     */
    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(ListingAdditionalInfo.this,
                new String[]{android.Manifest.permission.CALL_PHONE},
                1);
    }

    /**
     * Check permission
     * @return
     */
    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Making phone call
                    makePhoneCall();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // Toast.makeText(Profile.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

