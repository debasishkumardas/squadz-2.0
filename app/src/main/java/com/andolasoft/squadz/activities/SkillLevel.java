package com.andolasoft.squadz.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.Constants;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SkillLevel extends AppCompatActivity {

    public static SkillLevel skillLevel;
    private RelativeLayout all_text_layout,rookies_text_layout,
            veterans_text_layout,studs_text_layout,back_icon_finish_event_layout;
    private TextView skill_level_all,skill_level_rookies,
            skill_level_veterans,skill_level_studs;
    private ImageView ic_all,ic_rookies,ic_veterans,ic_studs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_skill_level);


        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        /**
         * Click events on the views
         */
        addClickEvents();

        /**
         * Set data coming from Venue listing page
         */
        setData(Constants.SKILL_LEVEL);

    }
    /**
     * Click events on the views
     */
    public void addClickEvents()
    {
        all_text_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.SKILL_LEVEL = "All";
                finish();
            }
        });

        rookies_text_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.SKILL_LEVEL = "Rookies";
                finish();
            }
        });

        veterans_text_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.SKILL_LEVEL = "Veterans";
                finish();
            }
        });

        studs_text_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.SKILL_LEVEL = "Studs";
                finish();
            }
        });

        back_icon_finish_event_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * Set data coming from Additional info page
     */
    public void setData(String skill_Level)
    {
        if(skill_Level.equalsIgnoreCase("All"))
        {
            skill_level_all.setText("All");
            setSkillLevelColor(skill_Level);
        }
        else if(skill_Level.equalsIgnoreCase("Rookies"))
        {
            skill_level_rookies.setText("Rookies");
            setSkillLevelColor(skill_Level);
        }
        else if(skill_Level.equalsIgnoreCase("Veterans"))
        {
            skill_level_veterans.setText("Veterans");
            setSkillLevelColor(skill_Level);
        }
        else if(skill_Level.equalsIgnoreCase("Studs"))
        {
            skill_level_studs.setText("Studs");
            setSkillLevelColor(skill_Level);
        }
    }

    /**
     * Chang Skill level text color with icon icon
     * @param skillLevel
     */
    public void setSkillLevelColor(String skillLevel)
    {
        if(skillLevel.equalsIgnoreCase("All"))
        {
            skill_level_all.setTextColor(getResources().getColor(R.color.orange));
            skill_level_rookies.setTextColor(getResources().getColor(R.color.gray_new));
            skill_level_veterans.setTextColor(getResources().getColor(R.color.gray_new));
            skill_level_studs.setTextColor(getResources().getColor(R.color.gray_new));

            ic_all.setBackgroundResource(R.mipmap.ic_run_orange);
            ic_rookies.setBackgroundResource(R.mipmap.ic_run_gray);
            ic_veterans.setBackgroundResource(R.mipmap.ic_run_gray);
            ic_studs.setBackgroundResource(R.mipmap.ic_run_gray);
        }
        else if(skillLevel.equalsIgnoreCase("Rookies"))
        {
            skill_level_all.setTextColor(getResources().getColor(R.color.gray_new));
            skill_level_rookies.setTextColor(getResources().getColor(R.color.orange));
            skill_level_veterans.setTextColor(getResources().getColor(R.color.gray_new));
            skill_level_studs.setTextColor(getResources().getColor(R.color.gray_new));

            ic_all.setBackgroundResource(R.mipmap.ic_run_gray);
            ic_rookies.setBackgroundResource(R.mipmap.ic_run_orange);
            ic_veterans.setBackgroundResource(R.mipmap.ic_run_gray);
            ic_studs.setBackgroundResource(R.mipmap.ic_run_gray);
        }
        else if(skillLevel.equalsIgnoreCase("Veterans"))
        {
            skill_level_all.setTextColor(getResources().getColor(R.color.gray_new));
            skill_level_rookies.setTextColor(getResources().getColor(R.color.gray_new));
            skill_level_veterans.setTextColor(getResources().getColor(R.color.orange));
            skill_level_studs.setTextColor(getResources().getColor(R.color.gray_new));

            ic_all.setBackgroundResource(R.mipmap.ic_run_gray);
            ic_rookies.setBackgroundResource(R.mipmap.ic_run_gray);
            ic_veterans.setBackgroundResource(R.mipmap.ic_run_orange);
            ic_studs.setBackgroundResource(R.mipmap.ic_run_gray);
        }
        else if(skillLevel.equalsIgnoreCase("Studs"))
        {
            skill_level_all.setTextColor(getResources().getColor(R.color.gray_new));
            skill_level_rookies.setTextColor(getResources().getColor(R.color.gray_new));
            skill_level_veterans.setTextColor(getResources().getColor(R.color.gray_new));
            skill_level_studs.setTextColor(getResources().getColor(R.color.orange));

            ic_all.setBackgroundResource(R.mipmap.ic_run_gray);
            ic_rookies.setBackgroundResource(R.mipmap.ic_run_gray);
            ic_veterans.setBackgroundResource(R.mipmap.ic_run_gray);
            ic_studs.setBackgroundResource(R.mipmap.ic_run_orange);
        }
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {

        all_text_layout = (RelativeLayout) findViewById(R.id.all_text_layout);
        rookies_text_layout = (RelativeLayout) findViewById(R.id.rookies_text_layout);
        veterans_text_layout = (RelativeLayout) findViewById(R.id.veterans_text_layout);
        studs_text_layout = (RelativeLayout) findViewById(R.id.studs_text_layout);
        back_icon_finish_event_layout = (RelativeLayout) findViewById(R.id.back_icon_finish_event_layout);

        skill_level_all = (TextView) findViewById(R.id.all_text);
        skill_level_rookies = (TextView) findViewById(R.id.rookies_text);
        skill_level_veterans = (TextView) findViewById(R.id.veterans_text);
        skill_level_studs = (TextView) findViewById(R.id.studs_text);

        ic_all = (ImageView) findViewById(R.id.all_skill_icon_summary);
        ic_rookies = (ImageView) findViewById(R.id.rookies_skill_icon_summary);
        ic_veterans = (ImageView) findViewById(R.id.veterans_skill_icon_summary);
        ic_studs = (ImageView) findViewById(R.id.studs_skill_icon_summary);


        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        skillLevel = this;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
