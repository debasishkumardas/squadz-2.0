package com.andolasoft.squadz.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.SyncStateContract;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.Manifest;
import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.ImageUploadManager;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NormalRegistrationStepTwo extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener {


    public static String userFirstName, userLastName, userBirthDate,
            userEmail, userPhoneNumber, userAboutMe, userImage, signedup_at;
    public final Pattern emailPattern = Pattern
            .compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
    @InjectView(R.id.btn_back)
    ImageView btnBack;
    @InjectView(R.id.tv_done)
    TextView btnDone;
    @InjectView(R.id.phnNumber_Et)
    EditText etNumber;
    @InjectView(R.id.email_Et)
    EditText etEmail;
    @InjectView(R.id.firstname_Et)
    EditText etFirstname;
    @InjectView(R.id.lastname_Et)
    EditText etLastname;
    @InjectView(R.id.birthday_Et)
    EditText etbirthday;
    @InjectView(R.id.about_me_Et)
    EditText etAboutme;
    @InjectView(R.id.profile_signup)
    CircleImageView imgUserProfileImage;
    @InjectView(R.id.intr_exp)
    ImageButton imgBtnIntExp;
    @InjectView(R.id.addProfilePic)
    TextView tvAddProfilePic;
    @InjectView(R.id.lay_int_exp)
    TableRow lay_int_exp;
    Dialog uploadoptions;
    Button fromcamera_txt;
    Button fromsdcard_txt;
    Button cancel_signup;
    File cameraImagefile;
    @InjectView(R.id.text_profile_image)
    TextView text_profile_image;
    String imgEncoded = null,image = "";
    DatabaseAdapter db;
    SharedPreferences profileSp;
    SharedPreferences.Editor profileEditor;
    SnackBar snackbar = new SnackBar(NormalRegistrationStepTwo.this);
    Dialog dialog;
    String skill_level = null;
    String encodedImage;
    String status = null;
    long selected_date_milisecond;
    String selectedimagePath;
    Bitmap myBitmap = null;
    Uri mCropImagedUri;
    String logInType = "";
    static int REQUEST_VIDEO_CAPTURED = 200;
    private static int RESULT_LOAD_IMAGE = 1;
    private static int SELECT_VIDEO = 2;
    private static int PICK_FROM_GALLERY = 3;
    static int TAKE_PICTURE = 1777;

    /**
     * Function responsible for making a image rounded
     */
    public static Bitmap getRoundedShape(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle((float) (bitmap.getWidth() / 2), (float) (bitmap.getHeight() / 2), (float) (bitmap.getWidth() / 2), paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    // check whether the num is between x
    public static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

    /**
     * Storing the user details to get a quick access
     *
     * @param auth_token Logged in user auth token
     * @param user_id    Logged in User user id
     * @param user_name  logged in user user name
     */
    public static void userInfoQuickAccess(String auth_token, String user_id,
                                           String user_name) {
        AppConstants.AUTH_TOKEN = auth_token;
        AppConstants.USERNAME = user_name;
        AppConstants.LOGGEDIN_USERID = user_id;
    }

    /**
     * Getting the file MIME Type
     *
     * @param url
     * @return
     */
    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    extension);
        }
        return type;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_normal_reg_step_two);
        ButterKnife.inject(this);

        //Hiding the actionBar
        hideActionBar();

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(NormalRegistrationStepTwo.this);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        db = new DatabaseAdapter(NormalRegistrationStepTwo.this);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAllValues();
                NormalRegistrationStepTwo.this.finish();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Function call to register User
                createNewAccount();
            }
        });

        imgUserProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Displaying pop up to select user profile picture
                CheckUserPermissions();

            }
        });

        imgBtnIntExp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NormalRegistrationStepTwo.this, InterestAndExperienceScreen.class);
                startActivity(intent);
            }
        });

        lay_int_exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NormalRegistrationStepTwo.this, InterestAndExperienceScreen.class);
                startActivity(intent);
            }
        });

        etbirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Hiding the Soft KeyBoard
                etbirthday.setFocusable(true);
                // ApplicationUtility.hideSoftKeyboard(NormalRegistrationStepTwo.this);
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        NormalRegistrationStepTwo.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.setMaxDate(now);
                dpd.setAccentColor(getResources().getColor(R.color.orange));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });


        tvAddProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Displaying pop up to select user profile picture
                CheckUserPermissions();
            }
        });


        //Initializing the temp. storage
        initializeStorage();

        //Restoring all the Data
        restoreData();

        //Prefilling the facebook user data
        setFacebookUserData();
    }


    /**
     * Function responsible for checking the run time
     * permissions for external storage
     */
    public void CheckUserPermissions() {
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
               // showProfilePictureDialog();
                requestForSpecificPermission();
            } else{
                showProfilePictureDialog();
            }
        } else{
            showProfilePictureDialog();
        }
    }

    /**
     * Check permission
     * @return
     */
    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Request persmission for Post lollipop devices
     */
    private void requestForSpecificPermission() {
        android.support.v4.app.ActivityCompat.requestPermissions(NormalRegistrationStepTwo.this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Getting Contacts from Phone book
                    showProfilePictureDialog();
                } else {
                }
                return;
            }
        }
    }

    /**
     * Hiding the ActionBar
     */
    public void hideActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.hide();
    }

    // Showing the Dialog for Profile pic upload options
    public void showProfilePictureDialog() {

        try {
            // initializing the uploadoptions dialog
            uploadoptions = new Dialog(NormalRegistrationStepTwo.this);

            // Setting no title for the dialog
            uploadoptions.requestWindowFeature(Window.FEATURE_NO_TITLE);

            // Setting the dialog to be cancellable
            uploadoptions.setCancelable(true);

            // Setting the view for the dialog
            uploadoptions.setContentView(R.layout.uploadimages_fromcamera);

            uploadoptions.getWindow().setGravity(Gravity.BOTTOM);
            uploadoptions.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            uploadoptions.getWindow().setBackgroundDrawableResource(
                    android.R.color.transparent);
            uploadoptions.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            // Initializing all the views of the uploadoptions dialog
            fromcamera_txt = (Button) uploadoptions
                    .findViewById(R.id.fromcamera_signup);
            fromsdcard_txt = (Button) uploadoptions
                    .findViewById(R.id.fromsdcard_signup);
            cancel_signup = (Button) uploadoptions
                    .findViewById(R.id.cancel_edit_photo_signup);

            fromcamera_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Calling the function for opening the camera
                    uploadoptions.dismiss();
                    Intent intent = new Intent(
                            "android.media.action.IMAGE_CAPTURE");
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                            .format(new Date());
                    cameraImagefile = new File(Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + timeStamp + "_image.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(cameraImagefile));
                    startActivityForResult(intent, 0);
                    text_profile_image.setVisibility(View.INVISIBLE);

                }
            });

            fromsdcard_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Calling the function for opening the Cameraroll
                    // openCameraRoll();
                    uploadoptions.dismiss();
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                    text_profile_image.setVisibility(View.INVISIBLE);

                }
            });
            cancel_signup.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub
                    uploadoptions.dismiss();
                }
            });

            // Showing the uploadoptions dialog
            uploadoptions.show();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * Function resposible for registering a user and validating user data
     */
    private void createNewAccount() {
        String emailText = etEmail.getText().toString();
        if (etFirstname != null
                && "".equalsIgnoreCase(etFirstname.getText().toString().trim())) {
            //Hiding the Soft Keyboard
            hideSoftKeyBoard();
            keepFocus(etFirstname);
            snackbar.setSnackBarMessage("First name should not be blank!");
        } else if (etLastname != null
                && "".equalsIgnoreCase(etLastname.getText().toString().trim())) {
            //Hiding the Soft Keyboard
            hideSoftKeyBoard();
            keepFocus(etLastname);
            snackbar.setSnackBarMessage("Last name should not be blank!");
        } else if (!checkEmail(emailText)) {
            //Hiding the Soft Keyboard
            hideSoftKeyBoard();
            keepFocus(etEmail);
            snackbar.setSnackBarMessage("Invalid email address");
        } else if (etNumber != null
                && "".equalsIgnoreCase(etNumber.getText().toString().trim())) {
            //Hiding the Soft Keyboard
            hideSoftKeyBoard();
            keepFocus(etNumber);
            snackbar.setSnackBarMessage("Phone Number should not be blank!");
        } else if (etNumber.getText().length() < 10) {
            //Hiding the Soft Keyboard
            hideSoftKeyBoard();
            keepFocus(etNumber);
            snackbar.setSnackBarMessage("Phone Number Should not be less than 10 characters !!");
        } else if (etNumber.getText().length() > 10) {
            //Hiding the Soft Keyboard
            hideSoftKeyBoard();
            keepFocus(etNumber);
            snackbar.setSnackBarMessage("Phone Number Should not exceed more than 10 characters !!");
        } else if (etAboutme.getText().length() == 0) {
            //Hiding the Soft Keyboard
            hideSoftKeyBoard();
            keepFocus(etAboutme);
            snackbar.setSnackBarMessage("Please entre something about yourself !!");
        } else {
            String code = GetCountryZipCode(NormalRegistrationStepTwo.this);
            if (TextUtils.isEmpty(code)) {
                code = "+1";
            }
            String phone_number = etNumber.getText().toString().trim();
            String phone_number_withoutcountrycode = phone_number;
            phone_number = code + " " + phone_number;
            //phone_number = code + " " + phone_number;
            String email = etEmail.getText().toString().trim();
            String firstname = etFirstname.getText().toString().trim();
            String lastname = etLastname.getText().toString().trim();
            String about_me = etAboutme.getText().toString().trim();
            String birth_date = etbirthday.getText().toString().trim();
            String formatted_birth_date = ApplicationUtility.formattedDateForSignup(birth_date);

            if (AppConstants.USER_LOGIN_TYPE_FACEBOOK) {
                facebookUserLoginTask(SigninActivity.fb_user_id, null,
                        SigninActivity.fb_user_image, email,
                        firstname, lastname, phone_number,about_me, birth_date);
            } else {
                //API call to register user
                userRegistrationTask(phone_number, email, firstname, lastname,
                        about_me, formatted_birth_date);
            }

        }
    }

    /**
     * Function to handle the request focus of the edit text
     *
     * @param requestedText
     */
    public void keepFocus(EditText requestedText) {
        requestedText.requestFocus();
    }

    /***
     * get country code
     ***/
    public static String GetCountryZipCode(Context mContext) {
        String CountryID = "";
        String CountryZipCode = "";

        TelephonyManager manager = (TelephonyManager) mContext
                .getSystemService(TELEPHONY_SERVICE);
        // getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();
        String[] rl = mContext.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /**
     * Function to validate the Email
     *
     * @param email
     * @return
     */
    // check email format
    private boolean checkEmail(String email) {
        return emailPattern.matcher(email).matches();
    }


    private File createNewFile(String prefix){
        if(prefix==null || "".equalsIgnoreCase(prefix)){
            prefix="IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory()+"/mypics/");
        if(!newDirectory.exists()){
            if(newDirectory.mkdir()){
               // Log.d(NormalRegistrationStepTwo.getClass().getName(), newDirectory.getAbsolutePath()+" directory created");
            }
        }
        File file = new File(newDirectory,(prefix+System.currentTimeMillis()+".jpg"));
        if(file.exists()){
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }


    /** Create a File for saving an image or video */
    private  File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Squadz");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            mediaStorageDir.mkdirs();
                // Create a media file name
            String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
            File mediaFile;
            String mImageName="MI_"+ timeStamp +".jpg";
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);

            try{
                if (!mediaFile.exists()){
                    mediaFile.createNewFile();
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }

            return mediaFile;

        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="image_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private String storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("This class",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("This class", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("This class", "Error accessing file: " + e.getMessage());
        }

        return pictureFile.toString();
    }


        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = null;
        Log.d("Result code", String.valueOf(requestCode));
        try {
            if (requestCode == 5) {
                /*Bundle selectedImage = data.getExtras();
                uri = data.getData();
                selectedimagePath = getRealPathFromURI(uri);*/
                Bundle selectedImage = data.getExtras();
                Bitmap pickedImage = (Bitmap) selectedImage.getParcelable("data");
                Bitmap rsz_image = getRoundedShape(pickedImage);
                imgUserProfileImage.setImageBitmap(rsz_image);
                selectedimagePath = storeImage(pickedImage);
                //Bitmap resizedBitmap = Bitmap.createBitmap(pickedImage, 0, 0, pickedImage.getWidth(), pickedImage.getHeight());
                ImageUploadManager imguplload = new ImageUploadManager(NormalRegistrationStepTwo.this,selectedimagePath);
            } else if (requestCode == 0) {
                Uri uri_gallery = Uri.fromFile(cameraImagefile);
                // Function called to crop the image
                performCrop(uri_gallery);
            } else if (requestCode == RESULT_LOAD_IMAGE
                    && resultCode == RESULT_OK && null != data) {
                Uri selectedImage = data.getData();
                // Function called to crop the image
                performCrop(selectedImage);
            } else {
                if (TextUtils.isEmpty(imgEncoded)) {
                    text_profile_image.setVisibility(View.GONE);
                }
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();

        }

    }

    /**
     * Function responsible for cropping the image
     *
     * @param uri The image file Uri
     */

    private void performCrop(Uri uri) {
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(uri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 600);
            cropIntent.putExtra("outputY", 600);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            File path = getCreatedFilePath();
            mCropImagedUri = Uri.fromFile(path);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 5);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast
                    .makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }


    }

    // set text value according to seekbar position
    public String setSeekbarText(int num) {
        String text;



        if (isBetween(num, 1, 25)) {
            text = "Recreational Player";
        } else if (isBetween(num, 26, 50)) {
            text = "High School Athlete";
        } else if (isBetween(num, 51, 75)) {
            text = "Collegiate Athlete";
        } else if (isBetween(num, 76, 100)) {
            text = "Professional Athlete";
        } else {
            text = "No Experience";
        }



        return text;
    }

    public File getCreatedFilePath(){
        File angel1 = new File(Environment.getExternalStorageDirectory() + "/.cropImageFolder/");
        if(!angel1.exists()) {
            angel1.mkdirs();
        }

        long i2 = System.currentTimeMillis();
        File file2 = new File(angel1, String.valueOf(i2) + ".jpg");
       /* if(file2.exists()) {
            file2.delete();
        }*/
        return file2;
    }

    /**
     * Getting the skill levels for other sports
     *
     * @return
     */
    public String getOtherSportDetail() {
        String text = "";

        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedBasketball)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Basketball")
                    + "~"
                    + profileSp.getString("basketball_skill_level",
                    "No Experience") + ",");

        }
        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedBaseball)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Baseball")
                    + "~"
                    + profileSp.getString("baseball_skill_level",
                    "No Experience") + ",");

        }
        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedFootball)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Football")
                    + "~"
                    + profileSp.getString("football_skill_level",
                    "No Experience") + ",");

        }
        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedSoccer)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Soccer")
                    + "~"
                    + profileSp
                    .getString("soccer_skill_level", "No Experience")
                    + ",");

        }
        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedTennis)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Tennis")
                    + "~"
                    + profileSp
                    .getString("tennis_skill_level", "No Experience")
                    + ",");

        }
        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedVolleyball)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Volleyball")
                    + "~"
                    + profileSp.getString("volleyball_skill_level",
                    "No Experience") + ",");

        }
        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedGolf)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Golf") + "~"
                    + profileSp.getString("golf_skill_level", "No Experience")
                    + ",");

        }
        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedCricket)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Cricket")
                    + "~"
                    + profileSp.getString("cricket_skill_level",
                    "No Experience") + ",");

        }
        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedBiking)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Biking")
                    + "~"
                    + profileSp
                    .getString("biking_skill_level", "No Experience")
                    + ",");

        }

        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedRunning)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Running")
                    + "~"
                    + profileSp.getString("running_skill_level",
                    "No Experience") + ",");

        }

        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedRugby)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Rugby") + "~"
                    + profileSp.getString("rugby_skill_level", "No Experience")
                    + ",");

        }

        if (!(setSeekbarText(InterestAndExperienceScreen.progressChangedFrisbee)
                .contentEquals("No Experience"))) {

            text = text.concat(db.getcurrentSportId("Ultimate Frisbee") + "~"
                    + profileSp.getString("_skill_level", "No Experience")
                    + ",");

        }

        return text;
    }

    public void initializeStorage() {
        profileSp = getSharedPreferences("SignupData", MODE_PRIVATE);
        profileEditor = profileSp.edit();
        skill_level = profileSp.getString("skill_level", null);
    }

    /**
     * Function to build the Registration APi request parameters
     *
     * @return APi request
     */
    public Request buildRegistrationApiParameters(String phone_number, String email,
                                                          String firstname, String lastname,
                                                          String about_me, String birth_date) {
        //Building the API with all the required Parameters
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "register_user?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, NormalRegistrationStepOne.mUserName);
        urlBuilder.addQueryParameter(Constants.PARAM_FIRST_NAME, firstname);
        urlBuilder.addQueryParameter(Constants.PARAM_LAST_NAME, lastname);
        urlBuilder.addQueryParameter(Constants.PARAM_PASSWORD, NormalRegistrationStepOne.mPassword);
        urlBuilder.addQueryParameter(Constants.PARAM_PHONENUMBER, phone_number);
        urlBuilder.addQueryParameter(Constants.PARAM_SPORTID, NormalRegistrationStepOne.mSportId);
        urlBuilder.addQueryParameter(Constants.PARAM_PUSHNOTIFICATION_STATUS, "true");
        //urlBuilder.addQueryParameter(Constants.PARAM_FCMID, SigninActivity.FCM_REGISTRATION_ID);
        urlBuilder.addQueryParameter(Constants.PARAM_EMAIL, email);
        //urlBuilder.addQueryParameter(Constants.PARAM_DEVICETOKEN, SigninActivity.getDeviceToken(NormalRegistrationStepTwo.this));
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETOKEN, SigninActivity.FCM_REGISTRATION_ID);
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETYPE, Constants.DEVICE_TYPE);
        urlBuilder.addQueryParameter(Constants.PARAM_LOCATION, SigninActivity.currLocLatitude + "," + SigninActivity.currLocLongitude);
        urlBuilder.addQueryParameter(Constants.PARAM_TIMEZONE, SigninActivity.getDeviceTimeZone());

        if(Constants.isImageUploaded &&
                TextUtils.isEmpty(ImageUploadManager.profilePicS3Url)){
            ImageUploadManager.profilePicS3Url = ImageUploadManager.getProfilePictureUrlAgain(ImageUploadManager.uploadedFileName);
            if(TextUtils.isEmpty(ImageUploadManager.profilePicS3Url)){
                ImageUploadManager.profilePicS3Url = AppConstants.AWS_IMAGE_BASE_URL + ImageUploadManager.uploadedFileName;
            }
            urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, ImageUploadManager.profilePicS3Url);
        }else if(!TextUtils.isEmpty(ImageUploadManager.profilePicS3Url)){
            urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, ImageUploadManager.profilePicS3Url);
        }
        urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE_NAME, AppConstants.UPLOADED_IMAGE_FILE_NAME);
        urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE_CONTENT_TYPE, Constants.IMAGE_CONTENT_TYPE);
        urlBuilder.addQueryParameter(Constants.PARAM_SKILLLEVEL, skill_level);
        urlBuilder.addQueryParameter(Constants.PARAM_ABOUTME, about_me);
        urlBuilder.addQueryParameter(Constants.PARAM_BIRTHDATE, birth_date);
        urlBuilder.addQueryParameter(Constants.PARAM_OTHERSPORT, getOtherSportDetail());
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function responsible for registering user
     *
     * @param phone_number
     * @param email
     * @param firstname
     * @param lastname
     * @param about_me
     * @param birth_date
     */
    public void userRegistrationTask(String phone_number, String email,
                                     String firstname, String lastname,
                                     String about_me, String birth_date) {

        String message = null;

        //Handling the loader state
        LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildRegistrationApiParameters(phone_number, email, firstname, lastname,
                about_me, birth_date);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {

                NormalRegistrationStepTwo.this.runOnUiThread(new Runnable() {
                    public void run() {
                        LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, false);
                        if(!ApplicationUtility.isConnected(NormalRegistrationStepTwo.this)){
                            ApplicationUtility.displayNoInternetMessage(NormalRegistrationStepTwo.this);
                        }else{
                            ApplicationUtility.displayErrorMessage(NormalRegistrationStepTwo.this,
                                    e.getMessage().toString());
                        }
                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    status = responseObject.getString("status");
                    if (status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                        //Parsing the data from the JSON object
                        final String auth_token = responseObject.getString("auth_token");
                        String device_info_id = responseObject.getString("device_info_id");
                        String user_id = responseObject.getString("user_id");
                        String phone_number = responseObject.getString("phone_number");
                        String firstname = responseObject.getString("first_name");
                        String lastname = responseObject.getString("last_name");
                        //String current_status = responseObject.getString("current_status");
                        String user_email = responseObject.getString("email");
                        String birth_day = responseObject.getString("birth_date");
                        if(responseObject.has("image") && !responseObject.isNull("image")) {
                            image = responseObject.getString("image");
                        }
                        String user_sport = responseObject.getString("sport");
                        String userName = responseObject.getString("username");
                        signedup_at = responseObject.getString("signedup_at");
                        /*int game_invitation_count = responseObject
                                .getInt("game_invitation_count");
                        int friend_invitation_count = responseObject
                                .getInt("friend_invitation_count");
                        boolean recive_push_notification = (Boolean) responseObject
                                .get("receive_push_notification");
                        JSONObject array = responseObject.getJSONObject("notifications");
                        boolean player_request = (Boolean) array.get("player_request");
                        boolean game_invitation = (Boolean) array.get("game_invitation");
                        boolean nearby_games = (Boolean) array.get("nearby_game");
                        boolean nearby_games_all = (Boolean) array.get("nearby_game_all");
                        boolean nearby_games_primary = (Boolean) array
                                .get("nearby_game_primary");
                        boolean message_board = (Boolean) array.get("message_board");
                        boolean game_update = (Boolean) array.get("game_update");
                        boolean comment_on_post = (Boolean) array.get("comment_on_post");
                        boolean game_cancellation = (Boolean) array.get("game_cancel");
                        boolean game_edit = (Boolean) array.get("game_edit");
                        boolean game_join_leave = (Boolean) array.get("game_join_leave");*/

                        //Keeping the login status either from Facebook or Normal login
                        logInType = "NORMAL_LOGIN";

                        //Storing the data in the Shatred Preferance
                        storeDatainSharedPreferance(auth_token, userName, device_info_id,
                                user_id, firstname, lastname, image, user_email, phone_number, user_sport);

                        //Stroing the user info for quick access
                        userInfoQuickAccess(auth_token, user_id, userName);

                        //CLearing all the previously holded values
                        clearAllUserValues();


                        NormalRegistrationStepTwo.this.runOnUiThread(new Runnable() {
                            public void run() {
                               /* ImageUploadManager.uploadedFileName = null;
                                Constants.isImageUploaded = false;
                                ImageUploadManager.profilePicS3Url = null;*/

                                releaseImageUploadValues();
                                //Handling the loader state
                                LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, false);
                                //Displaying the success message after successful sign up
                                Toast.makeText(NormalRegistrationStepTwo.this, "Account successfully created", Toast.LENGTH_LONG).show();
                                //Redirecting the user to the home screen.
                                redirectUserHomeScreen();
                            }
                        });

                        //Storing the data in the Shatred Preferance
                        storeDatainSharedPreferance(auth_token, userName, device_info_id,
                                user_id, firstname, lastname, image, user_email, phone_number, user_sport);


                    } else if (status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)) {
                        final String message = responseObject.getString("message");
                        //Displaying the Error Message
                        NormalRegistrationStepTwo.this.runOnUiThread(new Runnable() {
                            public void run() {
                                //Handling the loader state
                                LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, false);
                                snackbar.setSnackBarMessage(message);
                            }
                        });

                    } else {
                        NormalRegistrationStepTwo.this.runOnUiThread(new Runnable() {
                            public void run() {
                                //Handling the loader state
                                LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, false);
                                snackbar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE);
                            }
                        });
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    NormalRegistrationStepTwo.this.runOnUiThread(new Runnable() {
                        public void run() {
                            //Handling the loader state
                            LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, false);
                            snackbar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE);
                        }
                    });
                }

            }
        });
    }


    /**
     * Function responsible for releasing all the image upload values
     */
    public void releaseImageUploadValues(){
        ImageUploadManager.uploadedFileName = null;
        Constants.isImageUploaded = false;
        ImageUploadManager.profilePicS3Url = null;
    }

    /**
     * Keeping the user data in the shared preferance after successful login
     *
     * @param auth_token     user auth_token
     * @param mUsername      user Username
     * @param device_info_id user device infoid
     * @param user_id        user user_id
     * @param firstname      user firstname
     * @param lastname       user lastname
     * @param image          user Profile image
     */

    public void storeDatainSharedPreferance(String auth_token, String mUsername,
                                            String device_info_id, String user_id,
                                            String firstname, String lastname,
                                            String image, String userEmail,
                                            String userPhone, String userSport) {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(NormalRegistrationStepTwo.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("auth_token", auth_token);
        editor.putString("USERNAME", mUsername);
        editor.putString("device_info_id", device_info_id);
        editor.putString("loginuser_id", user_id);
        editor.putString("username", mUsername);
        editor.putString("firstname", firstname);
        editor.putString("lastname", lastname);
        editor.putString("image", image);
        editor.putString("user_email", userEmail);
        editor.putString("user_phone", userPhone);
        editor.putString("user_sport", userSport);
        editor.putString("LOGIN_TYPE", logInType);
        editor.commit();
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(NormalRegistrationStepTwo.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Redirecting the user to the NavigationDrawer screen
     */
    public void redirectUserHomeScreen() {
        SigninActivity.mcontext.finish();
        NormalRegistrationStepOne.mNormalsignupContext.finish();
        NormalRegistrationStepTwo.this.finish();
        if (NormalRegistrationStepOne.mNormalsignupContext != null) {
            NormalRegistrationStepOne.mNormalsignupContext.finish();
        }
        Intent intent = new Intent(NormalRegistrationStepTwo.this,
                NavigationDrawerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    /**
     * Redirecting the facebook user to the NavigationDrawer screen
     */
    public void redirectFacebookUserHomeScreen() {
        SigninActivity.mcontext.finish();
        FacebookUserRegistrationStepOne.facebookSignuppageoneContext.finish();
        NormalRegistrationStepTwo.this.finish();
        Intent intent = new Intent(NormalRegistrationStepTwo.this,
                NavigationDrawerActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getAllValues();
        NormalRegistrationStepTwo.this.finish();
    }

    /**
     * Function responsible for Closing the Application
     */
    public void stopApplication() {
        Intent homeIntent = new Intent("android.intent.action.MAIN");
        homeIntent.addCategory("android.intent.category.HOME");
        homeIntent.setFlags(67108864);
        this.startActivity(homeIntent);
    }

    /**
     * Function responsible for clearing all tpe hold values
     */
    public void clearAllUserValues() {
        NormalRegistrationStepOne.mUserName = null;
        NormalRegistrationStepOne.mPassword = null;
        NormalRegistrationStepOne.mConfPassword = null;
        NormalRegistrationStepOne.mPrimarySport = null;
        NormalRegistrationStepOne.mSportId = null;
        userAboutMe = null;
        userBirthDate = null;
        userLastName = null;
        userFirstName = null;
        userEmail = null;
        userImage = null;
        userPhoneNumber = null;
        SigninActivity.fb_user_firstname = null;
        SigninActivity.fb_user_lastname = null;
        SigninActivity.fb_user_name = null;
        SigninActivity.fb_user_email = null;
        SigninActivity.fb_user_image = null;
        SigninActivity.fb_user_id = null;
        ImageUploadManager.profilePicS3Url = null;
        profileEditor.clear();
    }

    /**
     * Function responsible for clearing all facebook uaer data
     */
    public void resetAllFacebookValues() {
        SigninActivity.fb_user_firstname = null;
        SigninActivity.fb_user_lastname = null;
        SigninActivity.fb_user_email = null;
        SigninActivity.fb_user_gender = null;
        SigninActivity.fb_user_image = null;
        SigninActivity.fb_user_name = null;
        profileEditor.clear();
    }

    /**
     * Function responsible for pre filling the user facebook data in the text fields
     */
    public void setFacebookUserData() {
        if (!TextUtils.isEmpty(SigninActivity.fb_user_firstname)) {
            etFirstname.setText(SigninActivity.fb_user_firstname);
            etLastname.setText(SigninActivity.fb_user_lastname);
        }

        if (!TextUtils.isEmpty(SigninActivity.fb_user_email)) {
            etEmail.setText(SigninActivity.fb_user_email);
            etEmail.setEnabled(false);
        }

        //Setting user profile image, retrived from facebook
        if (!TextUtils.isEmpty(SigninActivity.fb_user_image)) {
            Glide.with(this).load(SigninActivity.fb_user_image).into(imgUserProfileImage);
        }
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildFacebookLoginApiRequestBody(String fb_userId, String fb_userName,
                                                            String fb_userImage, String fb_userEmail,
                                                            String fb_userfirstname, String fb_userlastname,
                                                            String phone_number, String aboutMe, String birthDate) {
        //Building the API with all the required Parameters
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "fblogin?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_UID, fb_userId);
        urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, FacebookUserRegistrationStepOne.mUsername);
        urlBuilder.addQueryParameter(Constants.PARAM_FIRST_NAME, fb_userfirstname);
        urlBuilder.addQueryParameter(Constants.PARAM_LAST_NAME, fb_userlastname);
        urlBuilder.addQueryParameter(Constants.PARAM_PHONENUMBER, phone_number);
        urlBuilder.addQueryParameter(Constants.PARAM_FCMID, SigninActivity.FCM_REGISTRATION_ID);
        urlBuilder.addQueryParameter(Constants.PARAM_EMAIL, fb_userEmail);
        urlBuilder.addQueryParameter(Constants.PARAM_PROVIDER, Constants.VALUE_SOCIAL_USER_DATA_PROVIDER);
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETOKEN, SigninActivity.getDeviceToken(NormalRegistrationStepTwo.this));
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETYPE, Constants.DEVICE_TYPE);
        urlBuilder.addQueryParameter(Constants.PARAM_LOCATION, SigninActivity.currLocLatitude +","+SigninActivity.currLocLatitude);
        urlBuilder.addQueryParameter(Constants.PARAM_TIMEZONE, SigninActivity.getDeviceTimeZone());
        if(TextUtils.isEmpty(fb_userImage) && Constants.isImageUploaded &&
                TextUtils.isEmpty(ImageUploadManager.profilePicS3Url)){
            ImageUploadManager.profilePicS3Url = ImageUploadManager.getProfilePictureUrlAgain(ImageUploadManager.uploadedFileName);
            urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, ImageUploadManager.profilePicS3Url);
        }else if(!TextUtils.isEmpty(ImageUploadManager.profilePicS3Url)){
            urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, ImageUploadManager.profilePicS3Url);
        }else{
            urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, fb_userImage);
        }
        //urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, fb_userImage);
        urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE_NAME, "image_1");
        urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE_CONTENT_TYPE, "image/jpeg");


        urlBuilder.addQueryParameter(Constants.PARAM_OTHERSPORT, getOtherSportDetail());
        urlBuilder.addQueryParameter(Constants.PARAM_SKILLLEVEL, skill_level);
        urlBuilder.addQueryParameter(Constants.PARAM_SPORTID, FacebookUserRegistrationStepOne.mSportId);
        urlBuilder.addQueryParameter(Constants.PARAM_ABOUTME, aboutMe);
        urlBuilder.addQueryParameter(Constants.PARAM_BIRTHDATE, birthDate);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Facebook User Login task
     *
     * @param fb_userId_       Facebook User id
     * @param fb_userName      Facebook User full name
     * @param fb_userImage     Facebook User Image
     * @param fb_userEmail     Facebook User Email
     * @param fb_userfirstname Facebook User FirstName
     * @param fb_userlastname  Facebook User LastName
     */
    public void facebookUserLoginTask(String fb_userId_, String fb_userName,
                                      String fb_userImage, String fb_userEmail,
                                      String fb_userfirstname, String fb_userlastname,
                                      String phoneNumber, String aboutMe, String birthDate) {

        String message = null;
        //Handling the loader state
        LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildFacebookLoginApiRequestBody(fb_userId_, fb_userName, fb_userImage,
                fb_userEmail, fb_userfirstname, fb_userlastname, phoneNumber, aboutMe, birthDate);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    String err_msg = responseObject.getString("status");
                    if (err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                        //Parsing the data from the JSON object
                        String auth_token = responseObject.getString("auth_token");
                        String device_info_id = responseObject.getString("device_info_id");
                        String user_id = responseObject.getString("user_id");
                        String phone_number = responseObject.getString("phone_number");
                        String firstname = responseObject.getString("first_name");
                        String lastname = responseObject.getString("last_name");
                        //String current_status = responseObject.getString("current_status");
                        String user_email = responseObject.getString("email");
                        String birth_day = responseObject.getString("birth_date");
                        String image = responseObject.getString("image");
                        String user_sport = responseObject.getString("sport");
                        String userName = responseObject.getString("username");
                     /*   int game_invitation_count = responseObject
                                .getInt("game_invitation_count");
                        int friend_invitation_count = responseObject
                                .getInt("friend_invitation_count");
                        boolean recive_push_notification = (Boolean) responseObject
                                .get("receive_push_notification");
                        JSONObject array = responseObject.getJSONObject("notifications");
                        boolean player_request = (Boolean) array.get("player_request");
                        boolean game_invitation = (Boolean) array.get("game_invitation");
                        boolean nearby_games = (Boolean) array.get("nearby_game");
                        boolean nearby_games_all = (Boolean) array.get("nearby_game_all");
                        boolean nearby_games_primary = (Boolean) array
                                .get("nearby_game_primary");
                        boolean message_board = (Boolean) array.get("message_board");
                        boolean game_update = (Boolean) array.get("game_update");
                        boolean comment_on_post = (Boolean) array.get("comment_on_post");
                        boolean game_cancellation = (Boolean) array.get("game_cancel");
                        boolean game_edit = (Boolean) array.get("game_edit");
                        boolean game_join_leave = (Boolean) array.get("game_join_leave");*/

                        logInType = "FACEBOOK_LOGIN";

                        //Storing the data in the Shatred Preferance
                        storeDatainSharedPreferance(auth_token, userName, device_info_id,
                                user_id, firstname, lastname, image, user_email, phone_number, user_sport);

                        //Stroing the user info for quick access
                        userInfoQuickAccess(auth_token, user_id, userName);

                        //Resetting the User Login type
                        AppConstants.USER_LOGIN_TYPE_FACEBOOK = false;

                        //Resetting all the Facebook User Data
                        resetAllFacebookValues();

                        //Resetting all the user Registartion Data
                        clearAllUserValues();

                        //Resetting all the values
                        releaseImageUploadValues();

                        //Theating this user as a new user in the APP. Redirecting the user to the sign up screen
                        redirectFacebookUserHomeScreen();

                    } else if (err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_CREATE_FAILED)) {
                        String msg = responseObject.getString("message");
                        snackbar.setSnackBarMessage(msg);
                    } else if (err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)) {
                        etEmail.setEnabled(true);
                        //Displaying the Error Message
                        String msg = responseObject.getString("message");
                        snackbar.setSnackBarMessage(msg);
                    } else {
                        snackbar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                //Handling the loader state
                LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, false);
            }
        });
    }

    //Delegate method to set the date
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "/"+ (++monthOfYear) + "/" + year;
        String OutputDate_For_Display = ApplicationUtility.formattedDateForDisplay(date);
        //Getting current date from Calender & Converting into in milisecond
        long current_date_milisecond = currentDateInMilisecond();

        if (selected_date_milisecond > current_date_milisecond) {
            Toast.makeText(this, "Selected date should not be less from current date", Toast.LENGTH_LONG).show();
        } else {
            //etbirthday.setText(date);
            etbirthday.setText(OutputDate_For_Display);
        }
    }

    /**
     * Getting current date from Calender & Converting into in milisecond
     */
    public long currentDateInMilisecond() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(c.getTime());

        return convertToMilliseconds(formattedDate);
    }

    /**
     * Return date into milisecond
     *
     * @param date
     * @return
     */
    public long convertToMilliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();

            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public String formattedDate(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMM dd yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

            selected_date_milisecond = date1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * Get All the Values from the views
     */

    public void getAllValues() {
        userPhoneNumber = etNumber.getText().toString();
        userEmail = etEmail.getText().toString();
        userFirstName = etFirstname.getText().toString();
        userLastName = etLastname.getText().toString();
        userBirthDate = etbirthday.getText().toString();
        userAboutMe = etAboutme.getText().toString();
        userImage = this.encodedImage;
    }

    /**
     * Function responsible for converting the byteArray to bitmap
     */
    public void convertBitmap(String encodedImage) {
        short pic_hei = 165;
        short pic_wei = 165;
        byte[] imageAsBytes = Base64.decode(encodedImage.getBytes(), 0);
        Bitmap photo = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
        photo = Bitmap.createScaledBitmap(photo, pic_hei, pic_wei, true);
        Bitmap rsz_image = getRoundedShape(photo);
        this.imgUserProfileImage.setImageBitmap(rsz_image);
        this.imgUserProfileImage.setBackgroundColor(-1);
        this.imgUserProfileImage.setAdjustViewBounds(true);
        this.imgUserProfileImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }


    /**
     * Restoring User Inputted Data
     */
    public void restoreData() {
        if (!TextUtils.isEmpty(userFirstName)) {
            etFirstname.setText(userFirstName);
        }

        if (!TextUtils.isEmpty(userLastName)) {
            etLastname.setText(userLastName);
        }

        if (!TextUtils.isEmpty(userPhoneNumber)) {
            etNumber.setText(userPhoneNumber);
        }

        if (!TextUtils.isEmpty(userEmail)) {
            etEmail.setText(userEmail);
        }

        if (!TextUtils.isEmpty(userBirthDate)) {
            etbirthday.setText(userBirthDate);
        }

        if (!TextUtils.isEmpty(userAboutMe)) {
            etAboutme.setText(userAboutMe);
        }

        if (!TextUtils.isEmpty(ImageUploadManager.profilePicS3Url)) {
           // convertBitmap(userImage);
            Glide.with(NormalRegistrationStepTwo.this).load(ImageUploadManager.profilePicS3Url).into(imgUserProfileImage);
            text_profile_image.setVisibility(View.GONE);
        }
    }

    /**
     * Function to build the Registration APi request parameters
     *
     * @return APi request
     */
    public Request buildApiParametersUpdateImage(String image, String auth_token) {

        //Building the API with all the required Parameters
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "users/" + auth_token + "/save_image?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_PROFILE_IMAGE, image);
        urlBuilder.addQueryParameter(Constants.PARAM_PROFILE_IMAGE_FILENAME, "image1");
        urlBuilder.addQueryParameter(Constants.PARAM_PROFILE_IMAGE_CONTENT_TYPE, Constants.PARAM_PROFILE_IMAGE_CONTENT_TYPE);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    public void updateUserProfilePic(String encoded_user_profile_image, String authToken) {

        String message = null;
        //Handling the loader state
        LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiParametersUpdateImage(encoded_user_profile_image, authToken);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    String err_msg = responseObject.getString("status");
                    if (err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                        //Theating this user as a new user in the APP. Redirecting the user to the sign up screen
                        redirectUserHomeScreen();
                    } else if (err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_CREATE_FAILED)) {
                        String msg = responseObject.getString("message");
                        snackbar.setSnackBarMessage(msg);
                    } else if (err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)) {
                        //Displaying the Error Message
                        String msg = responseObject.getString("message");
                        snackbar.setSnackBarMessage(msg);
                    } else {
                        snackbar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                //Handling the loader state
                LoaderUtility.handleLoader(NormalRegistrationStepTwo.this, false);
            }
        });
    }

    /**
     * Hiding soft key board
     */
    public void hideSoftKeyBoard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public String getConvertedFileURL(Uri uri, String selectedVideoPath) {
        File root = new File(Environment.getExternalStorageDirectory() + "/.tempFolder/");
        if (!root.exists()) {
            root.mkdirs();
        }
        Matrix matrix = new Matrix();
        int angel = getCameraPhotoOrientation(NormalRegistrationStepTwo.this, uri, selectedVideoPath);
        matrix.postRotate(angel);
        Bitmap resizedBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
                myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);*/
        byte[] byteArray = stream.toByteArray();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File file = new File(root, timeStamp + ".jpg");
        // Log.i(TAG, "" + file);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            out.write(byteArray);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.toString();
    }

    /**
     * Function responsible for getting the camera photo orientation
     *
     * @param context
     * @param imageUri
     * @param imagePath
     * @return the camera angle
     */
    public int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.i("RotateImage", "Exif orientation: " + orientation);
            Log.i("RotateImage", "Rotate value: " + rotate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

   /* *//**
     * Function responsible for Getting the captured
     * image,recorded Video,picked image/Video from gallery
     *
     * @param requestCode The Request COde
     * @param resultCode  The Result Code
     * @param intent      intent Data
     *//*
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);

        *//**
     * Condition added for clicked images
     *//*
        if (requestCode == TAKE_PICTURE) {

            if (resultCode == RESULT_OK) {
                String selectedVideoPath = null;
                Uri uri = null;
                try {
                    uri = Uri.fromFile(cameraImagefile);
                    selectedVideoPath = getPath(uri);
                } catch (URISyntaxException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                String type = getMimeType(selectedVideoPath);
                if (type != null && type.contains("image")) {
                    File imgFile = new File(selectedVideoPath);
                    Bitmap myBitmap = null;
                    if (imgFile.exists()) {
                        myBitmap = BitmapFactory.decodeFile(imgFile
                                .getAbsolutePath());
                    }

                   *//* File root = new File(Environment.getExternalStorageDirectory() + "/.tempFolder/");
                    if (!root.exists()) {
                        root.mkdirs();
                    }
                    Matrix matrix = new Matrix();
                    int angel = getCameraPhotoOrientation(NormalRegistrationStepTwo.this, uri, selectedVideoPath);
                    matrix.postRotate(angel);
                    Bitmap resizedBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
                            myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] byteArray = stream.toByteArray();
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    File file = new File(root, timeStamp + ".jpg");
                    if (file.exists())
                        file.delete();
                    try {
                        FileOutputStream out = new FileOutputStream(file);
                        out.write(byteArray);
                        out.flush();
                        out.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*//*

                    String convertedFilePath = getConvertedFileURL(uri,selectedVideoPath);

                } else if (resultCode == RESULT_CANCELED) {

                    Toast.makeText(getApplicationContext(),
                            "User cancelled Image Capturing", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        } else if (resultCode == RESULT_CANCELED) {
            // user cancelled recording
            Toast.makeText(getApplicationContext(),
                    "User cancelled video recording", Toast.LENGTH_SHORT)
                    .show();
        } else if (requestCode == PICK_FROM_GALLERY) {

            if (resultCode == RESULT_OK) {

                Uri uri = intent.getData();

                String selectedVideoPath = null;
                try {
                    selectedVideoPath = getPath(uri);
                } catch (URISyntaxException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                File imgFile = new File(selectedVideoPath);

                if (imgFile.exists()) {
                    myBitmap = BitmapFactory.decodeFile(imgFile
                            .getAbsolutePath());
                }



                *//*File root = new File(Environment.getExternalStorageDirectory() + "/.tempFolder/");
                if (!root.exists()) {
                    root.mkdirs();
                }
                Matrix matrix = new Matrix();
                int angel = getCameraPhotoOrientation(NormalRegistrationStepTwo.this, uri, selectedVideoPath);
                matrix.postRotate(angel);
                Bitmap resizedBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
                        myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        *//**//*ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);*//**//*
                byte[] byteArray = stream.toByteArray();
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                File file = new File(root, timeStamp + ".jpg");
                // Log.i(TAG, "" + file);
                if (file.exists())
                    file.delete();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    out.write(byteArray);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }*//*

                String convertedFilePath = getConvertedFileURL(uri,selectedVideoPath);

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(),
                        "User has cancelled the operation", Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "No media file choosen", Toast.LENGTH_LONG).show();
            }

        }
    }*/

    /*
    * Gets the file path of the given Uri.
    */
    @SuppressLint("NewApi")
    public String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri
                && DocumentsContract
                .isDocumentUri(NormalRegistrationStepTwo.this, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/"
                        + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = NormalRegistrationStepTwo.this.getContentResolver().query(uri, projection, selection,
                        selectionArgs, null);
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }


}
