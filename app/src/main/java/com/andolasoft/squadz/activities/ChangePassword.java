package com.andolasoft.squadz.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.ConnectivityCheckApplicationClass;
import com.andolasoft.squadz.utils.ConnectivityReceiver;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.firebase.client.core.view.Change;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SpNayak on 1/31/2017.
 */

public class ChangePassword extends AppCompatActivity {

    @InjectView(R.id.back_changePassword_layout)
    RelativeLayout back_changePassword_layout;
    @InjectView(R.id.change_password_btn)
    Button btn_change_password;
    @InjectView(R.id.current_password)
    EditText current_password;
    @InjectView(R.id.new_password)
    EditText new_password;
    @InjectView(R.id.confirm_password)
    EditText confirm_password;
    SnackBar snackBar;
    SharedPreferences preferences;
    SharedPreferences.Editor profileEditor;
    ProgressDialog dialog;
    String auth_token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_change_password);

        /**
         * Initializing views
         */
        initViews();

        /**
         * Adding Click events on te views
         */
        setClickEvents();

        // Manually checking internet connection
       // checkConnection();

    }

    /*// Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }*/

    /**
     * Adding Click events on te views
     */
    public void setClickEvents() {
        //Back button click
        back_changePassword_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Change password button click
        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String get_Current_Password = current_password.getText().toString().trim();
                String get_New_Password = new_password.getText().toString().trim();
                String get_Confirm_Password = confirm_password.getText().toString().trim();

                //Adding validation & call change password API
                isChangePasswordValidate(get_Current_Password,get_New_Password,get_Confirm_Password);
            }
        });

        confirm_password.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    String get_Current_Password = current_password.getText().toString().trim();
                    String get_New_Password = new_password.getText().toString().trim();
                    String get_Confirm_Password = confirm_password.getText().toString().trim();

                    //Adding validation & call change password API
                    isChangePasswordValidate(get_Current_Password,get_New_Password,get_Confirm_Password);

                    return true;
                }
                return false;
            }
        });
    }

    /**
     * Change password validation & call change password API
     */
    public boolean isChangePasswordValidate(String current_Password,
                                            String new_Password,
                                            String confirm_NewPassword) {
        boolean isValidate = false;
        if(TextUtils.isEmpty(current_Password)) {
            snackBar.setSnackBarMessage("Current password cann't be empty");
        } else if(TextUtils.isEmpty(new_Password)) {
            snackBar.setSnackBarMessage("New password cann't be empty");
        } else if(TextUtils.isEmpty(confirm_NewPassword)) {
            snackBar.setSnackBarMessage("Confirm password cann't be empty");
        } else if(!confirm_NewPassword.equals(new_Password)) {
            snackBar.setSnackBarMessage("Confirm password should be same as new password");
        } else{
            isValidate = true;
            //Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
            changePassword(auth_token,current_Password,new_Password,confirm_NewPassword);
        }
        return isValidate;
    }

    /**
     * Integrated Change password API
     */
    public void changePassword(String auth_token, String old_password, String new_password, String confirm_password) {
        //Handling the loader state
        LoaderUtility.handleLoader(ChangePassword.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(auth_token,old_password,new_password,confirm_password);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(ChangePassword.this, false);
                if(!ApplicationUtility.isConnected(ChangePassword.this)){
                    ApplicationUtility.displayNoInternetMessage(ChangePassword.this);
                }else{
                    ApplicationUtility.displayErrorMessage(ChangePassword.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    if(jsonObject.has("status") && !jsonObject.isNull("status")) {
                        status = jsonObject.getString("status");
                    }
                    if(jsonObject.has("message") && !jsonObject.isNull("message")) {
                        message = jsonObject.getString("message");
                    }
                }catch (Exception exp) {
                    exp.printStackTrace();
                }
                final String finalStatus = status;
                final String finalMessage = message;

                ChangePassword.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(ChangePassword.this, false);

                        if(finalStatus.equalsIgnoreCase("success")) {
                            snackBar.setSnackBarMessage(finalMessage);
                            resetData();
                            ChangePassword.this.finish();
                            Toast.makeText(ChangePassword.this, "Password Successfully Changed", Toast.LENGTH_SHORT).show();
                        } else{
                            snackBar.setSnackBarMessage(finalMessage);
                        }
                    }
                });

            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String auth_token, String old_password, String new_password, String confirm_password) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "change_password").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("old_password", old_password);
        urlBuilder.addQueryParameter("new_password", new_password);
        urlBuilder.addQueryParameter("confirm_password", confirm_password);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(ChangePassword.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }
    /**
     * Reset fields
     */
    public void resetData() {
        current_password.setText("");
        new_password.setText("");
        confirm_password.setText("");

    }
    /**
     * Initializing views
     */
    public void initViews() {
        ButterKnife.inject(this);
        snackBar = new SnackBar(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        profileEditor = this.preferences.edit();

        if (preferences.contains("auth_token")) {
            auth_token = preferences.getString("auth_token", "");
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register connection status listener
       // ConnectivityCheckApplicationClass.getInstance().setConnectivityListener(ChangePassword.this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    /*@Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }*/

    public void showSnack(boolean isConnected){
        SnackBar snackBar = new SnackBar(ChangePassword.this);
        if(isConnected){
            snackBar.setSnackBarMessage("Connected");
        }else{
            snackBar.setSnackBarMessage("Not Connected");
        }

    }
}
