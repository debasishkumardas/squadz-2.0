package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.NestedListView;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.adapters.FilterVenuesSpecificationAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Filter extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    int minteger = 0;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    private ListView venue_spec_listView;
    private FilterVenuesSpecificationAdapter filterAdapter;
    private NestedListView nestedListView;
    private RelativeLayout selectDate_layout, from_time_main_layout,
            to_time_main_layout, radius_ralative_layout, date_time_layout,
            back_icon_filter_layout,ic_radius_drop_down_layout,
            ic_paid_venues_drop_down_layout,ic_venues_datetime_dropdown_layout,
            ic_venue_specification_layout;
    private TextView txt_select_date, txt_from_time, txt_to_time,
            txt_mapRadius_miles;
    private ScrollView scroll_view;
    private String str_IsTimeClicked = "";
    private ImageView ic_radius_drop_down,ic_radius_drop_up ,ic_paid_venues_drop_down,
            ic_venues_datetime_dropdown, ic_venue_specification,ic_paid_venues_drop_up,
            ic_venues_datetime_dropup,ic_venue_specification_up;
    private Button btn_apply_filter, plus_btn, minus_btn;
    private boolean isRadiusClicked = true, isPaidVenuesClicked = true,
            isVenuesDateTimeClicked = true, isVenuesSpecificationClicked = true;
    private RadioGroup radioGroup_Paid_Venues;
    private RadioButton filter_radio_proximity, filter_radio_low_high,
            filter_radio_high_low /*, filter_radio_public_free_venues, filter_radio_venues_nearby*/;
    private long selected_date_milisecond;
    boolean isSelected= true;
    private ProgressDialog dialog;
    public HashMap<String, ArrayList<String>> haspMap;
    SnackBar snackBar;
    boolean isStausSuccess = true;
    private CheckBox filter_check_venues_nearby, filter_checkbox_public_free_venues;
    ArrayList<String> all_specs_array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_filter);

        /**
         * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
         */
        initReferences();


        //Hiding the Action Bars
        //hideActionBar();

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(Filter.this);

        /**
         * CLICK EVENTS ON THE VIEWS
         */
        setClickEvents();

        /**
         * CHECK IF ANY VALUES EXIST
         */
        checkValueExistanceAfterFilter();

        /**
         * Function to get Venue Courts Specifications from server
         */
        try {
            getVenueCourtsSpec();

        }catch (Exception exp)
        {
            exp.printStackTrace();
        }

        /**
         * Getting Current Date at page loading
         */
        //currentDate();


    }

    /**
     * Hiding the ActionBar
     */
    public void hideActionBar(){
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.hide();
    }

    /**
     * FUNCTIONA;LITY TO SEND VENUES SPECIFICATION NAME
     * TO THE ADAPTER CLASS
     */
    public void sendVenuesNameToAdapter() {
        ArrayList<String> venuesNameList = new ArrayList<>();
        //String[] venus = {"Full court", "Half court", "High court", "NCAA", "Professional", "Indoor", "Outdoor"};

        if(!TextUtils.isEmpty(Constants.SPORTS_NAME))
        {
            ArrayList<String> court_name_array = haspMap.get(Constants.SPORTS_NAME);

            if(court_name_array != null)
            {
                for (int i = 0; i < court_name_array.size(); i++)
                {
                    venuesNameList.add(court_name_array.get(i));
                }
            }
            else if(Constants.SPORTS_NAME.equalsIgnoreCase("ALL"))
            {
                venuesNameList = all_specs_array;
            }
        }

        venuesNameList.add("High court");
        venuesNameList.add("NCAA");
        venuesNameList.add("Professional");
        venuesNameList.add("Indoor");
        venuesNameList.add("Outdoor");
        /*String[] venus = {"High court", "NCAA", "Professional", "Indoor", "Outdoor"};

        for (int i = 0; i < venus.length; i++) {
            venuesNameList.add(venus[i]);
        }*/

        //if(haspMap.)
        filterAdapter = new FilterVenuesSpecificationAdapter(Filter.this, venuesNameList);
        venue_spec_listView.setAdapter(filterAdapter);
        filterAdapter.notifyDataSetChanged();

        //SCROLLING WHOLE LAYOUT WITH LISTVIEW WITH THIS METHOD
        NestedListView.setListViewHeightBasedOnChildren(venue_spec_listView);

        /**
         * Scroll layout to up
         */
        scrollToUp();
    }

    /**
     * CLICK EVENTS ON THE VIEWS
     */
    public void setClickEvents() {
        /**
         * CLICK EVENT FOR SELECT DATE
         */

        selectDate_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        Filter.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.setAccentColor(getResources().getColor(R.color.deep_orange));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        /**
         * CLICK EVENT FOR FROM TIME
         */

        from_time_main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //KEEPING CLICKED NAME IN A STRING FOR DISPLAYING THE FROM AND TO TIME
                str_IsTimeClicked = "FROM_TIME";

                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        Filter.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false

                );

                tpd.setAccentColor(getResources().getColor(R.color.deep_orange));
                tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        /**
         * CLICK EVENT FOR TO TIME
         */

        to_time_main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //KEEPING CLICKED NAME IN A STRING FOR DISPLAYING THE FROM AND TO TIME
                str_IsTimeClicked = "TO_TIME";

                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        Filter.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false

                );

                tpd.setAccentColor(getResources().getColor(R.color.deep_orange));
                tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        /**
         * CLICK EVENT FOR EXPAND OR COLLAPSE RADIOUS
         */

        ic_radius_drop_down_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isRadiusClicked) {
                    radius_ralative_layout.setVisibility(View.GONE);
                    isRadiusClicked = false;

                    ic_radius_drop_down.setVisibility(View.VISIBLE);
                    ic_radius_drop_up.setVisibility(View.INVISIBLE);
                }
                else {
                    radius_ralative_layout.setVisibility(View.VISIBLE);
                    isRadiusClicked = true;

                    ic_radius_drop_up.setVisibility(View.VISIBLE);
                    ic_radius_drop_down.setVisibility(View.INVISIBLE);
                }
            }
        });

        /**
         * CLICK EVENT FOR EXPAND OR COLLAPSE PAID VENUES
         */

        ic_paid_venues_drop_down_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isPaidVenuesClicked) {
                    radioGroup_Paid_Venues.setVisibility(View.GONE);
                    filter_radio_proximity.setVisibility(View.GONE);
                    isPaidVenuesClicked = false;

                    ic_paid_venues_drop_down.setVisibility(View.VISIBLE);
                    ic_paid_venues_drop_up.setVisibility(View.INVISIBLE);

                } else {
                    radioGroup_Paid_Venues.setVisibility(View.VISIBLE);
                    filter_radio_proximity.setVisibility(View.VISIBLE);
                    isPaidVenuesClicked = true;

                    ic_paid_venues_drop_up.setVisibility(View.VISIBLE);
                    ic_paid_venues_drop_down.setVisibility(View.INVISIBLE);
                }
            }
        });

        /**
         * CLICK EVENT FOR EXPAND OR COLLAPSE DATE AND TIME
         */

        ic_venues_datetime_dropdown_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isVenuesDateTimeClicked) {
                    date_time_layout.setVisibility(View.GONE);
                    isVenuesDateTimeClicked = false;

                    ic_venues_datetime_dropdown.setVisibility(View.VISIBLE);
                    ic_venues_datetime_dropup.setVisibility(View.INVISIBLE);

                } else {
                    date_time_layout.setVisibility(View.VISIBLE);
                    isVenuesDateTimeClicked = true;

                    ic_venues_datetime_dropup.setVisibility(View.VISIBLE);
                    ic_venues_datetime_dropdown.setVisibility(View.INVISIBLE);
                }
            }
        });

        ic_venue_specification_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isVenuesSpecificationClicked) {
                    venue_spec_listView.setVisibility(View.GONE);
                    isVenuesSpecificationClicked = false;

                    ic_venue_specification.setVisibility(View.INVISIBLE);
                    ic_venue_specification_up.setVisibility(View.VISIBLE);

                } else {
                    venue_spec_listView.setVisibility(View.VISIBLE);
                    isVenuesSpecificationClicked = true;

                    ic_venue_specification_up.setVisibility(View.INVISIBLE);
                    ic_venue_specification.setVisibility(View.VISIBLE);

                    /**
                     * Scroll layout to buttom
                     */
                    scrollToButtom();
                }
            }
        });

        /**
         * ACTION BAR BACK BUTTON CLICK
         */
        back_icon_filter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.VENUE_FILTER_BTN_CLICK = false;
                finish();
//                Intent intent_Filter = new Intent(Filter.this, VenueListing.class);
//                startActivity(intent_Filter);
            }
        });

        /**
         *  PAID VENUES RADIO BUTTON CLICK
         */
        filter_radio_proximity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean selected) {

                if (selected) {
                    Constants.FILTER_PROXIMITY = "proximity";
                }

            }
        });

        filter_radio_low_high.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean selected) {

                if (selected) {
                    Constants.FILTER_PAID_VENUES_NAME = "lowtohigh";
                }

            }
        });

        filter_radio_high_low.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean selected) {

                if (selected) {
                    Constants.FILTER_PAID_VENUES_NAME = "hightolow";
                }

            }
        });

        filter_checkbox_public_free_venues.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean selected) {
                if (selected) {
                    Constants.FILTER_PUBLIC_OR_FREE_VENUES_STATUS = true;
                }
                else{
                    Constants.FILTER_PUBLIC_OR_FREE_VENUES_STATUS = false;
                }
            }
        });


        filter_check_venues_nearby.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean selected) {
                if (selected) {
                    Constants.FILTER_VENUES_NEARBY_STATUS = true;
                }
                else{
                    Constants.FILTER_VENUES_NEARBY_STATUS = false;
                }
            }
        });

        /**
         * APPLY BUTTON CLICK
         */
        btn_apply_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**
                 * SAVE VENUES SPECIFICATION ARRAYLIST AS INTERNAL STORAGE FILE
                 */
                storeSpecificationArray();
            }
        });

        /**
         * PLUS BUTTON CLICK EVENT
         */
        plus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.FILTER_MAP_RADIUS_MILES_COUNT != 0) {
                    Constants.FILTER_MAP_RADIUS_MILES_COUNT = Constants.FILTER_MAP_RADIUS_MILES_COUNT + 5;
                    display(Constants.FILTER_MAP_RADIUS_MILES_COUNT);
                } else {
                    minteger = minteger + 5;
                    display(minteger);
                    Constants.FILTER_MAP_RADIUS_MILES_COUNT = minteger;
                }
            }
        });

        /**
         * MINUS BUTTON CLICK EVENT
         */
        minus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.FILTER_MAP_RADIUS_MILES_COUNT != 0) {

                    Constants.FILTER_MAP_RADIUS_MILES_COUNT = Constants.FILTER_MAP_RADIUS_MILES_COUNT - 5;
                    display(Constants.FILTER_MAP_RADIUS_MILES_COUNT);
                } else {
                    if (minteger != 0) {
                        minteger = minteger - 5;
                        display(minteger);
                        Constants.FILTER_MAP_RADIUS_MILES_COUNT = minteger;
                    }
                }


            }
        });
    }

    /**
     * DISPLAY RADIUS COUNT
     *
     * @param number
     */

    private void display(int number) {

        txt_mapRadius_miles.setText("" + number + " Miles");
        Constants.FILTER_MAP_RADIUS_MILES = String.valueOf(number);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        //String date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        String date = (++monthOfYear)+ "/"+dayOfMonth + "/" + year;
        String finalOutputDate = formattedDate(date);
        String OutputDate_For_Display = formattedDateForDisplay(date);

        //Getting current date from Calender & Converting into in milisecond
        long current_date_milisecond = currentDateInMilisecond();

        if (selected_date_milisecond < current_date_milisecond) {
            Toast.makeText(this, "Selected date should not be less from current date", Toast.LENGTH_LONG).show();
        } else {
            //txt_select_date.setText(finalOutputDate);
            txt_select_date.setText(OutputDate_For_Display);
            //Constants.FILTER_SELECT_DATE = finalOutputDate;
            Constants.FILTER_SELECT_DATE = OutputDate_For_Display;

        }
    }

    /**
     * Getting current date from Calender & Converting into in milisecond
     */
    public long currentDateInMilisecond() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(c.getTime());

        return convertToMilliseconds(formattedDate);
    }

    /**
     * Current date
     * @return
     */
    public String currentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(c.getTime());

        txt_select_date.setText(formattedDate);
        Constants.FILTER_SELECT_DATE = formattedDate;

        return formattedDate;
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
        String time = hourString + ":" + minuteString + ":" + secondString;

        String finalOutputTime = formattedTime(time);
        finalOutputTime = ApplicationUtility.getTimeWithFormatWithSpace(finalOutputTime);

        if (str_IsTimeClicked.equalsIgnoreCase("FROM_TIME")) {
            txt_from_time.setText(finalOutputTime);
            Constants.FILTER_FROM_TIME = finalOutputTime;

        } else {
            txt_to_time.setText(finalOutputTime);
            Constants.FILTER_TO_TIME = finalOutputTime;
        }

    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public String formattedDate(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MM/dd/yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

            selected_date_milisecond = date1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }
    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public String formattedDateForDisplay(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMMM dd, yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING TIME FORMAT WITH CUSTOM TIME
     */
    public String formattedTime(String inputTime) {

        String finalFormatedTime = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("hh:mm a");

        try {

            Date date1 = inputFormatter.parse(inputTime);
            finalFormatedTime = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedTime;
    }

    /**
     * Return date into milisecond
     *
     * @param date
     * @return
     */
    public static long convertToMilliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();

            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * CHECK IF ANY VALUES EXIST AFTER FILTER
     */
    public void checkValueExistanceAfterFilter() {
        /**
         * CHECKING FOR PAID VENUES
         */
        if (!TextUtils.isEmpty(Constants.FILTER_PAID_VENUES_NAME)) {
             if (Constants.FILTER_PAID_VENUES_NAME.equalsIgnoreCase("lowtohigh")) {
                filter_radio_low_high.setChecked(true);
                Constants.FILTER_PAID_VENUES_NAME = "lowtohigh";
            } else if (Constants.FILTER_PAID_VENUES_NAME.equalsIgnoreCase("hightolow")) {
                filter_radio_high_low.setChecked(true);
                Constants.FILTER_PAID_VENUES_NAME = "hightolow";
            }
        }
        if (!TextUtils.isEmpty(Constants.FILTER_PROXIMITY)) {
            if (Constants.FILTER_PROXIMITY.equalsIgnoreCase("proximity")) {
                filter_radio_proximity.setChecked(true);
                Constants.FILTER_PAID_VENUES_NAME = "proximity";
            }
        }

        /**
         * CHECKING FOR MAP RADIUS MILES
         */
        if (!TextUtils.isEmpty(Constants.FILTER_MAP_RADIUS_MILES)) {
            txt_mapRadius_miles.setText("" + Constants.FILTER_MAP_RADIUS_MILES + " Miles");
        }

        /**
         * CHECKING PUBLIC VENUES
         */
        /*if (Constants.FILTER_PUBLIC_OR_FREE_VENUES_STATUS) {
            filter_radio_public_free_venues.setChecked(true);
        }*/

        /**
         * CHECKING VENUES NEARBY
         */
       /* if (Constants.FILTER_VENUES_NEARBY_STATUS) {
            filter_radio_venues_nearby.setChecked(true);
        }*/

        /**
         * CHECKING PUBLIC VENUES
         */
        if (Constants.FILTER_PUBLIC_OR_FREE_VENUES_STATUS) {
            filter_checkbox_public_free_venues.setChecked(true);
        }

        /**
         * CHECKING VENUES NEARBY
         */
        if (Constants.FILTER_VENUES_NEARBY_STATUS) {
            filter_check_venues_nearby.setChecked(true);
        }

        /**
         * CHECKING FOR SELECT DATE
         */
        if (!TextUtils.isEmpty(Constants.FILTER_SELECT_DATE)) {
            txt_select_date.setText(Constants.FILTER_SELECT_DATE);
        }
        /**
         * CHECKING FROM TIME & TO TIME
         */
        if (!TextUtils.isEmpty(Constants.FILTER_FROM_TIME)) {
            txt_from_time.setText(Constants.FILTER_FROM_TIME);
        }
        if (!TextUtils.isEmpty(Constants.FILTER_TO_TIME)) {
            txt_to_time.setText(Constants.FILTER_TO_TIME);
        }

    }

    /**
     * SAVE VENUES SPECIFICATION ARRAYLIST SHARED PREFERENCE
     */
    public void storeSpecificationArray() {

        if(Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY.size() > 0)
        {
            Set<String> set = new HashSet<String>();
            set.addAll(Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY);
            editor.putStringSet("VENUES_SPECIFICATIONS", set);
            editor.commit();

            Constants.VENUE_FILTER_BTN_CLICK = true;
            finish();
        }
        else if(Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY.size() == 0 && Constants.UNCHECK_SPEC)
        {
            Set<String> set = new HashSet<String>();
            set.addAll(Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY);
            editor.putStringSet("VENUES_SPECIFICATIONS", set);
            editor.clear();
            editor.commit();

            Constants.VENUE_FILTER_BTN_CLICK = true;
            finish();
        }
        else if(!TextUtils.isEmpty(Constants.FILTER_PAID_VENUES_NAME) ||
                !TextUtils.isEmpty(Constants.FILTER_SELECT_DATE) ||
                !TextUtils.isEmpty(Constants.FILTER_FROM_TIME) ||
                !TextUtils.isEmpty(Constants.FILTER_TO_TIME) ||
                Constants.FILTER_MAP_RADIUS_MILES_COUNT != 0)
        {
            Set<String> set = new HashSet<String>();
            set.addAll(Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY);
            editor.putStringSet("VENUES_SPECIFICATIONS", set);
            editor.clear();
            editor.commit();

            Constants.VENUE_FILTER_BTN_CLICK = true;
            finish();
        }
        else{

            Set<String> set = new HashSet<String>();
            set.addAll(Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY);
            editor.putStringSet("VENUES_SPECIFICATIONS", set);
            editor.clear();
            editor.commit();

            Constants.VENUE_FILTER_BTN_CLICK = false;
            finish();
        }


//        Intent intent_VenueListing = new Intent(Filter.this, VenueListing.class);
//        startActivity(intent_VenueListing);
    }


    /**
     * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
     */
    public void initReferences() {
        venue_spec_listView = (ListView) findViewById(R.id.venue_spec_listView);
        selectDate_layout = (RelativeLayout) findViewById(R.id.selectDate_layout);
        from_time_main_layout = (RelativeLayout) findViewById(R.id.from_time_main_layout);
        to_time_main_layout = (RelativeLayout) findViewById(R.id.to_time_main_layout);
        radius_ralative_layout = (RelativeLayout) findViewById(R.id.radius_ralative_layout);
        date_time_layout = (RelativeLayout) findViewById(R.id.date_time_layout);
        txt_select_date = (TextView) findViewById(R.id.select_date_btn);
        txt_from_time = (TextView) findViewById(R.id.filter_from_time);
        txt_to_time = (TextView) findViewById(R.id.filter_to_time);
        txt_mapRadius_miles = (TextView) findViewById(R.id.filter_mapRadius_miles);
        ic_radius_drop_down = (ImageView) findViewById(R.id.radius_drop_down_icon);
        ic_radius_drop_up = (ImageView) findViewById(R.id.radius_drop_up_icon);
        ic_radius_drop_down_layout = (RelativeLayout) findViewById(R.id.radius_drop_down_icon_layout);
        ic_paid_venues_drop_down = (ImageView) findViewById(R.id.paid_venues_drop_down_icon);
        ic_paid_venues_drop_up = (ImageView) findViewById(R.id.paid_venues_drop_up_icon);
        ic_paid_venues_drop_down_layout = (RelativeLayout) findViewById(R.id.paid_venues_drop_down_icon_layout);
        ic_venues_datetime_dropdown = (ImageView) findViewById(R.id.venues_datetime_dropdown_icon);
        ic_venues_datetime_dropdown_layout = (RelativeLayout) findViewById(R.id.venues_datetime_dropdown_icon_layout);
        ic_venues_datetime_dropup = (ImageView) findViewById(R.id.venues_datetime_dropup_icon);
        ic_venue_specification = (ImageView) findViewById(R.id.venue_specification_icon);
        ic_venue_specification_layout = (RelativeLayout) findViewById(R.id.venue_specification_icon_layout);
        ic_venue_specification_up = (ImageView) findViewById(R.id.venue_specification_up_icon);
        back_icon_filter_layout = (RelativeLayout) findViewById(R.id.back_icon_filter_layout);
        radioGroup_Paid_Venues = (RadioGroup) findViewById(R.id.radioGroup);
        filter_radio_proximity = (RadioButton) findViewById(R.id.filter_radio_proximity);
        filter_radio_low_high = (RadioButton) findViewById(R.id.filter_radio_low_high);
        filter_radio_high_low = (RadioButton) findViewById(R.id.filter_radio_high_low);
//        filter_radio_public_free_venues = (RadioButton) findViewById(R.id.filter_radio_public_free_venues);
//        filter_radio_venues_nearby = (RadioButton) findViewById(R.id.filter_radio_venues_nearby);
        filter_checkbox_public_free_venues = (CheckBox) findViewById(R.id.filter_checkbox_public_free_venues);
        filter_check_venues_nearby = (CheckBox) findViewById(R.id.filter_check_venues_nearby);
        btn_apply_filter = (Button) findViewById(R.id.filter_apply_btn);
        plus_btn = (Button) findViewById(R.id.plus_btn);
        minus_btn = (Button) findViewById(R.id.minus_btn);
        scroll_view = (ScrollView) findViewById(R.id.scroll_view);

        snackBar = new SnackBar(this);
        nestedListView = new NestedListView();
        //prefs = PreferenceManager.getDefaultSharedPreferences(Filter.this);
        prefs = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        editor = prefs.edit();

    }



    /**
     * Function to get Venue Courts Specifications from server
     */
    public void getVenueCourtsSpec() {
        //Handling the loader state
        LoaderUtility.handleLoader(Filter.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(Filter.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String venue_spinner_id = "", venue_spinner_name = "";
                String court_name = null;


                haspMap = new HashMap<String, ArrayList<String>>();
                ArrayList<String> arrayList;
                all_specs_array = new ArrayList<String>();
                try {
                    JSONArray courts_spec_array = new JSONArray(responseData);

                    for (int i = 0; i < courts_spec_array.length(); i++) {
                        JSONObject jsonObject = courts_spec_array.getJSONObject(i);
                        arrayList = new ArrayList<String>();

                        court_name = jsonObject.getString("court_name");

                        JSONArray sourt_json_array = jsonObject.getJSONArray("specs");
                        for (int j = 0; j < sourt_json_array.length(); j++)
                        {
                            String spec_Name = sourt_json_array.getString(j);
                            arrayList.add(spec_Name);
                            all_specs_array.add(spec_Name);
                        }

                        if(court_name.contains(" "))
                        {
                            String[] str_arr_court_name = court_name.split(" ");
                            court_name = str_arr_court_name[0];
                        }
                        court_name = court_name.replaceAll("\\s+","");

                        haspMap.put(court_name,arrayList);

                    }

                } catch (Exception ex) {
                    isStausSuccess = false;
                    //snackBar.setSnackBarMessage("Network error ! please try after sometime");
                    ex.printStackTrace();
                }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //Handling the loader state
                            LoaderUtility.handleLoader(Filter.this, false);

                            if(isStausSuccess)
                            {
                            /**
                             * FUNCTIONALITY TO SEND VENUES SPECIFICATION NAME
                             * TO THE ADAPTER CLASS
                             */
                            sendVenuesNameToAdapter();
                            }
                            else{
                                snackBar.setSnackBarMessage("Network error ! please try after sometime");
                            }

                        }
                    });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_court_specs").newBuilder();
        //urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, user_name);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


   /* *//**
     * Function to Filter the court
     *//*
    public void courtFilter() {
        //Handling the loader state
        handleLoader(true);

        //Getting the OkHttpClient
        OkHttpClient client = new OkHttpClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForFilter();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                handleLoader(false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();

                try{

                } catch (Exception ex) {
                    isStausSuccess = false;
                    //snackBar.setSnackBarMessage("Network error ! please try after sometime");
                    ex.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        handleLoader(false);

                        if(isStausSuccess)
                        {

                        }
                        else{
                            snackBar.setSnackBarMessage("Network error ! please try after sometime");
                        }

                    }
                });

            }
        });
    }


    *//**
     * Function to build the Login APi request parameters
     * @return APi request
     *//*
    public Request buildApiRequestBodyForFilter(String sport_name) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_venue_courts").newBuilder();
        urlBuilder.addQueryParameter("filter", Boolean.toString(true));
        urlBuilder.addQueryParameter("sport_name", user_name);
        urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, user_name);

//        parameters-:
//        filter = true
//        sport_name
//                current_city
//        specs (comma separated string ex. Hardwood,Full Court)


        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }*/

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(Filter.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Scroll layout to up
     * @param
     */
    public void scrollToUp()
    {
        scroll_view.post(new Runnable() {
            @Override
            public void run() {
                scroll_view.fullScroll(View.FOCUS_UP);
            }
        });
    }
    /**
     * Scroll layout to buttom
     * @param
     */
    public void scrollToButtom()
    {
        scroll_view.post(new Runnable() {
            @Override
            public void run() {
                scroll_view.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        //KEEPING ALL VALUES TO NULL
        //Constants.setAllValuesToNull();
        Constants.VENUE_FILTER_BTN_CLICK = false;
        finish();
//        Intent intent_Filter = new Intent(Filter.this, VenueListing.class);
//        startActivity(intent_Filter);
    }
}
