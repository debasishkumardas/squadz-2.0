package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.FriendsModel;
import com.andolasoft.squadz.models.TeamsModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.NestedListView;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.AddTeammatesAdapter;
import com.andolasoft.squadz.views.adapters.Add_Team_TeammatesAdapter;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.adapters.UpcomingPastAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class AddTeammates extends AppCompatActivity {

    @InjectView(R.id.back_add_teammates_layout)
    RelativeLayout back_add_teammates_layout;
    @InjectView(R.id.team_recycler_view)
    RecyclerView team_recycler_view;
    @InjectView(R.id.teammates_recycler_view)
    RecyclerView teammates_recycler_view;
    @InjectView(R.id.add_invite_apply_btn)
    Button add_invite_apply_btn;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    SnackBar snackBar;
    ProgressDialog dialog;
    String userId = "",rating_value = "";
    DatabaseAdapter db;
    ArrayList<TeamsModel> teamsArray;
    public static AddTeammatesAdapter addTeammatesAdapter;
    public static Add_Team_TeammatesAdapter teamTeammatesAdapter;
    ArrayList<FriendsModel> friendArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_add_teammates_layout);

        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        /**
         * Adding click events on the views
         */
        addClickEvents();

        /**
         * Fetching all teams from local database
         */
        getAllTeams();

        /**
         * Function to pull all the friends from Local DB
         */
        getAllFriendFromDB();

    }

    /**
     * Adding click events on the views
     */
    public void addClickEvents() {
        back_add_teammates_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.clear();
                Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
                Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
                finish();
            }
        });

        /**Apply button*/
        add_invite_apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONArray teams_ids_jsonArray = new JSONArray();
                JSONArray teams_images_jsonArray = new JSONArray();
                JSONArray teams_names_jsonArray = new JSONArray();

                ArrayList<String> player_ids_array = Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES;

                if(player_ids_array.size() > 0) {
                    HashSet<String> hashSet = new HashSet<String>();
                    hashSet.addAll(player_ids_array);
                    player_ids_array.clear();
                    player_ids_array.addAll(hashSet);

                    // Toast.makeText(AddTeammates.this, ""+player_ids_array, Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < player_ids_array.size(); i++) {
                        String[] username_name_image_split = db.getUserNameImage(player_ids_array.get(i)).split("@@@@");

                        if (username_name_image_split.length == 0) {
                            username_name_image_split = db.getUserNameImageFromFriend(player_ids_array.get(i)).split("@@@@");
                            if (username_name_image_split.length == 1) {
                                teams_names_jsonArray.put(username_name_image_split[0]);
                            } else {
                                teams_names_jsonArray.put(username_name_image_split[0]);
                                teams_images_jsonArray.put(username_name_image_split[1]);
                            }
                        } else {
                            if (username_name_image_split.length == 1) {
                                teams_names_jsonArray.put(username_name_image_split[0]);
                            } else {
                                teams_names_jsonArray.put(username_name_image_split[0]);
                                teams_images_jsonArray.put(username_name_image_split[1]);
                            }
                        }
                        teams_ids_jsonArray.put(player_ids_array.get(i));

                   /*if(username_name_image_split.length == 1) {
                        teams_names_jsonArray.put(username_name_image_split[0]);
                    }
                    else{
                        teams_names_jsonArray.put(username_name_image_split[0]);
                        teams_images_jsonArray.put(username_name_image_split[1]);
                    }
                    teams_ids_jsonArray.put(player_ids_array.get(i));*/
                    }
                    if (UpcomingPastAdapter.event_id != null) {
                        addTeammatesAPI(teams_ids_jsonArray, teams_images_jsonArray,
                                teams_names_jsonArray, UpcomingPastAdapter.event_id, userId);
                    } else {
                        if (!TextUtils.isEmpty(AppConstants.GAME_ID)) {
                            addTeammatesAPI(teams_ids_jsonArray, teams_images_jsonArray,
                                    teams_names_jsonArray, AppConstants.GAME_ID, userId);
                        } else {
                            addTeammatesAPI(teams_ids_jsonArray, teams_images_jsonArray,
                                    teams_names_jsonArray, EventAdapter.event_Id, userId);
                        }
                    }
                }
                else{
                    snackBar.setSnackBarMessage("Please choose any player to invite");
                }
            }
        });
    }

    /**
     * API integration for Add Review
     */
    public void saveReview(String userId, String list_id, String rating, String review) {
        //Handling the loader state
        LoaderUtility.handleLoader(AddTeammates.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForAddReview(userId, list_id, rating, review);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(AddTeammates.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {

                    JSONObject jsonObject = new JSONObject(responseData);
                    if(jsonObject.has("status") && !jsonObject.isNull("status"))
                    {
                        status = jsonObject.getString("status");
                    }
                    if(jsonObject.has("message") && !jsonObject.isNull("message"))
                    {
                        message = jsonObject.getString("message");
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalStatus = status;
                final String finalMessage = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(AddTeammates.this, false);
                        if(finalStatus.equalsIgnoreCase("success"))
                        {
                            Constants.REVIEW_ADDED = true;
                            Toast.makeText(AddTeammates.this, ""+ finalMessage, Toast.LENGTH_SHORT).show();
                            Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.clear();
                            Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
                            Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
                            finish();
                        }
                        else{
                            Toast.makeText(AddTeammates.this, ""+ finalMessage, Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForAddReview(String userId, String list_id, String rating, String review) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "save_review").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("list_id", list_id);
        urlBuilder.addQueryParameter("rating", rating);
        urlBuilder.addQueryParameter("review", review);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(AddTeammates.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {

        ButterKnife.inject(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        db = new DatabaseAdapter(this);
        snackBar = new SnackBar(this);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(AddTeammates.this);
        editor = preferences.edit();
        userId = preferences.getString("loginuser_id", null);
    }


    /**
     * Fetching all teams from local database
     */
    public void getAllTeams() {
        teamsArray = new ArrayList<TeamsModel>();
        Cursor cursor = db.getTeamsForAll();
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String team_id = cursor.getString(cursor
                            .getColumnIndex("team_id"));
                    String team_name = cursor.getString(cursor
                            .getColumnIndex("team_name"));
                    String team_sport = cursor.getString(cursor
                            .getColumnIndex("team_sport_id"));
                    String team_custom_sport = cursor.getString(cursor
                            .getColumnIndex("team_custom_sport_name"));
                    String team_creator_id = cursor.getString(cursor
                            .getColumnIndex("team_creator_user_id"));
                    String team_creator_name = cursor.getString(cursor
                            .getColumnIndex("team_creator_user_name"));
                    String team_creator_image = cursor.getString(cursor
                            .getColumnIndex("team_creator_profile_image"));
                    int team_player_count = db.getTeamPlayerCount(team_id);
                    TeamsModel teamModel = new TeamsModel(team_id, team_name, team_sport,
                            team_custom_sport, team_creator_id, team_creator_name,
                            team_creator_image, String.valueOf(team_player_count));
                    teamsArray.add(teamModel);
                } while (cursor.moveToNext());
            }
        }
        if(teamsArray.size() > 0) {
            addTeammatesAdapter = new AddTeammatesAdapter(AddTeammates.this, teamsArray);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            team_recycler_view.setLayoutManager(mLayoutManager);
            team_recycler_view.setItemAnimator(new DefaultItemAnimator());
            team_recycler_view.setAdapter(addTeammatesAdapter);
            addTeammatesAdapter.notifyDataSetChanged();
        }
        else{
            snackBar.setSnackBarMessage("No teams found");
        }
    }

    /**
     * Function to pull all the friends from Local DB
     */

    public void getAllFriendFromDB() {
        friendArray = new ArrayList<>();
        Cursor cursor = db.getAllFriendsDeomDb();
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String user_id = cursor.getString(cursor
                            .getColumnIndex("user_id"));
                    String username = cursor.getString(cursor
                            .getColumnIndex("username"));
                    String firstname = cursor.getString(cursor
                            .getColumnIndex("firstname"));
                    String lastname = cursor.getString(cursor
                            .getColumnIndex("lastname"));
                    String image = cursor.getString(cursor
                            .getColumnIndex("image"));
                    String sport = cursor.getString(cursor
                            .getColumnIndex("sport"));
                    String phonenumber = cursor.getString(cursor
                            .getColumnIndex("phonenumber"));
                    String status = cursor.getString(cursor
                            .getColumnIndex("status"));
                    String reward = cursor.getString(cursor
                            .getColumnIndex("reward"));
                    FriendsModel friendsModel = new FriendsModel(user_id, username, firstname,
                            lastname, image, sport, phonenumber, status, reward);
                    friendArray.add(friendsModel);
                } while (cursor.moveToNext());
            }
        }

        if(friendArray.size() > 0) {
            teamTeammatesAdapter = new Add_Team_TeammatesAdapter(AddTeammates.this, friendArray);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            teammates_recycler_view.setLayoutManager(mLayoutManager);
            teammates_recycler_view.setItemAnimator(new DefaultItemAnimator());
            teammates_recycler_view.setAdapter(teamTeammatesAdapter);
            teamTeammatesAdapter.notifyDataSetChanged();
        }
        else{
            snackBar.setSnackBarMessage("No teammates found");
        }

    }


    /**
     * API integration for Adding player to the Newly added team
     */
    public void addTeammatesAPI(JSONArray teammates_ids_array, JSONArray teammates_images_array,
                                JSONArray teammates_names_array, String game_id, String invited_by) {
        //Handling the loader state
        LoaderUtility.handleLoader(AddTeammates.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForAddPlayers(teammates_ids_array,teammates_images_array,
                teammates_names_array,game_id,invited_by);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(AddTeammates.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject teammates_jsonObject = new JSONObject(responseData);

                    if(teammates_jsonObject.has("status") && !teammates_jsonObject.isNull("status"))
                    {
                        status = teammates_jsonObject.getString("status");
                    }
                    if(teammates_jsonObject.has("message") && !teammates_jsonObject.isNull("message"))
                    {
                        message = teammates_jsonObject.getString("message");
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalStatus = status;
                final String finalMessage = message;
                final String finalStatus1 = status;
                final String finalMessage1 = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(AddTeammates.this, false);
                        if(finalStatus1.equalsIgnoreCase("success"))
                        {
                            Constants.ADD_TEAMMATES_IDS_ARRAY = new ArrayList<>();
                            Constants.ADD_TEAMMATES_IMAGES_ARRAY = new ArrayList<>();
                            Constants.ADD_TEAMMATES_NAMES_ARRAY = new ArrayList<>();
                            Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();

                            //snackBar.setSnackBarMessage(finalMessage1);
                            Toast.makeText(AddTeammates.this, ""+finalMessage1, Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else{
                            snackBar.setSnackBarMessage(finalMessage1);
                        }

                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForAddPlayers(JSONArray teammates_ids_array, JSONArray teammates_images_array,
                                                    JSONArray teammates_names_array, String game_id, String invited_by) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "invite_user").newBuilder();
        urlBuilder.addQueryParameter("user_id", teammates_ids_array.toString());
        urlBuilder.addQueryParameter("user_profile_image", teammates_images_array.toString());
        urlBuilder.addQueryParameter("user_name", teammates_names_array.toString());
        urlBuilder.addQueryParameter("game_id", game_id);
        urlBuilder.addQueryParameter("invited_by", invited_by);
        urlBuilder.addQueryParameter("multiple", "true");
        urlBuilder.addQueryParameter("invited_at", ApplicationUtility.getCurrentDateTime());

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.clear();
        Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
        Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
        finish();
    }
}

