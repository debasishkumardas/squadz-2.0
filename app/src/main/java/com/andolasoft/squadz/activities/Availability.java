package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.calenderHelper.MonthView;
import com.andolasoft.squadz.calenderHelper.MonthViewClickListeners;
import com.andolasoft.squadz.models.AvailabilityTimeSlotsModel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.ListUtils;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.NestedListView;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.adapters.AvailabilityAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Availability extends AppCompatActivity implements MonthViewClickListeners {

    public static Availability availability;
    public static TextView mTvMonthName;
    MonthView monthView;
    SnackBar snackBar;
    ProgressDialog dialog;
    ArrayList<String> datesArray;
    ArrayList<AvailabilityTimeSlotsModel> availabilityTimeSlots_Array;
    TextView no_time_slots_text;
    private ListView listView_availability;
    private AvailabilityAdapter availabilityAdapter;
    private Button next_btn_availability;
    private RelativeLayout back_icon_avail_layout;
    private TextView availability_time;
    private long selected_date_milisecond;
    public static boolean isDateLesser = false;
    boolean isMonthChanged;
    String currentDate = "";
    AvailabilityTimeSlotsModel availabilityTimeSlotsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_availability);

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(Availability.this);

        /**
         * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
         */
        initReferences();

        /**
         * Added click event
         */
        setClickEvents();

        /**
         * Adding initial call back for displaying calender
         */
        registerCalenderCallBacks();


        MonthView.mIvLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isMonthBeforeCurrentMonth(MonthView.selectedMonth)) {

                    //Refreshing the list view while monthe is changed
                    availabilityTimeSlots_Array = new ArrayList<AvailabilityTimeSlotsModel>();
                    sendAvalabilityTimeToAdapter(availabilityTimeSlots_Array);
                    Constants.AVAILABILITY_TIME_SLOT = "";
                    isMonthChanged = true;

                    if (MonthView.tv != null) {
                        MonthView.tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    }
                    MonthView.previousSelectedPosition = -1;
                    if (MonthView.selectedMonth == 0) {
                        MonthView.selectedMonth = 11;
                        MonthView.selectedYear = MonthView.selectedYear - 1;
                    } else {
                        MonthView.selectedMonth = MonthView.selectedMonth - 1;
                    }
                    //MonthView.mMonthGridAdapter.updateCalendar(MonthView.selectedYear, MonthView.selectedMonth);
                    MonthView.mTvMonthName.setText(MonthView.getMonthName(MonthView.selectedMonth) + " " + MonthView.selectedYear);
                    String dates = String.valueOf(MonthView.selectedMonth) + String.valueOf(MonthView.selectedYear);

                    String month = String.valueOf(MonthView.selectedMonth);
                    String year = String.valueOf(MonthView.selectedYear);

                    int month_value = (Integer.valueOf(month) + 1);
                    getAvailabilityDates(VenueAdapter.venueId, year, String.valueOf(month_value));

                    if(isMonthBeforeCurrentMonth(MonthView.selectedMonth)) {
                        availability_time.setText(currentDate);
                    }
                } else{
                    availability_time.setText(currentDate);
                }
            }
        });

        MonthView.mIvRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Refreshing the list view while monthe is changed
                availabilityTimeSlots_Array = new ArrayList<AvailabilityTimeSlotsModel>();
                sendAvalabilityTimeToAdapter(availabilityTimeSlots_Array);
                Constants.AVAILABILITY_TIME_SLOT = "";
                isMonthChanged = true;
                availability_time.setText("Select Date");

                if (MonthView.tv != null) {
                    MonthView.tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
                MonthView.previousSelectedPosition = -1;
                if (MonthView.selectedMonth == 11) {
                    MonthView.selectedMonth = 0;
                    MonthView.selectedYear = MonthView.selectedYear + 1;
                } else {
                    MonthView.selectedMonth = MonthView.selectedMonth + 1;
                }
                //MonthView.mMonthGridAdapter.updateCalendar(MonthView.selectedYear, MonthView.selectedMonth);
                MonthView.mTvMonthName.setText(MonthView.getMonthName(MonthView.selectedMonth) + " " + MonthView.selectedYear);

                String month = String.valueOf(MonthView.selectedMonth);
                String year = String.valueOf(MonthView.selectedYear);

                int month_value = (Integer.valueOf(month) + 1);
                getAvailabilityDates(VenueAdapter.venueId, year, String.valueOf(month_value));
            }
        });

        //currentTime();

    }


    /**
     * Added click event
     */
    public void setClickEvents() {
        back_icon_avail_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthView.previousSelectedPosition = -1;
                finish();
                Constants.AVAILABILITY_TIME_SLOT = "";
                Constants.AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<>();
            }
        });

        next_btn_availability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.AVAILABILITY_TIME_SLOT_ARRAY.size() > 0) {
                    Intent intent = new Intent(Availability.this, Additionalinfo.class);
                    startActivity(intent);
                }
                /*if(!TextUtils.isEmpty(Constants.AVAILABILITY_TIME_SLOT)) {
                    Intent intent = new Intent(Availability.this, Additionalinfo.class);
                    startActivity(intent);
                }*/
                else if(availabilityTimeSlots_Array == null || availabilityTimeSlots_Array.size() == 0) {
                    snackBar.setSnackBarMessage("Please select a date to see the time slot");
                } else if(isMonthBeforeCurrentMonth(MonthView.selectedMonth) && availabilityTimeSlots_Array != null) {
                    snackBar.setSnackBarMessage("Please choose any time slot");
                } else if(isMonthChanged) {
                    snackBar.setSnackBarMessage("Please select a date to see the time slot");
                } else {
                    snackBar.setSnackBarMessage("Please choose any time slot");
                }
            }
        });
    }

    /**
     * Send time slots to the Base adapter class with Start time & End time
     */
    public void sendAvalabilityTimeToAdapter(ArrayList<AvailabilityTimeSlotsModel> timeSlotsArray) {

        availabilityAdapter = new AvailabilityAdapter(Availability.this, timeSlotsArray);
        listView_availability.setAdapter(availabilityAdapter);
        availabilityAdapter.notifyDataSetChanged();
        ListUtils.setDynamicHeight_teammate(listView_availability);
        Constants.AVAILABILITY_TIME_SLOT = "";

        //SCROLLING WHOLE LAYOUT WITH LISTVIEW WITH THIS METHOD
        NestedListView.setListViewHeightBasedOnChildren(listView_availability);
    }

    /**
     * Adding initial call back for displaying calender
     */
    public void registerCalenderCallBacks() {
        //Displaying current date
        currentDate = ApplicationUtility.currentDate();
        availability_time.setText(currentDate);
        if(currentDate.contains(",")) {
            Constants.AVAILABILITY_SELECTED_DATE = ApplicationUtility.formattedCalenderDate(currentDate);
        }
        else{
            Constants.AVAILABILITY_SELECTED_DATE = currentDate;
        }

        String[] split_date = currentDate.split(" ");
        /*String date = split_date[0].trim();
        String month = split_date[1].trim();
        String year = split_date[2].trim();*/

        String month = split_date[0].trim();
        String date = split_date[1].trim().replace(",","");
        String year = split_date[2].trim();

        //registering the click listeners
        monthView.registerClickListener(this);

        /**
         * Getting Availability dates
         */
        getAvailabilityDates(VenueAdapter.venueId, year, monthIndex(month));

    }

    private List<Date> getEventList(ArrayList<String> dates_array) {
        //generating available list
        List<Date> eventList = new ArrayList<>();

        for (int i = 0; i < dates_array.size(); i++) {
            String[] split_dates = dates_array.get(i).split("-");
            int year = Integer.valueOf(split_dates[0]);
            int month = Integer.valueOf(split_dates[1]);
            int day = Integer.valueOf(split_dates[2]);
            eventList.add(getDate(year, month, day));
        }
        return eventList;
    }

    private Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
     */
    public void initReferences() {
        listView_availability = (ListView) findViewById(R.id.listView_availability);
        next_btn_availability = (Button) findViewById(R.id.next_btn_availability);
        back_icon_avail_layout = (RelativeLayout) findViewById(R.id.back_icon_avail_layout);
        availability_time = (TextView) findViewById(R.id.availability_time);
        monthView = (MonthView) findViewById(R.id.calendar_month_view);
        no_time_slots_text = (TextView) findViewById(R.id.not_time_slots_text);

        availability = this;
        snackBar = new SnackBar(this);
    }

    @Override
    public void dateClicked(Date dateClicked) {
        String selectedDate = dateClicked.toString();
        String date = ApplicationUtility.formattedGMTDate(selectedDate);
        String date_for_Display = ApplicationUtility.formattedGMTDateForAvailabilityDisplay(selectedDate);//For display

        //Getting current date from Calender & Converting into in milisecond
        long current_date_milisecond = currentDateInMilisecond();

        if (selectedDateInMili(date) < current_date_milisecond) {
            isDateLesser = true;
            Toast.makeText(this, "Please select current or future dates to create a game!", Toast.LENGTH_LONG).show();
        } else{
            if (datesArray.contains(ApplicationUtility.customDateFormat(date))) {
                //availability_time.setText(date);
                availability_time.setText(date_for_Display);
                Constants.AVAILABILITY_SELECTED_DATE = date;
                isMonthChanged = false;

                //Time slots API Integration
                getAvailabilityTimeSlots(VenueAdapter.venueId, ApplicationUtility.customDateFormat(date));
            } else {
                Toast.makeText(Availability.this, "All the slots are booked for the selected date", Toast.LENGTH_SHORT).show();
            }
        }
    }


    /**
     * Getting Availability dates
     */
    public void getAvailabilityDates(String list_id, String year, String month) {
        //Handling the loader state
        LoaderUtility.handleLoader(Availability.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(list_id, year, month);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(Availability.this, false);
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String startTime = "", endTime = "", blocked_day = "";
                boolean is_available_status = true;
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    datesArray = new ArrayList<String>();
                    availabilityTimeSlots_Array = new ArrayList<AvailabilityTimeSlotsModel>();

                    JSONObject responseObject = new JSONObject(responseData);
                    JSONArray responseArray = responseObject.getJSONArray("dates");
                    for (int i = 0; i < responseArray.length(); i++) {
                        datesArray.add(responseArray.getString(i));
                    }

                    if(responseObject.has("all_slots") && !responseObject.isNull("all_slots")) {

                        JSONArray slots_json_Array = responseObject.getJSONArray("all_slots");

                        //JSONArray slots_json_Arrayy = new JSONArray(slots_json_Array);

                        for (int i = 0; i < slots_json_Array.length(); i++) {

                            JSONArray jsonArray = slots_json_Array.getJSONArray(i);

                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(j);
                                availabilityTimeSlotsModel = new AvailabilityTimeSlotsModel();

                                if (jsonObject.has("start_time") && !jsonObject.isNull("start_time")) {
                                    startTime = jsonObject.getString("start_time");
                                }
                                if (jsonObject.has("end_time") && !jsonObject.isNull("end_time")) {
                                    endTime = jsonObject.getString("end_time");
                                }
//                                if (jsonObject.has("is_available") && !jsonObject.isNull("is_available")) {
//                                    is_available_status= (Boolean) jsonObject.get("is_available");
//                                }

                                availabilityTimeSlotsModel.setStart_end_time(startTime + " - " + endTime);
                                //availabilityTimeSlotsModel.setTime_position(j);
                                //availabilityTimeSlotsModel.setIs_available(is_available_status);

                                availabilityTimeSlots_Array.add(availabilityTimeSlotsModel);
                            }

                        }
                    }

                    if(responseObject.has("blocked_day") && !responseObject.isNull("blocked_day"))
                    {
                        blocked_day = responseObject.getString("blocked_day");
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalBlocked_day = blocked_day;
                Availability.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(Availability.this, false);

                        //creating list of events
                        List<Date> eventList = getEventList(datesArray);

                        //adding events to the calendar
                        monthView.setEventList(eventList);

                        MonthView.mMonthGridAdapter.updateCalendar(MonthView.selectedYear, MonthView.selectedMonth);

                        if (availabilityTimeSlots_Array.size() > 0) {
                            //Send time slots to the Base adapter class with Start time & End time
                            sendAvalabilityTimeToAdapter(availabilityTimeSlots_Array);
                            no_time_slots_text.setVisibility(View.GONE);
                            next_btn_availability.setVisibility(View.VISIBLE);
                        }
                        else if(!TextUtils.isEmpty(finalBlocked_day))
                        {
                            sendAvalabilityTimeToAdapter(availabilityTimeSlots_Array);
                            no_time_slots_text.setVisibility(View.VISIBLE);
                            no_time_slots_text.setText(finalBlocked_day);
                        }
                        else if(availabilityTimeSlots_Array.size() == 0)
                        {
                            no_time_slots_text.setVisibility(View.VISIBLE);
                            //no_time_slots_text.setText("Please select a date to see the time slot");
                            if(!isMonthBeforeCurrentMonth(MonthView.selectedMonth))
                            {
                                availability_time.setText("Select Date");
                                no_time_slots_text.setText("Please select a date to see the time slot");
                            }
                            else{
                                no_time_slots_text.setText("No time slots available for this date");
                            }
                        }
                        else {
                            sendAvalabilityTimeToAdapter(availabilityTimeSlots_Array);
                            no_time_slots_text.setVisibility(View.VISIBLE);
                            next_btn_availability.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
    }


    /**
     *  */
    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     *//*
     * @return
     */
    public Request buildApiRequestBody(String list_id, String year, String month) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_availability_calendar").newBuilder();
        urlBuilder.addQueryParameter("list_id", list_id);
        urlBuilder.addQueryParameter("year", year);
        urlBuilder.addQueryParameter("month", month);
        urlBuilder.addQueryParameter("current_time", currentTime()); //Current time

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }


    /**
     * Getting Availability Time slots
     */
    public void getAvailabilityTimeSlots(String list_id, String date) {
        //Handling the loader state
        LoaderUtility.handleLoader(Availability.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForTimeSlots(list_id, date);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(Availability.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String startTime = "", endTime = "",blocked_day = "";
                boolean is_available_status = true;

                String responseData = response.body().string();
                try {
                    availabilityTimeSlots_Array = new ArrayList<>();

                    JSONObject responseObject = null;
                    try {
                        responseObject = new JSONObject(responseData);

                        if(responseObject.has("all_slots") && !responseObject.isNull("all_slots")) {

                            JSONArray slots_json_Array = responseObject.getJSONArray("all_slots");

                            for (int i = 0; i < slots_json_Array.length(); i++) {

                                JSONArray jsonArray = slots_json_Array.getJSONArray(i);

                                for (int j = 0; j < jsonArray.length(); j++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(j);
                                    availabilityTimeSlotsModel = new AvailabilityTimeSlotsModel();

                                    if (jsonObject.has("start_time") && !jsonObject.isNull("start_time")) {
                                        startTime = jsonObject.getString("start_time");
                                    }
                                    if (jsonObject.has("end_time") && !jsonObject.isNull("end_time")) {
                                        endTime = jsonObject.getString("end_time");
                                    }
                                    availabilityTimeSlotsModel.setStart_end_time(startTime + " - " + endTime);
                                    availabilityTimeSlots_Array.add(availabilityTimeSlotsModel);
                                }
                            }
                        }
                        if(responseObject.has("blocked_day") && !responseObject.isNull("blocked_day")) {
                            blocked_day = responseObject.getString("blocked_day");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalBlocked_day = blocked_day;
                Availability.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(Availability.this, false);

                        if (availabilityTimeSlots_Array.size() > 0) {
                            //Send time slots to the Base adapter class with Start time & End time
                            sendAvalabilityTimeToAdapter(availabilityTimeSlots_Array);
                            no_time_slots_text.setVisibility(View.GONE);
                            next_btn_availability.setVisibility(View.VISIBLE);
                        }
                        else if(!TextUtils.isEmpty(finalBlocked_day))
                        {
                            sendAvalabilityTimeToAdapter(availabilityTimeSlots_Array);
                            no_time_slots_text.setVisibility(View.VISIBLE);
                            no_time_slots_text.setText(finalBlocked_day);
                        }
                        else {
                            sendAvalabilityTimeToAdapter(availabilityTimeSlots_Array);
                            no_time_slots_text.setVisibility(View.VISIBLE);
                            next_btn_availability.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
    }


    /**
     *  */
    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     *//*
     * @return
     */
    public Request buildApiRequestBodyForTimeSlots(String list_id, String date) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "list_availability").newBuilder();
        urlBuilder.addQueryParameter("list_id", list_id);
        urlBuilder.addQueryParameter("date", date);
        urlBuilder.addQueryParameter("current_time", currentTime()); //Current time

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(Availability.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Getting Month as index
     *
     * @param month
     * @return
     */
    public String monthIndex(String month) {
        String month_number = "";
        SimpleDateFormat inputFormatter = new SimpleDateFormat("MMMM");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MM");

        try {
            Date date1 = inputFormatter.parse(month);
            month_number = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return month_number;
    }

    /**
     * Checking
     *
     * @param calenderMonth
     * @return
     */
    public boolean isMonthBeforeCurrentMonth(int calenderMonth) {
        boolean isStatus = true;

        try {

            DateFormat dateFormat = new SimpleDateFormat("MM");
            Date date = new Date();

            int currentMonth = Integer.valueOf(dateFormat.format(date));

            if (calenderMonth >= currentMonth) {
                isStatus = false;
            }

        } catch (Exception exp) {
            exp.printStackTrace();
        }

        return isStatus;
    }

    /**
     * Getting current date from Calender & Converting into in milisecond
     */
    public long currentDateInMilisecond() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        String formattedDate = df.format(c.getTime());

        return convertToMilliseconds(formattedDate);
    }

    /**
     * Return date into milisecond
     *
     * @param date
     * @return
     */
    public static long convertToMilliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();

            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Convert milisecond for date selection
     * @param inputDate
     * @return
     */
    public long selectedDateInMili(String inputDate) {

        long selected_date_milisecond = 0;

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd MMMM yyyy");
        //SimpleDateFormat inputFormatter = new SimpleDateFormat("MMMM dd yyyy");
        try {

            Date date1 = inputFormatter.parse(inputDate);
            selected_date_milisecond = date1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return selected_date_milisecond;
    }


    /**
     * Current Time
     * @return
     */
    public String currentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mmaa");
        String currentTime = df.format(c.getTime()).toLowerCase();

        return currentTime;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        MonthView.previousSelectedPosition = -1;
        super.onBackPressed();
        Constants.AVAILABILITY_TIME_SLOT = "";
        Constants.AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<>();
        // MonthView.previousSelectedPosition = -1;
        finish();
    }
}
