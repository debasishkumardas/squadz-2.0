package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.LocationPickerManager;
import com.andolasoft.squadz.models.LocationVenueModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.ListUtils;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.PlaceJSONParser;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.adapters.LocationVenueAdapter;
import com.andolasoft.squadz.views.adapters.UpcomingPastAdapter;
import com.andolasoft.squadz.views.adapters.WishListAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.andolasoft.squadz.utils.Constants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SpNayak on 2/17/2017.
 */

public class CreateEventWithoutCourt extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener {

    @InjectView(R.id.select_date_btn_without_court)
    TextView select_date_btn_without_court;
    @InjectView(R.id.select_date_layout)
    RelativeLayout select_date_layout;
    @InjectView(R.id.from_time_without_court_layout)
    RelativeLayout from_time_without_court_layout;
    @InjectView(R.id.to_time_without_court_layout)
    RelativeLayout to_time_without_court_layout;
    @InjectView(R.id.from_time_without_court)
    TextView from_time_without_court;
    @InjectView(R.id.to_time_without_court)
    TextView to_time_without_court;
    @InjectView(R.id.switch_button_add_info)
    SwitchCompat sc_public_private;
    @InjectView(R.id.switch_button_indoor_outdoor_info)
    SwitchCompat sc_indoor_outdoor;
    private String str_IsTimeClicked = "";
    public static Additionalinfo additionalinfo;
    private SwitchCompat switch_button_add_info, switch_button_venue_type;
    public TextView private_add_info, public_add_info,
            skill_level_text, players_count_add_info;
    private RelativeLayout skill_level_layout;
    private RelativeLayout ic_back_layout;
    private Button minus_btn_add_info;
    private Button plus_btn_add_info;
    private Button  confirm_btn_add_info;
    private EditText edit_game_name_add_info,edit_little_notes_add_info, listingName;
    SnackBar snackBar;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    public static double game_latitude, game_longitude;
    public static String currentCity = null, address = null;
    TextView et_pick_location;
    //AutoCompleteTextView et_pick_location;
    @InjectView(R.id.private_add_info)
    TextView tv_private_game;
    @InjectView(R.id.public_add_info)
    TextView tv_public_game;
    @InjectView(R.id.tv_joingame)
    TextView tv_joingame;
    @InjectView(R.id.indoor_info)
    TextView tv_indoor_game;
    @InjectView(R.id.outdoor_info)
    TextView tv_outdoor_game;
    int players;
    public static String game_date, game_starttime = "", game_endtime = "",
            game_listing_name, game_name, max_player, game_location, game_notes, game_address,
            game_visibility, game_type;
    public static CreateEventWithoutCourt createEventWithoutCourt = null;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String userName = "", image = "", billing_card_count = "", user_Id = "", auth_token = "";
    ProgressDialog dialog;
    private String status = "";
    ParserTask parserTask;
    PlacesTask placesTask;
    ArrayList<LocationVenueModel> locationVenueModelArrayList;
    LocationVenueModel locationVenueModel;
    LocationVenueAdapter locationVenueAdapter;
    boolean is_Unregistred_Venues_Available = false;
    public GPSTracker gpsTracker;
    ScrollView scroll_bars_create_game;
    ListView listView_registered,listView2_unregisred;
    long from_Time_Mili,to_Time_Mili;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_create_event_without_court);
        ButterKnife.inject(this);

        initReferences();

        //Initializing the context
        createEventWithoutCourt = this;

        et_pick_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //openAutocompleteActivity();
                LocationPickerManager locationManager
                        = new LocationPickerManager(createEventWithoutCourt);
            }
        });

        //Hiding the Soft Key Board
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //Adding Click events on the view
        addClickEvents();

        //Getting Current Date at page loading
        currentDate();

        //Seetting the default skill level as All
        setDefaultData();
    }

    /**
     * Adding Click events on the view
     */
    public void addClickEvents() {
        select_date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        CreateEventWithoutCourt.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.setAccentColor(getResources().getColor(R.color.deep_orange));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        from_time_without_court_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //KEEPING CLICKED NAME IN A STRING FOR DISPLAYING THE FROM AND TO TIME
                str_IsTimeClicked = "FROM_TIME";
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        CreateEventWithoutCourt.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.setAccentColor(getResources().getColor(R.color.deep_orange));
                tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        to_time_without_court_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //KEEPING CLICKED NAME IN A STRING FOR DISPLAYING THE FROM AND TO TIME
                str_IsTimeClicked = "TO_TIME";

                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        CreateEventWithoutCourt.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false

                );
                tpd.setAccentColor(getResources().getColor(R.color.deep_orange));
                tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        sc_public_private.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                /**Hide softkeyboard*/
                ApplicationUtility.hideSoftKeyBoard(CreateEventWithoutCourt.this);

                if(isChecked){
                    tv_public_game.setTextColor(Color.parseColor("#FF7300"));
                    tv_private_game.setTextColor(Color.parseColor("#626262"));
                    tv_joingame.setText("Anyone can join");
                    Constants.GAME_VISIBILITY = "Public";
                }else{
                    tv_public_game.setTextColor(Color.parseColor("#626262"));
                    tv_private_game.setTextColor(Color.parseColor("#FF7300"));
                    tv_joingame.setText("Only teammates can join");
                    Constants.GAME_VISIBILITY = "Private";
                }
            }
        });



        sc_indoor_outdoor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                /**Hide softkeyboard*/
                ApplicationUtility.hideSoftKeyBoard(CreateEventWithoutCourt.this);

                if(isChecked){
                    tv_outdoor_game.setTextColor(Color.parseColor("#FF7300"));
                    tv_indoor_game.setTextColor(Color.parseColor("#626262"));
                    Constants.GAME_TYPE = "Outdoor";
                }else{
                    tv_outdoor_game.setTextColor(Color.parseColor("#626262"));
                    tv_indoor_game.setTextColor(Color.parseColor("#FF7300"));
                    Constants.GAME_TYPE = "Indoor";
                }
            }
        });

        skill_level_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CreateEventWithoutCourt.this, SkillLevel.class));
            }
        });

        ic_back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateEventWithoutCourt.this.finish();
               // game_name = edit_game_name_add_info.getText().toString().trim();
               // game_listing_name = listingName.getText().toString().trim();
              //  game_notes = edit_little_notes_add_info.getText().toString().trim();
            }
        });


        minus_btn_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**Hide softkeyboard*/
                ApplicationUtility.hideSoftKeyBoard(CreateEventWithoutCourt.this);

                minus_btn_add_info.setText("-");
                plus_btn_add_info.setText("+");
                players = Integer.valueOf(players_count_add_info.getText().toString().trim());

                if(players == 2) {
                    snackBar.setSnackBarMessage("Court Minimum player exceed");
                    plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));
                    minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    if(players == 2) {
                        minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));
                        plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                        minus_btn_add_info.setTextColor(getResources().getColor(R.color.white));
                        plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                    }
                }
                else if(players == Constants.NO_OF_PARTICIPANT)
                {
                    minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));
                    plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    minus_btn_add_info.setTextColor(getResources().getColor(R.color.white));
                    plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                    snackBar.setSnackBarMessage("Court Minimum player exceed");
                }
                else{
                    players--;
                    players_count_add_info.setText(String.valueOf(players));
                    Constants.PLAYERS_COUNT = String.valueOf(players);
                    plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));

                }
            }
        });

        plus_btn_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**Hide softkeyboard*/
                ApplicationUtility.hideSoftKeyBoard(CreateEventWithoutCourt.this);

                plus_btn_add_info.setText("+");
                minus_btn_add_info.setText("-");
                players = Integer.valueOf(players_count_add_info.getText().toString().trim());

                players++;
                plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));

                players_count_add_info.setText(String.valueOf(players));
                minus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));

                Constants.PLAYERS_COUNT = String.valueOf(players);
            }
        });

        confirm_btn_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.GAME_NAME = edit_game_name_add_info.getText().toString().trim();
                //Constants.VENUE_NAME = /*listingName.getText().toString().trim();*/game_address;
                Constants.VENUE_NAME = et_pick_location.getText().toString();
                Constants.NOTES_INFO = edit_little_notes_add_info.getText().toString().trim();
                Constants.COURT_PRICE = "";
                //Validating user inputted data
                validateUserEventData(Constants.GAME_NAME, Constants.VENUE_NAME, Constants.NOTES_INFO);
            }
        });

        /**
         * Location picker where both Google places api & venue api are called
         */
        /*et_pick_location.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                try {
                    if (ApplicationUtility.isConnectingToInternet(CreateEventWithoutCourt.this)) {
                        int lenght = s.length();
                        if(lenght >= 2) {
                            getVenuesFromSearch(s.toString());
                        }
                    } else {
                        snackBar.setSnackBarMessage("Please check your internet connection");
                    }

                } catch (Exception exp) {
                    exp.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }
        });*/

        /*et_pick_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {
                    InputMethodManager imm = (InputMethodManager) CreateEventWithoutCourt.this
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_pick_location.getWindowToken(), 0);

                    View view2 = getCurrentFocus();
                    if (view2 != null) {
                        InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm2.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    if(locationVenueModelArrayList.size() > 0) {

                        String short_address = locationVenueModelArrayList.get(position).get_Venue_Short_address();
                        String full_address = locationVenueModelArrayList.get(position).get_Venue_Full_address();
                        String venue_Id = locationVenueModelArrayList.get(position).get_Venue_Id();
                        boolean venue_registered_status = locationVenueModelArrayList.get(position).get_Venue_Registered_status();

                        *//**Keeping the address for displaying in POI game additional info page*//*
                        if (!TextUtils.isEmpty(full_address)) {
                            Constants.SEARCH_LOCATION_ADDRESS = short_address + ", " + full_address;
                        } else {
                            Constants.SEARCH_LOCATION_ADDRESS = short_address;
                        }

                        if (!venue_registered_status) {
                            et_pick_location.setText(Constants.SEARCH_LOCATION_ADDRESS);
                            game_address = Constants.SEARCH_LOCATION_ADDRESS;
                            Constants.VENUE_NAME = short_address;

                            //String locationAddress = parent.getItemAtPosition(position).toString();

                            //Getting latitude & longitude based on the city name*//*
                            Geocoder coder = new Geocoder(CreateEventWithoutCourt.this);
                            try {
                                ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(Constants.SEARCH_LOCATION_ADDRESS, 10);
                                for(Address add : adresses){
                                    double longitude = add.getLongitude();
                                    double latitude = add.getLatitude();

                                    Constants.GAME_LOCATION = latitude+","+longitude;
                                    currentCity = LocationPickerManager.getCity(createEventWithoutCourt,
                                            latitude, longitude);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch(IllegalArgumentException e){
                                e.printStackTrace();
                            }
                        }
                        else{
                            if(!TextUtils.isEmpty(venue_Id)) {
                                et_pick_location.setText("");
                                Constants.SEARCH_VENUE_ID = venue_Id;
                                finish();
                            }
                            else{
                                snackBar.setSnackBarMessage("No venue found");
                            }
                        }
                    }

                }catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }
        });*/
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        String date =  (++monthOfYear)+ "/"+ dayOfMonth + "/" + year;
        String finalOutputDate = ApplicationUtility.customFormattedDateNew(date);
        String OutputDate_For_Display = formattedDateForDisplay(date);

        //Getting current date from Calender & Converting into in milisecond
        long current_date_milisecond = currentDateInMilisecond();

        if (ApplicationUtility.selected_date_milisecond < current_date_milisecond) {
            Toast.makeText(this, "Please select current or future dates to create a game!",
                    Toast.LENGTH_LONG).show();
        } else {
            Constants.AVAILABILITY_SELECTED_DATE = finalOutputDate;
            //select_date_btn_without_court.setText(finalOutputDate);
            select_date_btn_without_court.setText(OutputDate_For_Display);
        }
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
        String time = hourString + ":" + minuteString + ":" + secondString;
        String finalOutputTime = formattedTime(time);
        finalOutputTime = ApplicationUtility.getTimeWithFormatWithSpace(finalOutputTime);

        if (str_IsTimeClicked.equalsIgnoreCase("FROM_TIME")) {
            from_time_without_court.setText(finalOutputTime);
            game_starttime = finalOutputTime;
            from_Time_Mili = ApplicationUtility.getCurrentDateTimeInMili(ApplicationUtility.getDeviceCurrentDate() +" "+ formattedTime(time));

        } else {
            to_Time_Mili = ApplicationUtility.getCurrentDateTimeInMili(ApplicationUtility.getDeviceCurrentDate() +" "+ formattedTime(time));
            if(to_Time_Mili < from_Time_Mili)
            {
                Toast.makeText(CreateEventWithoutCourt.this, "End time cann't be less than Start time", Toast.LENGTH_SHORT).show();
            }
            else{
                to_time_without_court.setText(finalOutputTime);
                game_endtime = finalOutputTime;
            }
        }
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public String formattedDate(String inputDate) {
        String finalFormatedDate = "";
        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }

    /**
     * PARSING TIME FORMAT WITH CUSTOM TIME
     */
    public String formattedTime(String inputTime) {

        String finalFormatedTime = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("hh:mm a");

        try {
            Date date1 = inputFormatter.parse(inputTime);
            finalFormatedTime = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedTime;
    }

    /**
     * Getting current date from Calender & Converting into in milisecond
     */
    public long currentDateInMilisecond() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(c.getTime());

        return convertToMilliseconds(formattedDate);
    }

    /**
     * Return date into milisecond
     *
     * @param date
     * @return
     */
    public static long convertToMilliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();

            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }
    /**
     * Current date
     * @return
     */
    public String currentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(c.getTime());
        String finalOutputDate = ApplicationUtility.customFormattedDateNew(formattedDate);
        String OutputDate_For_Display = formattedDateForDisplay(formattedDate);
        //select_date_btn_without_court.setText(finalOutputDate);
        select_date_btn_without_court.setText(OutputDate_For_Display);
        //storing the date
        Constants.AVAILABILITY_SELECTED_DATE = finalOutputDate;
        return formattedDate;
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public String formattedDateForDisplay(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMMM dd, yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            finalFormatedDate = toConvertFormatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalFormatedDate;
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {
        switch_button_add_info = (SwitchCompat) findViewById(R.id.switch_button_add_info);
        switch_button_venue_type = (SwitchCompat) findViewById(R.id.switch_button_indoor_outdoor_info);
        private_add_info = (TextView) findViewById(R.id.private_add_info);
        public_add_info = (TextView) findViewById(R.id.public_add_info);
        skill_level_text = (TextView) findViewById(R.id.skill_level_text);
        players_count_add_info = (TextView) findViewById(R.id.players_count_add_info);
        skill_level_layout = (RelativeLayout) findViewById(R.id.skill_level_layout);
        ic_back_layout = (RelativeLayout) findViewById(R.id.back_create_event_without_court_layout);
        minus_btn_add_info = (Button) findViewById(R.id.minus_btn_add_info);
        plus_btn_add_info = (Button) findViewById(R.id.plus_btn_add_info);
        confirm_btn_add_info = (Button) findViewById(R.id.confirm_btn_add_info);
        edit_game_name_add_info = (EditText) findViewById(R.id.edit_game_name_add_info);
        edit_little_notes_add_info = (EditText) findViewById(R.id.edit_little_notes_add_info);
        listingName = (EditText) findViewById(R.id.edit_nme_city);
        et_pick_location = (TextView) findViewById(R.id.edit_pick_location);
        //et_pick_location =  (AutoCompleteTextView) findViewById(R.id.pick_location_et);
        scroll_bars_create_game = (ScrollView) findViewById(R.id.scroll_bars_create_game);
        listView_registered = (ListView) findViewById(R.id.listView_registered);
        listView2_unregisred = (ListView) findViewById(R.id.listView2_unregisred);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        snackBar = new SnackBar(CreateEventWithoutCourt.this);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(CreateEventWithoutCourt.this);
        editor = preferences.edit();
        gpsTracker = new GPSTracker(this);

        if(Constants.GAME_VISIBILITY.equalsIgnoreCase("Public"))
        {
            sc_public_private.setChecked(true);
            tv_public_game.setTextColor(Color.parseColor("#FF7300"));
            tv_private_game.setTextColor(Color.parseColor("#626262"));
            tv_joingame.setText("Anyone can join");
            Constants.GAME_VISIBILITY = "Public";
        }
        else if(Constants.GAME_VISIBILITY.equalsIgnoreCase("Private"))
        {
            sc_public_private.setChecked(false);
            tv_public_game.setTextColor(Color.parseColor("#626262"));
            tv_private_game.setTextColor(Color.parseColor("#FF7300"));
            tv_joingame.setText("Only teammates can join");
            Constants.GAME_VISIBILITY = "Private";
        }
        else{
            switch_button_add_info.setChecked(true);
            Constants.GAME_VISIBILITY = "Public";
            public_add_info.setTextColor(getResources().getColor(R.color.orange));
            private_add_info.setTextColor(getResources().getColor(R.color.gray_new));
            tv_joingame.setText("Anyone can join.");
        }

        if (preferences.contains("USERNAME")) {
            userName = preferences.getString("USERNAME", "");
            image = preferences.getString("image", "");
            billing_card_count = preferences.getString("BILLING_COUNT", "");
            auth_token = preferences.getString("auth_token", "");
            user_Id = preferences.getString("loginuser_id", "");

            if (TextUtils.isEmpty(image)) {
                image = " ";
            }
        }
    }

    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Google", "Place Selected: " + place.getName());
                game_address = place.getAddress().toString();
                String latLng = place.getLatLng().toString();
                String[] latlngArr = latLng.split(" ");
                String latlngVal = latlngArr[1].substring(1, latlngArr[1].length() - 1);
                String[] finalArray = latlngVal.split(",");
                game_latitude = Double.parseDouble(finalArray[0].toString());
                game_longitude = Double.parseDouble(finalArray[1].toString());
                Constants.GAME_LOCATION = game_latitude+","+game_longitude;
                currentCity = LocationPickerManager.getCity(createEventWithoutCourt,
                        game_latitude, game_longitude);
                CharSequence attributions = place.getAttributions();
                et_pick_location.setText(game_address);
                Constants.SEARCH_LOCATION_ADDRESS = game_address;
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("Google", "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Constants.SKILL_LEVEL != null){
            skill_level_text.setText(Constants.SKILL_LEVEL);
        }
        /**Fetching this location from venue lsiting search */
        if(!TextUtils.isEmpty(Constants.SEARCH_LOCATION_ADDRESS))
        {
            et_pick_location.setText(Constants.SEARCH_LOCATION_ADDRESS);
        }
    }

    /**
     * Function responsible for setting the default skill level
     */
    public void setDefaultData(){

        /**Coming from Edit game page*/
        if(Constants.IS_EDIT_GAME_CLICKED) {
            Constants.IS_EDIT_GAME_CLICKED = false;
            confirm_btn_add_info.setText("UPDATE");
            edit_game_name_add_info.setText(Constants.GAME_NAME);
            edit_game_name_add_info.setSelection(edit_game_name_add_info.getText().length());
            if(!TextUtils.isEmpty(Constants.NOTE) && !Constants.NOTE.equalsIgnoreCase("null")) {
                edit_little_notes_add_info.setText(Constants.NOTE);
                edit_little_notes_add_info.setSelection(edit_little_notes_add_info.getText().length());
            }
            et_pick_location.setText(Constants.POI_GAME_ADDRESS);
            select_date_btn_without_court.setText(Constants.POI_GAME_DATE);
            from_time_without_court.setText(Constants.POI_GAME_START_TIME);
            to_time_without_court.setText(Constants.POI_GAME_END_TIME);
            players_count_add_info.setText(""+Constants.COURT_MAX_PLAYER_);
            skill_level_text.setText(Constants.SKILL_LEVEL);
            Constants.PLAYERS_COUNT = players_count_add_info.getText().toString().trim();

            if(Constants.GAME_VISIBILITY.equalsIgnoreCase("Public"))
            {
                sc_public_private.setChecked(true);
                tv_public_game.setTextColor(Color.parseColor("#FF7300"));
                tv_private_game.setTextColor(Color.parseColor("#626262"));
                tv_joingame.setText("Anyone can join");
                Constants.GAME_VISIBILITY = "Public";
            }
            else{
                sc_public_private.setChecked(false);
                tv_public_game.setTextColor(Color.parseColor("#626262"));
                tv_private_game.setTextColor(Color.parseColor("#FF7300"));
                tv_joingame.setText("Only teammates can join");
                Constants.GAME_VISIBILITY = "Private";
            }

            if(Constants.POI_GAME_END_COURT_TYPE.equalsIgnoreCase("Outdoor")){
                sc_indoor_outdoor.setChecked(true);
                tv_outdoor_game.setTextColor(Color.parseColor("#FF7300"));
                tv_indoor_game.setTextColor(Color.parseColor("#626262"));
                Constants.GAME_TYPE = "Outdoor";
            }else{
                sc_indoor_outdoor.setChecked(false);
                tv_outdoor_game.setTextColor(Color.parseColor("#626262"));
                tv_indoor_game.setTextColor(Color.parseColor("#FF7300"));
                Constants.GAME_TYPE = "Indoor";
            }

            plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
            minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
            plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
            minus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));

            currentCity = LocationPickerManager.getCity(createEventWithoutCourt,
                    Double.valueOf(Constants.POI_LATITUDE),  Double.valueOf(Constants.POI_LONGITUDE));

        }
        else{
            confirm_btn_add_info.setText("CONFIRM");
            Constants.SKILL_LEVEL = "All";
            Constants.GAME_VISIBILITY = "Public";
            Constants.GAME_TYPE = "Indoor";
            skill_level_text.setText(Constants.SKILL_LEVEL);
        }
    }

    //Validating user event data
    public void validateUserEventData(String eventName,
                                      String eventListingName,
                                      String eventNotes){

        if(TextUtils.isEmpty(eventName)){
            snackBar.setSnackBarMessage("Please enter a name for your event");
        }/*else if(TextUtils.isEmpty(eventListingName)){
            snackBar.setSnackBarMessage("Please enter a name for this listing");
        }*/else if(TextUtils.isEmpty(et_pick_location.getText().toString().trim())){
            snackBar.setSnackBarMessage("Please select a location for this event");
        }  else if(TextUtils.isEmpty(from_time_without_court.getText().toString().trim())
                || from_time_without_court.getText().toString().equalsIgnoreCase("From time")){
            snackBar.setSnackBarMessage("Please select start time for this event");
        } else if(TextUtils.isEmpty(to_time_without_court.getText().toString().trim())
                || to_time_without_court.getText().toString().equalsIgnoreCase("To time")){
            snackBar.setSnackBarMessage("Please select end time for this event");
        }else{
            if(confirm_btn_add_info.getText().toString().equalsIgnoreCase("UPDATE"))
            {
                /**Update POI Game API*/

                JSONArray json_time_array = new JSONArray();
                JSONObject obj = new JSONObject();
                try {
                    if(!TextUtils.isEmpty(game_starttime) && !TextUtils.isEmpty(game_endtime)) {
                        obj.put("start_time", game_starttime.toLowerCase());
                        obj.put("end_time", game_endtime.toLowerCase());
                    } else if(!TextUtils.isEmpty(game_starttime) && TextUtils.isEmpty(game_endtime)) {
                        obj.put("start_time", game_starttime.toLowerCase());
                        obj.put("end_time", Constants.POI_GAME_END_TIME);
                    } else if(TextUtils.isEmpty(game_starttime) && !TextUtils.isEmpty(game_endtime)) {
                        obj.put("start_time", Constants.POI_GAME_START_TIME);
                        obj.put("end_time", game_endtime.toLowerCase());
                    } else{
                        obj.put("start_time", Constants.POI_GAME_START_TIME);
                        obj.put("end_time",  Constants.POI_GAME_END_TIME);
                    }
                    json_time_array.put(obj);

                    if(!TextUtils.isEmpty(Constants.GAME_LOCATION)) {
                        updateUnRegisteredGame(Constants.GAME_NAME, et_pick_location.getText().toString(), Constants.NOTES_INFO,
                                String.valueOf(Constants.PLAYERS_COUNT), Constants.SKILL_LEVEL,
                                select_date_btn_without_court.getText().toString().trim(), json_time_array,
                                Constants.GAME_VISIBILITY, Constants.GAME_LOCATION, currentCity, et_pick_location.getText().toString(), Constants.GAME_TYPE);
                    } else{
                        updateUnRegisteredGame(Constants.GAME_NAME, et_pick_location.getText().toString(), Constants.NOTES_INFO,
                                String.valueOf(Constants.PLAYERS_COUNT), Constants.SKILL_LEVEL,
                                select_date_btn_without_court.getText().toString().trim(), json_time_array,
                                Constants.GAME_VISIBILITY, Constants.POI_LATITUDE+","+Constants.POI_LONGITUDE, currentCity, et_pick_location.getText().toString(), Constants.GAME_TYPE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Constants.isUnRegisteredvenueGameCreated = true;
                Constants.AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<>();
                Constants.AVAILABILITY_TIME_SLOT_ARRAY.add(game_starttime.toLowerCase() + " - " + game_endtime.toLowerCase());
                Constants.PLAYERS_COUNT = players_count_add_info.getText().toString().trim();
                Intent intent = new Intent(CreateEventWithoutCourt.this, Summary.class);
                startActivity(intent);
            }
        }
    }
    /**
     * Un-Registered POI games
     */
    public void updateUnRegisteredGame(String game_name,String courtName,String note,
                                       String playersCount,String skillLevel,
                                       String date,JSONArray time_jsonArray,
                                       String visibility,String location, String city,
                                       String game_address,String game_type)
    {
        if(Constants.UPCOMING_PAST_CLICKED)
        {
            Constants.UPCOMING_PAST_CLICKED_FOR_UPDATE_GAME = true;
            Constants.UPCOMING_GAME_UPDATED_SUCCESS = true;
            String game_id = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Id();
            String sport_name = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Sports_Type();

            gameCreateWithUnRegisteredCourts(user_Id, UpcomingPastAdapter.event_id,game_name,
                    note, playersCount, skillLevel, date, time_jsonArray, visibility,
                    userName, image, location, city, game_address,courtName, game_type);
        }
        else if(Constants.FROM_WISHLIST)
        {
            Constants.FROM_WISHLIST_GAME_UPDATED = true;
            gameCreateWithUnRegisteredCourts(user_Id, WishListAdapter.venueId,game_name,
                    note, playersCount, skillLevel, date, time_jsonArray, visibility,
                    userName, image, location, city, game_address,courtName, game_type);
        }
        else
        {
            gameCreateWithUnRegisteredCourts(user_Id, EventAdapter.event_Id,game_name,
                    note, playersCount, skillLevel, date, time_jsonArray, visibility,
                    userName, image, location, city, game_address,courtName, game_type);
        }
    }
    /**
     * Game create API call
     */
    public void gameCreateWithUnRegisteredCourts(String user_id, String list_id,String name,
                                                 String note, String no_of_player, String skill_level, String date,
                                                 JSONArray time_slots, String visibility, String userName, String image, String loation, String city,
                                                 String address, String courtName, String game_type) {
        //Handling the loader state
        LoaderUtility.handleLoader(CreateEventWithoutCourt.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForUnregisteredCourts(user_id, list_id, name, note,
                no_of_player, skill_level, date, time_slots, visibility, userName, image,loation, city, address, courtName, game_type);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(CreateEventWithoutCourt.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    if (responseObject.has("status") && !responseObject.isNull("status")) {
                        status = responseObject.getString("status");
                        AppConstants.GAME_ID = responseObject.getString("game_id");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                CreateEventWithoutCourt.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(CreateEventWithoutCourt.this, false);
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                Constants.isEventCreated = true;
                                Constants.isUnRegisteredvenueGameCreated = false;
                                Constants.AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<String>();
                                Toast.makeText(CreateEventWithoutCourt.this, "Event created successfully", Toast.LENGTH_SHORT).show();

                                finish();
                            } else {
                                snackBar.setSnackBarMessage("Unable to create this event");
                            }
                        } else {
                            snackBar.setSnackBarMessage("Network error! please try after sometime");
                        }
                    }
                });
            }
        });
    }



    /**
     *  */
    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     *//*
     * @return
     */
    public Request buildApiRequestBodyForUnregisteredCourts(String user_id, String game_id, String name,
                                                            String note, String no_of_player, String skill_level, String date,
                                                            JSONArray time_slots, String visibility, String userName, String image, String location,
                                                            String city, String address, String courtName, String gameType) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "save_unregistred_game_info").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("game_id", game_id);
        urlBuilder.addQueryParameter("court_name", courtName);
        urlBuilder.addQueryParameter("name", name);
        urlBuilder.addQueryParameter("note", note);
        urlBuilder.addQueryParameter("no_of_player", no_of_player);
        urlBuilder.addQueryParameter("skill_level", skill_level);
        urlBuilder.addQueryParameter("date", date);
        urlBuilder.addQueryParameter("time_slots", time_slots.toString());
        urlBuilder.addQueryParameter("visibility", visibility);
        urlBuilder.addQueryParameter("device_type", "android");
        urlBuilder.addQueryParameter("user_name", userName);
        urlBuilder.addQueryParameter("user_profile_image", image);
        urlBuilder.addQueryParameter("location", location);
        urlBuilder.addQueryParameter("city", city);
        urlBuilder.addQueryParameter("address", address);
        urlBuilder.addQueryParameter("game_type", gameType);
        urlBuilder.addQueryParameter("is_registered_event ", Boolean.FALSE.toString());
        String url = urlBuilder.build().toString();
        url = url.replace(" ", "");

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(CreateEventWithoutCourt.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }


    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();

            br.close();
        } catch (Exception e) {
            // Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    public class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {  //39.332641
            // For storing data from web service
            String data = "";
            // Obtain browser key from https://code.google.com/apis/console
            //String key = "key=AIzaSyAqGDJ21KFY_iuYsKeeucLbydRUBVcCXXo";
            String key = "key=AIzaSyAfwuoJ0Vn4QQ7mmlpe_FibC0keZ_Uv2V8";

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            // place type to be searched
            String types = "types=establishment|geocode";
            // Sensor enabled
//            String sensor = "sensor=false";
//            String radius = "32500";
            //String radius = preferences.getString("RADIUS_COUNT","");
            // String component="components=country:"+iso_countrycode;
            // Building the parameters to the web service
            String location = "location="+gpsTracker.getLatitude()+","+gpsTracker.getLongitude();
            /*String parameters = input + "&" + types + "&" + radius + "&"
                    + sensor + "&" + key;*/
            String parameters = input + "&" + location + "&" + key;
            // Output format
            String output = "json";
            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"
                    + output + "?" + parameters;
            try {
                // Fetching the data from web service in background
                data = downloadUrl(url);
                System.out.println("url----" + url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Creating ParserTask
            parserTask = new ParserTask();
            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    public class ParserTask extends
            AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;
        JSONArray contacts = null;
        JSONObject structured_formatting_object = null;
        ArrayList<String> places = new ArrayList<String>();
        ArrayAdapter<String> adapter;

        @Override
        protected List<HashMap<String, String>> doInBackground(
                String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {

                jObject = new JSONObject(jsonData[0]);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
                System.out.println("Places are" + places);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            try {
                String[] id_val;
                String short_address = "", full_address = "";

                contacts = jObject.getJSONArray("predictions");

                for (int i = 0; i < contacts.length(); i++) {
                    locationVenueModel = new LocationVenueModel();
                    JSONObject c = contacts.getJSONObject(i);

                    String address = c.getString("description");
                    String reference = c.getString("reference");
                    String place_id = c.getString("place_id");

                    if(c.has("structured_formatting")) {
                        structured_formatting_object = c.getJSONObject("structured_formatting");

                        if (structured_formatting_object.has("main_text") && !structured_formatting_object.isNull("main_text")) {
                            short_address = structured_formatting_object.getString("main_text");
                        }
                        if (structured_formatting_object.has("secondary_text") && !structured_formatting_object.isNull("secondary_text")) {
                            full_address = structured_formatting_object.getString("secondary_text");
                        }
                        locationVenueModel.setVenue_Short_Address(short_address);
                        locationVenueModel.setVenue_Full_Address(full_address);
                        locationVenueModel.setVenue_Registered_status(false);

                        locationVenueModelArrayList.add(locationVenueModel);
                        is_Unregistred_Venues_Available = true;
                    }
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //System.out.println("reault" + places);
            // Creating a SimpleAdapter for the AutoCompleteTextView
            try {
                /*adapter = new ArrayAdapter<String>(VenueListing.this,
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1, places);*/
                /**Handle Loader*/
                // Setting the adapter
                /*locationVenueAdapter = new LocationVenueAdapter(CreateEventWithoutCourt.this, locationVenueModelArrayList);
                et_pick_location.setAdapter(locationVenueAdapter);*/

            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    /**
     * Function to get Venue by search from server
     */
    public void getVenuesFromSearch(final String venue_name) {
        //Handling the loader state
        //LoaderUtility.handleLoader(VenueListing.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyFroSearch(venue_name);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(CreateEventWithoutCourt.this, true);
                //Displaying the Error Message if the call get failed
                if (!ApplicationUtility.isConnected(CreateEventWithoutCourt.this)) {
                    ApplicationUtility.displayNoInternetMessage(CreateEventWithoutCourt.this);
                } else {
                    ApplicationUtility.displayErrorMessage(CreateEventWithoutCourt.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", name = "", location = "";

                try {
                    locationVenueModelArrayList = new ArrayList<>();

                    JSONObject response_Object = new JSONObject(responseData);
                    if(response_Object.has("status"))
                    {
                        String status = response_Object.getString("status");
                        if(status.equalsIgnoreCase("success"))
                        {
                            if(response_Object.has("venues") && !response_Object.isNull("venues"))
                            {
                                JSONArray jsonArray = response_Object.getJSONArray("venues");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    locationVenueModel = new LocationVenueModel();

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    id = jsonObject.getString("id");
                                    name = jsonObject.getString("name");
                                    location = jsonObject.getString("location");

                                    locationVenueModel.setVenue_Id(id);
                                    locationVenueModel.setVenue_Short_Address(name);
                                    locationVenueModel.setVenue_Full_Address(location);
                                    locationVenueModel.setVenue_Registered_status(true);

                                    locationVenueModelArrayList.add(locationVenueModel);
                                }
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                CreateEventWithoutCourt.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        //LoaderUtility.handleLoader(VenueListing.this, false);
                        try {
                            placesTask = new PlacesTask();
                            placesTask.execute(venue_name);
                        }catch (Exception exp)
                        {
                            exp.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyFroSearch(String venue_name) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "search_venues").newBuilder();
        urlBuilder.addQueryParameter("venue_name", venue_name);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }
}