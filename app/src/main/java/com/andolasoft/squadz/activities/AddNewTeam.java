package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.TeamsModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SpNayak on 1/31/2017.
 */

public class AddNewTeam extends AppCompatActivity {

    @InjectView(R.id.back_add_team_layout)
    RelativeLayout back_add_team_layout;
    @InjectView(R.id.club_team)
    EditText etClubName;
    @InjectView(R.id.spinner_teammates_map_list)
    Spinner spPickSporttype;
    @InjectView(R.id.layAddplayer)
    RelativeLayout layAddPlayer;
    @InjectView(R.id.create_new_team_btn)
    Button btnCreateTeam;
    @InjectView(R.id.titletxt_addteam)
    TextView titletxt_addteam;
    ArrayList<String> sports_name_array,sports_id_array;
    HashMap<String, String> map_sports_name;
    DatabaseAdapter db;
    ProgressDialog dialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String user_id = "",auth_token = "",sport_id = "",
            team_id = "", username = "", image = "";
    SnackBar snackBar;
    public static String teamName = "";
    public static Set<String> set = null;
    ArrayAdapter<String> dataAdapter = null;
    String custom_sport_name = "";
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_addteams_layout);

        db = new DatabaseAdapter(AddNewTeam.this);


        //Initializing views

        initViews();


        //Adding Click events on te views
        setClickEvents();

        /**Adding existing data*/
        setData();

        //Getting all the sport from the local DB
        // and setting them in the spinner
        getSportsName();
    }

    /**
     * Adding Click events on te views
     */
    public void setClickEvents() {
        //Back button click
        back_add_team_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Initializibg the arralist
                Constants.ADD_PLAYER_ARRAYLIST = new ArrayList<String>();
                Constants.ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST = new ArrayList<>();
                Constants.IS_EDIT_BUTTON_CLICKED = false;
                clearSharedPref();
                finish();
            }
        });

        layAddPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                teamName = etClubName.getText().toString().trim();
                if(TextUtils.isEmpty(teamName)) {
                    snackBar.setSnackBarMessage("Please insert your team name before adding players");
                }
                else if(Constants.IS_EDIT_BUTTON_CLICKED)
                {
                    ArrayList<String> players_firstName_Array = db.getPlayers(Constants.TEAM_ID);

                    SharedPreferences add_playerpreferences = getSharedPreferences("AddPlayerPrefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor add_playereditor = add_playerpreferences.edit();
                    add_playereditor.clear();
                    add_playereditor.apply();

                    set = new HashSet<String>();
                    set.addAll(players_firstName_Array);
                    add_playereditor.putStringSet(Constants.EDIT_NAME_SPORT_NAME, set);
                    add_playereditor.apply();

                    startActivity(new Intent(AddNewTeam.this, TeamPlayerPicker.class));
                }
                else if(db.getAllPlayerCount() == 0)
                {
                    snackBar.setSnackBarMessage("You have no teammates to add");
                }
                else{
                    startActivity(new Intent(AddNewTeam.this, TeamPlayerPicker.class));
                }
            }
        });


        spPickSporttype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Counter check for not calling getVenuesFilterListing() for first time
                // as it was called for first time automatically
                TeamMates.SELECTED_SPORT_NAME = spPickSporttype.getSelectedItem().toString();
                int pos = HomeFragment.getCategoryPos(sports_name_array, TeamMates.SELECTED_SPORT_NAME);
                spPickSporttype.setSelection(pos);

                String sportName = spPickSporttype.getSelectedItem().toString();

                if(sportName != null &&
                        sportName.equalsIgnoreCase("Custom")){
                    counter = counter  + 1;
                    if(counter > 1) {
                        openDialog();
                    }
                }
                else{
                    counter = 1;
                }
                /**Getting Sport ID from database*/
                sport_id = db.getSportId(sportName);
                Constants.SPORTS_ID = sport_id;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCreateTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                teamName = etClubName.getText().toString().trim();

                if(TextUtils.isEmpty(teamName)){
                    Toast.makeText(AddNewTeam.this, "Please enter team name", Toast.LENGTH_SHORT).show();
                }
                else if(btnCreateTeam.getText().toString().equalsIgnoreCase("UPDATE"))
                {
                    /**API call for Update a Team*/
                    if(!TextUtils.isEmpty(custom_sport_name))
                    {
                        updateTeamAPI(auth_token, teamName, Constants.SPORTS_ID, custom_sport_name);
                    }
                    else{
                        updateTeamAPI(auth_token, teamName, Constants.SPORTS_ID, "");
                    }

                }

                else{
                    /**API call for Create New Team*/
                    if(!TextUtils.isEmpty(custom_sport_name))
                    {
                        createTeamAPI(auth_token, teamName, sport_id, custom_sport_name);
                    }
                    else{
                        createTeamAPI(auth_token, teamName, sport_id, "");
                    }

                }
            }
        });
    }

    /**
     * Initializing views
     */
    public void initViews() {
        ButterKnife.inject(this);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(AddNewTeam.this);
        editor = preferences.edit();
        snackBar = new SnackBar(this);

        if(preferences.contains("loginuser_id"))
        {
            user_id = preferences.getString("loginuser_id","");
            auth_token = preferences.getString("auth_token","");
            username = preferences.getString("username","");
            image = preferences.getString("image","");
        }
        /**Getting Sport ID from database*/
        sport_id = db.getSportId(Constants.SPORTS_NAME);
    }

    /**
     * Adding existing data
     */
    public void setData()
    {
        if(Constants.IS_EDIT_BUTTON_CLICKED)
        {
            etClubName.setText(Constants.TEAM_NAME);
            titletxt_addteam.setText("Edit Team");
            btnCreateTeam.setText("UPDATE");
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Initializibg the arralist
        Constants.ADD_PLAYER_ARRAYLIST = new ArrayList<String>();
        Constants.ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST = new ArrayList<>();
        Constants.IS_EDIT_BUTTON_CLICKED = false;
        clearSharedPref();
        finish();
    }

    //Getting Sports name from database
    public void getSportsName() {
        Cursor cursor = db.getSportsName();

        sports_name_array = new ArrayList<>();
        sports_id_array = new ArrayList<>();

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String sport_Name = cursor.getString(cursor
                            .getColumnIndex("sport_name"));
                    String sport_id = cursor.getString(cursor
                            .getColumnIndex("sport_id"));

                    sports_name_array.add(sport_Name);
                    sports_id_array.add(sport_id);

                } while (cursor.moveToNext());
            }

            //Sending sports name with sports id to the spinner
            setVenueSportsName(sports_name_array, sports_id_array);
        }
    }

    /**
     * Displaying the user sport in the drop down
     * @param sports_name_array Sports Name Array
     * @param sports_id_array Sports Id Array
     */
    public void setVenueSportsName(ArrayList<String> sports_name_array, ArrayList<String> sports_id_array) {

        //Getting venue name & id as a array to display inside the spinner
        String[] spinnerArray = new String[sports_id_array.size()];
        map_sports_name = new HashMap<String, String>();

        for (int i = 0; i < sports_id_array.size(); i++) {
            map_sports_name.put(sports_name_array.get(i), sports_id_array.get(i));
            spinnerArray[i] = sports_name_array.get(i);
        }

        //Sending sports name as a String[]
        displaySpinnerInfo(spinnerArray);
    }

    /**
     * Displaying name inside spinner
     * @param spinnerArray
     */
    public void displaySpinnerInfo(String[] spinnerArray) {
        dataAdapter = new ArrayAdapter<String>(AddNewTeam.this, android.R.layout.simple_spinner_item, spinnerArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPickSporttype.setAdapter(dataAdapter);
        int pos = HomeFragment.getCategoryPos(sports_name_array, TeamMates.SELECTED_SPORT_NAME);
        spPickSporttype.setSelection(pos);
        Constants.EDIT_NAME_SPORT_NAME = TeamMates.SELECTED_SPORT_NAME;
    }

    /**
     * Function responsible to create the team
     * @param auth_token Logged in User Auth token
     * @param team_name User inputted team Name
     * @param sport_id User selected Sport
     * @param custom_sport_name [Optional] User Selected custom Sport if any
     */
    public void createTeamAPI(final String auth_token,
                              String team_name,
                              String sport_id,
                              String custom_sport_name) {
        //Handling the loader state
        LoaderUtility.handleLoader(AddNewTeam.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(auth_token,team_name,sport_id,custom_sport_name);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(AddNewTeam.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject team_jsonObject = new JSONObject(responseData);
                    if(team_jsonObject.has("status") && !team_jsonObject.isNull("status")) {
                        status = team_jsonObject.getString("status");
                    }
                    if(team_jsonObject.has("message") && !team_jsonObject.isNull("message")) {
                        message = team_jsonObject.getString("message");
                    }

                    if(team_jsonObject.has("team") && !team_jsonObject.isNull("team")) {
                        JSONObject jsonObject_team = team_jsonObject.getJSONObject("team");

                        team_id =jsonObject_team.getString("_id");
                        String name =jsonObject_team.getString("name");
                        String sport_id =jsonObject_team.getString("sport_id");
                        String sportName = db.getcurrentSportName(sport_id);
                        String sport_name =jsonObject_team.getString("sport_name");
                        String updated_at =jsonObject_team.getString("updated_at");
                        String user_id =jsonObject_team.getString("user_id");

                        //Inserting the teams data in Local DB
                        db.insertMyTeam(team_id,name,sportName,sport_name,
                                user_id,username,image,"",updated_at);

                        if(jsonObject_team.has("players") && !jsonObject_team.isNull("players")) {
                            JSONArray team_jsoJsonArray = jsonObject_team.getJSONArray("players");

                            for(int j = 0; j < team_jsoJsonArray.length(); j++){
                                JSONObject teamPlayersObj = team_jsoJsonArray.getJSONObject(j);
                                String player_id = teamPlayersObj.getString("player_id");
                                String player_user_name = teamPlayersObj.getString("player_name");
                                String player_first_name = teamPlayersObj.getString("first_name");
                                String player_last_name = teamPlayersObj.getString("last_name");
                                String player_image = teamPlayersObj.getString("image");

                                //Inserting the teammayes data in Local DB
                                db.insertTeamPlayer(team_id, player_id, player_user_name,
                                        player_first_name, player_last_name, player_image );
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalStatus = status;
                final String finalMessage = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(AddNewTeam.this, false);
                        if(finalStatus.equalsIgnoreCase("success")) {
                            //API integration for Adding player to the Newly added team
                            if(Constants.ADD_PLAYER_ARRAYLIST.size() > 0) {
                                addPlayerAPI(auth_token, team_id);
                            } else{
                                Constants.IS_NEWLY_ADDED_TEAM = true;
                                /**Clear Shared preferences*/
                                clearSharedPref();
                                Toast.makeText(AddNewTeam.this, ""+"Team created successfully", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                        else if(finalStatus.equalsIgnoreCase("error")) {
                            snackBar.setSnackBarMessage(finalMessage);
                        }
                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String auth_token, String team_name, String sport_id,String custom_sport_name) {

        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "/teams/" + auth_token + "/" + "create").newBuilder();
        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION + "/teams/" + auth_token + "/" + "create").newBuilder();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+ "create_team").newBuilder();

        urlBuilder.addQueryParameter("name", team_name);
        urlBuilder.addQueryParameter("sport_id", sport_id);
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("sport_name", TeamMates.SELECTED_SPORT_NAME);

        if(!TextUtils.isEmpty(custom_sport_name)) {
            urlBuilder.addQueryParameter("custom_sport_name", custom_sport_name);
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Function responsible for adding player to the team
     * @param auth_token logged in uset ayth token
     * @param team_id newly created team id
     */
    public void addPlayerAPI(String auth_token, final String team_id) {
        //Handling the loader state
        LoaderUtility.handleLoader(AddNewTeam.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForAddPlayers(auth_token,team_id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(AddNewTeam.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject player_jsonObject = new JSONObject(responseData);
                    if(player_jsonObject.has("status") && !player_jsonObject.isNull("status")) {
                        status = player_jsonObject.getString("status");
                    }
                    if(player_jsonObject.has("message") && !player_jsonObject.isNull("message")) {
                        message = player_jsonObject.getString("message");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                final String finalStatus = status;
                final String finalMessage = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(AddNewTeam.this, false);

                        if(finalStatus.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                            Constants.IS_NEWLY_ADDED_TEAM = true;
                            /**Clear Shared preferences*/
                            clearSharedPref();
                            Toast.makeText(AddNewTeam.this, ""+ "Team created successfully", Toast.LENGTH_LONG).show();

                            /**Store Players records in local database*/
                            for (int i = 0; i < Constants.ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST.size(); i++) {
                                String[] splite_player_records = Constants.ADD_PLAYER_RECORDS_FOR_DB_ARRAYLIST.get(i).split("@@@@");

                                int length = splite_player_records.length;

                                if(length==2) {
                                    String player_id = splite_player_records[0];
                                    String plare_user_name = splite_player_records[1];

                                    //Inserting the teammayes data in Local DB
                                    db.insertTeamPlayer(team_id, player_id, plare_user_name,
                                            "", "", "");
                                }

                                if(length==3) {
                                    String player_id = splite_player_records[0];
                                    String plare_user_name = splite_player_records[1];
                                    String player_first_name = splite_player_records[2];

                                    //Inserting the teammayes data in Local DB
                                    db.insertTeamPlayer(team_id, player_id, plare_user_name,
                                            player_first_name, "", "");
                                }

                                if(length==4) {
                                    String player_id = splite_player_records[0];
                                    String plare_user_name = splite_player_records[1];
                                    String player_first_name = splite_player_records[2];
                                    String player_last_name = splite_player_records[3];

                                    //Inserting the teammayes data in Local DB
                                    db.insertTeamPlayer(team_id, player_id, plare_user_name,
                                            player_first_name, player_last_name, "");
                                } else if(length == 5) {
                                    String player_id = splite_player_records[0];
                                    String plare_user_name = splite_player_records[1];
                                    String player_first_name = splite_player_records[2];
                                    String player_last_name = splite_player_records[3];
                                    String player_image = splite_player_records[4];

                                    //Inserting the teammayes data in Local DB
                                    db.insertTeamPlayer(team_id, player_id, plare_user_name,
                                            player_first_name, player_last_name, player_image);
                                }
                            }

                            finish();
                        }
                        else if(finalStatus.equalsIgnoreCase("error")) {
                            snackBar.setSnackBarMessage(finalMessage);
                        }
                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForAddPlayers(String auth_token, String team_id) {
        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "/teams/" + auth_token + "/" +team_id + "/add_players").newBuilder();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"add_player_team").newBuilder();
        urlBuilder.addQueryParameter("team_id",team_id);
        urlBuilder.addQueryParameter("player_ids", strPlayers(Constants.ADD_PLAYER_ARRAYLIST));//Array of Players Ids in a String format with comma separator

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function responsible to update the team info and players
     * @param auth_token logged in user auth token
     * @param team_name Concern team name
     * @param sport_id Concern team sport is
     * @param custom_sport_name Concern team custom sport name
     */
    public void updateTeamAPI(final String auth_token, String team_name, String sport_id, String custom_sport_name) {
        //Handling the loader state
        LoaderUtility.handleLoader(AddNewTeam.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForGameUpdate(auth_token,team_name,sport_id,custom_sport_name);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(AddNewTeam.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject team_jsonObject = new JSONObject(responseData);
                    if(team_jsonObject.has("status") && !team_jsonObject.isNull("status")) {
                        status = team_jsonObject.getString("status");
                    }
                    if(team_jsonObject.has("message") && !team_jsonObject.isNull("message")) {
                        message = team_jsonObject.getString("message");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalStatus = status;
                final String finalMessage = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(AddNewTeam.this, false);
                        if(finalStatus != null &&
                                finalStatus.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                            //API integration for Adding player to the Newly added team
                            Constants.IS_UPDATED_TEAM = true;
                            clearSharedPref();
                            finish();
                            Toast.makeText(AddNewTeam.this, ""+finalMessage, Toast.LENGTH_SHORT).show();
                        } else if(finalStatus != null &&
                                finalStatus.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)) {
                            snackBar.setSnackBarMessage(finalMessage);
                        }
                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForGameUpdate(String auth_token, String team_name, String sport_id,String custom_sport_name) {

        //http://54.164.124.188/api/v1/teams/:auth_token/:team_id/update_team
        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION + "teams/" + auth_token + "/"+Constants.TEAM_ID + "/update_team").newBuilder();
        //HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "teams/" + auth_token + "/"+Constants.TEAM_ID + "/update_team").newBuilder();

        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE_MIGRATION+"update_team").newBuilder();

        urlBuilder.addQueryParameter("team_id", Constants.TEAM_ID);
        //urlBuilder.addQueryParameter("team_name", team_name);
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("name", team_name);
        urlBuilder.addQueryParameter("sport_id", sport_id);
        urlBuilder.addQueryParameter("player_ids", strPlayers(Constants.ADD_PLAYER_ARRAYLIST));

        if(!TextUtils.isEmpty(custom_sport_name)) {
            urlBuilder.addQueryParameter("custom_sport_name", custom_sport_name);
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function responsible for adding player ids from array
     * @param time_slots_Array
     * @return Comma separated
     */
    public String strPlayers(ArrayList<String> time_slots_Array) {
        String str_builder_players = "";
        StringBuilder builder = new StringBuilder();
        for (String details : time_slots_Array) {
            String playerId = db.getcurrentfriendId(details);
            builder.append(playerId + ",");
        }
        str_builder_players = builder.toString();
        return str_builder_players;
    }

    /**
     * Function responsible for Clearing the Shared Preferance
     */
    public void clearSharedPref() {
        SharedPreferences add_playerpreferences = getSharedPreferences("AddPlayerPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor add_playereditor = add_playerpreferences.edit();
        add_playereditor.clear();
        add_playereditor.apply();
        Set<String> set = new HashSet<String>();

        //Initializing the arralist
        Constants.ADD_PLAYER_ARRAYLIST = new ArrayList<String>();
    }

    /**
     * Function responsible for opening
     * the custom sport dialog
     */
    private void openDialog(){
        LayoutInflater inflater = LayoutInflater.from(AddNewTeam.this);
        final View subView = inflater.inflate(R.layout.dialog_custom_gamename, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialog_edittext);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Custom Game Name");
        builder.setMessage("");
        builder.setView(subView);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String custom_game_name = subEditText.getText().toString();
                if(!TextUtils.isEmpty(custom_game_name)){
                    //spPickSporttype.setPrompt(custom_game_name);
                    custom_sport_name = custom_game_name;
                }else{
                    snackBar.setSnackBarMessage("Please enter name for your custom game");
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }
}
