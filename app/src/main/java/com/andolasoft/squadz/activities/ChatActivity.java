package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.ChatFragmnet;
import com.andolasoft.squadz.fragments.TabFragment;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.ChatConversationModel;
import com.andolasoft.squadz.models.ChatModel;
import com.andolasoft.squadz.models.FirebaseBadgeCountModel;
import com.andolasoft.squadz.models.FirebaseGroupChatParticipantModel;
import com.andolasoft.squadz.models.FirebaseGroupDetailsModel;
import com.andolasoft.squadz.models.GroupChatReadMessageCount;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.MessagesListAdapter;
import com.andolasoft.squadz.views.adapters.NavigationDrawerAdapter;
import com.andolasoft.squadz.views.adapters.TeammateListAdapter;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatActivity extends AppCompatActivity {

    @InjectView(R.id.back_icon_profile)
    ImageView ivBack;
    @InjectView(R.id.titletxt)
    TextView tvTitle;
    @InjectView(R.id.profile_menu)
    ImageView ivShowpopup;
    public static ListView lv_Chat;
    @InjectView(R.id.inputMsg)
    EditText etChatMessage;
    @InjectView(R.id.btnSend)
    ImageView ivSendMessage;
    SharedPreferences pref;
    String senderId, loggedin_user_url, logged_in_username;
    public static MessagesListAdapter chatAdapter;
    ArrayList<ChatModel> chatArray;
    MaterialDialog mMaterialDialog;
    public static boolean isGroupNameUpdated = false;
    int MESSAGE_READ_COUNT = 0;
    ProgressDialog dialog;
    boolean isGroupMessageSentFirstTime = false;
    ArrayList<String> arr_added_user_group, arr_removed_user_group;
    Firebase postRef_delete_group;
    int messageBadgeCountUpdateCall = 0;
    public static long messageBadgeCount = 0;
    boolean isRecipientonline = false;
    ArrayList<String> groupChatoffLineUsersArray;
    boolean isTableExist = false;
    int updatedBadgeCount = 0;
    String chatMessage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        ButterKnife.inject(this);

        //Function responsibel for getting the user details
        getSenderData();

        getChannelBadgeCount();

        //chatPostTasks();



    }

    /**
     * Updating user online status
     * @param onlineStatus
     */
    public void updateUserOnlineStatus(boolean onlineStatus){
        //Initializing the firebase client
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Updating the group name
        postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(AppConstants.CHAT_USER_ID).child(AppConstants.CHAT_CHANNEL_ID);
        postRef.child("isOnline").setValue(onlineStatus);
    }


    /**
     * Updating user online status
     * @param offLineUsers
     */
    public void updateUserOnlineStatusForGroup(ArrayList<String> offLineUsers){
        //Initializing the firebase client
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Updating the group name
        postRef = postRef.child("group").child(AppConstants.CHAT_USER_ID).child("offLineUsers");
        postRef.setValue(offLineUsers);

        //Checking the group user online status
       // getUserOnlineStatusGroupChat();
    }


    /**
     * Fetching conversation data from firebase
     */
    public void fetchGroupOnlileUsersArray() {
        //Initializing the firebase instance
        groupChatoffLineUsersArray = new ArrayList<>();
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef.child("group").child(AppConstants.CHAT_USER_ID).child("offLineUsers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                    groupChatoffLineUsersArray = (ArrayList<String>) dataSnapshot.getValue();
                    if(groupChatoffLineUsersArray != null &&
                        groupChatoffLineUsersArray.contains(senderId)){
                        groupChatoffLineUsersArray.remove(senderId);
                    }
                    updateUserOnlineStatusForGroup(groupChatoffLineUsersArray);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

    }


    /**
     * Fetching conversation data from firebase
     */
    public void removeGroupOnlileUser() {
        //Initializing the firebase instance
        groupChatoffLineUsersArray = new ArrayList<>();
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef.child("group").child(AppConstants.CHAT_USER_ID).child("offLineUsers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                groupChatoffLineUsersArray = (ArrayList<String>) dataSnapshot.getValue();
                if(!groupChatoffLineUsersArray.contains(senderId)){
                    groupChatoffLineUsersArray.add(senderId);
                }
                updateUserOnlineStatusForGroup(groupChatoffLineUsersArray);
                //Handling the device back button presses
                handleBackButton();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    /**
     * Function to keep request focus in the edit text
     */
    public void putRequestFocus(){
        etChatMessage.requestFocus();
    }


    /**
     * Initialize Chat View
     */
    public void initializeChatView(){
        //Setting the user name in the chat board view
        if(AppConstants.CHAT_TYPE != null &&
                AppConstants.CHAT_TYPE.equalsIgnoreCase("group")){
            if(AppConstants.groupParticipantsIds.size() == 0){
                tvTitle.setText(AppConstants.CHAT_USER_NAME);
            }else{
                //Getting the player count for the participants array
                int player_count = AppConstants.groupParticipantsIds.size();

                //Getting the player count for the participants array and setting it in the text view
                tvTitle.setText(AppConstants.CHAT_USER_NAME + " + "+String.valueOf(player_count));

                if(!AppConstants.isGroupUpdated){
                    //initializing the user ids array list
                    TeammateListAdapter.userIds = new ArrayList<>();

                    TeammateListAdapter.userIds_duplicate = new ArrayList<>();

                    //Adding all the user ids into the  TeammateListAdapter.userIds array
                    for (int i = 0; i < AppConstants.groupParticipantsIds.size(); i++){
                        TeammateListAdapter.userIds.add(AppConstants.groupParticipantsIds.get(i));
                        TeammateListAdapter.userIds_duplicate.add(AppConstants.groupParticipantsIds.get(i));
                    }
                }
            }
        }else{
            tvTitle.setText(AppConstants.CHAT_USER_NAME);
        }
        lv_Chat = (ListView) findViewById(R.id.chat_list);
        chatArray = new ArrayList<>();
        chatAdapter = new MessagesListAdapter(ChatActivity.this,chatArray);
        lv_Chat.setAdapter(chatAdapter);
        //Scrolling the list view to the bottom
        scrollMyListViewToBottom();
    }

    /**
     * Text watcher for the chat message edit text
     */
    private final TextWatcher chatMessageWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable s) {
            if (s.toString().trim().length() == 0) {
                changeSendImage(false);
            } else{
                changeSendImage(true);
            }
        }
    };


    /**
     * Function responsible for handling the click events
     */
    public void setClickevents(){
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TeammateListAdapter.isChannelCreated = false;
                //Handleing data and page redirection
                if(AppConstants.CHAT_TYPE != null &&
                        AppConstants.CHAT_TYPE.equalsIgnoreCase("individual")){
                    //Function call to update the chat message read count
                    updatereadMessageCount();
                }else if(AppConstants.CHAT_TYPE != null &&
                        AppConstants.CHAT_TYPE.equalsIgnoreCase("group")){
                    updateGroupreadMessageCount();
                } else{
                    //Handling the device back button presses
                    handleBackButton();
                }
            }
        });
        ivSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Calling the firebase function to send the message
                sendMessage();
            }
        });

        ivShowpopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Displaying the edit group name dialog
                if(AppConstants.CHAT_TYPE != null &&
                        AppConstants.CHAT_TYPE.equalsIgnoreCase("group")){
                    //Displaying the group name edit dialog
                    displayEditTeamDialog();
                }
            }
        });

        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AppConstants.CHAT_TYPE != null &&
                        AppConstants.CHAT_TYPE.equalsIgnoreCase("group")){
                    AppConstants.isUserGointToUpdateTheGroup = true;
                    Intent intent = new Intent(ChatActivity.this, AddFriendToChat.class);
                    startActivity(intent);
                }
            }
        });


    }

    /**
     * Function responsible for handleing the send image color
     * @param textchangestatus
     */
    public void changeSendImage(boolean textchangestatus){
        if(textchangestatus){
            ivSendMessage.setBackgroundResource(R.drawable.ic_send_orange_48dp);
            ivSendMessage.setFocusable(true);
            ivSendMessage.setClickable(true);
        }else{
            ivSendMessage.setBackgroundResource(R.drawable.ic_send_gray_48dp);
            ivSendMessage.setFocusable(false);
            ivSendMessage.setClickable(false);
        }
    }


    /**
     * Function responsible for sending message
     */
    public void sendMessage(){
        //Getting the user chat message
        chatMessage = etChatMessage.getText().toString().trim();
        int getCurrentDate = getCurrentDate();
        String getCurrentDateandTIme = getCurrentDateAndTIme();
        etChatMessage.setText("");

        //Initializing the firebase client
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child("message").child(AppConstants.CHAT_CHANNEL_ID);

        ArrayList<String> chatMembersArray = new ArrayList<>();

        ChatModel chatModel = null;
        if(AppConstants.CHAT_TYPE != null &&
                AppConstants.CHAT_TYPE.equalsIgnoreCase(AppConstants.CHAT_TYPE_INDIVIDUAL)){

            //Building the chat message model
            chatModel = new ChatModel();
            chatModel.setChannelId(AppConstants.CHAT_CHANNEL_ID);
            chatModel.setChatUserName(logged_in_username);
            chatModel.setDate(getCurrentDate);
            //chatModel.setImageUrl(AppConstants.CHAT_USER_IMAGE_URL);
            chatModel.setImageUrl(loggedin_user_url);
            chatModel.setRecieverID(AppConstants.CHAT_USER_ID);
            //chatMembersArray.add(AppConstants.CHAT_USER_ID);
            chatModel.setMsgTimeStamp(getCurrentDateandTIme);
            chatModel.setText(chatMessage);
            chatModel.setChatType(AppConstants.CHAT_TYPE_INDIVIDUAL);
            chatModel.setSenderId(senderId);
            //chatModel.setSelf(true);

            // refreshChatBoard(chatModel);
            postRef.push().setValue(chatModel);

            //Refreshing the message time stamp
            refreshTimeStamp(getCurrentDate,AppConstants.CHAT_TYPE);

            getUserOnlineStatus();

           // if(!isRecipientonline){
                //API call to Send Push Notification

            //}
        }else if(AppConstants.CHAT_TYPE != null &&
                AppConstants.CHAT_TYPE.equalsIgnoreCase(AppConstants.CHAT_TYPE_GROUP)){

            //Building the chat message model
            chatModel = new ChatModel();
            chatModel.setChannelId(AppConstants.CHAT_CHANNEL_ID);
            chatModel.setChatUserName(logged_in_username);
            chatModel.setDate(getCurrentDate);
            //chatModel.setImageUrl(AppConstants.CHAT_USER_IMAGE_URL);
            chatModel.setImageUrl(loggedin_user_url);
            chatModel.setRecieverID(AppConstants.CHAT_USER_ID);
            //chatMembersArray.add(AppConstants.CHAT_USER_ID);
            chatModel.setMsgTimeStamp(getCurrentDateandTIme);
            chatModel.setText(chatMessage);
            chatModel.setChatType(AppConstants.CHAT_TYPE_GROUP);
            chatModel.setSenderId(senderId);
            //chatModel.setSelf(true);
            // refreshChatBoard(chatModel);
            postRef.push().setValue(chatModel);

            //Refreshing the message time stamp
            refreshTimeStamp(getCurrentDate,AppConstants.CHAT_TYPE);

            if(isGroupMessageSentFirstTime){
                isGroupMessageSentFirstTime = false;
                //initializeChatView();
                fetchValue();
            }

            getUserOffLineStatusGroupChat();

           /* //Calling the send Notification API after checking the Offline users Array Size
            //if(groupChatoffLineUsersArray.size() > 0){

            String recieverIds = buildJsonArraystring(groupChatoffLineUsersArray);

                //API call to Send Push Notification
                sendNotification(chatMessage, senderId,
                        recieverIds,
                        AppConstants.CHAT_USER_ID,
                        AppConstants.CHAT_TYPE_GROUP,
                        AppConstants.CHAT_CHANNEL_ID, logged_in_username);

            //}*/
        }
    }

    /**
     * Function to refresh the conversation time stamp
     * @param timeStamp Current time stamp
     */

    public void refreshTimeStamp(int timeStamp, String chatType){

        if(chatType != null && chatType.equalsIgnoreCase("individual")){
            //Initializing the firebase client
            Firebase.setAndroidContext(ChatActivity.this);
            Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
            postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(senderId).child(AppConstants.CHAT_CHANNEL_ID);
            // refreshChatBoard(chatModel);
            postRef.child("date").setValue(timeStamp);
        }else if(chatType != null && chatType.equalsIgnoreCase("group")){
            //Initializing the firebase client
            Firebase.setAndroidContext(ChatActivity.this);
            Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
            postRef = postRef.child(AppConstants.GROUP_NODE).child(AppConstants.CHAT_USER_ID);
            postRef.child("date").setValue(timeStamp);
        }
    }


    /**
     * Function to build the String from all the participants ids
     * @param Recevier_ids
     * @return
     */
    public String buildJsonArray(String Recevier_ids){
        JSONArray recieverIds = null;
        try{
            recieverIds = new JSONArray();
            recieverIds.put(Recevier_ids);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return recieverIds.toString();

    }


    /**
     * Function to build the String from all the participants ids
     * @param participants
     * @return
     */
    public String buildJsonArraystring(ArrayList<String> participants){
        JSONArray recieverIds = null;

        try{
            recieverIds = new JSONArray();
            if(participants.size() > 0){
                for(int i = 0; i < participants.size(); i++){
                    recieverIds.put(participants.get(i));
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return recieverIds.toString();
    }


    /**
     * Function responsible for sending push notification
     */
    public void sendNotification(String message, String sender_id,
                              String receiver_ids,String group_id,
                              String chat_type, String channel_id,
                                 String senderName) {
        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(message,sender_id,receiver_ids,
                group_id,chat_type,channel_id, senderName);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                chatMessage = null;
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                Log.d("Response", response.toString());
                chatMessage = null;

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String message, String sender_id,
                                                 String receiver_ids,String group_id,
                                                 String chat_type, String channel_id,
                                                 String senderName) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "send_chat_notification").newBuilder();
        urlBuilder.addQueryParameter("message", message);
        urlBuilder.addQueryParameter("sender_id", sender_id);
        urlBuilder.addQueryParameter("receiver_ids", receiver_ids);
        urlBuilder.addQueryParameter("group_id", group_id);
        urlBuilder.addQueryParameter("chat_type", chat_type);
        urlBuilder.addQueryParameter("channel_id", channel_id);
        urlBuilder.addQueryParameter("device_type", Constants.DEVICE_TYPE);
        urlBuilder.addQueryParameter("sender_name", senderName);

        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to refresh the chat view
     */
    public void refreshChatBoard(){
        //chatArray.add(chatModel);
        chatAdapter.notifyDataSetChanged();
        //Scrolling the list view to the bottom
        scrollMyListViewToBottom();
    }


    /**
     * Get Current Date
     * @return
     */
    public static int getCurrentDate(){
        Long tsLong = System.currentTimeMillis()/1000;
        //String ts = tsLong.toString();
        return tsLong.intValue();
    }

    /**
     * Get Current Date and time
     * @return
     */
    public static String getCurrentDateAndTIme(){
        TimeZone utc = TimeZone.getTimeZone("UTC");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        dateFormatter.setTimeZone(utc);
        //dateFormatter.setLenient(false);
        Date today = new Date();
        String current_time = dateFormatter.format(today);
        return current_time;
    }


    /**
     * Fetching all the values from a channel
     */
    public void fetchValue() {
        Firebase.setAndroidContext(ChatActivity.this);
        final Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE).child("message");

        postRef.child(AppConstants.CHAT_CHANNEL_ID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                ChatModel post = null;

                //Removing all the values from the chat array
                chatArray.clear();

                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    String message_sender_id = (String) child.child("senderId").getValue();
                    String message_timeStamp = (String) child.child("msgTimeStamp").getValue();
                    String convertedTime = formatDateandTime(message_timeStamp);

                    post = child.getValue(ChatModel.class);
                    if(message_sender_id.equalsIgnoreCase(senderId)){
                        post.setSelf(true);
                        post.setSenderImageUrl(loggedin_user_url);
                    }else{
                        post.setSelf(false);
                    }

                    //Setting the converted time stamp
                    post.setMsgTimeStamp(convertedTime);

                    //Adding the model data into the array
                    chatArray.add(post);

                    //Getting the message Array size
                    MESSAGE_READ_COUNT = chatArray.size();
                }

                /*if(AppConstants.CHAT_TYPE != null &&
                        AppConstants.CHAT_TYPE.equalsIgnoreCase(AppConstants.CHAT_TYPE_INDIVIDUAL)){
                    //Updating the Read Message Count
                    updatereadMessageCountInstantly();
                }else {
                    updatereadMessageCountInstantlyInGroupMessages();
                }*/

                if(post != null) {
                    if (chatArray == null) {
                        initializeChatView();
                    }
                    refreshChatBoard();
                }


                /*if(!isScreenOpenedForFirstTime){
                    if(AppConstants.CHAT_TYPE != null &&
                            AppConstants.CHAT_TYPE.equalsIgnoreCase(AppConstants.CHAT_TYPE_INDIVIDUAL)){
                        //Fetching the last message from the node
                        fetchLastMessage(AppConstants.CHAT_CHANNEL_ID);
                    }
                }

                isScreenOpenedForFirstTime = false;*/
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    /**
     * Fetching conversation data from firebase
     */
    public void fetchLastMessage(String channedId) {
        //Handling the loader
        // handleLoader(true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_NODE);

        //callback to handle the user conversation data
        postRef.child(channedId).limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String message_sender_id = (String) postSnapshot.child("senderId").getValue();
                    String message_receiver_id = (String) postSnapshot.child("recieverID").getValue();

                    if(message_sender_id != null &&
                            message_sender_id.equalsIgnoreCase(senderId)){
                        if(!isRecipientonline){
                            getAndUpdateTheBadgeCount();
                        }
                    }/*else if(message_receiver_id != null &&
                            message_receiver_id.equalsIgnoreCase(senderId)){
                        updateBadgeCountForMe();
                    }*/
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

    }


    /**
     * Fetching conversation data from firebase
     */
    public void fetchBadgeCountGroup(String channedId) {

        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_NODE);

        //callback to handle the user conversation data
        postRef.child(channedId).limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String message_sender_id = (String) postSnapshot.child("senderId").getValue();
                    String message_receiver_id = (String) postSnapshot.child("recieverID").getValue();

                    if(message_sender_id != null &&
                            message_sender_id.equalsIgnoreCase(senderId)){
                        //Adding all the user ids into the  TeammateListAdapter.userIds array
                        for (int i = 0; i < AppConstants.groupParticipantsIds.size(); i++){
                            getAndUpdateTheBadgeCountInGroup(AppConstants.groupParticipantsIds.get(i));
                        }
                    }else {
                        updateBadgeCountForMeInGroup();
                    }

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

    }


/*    public void fetchValue(){

        Firebase.setAndroidContext(ChatActivity.this);
        final Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE).child("message");

        postRef.child(AppConstants.CHAT_CHANNEL_ID).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                ChatModel post = null;

                //Removing all the values from the chat array
                //chatArray.clear();

                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    String message_sender_id = (String) child.child("senderId").getValue();
                    String message_timeStamp = (String) child.child("msgTimeStamp").getValue();
                    String convertedTime = formatDateandTime(message_timeStamp);

                    post = child.getValue(ChatModel.class);
                    if(message_sender_id.equalsIgnoreCase(senderId)){
                        post.setSelf(true);
                        post.setSenderImageUrl(loggedin_user_url);
                    }else{
                        post.setSelf(false);
                    }

                    //Setting the converted time stamp
                    post.setMsgTimeStamp(convertedTime);

                    //Adding the model data into the array
                    chatArray.add(post);

                    //Getting the message Array size
                    MESSAGE_READ_COUNT = chatArray.size();
                }

                if(AppConstants.CHAT_TYPE != null &&
                        AppConstants.CHAT_TYPE.equalsIgnoreCase(AppConstants.CHAT_TYPE_INDIVIDUAL)){
                    //Updating the Read Message Count
                    updatereadMessageCountInstantly();
                }

                if(post != null) {
                    if (chatArray == null) {
                        initializeChatView();
                    }
                    refreshChatBoard();
                }

                getAndUpdateTheBadgeCount();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }*/


    /**
     * Get the converted UTC date
     * @param formatDate
     * @return
     */
    public String formatDateandTime(String formatDate){

        String convertedDate = null;
        try{
            TimeZone utc = TimeZone.getTimeZone("UTC");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.US);
            inputFormat.setTimeZone(utc);
            DateFormat outputFormat = new SimpleDateFormat("h:mm aa",
                    Locale.US);
            //outputFormat.setTimeZone(utc);
            Date date = inputFormat.parse(formatDate);
            convertedDate = outputFormat.format(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return convertedDate;
    }

    /**
     * Getting user ids
     */
    public void getSenderData(){
        pref = PreferenceManager.getDefaultSharedPreferences(ChatActivity.this);
        senderId = pref.getString("loginuser_id", null);
        logged_in_username = pref.getString("USERNAME", null);
        loggedin_user_url = pref.getString("image", null);
    }

    /**
     * Function to scroll the list view
     */
    public static void scrollMyListViewToBottom() {
        lv_Chat.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                lv_Chat.setSelection(chatAdapter.getCount() - 1);
            }
        });
    }

    /**
     * Function responsible for refreshing the message badge count
     */
    public void refreshBadgeCount(){
        //Initializing the model
        FirebaseBadgeCountModel firebaseModel = new FirebaseBadgeCountModel();
        firebaseModel.setBadgeCount(0);

        //Refreshing the badge count for the reciver
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child("messageBadgeCount");
        postRef.child(AppConstants.CHAT_USER_ID).setValue(firebaseModel);

        //Refreshing the badge count for the sender
        Firebase postRef_sender = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef_sender = postRef_sender.child("messageBadgeCount");
        postRef_sender.child(senderId).setValue(firebaseModel);
    }


    /**
     * Function responsible for refreshing the message badge count
     */
    public void refreshBadgeCount(String userId){
        //Initializing the model
        FirebaseBadgeCountModel firebaseModel = new FirebaseBadgeCountModel();
        firebaseModel.setBadgeCount(0);

        //Refreshing the badge count for the reciver
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child("messageBadgeCount");
        postRef.child(userId).setValue(firebaseModel);
    }

    /**
     * Function responsible for refreshing the message badge count
     */
    public void refreshBadgeCountForMeInGroupChat(){
        //Initializing the model
        FirebaseBadgeCountModel firebaseModel = new FirebaseBadgeCountModel();
        firebaseModel.setBadgeCount(1);

        //Refreshing the badge count for the sender
        Firebase postRef_sender = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef_sender = postRef_sender.child("messageBadgeCount");
        postRef_sender.child(senderId).setValue(firebaseModel);
    }



    /**
     * Fetching conversation data from firebase
     */
    public void fetchReadMessageCountGroup() {
        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef.child(AppConstants.CONVERSATION_NODE).child(AppConstants.CHAT_USER_ID).child(AppConstants.CHAT_CHANNEL_ID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                 long readMessageCount = (long) dataSnapshot.child("readMessageCount").getValue();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

    }

    /**
     * Fetching conversation data from firebase
     */
    public void getUnreadMessageCount(String channedId, final long readMessageCount) {
        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_NODE);

        //callback to handle the user conversation data
        postRef.child(channedId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                int getCount = (int) dataSnapshot.getChildrenCount();
                long unreadMessageCount = getCount - readMessageCount;
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    /**
     * Updating the Badge Count
     * @param badgeCount
     */
    public void upDateBadgeCountForRecipient(long badgeCount){
        //Initializing the model
        FirebaseBadgeCountModel firebaseModel = new FirebaseBadgeCountModel();
        firebaseModel.setBadgeCount(badgeCount);

        //Refreshing the badge count for the reciver
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child("messageBadgeCount");
        postRef.child(AppConstants.CHAT_USER_ID).setValue(firebaseModel);

        //TabFragment.getChatMessageBadgeCount();
    }


    /**
     * Updating the Badge Count
     * @param badgeCount
     */
    public void upDateBadgeCount(long badgeCount){
        //Initializing the model
        FirebaseBadgeCountModel firebaseModel = new FirebaseBadgeCountModel();
        firebaseModel.setBadgeCount(badgeCount);

        //Refreshing the badge count for the reciver
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child("messageBadgeCount");
        postRef.child(senderId).setValue(firebaseModel);

        TabFragment.getChatMessageBadgeCount();
    }

    /**
     * Updating the Badge Count
     * @param badgeCount
     */
    public void upDateBadgeCountGroupUsers(long badgeCount, String userId){
        //Initializing the model
        FirebaseBadgeCountModel firebaseModel = new FirebaseBadgeCountModel();
        firebaseModel.setBadgeCount(badgeCount);

        //Refreshing the badge count for the reciver
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child("messageBadgeCount");
        postRef.child(userId).setValue(firebaseModel);

        TabFragment.getChatMessageBadgeCount();
    }

    /**
     * Updating the Badge Count
     * @param badgeCount
     */
    public void upDateBadgeCountForMe(long badgeCount){
        //Initializing the model
        FirebaseBadgeCountModel firebaseModel = new FirebaseBadgeCountModel();
        firebaseModel.setBadgeCount(badgeCount);

        //Refreshing the badge count for the reciver
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child("messageBadgeCount");
        postRef.child(senderId).setValue(firebaseModel);
    }


    /**
     * Fetching conversation data from firebase
     */
    public void getAndUpdateTheBadgeCount() {
        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_BADGE_COUNT_NODE);

        //callback to handle the user conversation data
        postRef.child(AppConstants.CHAT_USER_ID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                messageBadgeCount = (long) dataSnapshot.child("badgeCount").getValue();
                //Updating the badge count for the receiver
                upDateBadgeCountForRecipient(messageBadgeCount + 1);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


    /**
     * Fetching conversation data from firebase
     */
    public void getAndUpdateTheBadgeCountInGroup(final String userId) {
        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_BADGE_COUNT_NODE);

        //callback to handle the user conversation data
        postRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                messageBadgeCount = (long) dataSnapshot.child("badgeCount").getValue();
                //Updating the badge count for the receiver
                upDateBadgeCountGroupUsers(messageBadgeCount + 1, userId);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    /**
     * Fetching conversation data from firebase
     */
    public void updateBadgeCountForMe(/*final long readMessageCount*/) {
        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_BADGE_COUNT_NODE);

        //callback to handle the user conversation data
        postRef.child(senderId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                messageBadgeCount = (long) dataSnapshot.child("badgeCount").getValue();
                //Updating the badge count for the receiver
                upDateBadgeCountForMe(messageBadgeCount - 1);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


    /**
     * Fetching conversation data from firebase
     */
    public void updateBadgeCountForMeInGroup() {
        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_BADGE_COUNT_NODE);

        //callback to handle the user conversation data
        postRef.child(senderId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                messageBadgeCount = (long) dataSnapshot.child("badgeCount").getValue();
                //Updating the badge count for the receiver
                upDateBadgeCountForMe(messageBadgeCount - 1);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


    /**
     * Fetching conversation data from firebase
     */
    public void updateBadgeCountForUnreadMessages(final long readMessageCount) {
        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_BADGE_COUNT_NODE);

        //callback to handle the user conversation data
        postRef.child(senderId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                //Getting the message count
                messageBadgeCount = (long) dataSnapshot.child("badgeCount").getValue();

                SplashScreen.messageBadgeCount = messageBadgeCount - readMessageCount;

                //Updating the badge count for the receiver
                upDateBadgeCount(SplashScreen.messageBadgeCount);

                //Releasing the unread message count value
                AppConstants.CHAT_THREAD_UNREAD_MESSAGE_COUNT = 0;
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }




    @Override
    protected void onResume() {
        super.onResume();

        if(AppConstants.isAnyThingUpdated){
            AppConstants.isAnyThingUpdated = false;
            return;
        }

        /**
         * Creating the channel Id and storing the user data in firebase
         */
        if(TeammateListAdapter.isChannelCreated){
            int getCurrentDate = getCurrentDate();
            //Getting logged in user id
            getSenderData();
            if(AppConstants.USER_SELECTED_CHAT_TYPE != null &&
                    AppConstants.USER_SELECTED_CHAT_TYPE.equalsIgnoreCase("individual")){
                //Initializing the firebase client
                Firebase.setAndroidContext(ChatActivity.this);
                Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
                postRef = postRef.child("conversation").child(senderId).child(AppConstants.CHAT_CHANNEL_ID);

                //Setting data for reliever user
                ChatConversationModel chatMoodel = new ChatConversationModel();
                chatMoodel.setSenderId(senderId);
                chatMoodel.setChannelId(AppConstants.CHAT_CHANNEL_ID);
                chatMoodel.setChatUserName(AppConstants.CHAT_USER_NAME);
                chatMoodel.setDate(getCurrentDate);
                chatMoodel.setImageUrl(AppConstants.CHAT_USER_IMAGE_URL);
                chatMoodel.setRecieverID(AppConstants.CHAT_USER_ID);
                chatMoodel.setReadMessageCount(0);
                chatMoodel.setOnline(true);
                postRef.setValue(chatMoodel);

                //Setting data for sender user
                ChatConversationModel chatMoodel_reciever = new ChatConversationModel();
                chatMoodel_reciever.setSenderId(AppConstants.CHAT_USER_ID);
                chatMoodel_reciever.setChannelId(AppConstants.CHAT_CHANNEL_ID);
                chatMoodel_reciever.setChatUserName(logged_in_username);
                chatMoodel_reciever.setDate(getCurrentDate);
                chatMoodel_reciever.setImageUrl(loggedin_user_url);
                chatMoodel_reciever.setRecieverID(senderId);
                chatMoodel.setReadMessageCount(0);
                chatMoodel.setOnline(false);
                Firebase postRef1 = new Firebase(EndPoints.BASE_URL_FIREBASE);
                postRef1 = postRef1.child("conversation").child(AppConstants.CHAT_USER_ID).child(AppConstants.CHAT_CHANNEL_ID);
                postRef1.setValue(chatMoodel_reciever);
                TeammateListAdapter.isChannelCreated = false;
                AppConstants.isChannedCreated = true;

                //Refreshing the badge Count
                refreshBadgeCount();

            }else if(AppConstants.USER_SELECTED_CHAT_TYPE != null &&
                    AppConstants.USER_SELECTED_CHAT_TYPE.equalsIgnoreCase("group") &&
                    !AppConstants.isGroupUpdated){
                int keepTheCount = 0;

                DatabaseAdapter db = new DatabaseAdapter(ChatActivity.this);
                ArrayList<FirebaseGroupChatParticipantModel> allChatUserData =
                        new ArrayList<>();
                ArrayList<String> participantsIds = new ArrayList<String>();
                participantsIds.add(senderId);

                //Getting all the teammates id and saving it in the firebase
                if(TeammateListAdapter.userIds.size() > 0){
                    for(int i = 0; i < TeammateListAdapter.userIds.size(); i++){
                        FirebaseGroupChatParticipantModel firebaseCHatModel =
                                new FirebaseGroupChatParticipantModel();
                        firebaseCHatModel.setUser_id(TeammateListAdapter.userIds.get(i));
                        participantsIds.add(TeammateListAdapter.userIds.get(i));
                        firebaseCHatModel.setUsername(db.getcurrentfriendUserName(TeammateListAdapter.userIds.get(i)));
                        firebaseCHatModel.setImage(db.getcurrentfriendProfileImage(TeammateListAdapter.userIds.get(i)));
                        allChatUserData.add(firebaseCHatModel);
                    }
                }

                keepTheCount = keepTheCount + 1;

                if(keepTheCount == 1){

                    //Function responsible for creating the channel
                    createGroupChannelId(AppConstants.NEW_GROUP, AppConstants.CHAT_CHANNEL_ID,
                            allChatUserData, participantsIds);
                }


            }
        }

        if(AppConstants.isGroupUpdated){

            DatabaseAdapter db = new DatabaseAdapter(ChatActivity.this);
            //Initializing the arrays
            arr_added_user_group = new ArrayList<>();
            arr_removed_user_group = new ArrayList<>();

            for(int i = 0; i < TeammateListAdapter.userIds.size(); i++){
                String userId = TeammateListAdapter.userIds.get(i);

                if(!TeammateListAdapter.userIds_duplicate.contains(userId)){
                    arr_added_user_group.add(userId);
                }
            }

            for(int i = 0; i < TeammateListAdapter.userIds_duplicate.size(); i++){
                String userId = TeammateListAdapter.userIds_duplicate.get(i);

                if(!TeammateListAdapter.userIds.contains(userId)){
                    arr_removed_user_group.add(userId);
                }
            }

            //Initializing the firebase
            Firebase.setAndroidContext(ChatActivity.this);
            Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
            postRef = postRef.child(AppConstants.GROUP_NODE).child(AppConstants.CHAT_USER_ID).child("participants");

            ArrayList<FirebaseGroupChatParticipantModel> allChatUserData =
                    new ArrayList<>();

            if(AppConstants.groupParticipantsIds != null &&
                    AppConstants.groupParticipantsIds.size() > 0){
                AppConstants.groupParticipantsIds.clear();
            }

            //Getting and seting all the users in firebase
            for(int i = 0; i < TeammateListAdapter.userIds.size(); i++){
                FirebaseGroupChatParticipantModel firebaseCHatModel =
                        new FirebaseGroupChatParticipantModel();
                firebaseCHatModel.setUser_id(TeammateListAdapter.userIds.get(i));

                //adding the refreshed the team mates list
                AppConstants.groupParticipantsIds.add(TeammateListAdapter.userIds.get(i));
                firebaseCHatModel.setUsername(db.getcurrentfriendUserName(TeammateListAdapter.userIds.get(i)));
                firebaseCHatModel.setImage(db.getcurrentfriendProfileImage(TeammateListAdapter.userIds.get(i)));
                allChatUserData.add(firebaseCHatModel);
            }

            //Removing all the participants
            postRef.removeValue();

            //Reffeshing the firebase group participants
            postRef.setValue(allChatUserData);


            Firebase postRef_unreadmessage_count = new Firebase(EndPoints.BASE_URL_FIREBASE);
            postRef_unreadmessage_count = postRef_unreadmessage_count.child("groupmessagecount").child(AppConstants.CHAT_USER_ID);
            //Updating all the added users in the firebase
            for(int i = 0; i < arr_added_user_group.size(); i++){
                Firebase.setAndroidContext(ChatActivity.this);
                Firebase postRef_adduser = new Firebase(EndPoints.BASE_URL_FIREBASE);
                postRef_adduser = postRef_adduser.child(AppConstants.CONVERSATION_NODE).
                        child(arr_added_user_group.get(i)).child("groupIds");
                postRef_adduser.push().setValue(AppConstants.CHAT_USER_ID);

                //Refreshing the firebase table after adding members to the group
                String playerId = arr_added_user_group.get(i);

                //creating and adding read message count to the model
                GroupChatReadMessageCount unreadMessages = new GroupChatReadMessageCount();

                //Setting the default count as 0, as they have not involved in any kink of messages
                unreadMessages.setReadMessageCount(0);

                //Pushing and setting the values in firebase
                postRef_unreadmessage_count.child(playerId).setValue(unreadMessages);
            }

            for(int i = 0; i < arr_removed_user_group.size(); i++){
                //Removing the user from the group
                removeGroupUser(arr_removed_user_group.get(i));
            }

            //Setting the updated text in the text view
            tvTitle.setText(AppConstants.CHAT_USER_NAME + " + " + String.valueOf(TeammateListAdapter.userIds.size()));

            //Making the variables false
            AppConstants.isGroupUpdated = false;
            AppConstants.isUserGointToUpdateTheGroup = false;

            //Initializing all the values again to get the actual array
            if(!AppConstants.isGroupUpdated){
                //initializing the user ids array list
                TeammateListAdapter.userIds = new ArrayList<>();

                TeammateListAdapter.userIds_duplicate = new ArrayList<>();

                //Adding all the user ids into the  TeammateListAdapter.userIds array
                for (int i = 0; i < AppConstants.groupParticipantsIds.size(); i++){
                    TeammateListAdapter.userIds.add(AppConstants.groupParticipantsIds.get(i));
                    TeammateListAdapter.userIds_duplicate.add(AppConstants.groupParticipantsIds.get(i));
                }
            }
        }


    }




    /**
     * Function responsible for deleting a group
     * @param chatUserId Group Id
     */

/*    public void removeGroupUser(final String chatUserId){
        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);

        postRef_delete_group = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef_delete_group = postRef_delete_group.child(AppConstants.CONVERSATION_NODE).
                child(chatUserId).child("groupIds");

       // postRef_delete_group.orderByValue().equalTo(AppConstants.CHAT_USER_ID);

       // postRef_delete_group.child(AppConstants.CHAT_USER_ID).setValue(null);

        // Attach a listener to read the data at our posts reference
        postRef_delete_group.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                for(com.firebase.client.DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    String message_group_id_key = (String) postSnapshot.getKey();
                    String message_group_id = (String) postSnapshot.getValue();
                    if(message_group_id.trim().equalsIgnoreCase(AppConstants.CHAT_USER_ID.trim())){
                       // postRef_delete_group.child(message_group_id_key).removeValue();
                        postRef_delete_group.child(message_group_id_key).setValue(null);
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }*/


    /**
     * Function responsible for deleting a group
     * @param chatUserId Group Id
     */

    public void removeGroupUser(final String chatUserId){
        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);

        postRef_delete_group = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef_delete_group = postRef_delete_group.child(AppConstants.CONVERSATION_NODE).
                child(chatUserId).child("groupIds");

        // Attach a listener to read the data at our posts reference
        /*postRef_delete_group.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                for(com.firebase.client.DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    String message_group_id_key = (String) postSnapshot.getKey();
                    postRef_delete_group.child(message_group_id_key).removeValue();
                    *//*String message_group_id = (String) postSnapshot.getValue();
                    if(message_group_id.trim().equalsIgnoreCase(AppConstants.CHAT_USER_ID.trim())){
                        // postRef_delete_group.child(message_group_id_key).removeValue();
                        postRef_delete_group.child(message_group_id_key).setValue(null);
                    }*//*
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });*/

        postRef_delete_group.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d("Data", dataSnapshot.toString());
                        String key = dataSnapshot.getKey();

                        for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                            String clubkey = (String) childSnapshot.getKey();
                            String message_group_id = (String) childSnapshot.getValue();

                            if(message_group_id.trim().equalsIgnoreCase(AppConstants.CHAT_USER_ID.trim())){
                                // postRef_delete_group.child(message_group_id_key).removeValue();
                                postRef_delete_group.child(clubkey).removeValue();
                            }
                        }
                    }
                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }

    });
    }






    /**
     * Function responsible for creating the channel ID and saving the group info in firebase
     * @param groupName The Group/ Team name
     * @param groupId The Group/ Team id
     * @param groupModel The Group/ Team participant Model
     */



      public void createGroupChannelId(String groupName,
                                       String groupId,
                                       ArrayList<FirebaseGroupChatParticipantModel> groupModel,
                                       ArrayList<String> player_ids){
        //String channel Id
        String channel_id = ApplicationUtility.getChannelId();
        int getCurrentDate = ChatActivity.getCurrentDate();

        //Initializing the firebase Client
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef = postRef.child(AppConstants.GROUP_NODE).child(groupId);

        //Adding the
        ArrayList<String> onLineUserArray = new ArrayList<>();
        for(int i = 0; i < player_ids.size();  i++){
            onLineUserArray.add(player_ids.get(i));
            //Inserting the badge count for the group members
            refreshBadgeCount(player_ids.get(i));
        }

        if(onLineUserArray.contains(senderId)){
            onLineUserArray.remove(senderId);
        }

        //Creating the model to push firebase
        FirebaseGroupDetailsModel groupDetailsModel = new FirebaseGroupDetailsModel();

        //Setting the data in model
        groupDetailsModel.setChannelId(channel_id);
        groupDetailsModel.setDate(getCurrentDate);
        groupDetailsModel.setGroupId(groupId);
        groupDetailsModel.setGroupName(groupName);
        groupDetailsModel.setImageUrl("");
        groupDetailsModel.setParticipants(groupModel);
        groupDetailsModel.setParticipants(groupModel);
        groupDetailsModel.setReadMessageCount(0);
        groupDetailsModel.setOffLineUsers(onLineUserArray);

        //Pushing the model to the firebase
        postRef.setValue(groupDetailsModel);

        Firebase postRef_unreadmessage_count = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef_unreadmessage_count = postRef_unreadmessage_count.child("groupmessagecount").child(groupId);

        //Saving the data in the all the participant
        for(int i = 0; i < player_ids.size();  i++){
            //Initializing the firebase Client
            Firebase.setAndroidContext(ChatActivity.this);
            Firebase postRef_teammates = new Firebase(EndPoints.BASE_URL_FIREBASE);
            postRef_teammates = postRef_teammates.child(AppConstants.CONVERSATION_NODE).
                    child(player_ids.get(i)).child("groupIds");
            AppConstants.groupParticipantsIds.add(player_ids.get(i));
            postRef_teammates.push().setValue(groupId);
            String playerId = player_ids.get(i);

            //postRef_unreadmessage_count.child(playerId);
            GroupChatReadMessageCount unreadMessages = new GroupChatReadMessageCount();
            unreadMessages.setReadMessageCount(0);
            postRef_unreadmessage_count.child(playerId).setValue(unreadMessages);
        }

        //Inserting the badge count for the group creator
        //refreshBadgeCountForMeInGroupChat();

        if(AppConstants.groupParticipantsIds.contains(senderId)){
            AppConstants.groupParticipantsIds.remove(senderId);
        }

        TeamMates.isGroupChaneelIdCreated = false;

        //Asigning the values to the chat
        assignChatValues(channel_id, groupName, "", groupId);
    }

    /**
     * fUNCTION TO ASSIGN CHAT VALUES
     * @param channelId Chat CHannel ID
     * @param groupName Chat CHannel Id
     * @param imageUrl  Chat User Image URL
     * @param groupId   Chat Group Id
     */
    public void assignChatValues(String channelId, String groupName,
                                 String imageUrl, String groupId){
        AppConstants.CHAT_CHANNEL_ID = channelId;
        AppConstants.CHAT_USER_NAME = groupName;
        AppConstants.CHAT_USER_IMAGE_URL = imageUrl;
        AppConstants.CHAT_USER_ID = groupId;
        AppConstants.CHAT_TYPE = "group";
        tvTitle.setText(AppConstants.CHAT_USER_NAME + " + " +
                String.valueOf( AppConstants.groupParticipantsIds.size()));

        isGroupMessageSentFirstTime = true;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(ChatActivity.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }


    //Function to hide the soft keyboard
    public void hodeKeyboard(){
        //getWindow().setSoftInputMode(
                //WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //getWindow().setSoftInputMode(
               // WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }


    /**
     * Function to display the edit group name dialog
     */
    public void displayEditTeamDialog(){
        mMaterialDialog = new MaterialDialog(ChatActivity.this);
        View view = LayoutInflater.from(ChatActivity.this)
                .inflate(R.layout.layout_edit_team_name,
                        null);

        final EditText stTeamname = (EditText) view.findViewById(R.id.etteamname);
        stTeamname.setText(AppConstants.CHAT_USER_NAME);
        TextView btnSave = (TextView) view.findViewById(R.id.tv_save_teamnae);
        TextView btnCancel = (TextView) view.findViewById(R.id.tv_cancel_teammates);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                String groupname = stTeamname.getText().toString().trim();

                if(TextUtils.isEmpty(groupname)){
                    Toast.makeText(ChatActivity.this, "Please enter the group name", Toast.LENGTH_LONG).show();
                }else{
                    //Initializing the firebase client
                    Firebase.setAndroidContext(ChatActivity.this);
                    Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

                    //Updating the group name
                    postRef = postRef.child("group").child(AppConstants.CHAT_USER_ID);
                    postRef.child("groupName").setValue(groupname);

                    //Refreshing the team name
                    tvTitle.setText(groupname);

                    //Displaying the Chat members count
                    if(AppConstants.groupParticipantsIds.size() == 0){
                        tvTitle.setText(groupname);
                    }else{
                        int player_count = AppConstants.groupParticipantsIds.size();
                        tvTitle.setText(groupname + " + "+String.valueOf(player_count));
                    }

                    isGroupNameUpdated = true;
                }
            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();

    }


    /**
     * Function responsibel for upading the
     * chat message read count Update the chat Read message count
     */

    public void updatereadMessageCountInstantly(){
        //Initializing the firebase client
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Updating the group name
        postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(senderId).child(AppConstants.CHAT_CHANNEL_ID);
        postRef.child("readMessageCount").setValue(MESSAGE_READ_COUNT);

        //Handling the device back button presses
       // handleBackButton();
    }


    /**
     * Function responsibel for upading the
     * chat message read count Update the chat Read message count
     */

    public void updatereadMessageCountInstantlyInGroupMessages(){
        //Initializing the firebase client
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Updating the group name
        postRef = postRef.child("groupmessagecount").child(AppConstants.CHAT_USER_ID).child(senderId);
        postRef.child("readMessageCount").setValue(MESSAGE_READ_COUNT);
    }


    /**
     * Function responsibel for upading the
     * chat message read count Update the chat Read message count
     */

    public void updatereadMessageCount(){
        //Initializing the firebase client
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Updating the group name
        postRef = postRef.child(AppConstants.CONVERSATION_NODE).child(senderId).child(AppConstants.CHAT_CHANNEL_ID);
        postRef.child("readMessageCount").setValue(MESSAGE_READ_COUNT);

        //Updating user online status
        updateUserOnlineStatus(false);

        //Handling the device back button presses
        handleBackButton();
    }


    /**
     * Function responsibel for upadting the
     * chat message read count
     * Update the chat Read message count
     *
     * Updating the Group unread message count
     */

    public void updateGroupreadMessageCount(){
        //Initializing the firebase client
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        //Updating the group name
        postRef = postRef.child(AppConstants.GROUP_MESSAGE_READ_COUNT_NODE).
                child(AppConstants.CHAT_USER_ID).child(senderId);
        postRef.child("readMessageCount").setValue(MESSAGE_READ_COUNT);
        removeGroupOnlileUser();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TeammateListAdapter.isChannelCreated = false;
        //Handleing data and page redirection
        if(AppConstants.CHAT_TYPE != null &&
                AppConstants.CHAT_TYPE.equalsIgnoreCase("individual")){
            //Function call to update the chat message read count
            updatereadMessageCount();
        }else if(AppConstants.CHAT_TYPE != null &&
                AppConstants.CHAT_TYPE.equalsIgnoreCase("group")){
            updateGroupreadMessageCount();
        } else{
            //Handling the device back button presses
            handleBackButton();
        }

    }


    /**
     * Function responsible for handling the event while user pressing the device back button
     */
    public void handleBackButton(){
        if(AppConstants.isChatMessageRecieveedInBackground){
            AppConstants.isChatMessageRecieveedInBackground = false;
            if(NavigationDrawerActivity.baseActivity != null){
                NavigationDrawerActivity.baseActivity.finish();
            }
            ChatActivity.this.finish();
            clearArray();
            AppConstants.isUserinChatBoard = false;
            AppConstants.isGropuChatMessage = false;
            //AppConstants.CHAT_CHANNEL_ID = null;
            AppConstants.CHAT_CHANNEL_ID_DUPLICATE = null;
            startActivity(new Intent(ChatActivity.this, SplashScreen.class));
            TeammateListAdapter.userIds = new ArrayList<>();
        }else{
            ChatActivity.this.finish();
            clearArray();
            AppConstants.isUserinChatBoard = false;
            AppConstants.isGropuChatMessage = false;
            //AppConstants.CHAT_CHANNEL_ID = null;
            AppConstants.CHAT_CHANNEL_ID_DUPLICATE = null;
            TeammateListAdapter.userIds = new ArrayList<>();
        }
    }

    /**
     * Function responsible for clearing the array cotent
     */
    public void clearArray(){
        AppConstants.groupParticipantsIds.clear();
        AppConstants.groupParticipantsName.clear();
    }



    /**
     * Fetching conversation data from firebase
     */
    public void fetchGroupValue(String groupId){
        //Handling the loader
        // handleLoader(true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.GROUP_NODE);

        //callback to handle the user conversation data
        postRef.child(groupId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                Log.d("TAG", dataSnapshot.toString());
                ChatConversationModel chatConversationModel = new ChatConversationModel();
                String channelId = (String) dataSnapshot.child("channelId").getValue();
                String recieverID = (String) dataSnapshot.child("groupId").getValue();
                String chatUserName = (String) dataSnapshot.child("groupName").getValue();
                String imageUrl = (String) dataSnapshot.child("imageUrl").getValue();
                AppConstants.CHAT_USER_NAME = chatUserName;
                tvTitle.setText(AppConstants.CHAT_USER_NAME);
                fetchGroupParticipants(recieverID, chatConversationModel);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });

        // handleLoader(false);
    }


    /**
     * Fetching conversation data from firebase
     */
    public void fetchGroupParticipants(String groupId,
                                       final ChatConversationModel chatParticipantyModel){
        //Handling the loader
        // handleLoader(true);

        //Initializing the firebase instance
        Firebase.setAndroidContext(ChatActivity.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.GROUP_NODE);

        //callback to handle the user conversation data
        postRef.child(groupId).child(AppConstants.CHAT_MESSAGE_PARTICIPANTS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                ArrayList<FirebaseGroupChatParticipantModel> participants_array =
                        new ArrayList<FirebaseGroupChatParticipantModel>();
                AppConstants.groupParticipantsName = new ArrayList<String>();
                AppConstants.groupParticipantsIds = new ArrayList<String>();
                TeammateListAdapter.userIds = new ArrayList<String>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Log.d("TAG", dataSnapshot.toString());
                    FirebaseGroupChatParticipantModel chatConversationModel =
                            new FirebaseGroupChatParticipantModel();
                    String username = (String) postSnapshot.child("username").getValue();
                    String user_id = (String) postSnapshot.child("user_id").getValue();
                    String image = (String) postSnapshot.child("image").getValue();

                    //Adding all the usernames into the array
                    AppConstants.groupParticipantsName.add(username);

                    //Adding all the user Ids into the array
                    AppConstants.groupParticipantsIds.add(user_id);

                    if(!AppConstants.isGroupUpdated){
                        //Adding all the user Ids into the array for the group member edit funtion
                        TeammateListAdapter.userIds.add(user_id);
                    }
                }
                if(AppConstants.CHAT_TYPE != null &&
                        AppConstants.CHAT_TYPE.equalsIgnoreCase("group")){
                    int player_count = AppConstants.groupParticipantsName.size();
                    tvTitle.setText(AppConstants.CHAT_USER_NAME + " + "+String.valueOf(AppConstants.groupParticipantsName.size()));
                }

                //Fetching all the message for channel
                fetchValue();
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }


    /**
     * Getting the user online status
     */
    public void getUserOnlineStatus(){
        Firebase.setAndroidContext(ChatActivity.this);
        final Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef.child("conversation").child(senderId).
                child(AppConstants.CHAT_CHANNEL_ID).child("isOnline").
                addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String getKey = snapshot.getKey();
                if(getKey.contains("isOnline")){
                    isRecipientonline = (boolean) snapshot.getValue();
                    Log.d("Online status", String.valueOf(isRecipientonline));
                    if(!isRecipientonline){
                        sendNotification(chatMessage, senderId,
                                buildJsonArray(AppConstants.CHAT_USER_ID),"",
                                AppConstants.CHAT_TYPE_INDIVIDUAL,
                                AppConstants.CHAT_CHANNEL_ID, logged_in_username);
                    }
                }
            }
            @Override
            public void onCancelled(FirebaseError error) {
                Log.d("Online status", String.valueOf(isRecipientonline));
            }
        });
    }

    /**
     * Getting the user online status
     */
    public void getUserOnlineStatusGroupChat(){
        Firebase.setAndroidContext(ChatActivity.this);
        final Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef.child("group").child(AppConstants.CHAT_USER_ID).child("offLineUsers").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                    if(groupChatoffLineUsersArray.size() > 0){
                        groupChatoffLineUsersArray.clear();
                    }
                    groupChatoffLineUsersArray = (ArrayList<String>) snapshot.getValue();
            }
            @Override
            public void onCancelled(FirebaseError error) {

            }
        });
    }

    /**
     * Getting the tabel existance from firebase
     */
    public void getFirebaseTableExistanceStatus(){
        Firebase.setAndroidContext(ChatActivity.this);
        final Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.hasChild("group")) {
                    isTableExist = true;
                    getFirebaseTableExistanceStatusGroupId();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    /**
     * Getting the tabel existance from firebase
     */
    public void getFirebaseTableExistanceStatusGroupId(){
        Firebase.setAndroidContext(ChatActivity.this);
        final Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE).child("group");
        postRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.hasChild(AppConstants.CHAT_USER_ID)) {
                    isTableExist = true;
                    fetchGroupOnlileUsersArray();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }


    /**
     * Function Responsible for getting the badge count for a specific channel
     */
    public void getChannelBadgeCount() {
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildGameVerifyApiRequestBody();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    updatedBadgeCount = responseObject.getInt("chat_bc");
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                ChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Displaying the chat badge count in the view
                        TabFragment.displayChatBadgeCount(updatedBadgeCount);
                        //Doing all the task for the chat after updating the badge count
                        chatPostTasks();
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildGameVerifyApiRequestBody() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "decrement_badge_count").newBuilder();
        urlBuilder.addQueryParameter("channel_id", AppConstants.CHAT_CHANNEL_ID);
        urlBuilder.addQueryParameter("user_id", senderId);

        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    //Function responsible for manipulating all the action for chat
    public void chatPostTasks(){

        if(AppConstants.CHAT_TYPE != null &&
                AppConstants.CHAT_TYPE.equalsIgnoreCase("individual")){
            updateUserOnlineStatus(true);
        }else{
            //Getting firebase Table existance
            getFirebaseTableExistanceStatus();
        }

        //Setting user activity in the chat board
        AppConstants.isUserinChatBoard = true;

        //Getting user chat message status
        AppConstants.isUserSentMessage = true;

        //Hiding the soft key board
        hodeKeyboard();

        //Initializing the chat View
        initializeChatView();

        //Handling the click events
        setClickevents();

        //Handlding the send button image color
        changeSendImage(false);

        //Adding request focus to the edit text
        putRequestFocus();

        //Updating Chat message badge count value
        if(AppConstants.CHAT_THREAD_UNREAD_MESSAGE_COUNT != 0){
            updateBadgeCountForUnreadMessages(AppConstants.CHAT_THREAD_UNREAD_MESSAGE_COUNT);
        }

        if(AppConstants.isGropuChatMessage){
            //Getting the group details
            fetchGroupValue(AppConstants.CHAT_USER_ID);
        }else{
            //Registering all the firebase callback functions
            //Fetching all the message for channel
            fetchValue();
        }

         /* Set Text Watcher listener */
        etChatMessage.addTextChangedListener(chatMessageWatcher);

        /*if(AppConstants.CHAT_TYPE != null &&
                AppConstants.CHAT_TYPE.equalsIgnoreCase("individual")){
            //Checking user online status
            getUserOnlineStatus();
        }*/
    }


    /**
     * Getting the user online status
     */
    public void getUserOffLineStatusGroupChat(){
        Firebase.setAndroidContext(ChatActivity.this);
        final Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);
        postRef.child("group").child(AppConstants.CHAT_USER_ID).child("offLineUsers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                if(groupChatoffLineUsersArray == null){
                    return;
                }

                if(groupChatoffLineUsersArray.size() > 0){
                    groupChatoffLineUsersArray.clear();
                }
                groupChatoffLineUsersArray = (ArrayList<String>) snapshot.getValue();

                if(groupChatoffLineUsersArray != null &&
                        groupChatoffLineUsersArray.size() > 0){
                    String recieverIds = buildJsonArraystring(groupChatoffLineUsersArray);

                    //API call to Send Push Notification
                    sendNotification(chatMessage, senderId,
                            recieverIds,
                            AppConstants.CHAT_USER_ID,
                            AppConstants.CHAT_TYPE_GROUP,
                            AppConstants.CHAT_CHANNEL_ID, logged_in_username);
                }
            }
            @Override
            public void onCancelled(FirebaseError error) {

            }
        });
    }
}
