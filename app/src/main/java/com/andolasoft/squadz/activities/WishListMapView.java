package com.andolasoft.squadz.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WishListMapView extends AppCompatActivity implements OnMapReadyCallback {

    SharedPreferences pref;
    String userId;
    @InjectView(R.id.back_image_search)
    ImageView backImageSearch;
    @InjectView(R.id.back_icon_layout_search_wishlist)
    RelativeLayout backIconLayoutSearchWishlist;
    @InjectView(R.id.wishlist_search_edit_field)
    EditText wishlistSearchEditField;
    @InjectView(R.id.wishlist_search_layout)
    RelativeLayout wishlistSearchLayout;
    @InjectView(R.id.back_icon_summary)
    ImageView backIconSummary;
    @InjectView(R.id.titletxt)
    TextView titletxt;
    @InjectView(R.id.back_icon_wishlist_layout)
    RelativeLayout backIconWishlistLayout;
    @InjectView(R.id.wishlist_location_icon)
    ImageView wishlistLocationIcon;
    @InjectView(R.id.wishlist_search_icon)
    ImageView wishlistSearchIcon;
    @InjectView(R.id.wishlist_toolbar_layout)
    Toolbar wishlistToolbarLayout;
    private GoogleMap mMap;
    boolean gps_enabled;
    GPSTracker gpsTracker;
    double current_lat, current_long;
    Marker venueMark;
    List<Marker> markers = new ArrayList<Marker>();
    String venueImage, venueReviews, venueAmount, venueReviewsCount, venueName;
    double venueMiles;
    SnackBar snackBar = new SnackBar(WishListMapView.this);
    View v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_wish_list_map_view);
        ButterKnife.inject(this);

        //Getting user data
        getUserData();

        isLocationEnabled();

        initMapGps();

        wishlistLocationIcon.setVisibility(View.GONE);

        wishlistLocationIcon.setVisibility(View.GONE);

        titletxt.setText("My Wish List MapView");

        backIconSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishVIew();
            }
        });


    }

    public void finishVIew(){
        WishListMapView.this.finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * Function call to get user data
     */
    public void getUserData() {
        pref = PreferenceManager.getDefaultSharedPreferences(WishListMapView.this);
        userId = pref.getString("loginuser_id", null);
    }



    /**
     * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
     *
     * @return
     */
    public boolean isLocationEnabled() {
        gps_enabled = false;
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gps_enabled;
    }


    /**
     * INITIALIZING MAP WITH GPS TRACKER
     */
    public void initMapGps() {
        gpsTracker = new GPSTracker(this);

        if (!gps_enabled) {
            Toast.makeText(WishListMapView.this, "please enable your location service", Toast.LENGTH_SHORT).show();
            Intent myIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
        } else {
            if (gpsTracker.canGetLocation()) {
                current_lat = gpsTracker.getLatitude();
                current_long = gpsTracker.getLongitude();
            }
        }
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(WishListMapView.this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (gps_enabled) {
            LatLng lat_lng = new LatLng(current_lat, current_long);
            // mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory
            //   .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)).position(lat_lng).title(current_lat.toString() + "," + current_long.toString()));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat_lng, 15));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.isMyLocationEnabled();
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.clear();
            //Placing the markers in the map view
            setMarkers();
        }

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @SuppressLint("InflateParams")
            public View getInfoContents(final Marker arg0) {
                v = getLayoutInflater().inflate(
                        R.layout.map_info_window, null);
                try {

                    //Initializing the Views
                    ImageView imgv_venue_image = (ImageView) v.findViewById(R.id.venue_image_infowindow);
                    TextView tv_venueMile = (TextView) v.findViewById(R.id.text_venue_miles);
                    TextView tv_venuerating = (TextView) v.findViewById(R.id.rating_venue_reviews);
                    TextView tv_venuePrice = (TextView) v.findViewById(R.id.venue_price);
                    TextView tv_Venue_name = (TextView) v.findViewById(R.id.venue_name);
                    RatingBar ratReviewBar = (RatingBar) v.findViewById(R.id.rating_venue);

                    //tv_venueMile.setText("0.4 miles");
                    tv_venuerating.setText(venueReviewsCount + " Reviews");
                    tv_Venue_name.setText(venueName);
                    tv_venueMile.setText(String.valueOf(venueMiles + " miles away"));
                    if (!TextUtils.isEmpty(venueAmount) &&
                            !venueAmount.equalsIgnoreCase("FREE") &&
                            !venueAmount.equalsIgnoreCase("0")) {
                        tv_venuePrice.setText("$ " + venueAmount + "/ Player");
                    } else {
                        tv_venuePrice.setText("FREE");
                    }
                    ratReviewBar.setRating(Float.valueOf(venueReviews));

                    if (!TextUtils.isEmpty(venueImage)
                            && !venueImage.equalsIgnoreCase("")) {
                        Glide.with(WishListMapView.this).load(venueImage).asBitmap().override(200, 150).listener(new RequestListener<String, Bitmap>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                if (!isFromMemoryCache) arg0.showInfoWindow();
                                return false;
                            }
                        }).into(imgv_venue_image);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return v;
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                String title = marker.getTitle();
                setMarketInfoWindow(title);
                return false;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                String title = marker.getTitle();
                // getVenuesCourtsDetailsInfor(title);
            }
        });

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                //mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 15));
                // mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

                if (markers.size() != 0) {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (Marker marker : markers) {
                        builder.include(marker.getPosition());
                    }
                    LatLngBounds bounds = builder.build();
                    int padding = 150; // offset from edges of the map in
                    // pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(
                            bounds, padding);
                    mMap.moveCamera(cu);
                    markers.clear();
                }
            }
        });

    }


    public void setMarkers() {
        for (int i = 0; i < MyWishList.wishListModelArrayList.size(); i++) {
            String venuelat = MyWishList.wishListModelArrayList.get(i).getLatitude();
            String venuelon = MyWishList.wishListModelArrayList.get(i).getLongitude();
            String venueId = MyWishList.wishListModelArrayList.get(i).getId();

            if (!TextUtils.isEmpty(venuelat) ||
                    !TextUtils.isEmpty(venuelon)) {
                LatLng pinLocation = new LatLng(Double.valueOf(venuelat), Double.valueOf(venuelon));

                venueMark = mMap
                        .addMarker(new MarkerOptions()
                                .position(pinLocation)
                                .title(venueId)
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                markers.add(venueMark);
            }
        }
    }

    public void setMarketInfoWindow(String courtId) {
        try {
            for (int i = 0; i < MyWishList.wishListModelArrayList.size(); i++) {
                String venueId = MyWishList.wishListModelArrayList.get(i).getId();
                if (venueId.equalsIgnoreCase(courtId)) {
                    ArrayList<String> image_array = MyWishList.wishListModelArrayList.
                            get(i).getImages_array();
                    venueImage = image_array.get(0);
                    venueReviews = MyWishList.wishListModelArrayList.get(i).getReviews();
                    venueAmount = MyWishList.wishListModelArrayList.get(i).getPrice();

                    String latitude = MyWishList.wishListModelArrayList.get(i).getLatitude();
                    String longitude = MyWishList.wishListModelArrayList.get(i).getLongitude();

                    venueMiles = ApplicationUtility.distance(HomeFragment.currLocLatitude, HomeFragment.
                            currLocLongitude, Double.parseDouble(latitude), Double.parseDouble(longitude));
                    venueReviewsCount = MyWishList.wishListModelArrayList.get(i).getReview_count();
                    venueName = MyWishList.wishListModelArrayList.get(i).getName();
                }
            }
        } catch (Exception ex) {
            snackBar.setSnackBarMessage("Unable to get Event data");
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishVIew();
    }



 /*   @OnClick({R.id.back_image_search,
            R.id.back_icon_layout_search_wishlist,
            R.id.wishlist_search_edit_field,
            R.id.wishlist_search_layout,
            R.id.back_icon_summary,
            R.id.titletxt,
            R.id.back_icon_wishlist_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_image_search:
                break;
            case R.id.back_icon_layout_search_wishlist:
                break;
            case R.id.wishlist_search_edit_field:
                break;
            case R.id.wishlist_search_layout:
                break;
            case R.id.back_icon_summary:
                WishListMapView.this.finish();
                break;
            case R.id.titletxt:
                break;
            case R.id.back_icon_wishlist_layout:
                break;
        }
    }*/
}
