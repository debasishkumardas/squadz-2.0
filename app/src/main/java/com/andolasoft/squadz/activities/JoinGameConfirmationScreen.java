package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class JoinGameConfirmationScreen extends AppCompatActivity {

    @InjectView(R.id.sports_name_summary)
    TextView tv_sportName;
    @InjectView(R.id.no_of_players_count_summary)
    TextView tv_player_count;
    @InjectView(R.id.venue_name_summary)
    TextView tv_courtEventName;
    @InjectView(R.id.venue_date_text)
    TextView tv_eventDate;
    @InjectView(R.id.venue_time)
    TextView tv_eventTIme;
    @InjectView(R.id.total_amount_summary)
    TextView tv_eventAmount;
    @InjectView(R.id.book_btn_summary_info)
    Button btn_confirm;
    @InjectView(R.id.venue_price_summary)
    TextView tv_price_summary;
    @InjectView(R.id.back_icon_summary)
    ImageView back_icon_summary;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String userId, auth_token;
    ProgressDialog dialog;
    SnackBar snackBar = new SnackBar(JoinGameConfirmationScreen.this);
    String status = null, message = null, userName, userProfileImage, billing_card_count;
    public static JoinGameConfirmationScreen joinGameConfirmationScreen;
    MaterialDialog mMaterialDialog;
    String is_Refundable_Status = "true";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_game_confirmation_screen);
        ButterKnife.inject(this);
        joinGameConfirmationScreen = this;
        prefs = PreferenceManager.getDefaultSharedPreferences(JoinGameConfirmationScreen.this);
        editor = prefs.edit();
        userId = prefs.getString("loginuser_id", null);
        auth_token = prefs.getString("auth_token", null);
        userName = prefs.getString("USERNAME","");
        userProfileImage = prefs.getString("image","");
        billing_card_count = prefs.getString("BILLING_COUNT", "");

        //Setting up the confirmation data
        setEventInfo();

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                billing_card_count = prefs.getString("BILLING_COUNT", "");
                if(Integer.valueOf(billing_card_count) > 0){

                        //Toast.makeText(JoinGameConfirmationScreen.this, "This game is available", Toast.LENGTH_LONG).show();
                        if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE_POINT)) {
                            long upcoming_time_milisecond_policy = getUpcomingTimeInMiliseonds(Integer.parseInt(Constants.CANCELLATION_POLICY_TYPE_POINT));
                            long time_slots_milisecond_policy = getTimeSlotsDateTimeinMilisecond();

                            if(time_slots_milisecond_policy < upcoming_time_milisecond_policy) {
                                String policyTitle = "Cancellation Policy\n ( "+Constants.CANCELLATION_POLICY_TYPE + " )";
                                String policyDescription = "The payment is non-refundable, because you have crossed the cancellation point.";

                                cancellationPolicyDialog(policyTitle,policyDescription);
                            }
                            else if(AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")
                                    || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))
                            {
                                String policyTitle = "Payment info";
                                String policyDescription = "You will only get charged "
                                        + ApplicationUtility.getHour_Days(Constants.CANCELLATION_POLICY_TYPE_POINT)
                                        +" before the event. You may back out anytime before then.";

                                cancellationPolicyDialog(policyTitle,policyDescription);
                            }
                            else{

                                //Calling the API to Join an event
                                joinGame(userId,Constants.CURRENT_EVENT_ID,userName,userProfileImage);
                            }
                        } else{
                            if(AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")
                                    || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))
                            {
                                String policyTitle = "Payment info";
                                String policyDescription = "You will only get charged "
                                                            + ApplicationUtility.getHour_Days(Constants.CANCELLATION_POLICY_TYPE_POINT)
                                                            +" before the event. You may back out anytime before then.";

                                cancellationPolicyDialog(policyTitle,policyDescription);
                            }
                            else{
                                //Calling the API to Join an event
                                joinGame(userId,Constants.CURRENT_EVENT_ID,userName,userProfileImage);
                            }
                        }

                }else{
                    //Displaying the dialog if there is no payment card is present
                    displayDialog();
                }
            }
        });

        back_icon_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JoinGameConfirmationScreen.this.finish();
                if(EventDetail.isEventDetailsApiCalled){
                    EventDetail.isEventDetailsApiCalled = false;
                    AppConstants.userIdArray.clear();
                    EventDetail.eventDetails.finish();
                    AppConstants.userModelArary.clear();
                    EventDetail.releseDeeLinkData();
                    startActivity(new Intent(JoinGameConfirmationScreen.this, SplashScreen.class));
                }
            }
        });

    }

    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayDialog(){
        mMaterialDialog = new MaterialDialog(JoinGameConfirmationScreen.this);
        View view = LayoutInflater.from(JoinGameConfirmationScreen.this)
                .inflate(R.layout.lay_no_payment_card_found,
                        null);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        TextView tv_add_payment_card = (TextView) view.findViewById(R.id.tv_add_card);
        TextView tv_add_friend_username = (TextView)view.findViewById(R.id.tv_add_friend_username);
        tv_add_friend_username.setText("No Payment cards found. You need to add at least one payment card to join this game/event.");

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_add_payment_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                startActivity(new Intent(JoinGameConfirmationScreen.this, AddPaymentDetailsScreen.class));
            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }


    /**
     * Function responsible for setting the Join game data
     */
    public void setEventInfo(){
        tv_sportName.setText(Constants.EVENT_PLAYERS_SPORT_NAME);
        tv_player_count.setText(Constants.EVENT_PLAYERS_COUNT);
        tv_courtEventName.setText(Constants.EVENT_NAME + " in " + Constants.EVENT_COURT_NAME);
        tv_eventDate.setText(Constants.EVENT_DATE);
        tv_eventTIme.setText(Constants.EVENT_TIME);

        if(!TextUtils.isEmpty(Constants.EVENT_PRICE)
                && !Constants.EVENT_PRICE.equalsIgnoreCase("0")){
            tv_eventAmount.setText("$ "+ Constants.EVENT_PRICE);
        }else{
            tv_eventAmount.setText("FREE");
        }

        if(!TextUtils.isEmpty(Constants.EVENT_PRICE)
                && !Constants.EVENT_PRICE.equalsIgnoreCase("0")){
            tv_price_summary.setText("$ "+Constants.EVENT_PRICE+" /"+" Player");
        }else{
            tv_price_summary.setText("FREE");
        }
    }



    /**
     * Function responsible for joing a event
     */
    public void joinGame(String user_id, final String event_id,
                         String userName, String userProfileIamge) {
        //Handling the loader state
        LoaderUtility.handleLoader(JoinGameConfirmationScreen.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, event_id, userName, userProfileIamge);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(JoinGameConfirmationScreen.this, false);
                //snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE_ERR);

                if(!ApplicationUtility.isConnected(JoinGameConfirmationScreen.this)){
                    ApplicationUtility.displayNoInternetMessage(JoinGameConfirmationScreen.this);
                }else{
                    ApplicationUtility.displayErrorMessage(JoinGameConfirmationScreen.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    message = responseObject.getString("message");
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                JoinGameConfirmationScreen.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(JoinGameConfirmationScreen.this, false);
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            AppConstants.GAME_ID = event_id;
                            Toast.makeText(JoinGameConfirmationScreen.this, message, Toast.LENGTH_LONG).show();
                            //Finishing the activities
                            finishActivity();

                            /**Keeping Game info inside local database for future review*/
                            storeGameInfoForRating();

                            //Resetting all the values
                            resetValues();
                            Intent intent = new Intent(JoinGameConfirmationScreen.this, FinishEventCreation.class);
                            startActivity(intent);
                        }else if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                            Toast.makeText(JoinGameConfirmationScreen.this, message, Toast.LENGTH_LONG).show();
                        }else{
                            snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE_ERR);
                        }
                    }
                });
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String userId, String eventId, String userName, String userProfileImage) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "join_game").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("game_id", eventId);
        urlBuilder.addQueryParameter("user_name", userName);
        urlBuilder.addQueryParameter("user_profile_image", userProfileImage);
        urlBuilder.addQueryParameter("is_refundable", is_Refundable_Status);//true/false
        urlBuilder.addQueryParameter("joined_at", ApplicationUtility.getCurrentDateTime());
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }


    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(JoinGameConfirmationScreen.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        JoinGameConfirmationScreen.this.finish();
        if(EventDetail.isEventDetailsApiCalled){
            EventDetail.isEventDetailsApiCalled = false;
            AppConstants.userIdArray.clear();
            EventDetail.eventDetails.finish();
            AppConstants.userModelArary.clear();
            EventDetail.releseDeeLinkData();
            startActivity(new Intent(JoinGameConfirmationScreen.this, SplashScreen.class));
        }
    }

    /**
     * Function responsible for resetting all the values
     */
    public void resetValues(){
        Constants.EVENT_NAME = null;
        Constants.CURRENT_EVENT_ID = null;
        Constants.EVENT_DATE = null;
        Constants.EVENT_TIME = null;
        Constants.EVENT_PRICE = null;
        Constants.EVENT_VENUE_COURT = null;
        Constants.EVENT_PLAYERS_COUNT = null;
        Constants.EVENT_PLAYERS_SPORT_NAME = null;
    }

    /**
     * Function responsibel for removing all the rendered activities
     */
    public void finishActivity(){
        if(EventListing.eventListing != null){
            EventListing.eventListing.finish();
            EventDetail.eventDetails.finish();
        }else if(MyWishList.myWishList != null){
            MyWishList.myWishList.finish();
            EventDetail.eventDetails.finish();
        }
        if( EventDetail.eventDetails != null)
        {
            EventDetail.eventDetails.finish();
        }
        JoinGameConfirmationScreen.this.finish();
        AppConstants.userModelArary.clear();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //API call to make the user join the game
        if(AppConstants.isPaymentCardAdded){
            AppConstants.isPaymentCardAdded = false;

            //Refreshing the storage
            getCountAPI(userId, auth_token);
        }
    }

    /**
     * Fetching Radius,wishlist & Card count
     */
    /**
     * API integration for all counts
     */
    public void getCountAPI(String user_id, String auth_token) {
        //Handling the loader state
        //handleLoader(true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(user_id,auth_token);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                //handleLoader(false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String radius = "0",cards = "0",wish_lists_count = "0";
                try {
                    JSONObject radius_jsonObject = new JSONObject(responseData);
                    JSONObject user_json_object = radius_jsonObject.getJSONObject("user_info");
                    if(user_json_object.has("radius") && !user_json_object.isNull("radius")) {
                        radius = user_json_object.getString("radius");
                    }
                    if(user_json_object.has("cards") && !user_json_object.isNull("cards")) {
                        cards = user_json_object.getString("cards");
                    }
                    if(user_json_object.has("wish_lists_count") && !user_json_object.isNull("wish_lists_count")) {
                        wish_lists_count = user_json_object.getString("wish_lists_count");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalRadius = radius;
                final String finalCards = cards;
                final String finalWish_lists_count = wish_lists_count;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(JoinGameConfirmationScreen.this, "Payment card successfully added", Toast.LENGTH_SHORT).show();
                        //Store counts for Radius, Billings (Card listing) & Wishlist
                        setCountDatainPreferences(finalRadius, finalCards, finalWish_lists_count);
                        //Calling the API to Join an event
                       // joinGame(userId,Constants.CURRENT_EVENT_ID,userName,userProfileImage);
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String user_id, String auth_token) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_profile_info").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("user_id", user_id);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Store counts for Radius, Billings (Card listing) & Wishlist
     */
    public void setCountDatainPreferences(String radius_count, String billing_count,
                                          String wishlist_count) {
        if(radius_count.equalsIgnoreCase("0")) {
            this.editor.putString("RADIUS_COUNT","10");
        } else{
            this.editor.putString("RADIUS_COUNT",radius_count);
        }
        this.editor.putString("BILLING_COUNT",billing_count);
        this.editor.putString("WISHLIST_COUNT",wishlist_count);
        this.editor.apply();
    }

    /**
     * Getting next day's 24 time from the current date n time
     */
    public long getUpcomingTimeInMiliseonds(int hour)
    {
        long time_milisecond = 0;
        SimpleDateFormat inputFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        String currentDateandTime = sdf.format(new Date());
        Date current_time = null;
        try {
            current_time = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();
        c.setTime(current_time);
        //c.add(Calendar.DATE, 1);
        c.add(Calendar.HOUR, hour);

        String current = sdf.format(c.getTime());

        Date current_timee = null;
        try {
            current_timee = sdf.parse(current);
            time_milisecond = current_timee.getTime();
            System.out.print(""+time_milisecond);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time_milisecond;
    }


    public long getTimeSlotsDateTimeinMilisecond() {
        long time_milisecond = 0;
        String date = EventDetail.eventDate;

        String timeSlots = date+" "+EventDetail.start_time;

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy h:mma");

        try {
            Date date1 = inputFormatter.parse(timeSlots);
            time_milisecond = date1.getTime();

            System.out.print(""+time_milisecond);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time_milisecond;
    }

    /**
     * Function responsible for displaying of the dialog
     */
    public void cancellationPolicyDialog(String policy_title, String policy_description){
        mMaterialDialog = new MaterialDialog(JoinGameConfirmationScreen.this);
        View view = LayoutInflater.from(JoinGameConfirmationScreen.this)
                .inflate(R.layout.layout_dialog_cancellation_policy,
                        null);

        TextView tv_cancellation_policy_title = (TextView) view.findViewById(R.id.tv_cancellation_policy_title);
        TextView tv_cancellation_policy = (TextView) view.findViewById(R.id.tv_cancellation_policy);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel_policy);
        TextView tv_confirm = (TextView) view.findViewById(R.id.tv_confirm_policy);

        tv_cancellation_policy_title.setText(policy_title);
        tv_cancellation_policy.setText(policy_description);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();

            }
        });

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                is_Refundable_Status = "false";
                //Calling the API to Join an event
                joinGame(userId,Constants.CURRENT_EVENT_ID,userName,userProfileImage);
            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }

    /**
     * Store List info in databse for future ratings
     */
    public void storeGameInfoForRating()
    {
        String end_time = "";
        DatabaseAdapter db = new DatabaseAdapter(this);
        if(!TextUtils.isEmpty(AppConstants.OBJECT_TYPE_GAME)
                && AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("list")) {
            /**Keeping Game info inside local database for future review*/
            db.insertGameDetailsForRating(Constants.EVENT_NAME_FOR_RATING,Constants.LIST_ID,"","list",userId,ApplicationUtility.formattedDateForSignup(Constants.EVENT_DATE),
                    String.valueOf(ApplicationUtility.getEventDateTimeInMilisecond(Constants.EVENT_DATE+ " "+ Constants.EVENT_END_TIME)));
        }
        else{
            /**Keeping Game info inside local database for future review*/
            db.insertGameDetailsForRating(Constants.EVENT_NAME_FOR_RATING,Constants.CURRENT_EVENT_ID,"","event",userId,ApplicationUtility.formattedDateForSignup(Constants.EVENT_DATE),
                    String.valueOf(ApplicationUtility.getEventDateTimeInMilisecond(Constants.EVENT_DATE+ " "+ Constants.EVENT_END_TIME)));
        }

    }

}
