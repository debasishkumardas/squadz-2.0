package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.AddPlayerModel;
import com.andolasoft.squadz.models.InviteTeammatesModel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.AddPlayerAdapter;
import com.andolasoft.squadz.views.adapters.InviteTeammatesAdapter;
import com.andolasoft.squadz.views.adapters.UpcomingPastAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SpNayak on 3/1/2017.
 */

public class InviteTeammates extends AppCompatActivity {

    @InjectView(R.id.back_invite_temamates_layout)
    RelativeLayout back_invite_temamates_layout;
    @InjectView(R.id.invite_apply_btn)
    Button invite_apply_btn;
    @InjectView(R.id.invite_teammates_recycler_view)
    RecyclerView invite_teammates_recycler_view;
    DatabaseAdapter db;
    ArrayList<InviteTeammatesModel> inviteTeammatesModelArrayList;
    InviteTeammatesAdapter inviteTeammatesAdapter;
    ProgressDialog dialog;
    SnackBar snackBar;
    SharedPreferences pref;
    String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_invite_teammates_layout);
        ButterKnife.inject(this);
        db = new DatabaseAdapter(this);
        snackBar = new SnackBar(this);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        userId = pref.getString("loginuser_id", null);

        /**Set onclick event*/
        addClickEvent();

        /**Function to pull all the friends from Local DB*/
        getAllFriendFromDBWithFilter();
    }

    /**
     * Set onclick event
     */
    public void addClickEvent() {
        back_invite_temamates_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        invite_apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONArray teams_ids_jsonArray = new JSONArray();
                JSONArray teams_images_jsonArray = new JSONArray();
                JSONArray teams_names_jsonArray = new JSONArray();

                /**Keeping Teammates IDs inside Json array to send to server*/
                if(Constants.ADD_TEAMMATES_IDS_ARRAY.size() > 0)
                {
                    for (int i = 0; i < Constants.ADD_TEAMMATES_IDS_ARRAY.size(); i++) {
                        teams_ids_jsonArray.put(Constants.ADD_TEAMMATES_IDS_ARRAY.get(i));
                    }
                    for (int i = 0; i < Constants.ADD_TEAMMATES_IMAGES_ARRAY.size(); i++) {
                        teams_images_jsonArray.put(Constants.ADD_TEAMMATES_IMAGES_ARRAY.get(i));
                    }
                    for (int i = 0; i < Constants.ADD_TEAMMATES_NAMES_ARRAY.size(); i++) {
                        teams_names_jsonArray.put(Constants.ADD_TEAMMATES_NAMES_ARRAY.get(i));
                    }

                    addTeammatesAPI(teams_ids_jsonArray, teams_images_jsonArray,
                            teams_names_jsonArray, UpcomingPastAdapter.event_id, userId);
                }
                else{
                    snackBar.setSnackBarMessage("Please choose teammates to invite");
                }
            }
        });
    }

    /**
     * Function to pull all the friends from Local DB
     */

    public void getAllFriendFromDBWithFilter() {
        inviteTeammatesModelArrayList = new ArrayList<>();
        Cursor cursor = db.getAllFriendsDeomDb();
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String user_id = cursor.getString(cursor
                            .getColumnIndex("user_id"));
                    String username = cursor.getString(cursor
                            .getColumnIndex("username"));
                    String firstname = cursor.getString(cursor
                            .getColumnIndex("firstname"));
                    String lastname = cursor.getString(cursor
                            .getColumnIndex("lastname"));
                    String image = cursor.getString(cursor
                            .getColumnIndex("image"));
                    String sport = cursor.getString(cursor
                            .getColumnIndex("sport"));
                    String phonenumber = cursor.getString(cursor
                            .getColumnIndex("phonenumber"));
                    String status = cursor.getString(cursor
                            .getColumnIndex("status"));
                    String reward = cursor.getString(cursor
                            .getColumnIndex("reward"));

                    InviteTeammatesModel inviteTeammatesModel = new InviteTeammatesModel(user_id, username, firstname,
                            lastname, image, sport, phonenumber, status, reward);
                    inviteTeammatesModelArrayList.add(inviteTeammatesModel);

                } while (cursor.moveToNext());
            }
        }
        if(inviteTeammatesModelArrayList.size() > 0)
        {
            inviteTeammatesAdapter = new InviteTeammatesAdapter(InviteTeammates.this,inviteTeammatesModelArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            invite_teammates_recycler_view.setLayoutManager(mLayoutManager);
            invite_teammates_recycler_view.setItemAnimator(new DefaultItemAnimator());
            invite_teammates_recycler_view.setAdapter(inviteTeammatesAdapter);
            inviteTeammatesAdapter.notifyDataSetChanged();
        }
        else{
            Toast.makeText(InviteTeammates.this, "No Players Found", Toast.LENGTH_LONG).show();
            inviteTeammatesAdapter = new InviteTeammatesAdapter(InviteTeammates.this,inviteTeammatesModelArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            invite_teammates_recycler_view.setLayoutManager(mLayoutManager);
            invite_teammates_recycler_view.setItemAnimator(new DefaultItemAnimator());
            invite_teammates_recycler_view.setAdapter(inviteTeammatesAdapter);
            inviteTeammatesAdapter.notifyDataSetChanged();
        }

    }

    /**
     * API integration for Adding player to the Newly added team
     */
    public void addTeammatesAPI(JSONArray teammates_ids_array, JSONArray teammates_images_array,
                                JSONArray teammates_names_array, String game_id, String invited_by) {
        //Handling the loader state
        LoaderUtility.handleLoader(InviteTeammates.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForAddPlayers(teammates_ids_array,teammates_images_array,
                teammates_names_array,game_id,invited_by);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(InviteTeammates.this, false);

                if(!ApplicationUtility.isConnected(InviteTeammates.this)){
                    ApplicationUtility.displayNoInternetMessage(InviteTeammates.this);
                }else{
                    ApplicationUtility.displayErrorMessage(InviteTeammates.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject teammates_jsonObject = new JSONObject(responseData);

                    if(teammates_jsonObject.has("status") && !teammates_jsonObject.isNull("status"))
                    {
                        status = teammates_jsonObject.getString("status");
                    }
                    if(teammates_jsonObject.has("message") && !teammates_jsonObject.isNull("message"))
                    {
                        message = teammates_jsonObject.getString("message");
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalStatus = status;
                final String finalMessage = message;
                final String finalStatus1 = status;
                final String finalMessage1 = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(InviteTeammates.this, false);
                        if(finalStatus1.equalsIgnoreCase("success"))
                        {
                            Constants.ADD_TEAMMATES_IDS_ARRAY = new ArrayList<>();
                            Constants.ADD_TEAMMATES_IMAGES_ARRAY = new ArrayList<>();
                            Constants.ADD_TEAMMATES_NAMES_ARRAY = new ArrayList<>();

                            //snackBar.setSnackBarMessage(finalMessage1);
                            Toast.makeText(InviteTeammates.this, ""+finalMessage1, Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else{
                            snackBar.setSnackBarMessage(finalMessage1);
                        }

                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForAddPlayers(JSONArray teammates_ids_array, JSONArray teammates_images_array,
                                                    JSONArray teammates_names_array, String game_id, String invited_by) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "invite_user").newBuilder();
        urlBuilder.addQueryParameter("user_id", teammates_ids_array.toString());
        urlBuilder.addQueryParameter("user_profile_image", teammates_images_array.toString());
        urlBuilder.addQueryParameter("user_name", teammates_names_array.toString());
        urlBuilder.addQueryParameter("game_id", game_id);
        urlBuilder.addQueryParameter("invited_by", invited_by);
        urlBuilder.addQueryParameter("multiple", "true");
        urlBuilder.addQueryParameter("invited_at", ApplicationUtility.getCurrentDateTime());

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(InviteTeammates.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
