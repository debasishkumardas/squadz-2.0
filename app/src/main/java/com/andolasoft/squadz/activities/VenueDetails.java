package com.andolasoft.squadz.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.VenueDetailModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.MySpannable;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.utils.SportsSurfaceTypePicker;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.adapters.ImageSlideAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class VenueDetails extends FragmentActivity implements OnMapReadyCallback {

    public static VenueDetails venueDetails;
    private MapView mapView;
    private GPSTracker gpsTracker;
    private Double current_lat, current_long;
    private boolean gps_enabled = false;
    private GoogleMap mMap;
    private RatingBar rating_venue_details;
    private RelativeLayout venue_detail_back_icon_layout,
            layout_venue_detail_rating,equipments_layout;
    private TextView court_description_venue_detail,
            venue_detail_name, venue_detail_price,
            rating_venue_details_reviews, venue_detail_miles,
            total_member_capacity_venue_detail,
            court_type_venue_detail, equipments_name_venue_detail, tv_court_name_venue_detail,
            add_info_text,court_title_venue_detail;
    private ScrollView scroll_venue_detail;
    private Button check_availability_btn;
    private MaterialFavoriteButton venue_details_favorite_icon;
    private ProgressDialog dialog;
    boolean isAddedToFavorite = false;
    String status;
    SnackBar snackBar;
    ViewPager viewpager;
    ImageButton leftNav, rightNav;
    ImageView equipments_icon_venue,venue_details_eye_icon,indoor_icon, courtSpaceType;
    TextView venue_details_info_title;
    FrameLayout venue_detail_image_frame;
    boolean favorite_status = false;
    ArrayList<String> images_array;
    ImageView ivShareList;
    ShareDialog shareDialog;
    private CallbackManager callbackManager;
    private LoginManager manager;
    public static String venueId = null;
    public VenueDetailModel venueDetailModel;
    SharedPreferences prefs;
    String userId = null,courtSpaceDesc = "";
    public boolean isVenueDetailsApiCalled = false;
    MaterialDialog mMaterialDialog;
    TextView tvVenueName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_venue_details);


        //CHanging the Status Bar color
        StatusBarUtils.changeStatusBarColor(VenueDetails.this);

        //Getting user data
        getUserData();

        /**
         * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
         */
        initReferences();

        /**
         * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
         */
        isLocationEnabled();


        /**
         * CLICK EVENTS ON THE VIEWS
         */
        addClickEvents();

        if (AppConstants.isListDeepLinkRecieved) {
            releseDeeLinkData();
            getVenuesCourtsDetailsInfor(AppConstants.DEEP_LINKING_LIST_ID);
        } else {
            /**
             * Set Venue data
             */
            setVenueData();

            /**
             * INITIALIZING MAP WITH GPS TRACKER
             */
            initMapGps();
        }
    }


    public void getUserData() {
        prefs = PreferenceManager.getDefaultSharedPreferences(VenueDetails.this);
        userId = prefs.getString("loginuser_id", null);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (gps_enabled) {

            LatLng lat_lng = new LatLng(VenueAdapter.court_latitude, VenueAdapter.court_longitude);
            mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)).position(lat_lng).title(VenueAdapter.venueDetailModelArrayList.get(0).getName()));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat_lng, 15));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.isMyLocationEnabled();
            mMap.getUiSettings().setAllGesturesEnabled(false);
            //mMap.setMyLocationEnabled(true);
        }
    }

    /**
     * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
     *
     * @return
     */
    public boolean isLocationEnabled() {
        gps_enabled = false;
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gps_enabled;
    }

    /**
     * INITIALIZING MAP WITH GPS TRACKER
     */
    public void initMapGps() {
        gpsTracker = new GPSTracker(this);

        if (!gps_enabled) {
            Toast.makeText(VenueDetails.this, "please enable your location service", Toast.LENGTH_SHORT).show();
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
        } else {
            if (gpsTracker.canGetLocation()) {
                current_lat = gpsTracker.getLatitude();
                current_long = gpsTracker.getLongitude();
            }
        }

        final SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.mapView_Venue_Detail);
        mapFragment.getMapAsync(this);
    }

    /**
     * CLICK EVENTS ON THE VIEWS
     */

    public void addClickEvents() {
        venue_detail_back_icon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.FROM_MAP_VIEW) {
                    finish();
                } else if (Constants.FROM_WISHLIST) {
                    Constants.FROM_WISHLIST = false;
                    finish();
                    Intent intent = new Intent(VenueDetails.this, MyWishList.class);
                    startActivity(intent);
                } else if (isVenueDetailsApiCalled) {
                    //Releasing the deeplink data
                    releseDeeLinkData();
                    finish();
                    isVenueDetailsApiCalled = false;
                    startActivity(new Intent(VenueDetails.this, SplashScreen.class));
                } else {
                    finish();
                    Intent intent = new Intent(VenueDetails.this, VenueListing.class);
                    startActivity(intent);
                }
            }
        });

        check_availability_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                Intent intent = new Intent(VenueDetails.this, Availability.class);
                startActivity(intent);
            }
        });

        venue_details_favorite_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String venueId = VenueAdapter.venueId;
                String userId = VenueAdapter.userId;

                if (favorite_status) {
                    makeListFavorite(userId, venueId, false);
                } else {
                    makeListFavorite(userId, venueId, true);
                }
            }
        });
       /* venue_details_favorite_icon.setOnFavoriteChangeListener(new MaterialFavoriteButton.OnFavoriteChangeListener() {
            @Override
            public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
                String venueId = VenueAdapter.venueId;
                String userId = VenueAdapter.userId;

                if (favorite) {
                    makeListFavorite(userId, venueId, favorite);
                } else {
                    makeListFavorite(userId, venueId, favorite);
                }
            }
        });*/
       /* venue_details_favorite_icon.setOnFavoriteAnimationEndListener(new MaterialFavoriteButton.OnFavoriteAnimationEndListener() {
            @Override
            public void onAnimationEnd(MaterialFavoriteButton buttonView, boolean favorite) {

                String venueId = VenueAdapter.venueId;
                String userId = VenueAdapter.userId;

                if (favorite) {
                    makeListFavorite(userId, venueId, favorite);
                } else {
                    makeListFavorite(userId, venueId, favorite);
                }
            }
        });*/

        // Images left navigation
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewpager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    viewpager.setCurrentItem(tab);
                } else if (tab == 0) {
                    viewpager.setCurrentItem(tab);
                }
            }
        });

        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewpager.getCurrentItem();
                tab++;
                viewpager.setCurrentItem(tab);
            }
        });

        ivShareList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //shareListingDetails();
                //Calling function to share the listing details
                displayShareDialog();
            }
        });

        layout_venue_detail_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VenueDetails.this, ReviewDetail.class));
            }
        });

        venue_details_eye_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE)) {
                    cancellationPolicyDialog();
                }
            }
        });
        venue_details_info_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE)) {
                    cancellationPolicyDialog();
                }
            }
        });

        /**Equipments field click event*/
        equipments_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        /**Additional Info click*/
        add_info_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VenueDetails.this,ListingAdditionalInfo.class));
            }
        });


    }

    /**
     * Function responsible for diaplying of the dialog
     */
    public void cancellationPolicyDialog(){
        mMaterialDialog = new MaterialDialog(VenueDetails.this);
        View view = LayoutInflater.from(VenueDetails.this)
                .inflate(R.layout.layout_dialog_cancellation_policy,
                        null);

        TextView tv_cancellation_policy_title = (TextView) view.findViewById(R.id.tv_cancellation_policy_title);
        TextView tv_cancellation_policy = (TextView) view.findViewById(R.id.tv_cancellation_policy);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel_policy);
        TextView tv_confirm = (TextView) view.findViewById(R.id.tv_confirm_policy);

        tv_cancel.setVisibility(View.GONE);

        if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE)
                && Constants.CANCELLATION_POLICY_TYPE.equalsIgnoreCase("moderate")
                || Constants.CANCELLATION_POLICY_TYPE.equalsIgnoreCase("strict")
                ||Constants.CANCELLATION_POLICY_TYPE.equalsIgnoreCase("flexible")
                ||Constants.CANCELLATION_POLICY_TYPE.equalsIgnoreCase("super_strict")) {
            tv_cancellation_policy_title.setText("Cancellation Policy\n ( "+Constants.CANCELLATION_POLICY_TYPE + " )");
            String policyDescription = "Participants can cancel the booking "+
                    ApplicationUtility.getDaysFromHour(Constants.CANCELLATION_POLICY_TYPE_POINT) +" before the booking time.";
            tv_cancellation_policy.setText(policyDescription);
        }

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();

            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }

    /**
     * Function is for displying eqipments listed dialog
     */
    public void equipmentsDialog(){
        mMaterialDialog = new MaterialDialog(VenueDetails.this);
        View view = LayoutInflater.from(VenueDetails.this)
                .inflate(R.layout.equipments_provided_layout,
                        null);

        TextView tv_cancellation_policy_title = (TextView) view.findViewById(R.id.tv_cancellation_policy_title);
        TextView tv_cancellation_policy = (TextView) view.findViewById(R.id.tv_cancellation_policy);

        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Function responsible for sharing the listing details in facebook
     */
    public void shareListingDetails() {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    //.setContentTitle("Squadz Game Posts")
                    //.setContentDescription("Lace UP!!")
                    .setContentUrl(Uri.parse(EndPoints.BASE_URL_DEEP_LINKING + "list/" + venueId))
                    .build();
            ShareDialog shareDialog_new = new ShareDialog(VenueDetails.this);
            shareDialog_new.show(linkContent, ShareDialog.Mode.NATIVE);
            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                @Override
                public void onSuccess(Sharer.Result result) {
                    Toast.makeText(VenueDetails.this, "Shared your post successfully", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancel() {
                    //Toast.makeText(FinishEventCreation.this, "Cancel", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException e) {
                    e.printStackTrace();
                    Toast.makeText(VenueDetails.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            snackBar.setSnackBarMessage("Please check your internet connection");
        }
    }

    /**
     * Read more & Less functionality
     *
     * @param tv
     * @param maxLine
     * @param expandText
     * @param viewMore
     */
    public void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    public SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                    final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {

            ssb.setSpan(new MySpannable(false) {
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "Read less", false);

                        scroll_venue_detail.fullScroll(View.FOCUS_DOWN);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, "Read more", true);

                        scroll_venue_detail.fullScroll(View.FOCUS_UP);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    /**
     * Fetching venue details from venue adapter class
     */
    public void setVenueData() {
        try {
            String name = VenueAdapter.venueDetailModelArrayList.get(0).getName();
            String latitude = VenueAdapter.venueDetailModelArrayList.get(0).getLatitude();
            String longitude = VenueAdapter.venueDetailModelArrayList.get(0).getLongitude();
            String capacity = VenueAdapter.venueDetailModelArrayList.get(0).getCapacity();
            String court_type = VenueAdapter.venueDetailModelArrayList.get(0).getCourt_type();
            String eventSportName = VenueAdapter.venueDetailModelArrayList.get(0).getEvent_Sports_Type();
            venueId = VenueAdapter.venueDetailModelArrayList.get(0).getId();
            Constants.CANCELLATION_POLICY_TYPE = VenueAdapter.venueDetailModelArrayList.get(0).getCancel_policy_type();
            Constants.CANCELLATION_POLICY_TYPE_POINT = VenueAdapter.venueDetailModelArrayList.get(0).getCancellation_point();
            venueId = VenueAdapter.venueDetailModelArrayList.get(0).getId();
            //AppConstants.OBJECT_TYPE_GAME = VenueAdapter.venueDetailModelArrayList.get(0).getObject_Type();
            boolean isEquipmentPrivided = VenueAdapter.venueDetailModelArrayList.get(0).isEquipmentAvailable();
            favorite_status = VenueAdapter.venueDetailModelArrayList.get(0).isFavourite();
            String venueName = VenueAdapter.venueDetailModelArrayList.get(0).getVenueName();

            if (favorite_status) {
                venue_details_favorite_icon.setFavorite(true);
                venue_details_favorite_icon.setFavoriteResource(R.mipmap.ic_favorite_orange);
            } else {
                venue_details_favorite_icon.setFavorite(false);
                venue_details_favorite_icon.setFavoriteResource(R.mipmap.ic_favorite_gray);
            }
            images_array = VenueAdapter.venueDetailModelArrayList.get(0).getImages_array();
            /**
             * Set images inside view pager
             */
            setImagesInViewPager(images_array);

            String review_count = VenueAdapter.venueDetailModelArrayList.get(0).getReview_count();
            if (!TextUtils.isEmpty(review_count)) {
                int review = Integer.parseInt(VenueAdapter.venueDetailModelArrayList.get(0).getReview_count());
                if (review == 0) {
                    rating_venue_details_reviews.setText("" + review + " Review");
                } else if (review == 1) {
                    rating_venue_details_reviews.setText("" + review + " Review");
                } else {
                    rating_venue_details_reviews.setText("" + review + " Reviews");
                }
            } else {
                rating_venue_details_reviews.setText(review_count + " Reviews");
            }

            court_title_venue_detail.setText(VenueAdapter.venueDetailModelArrayList.get(0).getName());
            venue_detail_name.setText(VenueAdapter.venueDetailModelArrayList.get(0).getName());
            rating_venue_details.setRating(Float.parseFloat(VenueAdapter.venueDetailModelArrayList.get(0).getReview_rating()));

            if (capacity != null && capacity.equalsIgnoreCase("undefined")) {
                total_member_capacity_venue_detail.setText("0");
                Constants.COURT_MAX_PLAYER_ = 0;
            } else if (capacity != null && capacity.equalsIgnoreCase("No maximum player capacity")) {
                total_member_capacity_venue_detail.setText(capacity);
                Constants.COURT_MAX_PLAYER_ = 1;
            } else {
                Constants.COURT_MAX_PLAYER_ = Integer.valueOf(capacity);
                total_member_capacity_venue_detail.setText(capacity);
            }

            if(court_type.equalsIgnoreCase("Outdoor")) {
                indoor_icon.setImageResource(R.mipmap.ic_court_outdoor);
            } else{
                indoor_icon.setImageResource(R.mipmap.ic_court_indoor);
            }

            court_type_venue_detail.setText(court_type);

            if(!TextUtils.isEmpty(venueName)){
                tvVenueName.setText(venueName);
            }else{
                tvVenueName.setVisibility(View.GONE);
            }


            String price = VenueAdapter.venueDetailModelArrayList.get(0).getPrice();
            if (!TextUtils.isEmpty(price) &&
                    !price.equalsIgnoreCase("0")) {
                Constants.COURT_PRICE = VenueAdapter.venueDetailModelArrayList.get(0).getPrice();
                venue_detail_price.setText("$ " + VenueAdapter.venueDetailModelArrayList.get(0).getPrice()+" / hour");
            } else {
                Constants.COURT_PRICE = "FREE";
                venue_detail_price.setText("FREE");
            }
            court_description_venue_detail.setText(VenueAdapter.venueDetailModelArrayList.get(0).getDescription());

            if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE)) {
                venue_details_eye_icon.setVisibility(View.VISIBLE);
                venue_details_info_title.setVisibility(View.VISIBLE);
            } else{
                venue_details_eye_icon.setVisibility(View.GONE);
                venue_details_info_title.setVisibility(View.GONE);
            }

            /**
             * Read more functionality on text view
             */

            court_description_venue_detail.post(new Runnable() {

                @Override
                public void run() {
                    if (court_description_venue_detail.getLineCount() > 3) {
                        makeTextViewResizable(court_description_venue_detail, 3, "Read More", true);
                    }

                }
            });

            /*double current_loc_latitude = HomeFragment.currLocLatitude;
            double current_loc_longitude = HomeFragment.currLocLongitude;*/

            if (!TextUtils.isEmpty(latitude)) {
                double dest_loc_latitude = Double.valueOf(latitude);
                double dest_loc_longitude = Double.valueOf(longitude);

                if (VenueAdapter.gpsTracker == null) {
                    VenueAdapter.gpsTracker = new GPSTracker(VenueDetails.this);
                }

                double distance = ApplicationUtility.distance(VenueAdapter.gpsTracker.getLatitude(), VenueAdapter.gpsTracker.getLongitude(),
                        dest_loc_latitude, dest_loc_longitude);

                venue_detail_miles.setText(String.valueOf(distance) + " miles");
            } else {
                venue_detail_miles.setText("");
            }

            if (isEquipmentPrivided) {
                SportsImagePicker sport = new SportsImagePicker();
                int isEquipmentAvailable = sport.getSportImage(Constants.SPORTS_NAME);
                //equipments_icon_venue.setImageResource(isEquipmentAvailable);
                //equipments_name_venue_detail.setText("Equipments Provided");
            }

            VenueAdapter.court_latitude = Double.valueOf(latitude);
            VenueAdapter.court_longitude = Double.valueOf(longitude);

            String minAge = VenueAdapter.venueDetailModelArrayList.get(0).getCourt_min_players();
            String spaceType = VenueAdapter.venueDetailModelArrayList.get(0).getSpace_type();
            courtSpaceDesc = VenueAdapter.venueDetailModelArrayList.get(0).getCourt_space_desc();
            String spec = "";
            if(courtSpaceDesc.contains(","))
            {
                String[] courtSpaceDesc_str = courtSpaceDesc.split(",");
                spec = courtSpaceDesc_str[0];
            }
            else{
                spec = courtSpaceDesc;
            }

            int getCourtSpaceTypeImage = SportsSurfaceTypePicker.getSportSurfaceType(spaceType);
            courtSpaceType.setBackgroundResource(getCourtSpaceTypeImage);
            equipments_name_venue_detail.setText(minAge);
            tv_court_name_venue_detail.setText(spec);
        } catch (Exception exp) {
            exp.printStackTrace();
        }

    }


    /**
     * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
     */
    public void initReferences() {
        tvVenueName = (TextView) findViewById(R.id.tvVenueName);
        venue_detail_back_icon_layout = (RelativeLayout) findViewById(R.id.venue_detail_back_icon_layout);
        rating_venue_details = (RatingBar) findViewById(R.id.rating_venue_details);
        court_description_venue_detail = (TextView) findViewById(R.id.court_description_venue_detail);
        scroll_venue_detail = (ScrollView) findViewById(R.id.scroll_venue_detail);
        venue_detail_name = (TextView) findViewById(R.id.venue_detail_name);
        rating_venue_details_reviews = (TextView) findViewById(R.id.rating_venue_details_reviews);
        //venue_detail_image = (ImageView) findViewById(R.id.venue_detail_image);
        venue_detail_price = (TextView) findViewById(R.id.venue_detail_price);
        venue_detail_miles = (TextView) findViewById(R.id.venue_detail_miles);
        total_member_capacity_venue_detail = (TextView) findViewById(R.id.total_member_name_venue_detail);
        court_type_venue_detail = (TextView) findViewById(R.id.indoor_name_venue_detail);
        check_availability_btn = (Button) findViewById(R.id.check_availability_btn);
        venue_details_favorite_icon = (MaterialFavoriteButton) findViewById(R.id.venue_details_favorite_icon);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        leftNav = (ImageButton) findViewById(R.id.left_nav);
        rightNav = (ImageButton) findViewById(R.id.right_nav);
        venue_detail_image_frame = (FrameLayout) findViewById(R.id.venue_detail_image_frame);
        equipments_icon_venue = (ImageView) findViewById(R.id.equipments_icon_venue);
        equipments_name_venue_detail = (TextView) findViewById(R.id.equipments_name_venue_details);
        ivShareList = (ImageView) findViewById(R.id.venue_details_share_icon);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        layout_venue_detail_rating = (RelativeLayout) findViewById(R.id.rating_venue_details_main);
        venue_details_eye_icon = (ImageView) findViewById(R.id.venue_details_eye_icon);
        venue_details_info_title = (TextView) findViewById(R.id.venue_details_info_title);
        add_info_text = (TextView) findViewById(R.id.add_info_text);
        indoor_icon = (ImageView) findViewById(R.id.indoor_image);
        equipments_layout = (RelativeLayout) findViewById(R.id.equipments_layout);
        courtSpaceType = (ImageView) findViewById(R.id.court_icon);
        tv_court_name_venue_detail = (TextView) findViewById(R.id.court_name_venue_detail);
        court_title_venue_detail =  (TextView) findViewById(R.id.court_title_venue_detail);

        /**
         * Function to get Venue courts from server
         */
        snackBar = new SnackBar(this);
        venueDetails = this;

    }

    /**
     * Favorite API call
     *
     * @param user_id
     * @param list_id
     * @param isFavorite
     */
    public void makeListFavorite(String user_id, String list_id, boolean isFavorite) {
        //Handling the loader state
        LoaderUtility.handleLoader(VenueDetails.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, list_id, isFavorite);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(VenueDetails.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    isAddedToFavorite = responseObject.getBoolean("isFavorite");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                VenueDetails.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(VenueDetails.this, false);

                        if (status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                            if (isAddedToFavorite) {
                                snackBar.setSnackBarMessage("Successfully Added to WishList");
                                venue_details_favorite_icon.setFavorite(true);
                                venue_details_favorite_icon.setFavoriteResource(R.mipmap.ic_favorite_orange);
                                favorite_status = true;
                            } else {
                                snackBar.setSnackBarMessage("Successfully Removed from WishList");
                                venue_details_favorite_icon.setFavorite(false);
                                venue_details_favorite_icon.setFavoriteResource(R.mipmap.ic_favorite_gray);
                                favorite_status = false;
                            }

                        } else if (status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)) {
                            snackBar.setSnackBarMessage("Failed to Add to WishList");
                            venue_details_favorite_icon.setFavoriteResource(R.mipmap.ic_favorite_gray);
                            favorite_status = true;
                        } else {
                            snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE_ERR);
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String userId, String objectId, boolean favoriteStatus) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "add_to_favorite").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("object_id", objectId);
        urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_LIST);
        if (favoriteStatus) {
            urlBuilder.addQueryParameter("is_favorite", Boolean.TRUE.toString());
        } else {
            urlBuilder.addQueryParameter("is_favorite", Boolean.FALSE.toString());
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(VenueDetails.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Set images inside view pager to swipe
     *
     * @param images_array
     */
    public void setImagesInViewPager(ArrayList<String> images_array) {
        if (images_array.size() == 1) {
            ImageSlideAdapter imageSlideAdapter = new ImageSlideAdapter(VenueDetails.this, images_array);
            viewpager.setAdapter(imageSlideAdapter);

            leftNav.setVisibility(View.INVISIBLE);
            rightNav.setVisibility(View.INVISIBLE);
        } else if (images_array.size() > 0) {
            ImageSlideAdapter imageSlideAdapter = new ImageSlideAdapter(VenueDetails.this, images_array);
            viewpager.setAdapter(imageSlideAdapter);
        } else {
            //Display place holder image
            leftNav.setVisibility(View.INVISIBLE);
            rightNav.setVisibility(View.INVISIBLE);
            venue_detail_image_frame.setBackgroundResource(R.mipmap.ic_no_image_found);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Constants.FROM_MAP_VIEW) {
            finish();
        } else if (Constants.FROM_WISHLIST) {
            finish();
            Constants.FROM_WISHLIST = false;
            Intent intent = new Intent(VenueDetails.this, MyWishList.class);
            startActivity(intent);
        } else if (isVenueDetailsApiCalled) {
            releseDeeLinkData();
            finish();
            isVenueDetailsApiCalled = false;
            startActivity(new Intent(VenueDetails.this, SplashScreen.class));
        } else {
            finish();
            Intent intent = new Intent(VenueDetails.this, VenueListing.class);
            startActivity(intent);
        }
    }


    /**
     * Relesing the facebook Deep Link Data
     */
    public static void releseDeeLinkData() {
        AppConstants.DEEP_LINKING_LIST_ID = null;
        AppConstants.isListDeepLinkRecieved = false;
    }


    /**
     * Function to get Venue Courts detail from server
     */
    public void getVenuesCourtsDetailsInfor(String court_Id) {
        //Handling the loader state
        LoaderUtility.handleLoader(VenueDetails.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(court_Id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(VenueDetails.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "", venue_id = "", capacity = "", name = "",
                        latitude = "", longitude = "", review_rating = "",
                        review_count = "", court_type = "", description = "",
                        price = "", address = "", city = "", state = "", country = "",
                        eventSportName = "", minimum_age = "",
                        space_type = "", space_desc = "", venueName = "";
                boolean favourite_status = false, isEquipmenmtAvailable = false;
                ;

                ArrayList<String> images_arraylist = new ArrayList<String>();
                ArrayList<String> specs_arraylist = new ArrayList<String>();

                try {
                    //JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    VenueAdapter.venueDetailModelArrayList = new ArrayList<VenueDetailModel>();
                    venueDetailModel = new VenueDetailModel();

                    if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if (jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                    }
                    if (jsonObject.has("capacity") && !jsonObject.isNull("capacity")) {
                        capacity = jsonObject.getString("capacity");
                    }
                    if (jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                    }
                    if (jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                    }
                    if (jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }
                    if (jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }
                    if (jsonObject.has("price") && !jsonObject.isNull("price")) {
                        price = jsonObject.getString("price");
                    }
                    if (jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if (jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }
                    if (jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if (jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if (jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if (jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }
                    if (jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")) {
                        favourite_status = jsonObject.getBoolean("is_favorite");
                    }

                    if (jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")) {
                        eventSportName = jsonObject.getString("sport_name");
                    }
                    if (jsonObject.has("equipments") && !jsonObject.isNull("equipments")) {
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");

                        if (equipmentArray.length() > 0) {
                            isEquipmenmtAvailable = true;
                        }
                    }

                    if (jsonObject.has("images") && !jsonObject.isNull("images")) {
                        JSONArray images_json_array = jsonObject.getJSONArray("images");

                        for (int i = 0; i < images_json_array.length(); i++) {

                            images_arraylist.add(images_json_array.getString(i));
                        }
                        venueDetailModel.setImages_array(images_arraylist);
                    }

                    if(jsonObject.has("minimum_age")&& !jsonObject.isNull("minimum_age")) {
                        minimum_age = jsonObject.getString("minimum_age");
                        minimum_age = minimum_age + " + ";
                    }

                    if(jsonObject.has("space_type")&& !jsonObject.isNull("space_type")) {
                        space_type = jsonObject.getString("space_type");
                    }

                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        String space = "";
                        JSONObject jsonObject_additional_info = jsonObject.getJSONObject("additional_info");

                        if(jsonObject_additional_info.has("space_desc") && !jsonObject_additional_info.isNull("space_desc")){
                            JSONArray spaceArray = jsonObject_additional_info.getJSONArray("space_desc");
                            for (int i = 0; i < spaceArray.length(); i++) {

                                //if(!TextUtils.isEmpty(space)){
                                    space = space + " , " + spaceArray.get(i);
                                //}
                            }

                            if(TextUtils.isEmpty(space)){
                                space_desc = "No Space Available";
                            }else {
                                space_desc = space;
                            }
                        }
                    }

                    if(jsonObject.has("venue_name")&& !jsonObject.isNull("venue_name")) {
                        venueName = jsonObject.getString("venue_name");
                    }

                    venueDetailModel.setId(id);
                    venueDetailModel.setVenue_id(venue_id);
                    venueDetailModel.setName(name);
                    venueDetailModel.setCapacity(capacity);
                    venueDetailModel.setLatitude(latitude);
                    venueDetailModel.setLongitude(longitude);
                    venueDetailModel.setReview_rating(review_rating);
                    venueDetailModel.setReview_count(review_count);
                    venueDetailModel.setPrice(price);
                    venueDetailModel.setCourt_type(court_type);
                    venueDetailModel.setDescription(description);
                    venueDetailModel.setAddress(address);
                    venueDetailModel.setCity(city);
                    venueDetailModel.setState(state);
                    venueDetailModel.setCountry(country);
                    venueDetailModel.setFavourite(favourite_status);
                    venueDetailModel.setEvent_Sports_Type(eventSportName);
                    venueDetailModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    venueDetailModel.setSpace_type(space_type);
                    venueDetailModel.setCourt_min_players(minimum_age);
                    venueDetailModel.setCourt_space_desc(space_desc);
                    venueDetailModel.setVenueName(venueName);
                    VenueAdapter.venueDetailModelArrayList.add(venueDetailModel);

                } catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(VenueDetails.this, false);

                if (isSuccessStatus) {
                    VenueDetails.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //Setting venue details
                            setVenueData();

                            //Initializing the map
                            initMapGps();

                            isVenueDetailsApiCalled = true;
                        }
                    });
                } else {
                    snackBar.setSnackBarMessage("Network error! Please try after sometime");
                }
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String court_Id) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_court_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.VENUE_COURT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", userId);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(Constants.TOTAL_REVIEW_COUNT)) {
            rating_venue_details_reviews.setText(Constants.TOTAL_REVIEW_COUNT + " Reviews");
            rating_venue_details.setRating(Float.parseFloat(Constants.TOTAL_REVIEW_AVERAGE));
            Constants.TOTAL_REVIEW_COUNT = "";
        }
    }

    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayShareDialog(){
        mMaterialDialog = new MaterialDialog(VenueDetails.this);
        View view = LayoutInflater.from(VenueDetails.this)
                .inflate(R.layout.layout_update_card_status,
                        null);
        TextView tv_card_numner = (TextView) view.findViewById(R.id.tv_card_number);
        TextView tv_share_using_facebook = (TextView) view.findViewById(R.id.tv_make_card_primary);
        TextView tv_share_using_sms = (TextView) view.findViewById(R.id.tv_delete_card);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_card_numner.setText("Share Using");
        tv_share_using_facebook.setText("Facebook");
        tv_cancel.setText("CANCEL");
        tv_share_using_sms.setText("SMS");
        tv_share_using_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                //Sharing and deep linking the list details in to facebook
                shareListingDetails();

            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_share_using_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();

                //Sharing the venue details through SMS
                FinishEventCreation.shareDeepLinkUsingSms(venueId,
                        AppConstants.OBJECT_TYPE_LIST,
                        VenueDetails.this, null);


            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }



}
