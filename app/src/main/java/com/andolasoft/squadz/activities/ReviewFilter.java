package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.ReviewDetailModel;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.ReviewDetailAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class ReviewFilter extends AppCompatActivity {

    @InjectView(R.id.review_filter_back_icon_layout)
    RelativeLayout review_filter_back_icon_layout;
    @InjectView(R.id.filter_radio_by_date)
    RadioButton filter_radio_by_date;
    @InjectView(R.id.filter_radio_by_ratings)
    RadioButton filter_radio_by_ratings;
    @InjectView(R.id.review_filter_apply_btn)
    Button review_filter_apply_btn;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    SnackBar snackBar;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_review_filter_layout);

        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        /**
         * Adding click events on the views
         */
        addClickEvents();

    }

    /**
     * Adding click events on the views
     */
    public void addClickEvents() {
        review_filter_back_icon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        filter_radio_by_date.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Constants.REVIEW_FILTER_TYPE = "date";
                }
            }
        });
        filter_radio_by_ratings.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Constants.REVIEW_FILTER_TYPE = "ratings";
                }
            }
        });

        review_filter_apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.REVIEW_FILTER_BTN_CLICKED = true;
                finish();
            }
        });
    }

    /**
     * API integration for display Questions with Answers
     */
    public void displayFAQ() {
        //Handling the loader state
        LoaderUtility.handleLoader(ReviewFilter.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(ReviewFilter.this, false); 
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                final ArrayList<String> faq_arrayList = new ArrayList<String>();

                try {


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(ReviewFilter.this, false); 

                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_faq").newBuilder();

        String url = urlBuilder.build().toString();

        /*Request request = new Request.Builder()
                .url(url)
                .method("GET", RequestBody.create(null, new byte[0]))
                .build();*/
        Request request = new Request.Builder()
                .url(url)
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(ReviewFilter.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {

        ButterKnife.inject(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        snackBar = new SnackBar(this);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(ReviewFilter.this);
        editor = preferences.edit();


        if(Constants.REVIEW_FILTER_TYPE.equalsIgnoreCase("date"))
        {
            filter_radio_by_date.setChecked(true);
            filter_radio_by_ratings.setChecked(false);
        }
        else{
            filter_radio_by_date.setChecked(false);
            filter_radio_by_ratings.setChecked(true);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

