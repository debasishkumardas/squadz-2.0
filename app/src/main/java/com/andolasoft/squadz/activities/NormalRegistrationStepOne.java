 package com.andolasoft.squadz.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.provider.*;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.ActionSheetDialog;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.OnOperItemClickL;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

 public class NormalRegistrationStepOne extends AppCompatActivity {

    @InjectView(R.id.username)
    EditText et_username;
    @InjectView(R.id.password_txt)
    EditText et_password;
    @InjectView(R.id.confirm_password_txt)
    EditText et_confPassword;
    @InjectView(R.id.primary_sports_txt)
    EditText et_selectsport;
    @InjectView(R.id.signup_btn_btn)
    Button btn_signup;
    @InjectView(R.id.login)
    TextView btn_backToLogin;
    @InjectView(R.id.btn_back)
    ImageView btnBack;
    @InjectView(R.id.loginwithfacebook)
    LoginButton btn_fblogin;
    ArrayList <String> sportNameArray = null;
    ArrayList <String> sportIdArray = null;
    String[] sportStringArray = null;
    String[] getSportName = new String[AppConstants.SPORT_ARRAY.length()];
    int clickedPosition = 0;
    SnackBar snackBar = new SnackBar(NormalRegistrationStepOne.this);
    boolean isUserExist = false;
    public static String mUserName, mPassword, mConfPassword, mPrimarySport,mSportId;
    DatabaseAdapter db;
    Double currLoc_lat, currloc_lon;
    private ProfileTracker profileTracker;
    CallbackManager callbackManager ;
    private AccessTokenTracker accessTokenTracker;
    public static String android_device_id = null;
    public static String fb_user_id, fb_user_name, fb_user_email, fb_user_image,
            fb_user_gender, fb_user_firstname, fb_user_lastname;
    public static final String FACEBOOK_API_CALL_PERMISSIONs = "id,email,gender,name, first_name, last_name, birthday";
    SnackBar snackbar = new SnackBar(NormalRegistrationStepOne.this);
    ProgressDialog dialog = null;
    public static NormalRegistrationStepOne mNormalsignupContext;
    int pageRedirectionCount = 0;
     SharedPreferences profileSp;
     SharedPreferences.Editor profileEditor;
     String logInType = "";
     boolean isLocationDialogDisplayed = false;
     private String blockCharacterSet = "~#^|$%&*!";

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken.getCurrentAccessToken().getToken();
            AccessToken accessToken = loginResult.getAccessToken();
           /* Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);*/

            if(Profile.getCurrentProfile() == null) {
                profileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                        // profile2 is the new profile
                        Log.v("facebook - profile", profile2.getFirstName());
                        profileTracker.stopTracking();
                    }
                };
            }
            else {
                Profile profile = Profile.getCurrentProfile();
            }
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_new_signup);
        ButterKnife.inject(this);

        et_username.setFilters(new InputFilter[] { filter });

        //Getting the activity context
        mNormalsignupContext = this;

        //Function to get the SportNames

        if(AppConstants.SPORT_ARRAY.length() > 0){
            getALlSportNames();
        }

        //initializing the preferance
        profileSp = getSharedPreferences("SignupData", MODE_PRIVATE);
        profileEditor = profileSp.edit();


        //Hiding the ActionBar
        hideActionBar();

        //Hiding the Soft Keyboard
        ApplicationUtility.hideSoftKeyboard(NormalRegistrationStepOne.this);

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(NormalRegistrationStepOne.this);

        //Initializing the Datavbase c;ass
        db = new DatabaseAdapter(NormalRegistrationStepOne.this);

        //Handling the Space in the UserName field
        this.et_username.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                String result = s.toString().replaceAll(" ", "");
                if(!s.toString().equals(result)) {
                    NormalRegistrationStepOne.this.et_username.setText(result);
                    NormalRegistrationStepOne.this.et_username.setSelection(result.length());
                }

            }
        });

        this.btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Checking the validation and redirecting the user
                validateUserData();
            }
        });

        this.et_selectsport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_selectsport.setFocusable(true);
                if(AppConstants.SPORT_ARRAY.length() > 0){
                    //Sports Filter
                    sportsDialog();
                }else{
                    snackBar.setSnackBarMessage("No Sports Found");
                }
            }
        });

        btn_backToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NormalRegistrationStepOne.this.finish();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NormalRegistrationStepOne.this.finish();
            }
        });

        //Integrating click event on the keyboard done button
        et_confPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    // Validating user entered data
                    et_selectsport.setFocusable(true);
                    if(AppConstants.SPORT_ARRAY.length() > 0){
                        //Sports Filter
                        sportsDialog();
                    }else{
                        snackBar.setSnackBarMessage("No Sports Found");
                    }
                }
                return false;
            }
        });

        //Configuring the facebook Ligin button
        configureFacebookLoginButton();

        //Restoring the uuser input data
        restoreData();


        //Getting the current Location
        getCurrentLocation();

        //Function to handle the facebook callbacks
        facebookStratTracking();
    }


    /**
     * Function to handle the facebook callbacks
     */
    public void facebookStratTracking(){
        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
                pageRedirectionCount = pageRedirectionCount + 1;
                AccessToken accessToken = newToken;
                Profile profile = Profile.getCurrentProfile();
                displayMessage(profile);
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
    }

    /**
     * Hiding the ActionBar
     */
    public void hideActionBar(){
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.hide();
    }

    /**
     * Configuring all the permissions for the facebook user data retrival
     */
    public void configureFacebookLoginButton(){
        //btn_fblogin.setBackgroundResource(0);
        btn_fblogin.setCompoundDrawablesWithIntrinsicBounds(null, null,
                null, null);
        btn_fblogin.setText("Login with Facebook");
        btn_fblogin.setCompoundDrawablePadding(0);
        btn_fblogin.setReadPermissions("public_profile email");
        callbackManager = CallbackManager.Factory.create();
        btn_fblogin.registerCallback(callbackManager, callback);

    }


    /**
     * Checking the validation and redirecting the user
     */
    public void validateUserData(){
        mUserName = et_username.getText().toString().trim();
        mPassword = et_password.getText().toString().trim();
        mConfPassword = et_confPassword.getText().toString().trim();
        mPrimarySport = et_selectsport.getText().toString().trim();

        if(TextUtils.isEmpty(mUserName)) {
            snackBar.setSnackBarMessage("Please enter username");
        } else if(TextUtils.isEmpty(mPassword)) {
            snackBar.setSnackBarMessage("Please enter password");
        } else if(mPassword.length() < 8) {
            snackBar.setSnackBarMessage("The password must be min. 8 charcters");
        } else if(TextUtils.isEmpty(mConfPassword)) {
            snackBar.setSnackBarMessage("Please enter confirm password");
        } else if(!mPassword.equalsIgnoreCase(mConfPassword)) {
            snackBar.setSnackBarMessage("Confirm password does not match");
        } else if(TextUtils.isEmpty(mPrimarySport)) {
            snackBar.setSnackBarMessage("Please select your primary sport");
        } else  {
            checkUserName(mUserName);
        }
    }


    /***
     * display  list dialog
     ***/
    private void sportsDialog() {
        //sport = sportNameArray.toArray(sportNameArray);
        getSportName = getALlSportNames();
        final ActionSheetDialog dialog = new ActionSheetDialog(NormalRegistrationStepOne.this,
                getSportName, null);
        dialog.title("Select Sport")
                .titleTextSize_SP(15.5f)
                .show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long _id) {
                dialog.dismiss();
                clickedPosition = position;
                String mgetSportName = getSportName[position];
                mSportId = db.getcurrentSportId(mgetSportName);
                if(mgetSportName != null &&
                        mgetSportName.equalsIgnoreCase("Custom")){
                    openDialog();
                }else{
                    et_selectsport.setText(mgetSportName);
                }
            }
        });

    }

    /**
     * Getting all the sports Names
     * @return
     */
    public String[] getALlSportNames(){
        sportStringArray = new String[AppConstants.SPORT_ARRAY.length() - 1];
        sportNameArray = new ArrayList<>();
        sportIdArray = new ArrayList<>();
        for(int i = 0; i < AppConstants.SPORT_ARRAY.length(); i++){
            try{
                JSONObject sportObject = AppConstants.SPORT_ARRAY.getJSONObject(i);
                String sportName = sportObject.getString("name");
                String sportId = sportObject.getString("id");

                if(sportName != null &&
                        !sportName.equalsIgnoreCase("None")&&
                        !sportName.equalsIgnoreCase("All")){
                    sportNameArray.add(sportName);
                    sportIdArray.add(sportId);
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
        return sportStringArray = sportNameArray.toArray(sportStringArray);
    }


    /**
     * Function to Check username Existance
     * @param userName
     */
    public void checkUserName(String userName){

        final ProgressDialog dialog = ProgressDialog
                .show(NormalRegistrationStepOne.this, "", "Please wait ...Validating username");
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setCancelable(false);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(userName);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialog.dismiss();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    String status = responseObject.getString("status");
                    if(status != null && status.equalsIgnoreCase("Success")) {
                        isUserExist = responseObject.getBoolean("is_available");
                    } else if(status != null && status.equalsIgnoreCase("error")) {

                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                }

                //Handling the loader state
                NormalRegistrationStepOne.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        if(!isUserExist){
                            snackBar.setSnackBarMessage("UserName Already Taken. Please choose another one");
                        }else{
                            profileEditor.putString(Constants.PARAM_USERNAME, mUserName);
                            profileEditor.putString(Constants.PARAM_PASSWORD, mPassword);
                            profileEditor.putString(Constants.PARAM_PRIMATY_SPORT,
                                    mPrimarySport);
                            profileEditor.commit();
                            Intent intent = new Intent(NormalRegistrationStepOne.this,NormalRegistrationStepTwo.class);
                            startActivity(intent);
                        }
                    }
                });
            }
        });
    }



    public Request buildApiRequestBody(String user_name){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"check_username?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, user_name);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Restoring the user input data
     */
    public void restoreData(){
        if(TextUtils.isEmpty(mUserName)){
            et_username.setText(mUserName);
            et_password.setText(mPassword);
            et_confPassword.setText(mConfPassword);
            et_selectsport.setText(mPrimarySport);
        }
    }

    /**
     * Function responsible for getting the current location latitude and longitude
     */
    public void getCurrentLocation(){
        GPSTracker locationTracker = new GPSTracker(NormalRegistrationStepOne.this);

        if(locationTracker.canGetLocation()) {
            currLoc_lat = locationTracker.getLatitude();
            currloc_lon = locationTracker.getLongitude();
        } else {
            locationTracker.displayPromptForEnablingGPS(NormalRegistrationStepOne.this);
        }
    }


    /**
     * Getting data from Facebook
     * @param profile User facebook Profile Object
     */

    private void displayMessage(final Profile profile){
        if(profile != null){
            Bundle params = new Bundle();
            params.putString("fields", FACEBOOK_API_CALL_PERMISSIONs);
            new GraphRequest(AccessToken.getCurrentAccessToken(), "me", params, HttpMethod.GET,
                    new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse response) {
                            if (response != null) {
                                try {
                                    JSONObject data = response.getJSONObject();

                                    //Getting user_id
                                    fb_user_id = data.getString("id");

                                    //Getting Email
                                    if(data.has("email")){
                                        fb_user_email = data.getString("email");
                                    }else{
                                        fb_user_email = null;
                                    }

                                    //Getting Fullname
                                    fb_user_name = data.getString("name");

                                    //Getting gender
                                    fb_user_gender = data.getString("gender");

                                    //Getting firstname
                                    fb_user_firstname = data.getString("first_name");

                                    //Getting lastname
                                    fb_user_lastname = data.getString("last_name");


                                    //Building the Facebook user Profile Picture
                                    fb_user_image = AppConstants.FACEBOOK_USER_IMAGE_BASE_URL+ fb_user_id
                                            +AppConstants.FACEBOOK_USER_IMAGE_OFFSET_URL;

                                    //Logging out from the instance
                                    LoginManager.getInstance().logOut();

                                    //Calling the API for Facebook Login
                                    facebookUserLoginTask(fb_user_id, fb_user_name, fb_user_image, fb_user_email,
                                            fb_user_firstname,fb_user_lastname);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).executeAsync();
        }
    }


    /**
     *
     * Facebook User Login task
     * @param fb_userId_ Facebook User id
     * @param fb_userName Facebook User full name
     * @param fb_userImage Facebook User Image
     * @param fb_userEmail Facebook User Email
     * @param fb_userfirstname Facebook User FirstName
     * @param fb_userlastname Facebook User LastName
     */
    public void facebookUserLoginTask(String fb_userId_, String fb_userName,
                                      String fb_userImage, String fb_userEmail,
                                      String fb_userfirstname, String fb_userlastname){

        String message = null;

        //Handling the loader state
        LoaderUtility.handleLoader(NormalRegistrationStepOne.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildFacebookLoginApiRequestBody(fb_userId_, fb_userName, fb_userImage,
                fb_userEmail,fb_userfirstname,fb_userlastname);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(NormalRegistrationStepOne.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    String err_msg = responseObject.getString("status");
                    String facebook_id = responseObject.getString("fb_id");
                    //String message = responseObject.getString("message");
                    if (err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                        //Parsing the data from the JSON object
                        String auth_token = responseObject.getString("auth_token");
                        String device_info_id = responseObject.getString("device_info_id");
                        String user_id = responseObject.getString("user_id");
                        String phone_number = responseObject.getString("phone_number");
                        String firstname = responseObject.getString("first_name");
                        String lastname = responseObject.getString("last_name");
                        String current_status = responseObject.getString("current_status");
                        String user_email = responseObject.getString("email");
                        String birth_day = responseObject.getString("birth_date");
                        String image = responseObject.getString("image");
                        String user_sport = responseObject.getString("sport");
                        String userName = responseObject.getString("username");
                        int game_invitation_count = responseObject
                                .getInt("game_invitation_count");
                        int friend_invitation_count = responseObject
                                .getInt("friend_invitation_count");
                        boolean recive_push_notification = (Boolean) responseObject
                                .get("receive_push_notification");
                        JSONObject array = responseObject.getJSONObject("notifications");
                        boolean player_request = (Boolean) array.get("player_request");
                        boolean game_invitation = (Boolean) array.get("game_invitation");
                        boolean nearby_games = (Boolean) array.get("nearby_game");
                        boolean nearby_games_all = (Boolean) array.get("nearby_game_all");
                        boolean nearby_games_primary = (Boolean) array
                                .get("nearby_game_primary");
                        boolean message_board = (Boolean) array.get("message_board");
                        boolean game_update = (Boolean) array.get("game_update");
                        boolean comment_on_post = (Boolean) array.get("comment_on_post");
                        boolean game_cancellation = (Boolean) array.get("game_cancel");
                        boolean game_edit = (Boolean) array.get("game_edit");
                        boolean game_join_leave = (Boolean) array.get("game_join_leave");

                        //Keeping the login status either from Facebook or Normal login
                        logInType = "FACEBOOK_LOGIN";

                        //Storing the data in the Shatred Preferance
                        storeDatainSharedPreferance(auth_token,userName,device_info_id,
                                user_id, firstname,lastname, image, user_email, phone_number, facebook_id);

                        //Stroing the user info for quick access
                        userInfoQuickAccess(auth_token,user_id,userName);

                        //Redirecting the user to the Home Svreen
                        redirectUserHomePage();

                    } else if(err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_CREATE_FAILED)) {
                            redirectUserSignup();
                        //Theating this user as a new user in the APP. Redirecting the user to the sign up screen
                    }else if(err_msg.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the Error Message
                        String message = responseObject.getString("message");
                        snackbar.setSnackBarMessage(message);
                    }else{
                        snackbar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                //Handling the loader state
                LoaderUtility.handleLoader(NormalRegistrationStepOne.this, false);
            }
        });
    }

    /**
     * Redirecting the user to the signup screen
     */
    public void redirectUserHomePage(){
        NormalRegistrationStepOne.this.finish();
        //SigninActivity.mcontext.finish();
        Intent intent = new Intent(NormalRegistrationStepOne.this,
                NavigationDrawerActivity.class);
        startActivity(intent);
    }


    /**
     * Redirecting the user to the signup screen
     */
    public void redirectUserSignup(){
        pageRedirectionCount = pageRedirectionCount + 1;

        if(pageRedirectionCount == 1){
            NormalRegistrationStepOne.this.finish();
            Intent intent = new Intent(NormalRegistrationStepOne.this,
                    FacebookUserRegistrationStepOne.class);
            startActivity(intent);
        }

    }


    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(NormalRegistrationStepOne.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }


    /**
     * Redirect the user to the next screen
     */
    /*public void redirectUser(){
        NormalRegistrationStepOne.this.finish();
        Intent intent = new Intent(NormalRegistrationStepOne.this,NavigationDrawerActivity.class);
        startActivity(intent);
    }*/

    /**
     * Storing the user details to get a quick access
     * @param auth_token Logged in user auth token
     * @param user_id Logged in User user id
     * @param user_name logged in user user name
     */
    public void userInfoQuickAccess(String auth_token, String user_id,
                                    String user_name){
        AppConstants.AUTH_TOKEN = auth_token;
        AppConstants.USERNAME = user_name;
        AppConstants.LOGGEDIN_USERID = user_id;
    }

    /**
     * Keeping the user data in the shared preferance after successful login
     * @param auth_token user auth_token
     * @param mUsername user Username
     * @param device_info_id user device infoid
     * @param user_id user user_id
     * @param firstname user firstname
     * @param lastname user lastname
     * @param image user Profile image
     */

    public void storeDatainSharedPreferance(String auth_token,String mUsername,
                                            String device_info_id, String user_id,
                                            String firstname, String lastname,
                                            String image, String userEmail,
                                            String userPhone, String facebook_id){
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(NormalRegistrationStepOne.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("auth_token", auth_token);
        editor.putString("USERNAME", mUsername);
        editor.putString("device_info_id", device_info_id);
        editor.putString("loginuser_id", user_id);
        editor.putString("username", mUsername);
        editor.putString("firstname", firstname);
        editor.putString("lastname", lastname);
        editor.putString("image", image);
        editor.putString("user_email", userEmail);
        editor.putString("user_phone", userPhone);
        editor.putString("LOGIN_TYPE", logInType);
        editor.putString("Facebook_Id", facebook_id);
        editor.commit();
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildFacebookLoginApiRequestBody(String fb_userId, String fb_userName,
                                                            String fb_userImage, String fb_userEmail,
                                                            String fb_userfirstname,String fb_userlastname){

        //Building the API with all the required Parameters
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"fblogin?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_UID, fb_userId);
        urlBuilder.addQueryParameter(Constants.PARAM_FIRST_NAME, fb_userfirstname);
        urlBuilder.addQueryParameter(Constants.PARAM_LAST_NAME, fb_userlastname);
        urlBuilder.addQueryParameter(Constants.PARAM_FCMID, SigninActivity.FCM_REGISTRATION_ID);
        urlBuilder.addQueryParameter(Constants.PARAM_EMAIL, fb_userEmail);
        urlBuilder.addQueryParameter(Constants.PARAM_PROVIDER, Constants.VALUE_SOCIAL_USER_DATA_PROVIDER);
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETOKEN, SigninActivity.getDeviceToken(NormalRegistrationStepOne.this));
        urlBuilder.addQueryParameter(Constants.PARAM_DEVICETYPE, Constants.DEVICE_TYPE);
        urlBuilder.addQueryParameter(Constants.PARAM_LOCATION, buildLocation());
        urlBuilder.addQueryParameter(Constants.PARAM_TIMEZONE, SigninActivity.getDeviceTimeZone());
        urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, fb_userImage);
        String url = urlBuilder.build().toString();

        //Building the request
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to get the location
     * @return location
     */
    public String buildLocation(){
        return String.valueOf(SigninActivity.currLocLatitude) +","+
                String.valueOf(SigninActivity.currLocLongitude);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(!ApplicationUtility.isConnectingToInternet(NormalRegistrationStepOne.this))
        {
            snackBar.setSnackBarMessage("Please check your internet connection");
        }

    }


    private void openDialog(){
        LayoutInflater inflater = LayoutInflater.from(NormalRegistrationStepOne.this);
        View subView = inflater.inflate(R.layout.dialog_custom_gamename, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialog_edittext);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Custom Game Name");
        builder.setMessage("");
        builder.setView(subView);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String custom_game_name = subEditText.getText().toString();
                if(!TextUtils.isEmpty(custom_game_name)){
                    et_selectsport.setText(custom_game_name);
                }else{
                    snackBar.setSnackBarMessage("Please enter name for your custom game");
                }


            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

     @Override
     protected void attachBaseContext(Context newBase) {
         super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
     }

     private InputFilter filter = new InputFilter() {

         @Override
         public CharSequence filter(CharSequence source, int start,
                                    int end, Spanned dest, int dstart, int dend) {

             if (source != null && blockCharacterSet.contains(("" + source))) {
                 return "";
             }
             return null;
         }
     };

 }
