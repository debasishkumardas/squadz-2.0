package com.andolasoft.squadz.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.managers.LocationPickerManager;
import com.andolasoft.squadz.models.VenueDetailModel;
import com.andolasoft.squadz.models.VenueModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class VenueMapView extends FragmentActivity implements OnMapReadyCallback {

    public static VenueMapView venueMapView;
    JSONArray venue_listing_array = null;
    ImageView ic_back_list, imgv_filter;
    ArrayList<String> latArray = null;
    ArrayList<String> lonArray = null;
    Marker venueMark;
    List<Marker> markers = new ArrayList<Marker>();
    VenueDetailModel venueDetailModel;
    Dialog dialog;
    SnackBar snackBar = new SnackBar(VenueMapView.this);
    String venueImage, venueReviews, venueAmount, venueMiles, venueReviewsCount, venueName, venueLat, venueLong;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private View rootView;
    private MapView mapView;
    private GPSTracker gpsTracker;
    private Double current_lat, current_long;
    private boolean gps_enabled = false;
    private GoogleMap mMap;
    private Spinner spinner_venue_map_list;
    private RelativeLayout ic_back_layout;
    private View v;
    private VenueModel venueModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_map_view);

        /**
         * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
         */
        initReferences();

        /**
         * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
         */
        isLocationEnabled();

        /**
         * INITIALIZING MAP WITH GPS TRACKER
         */
        initMapGps();

        /**
         * KEEPING APPLETON NNFAME INSIDE THE SPINNER
         */
        //setAppletonType();

        setVenueAppletonType();

        /**
         * Added click events on the views
         */
        setOnClickEvents();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (gps_enabled) {
            LatLng lat_lng = new LatLng(current_lat, current_long);
            // mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory
            //   .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)).position(lat_lng).title(current_lat.toString() + "," + current_long.toString()));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat_lng, 15));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.isMyLocationEnabled();
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.clear();
            //Placing the markers in the map view
            setMarkers();
            //mMap.setMyLocationEnabled(true);

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @SuppressLint("InflateParams")
                public View getInfoContents(final Marker arg0) {
                    v = getLayoutInflater().inflate(
                            R.layout.map_info_window, null);
                    try {
                        ImageView imgv_venue_image = (ImageView) v.findViewById(R.id.venue_image_infowindow);
                        TextView tv_venueMile = (TextView) v.findViewById(R.id.text_venue_miles);
                        TextView tv_venuerating = (TextView) v.findViewById(R.id.rating_venue_reviews);
                        TextView tv_venuePrice = (TextView) v.findViewById(R.id.venue_price);
                        TextView tv_Venue_name = (TextView) v.findViewById(R.id.venue_name);
                        RatingBar ratReviewBar = (RatingBar) v.findViewById(R.id.rating_venue);


                        tv_venueMile.setText(venueMiles + " miles away");
                        tv_venuerating.setText(venueReviewsCount+" Reviews");

                        //tv_venueMile.setText("0.4 miles");
                        //tv_venuerating.setText(venueReviewsCount + " Reviews");

                        tv_Venue_name.setText(venueName);
                        if (!TextUtils.isEmpty(venueAmount)) {
                            tv_venuePrice.setText("$ " + venueAmount);
                        } else {
                            tv_venuePrice.setText("FREE");
                        }
                        ratReviewBar.setRating(Float.valueOf(venueReviews));

                        if (!TextUtils.isEmpty(venueImage)) {
                            // Glide.with(VenueMapView.this).load(venueImage).into(imgv_venue_image);

                            //Glide.with(VenueMapView.this).load(venueImage).override(50,50).into(imgv_venue_image);

                            Glide.with(VenueMapView.this).load(venueImage).asBitmap().override(200, 150).listener(new RequestListener<String, Bitmap>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                    e.printStackTrace();
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    if (!isFromMemoryCache) arg0.showInfoWindow();
                                    return false;
                                }
                            }).into(imgv_venue_image);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    return v;
                }
            });


            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String title = marker.getTitle();
                    setMarketInfoWindow(title);
                    return false;
                }
            });

            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    String title = marker.getTitle();
                    getVenuesCourtsDetailsInfor(title);
                }
            });

            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    //mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 15));
                    // mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

                    if (markers.size() != 0) {
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (Marker marker : markers) {
                            builder.include(marker.getPosition());
                        }
                        LatLngBounds bounds = builder.build();
                        int padding = 150; // offset from edges of the map in
                        // pixels
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(
                                bounds, padding);
                        mMap.moveCamera(cu);
                        markers.clear();
                    }
                }
            });
        }

    }

    /**
     * INITIALIZING MAP WITH GPS TRACKER
     */
    public void initMapGps() {
        gpsTracker = new GPSTracker(this);

        if (!gps_enabled) {
            Toast.makeText(VenueMapView.this, "please enable your location service", Toast.LENGTH_SHORT).show();
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
        } else {
            if (gpsTracker.canGetLocation()) {
                current_lat = gpsTracker.getLatitude();
                current_long = gpsTracker.getLongitude();
            }
        }


        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
    }

    /**
     * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
     *
     * @return
     */
    public boolean isLocationEnabled() {
        gps_enabled = false;
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gps_enabled;
    }

    /**
     * KEEPING APPLETON NAME INSIDE THE SPINNER
     */
    public void setAppletonType() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(VenueMapView.this, android.R.layout.simple_spinner_item, VenueListing.currLocItems);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_venue_map_list.setAdapter(dataAdapter);
        spinner_venue_map_list.setSelection(0);
    }

    /**
     * Added click events on the views
     */
    public void setOnClickEvents() {

        //Action bar back icon click
        ic_back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //VenueListing.isApiCalled = false;
                finish();
                //Intent intent_availability = new Intent(VenueMapView.this, VenueListing.class);
                //startActivity(intent_availability);
            }
        });

        ic_back_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // VenueListing.isApiCalled = false;
                VenueMapView.this.finish();
            }
        });

        imgv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_availability = new Intent(VenueMapView.this, Filter.class);
                startActivity(intent_availability);
            }
        });

        spinner_venue_map_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(VenueListing.this, "Catch it!", Toast.LENGTH_SHORT).show();
                    LocationPickerManager locationPickerManager
                            = new LocationPickerManager(VenueMapView.this);
                }
                return true;
            }
        });


    }

    /**
     * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
     */
    public void initReferences() {
        spinner_venue_map_list = (Spinner) findViewById(R.id.spinner_venue_map_list);
        ic_back_layout = (RelativeLayout) findViewById(R.id.back_icon_venue_mapview_layout);
        ic_back_list = (ImageView) findViewById(R.id.book_icon_venue);
        imgv_filter = (ImageView) findViewById(R.id.venue_filter_icon);

        venueMapView = this;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // Intent intent_availability = new Intent(VenueMapView.this, VenueListing.class);
        //startActivity(intent_availability);
    }


    /**
     * @param courtId
     */
    public void setMarketInfoWindow(String courtId) {
        try {
            for (int i = 0; i < VenueListing.venueModelArrayList.size(); i++) {
                String venueId = VenueListing.venueModelArrayList.get(i).getVenueId();
                if (venueId.equalsIgnoreCase(courtId)) {
                    venueImage = VenueListing.venueModelArrayList.get(i).getVenueImage();
                    venueReviews = VenueListing.venueModelArrayList.get(i).getVenueRatings();
                    venueAmount = VenueListing.venueModelArrayList.get(i).getVenuePrice();
                   // venueMiles = VenueListing.venueModelArrayList.get(i).getVenueMiles();
                    venueReviewsCount = VenueListing.venueModelArrayList.get(i).getReview_count();
                    venueName = VenueListing.venueModelArrayList.get(i).getVenueName();
                    venueLat = VenueListing.venueModelArrayList.get(i).getLatitude();
                    venueLong = VenueListing.venueModelArrayList.get(i).getLongitude();
                    double miles = ApplicationUtility.distance(gpsTracker.getLatitude(),gpsTracker.getLongitude(),
                            Double.parseDouble(venueLat), Double.parseDouble(venueLong));
                    venueMiles = String.valueOf(miles);
                }
            }
        } catch (Exception ex) {
            snackBar.setSnackBarMessage("Unable to get court data");
        }

    }

    public void setMarkers() {
        for (int i = 0; i < VenueListing.venueModelArrayList.size(); i++) {
            String venuelat = VenueListing.venueModelArrayList.get(i).getLatitude();
            String venuelon = VenueListing.venueModelArrayList.get(i).getLongitude();
            String venueId = VenueListing.venueModelArrayList.get(i).getVenueId();

            if (!TextUtils.isEmpty(venuelat) ||
                    !TextUtils.isEmpty(venuelon)) {
                LatLng pinLocation = new LatLng(Double.valueOf(venuelat), Double.valueOf(venuelon));

                venueMark = mMap
                        .addMarker(new MarkerOptions()
                                .position(pinLocation)
                                .title(venueId)
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                markers.add(venueMark);
            }
        }
    }


    /**
     * Function to get Venue Courts detail from server
     */
    public void getVenuesCourtsDetailsInfor(final String court_Id) {
        //Handling the loader state
        LoaderUtility.handleLoader(VenueMapView.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(court_Id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(VenueMapView.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "",venue_id ="",capacity = "", name = "",
                        latitude = "", longitude = "",review_rating = "",
                        review_count = "",court_type = "",description = "",
                        price = "",address = "",city = "",state = "",country = "",
                        eventSportName = "",cancel_policy_type = "",cancellation_point = "";
                boolean favourite_status = false,isEquipmenmtAvailable = false;;

                ArrayList<String> images_arraylist = new ArrayList<String>();
                ArrayList<String> specs_arraylist = new ArrayList<String>();
                VenueAdapter.user_review_arrayList = new ArrayList<String>();

                try {
                    // JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    VenueAdapter.venueDetailModelArrayList = new ArrayList<VenueDetailModel>();

                    venueDetailModel = new VenueDetailModel();

                    if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if (jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if (jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                    }
                    if (jsonObject.has("capacity") && !jsonObject.isNull("capacity")) {
                        capacity = jsonObject.getString("capacity");
                    }
                    if (jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                    }
                    if (jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                    }
                    if (jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }

                    if (jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }
                    if (jsonObject.has("price") && !jsonObject.isNull("price")) {
                        price = jsonObject.getString("price");
                    }
                    if (jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if (jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }

                    if(jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if(jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if(jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }
                    if(jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")) {
                        favourite_status = jsonObject.getBoolean("is_favorite");
                    }

                    if(jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")){
                        eventSportName = jsonObject.getString("sport_name");
                    }
                    if(jsonObject.has("equipments") && !jsonObject.isNull("equipments")){
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");
                        if(equipmentArray.length() > 0){
                            isEquipmenmtAvailable = true;
                        }
                    }

                    if(jsonObject.has("images")&& !jsonObject.isNull("images")) {
                        JSONArray images_json_array = jsonObject.getJSONArray("images");
                        for (int i = 0; i < images_json_array.length(); i++) {
                            images_arraylist.add(images_json_array.getString(i));
                        }
                        venueDetailModel.setImages_array(images_arraylist);
                    }

                    /**Fetching user review & save in a arraylist
                     * for prefilling in the Add review page for again updating the review*/
                    if(jsonObject.has("user_review")&& !jsonObject.isNull("user_review")) {
                        JSONObject user_review_object = jsonObject.getJSONObject("user_review");
                        if(user_review_object.length() > 0) {
                            String user_rating = user_review_object.getString("rating");
                            String user_review = user_review_object.getString("review");
                            VenueAdapter.user_review_arrayList.add(user_rating + "@@@@" + user_review);
                        }
                    }

                    if(jsonObject.has("cancel_policy_type")&& !jsonObject.isNull("cancel_policy_type")) {
                        cancel_policy_type = jsonObject.getString("cancel_policy_type");
                    }

                    if(jsonObject.has("cancellation_point")&& !jsonObject.isNull("cancellation_point")) {
                        cancellation_point = jsonObject.getString("cancellation_point");
                    }

                    venueDetailModel.setId(id);
                    venueDetailModel.setVenue_id(venue_id);
                    venueDetailModel.setName(name);
                    venueDetailModel.setCapacity(capacity);
                    venueDetailModel.setLatitude(latitude);
                    venueDetailModel.setLongitude(longitude);
                    venueDetailModel.setReview_rating(review_rating);
                    venueDetailModel.setReview_count(review_count);
                    venueDetailModel.setPrice(price);
                    venueDetailModel.setCourt_type(court_type);
                    venueDetailModel.setDescription(description);
                    venueDetailModel.setAddress(address);
                    venueDetailModel.setCity(city);
                    venueDetailModel.setState(state);
                    venueDetailModel.setCountry(country);
                    venueDetailModel.setFavourite(favourite_status);
                    venueDetailModel.setEvent_Sports_Type(eventSportName);
                    venueDetailModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    venueDetailModel.setCancel_policy_type(cancel_policy_type);
                    venueDetailModel.setCancellation_point(cancellation_point);
                    VenueAdapter.venueDetailModelArrayList.add(venueDetailModel);

                } catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(VenueMapView.this, false);

                if (isSuccessStatus) {
                    VenueMapView.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            VenueAdapter.venueId = court_Id;
                            Constants.FROM_MAP_VIEW = true;

                            Intent intent = new Intent(VenueMapView.this, VenueDetails.class);
                            startActivity(intent);
                        }
                    });
                } else {
                    snackBar.setSnackBarMessage("Network error! Please try after sometime");
                }
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String court_Id) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_court_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.VENUE_COURT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", VenueAdapter.userId);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(VenueMapView.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Google", "Place Selected: " + place.getName());
                HomeFragment.address = place.getAddress().toString();
                String latLng = place.getLatLng().toString();
                String[] latlngArr = latLng.split(" ");
                String latlngVal = latlngArr[1].substring(1, latlngArr[1].length() - 1);
                String[] finalArray = latlngVal.split(",");
                HomeFragment.currLocLatitude = Double.parseDouble(finalArray[0].toString());
                HomeFragment.currLocLongitude = Double.parseDouble(finalArray[1].toString());
                AppConstants.isLocationPicked = true;
                AppConstants.isLocationPicked_venue_mapview = true;
                AppConstants.isLocationPicked_venue_listing = true;
                VenueListing.isApiCalled = false;
                HomeFragment.getCurrentAddress();
                setVenueAppletonType();
                // Display attributions if required.
                CharSequence attributions = place.getAttributions();
                if (!TextUtils.isEmpty(attributions)) {
                    //mPlaceAttribution.setText(Html.fromHtml(attributions.toString()));
                } else {
                    //mPlaceAttribution.setText("");
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("Google", "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }
    }

    /**
     * KEEPING VENUE APPLETON NAME INSIDE THE SPINNER
     */
    public void setVenueAppletonType() {

       /* //Getting venue name & id as a array to display inside the spinner
        spinnerArray = new String[venue_spinner_id_array.size()];
        map_venue_name_spiner = new HashMap<String, String>();

        for (int i = 0; i < venue_spinner_id_array.size(); i++) {
            map_venue_name_spiner.put(venue_spinner_name_array.get(i), venue_spinner_id_array.get(i));
            spinnerArray[i] = venue_spinner_name_array.get(i);
        }*/

        VenueListing.currLocItems = new ArrayList<>();
        VenueListing.currLocItems.add(HomeFragment.current_city);
        //Displaying name inside spinner
        setAppletonType();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationPickerManager.getCurrentCity(VenueMapView.this);
        //Refreshing the Map view
        if (AppConstants.isLocationPicked_venue_mapview) {
            AppConstants.isLocationPicked_venue_mapview = false;
            getVenuesFilterListing();
        }

    }


    /**
     * Function to get Venue courts from server
     */
    public void getVenuesFilterListing() {
        //Handling the loader state
        LoaderUtility.handleLoader(VenueMapView.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(VenueMapView.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", image = "", name = "", latitude = "", longitude = "",
                        review_rating = "", review_count = "", price = "";

                try {
                    VenueListing.venueModelArrayList = new ArrayList<>();
                    venue_listing_array = new JSONArray(responseData);

                    for (int i = 0; i < venue_listing_array.length(); i++) {
                        venueModel = new VenueModel();

                        JSONObject responseObject = venue_listing_array.getJSONObject(i);

                        if (responseObject.has("id") && !responseObject.isNull("id")) {
                            id = responseObject.getString("id");
                        }
                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            image = responseObject.getString("image");
                        }
                        if (responseObject.has("name") && !responseObject.isNull("name")) {
                            name = responseObject.getString("name");
                        }
                        if (responseObject.has("latitude") && !responseObject.isNull("latitude")) {
                            latitude = responseObject.getString("latitude");
                        }
                        if (responseObject.has("longitude") && !responseObject.isNull("longitude")) {
                            longitude = responseObject.getString("longitude");
                        }
                        if (responseObject.has("review_rating") && !responseObject.isNull("review_rating")) {
                            review_rating = responseObject.getString("review_rating");
                        }
                        if (responseObject.has("review_count") && !responseObject.isNull("review_count")) {
                            review_count = responseObject.getString("review_count");
                        }
                        if (responseObject.has("price") && !responseObject.isNull("price")) {
                            price = responseObject.getString("price");
                        }

                        venueModel.setVenueId(id);
                        venueModel.setVenueImage(image);
                        venueModel.setVenueName(name);
                        venueModel.setLatitude(latitude);
                        venueModel.setLongitude(longitude);
                        venueModel.setVenueRatings(review_rating);
                        venueModel.setReview_count(review_count);
                        venueModel.setVenuePrice(price);

                        VenueListing.venueModelArrayList.add(venueModel);
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                VenueMapView.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(VenueMapView.this, false);

                        if (venue_listing_array != null) {
                            if (venue_listing_array.length() > 0) {
                                handleButtonClickes(true);
                                mMap.clear();
                                //Placing the markers in the map view
                                setMarkers();
                            } else {
                                handleButtonClickes(false);
                                mMap.clear();
                                snackBar.setSnackBarMessage("No courts found");
                            }
                        } else {
                            mMap.clear();
                            snackBar.setSnackBarMessage("No courts found");
                        }
                    }
                });
            }
        });
    }

    public void handleButtonClickes(boolean status) {
        if (status) {
            imgv_filter.setEnabled(true);
            imgv_filter.setFocusable(true);
        } else {
            imgv_filter.setEnabled(false);
            imgv_filter.setFocusable(false);
        }

    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_venue_courts").newBuilder();
        urlBuilder.addQueryParameter("location", String.valueOf(HomeFragment.currLocLatitude + "," + HomeFragment.currLocLongitude));
        urlBuilder.addQueryParameter("sportid", Constants.SPORTS_ID);
        urlBuilder.addQueryParameter("sport_name", Constants.SPORTS_NAME);
        urlBuilder.addQueryParameter("current_city", HomeFragment.current_city);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }


}
