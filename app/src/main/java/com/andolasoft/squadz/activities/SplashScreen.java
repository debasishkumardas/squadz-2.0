package com.andolasoft.squadz.activities;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.CredentialSingleton;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.facebook.stetho.Stetho;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.appinvite.AppInviteReferral;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.TimeZone;

import bolts.AppLinks;
import me.leolin.shortcutbadger.ShortcutBadger;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SplashScreen extends AppCompatActivity {
    Handler handler = new Handler();
    DatabaseAdapter db;
    SharedPreferences prefs;
    String deepLink = null;
    int badgeCount = 0;
    ProgressDialog dialog;
    String user_id;
    public static String called_time = null;
    String loggedinUserId;
    public static long messageBadgeCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        //Initializing the Database
        db = new DatabaseAdapter(SplashScreen.this);

       // int badgeCount = 5;
        //ShortcutBadger.applyCount(SplashScreen.this, badgeCount); //for 1.1.4+
        //ShortcutBadger.with(SplashScreen.this).count(badgeCount); //for 1.1.3

        //String convertedDate = ApplicationUtility.getTimeWithFormat("07:30 pm");
        //Log.d("Converted Time", convertedDate);

        //Initializing the shared preferance
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        loggedinUserId = prefs.getString("loginuser_id", null);

        //Getting the internet connection status
        boolean isConnected = ApplicationUtility.isConnectingToInternet(SplashScreen.this);

        if(isConnected){
            //Initializing stetho
            initializeStethoDebugBridge();

            //Getting the facebook deep link intent
            Uri targetUrl =
                    AppLinks.getTargetUrlFromInboundIntent(this, getIntent());

            // Check if the intent contains an AppInvite and then process the referral information.
            Intent intent = getIntent();

            if (AppInviteReferral.hasReferral(intent)) {
                //Processing the Normal deep link intent
                processReferralIntent(intent);
                //Calling the sports API
                sportApiRequest();
            }else if(targetUrl != null){
                //Processing the Facebook deep Link intent
                processFacebookdeepLinkIntent(targetUrl);
                //Calling the sports API
                sportApiRequest();
            }else if(intent.hasExtra("key")){
                //Bundle bundle = intent.getBundleExtra("key");
                String messagetype = intent.getStringExtra("key");

                //Clearing all the notification from the notification tray
                NavigationDrawerActivity.clearNotifications(SplashScreen.this);

                if(messagetype != null &&
                        messagetype.equalsIgnoreCase("ChatMessage")){
                    String chat_type = intent.getStringExtra("chat_type");
                    String channelId = intent.getStringExtra("channel_id");
                    String sender_Id = intent.getStringExtra("sender_id");
                    String group_id = intent.getStringExtra("group_id");
                    String senderName = intent.getStringExtra("sender_name");

                    if(chat_type != null &&
                            chat_type.equalsIgnoreCase("group")){
                        AppConstants.isGropuChatMessage = true;
                        AppConstants.CHAT_CHANNEL_ID = channelId;
                        AppConstants.CHAT_USER_IMAGE_URL = null;
                        AppConstants.CHAT_USER_ID = group_id;
                        AppConstants.CHAT_TYPE = chat_type;
                        AppConstants.CHAT_CHANNEL_ID_DUPLICATE = "Received";
                    }else{
                        AppConstants.isGropuChatMessage = false;
                        AppConstants.CHAT_USER_NAME = senderName;
                        AppConstants.CHAT_CHANNEL_ID = channelId;
                        AppConstants.CHAT_USER_IMAGE_URL = null;
                        AppConstants.CHAT_USER_ID = sender_Id;
                        AppConstants.CHAT_TYPE = chat_type;
                        AppConstants.CHAT_CHANNEL_ID_DUPLICATE = "Received";
                    }
                } else if (messagetype != null &&
                        (messagetype.equalsIgnoreCase("NearbyGame")
                                || messagetype.equalsIgnoreCase("JoinGame")
                                || messagetype.equalsIgnoreCase("GameInvitation")
                                || messagetype.equalsIgnoreCase("EditGame")
                                || messagetype.equalsIgnoreCase("LeaveGame")
                                || messagetype.equalsIgnoreCase("GameTransaction"))) {
                    Constants.PUSH_NOTIFICATION_GAME_ID = intent.getStringExtra("object_id");
                }
                /**Redirect the user to Teammate page to see the newly accepted user*/
                else if(messagetype != null &&
                        messagetype.equalsIgnoreCase("AcceptFriend")){
                    Constants.FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY = true;
                } else if(messagetype != null &&
                        messagetype.equalsIgnoreCase("InviteFriend")) {
                    Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND = true;
                }
                //Calling the sports API
                sportApiRequest();
            } else{
                //Calling the sports API
                sportApiRequest();
            }
        }else{
            //Displaying the no internet connection dialog
            ApplicationUtility.displayNoInternetAlert(SplashScreen.this);
        }

    }


    /**
     * Initializing the stetho Drbug Bridge
     */
    public void initializeStethoDebugBridge(){
        // Create an InitializerBuilder
        Stetho.InitializerBuilder initializerBuilder = Stetho
                .newInitializerBuilder(this);

        // Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(Stetho
                .defaultInspectorModulesProvider(SplashScreen.this));

        // Enable command line interface
        initializerBuilder.enableDumpapp(Stetho
                .defaultDumperPluginsProvider(SplashScreen.this));

        // Use the InitializerBuilder to generate an Initializer
        Stetho.Initializer initializer = initializerBuilder.build();

        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer);
    }


    /**
     * Function to get the Application Version Name
     */
    public void getApplicationVersionName(){
        int ver_code = 0;
        PackageManager packageManager = SplashScreen.this.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    SplashScreen.this.getPackageName(), 0);
            ver_code = packageInfo.versionCode;
            String ver_name = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Function to handle the page redirection after the splash screen appear
     */
    public void handlePageRedirections(){
        String authToken = prefs.getString("auth_token", null);
        if(TextUtils.isEmpty(authToken)){
            SplashScreen.this.finish();
            Intent intent = new Intent(SplashScreen.this, SigninActivity.class);
            startActivity(intent);
        }else{
            //Getting the logged in user id
            user_id = prefs.getString("loginuser_id", null);

            //Getting the last badge count API call time
            String timeStamp = prefs.getString(Constants.BADGE_COUNT_API_CALLED_TIME, null);

            //Calling the API accroding to the last time stamp value
            if(!TextUtils.isEmpty(timeStamp)){
                //Getting the badge Count for the user
                getBadgeCount(timeStamp);
            }else{
                //Calling with the null value
                getBadgeCount(null);
            }
        }
    }

    /**
     * Calling the API to getting the sports
     */

    public void sportApiRequest(){
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE_MIGRATION+"get_sports").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/
        Request request = new Request.Builder()
                .url(EndPoints.BASE_URL+"get_sports").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                //displayMessageInUiThread(SplashScreen.this,e.toString());
                //Boolean isconnected = ApplicationUtility.isConnected(SplashScreen.this);
                if(!ApplicationUtility.isConnected(SplashScreen.this)){
                    ApplicationUtility.displayNoInternetMessage(SplashScreen.this);
                }
                //dialog.dismiss();
                handlePageRedirections();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    String status = responseObject.getString("status");

                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        JSONArray sportresponseArray = responseObject.getJSONArray("sports");
                        AppConstants.SPORT_ARRAY = new JSONArray();
                        AppConstants.SPORT_ARRAY = sportresponseArray;
                        db.deleteAllSports();
                        for(int i = 0; i < sportresponseArray.length(); i++){
                            try{
                                JSONObject sportObject = sportresponseArray.getJSONObject(i);
                                String sportName = sportObject.getString("name");
                                String sportId = sportObject.getString("id");
                                if(sportName != null &&
                                        !sportName.equalsIgnoreCase("None")&&
                                        !sportName.equalsIgnoreCase("All")){
                                    db.insertsport(sportName,sportId);
                                }
                            }catch(Exception ex){
                                ex.printStackTrace();
                            }
                        }
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        SnackBar snackBar = new SnackBar(SplashScreen.this);
                        snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                SplashScreen.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Calling the Api by checking the count
                        if(db.getCredentialsCount() == 0){
                            getCredentials();
                        }else{
                            getCredentialsFromDb();
                            if(!TextUtils.isEmpty(loggedinUserId)){
                                getMyBadgeCount();
                            }else{
                                handlePageRedirections();
                            }
                        }
                    }
                });
            }
        });
    }

    /**
     * Function responsible to display the error message in Ui thread
     * @param ctx Activity Context
     * @param message Message need to display
     */
    public static void  displayMessageInUiThread(final Activity ctx, final String message){
        ctx.runOnUiThread(new Runnable() {
            public void run() {
                //Displaying the success message after successful sign up
                Toast.makeText(ctx,message, Toast.LENGTH_LONG).show();
            }
        });
    }



    /**
     * Calling the API to getting the sports
     */

    public void getCredentials(){
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_credentials")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                if(!ApplicationUtility.isConnected(SplashScreen.this)){
                    ApplicationUtility.displayNoInternetMessage(SplashScreen.this);
                }
                //dialog.dismiss();
                handlePageRedirections();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    String aws_access_key = responseObject.getString("aws_access_key");
                    String aws_secret_key = responseObject.getString("aws_secret_key");
                    String aws_bucket = responseObject.getString("aws_bucket");
                    String google_api_key = responseObject.getString("google_api_key");
                    String google_places_api_key = responseObject.getString("google_places_api_key");
                    String facebook_id = responseObject.getString("facebook_id");
                    String stripe_secret_key = responseObject.getString("stripe_secret_key");
                    String stripe_publishable_key = responseObject.getString("stripe_publishable_key");

                    db.insertCredentials(aws_access_key, aws_secret_key, aws_bucket, google_api_key,
                            google_places_api_key, facebook_id, stripe_secret_key,
                            stripe_publishable_key);
                    } catch (JSONException e) {
                    e.printStackTrace();
                }

                SplashScreen.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getCredentialsFromDb();
                        if(!TextUtils.isEmpty(loggedinUserId)){
                            getMyBadgeCount();
                        }else{
                            handlePageRedirections();
                        }

                    }
                });
            }
        });
    }


    /**
     * Fetching conversation data from firebase
     */
    public void getMyBadgeCount() {
        //Initializing the firebase instance
        Firebase.setAndroidContext(SplashScreen.this);
        Firebase postRef = new Firebase(EndPoints.BASE_URL_FIREBASE);

        //Initializing the child
        postRef = postRef.child(AppConstants.MESSAGE_BADGE_COUNT_NODE);

        //callback to handle the user conversation data
        postRef.child(loggedinUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    messageBadgeCount = (long) dataSnapshot.child("badgeCount").getValue();
                }
                handlePageRedirections();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                handlePageRedirections();
            }
        });

    }


    /**
     * Function responsible for getting the stored credential
     * in the database and setting then into the Singleton
     */
    public void getCredentialsFromDb(){
        Cursor cursor = db.getAllCredentials();
        if (cursor.moveToFirst()){
            do{
                String aws_access_key = cursor.getString(cursor.getColumnIndex("aws_access_key"));
                String aws_secret_key = cursor.getString(cursor.getColumnIndex("aws_secret_key"));
                String stripe_secret_key = cursor.getString(cursor.getColumnIndex("stripe_secret_key"));
                String stripe_publishable_key = cursor.getString(cursor.getColumnIndex("stripe_publishable_key"));
                CredentialSingleton.getInstance().setAwsAccessKey(aws_access_key);
                CredentialSingleton.getInstance().setAwsSecretKey(aws_secret_key);
                CredentialSingleton.getInstance().setStripeSecretKey(stripe_secret_key);
                CredentialSingleton.getInstance().setStripePublishableKey(stripe_publishable_key);
            }while(cursor.moveToNext());
        }
        cursor.close();

    }


    // [START deep_link_on_start]
    @Override
    protected void onStart() {
        super.onStart();


    }

    // [START process_referral_intent]
    private void processReferralIntent(Intent intent) {
        // Extract referral information from the intent
        String invitationId = AppInviteReferral.getInvitationId(intent);
        String deepLink = AppInviteReferral.getDeepLink(intent);

        // Getting the Deep linking Id from the message
        if(!TextUtils.isEmpty(deepLink)){
            String [] deepLinkarr = deepLink.split("=");

            if(deepLinkarr[0] != null &&
                    deepLinkarr[0].contains("gameId")) {
                AppConstants.DEEP_LINKING_EVENT_ID = deepLinkarr[1];
                AppConstants.isDeepLinkRecieved = true;
            }else if(deepLinkarr[0] != null &&
                    deepLinkarr[0].contains("listId")){
                AppConstants.isListDeepLinkRecieved = true;
                AppConstants.DEEP_LINKING_LIST_ID = deepLinkarr[1];

            }
        }
        Log.d("Text", invitationId);
        Log.d("Deeplink", deepLink);
    }

    /**
     * Function responsible for processing the Facebook Deep Link Intent
     * @param targetUrl
     */
    private void processFacebookdeepLinkIntent(Uri targetUrl){
        if(targetUrl != null){
            deepLink = targetUrl.toString();

            //Deeplink for events and games
            if(deepLink.contains("/game/")){
                if(deepLink.contains("http://34.192.117.108/game/")){
                    deepLink = deepLink.replace("http://34.192.117.108/game/","");
                }else if(deepLink.contains("http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/game/")){
                    deepLink = deepLink.replace("http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/game/","");
                }
                AppConstants.DEEP_LINKING_EVENT_ID = deepLink;
                AppConstants.isDeepLinkRecieved = true;
            }
            //Deep link for Listings
            else if(deepLink.contains("/list/")){
                if(deepLink.contains("http://34.192.117.108/list/")){
                    deepLink = deepLink.replace("http://34.192.117.108/list/","");
                }else if(deepLink.contains("http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/list/")){
                    deepLink = deepLink.replace("http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/list/","");
                }
                AppConstants.DEEP_LINKING_LIST_ID = deepLink;
                AppConstants.isListDeepLinkRecieved = true;
            }

        }
    }


    /**
     * Function responsible for update the notification badge count
     */
    public void getBadgeCount(final String timeStamp) {
        //Handling the loader state
        //handleLoader(true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildBadgeCountApiRequestBody(timeStamp);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                //handleLoader(false);
                //Letting the user get insode to the application
                if(!ApplicationUtility.isConnected(SplashScreen.this)){
                    ApplicationUtility.displayNoInternetMessage(SplashScreen.this);
                }
                letUserin();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    badgeCount = responseObject.getInt("badge_count");
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                SplashScreen.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                       // handleLoader(false);

                        //Keeping the last API called time
                        keepTheBadgeCountApiCalledTime(SplashScreen.this, called_time);
                        //Assigning the value after the API get called
                        if(badgeCount > 0){
                            Constants.PUSH_NOTIFICATION_BADGE_COUNT = badgeCount;
                        }
                        //Letting the user get insode to the application
                        letUserin();
                    }
                });
            }
        });
    }

    //Handling the page redirection inside to the app
    public void letUserin(){
        SplashScreen.this.finish();
        // Redirecting the user to the navigation drawer activity
        if(Constants.FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY) {
            Intent intent = new Intent(SplashScreen.this, TeamMates.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(SplashScreen.this, NavigationDrawerActivity.class);
            startActivity(intent);
        }
    }

    /**
     * Function responsible for keeping the badge count APi called time
     * @param context
     * @param time
     */
    public static void keepTheBadgeCountApiCalledTime(Context context, String time){
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.BADGE_COUNT_API_CALLED_TIME, time);
        editor.commit();
    }


    /**
     * Function to build the Badge count API request
     *
     * @return APi request
     */
    public Request buildBadgeCountApiRequestBody(String time) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_badge_count").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);

        if(!TextUtils.isEmpty(time)){
            urlBuilder.addQueryParameter("prev_seen_at", time);
        }else{
            time = ApplicationUtility.getCurrentDateTime();
            urlBuilder.addQueryParameter("prev_seen_at", time);
        }
        called_time = ApplicationUtility.getCurrentDateTime();
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Function responsible for getting the current time
     * @return
     */
    public static String getApiCalledTime(){
        called_time = ApplicationUtility.getCurrentDateTime();
        return called_time;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(SplashScreen.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }
}
