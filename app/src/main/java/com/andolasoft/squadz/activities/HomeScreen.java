package com.andolasoft.squadz.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.andolasoft.squadz.R;

public class HomeScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
    }
}
