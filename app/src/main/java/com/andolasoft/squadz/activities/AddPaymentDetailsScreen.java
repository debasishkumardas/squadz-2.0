package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.CredentialSingleton;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddPaymentDetailsScreen extends AppCompatActivity {

    @InjectView(R.id.etCardNumber)
    EditText etCardNumber;
    @InjectView(R.id.etNameDisplay)
    EditText etCardNameHolder;
    @InjectView(R.id.etcardCvv)
    EditText etCvv;
    @InjectView(R.id.mnthSpinner)
    Spinner spnMonth;
    @InjectView(R.id.yrSpinner)
    Spinner spnYr;
    @InjectView(R.id.btnAddPayment)
    Button btnAddPayment;
    @InjectView(R.id.back_icon_add_payment_layout)
    RelativeLayout back_icon_add_payment_layout;
    public static ArrayList<String> monthArray;
    public static ArrayList<String> yrArray;
    SnackBar snackBar = new SnackBar(AddPaymentDetailsScreen.this);
    String selectedMonth = null, selectedyear = null, logged_in_user_email;
    Card card = null;
    AddPaymentDetailsScreen paymentDetails = null;
    ProgressDialog progressFragment;
    SharedPreferences pref;
    public static String customer_id = null;
    String status = "",message = "", a;
    int keyDel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment_details_screen);
        ButterKnife.inject(this);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        logged_in_user_email = pref.getString("user_email", null);

        //Initializing the context
        paymentDetails = this;
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        //Initializing the month array
        initializeMonthArray();

        //Initializing the year array
        initializeYearArray();


        btnAddPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Getting the card details
                getCarddetails();
            }
        });


        spnMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                selectedMonth = monthArray.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spnYr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                selectedyear = yrArray.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        back_icon_add_payment_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.ADD_PAYMENY_BTN_CLICKED = false;
                finish();
            }
        });


        etCardNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean flag = true;
                String eachBlock[] = etCardNumber.getText().toString().split(" ");
                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 4) {
                        flag = false;
                    }
                }
                if (flag) {

                    etCardNumber.setOnKeyListener(new View.OnKeyListener() {

                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((etCardNumber.getText().length() + 1) % 5) == 0) {

                            if (etCardNumber.getText().toString().split(" ").length <= 3) {
                                etCardNumber.setText(etCardNumber.getText() + " ");
                                etCardNumber.setSelection(etCardNumber.getText().length());
                            }
                        }
                        a = etCardNumber.getText().toString();
                    } else {
                        a = etCardNumber.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    etCardNumber.setText(a);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //displayCardDetails();

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }


    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            progressFragment = ProgressDialog
                    .show(AddPaymentDetailsScreen.this, "", "Please wait ...");
            progressFragment.getWindow().setGravity(Gravity.CENTER);
            progressFragment.setCancelable(false);
        } else {
            progressFragment.dismiss();
        }
    }
    /**
     * Funtion to get all the card details
     */
    public void getCarddetails(){
        String cardNumber = etCardNumber.getText().toString().replace(" ","");
        String CardNameHolder = etCardNameHolder.getText().toString();
        String CardCvvNumber = etCvv.getText().toString();
        String spn_month= spnMonth.getSelectedItem().toString();
        String spn_year = spnYr.getSelectedItem().toString();

        if(TextUtils.isEmpty(CardNameHolder)) {
            etCardNameHolder.requestFocus();
            snackBar.setSnackBarMessage("Please enter card holder name");
        } else if(TextUtils.isEmpty(cardNumber)) {
            etCardNumber.requestFocus();
            snackBar.setSnackBarMessage("Please enter card number");
        } else if(TextUtils.isEmpty(selectedMonth) ||
                    TextUtils.isEmpty(selectedyear)) {
            snackBar.setSnackBarMessage("Please enter card expiring date");
        } else if(TextUtils.isEmpty(CardCvvNumber)) {
            etCvv.requestFocus();
            snackBar.setSnackBarMessage("Please enter card cvv number");
        } else if(spn_month.equalsIgnoreCase("Select month")) {
            snackBar.setSnackBarMessage("Please enter card expiry month");
        } else if(spn_year.equalsIgnoreCase("Select year")) {
            snackBar.setSnackBarMessage("Please enter card expiry year");
        } else{
            Card card = ApplicationUtility.buildCardParameters(cardNumber,selectedMonth,
                    selectedyear,
                    CardCvvNumber);
            boolean validateCard = ApplicationUtility.validateCard(card);
            if(validateCard){
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, true);
                new Stripe().createToken(
                        card,
                        CredentialSingleton.getInstance().getStripePublishableKey(),
                        new TokenCallback() {
                            public void onSuccess(final Token token) {
                                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                                //Creating Creating Customer & savng cards
                                if(pref.contains("loginuser_id")) {
                                    String user_id = pref.getString("loginuser_id","");
                                    String auth_token = pref.getString("auth_token","");
                                    String user_email = pref.getString("user_email","");
                                    String stripe_token = token.getId();

                                    //API integration for save customer cards
                                    saveCardsAPI(auth_token, user_id,user_email,stripe_token);
                                }
                            }
                            public void onError(Exception error) {
                                Toast.makeText(paymentDetails, ""+error.getMessage(), Toast.LENGTH_LONG).show();
                                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                            }
                        });
            } else if (!card.validateNumber()) {
                Toast.makeText(paymentDetails, "The card number that you entered is invalid", Toast.LENGTH_LONG).show();
            } else if (!card.validateExpiryDate()) {
                Toast.makeText(paymentDetails, "The expiration date that you entered is invalid", Toast.LENGTH_LONG).show();
            } else if (!card.validateCVC()) {
                Toast.makeText(paymentDetails, "The CVC code that you entered is invalid", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(paymentDetails, "The card details that you entered are invalid", Toast.LENGTH_LONG).show();
            }
            }
        }
    /**
     * @author Prabu
     * Private class which runs the long operation. ( Sleeping for some time )
     */
    private class CreateCustomerAsyncTask extends AsyncTask<String, String, String> {

        private String resp;
        Customer customer= null;
        Map<String, Object> customerParam = null;

        public CreateCustomerAsyncTask(Token token){
            com.stripe.Stripe.apiKey = CredentialSingleton.getInstance().getStripeSecretKey();
            customerParam = new HashMap<String, Object>();
            customerParam.put("email", logged_in_user_email);
            customerParam.put("description", "Test Description");
            customerParam.put("card", token.getId());
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                /*Map<String, Object> params = new HashMap<String, Object>();
                params.put("source", token.getId());
                try {
                    customer.getSources().create(params);
                } catch (AuthenticationException e) {
                    e.printStackTrace();
                } catch (InvalidRequestException e) {
                    e.printStackTrace();
                } catch (APIConnectionException e) {
                    e.printStackTrace();
                } catch (CardException e) {
                    e.printStackTrace();
                } catch (APIException e) {
                    e.printStackTrace();
                }*/


                customer = Customer.create(customerParam,
                        CredentialSingleton.getInstance().getStripeSecretKey());
                customer_id = customer.getId();

                Log.d("Customer Id", customer_id);
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
            } catch (AuthenticationException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            } catch (InvalidRequestException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            } catch (APIConnectionException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            } catch (CardException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            } catch (APIException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            }

            return resp;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            //new GetAllStoredCards(customer_id).execute();

            new GetAllStoredCards(customer_id).execute();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, true);
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(String... text) {

        }
    }

    /**
     * Initializing the Month Array
     */
    public void initializeMonthArray(){
        monthArray = new ArrayList<>();
        monthArray.add("Select month");
        for(int i = 1; i < 13; i++){
            monthArray.add(String.valueOf(i));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,monthArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnMonth.setAdapter(adapter);
    }


    /**
     * Initializing the Month Array
     */
    public void initializeYearArray(){
        yrArray = new ArrayList<>();
        yrArray.add("Select year");
        for(int i = 2040; i > 1990; i--){
            yrArray.add(String.valueOf(i));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,yrArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnYr.setAdapter(adapter);

    }

    public void displayCardDetails(){
        etCardNumber.setText("378282246310005");
        etCardNameHolder.setText("Test User");
        etCvv.setText("123");
    }



    /**
     * @author Prabu
     * Private class which runs the long operation. ( Sleeping for some time )
     */
    private class GetAllStoredCards extends AsyncTask<String, String, String> {

        private String resp;
        com.stripe.model.ExternalAccountCollection customer= null;
        String customer_id = null;
        Map<String, Object> cardParam = null;
        DatabaseAdapter db = new DatabaseAdapter(AddPaymentDetailsScreen.this);


        public GetAllStoredCards(String customerId){
            this.customer_id = customerId;
            com.stripe.Stripe.apiKey = CredentialSingleton.getInstance().getStripeSecretKey();
            cardParam = new HashMap<String, Object>();
            cardParam.put("limit", 40);
            cardParam.put("object", "card");
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                customer = Customer.retrieve(customer_id).getSources().all(cardParam);

                List<ExternalAccount> cardBrand = customer.getData();
                for (int i = 0; i < cardBrand.size(); i++){
                    ExternalAccount account = cardBrand.get(i);

                    String sources = cardBrand.get(i).toString();

                    String[] split_sources = sources.split("\\{");
                    sources = sources.replace(split_sources[0],"");

                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(sources);

                        String cardBrand_type = jsonObject.getString("brand");
                        String cardcountry = jsonObject.getString("country");
                        String cardFunding = jsonObject.getString("funding");
                        String cardlastfour = jsonObject.getString("last4");
                        String cardCustId = jsonObject.getString("customer");
                        String cardId = jsonObject.getString("id");
                        String cardHolderName = jsonObject.getString("name");
                        String exp_month = jsonObject.getString("exp_month");
                        String exp_year = jsonObject.getString("exp_year");

                        db.insertCarddetails(cardBrand_type,
                                cardcountry, cardFunding,
                                cardlastfour, cardCustId, cardId,
                                cardHolderName,exp_month,exp_year,0);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                /*JSONObject carddetails = new JSONObject(customer.toString());
                JSONArray cardArray = carddetails.getJSONArray("data");
                for(int i = 0; i < cardArray.length(); i++){
                    JSONObject cardDetailsObject = cardArray.getJSONObject(i);
                    String cardBrand = cardDetailsObject.getString("brand");
                    String cardcountry = cardDetailsObject.getString("country");
                    String cardFunding = cardDetailsObject.getString("funding");
                    String cardlastfour = cardDetailsObject.getString("last4");
                    String cardCustId = cardDetailsObject.getString("customer");
                    String cardId = cardDetailsObject.getString("id");
                    db.insertCarddetails(cardBrand,
                            cardcountry, cardFunding,
                            cardlastfour, cardCustId, cardId);
                }*/
                Log.d("Customer Card Details",customer.toString());
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
            } catch (AuthenticationException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            } catch (InvalidRequestException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            } catch (APIConnectionException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            } catch (CardException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            } catch (APIException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                e.printStackTrace();
            } /*catch (JSONException e) {
                e.printStackTrace();
            }*/

            return resp;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            Constants.ADD_PAYMENY_BTN_CLICKED = true;
            AddPaymentDetailsScreen.this.finish();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {
            LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, true);
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onProgressUpdate(Progress[])
         */
        @Override
        protected void onProgressUpdate(String... text) {

        }
    }

    /**
     * Generating card info with list of all cards
     */
    public void displayAllCards(Token token)
    {
        // getStripeCustomerId(token);
        // getStripeCustomerId(token);
        com.stripe.Stripe.apiKey = CredentialSingleton.getInstance().getStripeSecretKey();
        Customer customer = null;
        try {
            customer = Customer.retrieve("cus_A4wpupHJMnKpV1");

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("source", token.getId());
            customer.getSources().create(params);

            String tokenn =  token.getId();

            new GetAllStoredCards("cus_A4wpupHJMnKpV1").execute();

        } catch (AuthenticationException e) {
            e.printStackTrace();
        } catch (InvalidRequestException e) {
            e.printStackTrace();
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (CardException e) {
            e.printStackTrace();
        } catch (APIException e) {
            e.printStackTrace();
        }


        Log.d("Card Info", token.toString());
    }


    /**
     * API integration for save customer cards
     */
    public void saveCardsAPI(String auth_token, String user_id,
                             String email, String stripe_token) {
        //Handling the loader state
        LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(auth_token,user_id,email,stripe_token);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                if(!ApplicationUtility.isConnected(AddPaymentDetailsScreen.this)){
                    ApplicationUtility.displayNoInternetMessage(AddPaymentDetailsScreen.this);
                }else{
                    ApplicationUtility.displayErrorMessage(AddPaymentDetailsScreen.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    if(jsonObject.has("status") && !jsonObject.isNull("status")) {
                        status = jsonObject.getString("status");
                        message = jsonObject.getString("message");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(AddPaymentDetailsScreen.this, false);
                        if(status.equalsIgnoreCase("success")) {
                            snackBar.setSnackBarMessage(message);
                            Constants.ADD_PAYMENY_BTN_CLICKED = true;
                            AppConstants.isPaymentCardAdded = true;
                            finish();
                        } else{
                            snackBar.setSnackBarMessage(message);
                        }

                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String auth_token, String user_id, String email, String stripe_token) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "create_stripe_customer").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("email", email);
        urlBuilder.addQueryParameter("stripe_token", stripe_token);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.ADD_PAYMENY_BTN_CLICKED = false;
        finish();
    }
}
