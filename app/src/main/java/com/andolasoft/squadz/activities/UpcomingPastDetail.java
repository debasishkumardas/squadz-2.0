package com.andolasoft.squadz.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.models.VenueDetailModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.CalenderSyncUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.adapters.ImageSlideAdapter;
import com.andolasoft.squadz.views.adapters.UpcomingPastAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SpNayak on 2/27/2017.
 */

public class UpcomingPastDetail extends AppCompatActivity implements OnMapReadyCallback {

    @InjectView(R.id.upcoming_back_icon_layout)
    RelativeLayout upcoming_back_icon_layout;
    @InjectView(R.id.participant_sub_layout)
    RelativeLayout participant_sub_layout;
    @InjectView(R.id.invite_btn_upcoming_detail_layout)
    RelativeLayout invite_btn_upcoming_detail;
    @InjectView(R.id.add_btn_upcoming_detail_layout)
    RelativeLayout add_btn_upcoming_detail;
    @InjectView(R.id.viewpager_upcoming_past)
    ViewPager viewpager_upcoming_past;
    @InjectView(R.id.upcoming_event_name)
    TextView upcoming_event_name;
    @InjectView(R.id.upcoming_detail_image_frame)
    FrameLayout upcoming_detail_image_frame;
    @InjectView(R.id.upcoming_detail_price)
    TextView upcoming_detail_price;
    @InjectView(R.id.upcoming_detail_Minimum_Amount_Text)
    TextView upcoming_detail_Minimum_Amount_Text;
    @InjectView(R.id.upcoming_price_players)
    TextView upcoming_price_players;
    @InjectView(R.id.upcoming_detail_participants)
    TextView upcoming_detail_participants;
    @InjectView(R.id.upcoming_spots)
    TextView upcoming_spots;
    @InjectView(R.id.upcoming_detail_date_time)
    TextView upcoming_detail_date_time;
    @InjectView(R.id.upcoming_sports_type)
    TextView upcoming_sports_type;
    @InjectView(R.id.skill_level_name)
    TextView skill_level_name;
    @InjectView(R.id.upcoming_participant_title)
    TextView upcoming_participant_title;
    @InjectView(R.id.upcoming_buttom_spots_remaining_title)
    TextView upcoming_buttom_spots_remaining_title;
    @InjectView(R.id.game_type)
    TextView game_type;
    @InjectView(R.id.upcoming_event_address_name)
    TextView upcoming_event_address_name;
    @InjectView(R.id.upcoming_detail_description)
    TextView tvDescription;
    @InjectView(R.id.navigation_upcoming)
    ImageView navigation_upcoming;
    @InjectView(R.id.favourite_upcoming_detail)
    MaterialFavoriteButton favourite_upcoming_detail;
    @InjectView(R.id.upcoming_game_detail_menu_icon_layout)
    RelativeLayout upcoming_game_detail_menu_icon_layout;
    @InjectView(R.id.share_event_detail)
    ImageView ivShare;
    @InjectView(R.id.indoor_icon_upcoming)
    ImageView indoor_icon_upcoming;
    @InjectView(R.id.projected_cost)
    TextView projected_cost;
    @InjectView(R.id.projected_cost_layout)
    RelativeLayout projected_cost_layout;
    @InjectView(R.id.public_private_icon)
    ImageView ic_game_visibility;
    private GoogleMap mMap;
    private boolean gps_enabled = false;
    private GPSTracker gpsTracker;
    private Double current_lat, current_long;
    ProgressDialog dialog;
    String event_lat,event_lon,address,name = "",
            game_Creator_userId = "",userId = "",
            visibility = "",skillLevel = "",
            total_player_count = "",note = "",
            accepted_player_count = "0",list_ID = "",
            eventAddress = "",eventDate = "",
            eventStartTIme = "",eventEndTIme = "", eventSportName = "",
            court_type = "",court_total_player_capacity = "",type,eventTotalPrice = "0";
    boolean favorite_status = false;
    boolean isAddedToFavorite = false;
    String status;
    SnackBar snackBar;
    CalenderSyncUtility calenderSyncUtility;
    SharedPreferences prefs;
    public EventListingModel eventListingModel;
    public ArrayList<EventListingModel> eventListingModelArrayList;
    ShareDialog shareDialog;
    private CallbackManager callbackManager;
    private LoginManager manager;
    MaterialDialog mMaterialDialog;
    String eventId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_upcoming_past_detail);
        ButterKnife.inject(this);
        snackBar = new SnackBar(this);
        prefs = PreferenceManager.getDefaultSharedPreferences(UpcomingPastDetail.this);
        userId = prefs.getString("loginuser_id", null);

        /**Initializing views*/
        initViews();

        isLocationEnabled();

        /**Click effect*/
        addClickEvent();

        /**Checking whether coming from Upcoming or Past game listing page*/
        setUpcomingPastEventType();

        /**Fetch data Upcoming / Past detail API*/
        setUpcomingData();
    }

    /**
     * Initializing the facebook SDK
     */
    public void initializeFacebook(){
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
    }

    /**
     * Set onclick event
     */
    public void addClickEvent() {
        upcoming_back_icon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Constants.UPCOMING_PAST_CLICKED = false;
                finish();
            }
        });

        invite_btn_upcoming_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.clear();
                Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
                Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
                Intent intent = new Intent(UpcomingPastDetail.this, AddTeammates.class);
                startActivity(intent);
            }
        });
        /**Participants click*/
        participant_sub_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UpcomingPastDetail.this, EventParticipantsScreen.class);
                startActivity(intent);
            }
        });

        /**Diection Icon click*/
        navigation_upcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UpcomingPastDetail.this, ShowDirections.class);
                startActivity(intent);
            }
        });

        if(Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("UPCOMING")) {
            favourite_upcoming_detail.setClickable(true);
            favourite_upcoming_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String eventId = UpcomingPastAdapter.event_id;
                    String userId = UpcomingPastAdapter.userId;

                    if (favorite_status) {
                        makeListFavorite(userId, eventId, false);
                    } else {
                        makeListFavorite(userId, eventId, true);
                    }
                }});
        }else{
            favourite_upcoming_detail.setClickable(false);
        }

        upcoming_game_detail_menu_icon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayMenuPopUp();
            }
        });

        add_btn_upcoming_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Initializing facebook
                initializeFacebook();

                //Getting the event id
                String event_id = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Id();

                //Function call to display the
                // option to share in the respective plat forms

                if(eventDate.contains(" ")) {
                    eventDate = ApplicationUtility.formattedDateContainSpace(eventDate);
                } else{
                    eventDate = ApplicationUtility.formattedDate(eventDate);
                }
                displayShareDialog(event_id, eventSportName, eventDate,
                        eventStartTIme, eventEndTIme);

            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //FInction call to doing the calender sync
                doCalenderSync();
            }
        });
    }


    /**
     * Function responsible for sharing the game details in facebook
     */
    public void shareGameDetails(String eventId, String sport,
                                 String date, String startTime,
                                 String endTime){

        String deepLink = EndPoints.BASE_URL_DEEP_LINKING+"game/"+eventId;
        Log.d("Deep Link", deepLink);
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    //.setContentTitle("Squadz Game Posts")
                    .setContentDescription(sport + " on " + date + " at "+ startTime + " to " + endTime)
                    .setContentUrl(Uri.parse(EndPoints.BASE_URL_DEEP_LINKING+"game/"+eventId))
                    //.setContentUrl(Uri.parse("http://squadzvenue-dev-env.us-east-1.elasticbeanstalk.com/game/f4840d91-850d-45a2-8ab4-f253dfa7996c"))
                    //.setContentUrl(Uri.parse("http://34.192.117.108"))
                    .build();
            ShareDialog shareDialog_new = new ShareDialog(UpcomingPastDetail.this);
            shareDialog_new.show(linkContent, ShareDialog.Mode.NATIVE);
            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                @Override
                public void onSuccess(Sharer.Result result) {
                    Toast.makeText(UpcomingPastDetail.this, "Shared your post successfully", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancel() {
                    //Toast.makeText(FinishEventCreation.this, "Cancel", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException e) {
                    e.printStackTrace();
                    Toast.makeText(UpcomingPastDetail.this,  e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            snackBar.setSnackBarMessage("Please check your internet connection");
        }

    }

    /**
     * Function responsible to doing the sync to the calender
     */
    public void syncToCalender(){
        //Initializing the class to sync to the calender
        CalenderSyncUtility cs = new CalenderSyncUtility(UpcomingPastDetail.this);

        //Getting the Event details
        name = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Name();
        String eventDate = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEventDate();
        String eventStartTIme = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEventStartTime();
        String eventEndTIme = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEventEndTime();
        String eventAddress = UpcomingPastAdapter.eventListingModelArrayList.get(0).getAddress();
        String eventId = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Id();

        //Syncing to the calender
        try {
            //boolean getEvent_exiatance = cs.getEventExistance(eventDate,name, eventStartTIme,eventEndTIme);
            //Getting the calender sync status
            boolean isEventAlreadyAdded = cs.getCalendarEventExistance(name);

            /**
             * Syncing to the calender only and only if the event is not synced
             *
             * Checking the sync status using the event title
             */
            if(!isEventAlreadyAdded){
                /**
                 * Function call to add the event in the calendar
                 */
                cs.addEvent(eventId, eventDate, name, null, eventAddress,
                        eventStartTIme, eventEndTIme);

                //Making the add to calendar button invisible once synced
               // add_btn_upcoming_detail.setVisibility(View.GONE);

                //Displaying the message to the user after succssful sync
                Toast.makeText(UpcomingPastDetail.this,
                        "Event added to calendar successfully",
                        Toast.LENGTH_SHORT).show();
            }else {
                //Displaying the message to the user after succssful sync
                Toast.makeText(UpcomingPastDetail.this,
                        "This Event is already added to calendar",
                        Toast.LENGTH_SHORT).show();
            }
           // cs.addEventToCalendar(eventId, eventDate,name,null, eventAddress,eventStartTIme,eventEndTIme );
            //cs.addEvent(eventId, eventDate, name, null, eventAddress,String starttime,String endtime);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (gps_enabled) {

            LatLng lat_lng = new LatLng(Double.parseDouble(event_lat), Double.parseDouble(event_lon));
            mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)).position(lat_lng).title(name));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat_lng, 15));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.isMyLocationEnabled();
            mMap.getUiSettings().setAllGesturesEnabled(false);
        }
    }

    /**
     * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
     *
     * @return
     */
    public boolean isLocationEnabled() {
        gps_enabled = false;
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gps_enabled;
    }

    /**
     * Initializing views
     */
    public void initViews() {
        gpsTracker = new GPSTracker(this);
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView_upcoming_Detail);
        mapFragment.getMapAsync(this);
        if (gpsTracker.canGetLocation()) {
            current_lat = gpsTracker.getLatitude();
            current_long = gpsTracker.getLongitude();
        }
    }

    /**
     * Checking whether coming from Upcoming or Past game listing page
     */
    public void setUpcomingPastEventType() {
        if(Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("PAST")) {
            invite_btn_upcoming_detail.setVisibility(View.GONE);
            add_btn_upcoming_detail.setVisibility(View.GONE);
        } else{
            invite_btn_upcoming_detail.setVisibility(View.VISIBLE);
            add_btn_upcoming_detail.setVisibility(View.VISIBLE);
        }
    }

    /**
     *Fetch data Upcoming / Past detail API 
     */
    public void setUpcomingData() {
        game_Creator_userId = UpcomingPastAdapter.eventListingModelArrayList.get(0).getCreator_user_id();
        name = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Name();
        court_type = UpcomingPastAdapter.eventListingModelArrayList.get(0).getCourt_type();
        skillLevel = UpcomingPastAdapter.eventListingModelArrayList.get(0).getSkill_level();
        String noOfParticipants = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Total_Participants();
        eventDate = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEventDate();
        eventStartTIme = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEventStartTime();
        eventEndTIme = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEventEndTime();
        String eventPrice = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Price();
        String eventCourtName = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_court_name();
        String eventId = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Id();
        eventSportName = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Sports_Type();
        String event_creator_name = UpcomingPastAdapter.eventListingModelArrayList.get(0).getCreator_name();
        String event_creator_image = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Creator_Image();
        eventAddress = UpcomingPastAdapter.eventListingModelArrayList.get(0).getAddress();
        event_lat = UpcomingPastAdapter.eventListingModelArrayList.get(0).getLatitude();
        event_lon = UpcomingPastAdapter.eventListingModelArrayList.get(0).getLongitude();
        address = UpcomingPastAdapter.eventListingModelArrayList.get(0).getAddress();
        visibility = UpcomingPastAdapter.eventListingModelArrayList.get(0).getVisibility();
        note = UpcomingPastAdapter.eventListingModelArrayList.get(0).getNote();
        list_ID = UpcomingPastAdapter.eventListingModelArrayList.get(0).getList_id();
        Constants.CANCELLATION_POLICY_TYPE = UpcomingPastAdapter.eventListingModelArrayList.get(0).getCancel_policy_type();
        Constants.CANCELLATION_POLICY_TYPE_POINT = UpcomingPastAdapter.eventListingModelArrayList.get(0).getCancellation_point();
        AppConstants.OBJECT_TYPE_GAME = UpcomingPastAdapter.eventListingModelArrayList.get(0).getObject_Type();
        eventTotalPrice = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Total_Price();
        String getMinimumPrice = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Minimum_Price();
        accepted_player_count = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_accepted_Participants();
        favorite_status = UpcomingPastAdapter.eventListingModelArrayList.get(0).isFavorite();
        String note = UpcomingPastAdapter.eventListingModelArrayList.get(0).getNote();
        String review_rating = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Reviews();
        String review_count = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Ratings();
        court_total_player_capacity = UpcomingPastAdapter.eventListingModelArrayList.get(0).getCapacity();


        if (favorite_status) {
            favourite_upcoming_detail.setFavorite(true);
            favourite_upcoming_detail.setFavoriteResource(R.mipmap.ic_favorite_orange);
        } else {
            favourite_upcoming_detail.setFavorite(false);
            favourite_upcoming_detail.setFavoriteResource(R.mipmap.ic_favorite_gray);
        }

        total_player_count = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Total_Participants();
        int spots_left = Integer.valueOf(total_player_count) - Integer.valueOf(accepted_player_count);

        ArrayList<String> imagesArray = UpcomingPastAdapter.eventListingModelArrayList.get(0).getImages_array();

        if (imagesArray.size() > 0) {
            setImagesInViewPager(imagesArray);
        } else {
            int getEventImage = SportsImagePicker.getDefaultSportImage(eventSportName);
            upcoming_detail_image_frame.setBackgroundResource(getEventImage);
        }
         //upcoming_detail_Minimum_Amount_Text.setVisibility(View.VISIBLE);

        /**Displaying player price depending upon the object type*/
           displayPlayerPrice();


        if (spots_left > 1) {
            upcoming_spots.setText(spots_left + " spots Remaining");
            upcoming_buttom_spots_remaining_title.setText(spots_left + " spots remaining");
        } else {
            upcoming_spots.setText(spots_left + " spot Remaining");
            upcoming_buttom_spots_remaining_title.setText(spots_left + " spot remaining");
        }

        upcoming_event_name.setText(name);
        upcoming_detail_participants.setText(accepted_player_count);
        if(eventDate.contains(" ")) {
            //eventDate = ApplicationUtility.formattedDateContainSpace(eventDate);
            upcoming_detail_date_time.setText(ApplicationUtility.formattedDateContainSpace(eventDate) + "\n" +
                    ApplicationUtility.getTimeWithFormat(eventStartTIme) + " to " +
                    ApplicationUtility.getTimeWithFormat(eventEndTIme));
        } else{
            //eventDate = ApplicationUtility.formattedDateContainSpace(eventDate);
            upcoming_detail_date_time.setText(ApplicationUtility.formattedDate(eventDate) + "\n" +
                    ApplicationUtility.getTimeWithFormat(eventStartTIme) + " to " +
                    ApplicationUtility.getTimeWithFormat(eventEndTIme));
        }

        if(court_type.equalsIgnoreCase("Outdoor")) {
            indoor_icon_upcoming.setImageResource(R.mipmap.ic_court_outdoor);
        } else{
            indoor_icon_upcoming.setImageResource(R.mipmap.ic_court_indoor);
        }
        upcoming_sports_type.setText(court_type);
        skill_level_name.setText(skillLevel);
        game_type.setText(Constants.VISIBILITY);
        upcoming_event_address_name.setText(eventAddress);

        if(!TextUtils.isEmpty(note)){
            tvDescription.setText(note);
        }else{
            tvDescription.setVisibility(View.GONE);
        }


        if(Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("UPCOMING")) {
            upcoming_detail_Minimum_Amount_Text.setVisibility(View.VISIBLE);
            upcoming_participant_title.setText(accepted_player_count + " Playing");
            ivShare.setClickable(true);
            ivShare.setEnabled(true);
        } else{
            upcoming_participant_title.setText(accepted_player_count + " Played");
            upcoming_spots.setVisibility(View.GONE);
            upcoming_buttom_spots_remaining_title.setVisibility(View.GONE);
            upcoming_detail_Minimum_Amount_Text.setVisibility(View.INVISIBLE);
            ivShare.setClickable(false);
            ivShare.setEnabled(false);
        }

        if(visibility != null && visibility.equalsIgnoreCase("Public")){
            ic_game_visibility.setImageResource(R.mipmap.ic_public_gray);
        }else if(visibility != null && visibility.equalsIgnoreCase("Private")){
            ic_game_visibility.setImageResource(R.mipmap.ic_private_gray);
        }

       /* if(Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("UPCOMING")) {
            if (!TextUtils.isEmpty(eventTotalPrice) &&
                    !eventTotalPrice.equalsIgnoreCase("0")) {
                double total_projected_cost = Double.valueOf(eventTotalPrice) / Double.valueOf(accepted_player_count);
                projected_cost.setText("$ " + new DecimalFormat("##.##").format(total_projected_cost));
                projected_cost_layout.setVisibility(View.VISIBLE);
            } else {
                projected_cost_layout.setVisibility(View.GONE);
            }
        }
        else{
            projected_cost_layout.setVisibility(View.GONE);
        }*/

        calenderSyncUtility = new CalenderSyncUtility(UpcomingPastDetail.this);

//        boolean getCalenderSyncStatus = calenderSyncUtility.getCalendarEventExistance(name);
//
//        if (getCalenderSyncStatus) {
//            add_btn_upcoming_detail.setVisibility(View.GONE);
//            if (Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("UPCOMING") && userId.equals(game_Creator_userId)) {
//                upcoming_game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
//            } else {
//                upcoming_game_detail_menu_icon_layout.setVisibility(View.GONE);
//            }
//        }
            /*if (Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("UPCOMING") &&
                    userId.equals(game_Creator_userId)) {
                upcoming_game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
            } else {
                upcoming_game_detail_menu_icon_layout.setVisibility(View.GONE);
            }*/

        /**Checking this event is created from WEB, so that the menu icon won't be display*/
        if(Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("UPCOMING")) {
            if (!TextUtils.isEmpty(AppConstants.OBJECT_TYPE_GAME)
                    && (AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event")
                    || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event"))) {
                upcoming_game_detail_menu_icon_layout.setVisibility(View.GONE);
                upcoming_detail_Minimum_Amount_Text.setVisibility(View.GONE);
            } else {
                /**Check whether this game is created by this logged user or not,
                 * so that edit game icon will be displayed*/
                if (Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("UPCOMING") &&
                        userId.equals(game_Creator_userId)) {
                    upcoming_game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
                } else if (Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("UPCOMING") &&
                        AppConstants.userIdArray.contains(userId)) {
                    upcoming_game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
                } else {
                    upcoming_game_detail_menu_icon_layout.setVisibility(View.GONE);
                }
            }
        } else{
            upcoming_game_detail_menu_icon_layout.setVisibility(View.GONE);
        }

        /**
         * Hiding INVITE & SHARE button, if maximum has reached to this game
         */
        if(upcoming_buttom_spots_remaining_title.getText().toString().trim().equalsIgnoreCase("0 spot Remaining")) {
            invite_btn_upcoming_detail.setClickable(false);
            invite_btn_upcoming_detail.setEnabled(false);
            add_btn_upcoming_detail.setClickable(false);
            add_btn_upcoming_detail.setEnabled(false);
        } else{
            invite_btn_upcoming_detail.setClickable(true);
            invite_btn_upcoming_detail.setEnabled(true);
            add_btn_upcoming_detail.setClickable(true);
            add_btn_upcoming_detail.setEnabled(true);
        }


        if(AppConstants.OBJECT_TYPE_GAME != null &&
                (AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event") ||
                        AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))) {
            VenueAdapter.venueDetailModelArrayList = new ArrayList<>();
            VenueDetailModel venueModel = new VenueDetailModel();
            venueModel.setName(name);
            venueModel.setId(eventId);
            venueModel.setReview_rating(review_rating);
            venueModel.setReview_count(review_count);
            venueModel.setAddress(eventAddress);
            venueModel.setImages_array(UpcomingPastAdapter.eventListingModelArrayList.get(0).getImages_array());
            VenueAdapter.venueDetailModelArrayList.add(venueModel);
        }

    }

    /**
     * Dispalying the Upcoming / Past images in view pager
     */
    public void setImagesInViewPager(ArrayList<String> images_array) {
        if(images_array.size() == 1) {
            ImageSlideAdapter imageSlideAdapter =
                    new ImageSlideAdapter(UpcomingPastDetail.this,images_array);
            viewpager_upcoming_past.setAdapter(imageSlideAdapter);

        } else if(images_array.size() > 0) {
            ImageSlideAdapter imageSlideAdapter =
                    new ImageSlideAdapter(UpcomingPastDetail.this,images_array);
            viewpager_upcoming_past.setAdapter(imageSlideAdapter);
        } else{
            //Display place holder image
            upcoming_detail_image_frame.setBackgroundResource(R.mipmap.ic_no_image_found);
        }
    }


    /**
     * Favorite API call
     * @param user_id
     * @param list_id
     * @param isFavorite
     */
    public void makeListFavorite(String user_id, String list_id, boolean isFavorite) {
        //Handling the loader state
        LoaderUtility.handleLoader(UpcomingPastDetail.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, list_id, isFavorite);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(UpcomingPastDetail.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    isAddedToFavorite = responseObject.getBoolean("isFavorite");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                UpcomingPastDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(UpcomingPastDetail.this, false);

                        Constants.UPCOMING_DETAIL_FAVORITE = true;
                        if (status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                            if (isAddedToFavorite) {
                                snackBar.setSnackBarMessage("Successfully Added to WishList");
                                favourite_upcoming_detail.setFavorite(true);
                                favourite_upcoming_detail.setFavoriteResource(R.mipmap.ic_favorite_orange);
                                favorite_status = true;
                            } else {
                                snackBar.setSnackBarMessage("Successfully Removed from WishList");
                                favourite_upcoming_detail.setFavorite(false);
                                favourite_upcoming_detail.setFavoriteResource(R.mipmap.ic_favorite_gray);
                                favorite_status = false;
                            }

                        } else if (status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)) {
                            snackBar.setSnackBarMessage("Failed to Add to WishList");
                            favourite_upcoming_detail.setFavoriteResource(R.mipmap.ic_favorite_gray);
                            favorite_status = true;
                        } else {
                            snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE_ERR);
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String userId, String objectId, boolean favoriteStatus) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "add_to_favorite").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("object_id", objectId);
        //urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_EVENT);
        if(!TextUtils.isEmpty(AppConstants.OBJECT_TYPE_GAME)
                && ( AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")))
        {
            urlBuilder.addQueryParameter("object_type", "event");
        }
        else {
            urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_EVENT);
        }
        if (favoriteStatus) {
            urlBuilder.addQueryParameter("is_favorite", Boolean.TRUE.toString());
        } else {
            urlBuilder.addQueryParameter("is_favorite", Boolean.FALSE.toString());
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(UpcomingPastDetail.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Display Menu pop up window
     */
    public void displayMenuPopUp() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.edit_game_menu_info_popup);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        wlp.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        TextView edit_game_text = (TextView)dialog.findViewById(R.id.edit_game_text);
        edit_game_text.setVisibility(View.GONE);
        TextView tv_cancel_game = (TextView)dialog.findViewById(R.id.cancel_game_text);
        tv_cancel_game.setVisibility(View.GONE);
        TextView tv_leave_game = (TextView)dialog.findViewById(R.id.leave_game_text);
        tv_leave_game.setVisibility(View.GONE);

        if(userId.equals(game_Creator_userId)) {
            //game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
            edit_game_text.setVisibility(View.VISIBLE);
            tv_cancel_game.setVisibility(View.VISIBLE);
        } else if(AppConstants.userIdArray.contains(userId)){
            //game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
            tv_leave_game.setVisibility(View.VISIBLE);
        } else{
            upcoming_game_detail_menu_icon_layout.setVisibility(View.GONE);
        }

        eventId = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Id();

        edit_game_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Constants.IS_EDIT_GAME_CLICKED = true;
                Constants.GAME_NAME = name;
                Constants.GAME_VISIBILITY = visibility;
                Constants.SKILL_LEVEL = skillLevel;
                Constants.NOTE = note;
                if(!TextUtils.isEmpty(total_player_count)) {
                    Constants.COURT_MAX_PLAYER_ = Integer.valueOf(total_player_count);
                }
                /*Keeping Court Total Player Count*/
                if(!TextUtils.isEmpty(court_total_player_capacity)) {
                    Constants.COURT_TOTAL_MAX_PLAYER_EDIT = Integer.parseInt(court_total_player_capacity);
                }
                if(!TextUtils.isEmpty(accepted_player_count)) {
                    Constants.NO_OF_PARTICIPANT = Integer.parseInt(accepted_player_count);
                }

                if(!TextUtils.isEmpty(list_ID)) {
                    startActivity(new Intent(UpcomingPastDetail.this, Additionalinfo.class));
                }
                else{
                    Constants.POI_GAME_ADDRESS = eventAddress;
                    Constants.POI_GAME_DATE = eventDate;
                    Constants.POI_GAME_START_TIME = eventStartTIme;
                    Constants.POI_GAME_END_TIME = eventEndTIme;
                    Constants.POI_GAME_END_COURT_TYPE = court_type;
                    Constants.GAME_VISIBILITY = visibility;
                    Constants.SKILL_LEVEL = skillLevel;
                    Constants.POI_LATITUDE = event_lat;
                    Constants.POI_LONGITUDE = event_lon;
                    if(!TextUtils.isEmpty(total_player_count)) {
                        Constants.COURT_MAX_PLAYER_ = Integer.valueOf(total_player_count);
                    }
                    if(!TextUtils.isEmpty(accepted_player_count)) {
                        Constants.NO_OF_PARTICIPANT = Integer.parseInt(accepted_player_count);
                    }
                    startActivity(new Intent(UpcomingPastDetail.this, CreateEventWithoutCourt.class));
                }
            }
        });

        tv_cancel_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "cancel_game";
                if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE_POINT))
                {
                    long upcoming_time_milisecond_policy = getUpcomingTimeInMiliseonds(Integer.parseInt(Constants.CANCELLATION_POLICY_TYPE_POINT));
                    long time_slots_milisecond_policy = getTimeSlotsDateTimeinMilisecond();

                    if(time_slots_milisecond_policy < upcoming_time_milisecond_policy)
                    {
                        String policyTitle = "Cancellation Policy\n ( "+Constants.CANCELLATION_POLICY_TYPE + " )";
                        String policyDescription = "The payment is non-refundable, because you have crossed the cancellation point.";

                        cancellationPolicyDialog(policyTitle,policyDescription);
                    }
                    else{
                        cancelGame(eventId);
                    }
                }
                else{
                    cancelGame(eventId);
                }
            }
        });

        tv_leave_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                type = "";
                if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE_POINT))
                {
                    long upcoming_time_milisecond_policy = getUpcomingTimeInMiliseonds(Integer.parseInt(Constants.CANCELLATION_POLICY_TYPE_POINT));
                    long time_slots_milisecond_policy = getTimeSlotsDateTimeinMilisecond();

                    if(time_slots_milisecond_policy < upcoming_time_milisecond_policy)
                    {
                        String policyTitle = "Cancellation Policy\n ( "+Constants.CANCELLATION_POLICY_TYPE + " )";
                        String policyDescription = "The payment is non-refundable, because you have crossed the cancellation point.";

                        cancellationPolicyDialog(policyTitle,policyDescription);
                    }
                    else{
                        leaveGame(eventId);
                    }
                }
                else{
                    leaveGame(eventId);
                }
            }
        });

        dialog.show();
    }

    /**
     * Function to get Upcoming detail from server
     */
    public void getUpcomingEventDetailsInfo(String court_Id, String userId) {
        //Handling the loader state
        LoaderUtility.handleLoader(UpcomingPastDetail.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForDetail(court_Id, userId);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(UpcomingPastDetail.this, false);
                Toast.makeText(UpcomingPastDetail.this,e.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "",venue_id ="",capacity = "", name = "",
                        latitude = "", longitude = "",review_rating = "",
                        review_count = "",court_type = "",description = "",
                        price = "",address = "",city = "",state = "",
                        country = "", total_number_players = "",
                        game_date = "", startTIme = "", endTime = "",
                        skillLevel = "", no_of_participants = "",
                        eventSportName = "", eventCourtName = "",
                        event_creator_name = "", event_creator_image = "",
                        event_creator_user_id = "", minimum_price = "",
                        actual_price = "",visibility = "",note = "";
                boolean isFavorite = false, isEquipmenmtAvailable = false;

                try {
                    //JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    eventListingModelArrayList = new ArrayList<EventListingModel>();
                    eventListingModel = new EventListingModel();
                    ArrayList<String> images_arraylist = new ArrayList<String>();

                    if(jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if(jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if(jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                        Constants.EVENT_NAME_MAP = name;
                    }
                    if(jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                        Constants.EVENT_LATITUDE = Double.parseDouble(latitude);
                    }
                    if(jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                        Constants.EVENT_LONGITUDE = Double.parseDouble(longitude);
                    }
                    if(jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }

                    if(jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }

                    if(jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if(jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }
                    if(jsonObject.has("note") && !jsonObject.isNull("note")) {
                        note = jsonObject.getString("note");
                    }
                    if(jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if(jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if(jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }

                    if(jsonObject.has("no_of_player") && !jsonObject.isNull("no_of_player")){
                        total_number_players = jsonObject.getString("no_of_player");
                    }

                    if(jsonObject.has("no_of_partitipants") && !jsonObject.isNull("no_of_partitipants")){
                        no_of_participants = jsonObject.getString("no_of_partitipants");
                    }

                    if(jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")){
                        isFavorite = jsonObject.getBoolean("is_favorite");
                    }

                    if(jsonObject.has("price") && !jsonObject.isNull("price")) {
                        try{
                            price = jsonObject.getString("price");
                            int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                            float price_pre_player = Float.parseFloat(price) / (float)totalPlayer;
                            actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                            float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                            minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                        }catch(Exception ex){
                            ex.printStackTrace();;
                        }
                    }

                    if(jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")){
                        eventSportName = jsonObject.getString("sport_name");
                    }

                    if(jsonObject.has("game_date") && !jsonObject.isNull("game_date")){
                        game_date = jsonObject.getString("game_date");
                    }

                    if(jsonObject.has("skill_level") && !jsonObject.isNull("skill_level")){
                        skillLevel = jsonObject.getString("skill_level");
                    }

                    if(jsonObject.has("list_name") && !jsonObject.isNull("list_name")){
                        eventCourtName = jsonObject.getString("list_name");
                    }

                    if(jsonObject.has("user_name") && !jsonObject.isNull("user_name")){
                        event_creator_name = jsonObject.getString("user_name");
                    }

                    if(jsonObject.has("user_id") && !jsonObject.isNull("user_id")){
                        event_creator_user_id = jsonObject.getString("user_id");
                    }

                    if(jsonObject.has("user_profile_image") && !jsonObject.isNull("user_profile_image")){
                        event_creator_image = jsonObject.getString("user_profile_image");
                    }
                    if(jsonObject.has("visibility") && !jsonObject.isNull("visibility")){
                        visibility = jsonObject.getString("visibility");
                    }

                    if(jsonObject.has("equipments") && !jsonObject.isNull("equipments")){
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");

                        if(equipmentArray.length() > 0){
                            isEquipmenmtAvailable = true;
                        }
                    }

                    if(jsonObject.has("time_slot")&& !jsonObject.isNull("time_slot")) {
                        String start_time = null;
                        JSONArray time_slot_array_json_array = jsonObject.getJSONArray("time_slot");
                        for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                            JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                            if(j == 0){
                                startTIme = getValues.getString("start_time");
                            }
                            endTime = getValues.getString("end_time");
                        }
                    }


                    if(jsonObject.has("images")&& !jsonObject.isNull("images")) {
                        ArrayList<String> images_array = new ArrayList<String>();
                        JSONArray images_json_array = jsonObject.getJSONArray("images");
                        for (int j = 0; j < images_json_array.length(); j++) {
                            images_array.add(images_json_array.getString(j));
                        }
                        eventListingModel.setImages_array(images_array);
                    }


                    if(jsonObject.has("game_partitipants")&& !jsonObject.isNull("game_partitipants")){

                        try{
                            JSONArray particiapntsArray = jsonObject.getJSONArray("game_partitipants");
                            AppConstants.userModelArary = new ArrayList<UserMolel>();
                            AppConstants.userIdArray = new ArrayList<String>();
                            if(particiapntsArray.length() > 0){
                                for(int i = 0; i < particiapntsArray.length(); i++){
                                    UserMolel model = new UserMolel();
                                    JSONObject participantsObj = particiapntsArray.getJSONObject(i);
                                    String userName = participantsObj.getString("user_name");
                                    String userId = participantsObj.getString("user_id");
                                    String user_profile_image = participantsObj.getString("user_profile_image");
                                    model.setUserFullName(userName);
                                    model.setUserId(userId);
                                    model.setUserProfileImage(user_profile_image);
                                    AppConstants.userIdArray.add(userId);
                                    AppConstants.userModelArary.add(model);
                                }
                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }

                    eventListingModel.setEvent_Id(id);
                    eventListingModel.setEvent_Name(name);
                    eventListingModel.setLatitude(latitude);
                    eventListingModel.setLongitude(longitude);
                    eventListingModel.setEvent_Ratings(review_rating);
                    eventListingModel.setEvent_Reviews(review_count);
                    eventListingModel.setEvent_Price(actual_price);
                    eventListingModel.setCourt_type(court_type);
                    eventListingModel.setDescription(description);
                    eventListingModel.setAddress(address);
                    eventListingModel.setVisibility(visibility);
                    eventListingModel.setNote(note);
                    eventListingModel.setCity(city);
                    eventListingModel.setState(state);
                    eventListingModel.setCountry(country);
                    eventListingModel.setCapacity(capacity);
                    eventListingModel.setEvent_Total_Participants(total_number_players);
                    eventListingModel.setEventDate(game_date);
                    eventListingModel.setEventStartTime(startTIme);
                    eventListingModel.setEventEndTime(endTime);
                    eventListingModel.setSkill_level(skillLevel);
                    eventListingModel.setEvent_accepted_Participants(no_of_participants);
                    eventListingModel.setEvent_Sports_Type(eventSportName);
                    eventListingModel.setEvent_court_name(eventCourtName);
                    eventListingModel.setEvent_Creator_Image(event_creator_image);
                    eventListingModel.setCreator_name(event_creator_name);
                    eventListingModel.setCreator_user_id(event_creator_user_id);
                    eventListingModel.setEvent_Minimum_Price(minimum_price);
                    eventListingModel.setFavorite(isFavorite);
                    eventListingModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    eventListingModelArrayList.add(eventListingModel);

                }catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(UpcomingPastDetail.this, false);

                if(isSuccessStatus) {
                    UpcomingPastDetail.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //context.finish();
                            UpcomingPastAdapter.eventListingModelArrayList = eventListingModelArrayList;
                            Toast.makeText(UpcomingPastDetail.this, "Event updates successfully", Toast.LENGTH_SHORT).show();
                            /**Fetch data Upcoming / Past detail API*/
                            setUpcomingData();
                        }
                    });
                }
                else
                {
                    snackBar.setSnackBarMessage("Network error! Please try after sometime");
                }
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForDetail(String court_Id, String userId) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_game_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.EVENT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", userId);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Constants.UPCOMING_PAST_CLICKED_FOR_UPDATE_GAME)
        {
            Constants.UPCOMING_PAST_CLICKED_FOR_UPDATE_GAME = false;
            getUpcomingEventDetailsInfo(UpcomingPastAdapter.event_id, userId);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Constants.UPCOMING_PAST_CLICKED = false;
        finish();
    }


    /**
     * Request persmission for Post lollipop devices
     */
    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(UpcomingPastDetail.this,
                new String[]{Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR},
                1);
    }

    /**
     * Check permission
     * @return
     */
    private boolean checkIfAlreadyhavePermission() {
        int result_calender_read = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CALENDAR);
        int result_calender_write = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_CALENDAR);
        if (result_calender_read == PackageManager.PERMISSION_GRANTED &&
                result_calender_write == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean getCalenderSyncStatus = calenderSyncUtility.getCalendarEventExistance(name);
                    if(!getCalenderSyncStatus){
                        //Syncing to calender
                        syncToCalender();
                    }else{
                        Toast.makeText(UpcomingPastDetail.this, "This event is already added to the calendar", Toast.LENGTH_LONG).show();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // Toast.makeText(Profile.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }


    /**
     * Function responsible for asking the ren time permission for calender rean adn write
     */
    public void doCalenderSync() {
        int getDeviceVersion = Build.VERSION.SDK_INT;

        /**
         *
         */
        if (getDeviceVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                //Requesting run time permission to add event in the calendar
                requestPermissionAccessCalendar();
            } else{

                /**
                 * Checking the event sync status in the calendar and
                 * displaying the message accrodingly
                 */

                // Getting the calendar sync status
                boolean getCalenderSyncStatus = calenderSyncUtility.getCalendarEventExistance(name);
                if(!getCalenderSyncStatus){
                    //Syncing to calender
                    syncToCalender();
                }else{
                    //Displaying the message to the user if the event is already sync with the calendar
                    Toast.makeText(UpcomingPastDetail.this,
                            "This event is already added to the calendar",
                            Toast.LENGTH_LONG).show();
                }
            }
        }
        else{
            //Function call for Syncing the event in the calender
            syncToCalender();
        }
    }

    /**
     * Function to request Permission
     *
     * @return
     */
    private void requestPermissionAccessCalendar() {
        final String[] permissions = new String[]{Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR};
        if (android.support.v13.app.ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CALENDAR)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("To add events to device caleder. You need to give permission to access your device calender");
            builder.setTitle("Access Calendar");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    android.support.v13.app.ActivityCompat.requestPermissions(UpcomingPastDetail.this, permissions, 1);
                }
            });
            builder.show();
        } else {
            android.support.v13.app.ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALENDAR}, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyLeaveGame(String game_id){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE+"leave_game?").newBuilder();
        urlBuilder.addQueryParameter("game_id", game_id);
        urlBuilder.addQueryParameter("user_id", userId);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Calling the API to getting the sports
     */

    public void leaveGame(String eventId){
        //Displaying loader
        LoaderUtility.handleLoader(UpcomingPastDetail.this, true);
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBodyLeaveGame(eventId);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                SplashScreen.displayMessageInUiThread(UpcomingPastDetail.this,e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages
                        displayMessage(message);
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                UpcomingPastDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            Constants.LEAVED_STATUS = true;
                            UpcomingPastDetail.this.finish();
                        }
                    }
                });
                LoaderUtility.handleLoader(UpcomingPastDetail.this, false);
            }
        });
    }

    /**
     * Dispaly ing the message
     * @param message status message
     */
    public void displayMessage(final String message){
        UpcomingPastDetail.this.runOnUiThread(new Runnable() {
            public void run() {

                //Displaying the success message after successful sign up
                //Toast.makeText(ForgotPasswordScreen.this,message, Toast.LENGTH_LONG).show();
                // snackbar.setSnackBarMessage(message);
                Toast.makeText(UpcomingPastDetail.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Function responsible for displaying of the dialog
     */
    public void cancellationPolicyDialog(String policy_title, String policy_description){
        mMaterialDialog = new MaterialDialog(UpcomingPastDetail.this);
        View view = LayoutInflater.from(UpcomingPastDetail.this)
                .inflate(R.layout.layout_dialog_cancellation_policy,
                        null);

        TextView tv_cancellation_policy_title = (TextView) view.findViewById(R.id.tv_cancellation_policy_title);
        TextView tv_cancellation_policy = (TextView) view.findViewById(R.id.tv_cancellation_policy);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel_policy);
        TextView tv_confirm = (TextView) view.findViewById(R.id.tv_confirm_policy);

        tv_cancellation_policy_title.setText(policy_title);
        tv_cancellation_policy.setText(policy_description);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                if(!TextUtils.isEmpty(type) && type.equalsIgnoreCase("cancel_game"))
                {
                    /**Cancel game API*/
                    cancelGame(eventId);
                }
                else{
                    /**Leave game API*/
                    leaveGame(eventId);
                }

            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }

    /**
     * Getting next day's 24 time from the current date n time
     */
    public long getUpcomingTimeInMiliseonds(int hour)
    {
        long time_milisecond = 0;
        SimpleDateFormat inputFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        String currentDateandTime = sdf.format(new Date());
        Date current_time = null;
        try {
            current_time = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();
        c.setTime(current_time);
        //c.add(Calendar.DATE, 1);
        c.add(Calendar.HOUR, hour);

        String current = sdf.format(c.getTime());

        Date current_timee = null;
        try {
            current_timee = sdf.parse(current);
            time_milisecond = current_timee.getTime();
            System.out.print(""+time_milisecond);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time_milisecond;
    }

    /**
     *
     */
    public long getTimeSlotsDateTimeinMilisecond()
    {
        long time_milisecond = 0;
        String date = eventDate;

        String timeSlots = date+" "+eventStartTIme;

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mma");

        try {
            Date date1 = inputFormatter.parse(timeSlots);
            time_milisecond = date1.getTime();

            System.out.print(""+time_milisecond);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time_milisecond;
    }
    /**
     * Cancel Game API
     */
    public void cancelGame(String eventId){
        //Displaying loader
        LoaderUtility.handleLoader(UpcomingPastDetail.this, true);
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBodyCancelGame(eventId);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                SplashScreen.displayMessageInUiThread(UpcomingPastDetail.this,e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages
                        displayMessage(message);
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                UpcomingPastDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            Constants.CANCEL_STATUS = true;
                            UpcomingPastDetail.this.finish();
                        }
                    }
                });
                LoaderUtility.handleLoader(UpcomingPastDetail.this, false);
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyCancelGame(String game_id){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE+"cancel_game").newBuilder();
        urlBuilder.addQueryParameter("game_id", game_id);
        urlBuilder.addQueryParameter("cancelled_at", getCurrentDateAndTIme());
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Get Current Date and time
     * @return
     */
    public static String getCurrentDateAndTIme(){
        //TimeZone utc = TimeZone.getTimeZone("UTC");
        DateFormat dateFormatter = new SimpleDateFormat("dd/mm/yyyy hh:mm a", Locale.US);
        // dateFormatter.setTimeZone(utc);
        //dateFormatter.setLenient(false);
        Date today = new Date();
        String current_time = dateFormatter.format(today);
        return current_time;
    }

    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayShareDialog(final String eventId, final String eventSport, final String eventDate,
                                   final String eventStartTime, final String eventendTime){
        mMaterialDialog = new MaterialDialog(UpcomingPastDetail.this);
        View view = LayoutInflater.from(UpcomingPastDetail.this)
                .inflate(R.layout.layout_update_card_status,
                        null);
        TextView tv_card_numner = (TextView) view.findViewById(R.id.tv_card_number);
        TextView tv_share_using_facebook = (TextView) view.findViewById(R.id.tv_make_card_primary);
        TextView tv_share_using_sms = (TextView) view.findViewById(R.id.tv_delete_card);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_cancel.setText("CANCEL");
        tv_card_numner.setText("Share Using");
        tv_share_using_facebook.setText("Facebook");
        tv_share_using_sms.setText("SMS");


        tv_share_using_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                //Sharing and deep linking the list details in to facebook
                //Initializing the facebook share dialog
                shareGameDetails(eventId, eventSport,
                        eventDate, eventStartTime,
                        eventendTime);

            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_share_using_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                String eventSportName = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Sports_Type();
                //Sharing the venue details through SMS
                FinishEventCreation.shareDeepLinkUsingSms(eventId,
                        AppConstants.OBJECT_TYPE_EVENT,
                        UpcomingPastDetail.this, eventSportName);


            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }

    /**
     * Displaying player price depending upon the object type
     */
    public void displayPlayerPrice()
    {
        if(TextUtils.isEmpty(list_ID))
        {
            upcoming_detail_price.setVisibility(View.GONE);
            upcoming_detail_Minimum_Amount_Text.setVisibility(View.GONE);
            upcoming_price_players.setVisibility(View.GONE);

        }
        else if((AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Game")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("game"))) {

            if (!TextUtils.isEmpty(eventTotalPrice) &&
                    !eventTotalPrice.equalsIgnoreCase("0")) {
                upcoming_detail_price.setText("$ " + eventTotalPrice);
                //upcoming_detail_Minimum_Amount_Text.setText("As low as $" + getMinimumPrice);
                double total_projected_cost = Double.valueOf(eventTotalPrice) / Double.valueOf(accepted_player_count);
                upcoming_detail_Minimum_Amount_Text.setText("Projected cost: " + "$ " + ApplicationUtility.twoDigitAfterDecimal(total_projected_cost));

            } else {
                upcoming_detail_price.setText("FREE");
                upcoming_detail_Minimum_Amount_Text.setVisibility(View.GONE);
                upcoming_price_players.setVisibility(View.GONE);
            }
        }
        else if((AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event")))
        {
            if (!TextUtils.isEmpty(eventTotalPrice) &&
                    !eventTotalPrice.equalsIgnoreCase("0")) {
                upcoming_detail_price.setText("$ " + eventTotalPrice);
                //upcoming_detail_Minimum_Amount_Text.setText("As low as $" + getMinimumPrice);
                double total_projected_cost = Double.valueOf(eventTotalPrice) / Double.valueOf(accepted_player_count);
                upcoming_detail_Minimum_Amount_Text.setText("Projected cost: " + "$ " + ApplicationUtility.twoDigitAfterDecimal(total_projected_cost));

            } else {
                upcoming_detail_price.setText("FREE");
                upcoming_detail_Minimum_Amount_Text.setVisibility(View.GONE);
                upcoming_price_players.setVisibility(View.GONE);
            }
        }

    }
}
