package com.andolasoft.squadz.activities;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;

import java.util.ArrayList;
import java.util.TimeZone;

public class InterestAndExperienceScreen extends Activity {

    static int progressChanged = 0;
    static int progressChangedPrimary = 0;
    static int progressChangedBasketball = 0;
    static int progressChangedBaseball = 0;
    static int progressChangedFootball = 0;
    static int progressChangedSoccer = 0;
    static int progressChangedTennis = 0;
    static int progressChangedVolleyball = 0;
    static int progressChangedGolf = 0;
    static int progressChangedRunning = 0;
    static int progressChangedCricket = 0;
    static int progressChangedBiking = 0;
    static int progressChangedRugby = 0;
    static int progressChangedFrisbee = 0;
    TextView done, profile_title, primarySport, primarySportSeekbarText,
            basketballSeekbarText, baseballSeekbarText, footballSeekbarText,
            soccerSeekbarText, tennisSeekbarText, volleyballSeekbarText,
            golfSeekbarText, runningSeekbarText, cricketSeekbarText,
            bikingSeekbarText, rugbySeekBarText, frisbeeseekbarText;
    ImageView back_editprofile, primarySportPlus, primarySportMinus,
            basketballPlus, basketballMinus, baseballPlus, baseballMinus,
            footballPlus, footballMinus, soccerPlus, soccerMinus, tennisPlus,
            tennisMinus, volleyballPlus, volleyballMinus, golfPlus, golfMinus,
            runningPlus, runningMinus, cricketPlus, cricketMinus, bikingPlus,
            bikingMinus, rugby_plus, rugby_minus, frisbee_plus, frisbee_minus;
    DatabaseAdapter db;
    String primary_sport, auth_token, device_info_id, loginuser_id,
            getusername, login_user_id, birth_date;
    SeekBar primary_seekbar, baseball_seekbar, basketball_seekbar,
            football_seekbar, soccer_seekbar, tennis_seekbar,
            volleyball_seekbar, golf_seekbar, running_seekbar, cricket_seekbar,
            biking_seekbar, rugbySeekbar, frisbeeSeekBar;
    RelativeLayout sportBasketball, sportBaseball, sportFootball, sportSoccer,
            sportTennis, sportVolleyball, sportGolf, sportRunning,
            sportCricket, sportBiking, sportRugby, sportfreebie;
    View viewBasketball, viewBaseball, viewFootball, viewSoccer, viewTennis,
            viewVolleyball, viewGolf, viewRunning, viewCricket;
    TextView edit_interest_exp_save;
    ImageView backbtn_interest_exp;
    View mCustomVIew;
    ArrayList<Integer> skill_data;
    String[] sportlist = {"Basketball", "Baseball", "Football", "Soccer",
            "Tennis", "Volleyball", "Golf", "Running"};
    ArrayAdapter<String> sportsadapter;
    // ImageView selectSport;
    String name, user_email, phone_number, user_aboutme, friend_id,
            from_intent, intent_extra, getOtherSport;
    String username, firstname, lastname, email, android_id, password,
            phoneNumber, encodedImage, device_timezone, fb_userid, intentName,
            user_email_api;
    double latitude, longitude;
    SharedPreferences pref;
    boolean is_available;
    String user_id, current_status, image, user_sport, intent_extra_fragment,
            user_id_api;
    int game_invitation_count, friend_invitation_count;
    SharedPreferences profileSp;
    SharedPreferences.Editor profileEditor;
    Handler handler = new Handler();
    SharedPreferences notificationSettingsSp, privacySettingsSp;
    SharedPreferences.Editor notificationSettingsEditor, privacySettingsEditor;
    String user_type;
    ImageView btnback;

    // RelativeLayout
    // noram_user_signup_indicatioe,facebook_user_progress_indicator;

    // check whether the num is between x
    public static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        setContentView(R.layout.activity_new_interest_and_experience);

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(InterestAndExperienceScreen.this);

        //Getting user data from SharedPrefaerance
        getUserData();

        db = new DatabaseAdapter(getApplicationContext());

        skill_data = new ArrayList<Integer>();

        notificationSettingsSp = InterestAndExperienceScreen.this
                .getSharedPreferences("NotificationSettings",
                        InterestAndExperienceScreen.this.MODE_PRIVATE);

        notificationSettingsEditor = notificationSettingsSp.edit();

        Intent intent = getIntent();
        user_type = intent.getStringExtra("Facebook_user");

        //getProfileData();

        // selectSport = (ImageView) findViewById(R.id.selectSport);
        done = (TextView) findViewById(R.id.tv_done);
        btnback = (ImageView) findViewById(R.id.btn_back);
        primary_seekbar = (SeekBar) findViewById(R.id.primarySeekBar);
        basketball_seekbar = (SeekBar) findViewById(R.id.basketball_seekBar);
        baseball_seekbar = (SeekBar) findViewById(R.id.baseball_seekBar);
        football_seekbar = (SeekBar) findViewById(R.id.football_seekBar);
        soccer_seekbar = (SeekBar) findViewById(R.id.soccer_seekBar);
        tennis_seekbar = (SeekBar) findViewById(R.id.tennis_seekBar);
        volleyball_seekbar = (SeekBar) findViewById(R.id.volleyball_seekBar);
        golf_seekbar = (SeekBar) findViewById(R.id.golf_seekBar);
        running_seekbar = (SeekBar) findViewById(R.id.running_seekBar);
        cricket_seekbar = (SeekBar) findViewById(R.id.cricket_seekBar);
        biking_seekbar = (SeekBar) findViewById(R.id.biking_seekBar);
        rugbySeekbar = (SeekBar) findViewById(R.id.rugby_seekBar);
        frisbeeSeekBar = (SeekBar) findViewById(R.id.ultimate_freesbies_seekBar);

        primarySportSeekbarText = (TextView) findViewById(R.id.primarySportSeekbarText);
        basketballSeekbarText = (TextView) findViewById(R.id.basketballSeekbarText);
        baseballSeekbarText = (TextView) findViewById(R.id.baseballSeekbarText);
        footballSeekbarText = (TextView) findViewById(R.id.footballSeekbarText);
        soccerSeekbarText = (TextView) findViewById(R.id.soccerSeekbarText);
        tennisSeekbarText = (TextView) findViewById(R.id.tennisSeekbarText);
        volleyballSeekbarText = (TextView) findViewById(R.id.volleyballSeekbarText);
        golfSeekbarText = (TextView) findViewById(R.id.golfSeekbarText);
        runningSeekbarText = (TextView) findViewById(R.id.runningSeekbarText);
        cricketSeekbarText = (TextView) findViewById(R.id.cricketSeekbarText);
        bikingSeekbarText = (TextView) findViewById(R.id.bikingSeekbarText);
        rugbySeekBarText = (TextView) findViewById(R.id.rugbySeekbarText);
        frisbeeseekbarText = (TextView) findViewById(R.id.ultimate_freesbiesSeekbarText);

        sportBasketball = (RelativeLayout) findViewById(R.id.basketball_others_layout_main_layout);
        sportBaseball = (RelativeLayout) findViewById(R.id.basetball_primary_layout_main_layout);
        sportFootball = (RelativeLayout) findViewById(R.id.football_primary_layout_main_layout);
        sportSoccer = (RelativeLayout) findViewById(R.id.soccer_primary_layout_main_layout);
        sportTennis = (RelativeLayout) findViewById(R.id.tennis_primary_layout_main_layout);
        sportVolleyball = (RelativeLayout) findViewById(R.id.volleyball_primary_layout_main_layout);
        sportGolf = (RelativeLayout) findViewById(R.id.golf_primary_layout_main_layout);
        sportRunning = (RelativeLayout) findViewById(R.id.running_primary_layout_main_layout);
        sportCricket = (RelativeLayout) findViewById(R.id.cricket_primary_layout_main_layout);
        sportBiking = (RelativeLayout) findViewById(R.id.biking_primary_layout_main_layout);
        sportRugby = (RelativeLayout) findViewById(R.id.rugby_primary_layout_main_layout);
        sportfreebie = (RelativeLayout) findViewById(R.id.ultimate_freesbies_primary_layout_main_layout);

        viewBasketball = (View) findViewById(R.id.basketball_view);
        viewBaseball = (View) findViewById(R.id.edit_profile_view_interest);
        viewFootball = (View) findViewById(R.id.soccer_edit_profile_view_interest);
        viewSoccer = (View) findViewById(R.id.tennis_edit_profile_view_interest);
        viewTennis = (View) findViewById(R.id.volley_edit_profile_view_interest);
        viewVolleyball = (View) findViewById(R.id.golf_edit_profile_view_interest);
        viewGolf = (View) findViewById(R.id.running_edit_profile_view_interest);
        viewRunning = (View) findViewById(R.id.cricket_edit_profile_view_interest);
        viewCricket = (View) findViewById(R.id.biking_edit_profile_view_interest);

        primarySport = (TextView) findViewById(R.id.basketball_primary_lay);

        primarySportPlus = (ImageView) findViewById(R.id.primarySportPlus);
        primarySportMinus = (ImageView) findViewById(R.id.primarySportMinus);
        basketballPlus = (ImageView) findViewById(R.id.basketballPlus);
        basketballMinus = (ImageView) findViewById(R.id.basketballMinus);
        baseballPlus = (ImageView) findViewById(R.id.baseballPlus);
        baseballMinus = (ImageView) findViewById(R.id.baseballMinus);
        footballPlus = (ImageView) findViewById(R.id.footballPlus);
        footballMinus = (ImageView) findViewById(R.id.footballMinus);
        soccerPlus = (ImageView) findViewById(R.id.soccerPlus);
        soccerMinus = (ImageView) findViewById(R.id.soccerMinus);
        tennisPlus = (ImageView) findViewById(R.id.tennisPlus);
        tennisMinus = (ImageView) findViewById(R.id.tennisMinus);
        volleyballPlus = (ImageView) findViewById(R.id.volleyballPlus);
        volleyballMinus = (ImageView) findViewById(R.id.volleyballMinus);
        golfPlus = (ImageView) findViewById(R.id.golfPlus);
        golfMinus = (ImageView) findViewById(R.id.golfMinus);
        runningPlus = (ImageView) findViewById(R.id.runningPlus);
        runningMinus = (ImageView) findViewById(R.id.runningMinus);
        cricketPlus = (ImageView) findViewById(R.id.cricketPlus);
        cricketMinus = (ImageView) findViewById(R.id.cricketMinus);
        bikingPlus = (ImageView) findViewById(R.id.bikingPlus);
        bikingMinus = (ImageView) findViewById(R.id.bikingMinus);

        rugby_plus = (ImageView) findViewById(R.id.tugbyPlus);
        rugby_minus = (ImageView) findViewById(R.id.rugbyMinus);
        frisbee_plus = (ImageView) findViewById(R.id.ultimate_freesbiesPlus);
        frisbee_minus = (ImageView) findViewById(R.id.ultimate_freesbiesMinus);

        profileSp = getSharedPreferences("SignupData", MODE_PRIVATE);
        profileEditor = profileSp.edit();

        privacySettingsSp = getSharedPreferences("PrivacySettings",
                MODE_PRIVATE);
        privacySettingsEditor = privacySettingsSp.edit();

        primary_sport = "Baseball";


        if (!TextUtils.isEmpty(profileSp.getString(Constants.PARAM_PRIMATY_SPORT, null))) {
            primary_sport = profileSp.getString(Constants.PARAM_PRIMATY_SPORT, null);

        }
        primarySportSeekbarText.setText(profileSp.getString("skill_level",
                "No Experience"));
        primarySport.setText(primary_sport);

        // update view after selecting sport
        updateView(primary_sport);

        username = profileSp.getString("username", null);
        firstname = profileSp.getString("firstname", null);
        lastname = profileSp.getString("lastname", null);
        email = profileSp.getString("email", null);
        password = profileSp.getString("password", null);
        phoneNumber = profileSp.getString("phone_number", null);
        encodedImage = profileSp.getString("image", null);
        fb_userid = profileSp.getString("user_id", null);
        intentName = profileSp.getString("intent_name", null);
        birth_date = profileSp.getString("bdate", null);

        try {
            TimeZone tz = TimeZone.getDefault();
            String displayname = tz.getDisplayName(false, TimeZone.SHORT);
            device_timezone = tz.getID();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        done.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                InterestAndExperienceScreen.this.finish();
            }
        });

        btnback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                InterestAndExperienceScreen.this.finish();
            }
        });

        displayRestoredData();

        primary_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        primarySportSeekbarText
                                .setText(setSeekbarText(progressChangedPrimary));
                        profileEditor.putString("skill_level",
                                setSeekbarText(progressChangedPrimary));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedPrimary = progress;

                    }
                });

        basketball_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        basketballSeekbarText
                                .setText(setSeekbarText(progressChangedBasketball));
                        profileEditor.putString("basketball_skill_level",
                                setSeekbarText(progressChangedBasketball));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedBasketball = progress;

                    }
                });

        baseball_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        baseballSeekbarText
                                .setText(setSeekbarText(progressChangedBaseball));
                        profileEditor.putString("baseball_skill_level",
                                setSeekbarText(progressChangedBaseball));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedBaseball = progress;

                    }
                });

        football_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        footballSeekbarText
                                .setText(setSeekbarText(progressChangedFootball));
                        profileEditor.putString("football_skill_level",
                                setSeekbarText(progressChangedFootball));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedFootball = progress;

                    }
                });

        soccer_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        soccerSeekbarText
                                .setText(setSeekbarText(progressChangedSoccer));
                        profileEditor.putString("soccer_skill_level",
                                setSeekbarText(progressChangedSoccer));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedSoccer = progress;

                    }
                });

        tennis_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        tennisSeekbarText
                                .setText(setSeekbarText(progressChangedTennis));
                        profileEditor.putString("tennis_skill_level",
                                setSeekbarText(progressChangedTennis));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedTennis = progress;

                    }
                });

        volleyball_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        volleyballSeekbarText
                                .setText(setSeekbarText(progressChangedVolleyball));
                        profileEditor.putString("volleyball_skill_level",
                                setSeekbarText(progressChangedVolleyball));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedVolleyball = progress;

                    }
                });

        golf_seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                golfSeekbarText.setText(setSeekbarText(progressChangedGolf));
                profileEditor.putString("golf_skill_level",
                        setSeekbarText(progressChangedGolf));
                profileEditor.commit();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                progressChangedGolf = progress;

            }
        });

        running_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        runningSeekbarText
                                .setText(setSeekbarText(progressChangedRunning));
                        profileEditor.putString("running_skill_level",
                                setSeekbarText(progressChangedRunning));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedRunning = progress;

                    }
                });

        cricket_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        cricketSeekbarText
                                .setText(setSeekbarText(progressChangedCricket));
                        profileEditor.putString("cricket_skill_level",
                                setSeekbarText(progressChangedCricket));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedCricket = progress;

                    }
                });

        biking_seekbar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        bikingSeekbarText
                                .setText(setSeekbarText(progressChangedBiking));
                        profileEditor.putString("biking_skill_level",
                                setSeekbarText(progressChangedBiking));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedBiking = progress;

                    }
                });

        rugbySeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                rugbySeekBarText.setText(setSeekbarText(progressChangedBiking));
                profileEditor.putString("rugby_skill_level",
                        setSeekbarText(progressChangedRugby));
                profileEditor.commit();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                progressChangedRugby = progress;

            }
        });

        frisbeeSeekBar
                .setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        frisbeeseekbarText
                                .setText(setSeekbarText(progressChangedBiking));
                        profileEditor.putString("ultimate_skill_level",
                                setSeekbarText(progressChangedFrisbee));
                        profileEditor.commit();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progressChangedFrisbee = progress;

                    }
                });

        primarySportPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarPlusPosition(progressChangedPrimary);
                primary_seekbar.setProgress(pos);
                progressChangedPrimary = pos;
                primarySportSeekbarText
                        .setText(setSeekbarText(progressChangedPrimary));
                profileEditor.putString("skill_level",
                        setSeekbarText(progressChangedPrimary));
                profileEditor.commit();
            }
        });

        primarySportMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarMinusPosition(progressChangedPrimary);
                primary_seekbar.setProgress(pos);
                progressChangedPrimary = pos;
                primarySportSeekbarText
                        .setText(setSeekbarText(progressChangedPrimary));
                profileEditor.putString("skill_level",
                        setSeekbarText(progressChangedPrimary));
                profileEditor.commit();

            }
        });

        basketballPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarPlusPosition(progressChangedBasketball);
                basketball_seekbar.setProgress(pos);
                progressChangedBasketball = pos;
                basketballSeekbarText
                        .setText(setSeekbarText(progressChangedBasketball));
                profileEditor.putString("basketball_skill_level",
                        setSeekbarText(progressChangedBasketball));
                profileEditor.commit();

            }
        });

        basketballMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarMinusPosition(progressChangedBasketball);
                basketball_seekbar.setProgress(pos);
                progressChangedBasketball = pos;
                basketballSeekbarText
                        .setText(setSeekbarText(progressChangedBasketball));
                profileEditor.putString("basketball_skill_level",
                        setSeekbarText(progressChangedBasketball));
                profileEditor.commit();

            }
        });

        baseballPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarPlusPosition(progressChangedBaseball);
                baseball_seekbar.setProgress(pos);
                progressChangedBaseball = pos;
                baseballSeekbarText
                        .setText(setSeekbarText(progressChangedBaseball));
                profileEditor.putString("baseball_skill_level",
                        setSeekbarText(progressChangedBaseball));
                profileEditor.commit();

            }
        });

        baseballMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                int pos = setSeekbarMinusPosition(progressChangedBaseball);
                baseball_seekbar.setProgress(pos);
                progressChangedBaseball = pos;
                baseballSeekbarText
                        .setText(setSeekbarText(progressChangedBaseball));
                profileEditor.putString("baseball_skill_level",
                        setSeekbarText(progressChangedBaseball));
                profileEditor.commit();
            }
        });

        footballPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarPlusPosition(progressChangedFootball);
                football_seekbar.setProgress(pos);
                progressChangedFootball = pos;
                footballSeekbarText
                        .setText(setSeekbarText(progressChangedFootball));
                profileEditor.putString("football_skill_level",
                        setSeekbarText(progressChangedFootball));
                profileEditor.commit();
            }
        });

        footballMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarMinusPosition(progressChangedFootball);
                football_seekbar.setProgress(pos);
                progressChangedFootball = pos;
                footballSeekbarText
                        .setText(setSeekbarText(progressChangedFootball));
                profileEditor.putString("football_skill_level",
                        setSeekbarText(progressChangedFootball));
                profileEditor.commit();
            }
        });

        soccerPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarPlusPosition(progressChangedSoccer);
                soccer_seekbar.setProgress(pos);
                progressChangedSoccer = pos;
                soccerSeekbarText
                        .setText(setSeekbarText(progressChangedSoccer));
                profileEditor.putString("soccer_skill_level",
                        setSeekbarText(progressChangedSoccer));
                profileEditor.commit();

            }
        });

        soccerMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarMinusPosition(progressChangedSoccer);
                soccer_seekbar.setProgress(pos);
                progressChangedSoccer = pos;
                soccerSeekbarText
                        .setText(setSeekbarText(progressChangedSoccer));
                profileEditor.putString("soccer_skill_level",
                        setSeekbarText(progressChangedSoccer));
                profileEditor.commit();

            }
        });
        tennisPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarPlusPosition(progressChangedTennis);
                tennis_seekbar.setProgress(pos);
                progressChangedTennis = pos;
                tennisSeekbarText
                        .setText(setSeekbarText(progressChangedTennis));
                profileEditor.putString("tennis_skill_level",
                        setSeekbarText(progressChangedTennis));
                profileEditor.commit();
            }
        });

        tennisMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int pos = setSeekbarMinusPosition(progressChangedTennis);
                tennis_seekbar.setProgress(pos);
                progressChangedTennis = pos;
                tennisSeekbarText
                        .setText(setSeekbarText(progressChangedTennis));
                profileEditor.putString("tennis_skill_level",
                        setSeekbarText(progressChangedTennis));
                profileEditor.commit();
            }
        });

        volleyballPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), "plus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarPlusPosition(progressChangedVolleyball);
                volleyball_seekbar.setProgress(pos);
                progressChangedVolleyball = pos;
                volleyballSeekbarText
                        .setText(setSeekbarText(progressChangedVolleyball));
                profileEditor.putString("volleyball_skill_level",
                        setSeekbarText(progressChangedVolleyball));
                profileEditor.commit();

            }
        });

        volleyballMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), "minus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarMinusPosition(progressChangedVolleyball);
                volleyball_seekbar.setProgress(pos);
                progressChangedVolleyball = pos;
                volleyballSeekbarText
                        .setText(setSeekbarText(progressChangedVolleyball));
                profileEditor.putString("volleyball_skill_level",
                        setSeekbarText(progressChangedVolleyball));
                profileEditor.commit();

            }
        });

        golfPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), "plus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarPlusPosition(progressChangedGolf);
                golf_seekbar.setProgress(pos);
                progressChangedGolf = pos;
                golfSeekbarText.setText(setSeekbarText(progressChangedGolf));
                profileEditor.putString("golf_skill_level",
                        setSeekbarText(progressChangedGolf));
                profileEditor.commit();

            }
        });

        golfMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), "minus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarMinusPosition(progressChangedGolf);
                golf_seekbar.setProgress(pos);
                progressChangedGolf = pos;
                golfSeekbarText.setText(setSeekbarText(progressChangedGolf));
                profileEditor.putString("golf_skill_level",
                        setSeekbarText(progressChangedGolf));
                profileEditor.commit();

            }
        });

        runningPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), "plus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarPlusPosition(progressChangedRunning);
                running_seekbar.setProgress(pos);
                progressChangedRunning = pos;
                runningSeekbarText
                        .setText(setSeekbarText(progressChangedRunning));
                profileEditor.putString("running_skill_level",
                        setSeekbarText(progressChangedRunning));
                profileEditor.commit();
            }
        });

        runningMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), "minus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarMinusPosition(progressChangedRunning);
                running_seekbar.setProgress(pos);
                progressChangedRunning = pos;
                runningSeekbarText
                        .setText(setSeekbarText(progressChangedRunning));
                profileEditor.putString("running_skill_level",
                        setSeekbarText(progressChangedRunning));
                profileEditor.commit();
            }
        });

        cricketPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), "plus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarPlusPosition(progressChangedCricket);
                cricket_seekbar.setProgress(pos);
                progressChangedCricket = pos;
                cricketSeekbarText
                        .setText(setSeekbarText(progressChangedCricket));
                profileEditor.putString("cricket_skill_level",
                        setSeekbarText(progressChangedCricket));
                profileEditor.commit();
            }
        });

        cricketMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(), "minus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarMinusPosition(progressChangedCricket);
                cricket_seekbar.setProgress(pos);
                progressChangedCricket = pos;
                cricketSeekbarText
                        .setText(setSeekbarText(progressChangedCricket));
                profileEditor.putString("cricket_skill_level",
                        setSeekbarText(progressChangedCricket));
                profileEditor.commit();
            }
        });

        bikingPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // Toast.makeText(getApplicationContext(), "plus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarPlusPosition(progressChangedBiking);
                biking_seekbar.setProgress(pos);
                progressChangedBiking = pos;
                bikingSeekbarText
                        .setText(setSeekbarText(progressChangedBiking));
                profileEditor.putString("biking_skill_level",
                        setSeekbarText(progressChangedBiking));
                profileEditor.commit();
            }
        });

        bikingMinus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // Toast.makeText(getApplicationContext(), "minus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarMinusPosition(progressChangedBiking);
                biking_seekbar.setProgress(pos);
                progressChangedBiking = pos;
                bikingSeekbarText
                        .setText(setSeekbarText(progressChangedBiking));
                profileEditor.putString("biking_skill_level",
                        setSeekbarText(progressChangedBiking));
                profileEditor.commit();
            }
        });

        rugby_minus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // Toast.makeText(getApplicationContext(), "minus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarMinusPosition(progressChangedRugby);
                rugbySeekbar.setProgress(pos);
                progressChangedRugby = pos;
                rugbySeekBarText.setText(setSeekbarText(progressChangedRugby));
                profileEditor.putString("rugby_skill_level",
                        setSeekbarText(progressChangedRugby));
                profileEditor.commit();
            }
        });

        rugby_plus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // Toast.makeText(getApplicationContext(), "minus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarPlusPosition(progressChangedRugby);
                rugbySeekbar.setProgress(pos);
                progressChangedRugby = pos;
                rugbySeekBarText.setText(setSeekbarText(progressChangedRugby));
                profileEditor.putString("rugby_skill_level",
                        setSeekbarText(progressChangedRugby));
                profileEditor.commit();
            }
        });

        frisbee_minus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // Toast.makeText(getApplicationContext(), "minus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarMinusPosition(progressChangedFrisbee);
                frisbeeSeekBar.setProgress(pos);
                progressChangedFrisbee = pos;
                frisbeeseekbarText
                        .setText(setSeekbarText(progressChangedFrisbee));
                profileEditor.putString("ultimate_skill_level",
                        setSeekbarText(progressChangedFrisbee));
                profileEditor.commit();
            }
        });

        frisbee_plus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // Toast.makeText(getApplicationContext(), "minus",
                // Toast.LENGTH_LONG).show();
                int pos = setSeekbarPlusPosition(progressChangedFrisbee);
                frisbeeSeekBar.setProgress(pos);
                progressChangedFrisbee = pos;
                frisbeeseekbarText
                        .setText(setSeekbarText(progressChangedFrisbee));
                profileEditor.putString("rugby_skill_level",
                        setSeekbarText(progressChangedFrisbee));
                profileEditor.commit();
            }
        });
    }

    // set text value according to seekbar position
    public String setSeekbarText(int num) {
        String text;

        if (isBetween(num, 1, 25)) {
            text = "Recreational Player";
        } else if (isBetween(num, 26, 50)) {
            text = "High School Athlete";
        } else if (isBetween(num, 51, 75)) {
            text = "Collegiate Athlete";
        } else if (isBetween(num, 76, 100)) {
            text = "Professional Athlete";
        } else {
            text = "No Experience";
        }
        return text;
    }

    // set seekbar position for plus button click
    public int setSeekbarPlusPosition(int num) {
        int num1 = 0;
        if (isBetween(num, 1, 25)) {
            num1 = 41;
        } else if (isBetween(num, 26, 50)) {
            num1 = 71;
        } else if (isBetween(num, 51, 75)) {
            num1 = 99;
        } else if (isBetween(num, 76, 100)) {
            num1 = 100;
        } else {
            num1 = 20;
        }

        return num1;
    }

    // set seekbar position for minus button click
    public int setSeekbarMinusPosition(int num) {
        int num1 = 0;

        if (isBetween(num, 1, 25)) {
            num1 = 0;
        } else if (isBetween(num, 26, 50)) {
            num1 = 10;
        } else if (isBetween(num, 51, 75)) {
            num1 = 41;
        } else if (isBetween(num, 76, 100)) {
            num1 = 61;
        } else {

            num1 = 0;
        }

        return num1;
    }

    // set Relativelayout and view visibility
    public void updateView(String sport) {

        if (sport.contentEquals("Basketball")) {

            sportBasketball.setVisibility(View.GONE);
            viewBasketball.setVisibility(View.GONE);
        } else if (sport.contentEquals("Baseball")) {

            sportBaseball.setVisibility(View.GONE);
            viewBaseball.setVisibility(View.GONE);
        } else if (sport.contentEquals("Football")) {

            sportFootball.setVisibility(View.GONE);
            viewFootball.setVisibility(View.GONE);
        } else if (sport.contentEquals("Soccer")) {

            sportSoccer.setVisibility(View.GONE);
            viewSoccer.setVisibility(View.GONE);
        } else if (sport.contentEquals("Tennis")) {

            sportTennis.setVisibility(View.GONE);
            viewTennis.setVisibility(View.GONE);
        } else if (sport.contentEquals("Volleyball")) {

            sportVolleyball.setVisibility(View.GONE);
            viewVolleyball.setVisibility(View.GONE);
        } else if (sport.contentEquals("Golf")) {

            sportGolf.setVisibility(View.GONE);
            viewGolf.setVisibility(View.GONE);
        } else if (sport.contentEquals("Running")) {

            sportRunning.setVisibility(View.GONE);
            viewRunning.setVisibility(View.GONE);
        } else if (sport.contentEquals("Cricket")) {

            sportCricket.setVisibility(View.GONE);
            viewCricket.setVisibility(View.GONE);
        } else if (sport.contentEquals("Biking")) {

            sportBiking.setVisibility(View.GONE);

        }

    }

    // change seekbar postion according to prevoius data
    public void displayRestoredData() {
        // set seekbar position

        primary_seekbar.setProgress(progressChangedPrimary);
        basketball_seekbar.setProgress(progressChangedBasketball);
        baseball_seekbar.setProgress(progressChangedBaseball);
        football_seekbar.setProgress(progressChangedFootball);
        soccer_seekbar.setProgress(progressChangedSoccer);
        tennis_seekbar.setProgress(progressChangedTennis);
        volleyball_seekbar.setProgress(progressChangedVolleyball);
        golf_seekbar.setProgress(progressChangedGolf);
        running_seekbar.setProgress(progressChangedRunning);
        cricket_seekbar.setProgress(progressChangedCricket);
        biking_seekbar.setProgress(progressChangedBiking);
        rugbySeekbar.setProgress(progressChangedRugby);
        frisbeeSeekBar.setProgress(progressChangedFrisbee);


        primarySportSeekbarText.setText(setSeekbarText(progressChangedPrimary));
        basketballSeekbarText
                .setText(setSeekbarText(progressChangedBasketball));
        baseballSeekbarText.setText(setSeekbarText(progressChangedBaseball));
        footballSeekbarText.setText(setSeekbarText(progressChangedFootball));
        soccerSeekbarText.setText(setSeekbarText(progressChangedSoccer));
        tennisSeekbarText.setText(setSeekbarText(progressChangedTennis));
        volleyballSeekbarText
                .setText(setSeekbarText(progressChangedVolleyball));
        golfSeekbarText.setText(setSeekbarText(progressChangedGolf));
        runningSeekbarText.setText(setSeekbarText(progressChangedRunning));
        cricketSeekbarText.setText(setSeekbarText(progressChangedCricket));
        bikingSeekbarText.setText(setSeekbarText(progressChangedBiking));
        rugbySeekBarText.setText(setSeekbarText(progressChangedRugby));
        frisbeeseekbarText.setText(setSeekbarText(progressChangedFrisbee));

    }

    public void getProfileData() {

        db.open();
        Cursor cursor = db.getProfileData();
        cursor.moveToFirst();
        if (cursor.getCount() != 0) {
            do {

                name = cursor.getString(cursor.getColumnIndex("name"));

                phone_number = cursor.getString(cursor
                        .getColumnIndex("phone_number"));

                user_email = cursor.getString(cursor
                        .getColumnIndex("user_email"));

                user_aboutme = cursor.getString(cursor
                        .getColumnIndex("user_aboutme"));

            } while (cursor.moveToNext());
        }

    }

    /**
     * Getting user data from SharedPrefaerance
     */
    public void getUserData(){
        // Initializing the Shared preferences
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        // Getting the auth_token from Shared preferences
        auth_token = preferences.getString("auth_token", null);

        // Getting the device info id from Shared preferences
        device_info_id = preferences.getString("device_info_id", null);

        // Getting the login user id from Shared preferences
        loginuser_id = preferences.getString("loginuser_id", null);

        // Getting the user name from Shared preferences
        getusername = preferences.getString("USERNAME", null);

        login_user_id = preferences.getString("loginuser_id", null);
    }

}
