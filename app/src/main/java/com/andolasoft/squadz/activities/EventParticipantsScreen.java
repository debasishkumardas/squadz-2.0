package com.andolasoft.squadz.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.adapters.EventParticipantsAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EventParticipantsScreen extends AppCompatActivity {

    @InjectView(R.id.back_icon_summary)
    ImageView imgv_back;
    @InjectView(R.id.titletxt)
    TextView tv_titletxt;
    @InjectView(R.id.event_participant_view)
    RecyclerView rv_participants_list;
    UserMolel usermodel;
    ArrayList<UserMolel> userModelArray;
    EventParticipantsAdapter participantsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_event_participants_screen);
        ButterKnife.inject(this);

        //Setting the title of the page
        tv_titletxt.setText("Participants");

        //Adding the data into the array
       // getArray();

        //Setting the Adapter
        setAdapter();

        imgv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventParticipantsScreen.this.finish();
            }
        });
    }

    public void getArray(){
        userModelArray = new ArrayList<>();

        for(int i = 0; i < 5; i++){
            usermodel = new UserMolel();
            usermodel.setUserFullName("Name Display");
            userModelArray.add(usermodel);
        }
    }

    /**
     * Function responsible for setting the list
     */
    public void setAdapter(){
        if(AppConstants.userModelArary.size() > 0){
            participantsAdapter = new EventParticipantsAdapter(EventParticipantsScreen.this, AppConstants.userModelArary);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            rv_participants_list.setLayoutManager(mLayoutManager);
            rv_participants_list.setItemAnimator(new DefaultItemAnimator());
            rv_participants_list.setAdapter(participantsAdapter);
            participantsAdapter.notifyDataSetChanged();
        }else{
            Toast.makeText(EventParticipantsScreen.this, "No Participants Found", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
