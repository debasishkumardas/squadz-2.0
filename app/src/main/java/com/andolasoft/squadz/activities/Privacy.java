package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Privacy extends AppCompatActivity {

    @InjectView(R.id.back_icon_privacy_layout)
    RelativeLayout back_icon_privacy_layout;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    SnackBar snackBar;
    @InjectView(R.id.back_icon_summary)
    ImageView btnBack;
    @InjectView(R.id.switch_button_add_info)
    SwitchCompat scViewFullName;
    @InjectView(R.id.profileinfo)
    SwitchCompat scDisplayContactInfo;
    @InjectView(R.id.profilevisibility)
    SwitchCompat scprofileVisibility;
    @InjectView(R.id.user_name_title)
    TextView tv_displayName;
    @InjectView(R.id.contact_info_title)
    TextView tv_displaycontact;
    @InjectView(R.id.profile_visibility)
    TextView tv_displayprofile;
    String switchName = null,switchState = null, auth_token = null, status = null;
    ProgressDialog dialog;
    public static final String TYPE_PRIVACY_SHOW_FULLNAME = "show_fullname";
    public static final String TYPE_PRIVACY_DISPLAY_CONTACT_INFO = "show_contact";
    public static final String TYPE_PRIVACY_PROFILE_VISIBILITY = "show_public";
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editorPrivacy;
    int DisplayingfullnamecheckChanged = 0;
    int DisplayingContactcheckChanged = 0;
    int DisplayingprofilecheckChanged = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_privacy);

        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Privacy.this.finish();
            }
        });


        scViewFullName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DisplayingfullnamecheckChanged = DisplayingfullnamecheckChanged + 1;
                //Assigning the TYPE
                switchName = TYPE_PRIVACY_SHOW_FULLNAME;

                //Assigning the check status
                if (isChecked) {
                    switchState = "on";
                    tv_displayName.setText("Your username will be displayed");
                } else {
                    switchState = "off";
                    tv_displayName.setText("Your username will be hidden");
                }

                if(DisplayingfullnamecheckChanged > 1){
                    //Calling the server Side API to update the user privacy
                    ChangeUserPrivacySettings(switchName,switchState);
                }
            }
        });

        scDisplayContactInfo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DisplayingContactcheckChanged = DisplayingContactcheckChanged + 1;
                //Assigning the TYPE
                switchName = TYPE_PRIVACY_DISPLAY_CONTACT_INFO;

                //Assigning the check status
                if (isChecked) {
                    switchState = "on";
                    tv_displaycontact.setText("Your contact info will be displayed");
                } else {
                    switchState = "off";
                    tv_displaycontact.setText("Your contact info will be hidden");
                }

                if(DisplayingContactcheckChanged > 1){
                    //Calling the server Side API to update the user privacy
                    ChangeUserPrivacySettings(switchName,switchState);
                }
            }
        });

        scprofileVisibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DisplayingprofilecheckChanged = DisplayingprofilecheckChanged + 1;
                //Assigning the TYPE
                switchName = TYPE_PRIVACY_PROFILE_VISIBILITY;

                //Assigning the check status
                if (isChecked) {
                    switchState = "on";
                    tv_displayprofile.setText("Everyone will be able to see your profile");
                } else {
                    switchState = "off";
                    tv_displayprofile.setText("Your profile will be private");
                }

                if(DisplayingprofilecheckChanged > 1){
                    //Calling the server Side API to update the user privacy
                    ChangeUserPrivacySettings(switchName,switchState);
                }
            }
        });

        //Refreshing the view with user privacy data
        setUserPrivacydata();

    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {

        ButterKnife.inject(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        snackBar = new SnackBar(this);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(Privacy.this);
        auth_token = preferences.getString("auth_token", "");
        editor = preferences.edit();


        sharedpreferences = getSharedPreferences("Privacy_Data", Context.MODE_PRIVATE);
        editorPrivacy = getSharedPreferences("Privacy_Data", MODE_PRIVATE).edit();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Privacy.this.finish();
    }


    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(Privacy.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }

    /**
     * Calling the API to getting the sports
     */

    public void ChangeUserPrivacySettings(final String privacyType,
                                          final String privacyStatus){
        //Displaying loader
        LoaderUtility.handleLoader(Privacy.this, true);
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBody(privacyType,privacyStatus);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                if(!ApplicationUtility.isConnected(Privacy.this)){
                    ApplicationUtility.displayNoInternetMessage(Privacy.this);
                }else{
                    ApplicationUtility.displayErrorMessage(Privacy.this, e.getMessage().toString());
                }


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        JSONObject settingData = responseObject.getJSONObject("settings");
                        boolean privacy_show_full_name = settingData.getBoolean("show_fullname");
                        boolean privacy_show_contacts = settingData.getBoolean("show_contact");
                        boolean privacy_show_public = settingData.getBoolean("show_public");
                        //Refreshing the data in Shared preferance
                        storeDatainSharedPreferance(privacy_show_full_name,
                                privacy_show_contacts,privacy_show_public);

                        //Displaying the messages
                        displayMessage(message);
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }
                LoaderUtility.handleLoader(Privacy.this, false);
            }
        });
    }



    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String privacyType, String privacyStatus){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"users/"+auth_token+"/privacy_setting?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_PRIVACY_TYPE, privacyType);
        urlBuilder.addQueryParameter(Constants.PARAM_PRIVACY_STATUS, privacyStatus);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Dispaly ing the message
     * @param message status message
     */
    public void displayMessage(final String message){
        Privacy.this.runOnUiThread(new Runnable() {
            public void run() {
                //Displaying the success message after successful sign up
                //Toast.makeText(ForgotPasswordScreen.this,message, Toast.LENGTH_LONG).show();
                // snackbar.setSnackBarMessage(message);
                Toast.makeText(Privacy.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Displaying uder privacy data
     */

    public void setUserPrivacydata(){
        SharedPreferences prefs = getSharedPreferences("Privacy_Data", MODE_PRIVATE);
        boolean statusShowFullname = prefs.getBoolean("Privacy_show_fullname", false);
        boolean statusShowontactInfi = prefs.getBoolean("Privacy_show_contact_info", false);
        boolean statusShowProfile = prefs.getBoolean("Privacy_show_Profile", false);
        scViewFullName.setChecked(statusShowFullname);
        scDisplayContactInfo.setChecked(statusShowontactInfi);
        scprofileVisibility.setChecked(statusShowProfile);

        if(!statusShowFullname){
            DisplayingfullnamecheckChanged = DisplayingfullnamecheckChanged + 1;
            tv_displayName.setText("Your username will be hidden");
        }

        if(!statusShowontactInfi){
            DisplayingContactcheckChanged = DisplayingContactcheckChanged + 1;
            tv_displaycontact.setText("Your contact info will be hidden");
        }

        if(!statusShowProfile){
            DisplayingprofilecheckChanged =  DisplayingContactcheckChanged + 1;
            tv_displayprofile.setText("Your profile will be private");
        }

    }

    //Function implemented to handle the APi call count
    public void setApiCallStatus(int call_status){
        call_status = call_status + 1;
    }


    /**
     * Refreshing User privacy data in shared preferance
     * @param privacy_show_fullname
     * @param show_contact_info
     * @param show_profile
     */
    public void storeDatainSharedPreferance(boolean privacy_show_fullname,
                                            boolean show_contact_info,
                                            boolean show_profile){
        SharedPreferences sharedpreferences = getSharedPreferences("Privacy_Data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorPrivacy = getSharedPreferences("Privacy_Data", MODE_PRIVATE).edit();
        editorPrivacy.putBoolean("Privacy_show_fullname", privacy_show_fullname);
        editorPrivacy.putBoolean("Privacy_show_contact_info", show_contact_info);
        editorPrivacy.putBoolean("Privacy_show_Profile", show_profile);
        editorPrivacy.commit();
    }

}

