    package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.ReviewDetailModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.NestedListView;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.FaqAdapter;
import com.andolasoft.squadz.views.adapters.ReviewDetailAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL;
import static com.andolasoft.squadz.R.id.filter_apply_btn;
import static com.andolasoft.squadz.R.id.venue_recycler_view;


public class ReviewDetail extends AppCompatActivity {

    @InjectView(R.id.review_detail_back_icon_layout)
    RelativeLayout review_detail_back_icon_layout;
//    @InjectView(R.id.review_listView)
//    ListView review_listView;
    @InjectView(R.id.review_recycler_view)
    RecyclerView review_recycler_view;
    @InjectView(R.id.review_detail_filter_icon_layout)
    RelativeLayout review_detail_filter_icon_layout;
    @InjectView(R.id.review_court_detail_name)
    TextView review_court_detail_name;
    @InjectView(R.id.review_court_detail_sub_name)
    TextView review_court_detail_sub_name;
    @InjectView(R.id.review_court_detail_review_venue_name)
    TextView review_court_detail_review_venue_name;
    @InjectView(R.id.rating_review_details)
    RatingBar rating_review_details;
    @InjectView(R.id.rating_event_details_reviews)
    TextView rating_event_details_reviews;
    @InjectView(R.id.review_detail_image)
    ImageView review_detail_image;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    SnackBar snackBar;
    ProgressDialog dialog;
    ReviewDetailModel reviewDetailModel;
    ArrayList<ReviewDetailModel> reviewDetailModelArrayList;
    ReviewDetailAdapter reviewDetailAdapter;
    com.melnykov.fab.FloatingActionButton floatingActionButton;
    int counter = 0;
    String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_review_detail);

        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        /**
         * Adding click events on the views
         */
        addClickEvents();

        /**Set court data*/
        setCourtData();

    }

    /**
     * Adding click events on the views
     */
    public void addClickEvents() {
        review_detail_back_icon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /**Filter icon redirection*/
        review_detail_filter_icon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReviewDetail.this,ReviewFilter.class));
            }
        });
        /**Floating action button redirection*/
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReviewDetail.this,AddReview.class));
            }
        });


    }

    /**
     * API integration for display all reviews
     */
    public void getReviewsAPI(String list_id, String sort_by) {
        //Handling the loader state
        LoaderUtility.handleLoader(ReviewDetail.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForReviews(list_id,sort_by);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(ReviewDetail.this, false); 
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String user_id = "",user_name = "", user_profile_image = "",
                        rating = "",review = "",created_at = "",review_rating = "",
                        review_count = "";
                String responseData = response.body().string();
                try {
                        reviewDetailModelArrayList = new ArrayList<>();
                        JSONObject reviewsJsonObject = new JSONObject(responseData);
                        if(reviewsJsonObject.has("review_rating") && !reviewsJsonObject.isNull("review_rating")) {
                            review_rating = reviewsJsonObject.getString("review_rating");
                        }
                        if(reviewsJsonObject.has("review_count") && !reviewsJsonObject.isNull("review_count")) {
                            review_count = reviewsJsonObject.getString("review_count");
                        }

                        if(reviewsJsonObject.has("reviews") &&
                                !reviewsJsonObject.isNull("reviews")) {
                            JSONArray reviews_jsJsonArray = reviewsJsonObject.getJSONArray("reviews");
                            for (int j = 0; j < reviews_jsJsonArray.length(); j++) {
                                //Keeping the Review Count
                                Constants.REVIEW_LIST_COUNT = reviews_jsJsonArray.length();
                                JSONObject reviewsObject = reviews_jsJsonArray.getJSONObject(j);
                                reviewDetailModel = new ReviewDetailModel();

                                if(reviewsObject.has("user_id") && !reviewsObject.isNull("user_id")) {
                                    user_id = reviewsObject.getString("user_id");
                                }

                                if(reviewsObject.has("user_name") && !reviewsObject.isNull("user_name")) {
                                    user_name = reviewsObject.getString("user_name");
                                }
                                if(reviewsObject.has("user_profile_image") && !reviewsObject.isNull("user_profile_image")) {
                                    user_profile_image = reviewsObject.getString("user_profile_image");
                                }
                                if(reviewsObject.has("rating") && !reviewsObject.isNull("rating")) {
                                    rating = reviewsObject.getString("rating");
                                }
                                if(reviewsObject.has("review") && !reviewsObject.isNull("review")) {
                                    review = reviewsObject.getString("review");
                                }
                                if(reviewsObject.has("created_at") && !reviewsObject.isNull("created_at")) {
                                    created_at = reviewsObject.getString("created_at");
                                }

                                if(userId.equals(user_id)) {
                                    if(VenueAdapter.user_review_arrayList == null){
                                        VenueAdapter.user_review_arrayList = new ArrayList<String>();
                                    }
                                    VenueAdapter.user_review_arrayList.add(rating + "@@@@" + review);
                                }

                                reviewDetailModel.setReviewed_by_creator_id(user_id);
                                reviewDetailModel.setReviewed_by_creator_name(user_name);
                                reviewDetailModel.setReviewed_by_creator_image(user_profile_image);
                                reviewDetailModel.setReviewed_by_ratings(rating);
                                reviewDetailModel.setReviewed_by_date(created_at);
                                reviewDetailModel.setReviewed_by_creator_description(review);

                                reviewDetailModelArrayList.add(reviewDetailModel);

                                user_profile_image = null;
                            }
                        }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalReview_rating = review_rating;
                final String finalReview_count = review_count;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(ReviewDetail.this, false); 
                        if(reviewDetailModelArrayList.size() > 0) {
                            rating_event_details_reviews.setText(finalReview_count);
                            rating_review_details.setRating(Float.valueOf(finalReview_rating));
                            Constants.TOTAL_REVIEW_COUNT = finalReview_count;
                            Constants.TOTAL_REVIEW_AVERAGE = finalReview_rating;

                            reviewDetailAdapter = new ReviewDetailAdapter(ReviewDetail.this,reviewDetailModelArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            review_recycler_view.setLayoutManager(mLayoutManager);
                            review_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            review_recycler_view.setAdapter(reviewDetailAdapter);
                            reviewDetailAdapter.notifyDataSetChanged();

                            review_detail_filter_icon_layout.setClickable(true);
                            review_detail_filter_icon_layout.setEnabled(true);

                        } else{
                            reviewDetailAdapter = new ReviewDetailAdapter(ReviewDetail.this,reviewDetailModelArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            review_recycler_view.setLayoutManager(mLayoutManager);
                            review_recycler_view.setItemAnimator(new DefaultItemAnimator());
                            review_recycler_view.setAdapter(reviewDetailAdapter);
                            reviewDetailAdapter.notifyDataSetChanged();

                            snackBar.setSnackBarMessage("No reviews available");

                            review_detail_filter_icon_layout.setClickable(false);
                            review_detail_filter_icon_layout.setEnabled(false);
                        }

                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForReviews(String list_id, String sort_by) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_reviews").newBuilder();

        if(AppConstants.OBJECT_TYPE_GAME != null
                && (AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))){
            urlBuilder.addQueryParameter("event_id", list_id);
            urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_GAME);
        }else {
            urlBuilder.addQueryParameter("list_id", list_id);
            urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_LIST);
        }
       // urlBuilder.addQueryParameter("list_id", list_id);
        urlBuilder.addQueryParameter("sort_by", sort_by);
//        urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_GAME);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(ReviewDetail.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {

        ButterKnife.inject(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        snackBar = new SnackBar(this);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(ReviewDetail.this);
        editor = preferences.edit();
        userId = preferences.getString("loginuser_id", null);
        floatingActionButton = (com.melnykov.fab.FloatingActionButton) findViewById(R.id.fab2);
    }

    /**
     * Set court data
     */
    public void setCourtData() {
        String court_Name = VenueAdapter.venueDetailModelArrayList.get(0).getName();
        String court_review_rating = VenueAdapter.venueDetailModelArrayList.get(0).getReview_rating();
        String court_review_rating_count = VenueAdapter.venueDetailModelArrayList.get(0).getReview_count();
        String court_review_venue_name = VenueAdapter.venueDetailModelArrayList.get(0).getAddress();
        ArrayList<String> image_array = VenueAdapter.venueDetailModelArrayList.get(0).getImages_array();

        review_court_detail_name.setText(court_Name);
        review_court_detail_sub_name.setText(court_Name);
        review_court_detail_review_venue_name.setText(court_review_venue_name);
        rating_event_details_reviews.setText(court_review_rating_count);
        rating_review_details.setRating(Float.valueOf(court_review_rating));

        if(image_array.size() > 0) {
            Glide.with(ReviewDetail.this)
                    .load(image_array.get(0))
                    .placeholder(R.mipmap.loading_placeholder_icon)
                    .into(review_detail_image);
        }

        review_recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if (dy > 0 ||dy<0 && floatingActionButton.isShown())
                {
                    floatingActionButton.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                {
                    floatingActionButton.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        //If review Added, Then calling the API
        if(Constants.REVIEW_ADDED) {
            Constants.REVIEW_ADDED_EVENT = true;
            Constants.REVIEW_ADDED = false;
            getReviewsAPI(VenueAdapter.venueId, Constants.REVIEW_FILTER_TYPE);
        } else if(counter == 0) {
            if(AppConstants.OBJECT_TYPE_GAME != null
                    && (AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")
                    || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))){
                //Setting the event id as venue id
                VenueAdapter.venueId = VenueAdapter.venueDetailModelArrayList.get(0).getId();
            }
            counter = counter + 1;
            getReviewsAPI(VenueAdapter.venueId, Constants.REVIEW_FILTER_TYPE);
        } else if(Constants.REVIEW_FILTER_BTN_CLICKED) {
            Constants.REVIEW_FILTER_BTN_CLICKED = false;
            getReviewsAPI(VenueAdapter.venueId, Constants.REVIEW_FILTER_TYPE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

