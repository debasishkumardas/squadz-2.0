package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.managers.JSONParser;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.NavigationDrawerAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Summary extends AppCompatActivity {

    public static Summary summary;
    ArrayList<String> time_slots_array;
    HashMap<String, String> hash_time_slot;
    SnackBar snackBar;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String userName = "", image = "", billing_card_count = "", user_Id = "",
            auth_token = "",user_id = "";
    private RelativeLayout back_icon_summary_layout;
    private TextView game_name_display_summary, sports_name_summary,
            no_of_players_count_summary, venue_name_summary,
            venue_price_summary, total_amount_summary,
            game_visisbility_status_summary, skill_level_name_summary,
            notes_summary;
    private Button book_btn_summary_info;
    private View view_descrption;
    private ProgressDialog dialog;
    private String status = "", message = "";
    private TextView summary_date_text, summary_time;
    MaterialDialog mMaterialDialog;
    SharedPreferences.Editor profileEditor;
    String is_Refundable_Status = "true",date = "", venueId = "";
    DatabaseAdapter db;
    public static String eventSport, eventDate, eventstartTIme, eventEndTime;
    String startTIme, endtime, city = "",address = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_summary);


        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        /**
         * Click events on the views
         */
        addClickEvents();

        /**
         * Set data coming from Venue listing page
         */
        setData();

    }

    /**
     * Click events on the views
     */
    public void addClickEvents() {
        back_icon_summary_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Constants.AVAILABILITY_TIME_SLOT_ARRAY.clear();
                finish();
            }
        });

        book_btn_summary_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Calling the Create Game API
                createGameAPICall();
            }
        });
    }

    //Getting Time slot by removing "-" symbol
    public String[] splitTimeslots(String time_slot) {
        String[] time_slots = new String[0];

        if (time_slot.contains("-")) {
            time_slots = time_slot.split(" - ");
        }

        return time_slots;
    }

    /**
     * Set data coming from Additional info page
     */
    public void setData() {
        game_name_display_summary.setText(Constants.GAME_NAME);
        sports_name_summary.setText(Constants.SPORTS_NAME);
        no_of_players_count_summary.setText(Constants.PLAYERS_COUNT);
        venue_name_summary.setText(Constants.VENUE_NAME);

        game_visisbility_status_summary.setText(Constants.GAME_VISIBILITY);
        skill_level_name_summary.setText(Constants.SKILL_LEVEL);

        if (!TextUtils.isEmpty(Constants.NOTES_INFO)) {
            notes_summary.setText(Constants.NOTES_INFO);
        } else {
            view_descrption.setVisibility(View.GONE);
            notes_summary.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(Constants.COURT_PRICE) &&
                !Constants.COURT_PRICE.equalsIgnoreCase("FREE")) {
            venue_price_summary.setText("$ " + Constants.COURT_PRICE);
            if(Constants.AVAILABILITY_TIME_SLOT_ARRAY.size() > 0) {
                double total_price = Double.valueOf(Constants.COURT_PRICE) * Constants.AVAILABILITY_TIME_SLOT_ARRAY.size();
                total_amount_summary.setText("$ " + ApplicationUtility.twoDigitAfterDecimal(total_price));
            }
            else{
                total_amount_summary.setText("$ " + Constants.COURT_PRICE);
            }
        } else {
            /*venue_price_summary.setText("FREE");
            total_amount_summary.setText("FREE");*/
            venue_price_summary.setText("Not Applicable");
            total_amount_summary.setText("Not Applicable");
        }

        //summary_date_text.setText(Constants.AVAILABILITY_SELECTED_DATE);
        summary_date_text.setText(ApplicationUtility.formattedDateContainSpace(Constants.AVAILABILITY_SELECTED_DATE));

        //Displaying selected Time slots
        displayTimeSlots(Constants.AVAILABILITY_TIME_SLOT_ARRAY);

        /*String time_slots = Constants.AVAILABILITY_TIME_SLOT;

        String start_time = splitTimeslots(time_slots)[0];
        String end_time = splitTimeslots(time_slots)[1];

        summary_time.setText(start_time +" - " + end_time);*/

    }

    /**
     * Displaying selected Time slots
     */
    public void displayTimeSlots(ArrayList<String> time_slots_Array) {
        StringBuilder builder = new StringBuilder();
        for (String details : time_slots_Array) {
            builder.append(details + "\n");
        }
        summary_time.setText(builder.toString());
    }

    /**
     * Game create API call
     */
    public void gameCreate(final String user_id, final String list_id, String sport_id, String sport_name, String name,
                           String note, String no_of_player, String skill_level, final String date,
                           JSONArray time_slots, String visibility, String userName, String image) {
        //Handling the loader state
        LoaderUtility.handleLoader(Summary.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, list_id, sport_id, sport_name, name, note,
                no_of_player, skill_level, date, time_slots, visibility, userName, image);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(Summary.this, false);

                if(!ApplicationUtility.isConnected(Summary.this)){
                    ApplicationUtility.displayNoInternetMessage(Summary.this);
                }else{
                    ApplicationUtility.displayErrorMessage(Summary.this, "Something went wrong");
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    if (responseObject.has("status") && !responseObject.isNull("status")) {
                        status = responseObject.getString("status");
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            AppConstants.GAME_ID = responseObject.getString("game_id");
                        }else{
                            message = responseObject.getString("message");
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                Summary.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(Summary.this, false);
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                Constants.isEventCreated = true;

                                /*//Time sloe array value set to zero after successfully created a game
                                Constants.AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<String>();*/
                                Toast.makeText(Summary.this, "Event created successfully", Toast.LENGTH_SHORT).show();

                                /**Keeping Game info inside local database for future review*/
                                storeGameInfoForRating();

                                Intent intent = new Intent(Summary.this, FinishEventCreation.class);
                                startActivity(intent);
                            } else {
                                snackBar.setSnackBarMessage(message);
                            }
                        } else {
                            snackBar.setSnackBarMessage("Network error! please try after sometime");
                        }
                    }
                });
            }
        });
    }


    /**
     *  */
    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     *//*
     * @return
     */
    public Request buildApiRequestBody(String user_id, String list_id, String sport_id, String sport_name, String name,
                                       String note, String no_of_player, String skill_level, String date,
                                       JSONArray time_slots, String visibility, String userName, String image) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "save_game_info").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("list_id", list_id);
        urlBuilder.addQueryParameter("sport_id", sport_id);
        urlBuilder.addQueryParameter("sport_name", sport_name);
        urlBuilder.addQueryParameter("name", name);
        urlBuilder.addQueryParameter("note", note);
        urlBuilder.addQueryParameter("no_of_player", no_of_player);
        urlBuilder.addQueryParameter("skill_level", skill_level);
        urlBuilder.addQueryParameter("date", date);
        urlBuilder.addQueryParameter("time_slots", time_slots.toString());
        urlBuilder.addQueryParameter("visibility", visibility);
        urlBuilder.addQueryParameter("device_type", "android");
        urlBuilder.addQueryParameter("user_name", userName);
        urlBuilder.addQueryParameter("user_profile_image", image);
        urlBuilder.addQueryParameter("game_created_at", ApplicationUtility.getCurrentDateTime());
        urlBuilder.addQueryParameter("is_refundable", is_Refundable_Status);//true/false

        String url = urlBuilder.build().toString();
        url = url.replace(" ", "");

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(Summary.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {
        back_icon_summary_layout = (RelativeLayout) findViewById(R.id.back_icon_summary_layout);
        game_name_display_summary = (TextView) findViewById(R.id.game_name_display_summary);
        sports_name_summary = (TextView) findViewById(R.id.sports_name_summary);
        no_of_players_count_summary = (TextView) findViewById(R.id.no_of_players_count_summary);
        venue_name_summary = (TextView) findViewById(R.id.venue_name_summary);
        venue_price_summary = (TextView) findViewById(R.id.venue_price_summary);
        total_amount_summary = (TextView) findViewById(R.id.total_amount_summary);
        game_visisbility_status_summary = (TextView) findViewById(R.id.game_visisbility_status_summary);
        skill_level_name_summary = (TextView) findViewById(R.id.skill_level_name_summary);
        notes_summary = (TextView) findViewById(R.id.notes_summary);
        book_btn_summary_info = (Button) findViewById(R.id.book_btn_summary_info);
        view_descrption = (View) findViewById(R.id.view9);
        summary_date_text = (TextView) findViewById(R.id.summary_date_text);
        summary_time = (TextView) findViewById(R.id.summary_time);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        db = new DatabaseAdapter(this);
        snackBar = new SnackBar(this);
        summary = this;
        preferences = PreferenceManager
                .getDefaultSharedPreferences(Summary.this);
        editor = preferences.edit();

        //Setting the book button text
        if(Constants.isUnRegisteredvenueGameCreated){
            book_btn_summary_info.setText("Create");
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Constants.AVAILABILITY_TIME_SLOT_ARRAY.clear();
        finish();
    }

    /**
     * Function responsible for displaying the unregistered venue data
     */
    public void setUnRegisteredVenueData(){
        game_name_display_summary.setText(Constants.GAME_NAME);
        sports_name_summary.setText(Constants.SPORTS_NAME);
        no_of_players_count_summary.setText(Constants.PLAYERS_COUNT);
        venue_name_summary.setText(Constants.VENUE_NAME);

        game_visisbility_status_summary.setText(Constants.GAME_VISIBILITY);
        skill_level_name_summary.setText(Constants.SKILL_LEVEL);

        if (!TextUtils.isEmpty(Constants.NOTES_INFO)) {
            notes_summary.setText(Constants.NOTES_INFO);
        } else {
            view_descrption.setVisibility(View.GONE);
            notes_summary.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(Constants.COURT_PRICE) &&
                !Constants.COURT_PRICE.equalsIgnoreCase("FREE")) {
            venue_price_summary.setText("$ " + Constants.COURT_PRICE);
            total_amount_summary.setText("$ " + Constants.COURT_PRICE);
        } else {
            venue_price_summary.setText("FREE");
            total_amount_summary.setText("FREE");
        }

        summary_date_text.setText(Constants.AVAILABILITY_SELECTED_DATE);

        //Displaying selected Time slots
        displayTimeSlots(Constants.AVAILABILITY_TIME_SLOT_ARRAY);
    }


    /**
     * Game create API call
     */
    public void gameCreateWithUnRegisteredCourts(String user_id, String list_id, String sport_id, String sport_name, String name,
                           String note, String no_of_player, String skill_level, String date,
                           JSONArray time_slots, String visibility, String userName, String image, String loation, String city,
                                                 String address, String courtName, String game_type) {
        //Handling the loader state
        LoaderUtility.handleLoader(Summary.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForUnregisteredCourts(user_id, list_id, sport_id, sport_name, name, note,
                no_of_player, skill_level, date, time_slots, visibility, userName, image,loation, city, address, courtName, game_type);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(Summary.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    if (responseObject.has("status") && !responseObject.isNull("status")) {
                        status = responseObject.getString("status");
                        AppConstants.GAME_ID = responseObject.getString("game_id");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                Summary.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(Summary.this, false);
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                Constants.isEventCreated = true;
                                Constants.isUnRegisteredvenueGameCreated = false;
                                //Time sloe array value set to zero after successfully created a game
                                Constants.AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<String>();
                                //snackBar.setSnackBarMessage("Event created successfully");
                                Toast.makeText(Summary.this, "Event created successfully", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(Summary.this, FinishEventCreation.class);
                                startActivity(intent);
                            } else {
                                snackBar.setSnackBarMessage("Unable to create this event");
                            }
                        } else {
                            snackBar.setSnackBarMessage("Network error! please try after sometime");
                        }
                    }
                });
            }
        });
    }



    /**
     *  */
    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     *//*
     * @return
     */
    public Request buildApiRequestBodyForUnregisteredCourts(String user_id, String list_id, String sport_id, String sport_name, String name,
                                       String note, String no_of_player, String skill_level, String date,
                                       JSONArray time_slots, String visibility, String userName, String image, String location,
                                                            String city, String address, String courtName, String gameType) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "save_unregistred_game_info").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("sport_id", sport_id);
        urlBuilder.addQueryParameter("sport_name", sport_name);
        urlBuilder.addQueryParameter("court_name", courtName);
        urlBuilder.addQueryParameter("name", name);
        urlBuilder.addQueryParameter("note", note);
        urlBuilder.addQueryParameter("no_of_player", no_of_player);
        urlBuilder.addQueryParameter("skill_level", skill_level);
        urlBuilder.addQueryParameter("date", date);
        urlBuilder.addQueryParameter("time_slots", time_slots.toString());
        urlBuilder.addQueryParameter("visibility", visibility);
        urlBuilder.addQueryParameter("device_type", "android");
        urlBuilder.addQueryParameter("user_name", userName);
        urlBuilder.addQueryParameter("user_profile_image", image);
        urlBuilder.addQueryParameter("location", location);
        urlBuilder.addQueryParameter("city", city);
        urlBuilder.addQueryParameter("address", address);
        urlBuilder.addQueryParameter("game_type", gameType);
        urlBuilder.addQueryParameter("is_registered_event ", Boolean.FALSE.toString());
        urlBuilder.addQueryParameter("game_created_at", ApplicationUtility.getCurrentDateTime());
        String url = urlBuilder.build().toString();
        url = url.replace(" ", "");

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }


    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayDialog(){
        mMaterialDialog = new MaterialDialog(Summary.this);
        View view = LayoutInflater.from(Summary.this)
                .inflate(R.layout.lay_no_payment_card_found,
                        null);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        TextView tv_add_payment_card = (TextView) view.findViewById(R.id.tv_add_card);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_add_payment_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                startActivity(new Intent(Summary.this, AddPaymentDetailsScreen.class));
            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }

    /**
     * Function responsible for displaying of the dialog for cancella tion policy
     */
    public void cancellationPolicyDialog(String policy_title, String policy_description,
                                         final String userId, final String venueId, final String sportId, final String sportName, final String gameName,
                                         final String note, final String playersCount, final String skillLevel, final String date,
                                         final JSONArray json_array, final String visibility, final String userName, final String image,
                                         final boolean status){
        mMaterialDialog = new MaterialDialog(Summary.this);
        View view = LayoutInflater.from(Summary.this)
                .inflate(R.layout.layout_dialog_cancellation_policy,
                        null);

        TextView tv_cancellation_policy_title = (TextView) view.findViewById(R.id.tv_cancellation_policy_title);
        TextView tv_cancellation_policy = (TextView) view.findViewById(R.id.tv_cancellation_policy);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel_policy);

        //Hiding the cancel button
        if(status){
            tv_cancel.setVisibility(View.GONE);
        }

        TextView tv_confirm = (TextView) view.findViewById(R.id.tv_confirm_policy);

        tv_cancellation_policy_title.setText(policy_title);
        tv_cancellation_policy.setText(policy_description);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                is_Refundable_Status = "false";
                gameCreate(userId, venueId, sportId, sportName, gameName,
                        note, playersCount, skillLevel, date,//"12/10/2017"
                        json_array, visibility, userName, image);

            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //API call to create a game
        if(AppConstants.isPaymentCardAdded){
            AppConstants.isPaymentCardAdded = false;
            getCountAPI(user_Id, auth_token);
        }
    }


    /**
     * Fetching Radius,wishlist & Card count
     */
    /**
     * API integration for all counts
     */
    public void getCountAPI(String user_id, String auth_token) {
        //Handling the loader state
        //handleLoader(true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(user_id,auth_token);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                //handleLoader(false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String radius = "0",cards = "0",wish_lists_count = "0";
                try {
                    JSONObject radius_jsonObject = new JSONObject(responseData);
                    JSONObject user_json_object = radius_jsonObject.getJSONObject("user_info");
                    if(user_json_object.has("radius") && !user_json_object.isNull("radius")) {
                        radius = user_json_object.getString("radius");
                    }
                    if(user_json_object.has("cards") && !user_json_object.isNull("cards")) {
                        cards = user_json_object.getString("cards");
                    }
                    if(user_json_object.has("wish_lists_count") && !user_json_object.isNull("wish_lists_count")) {
                        wish_lists_count = user_json_object.getString("wish_lists_count");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalRadius = radius;
                final String finalCards = cards;
                final String finalWish_lists_count = wish_lists_count;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Summary.this, "Payment card successfully added", Toast.LENGTH_SHORT).show();
                        //Store counts for Radius, Billings (Card listing) & Wishlist
                        setCountDatainPreferences(finalRadius, finalCards, finalWish_lists_count);
                        //createGameAPICall();
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String user_id, String auth_token) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_profile_info").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("user_id", user_id);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Store counts for Radius, Billings (Card listing) & Wishlist
     */
    public void setCountDatainPreferences(String radius_count, String billing_count,
                                          String wishlist_count) {
        if(radius_count.equalsIgnoreCase("0")) {
            this.editor.putString("RADIUS_COUNT","10");
        } else{
            this.editor.putString("RADIUS_COUNT",radius_count);
        }
        this.editor.putString("BILLING_COUNT",billing_count);
        this.editor.putString("WISHLIST_COUNT",wishlist_count);
        this.editor.apply();
    }


    /**
     * Function Responsible for calling the create game API
     */
    public void createGameAPICall(){
        String userId = VenueAdapter.userId;
        venueId = VenueAdapter.venueId;
        String sportId = Constants.SPORTS_ID;
        String courtName = Constants.VENUE_NAME;
        String sportName = Constants.SPORTS_NAME;
        String gameName = Constants.GAME_NAME;
        String note = Constants.NOTES_INFO;
        String playersCount = Constants.PLAYERS_COUNT;
        String skillLevel = Constants.SKILL_LEVEL;
        String visibility = Constants.GAME_VISIBILITY;
        date = ApplicationUtility.
                customFormattedDate(Constants.AVAILABILITY_SELECTED_DATE);
        String location = Constants.GAME_LOCATION;

        if(!TextUtils.isEmpty(VenueListing.currentCity))
        {
            city = VenueListing.currentCity;
        }
        else {
            city = CreateEventWithoutCourt.currentCity;
        }

        if(!TextUtils.isEmpty(VenueListing.game_address))
        {
            address = VenueListing.game_address;
        }
        else {
            address = CreateEventWithoutCourt.game_address;
        }

        String game_type = Constants.GAME_TYPE;

        if (preferences.contains("USERNAME")) {
            userName = preferences.getString("USERNAME", "");
            image = preferences.getString("image", "");
            billing_card_count = preferences.getString("BILLING_COUNT", "");
            auth_token = preferences.getString("auth_token", "");
            user_id = preferences.getString("loginuser_id","");

            if (TextUtils.isEmpty(image)) {
                image = " ";
            }
        }

        JSONArray json_array = new JSONArray();
        ArrayList<String> timeslots_array =  Constants.AVAILABILITY_TIME_SLOT_ARRAY;
        for (int i = 0; i < timeslots_array.size(); i++) {
            String[] split = timeslots_array.get(i).split(" - ");
            int arraySize = timeslots_array.size();
            int endTimePoint = arraySize - 1;

            if(i == 0){
                startTIme = split[0];
            }

            if(i == endTimePoint){
                endtime = split[1];
            }

            JSONObject obj = new JSONObject();
            try {
                obj.put("start_time", split[0]);
                obj.put("end_time", split[1]);
                json_array.put(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Storing the values as static values
        storeSharingValues(sportName, date,
                ApplicationUtility.getTimeWithFormat(startTIme),
                ApplicationUtility.getTimeWithFormat(endtime));

        //Converting the String value to integer
        int billing_cards_count = Integer.valueOf(billing_card_count);
        user_Id = preferences.getString("loginuser_id", "");

        if(!Constants.isUnRegisteredvenueGameCreated){

            if(billing_cards_count > 0){

                if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE_POINT)) {
                    long upcoming_time_milisecond_policy = getUpcomingTimeInMiliseonds(Integer.parseInt(Constants.CANCELLATION_POLICY_TYPE_POINT));
                    long time_slots_milisecond_policy = getTimeSlotsDateTimeinMilisecond();

                    if(time_slots_milisecond_policy < upcoming_time_milisecond_policy) {
                        String policyTitle = "Cancellation Policy\n ( "+Constants.CANCELLATION_POLICY_TYPE + " )";
                        String policyDescription = "Your payment is non-refundable due to short notice and you will be charged immediately.";

                        cancellationPolicyDialog(policyTitle,policyDescription,
                                userId, venueId, sportId, sportName, gameName,
                                note, playersCount, skillLevel, date,
                                json_array, visibility, userName, image, false);
                    } else{
                        //String policyTitle = "Cancellation Policy\n ( "+Constants.CANCELLATION_POLICY_TYPE + " )";
                        String policyTitle = "Payment info";
                        //String policyDescription = "Participants will only be charged at the corresponding cancellation point. The cost may be split up until cancellation point.";
                        String policyDescription = "You will only get charged "
                                                    + ApplicationUtility.getHour_Days(Constants.CANCELLATION_POLICY_TYPE_POINT)
                                                    + " before the event.  The cost will be split with whoever joins until then.";

                        cancellationPolicyDialog(policyTitle,policyDescription,
                                userId, venueId, sportId, sportName, gameName,
                                note, playersCount, skillLevel, date,
                                json_array, visibility, userName, image, true);
                    }
                }
                else{
                    gameCreate(userId, venueId, sportId, sportName, gameName,
                            note, playersCount, skillLevel, date,
                            json_array, visibility, userName, image);
                }

            }else{
                //Displaying dialog if user does not have a payment card
                displayDialog();
            }

        }else{
            gameCreateWithUnRegisteredCourts(user_Id, venueId, sportId, sportName, gameName,
                    note, playersCount, skillLevel, date,
                    json_array, visibility, userName, image, location, city, address,courtName, game_type);
        }
    }

    /**
     * Getting next day's 24 time from the current date n time
     */
    public long getUpcomingTimeInMiliseonds(int hour) {
        long time_milisecond = 0;
        SimpleDateFormat inputFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        String currentDateandTime = sdf.format(new Date());
        Date current_time = null;
        try {
            current_time = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();
        c.setTime(current_time);
        //c.add(Calendar.DATE, 1);
        c.add(Calendar.HOUR, hour);

        String current = sdf.format(c.getTime());

        Date current_timee = null;
        try {
            current_timee = sdf.parse(current);
            time_milisecond = current_timee.getTime();
            System.out.print(""+time_milisecond);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time_milisecond;
    }

    /**
     *
     */
    public long getTimeSlotsDateTimeinMilisecond() {
        long time_milisecond = 0;
        String date = ApplicationUtility.customFormattedDate(Constants.AVAILABILITY_SELECTED_DATE);
        ArrayList<String> timeslots_array =  Constants.AVAILABILITY_TIME_SLOT_ARRAY;

        String [] time_split = timeslots_array.get(0).split(" - ");
        String timeSlots = date+" "+time_split[0];

        SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mma");

        try {
            Date date1 = inputFormatter.parse(timeSlots);
            time_milisecond = date1.getTime();

            System.out.print(""+time_milisecond);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time_milisecond;
    }

    /**
     * Store List info in databse for future ratings
     */
    public void storeGameInfoForRating() {
        String end_time = "";
        ArrayList<String> time_slots_array = Constants.AVAILABILITY_TIME_SLOT_ARRAY;

        if(time_slots_array.size() > 0) {
            for (int i = 0; i < time_slots_array.size(); i++) {
                String [] time_slots_split = time_slots_array.get(i).split(" - ");
                String start_time = time_slots_split[0];
                end_time = time_slots_split[1];
            }
            /**Keeping Game info inside local database for future review*/
            db.insertGameDetailsForRating(Constants.GAME_NAME,venueId,"","list",user_id, date, String.valueOf(ApplicationUtility.getGameDateTimeInMilisecond(date+ " "+ end_time)));
            //Time sloe array value set to zero after successfully created a game
            Constants.AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<String>();
        }
    }

    /**
     * Function responsible for storing the facebook sharing values
     */
    public static void storeSharingValues(String sport, String date,
                                          String starttime, String endtime){

        if(date.contains(" ")) {
            //eventDate = ApplicationUtility.formattedDateContainSpace(eventDate);
            date = ApplicationUtility.formattedDateContainSpace(date);
        } else{
            //eventDate = ApplicationUtility.formattedDateContainSpace(eventDate);
            date = ApplicationUtility.formattedDate(date);
        }
        eventSport = sport;
        eventDate = date;
        eventstartTIme = starttime;
        eventEndTime = endtime;
    }

    /**
     * Clearing all the values
     */
    public static void storeSharingValues(){
        eventSport = null;
        eventDate = null;
        eventstartTIme = null;
        eventEndTime = null;
    }
}
