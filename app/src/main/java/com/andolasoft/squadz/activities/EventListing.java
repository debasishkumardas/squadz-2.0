package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.managers.LocationPickerManager;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EventListing extends AppCompatActivity {

    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    public static ArrayList<EventListingModel> eventListingModelArrayList;
    public static EventListing eventListing;
    EventListingModel eventListingModel;
    JSONArray event_listing_array = null;
    EventAdapter adapter;
    RecyclerView event_recycler_view;
    EventAdapter venueAdapter;
    SnackBar snackBar;
    ArrayList<String> currLocItems;
    @InjectView(R.id.event_location_icon_venue)
    ImageView imgv_eventMap;
    @InjectView(R.id.search_edit_field)
    EditText et_search_edit_field;
    @InjectView(R.id.venue_toolbar)
    Toolbar seatchBar;
    @InjectView(R.id.event_search_icon)
    ImageView searchIcon;
    @InjectView(R.id.search_layout)
    RelativeLayout search_layout;
    @InjectView(R.id.back_image_search)
    ImageView imgv_search;
    @InjectView(R.id.event_filter_icon)
    ImageView iv_event_filter;
    @InjectView(R.id.lay_no_event_found)
    RelativeLayout layNoEventFound;
    boolean isSoftKeyboardDisplay = false;
    String userId = null;
    SharedPreferences pref;
    @InjectView(R.id.event_recycler_view)
    RecyclerView rvEventListing;
    @InjectView(R.id.swipe_refresh_layout_event)
    SwipeRefreshLayout swipe_refresh_layout_event;
    private RelativeLayout back_icon_event_listing_layout;
    private ProgressDialog dialog;
    private Spinner spinner_event_list;
    RelativeLayout no_events_filter_button_layout;
    Button modify_event_btn,reset_event_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_event_listing);
        ButterKnife.inject(this);

        layNoEventFound.setVisibility(View.GONE);

        pref = PreferenceManager.getDefaultSharedPreferences(EventListing.this);
        userId = pref.getString("loginuser_id", null);

        eventListing = this;

        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        /**
         * Added onclick events on the views
         */
        setOnClickEvents();

        //Displying the selected location
        setVenueAppletonType();

        /**
         * Function to get Event listing from server
         */
        getEventListing();

    }

    /**
     * Added onclick events on the views
     */
    public void setOnClickEvents() {
        back_icon_event_listing_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.setAllValuesToNull();
                Constants.isUnRegisteredvenueGameCreated = false;
                finish();
            }
        });

        spinner_event_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    LocationPickerManager locationPickerManager
                            = new LocationPickerManager(EventListing.this);
                }
                return true;
            }
        });

        imgv_eventMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EventListing.this, EventMapview.class);
                startActivity(intent);
            }
        });

        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seatchBar.setVisibility(View.INVISIBLE);
                search_layout.setVisibility(View.VISIBLE);
                et_search_edit_field.requestFocus();
                et_search_edit_field.setText("");

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        });

        et_search_edit_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                venueAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    event_recycler_view.setLayoutManager(mLayoutManager);
                    event_recycler_view.setItemAnimator(new DefaultItemAnimator());
                    event_recycler_view.setAdapter(venueAdapter);
                    venueAdapter.notifyDataSetChanged();
                }
            }
        });

        imgv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View view2 = getCurrentFocus();
                if (view2 != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                seatchBar.setVisibility(View.VISIBLE);
                search_layout.setVisibility(View.INVISIBLE);

                //Setting Adapter to Recycler view
                setDataToRecyclerView();
            }
        });

        /**
         * CHECKING IF SOFT KEYBOARD IS UP OR DOWN
         */
        et_search_edit_field.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (keyboardShown(et_search_edit_field.getRootView())) {
                    isSoftKeyboardDisplay = true;
                } else {

                }
            }
        });

        /**
         * Click event on Filter icon
         */
        iv_event_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_filter = new Intent(EventListing.this, EventFilter.class);
                startActivity(intent_filter);
            }
        });

        //Pull to refresh functionality
        swipe_refresh_layout_event.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getEventListing();
                swipe_refresh_layout_event.setRefreshing(false);
            }
        });
    }

    private boolean keyboardShown(View rootView) {

        final int softKeyboardHeight = 100;
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
        int heightDiff = rootView.getBottom() - r.bottom;
        return heightDiff > softKeyboardHeight * dm.density;
    }

    /**
     * Setting Adapter to Recycler view
     */
    public void setDataToRecyclerView() {
        //Passing venue info to Recycler view adapter
        venueAdapter = new EventAdapter(EventListing.this, eventListingModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        event_recycler_view.setLayoutManager(mLayoutManager);
        event_recycler_view.setItemAnimator(new DefaultItemAnimator());
        event_recycler_view.setAdapter(venueAdapter);
        venueAdapter.notifyDataSetChanged();
    }

    /**
     * add values to Recycler view
     */
    public void setAdapterData() {
        eventListingModelArrayList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {

            eventListingModel = new EventListingModel();
            eventListingModel.setEvent_Name("Basketball court in Snap...");
            eventListingModelArrayList.add(eventListingModel);

            adapter = new EventAdapter(EventListing.this, eventListingModelArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            event_recycler_view.setLayoutManager(mLayoutManager);
            event_recycler_view.setItemAnimator(new DefaultItemAnimator());
            event_recycler_view.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }
    }


    /**
     * Function to get Event listing from server
     */
    public void getEventListing() {
        //Handling the loader state
        LoaderUtility.handleLoader(EventListing.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(EventListing.this, false);
                if(!ApplicationUtility.isConnected(EventListing.this)){
                    ApplicationUtility.displayNoInternetMessage(EventListing.this);
                }else{
                    ApplicationUtility.displayErrorMessage(EventListing.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", image = "", name = "", latitude = "", longitude = "",
                        review_rating = "", review_count = "", price = "", court_type = "", skill_level = "",
                        eventDate = "", no_of_participants = "", total_number_players = "", startTime = "", endTime = "",
                        creator_username = "", creator_user_id = "", creator_profile_image = "",
                        actual_price = "", minimum_price = "", eventVisibility = "", event_sport = "",
                        description = "",venue_id = "",object_type = "",list_id = "",venue_name = "";
                boolean isfavorite = false;
                ArrayList<String> participantsArray = null;

                try {
                    eventListingModelArrayList = new ArrayList<>();
                    event_listing_array = new JSONArray(responseData);

                        for (int i = 0; i < event_listing_array.length(); i++) {
                            eventListingModel = new EventListingModel();

                        JSONObject responseObject = event_listing_array.getJSONObject(i);

                        if (responseObject.has("id") && !responseObject.isNull("id")) {
                            id = responseObject.getString("id");
                        }
                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            image = responseObject.getString("image");
                        }
                        if (responseObject.has("name") && !responseObject.isNull("name")) {
                            name = responseObject.getString("name");
                        }
                        if (responseObject.has("latitude") && !responseObject.isNull("latitude")) {
                            latitude = responseObject.getString("latitude");
                        }
                        if (responseObject.has("longitude") && !responseObject.isNull("longitude")) {
                            longitude = responseObject.getString("longitude");
                        }
                        if (responseObject.has("review_rating") && !responseObject.isNull("review_rating")) {
                            review_rating = responseObject.getString("review_rating");
                        }
                        if (responseObject.has("review_count") && !responseObject.isNull("review_count")) {
                            review_count = responseObject.getString("review_count");
                        }

                        if (responseObject.has("no_of_player") && !responseObject.isNull("no_of_player")) {
                            total_number_players = responseObject.getString("no_of_player");
                        }

                        if (responseObject.has("no_of_partitipants") && !responseObject.isNull("no_of_partitipants")) {
                            no_of_participants = responseObject.getString("no_of_partitipants");
                        }
                        if (responseObject.has("object_type") && !responseObject.isNull("object_type")) {
                            object_type = responseObject.getString("object_type");
                        }
                        if (responseObject.has("venue_name") && !responseObject.isNull("venue_name")) {
                            venue_name = responseObject.getString("venue_name");
                        }


                        if (responseObject.has("price") && !responseObject.isNull("price")) {
                            price = responseObject.getString("price");
                            try {
                                int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                                float price_pre_player = Float.parseFloat(price) / (float) totalPlayer;
                                actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                                float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                                minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                ;
                            }
                        }

                        if (responseObject.has("skill_level") && !responseObject.isNull("skill_level")) {
                            skill_level = responseObject.getString("skill_level");
                        }

                        if (responseObject.has("date") && !responseObject.isNull("date")) {
                            eventDate = responseObject.getString("date");
                        }


                        if (responseObject.has("user_name") && !responseObject.isNull("user_name")) {
                            creator_username = responseObject.getString("user_name");
                        }

                        if (responseObject.has("user_id") && !responseObject.isNull("user_id")) {
                            creator_user_id = responseObject.getString("user_id");
                        }

                        if (responseObject.has("user_profile_image") && !responseObject.isNull("user_profile_image")) {
                            creator_profile_image = responseObject.getString("user_profile_image");
                        }

                        if (responseObject.has("user_profile_image") && !responseObject.isNull("user_profile_image")) {
                            creator_profile_image = responseObject.getString("user_profile_image");
                        }

                        if (responseObject.has("is_favorite") && !responseObject.isNull("is_favorite")) {
                            isfavorite = responseObject.getBoolean("is_favorite");
                        }
                            if (responseObject.has("list_id") && !responseObject.isNull("list_id")) {
                                list_id = responseObject.getString("list_id");
                            }


                        if (responseObject.has("sport_name") && !responseObject.isNull("sport_name")) {
                            event_sport = responseObject.getString("sport_name");
                        }
                            /**Keeping venue ID for NON-Register games*/
                            if (responseObject.has("venue_id") && !responseObject.isNull("venue_id")) {
                                venue_id = responseObject.getString("venue_id");
                            }


                        /*if (responseObject.has("time_slots") && !responseObject.isNull("time_slots")) {
                            ArrayList<String> images_arraylist = new ArrayList<String>();
                            JSONArray images_json_array = responseObject.getJSONArray("time_slots");
                            for (int j = 0; j < images_json_array.length(); j++) {
                                JSONArray getValues = images_json_array.getJSONArray(j);
                                for (int x = 0; x < getValues.length(); x++) {
                                    if (j == 0) {
                                        startTime = getValues.getString(1);
                                    } else {
                                        endTime = getValues.getString(1);
                                    }
                                }
                            }
                            eventListingModel.setImages_array(images_arraylist);
                        }*/


                        if(responseObject.has("time_slots") && !responseObject.isNull("time_slots")) {
                            String start_time = null;
                            JSONArray time_slot_array_json_array = responseObject.getJSONArray("time_slots");
                            for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                                JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                                if(j == 0){
                                    startTime = getValues.getString("start_time");
                                }
                                endTime = getValues.getString("end_time");
                            }
                        }

                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            ArrayList<String> images_arraylist = new ArrayList<String>();
                            JSONArray images_json_array = responseObject.getJSONArray("image");
                            for (int j = 0; j < images_json_array.length(); j++) {
                                images_arraylist.add(images_json_array.getString(j));
                            }
                            eventListingModel.setImages_array(images_arraylist);
                        }


                        if (responseObject.has("visibility") && !responseObject.isNull("visibility")) {
                            eventVisibility = responseObject.getString("visibility");
                        }
                        if (responseObject.has("description") && !responseObject.isNull("description")) {
                            description = responseObject.getString("description");
                        }
                        if (responseObject.has("partitipant_ids") && !responseObject.isNull("partitipant_ids")) {
                            participantsArray = new ArrayList<String>();
                            JSONArray images_json_array = responseObject.getJSONArray("partitipant_ids");
                            for (int j = 0; j < images_json_array.length(); j++) {
                                participantsArray.add(images_json_array.getString(j));
                            }
                            //eventListingModel.setImages_array(participantsArray);
                        }

                        if(eventVisibility != null &&
                                eventVisibility.equalsIgnoreCase("Private") &&
                                participantsArray.contains(userId)){
                            //Setting the data in model
                            eventListingModel.setEvent_Id(id);
                            eventListingModel.setVenue_id(venue_id);
                            eventListingModel.setEvent_Image(image);
                            eventListingModel.setEvent_Name(name);
                            eventListingModel.setLatitude(latitude);
                            eventListingModel.setLongitude(longitude);
                            eventListingModel.setEvent_Ratings(review_rating);
                            eventListingModel.setEvent_Reviews(review_count);
                            eventListingModel.setEvent_Price(actual_price);
                            eventListingModel.setSkill_level(skill_level);
                            eventListingModel.setEventDate(eventDate);
                            eventListingModel.setEvent_Total_Participants(no_of_participants);
                            eventListingModel.setEventStartTime(startTime);
                            eventListingModel.setEventEndTime(endTime);
                            eventListingModel.setCapacity(total_number_players);
                            eventListingModel.setCreator_name(creator_username);
                            eventListingModel.setCreator_user_id(creator_user_id);
                            eventListingModel.setEvent_Creator_Image(creator_profile_image);
                            eventListingModel.setFavorite(isfavorite);
                            eventListingModel.setEvent_Minimum_Price(minimum_price);
                            eventListingModel.setEvent_Sports_Type(event_sport);
                            eventListingModel.setDescription(description);
                            eventListingModel.setObject_Type(object_type);
                            eventListingModel.setEvent_Total_Price(price);
                            eventListingModel.setList_id(list_id);
                            eventListingModel.setVenue_name(venue_name);
                            eventListingModelArrayList.add(eventListingModel);
                        }else if(eventVisibility != null &&
                                eventVisibility.equalsIgnoreCase("Public")){
                            //Setting the data in model
                            eventListingModel.setEvent_Id(id);
                            eventListingModel.setVenue_id(venue_id);
                            eventListingModel.setEvent_Image(image);
                            eventListingModel.setEvent_Name(name);
                            eventListingModel.setLatitude(latitude);
                            eventListingModel.setLongitude(longitude);
                            eventListingModel.setEvent_Ratings(review_rating);
                            eventListingModel.setEvent_Reviews(review_count);
                            eventListingModel.setEvent_Price(actual_price);
                            eventListingModel.setSkill_level(skill_level);
                            eventListingModel.setEventDate(eventDate);
                            eventListingModel.setEvent_Total_Participants(no_of_participants);
                            eventListingModel.setEventStartTime(startTime);
                            eventListingModel.setEventEndTime(endTime);
                            eventListingModel.setCapacity(total_number_players);
                            eventListingModel.setCreator_name(creator_username);
                            eventListingModel.setCreator_user_id(creator_user_id);
                            eventListingModel.setEvent_Creator_Image(creator_profile_image);
                            eventListingModel.setFavorite(isfavorite);
                            eventListingModel.setEvent_Minimum_Price(minimum_price);
                            eventListingModel.setEvent_Sports_Type(event_sport);
                            eventListingModel.setDescription(description);
                            eventListingModel.setObject_Type(object_type);
                            eventListingModel.setEvent_Total_Price(price);
                            eventListingModel.setList_id(list_id);
                            eventListingModel.setVenue_name(venue_name);
                            eventListingModelArrayList.add(eventListingModel);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                EventListing.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(EventListing.this, false);

                        if (eventListingModelArrayList != null &&
                                eventListingModelArrayList.size() != 0) {
                            if (event_listing_array != null &&
                                    event_listing_array.length() > 0) {
                                rvEventListing.setVisibility(View.VISIBLE);
                                layNoEventFound.setVisibility(View.GONE);
                                //Enabling the views
                                enableViews(true);
                                venueAdapter = new EventAdapter(EventListing.this, eventListingModelArrayList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                event_recycler_view.setLayoutManager(mLayoutManager);
                                event_recycler_view.setItemAnimator(new DefaultItemAnimator());
                                event_recycler_view.setAdapter(venueAdapter);
                                venueAdapter.notifyDataSetChanged();

                                no_events_filter_button_layout.setVisibility(View.GONE);
                            } else {
                                eventListingModelArrayList = new ArrayList<EventListingModel>();
                                venueAdapter = new EventAdapter(EventListing.this, eventListingModelArrayList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                event_recycler_view.setLayoutManager(mLayoutManager);
                                event_recycler_view.setItemAnimator(new DefaultItemAnimator());
                                event_recycler_view.setAdapter(venueAdapter);
                                venueAdapter.notifyDataSetChanged();

                                if(Constants.EVENT_FILTER_BTN_CLICK) {
                                    iv_event_filter.setFocusable(true);
                                    iv_event_filter.setClickable(true);
                                    imgv_eventMap.setFocusable(false);
                                    imgv_eventMap.setClickable(false);
                                    searchIcon.setFocusable(false);
                                    searchIcon.setClickable(false);

                                    /**Display message with button if no results found in filter*/
                                    displayMessageButton();
                                } else{
                                    //Enabling the views
                                    enableViews(false);
                                    //layNoEventFound.setVisibility(View.VISIBLE);
                                }
                                /**Hiding the Recycler view*/
                                rvEventListing.setVisibility(View.GONE);
                            }
                        } else {
                            if(Constants.EVENT_FILTER_BTN_CLICK) {
                                iv_event_filter.setFocusable(true);
                                iv_event_filter.setClickable(true);
                                imgv_eventMap.setFocusable(false);
                                imgv_eventMap.setClickable(false);
                                searchIcon.setFocusable(false);
                                searchIcon.setClickable(false);

                                /**Display message with button if no results found in filter*/
                                displayMessageButton();
                            } else{
                                //Enabling the views
                                enableViews(false);
                                //layNoEventFound.setVisibility(View.VISIBLE);
                            }
                            /**Hiding the Recycler view*/
                            rvEventListing.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
    }

    /**
     * Function to handle the clicks on the toolbarbuttons
     *
     * @param status
     */
    public void enableViews(boolean status) {
        if (status) {
            iv_event_filter.setFocusable(true);
            iv_event_filter.setClickable(true);
            imgv_eventMap.setFocusable(true);
            imgv_eventMap.setClickable(true);
            searchIcon.setFocusable(true);
            searchIcon.setClickable(true);
        } else {
            iv_event_filter.setFocusable(false);
            iv_event_filter.setClickable(false);
            imgv_eventMap.setFocusable(false);
            imgv_eventMap.setClickable(false);
            searchIcon.setFocusable(false);
            searchIcon.setClickable(false);

        }
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_venue_games").newBuilder();
        urlBuilder.addQueryParameter("location", String.valueOf(HomeFragment.currLocLatitude+","+ HomeFragment.currLocLongitude));
        urlBuilder.addQueryParameter("sportid", Constants.SPORTS_ID);
        urlBuilder.addQueryParameter("sport_name", Constants.SPORTS_NAME);
        urlBuilder.addQueryParameter("current_city", HomeFragment.current_city);
        urlBuilder.addQueryParameter("current_time", ApplicationUtility.getDeviceCurrentTime());
        urlBuilder.addQueryParameter("date", ApplicationUtility.getDeviceCurrentDate());
        urlBuilder.addQueryParameter("user_id", userId);

        if(Constants.EVENT_FILTER_BTN_CLICK) {
            urlBuilder.addQueryParameter("filter", Boolean.TRUE.toString());

            if(!TextUtils.isEmpty(Constants.EVENT_FILTER_PRICE)) {
                urlBuilder.addQueryParameter("price", Constants.EVENT_FILTER_PRICE);
            }
            if(!TextUtils.isEmpty(Constants.NO_OF_PLAYER)) {
                urlBuilder.addQueryParameter("no_of_player", Constants.NO_OF_PLAYER);
            }
            if(!TextUtils.isEmpty(Constants.EVENT_FILTER_SELECT_DATE)) {
                String date = ApplicationUtility.customFormattedDateForEventFilter(Constants.EVENT_FILTER_SELECT_DATE);
                urlBuilder.addQueryParameter("filter_date", ApplicationUtility.customFormattedDateForEventFilter(Constants.EVENT_FILTER_SELECT_DATE));//date - dd/mm/yyyy
            }
            if(!TextUtils.isEmpty(Constants.EVENT_FILTER_MAP_RADIUS_MILES)) {
                urlBuilder.addQueryParameter("miles", Constants.EVENT_FILTER_MAP_RADIUS_MILES);
            }
        }


        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(EventListing.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Display message with button if no results found in filter
     */
    public void displayMessageButton()
    {
        no_events_filter_button_layout.setVisibility(View.VISIBLE);

        /**Modify button click*/
        modify_event_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_Filter = new Intent(EventListing.this, EventFilter.class);
                startActivity(intent_Filter);
            }
        });

        /**Reset button click*/
        reset_event_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.setAllValuesToNull();
                getEventListing();
            }
        });
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {
        event_recycler_view = (RecyclerView) findViewById(R.id.event_recycler_view);
        back_icon_event_listing_layout = (RelativeLayout) findViewById(R.id.back_icon_event_listing_layout);
        spinner_event_list = (Spinner) findViewById(R.id.spinner_event_list);
        no_events_filter_button_layout = (RelativeLayout) findViewById(R.id.no_events_filter_button_layout);
        modify_event_btn = (Button) findViewById(R.id.modify_event_btn);
        reset_event_btn = (Button) findViewById(R.id.reset_event_btn);

        snackBar = new SnackBar(EventListing.this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //Change the Swipe layout color
        swipe_refresh_layout_event.setColorSchemeResources(R.color.orange,
                R.color.orange,
                R.color.orange);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (isSoftKeyboardDisplay) {
            isSoftKeyboardDisplay = false;
            seatchBar.setVisibility(View.VISIBLE);
            search_layout.setVisibility(View.INVISIBLE);
            //Setting Adapter to Recycler view
            setDataToRecyclerView();
        } else {
            //KEEPING ALL VALUES TO NULL
            Constants.setAllValuesToNull();

            // removeDataFromFile();
            finish();
        }
        // finish();
    }


    /**
     * KEEPING VENUE APPLETON NAME INSIDE THE SPINNER
     */
    public void setVenueAppletonType() {

       /* //Getting venue name & id as a array to display inside the spinner
        spinnerArray = new String[venue_spinner_id_array.size()];
        map_venue_name_spiner = new HashMap<String, String>();

        for (int i = 0; i < venue_spinner_id_array.size(); i++) {
            map_venue_name_spiner.put(venue_spinner_name_array.get(i), venue_spinner_id_array.get(i));
            spinnerArray[i] = venue_spinner_name_array.get(i);
        }*/

        currLocItems = new ArrayList<>();
        currLocItems.add(HomeFragment.current_city);
        //Displaying name inside spinner
        displaySpinnerInfo(currLocItems);
    }

    /**
     * Displaying name inside spinner
     */
    public void displaySpinnerInfo(ArrayList<String> spinnerArray) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(EventListing.this, android.R.layout.simple_spinner_item, spinnerArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_event_list.setAdapter(dataAdapter);
        spinner_event_list.setSelection(0);
    }


    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Google", "Place Selected: " + place.getName());
                HomeFragment.address = place.getAddress().toString();
                String latLng = place.getLatLng().toString();
                String[] latlngArr = latLng.split(" ");
                String latlngVal = latlngArr[1].substring(1, latlngArr[1].length() - 1);
                String[] finalArray = latlngVal.split(",");
                HomeFragment.currLocLatitude = Double.parseDouble(finalArray[0].toString());
                HomeFragment.currLocLongitude = Double.parseDouble(finalArray[1].toString());
                setVenueAppletonType();
                LocationPickerManager.getCurrentCity(EventListing.this);
                Constants.EVENT_FILTER_BTN_CLICK = false;
                getEventListing();
                // Display attributions if required.
                CharSequence attributions = place.getAttributions();
                if (!TextUtils.isEmpty(attributions)) {
                    //mPlaceAttribution.setText(Html.fromHtml(attributions.toString()));
                } else {
                    //mPlaceAttribution.setText("");
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("Google", "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }
    }

    /**
     * Current Time
     * @return
     */
    public String currentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mmaa");
        String currentTime = df.format(c.getTime()).toLowerCase();

        return currentTime;
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationPickerManager.getCurrentCity(EventListing.this);
        setVenueAppletonType();

        if(Constants.EVENT_FILTER_BTN_CLICK)
        {
            getEventListing();
        }
        if(Constants.LEAVED_STATUS || Constants.CANCEL_STATUS)
        {
            Constants.LEAVED_STATUS = false;
            Constants.CANCEL_STATUS = false;
            getEventListing();
        }
    }
}
