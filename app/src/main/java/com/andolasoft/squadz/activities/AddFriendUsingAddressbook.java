package com.andolasoft.squadz.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.ContactBean;
import com.andolasoft.squadz.models.SquadzFriends;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.JSONParser;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.MyContactAdapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddFriendUsingAddressbook extends AppCompatActivity {

    @InjectView(R.id.tv_availability)
    TextView tv_header;
    @InjectView(R.id.back_icon_finish_event)
    ImageView iv_back;
    Button addressbook_button, facebook_button;
    @InjectView(R.id.friends_listt)
    ListView listView;
    View rootView;
    private List<ContactBean> list, Facebook_friend_list,
            Facebook_friend_list_matched;
    List<SquadzFriends> onlySquadzfriends_List;
    View mCustomView;
    ImageView warning_btn;
    SharedPreferences preferences;
    String getusername, usernumber, str, auth_token, own_number,
            user_number_countrycode = null;
    ArrayList<String> getnumber, countrycode;
    String country_code;
    StringBuilder sb, usernumber_countrycode;
    boolean is_connected;
    boolean userown_phonenumber;
    Dialog warning_dialog;
    TextView all_friends;
    String phoneNumber;
    RelativeLayout addressbook_layout, facebook_layout;
    ListView facebook_fiendlist, friemdsList;
    ImageView done_image, reset_image;
    ArrayList<String> currrent_friends_Id, current_friends_Name;
    ArrayList<String> friendId;
    ArrayList<String> friendName;
    ArrayList<String> friendphoto;
    ArrayList<Bitmap> photo;
    DatabaseAdapter db;
    boolean is_available;
    String FILENAME = "AndroidSSO_data", userid;
    ArrayList<String> fullname;
    ArrayList<String> user_image;
    private static final int REQUEST_CODE_PICK_CONTACTS = 1;
    private static final int REQUEST_CODE_PICK_FRIENDS = 32665;
    private Handler mHandler = new Handler();
    String user_id = null, allfrddataforiteration, allfrddataforiteration1;
    String User_EMAIL, intent_extra, intent_extra_parameter;
    ImageView bact_button;
    TextView warningtext;
    Cursor phones;
    Handler handler = new Handler();
    ProgressDialog dialog;
    String status, message;
    JSONArray contacts;
    String name;
    ArrayList<String> playername = new ArrayList<String>();
    ArrayList<String> playerId = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend_using_addressbook);
        ButterKnife.inject(this);

        //Setting the header text
        setHeaderText();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddFriendUsingAddressbook.this.finish();
            }
        });

        list = new ArrayList<>();

        getnumber = new ArrayList<>();

        countrycode = new ArrayList<>();

        sb = new StringBuilder();

        db = new DatabaseAdapter(AddFriendUsingAddressbook.this);

        //Function responsible for syncing the user address book data
        getUserAddressbookData();
    }

    //Setting the Header text
    public void setHeaderText(){
        tv_header.setText("Addressbook");
    }


    /**
     * Function responsible for getting all the contacts from address book
     */
    public void getUserAddressbookData(){
        phones = AddFriendUsingAddressbook.this.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                null, null);
        usernumber_countrycode = new StringBuilder();
        // Getting the phone numbers from the address book
        while (phones.moveToNext()) {
            // Getting the Display name from the phone contact
            String name = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            // Getting the Display number from the phone contact
            phoneNumber = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            // Replacing all the special character from the phone number
            phoneNumber = phoneNumber.replace(" ", "");
            phoneNumber = phoneNumber.replaceAll("[#(;\\\\/:+*?\")<>|&']",
                    "");
            phoneNumber = phoneNumber.replace("-", "");
            // Adding the country code before the phone number
            if (phoneNumber.length() == 10) {
                phoneNumber = GetCountryZipCode() + phoneNumber;
                phoneNumber = phoneNumber.replace(" ", "");
                phoneNumber = phoneNumber.replace("+", "");
                countrycode.add(phoneNumber);
            } else if (phoneNumber.length() > 10) {
                phoneNumber = phoneNumber.replace(" ", "");
                phoneNumber = phoneNumber.replace("-", "");
                countrycode.add(phoneNumber);
            }

            // Initializing the Model class
            ContactBean objContact = new ContactBean();

            // Setting the name into the model class
            objContact.setName(name);

            // Setting the phone number into the model class
            objContact.setPhoneNo(phoneNumber);

            // Adding number into the array list
            getnumber.add(phoneNumber);

            // Adding the arraylist into the model class
            list.add(objContact);
        }

        // Sorting the array list
        Collections.sort(list, new Comparator<ContactBean>() {
            public int compare(ContactBean v1, ContactBean v2) {
                return v1.getName().compareTo(v2.getName());
            }
        });

        // Closing the phone cursor
        phones.close();

        // Manipulating the phone number after appending the country code
        for (String number : getnumber) {
            sb.append(number + ",");
        }
        usernumber = sb.toString();
        if (usernumber != null && usernumber.length() > 1) {
            str = usernumber.substring(0, usernumber.length() - 1);
        }

        for (String number : countrycode) {
            usernumber_countrycode.append(number + ",");
        }
        usernumber = usernumber_countrycode.toString();
        if (usernumber != null && usernumber.length() > 1) {
            user_number_countrycode = usernumber.substring(0,
                    usernumber.length() - 1);
        }

        own_number = getUserPhone();

        if(list.size() == 0){
            showToast1("No Contacts found. Unable to sync your contacts.");
        }else{
            //Calling the address book sync API
            //UserAddressbookSyncTask(own_number,user_number_countrycode);
            new UserSyncTask(own_number,user_number_countrycode).execute();
        }


    }

    // Displaying the Toast message
    private void showToast1(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    // Getting the Country code
    public String GetCountryZipCode() {
        String CountryID = "";
        String CountryZipCode = "";

        TelephonyManager manager = (TelephonyManager) getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        // getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();
        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }




    /**
     * Calling the API to getting the sports
     */

    public void UserAddressbookSyncTask(String own_number,
                                        String friend_number){
        //Displaying loader
        handleLoader(true);
        // should be a singleton
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBody(own_number, friend_number);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        status = responseObject.getString("status");
                        userown_phonenumber = responseObject.getBoolean("own_number");

                        if (status.equalsIgnoreCase("success")) {
                            contacts = responseObject.getJSONArray("all_users");
                            for (int i = 0; i < contacts.length(); i++) {
                                JSONObject c = contacts.getJSONObject(i);
                                name = c.getString("username");
                                String id = c.getString("user_id");
                                String ph_number = c.getString("phone_number");
                                String user_image = c.getString("image");
                                boolean frnd_status = c.getBoolean("friendship_status");
                                for (int ii = 0; ii < list.size(); ii++) {
                                    String country_code_number = "+"
                                            + list.get(ii).getPhoneNo()
                                            .replace(" ", "");
                                    if (list.get(ii).getPhoneNo().replace(" ", "")
                                            .equalsIgnoreCase(ph_number)
                                            || country_code_number
                                            .equalsIgnoreCase(ph_number)) {
                                        list.get(ii).setusername(name);
                                        list.get(ii).setFacebookImageUrl(user_image);
                                        list.get(ii).setuserId(id);
                                        list.get(ii).setfriendship_status(frnd_status);
                                        list.get(ii).setMatch(true);
                                        break;
                                    }
                                }
                                playerId.add(id);
                                playername.add(name);
                            }
                        } else {
                            message = responseObject.getString("message");
                        }
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                AddFriendUsingAddressbook.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            Toast.makeText(AddFriendUsingAddressbook.this, message, Toast.LENGTH_SHORT).show();

                            MyContactAdapter objAdapter = new MyContactAdapter(
                                    AddFriendUsingAddressbook.this, list, AddFriendUsingAddressbook.this);

                            listView.setAdapter(objAdapter);
                            // objAdapter.notifyDataSetChanged();
                            listView.setFastScrollEnabled(true);

                            if (userown_phonenumber) {
                                // warning_btn.setVisibility(View.GONE);
                            } else {
                                // warning_btn.setVisibility(View.VISIBLE);
                            }

                        }else if (status != null && status.equals("error")) {

                            Toast.makeText(getApplicationContext(), message,
                                    Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getApplicationContext(),
                                    AppConstants.API_RESPONSE_ERROR_MESSAGE,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                LoaderUtility.handleLoader(AddFriendUsingAddressbook.this, false);
            }
        });
    }

    /**
     * Dispaly ing the message
     * @param message status message
     */
    public void displayMessage(final String message){
        AddFriendUsingAddressbook.this.runOnUiThread(new Runnable() {
            public void run() {

                //Displaying the success message after successful sign up
                //Toast.makeText(ForgotPasswordScreen.this,message, Toast.LENGTH_LONG).show();
                // snackbar.setSnackBarMessage(message);
                Toast.makeText(AddFriendUsingAddressbook.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(AddFriendUsingAddressbook.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }

    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String own_phn_number, String friends_number){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+getUserAuthToken()+"/sync?").newBuilder();
        urlBuilder.addQueryParameter("own_number", own_phn_number);
        urlBuilder.addQueryParameter("phone_nos", friends_number);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    public String getUserPhone(){
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(AddFriendUsingAddressbook.this);
        String user_phn = preferences.getString("user_phone", null);
        return  user_phn;
    }

    public String getUserAuthToken(){
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(AddFriendUsingAddressbook.this);
        String user_auth = preferences.getString("auth_token", null);
        return user_auth;
    }


    // Calling the Sync API
    public class UserSyncTask extends AsyncTask<Void, Void, Boolean> {
        String message, status;
        String name, ownNumber, friendsNumbers;
        //String syncURL = EndPoints.BASE_URL_VENUE_MIGRATION+"users/" + getUserAuthToken() + "/sync?";
        String syncURL = EndPoints.BASE_URL+"users/" + getUserAuthToken() + "/sync?";

        JSONArray contacts;
        ArrayList<String> playername = new ArrayList<String>();
        ArrayList<String> playerId = new ArrayList<String>();
        ProgressDialog dialog;


        public UserSyncTask(String own_number, String friends_number){
            this.ownNumber = own_number;
            this.friendsNumbers = friends_number;
        }

        @Override
        protected void onPreExecute() {

            if (!TextUtils.isEmpty(own_number)
                    && own_number.equalsIgnoreCase("NA")) {
                own_number = "";
            }
        }

        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(Void... params) {
            JSONParser jsonParser = new JSONParser();
            List<NameValuePair> parameters = new ArrayList<NameValuePair>();
            parameters.add(new BasicNameValuePair("own_number", this.ownNumber
                    .replace(" ", "")));
            parameters.add(new BasicNameValuePair("phone_nos",
                    this.friendsNumbers));

            JSONObject json;
            try {
                json = jsonParser.makeHttpRequest(syncURL, "POST", parameters);
                status = json.getString("status");
                userown_phonenumber = json.getBoolean("own_number");

                if (status.equalsIgnoreCase("success")) {
                    contacts = json.getJSONArray("all_users");
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        name = c.getString("username");
                        String id = c.getString("user_id");
                        String ph_number = c.getString("phone_number");
                        String user_image = c.getString("image");
                        boolean frnd_status = c.getBoolean("friendship_status");
                        for (int ii = 0; ii < list.size(); ii++) {
                            String country_code_number = "+"
                                    + list.get(ii).getPhoneNo()
                                    .replace(" ", "");
                            if (list.get(ii).getPhoneNo().replace(" ", "")
                                    .equalsIgnoreCase(ph_number)
                                    || country_code_number
                                    .equalsIgnoreCase(ph_number)) {
                                list.get(ii).setusername(name);
                                list.get(ii).setFacebookImageUrl(user_image);
                                list.get(ii).setuserId(id);
                                list.get(ii).setfriendship_status(frnd_status);
                                list.get(ii).setMatch(true);
                                break;
                            }
                        }
                        playerId.add(id);
                        playername.add(name);
                    }
                } else {
                    message = json.getString("message");
                }
                if (status != null) {
                    return false;
                } else {
                    return true;
                }
            } catch (JSONException e) {

                e.getMessage();

            } catch (Exception e) {
                e.getMessage();
                return false;
            }
            return null;
        }

        protected void onPostExecute(final Boolean success) {
            if (status != null && status.equalsIgnoreCase("success")) {
                MyContactAdapter objAdapter = new MyContactAdapter(
                        AddFriendUsingAddressbook.this, list, AddFriendUsingAddressbook.this);
                listView.setAdapter(objAdapter);
                listView.setFastScrollEnabled(true);
                if (userown_phonenumber) {
                    // warning_btn.setVisibility(View.GONE);
                } else {
                    // warning_btn.setVisibility(View.VISIBLE);
                }
            } else if (status != null && status.equals("error")) {

                Toast.makeText(getApplicationContext(), message,
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        AppConstants.API_RESPONSE_ERROR_MESSAGE,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

}
