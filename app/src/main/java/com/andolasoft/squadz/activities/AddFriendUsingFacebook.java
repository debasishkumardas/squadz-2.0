package com.andolasoft.squadz.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.ContactBean;
import com.andolasoft.squadz.models.SquadzFriends;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.JSONParser;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.views.adapters.FacebookFriendAdapter;
import com.facebook.*;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AddFriendUsingFacebook extends AppCompatActivity {

    Button addressbook_button, facebook_button;
    @InjectView(R.id.friends_listt)
    ListView friemdsList;
    @InjectView(R.id.tv_availability)
    TextView tv_title;
    @InjectView(R.id.back_icon_finish_event)
    ImageView ivBack;
    @InjectView(R.id.warningtest)
    TextView tv_warningtest;
    View rootView;
    private List<ContactBean> list, Facebook_friend_list,
            Facebook_friend_list_matched;
    List<SquadzFriends> onlySquadzfriends_List;
    View mCustomView;
    ImageView warning_btn;
    SharedPreferences preferences;
    String getusername, usernumber, str, auth_token, own_number,
            user_number_countrycode = null;
    ArrayList<String> getnumber, countrycode;
    String country_code;
    StringBuilder sb, usernumber_countrycode;
    boolean is_connected;
    boolean userown_phonenumber;
    Dialog warning_dialog;
    TextView all_friends;
    String phoneNumber;
    RelativeLayout addressbook_layout, facebook_layout;
    ListView facebook_fiendlist;
    ImageView done_image, reset_image;
    ArrayList<String> currrent_friends_Id, current_friends_Name;
    ArrayList<String> friendId;
    ArrayList<String> friendName;
    ArrayList<String> friendphoto;
    ArrayList<Bitmap> photo;
    DatabaseAdapter db;
    boolean is_available;
    public static final String APP_ID = "301785856688183";
    String FILENAME = "AndroidSSO_data", userid;
    ArrayList<String> fullname;
    ArrayList<String> user_image;
    private static final String[] PERMISSIONS = new String[] {
            "public_profile", "email", "read_stream", "user_friends" };
    private static final int REQUEST_CODE_PICK_CONTACTS = 1;
    private static final int REQUEST_CODE_PICK_FRIENDS = 32665;
    private Handler mHandler = new Handler();
    String user_id = null, allfrddataforiteration, allfrddataforiteration1;
    String User_EMAIL, intent_extra, intent_extra_parameter, fb_user_id;
    ImageView bact_button;
    TextView warningtext;
    Cursor phones;
    Handler handler = new Handler();
    private ProfileTracker profileTracker;
    CallbackManager callbackManager ;
    public static final String FACEBOOK_API_CALL_PERMISSIONs = "public_profile, email, read_stream, user_friends";
    private AccessTokenTracker accessTokenTracker;
    SharedPreferences pref;
    ProgressDialog dialog;

    private final FacebookCallback fbLoginCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            Log.d("Test Data", loginResult.toString());

            //onGetFBFriendsList();

            displayMessage(loginResult);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_add_friend_using_facebook);
        ButterKnife.inject(this);
        tv_title.setText("Facebook Sync");
        tv_warningtest.setVisibility(View.GONE);


        Facebook_friend_list = new ArrayList<ContactBean>();



        friendId = new ArrayList<>();

       // allfrddataforiteration = new StringBuilder();


        callbackManager = CallbackManager.Factory.create();

        //facebookStratTracking();

        //getfriendList();

        //onGetFBFriendsList();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddFriendUsingFacebook.this.finish();
            }
        });

        if(getUserLoginType().equalsIgnoreCase("FACEBOOK_LOGIN")){
            loginUsingFBManager();
        }else{
            Toast.makeText(AddFriendUsingFacebook.this, "Please login with facebook to do the sync", Toast.LENGTH_SHORT).show();
            finish();
        }


    }


    public void loginUsingFBManager() {
        //"user_friends" this will return only the common friends using this app
        LoginManager.getInstance().logInWithReadPermissions(AddFriendUsingFacebook.this,
                Arrays.asList("public_profile", "user_friends", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, fbLoginCallback);
    }

   /* public void getfriendList(){
        //AccesToken token = AccessToken.getCurrentAccessToken();
        GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                try {
                    JSONArray jsonArrayFriends = jsonObject.getJSONObject("friendlist").getJSONArray("data");
                    JSONObject friendlistObject = jsonArrayFriends.getJSONObject(0);
                    String frienListID = friendlistObject.getString("id");
                    //myNewGraphReq(friendListID);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle param = new Bundle();
        param.putString("fields", "friendlist");
        graphRequest.setParameters(param);
        graphRequest.executeAsync();
    }*/

    public void onGetFBFriendsList() {
        AccessToken fbToken = AccessToken.getCurrentAccessToken();

        //fbToken return from login with facebook
        GraphRequestAsyncTask r = GraphRequest.newGraphPathRequest(fbToken,
                "/me/friends", new GraphRequest.Callback() {

                    @Override
                    public void onCompleted(GraphResponse response) {
                       // parseResponse(response.getJSONObject());

                        //Toast.makeText(AddFriendUsingAddressbook.this, response.toString(), Toast.LENGTH_LONG).show();

                        Log.d("Response",response.toString());
                    }
                }
        ).executeAsync();
    }

    public void getMyDetails(){
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,picture");

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "me/friends", parameters, HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        Log.d("Response",response.toString());
                    }
                }
        ).executeAndWait();
    }


    public void getUserFriendList(){
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/{user-id}/friendlists",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            /* handle the result */
                    }
                }
        ).executeAsync();
    }

    /**
     * Function to handle the facebook callbacks
     */
    public void facebookStratTracking(){
        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
                AccessToken accessToken = newToken;
                com.facebook.Profile profile = com.facebook.Profile.getCurrentProfile();
               // displayMessage(profile);
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(com.facebook.Profile oldProfile, Profile newProfile) {
               // displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
    }


    /**
     * Getting data from Facebook
     * @param profile User facebook Profile Object
     */

    private void displayMessage(final LoginResult profile){
        if(profile != null){
            Bundle params = new Bundle();
            params.putString("fields", "id,name,picture");
            new GraphRequest(AccessToken.getCurrentAccessToken(), "me/friends", params,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse response) {
                            if (response != null) {
                                try {
                                    JSONArray data = response.getJSONObject().getJSONArray("data");

                                    for(int i = 0; i < data.length(); i++){
                                        JSONObject user_data = data.getJSONObject(i);
                                        //Getting user_id
                                        String fb_user_id = user_data.getString("id");
                                        friendId.add(fb_user_id);

                                        //Getting Fullname
                                        String fb_user_name = user_data.getString("name");

                                        JSONObject image_obj = user_data.getJSONObject("picture");

                                        JSONObject imageData = image_obj.getJSONObject("data");

                                        String userImage = imageData.getString("url");

                                        ContactBean objContact = new ContactBean();
                                        objContact.setName(fb_user_name);
                                        objContact.setMatch(false);
                                        objContact.setFacebookuserId(fb_user_id);
                                        objContact.setFacebookImageUrl(userImage.replace(
                                                "\\.", ""));
                                        Facebook_friend_list.add(objContact);

                                    }


                                    if (friendId.size() != 0) {
                                        StringBuilder s = new StringBuilder(100);
                                        for (int k = 0; k < friendId.size(); k++) {
                                            s.append(friendId.get(k).toString());
                                            s.append(",");
                                        }
                                        allfrddataforiteration = s.toString();
                                        if (allfrddataforiteration != null
                                                && allfrddataforiteration
                                                .length() > 1) {
                                            allfrddataforiteration1 = allfrddataforiteration
                                                    .substring(
                                                            0,
                                                            allfrddataforiteration
                                                                    .length() - 1);
                                        }

                                    }

                                    displayMyData(true);

                                    //Logging out from the instance
                                   // LoginManager.getInstance().logOut();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).executeAsync();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    /**
     * Getting data from Facebook
     */

    private void displayMyData(boolean status){
        if(status){
            Bundle params = new Bundle();
            params.putString("fields", "id,name,picture");
            new GraphRequest(AccessToken.getCurrentAccessToken(), "me", params,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse response) {
                            if (response != null) {
                                try {
                                    JSONObject data = response.getJSONObject();
                                    //Getting user_id
                                    fb_user_id = data.getString("id");
                                    //Logging out from the instance
                                    LoginManager.getInstance().logOut();
                                    String loggedinUserFacebookId = getUserFacebookId();
                                    if(loggedinUserFacebookId.equalsIgnoreCase(fb_user_id)){
                                        new FacebookInviteFriends(fb_user_id, allfrddataforiteration1).execute();    
                                    }else{
                                        tv_warningtest.setVisibility(View.VISIBLE);
                                        tv_warningtest.setText("It seems, you have the email used on Facebook doesn't match with the Squadz app");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).executeAsync();
        }
    }

    //Getting user Login Type
    public String getUserLoginType(){
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        String login_type = pref.getString("LOGIN_TYPE", "");
        return login_type;
    }

    //Getting user Authroziation token
    public String getUserAuthToken(){
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        auth_token = pref.getString("auth_token", "");
        return auth_token;
    }

    //Getting user Authroziation token
    public String getUserFacebookId(){
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        String facebookId = pref.getString("Facebook_Id", "");
        return facebookId;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(AddFriendUsingFacebook.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }


    // Calling facebook friends API
    private class FacebookInviteFriends extends
            AsyncTask<String, Void, Boolean> {
        String msg, status;
        ArrayList<Integer> position_array;
        String ownId, userIds;
        String syncUrl = null;

        public FacebookInviteFriends(String own_user_id, String friends_user_id){
            this.ownId = own_user_id;
            this.userIds = friends_user_id;
            //syncUrl = EndPoints.BASE_URL_VENUE_MIGRATION+"users/"+getUserAuthToken()+"/sync/facebook?";
            syncUrl = EndPoints.BASE_URL+"users/"+getUserAuthToken()+"/sync/facebook?";
        }

        protected void onPreExecute() {
            LoaderUtility.handleLoader(AddFriendUsingFacebook.this, true);
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            try {
                position_array = new ArrayList<Integer>();

                Facebook_friend_list_matched = new ArrayList<ContactBean>();

                if (status != null && status.equalsIgnoreCase("success")) {

                    for (int j = 0; j < Facebook_friend_list.size(); j++) {
                        boolean matches = Facebook_friend_list.get(j)
                                .getMatch();

                        if (matches) {
                            Facebook_friend_list_matched
                                    .add(Facebook_friend_list.get(j));
                        }
                    }

                    if (Facebook_friend_list_matched.size() != 0) {
                        FacebookFriendAdapter objAdapter = new FacebookFriendAdapter(
                                AddFriendUsingFacebook.this,
                                R.layout.layout_facebook_sync,
                                Facebook_friend_list_matched,
                                AddFriendUsingFacebook.this);
                        friemdsList.setAdapter(objAdapter);
                        friemdsList.setFastScrollEnabled(false);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "No friend found", Toast.LENGTH_SHORT).show();
                    }

                }

                else if (status != null && status.equalsIgnoreCase("error")) {
                    Toast.makeText(getApplicationContext(), msg,
                            Toast.LENGTH_SHORT).show();
                }

                else {
                    Toast.makeText(getApplicationContext(),
                            AppConstants.API_RESPONSE_ERROR_MESSAGE,
                            Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            LoaderUtility.handleLoader(AddFriendUsingFacebook.this, false);
        }

        protected Boolean doInBackground(final String... args) {

            JSONParser jsonParser = new JSONParser();
            List<NameValuePair> parameters = new ArrayList<NameValuePair>();
            parameters.add(new BasicNameValuePair("own_uid", this.ownId
                    .replace(" ", "")));
            parameters.add(new BasicNameValuePair("uids",
                    this.userIds));
            JSONObject jsondata;
            try {
                jsondata = jsonParser.makeHttpRequest(syncUrl, "POST", parameters);
                status = jsondata.getString("status");

                if (status.equalsIgnoreCase("success")) {
                    JSONArray data = jsondata.getJSONArray("all_users");

                    if (data.length() != 0) {
                        for (int i = 0; i < data.length(); i++) {
                            try {
                                JSONObject c = data.getJSONObject(i);
                                String userID = c.getString("user_id");
                                String facebookID = c.getString("fb_uid");
                                String userName = c.getString("username");
                                String userSport = c.getString("sport");
                                boolean connection_ststus = c
                                        .getBoolean("friendship_status");
                                boolean request_status = c
                                        .getBoolean("request_status");

                                String user_image = c.getString("image");

                                for (int ii = 0; ii < Facebook_friend_list
                                        .size(); ii++) {
                                    if (Facebook_friend_list.get(ii)
                                            .getFacebookUserid()
                                            .equalsIgnoreCase(facebookID)) {

                                        Facebook_friend_list.get(ii).setuserId(
                                                userID);

                                        Facebook_friend_list.get(ii)
                                                .setusername(userName);

                                        Facebook_friend_list.get(ii)
                                                .setFacebookuserId(facebookID);

                                        Facebook_friend_list
                                                .get(ii)
                                                .setFacebookfriendship_status(
                                                        String.valueOf(connection_ststus));

                                        Facebook_friend_list
                                                .get(ii)
                                                .setRequest_status(
                                                        String.valueOf(request_status));

                                        Facebook_friend_list.get(ii)
                                                .setSquadzImageUrl(user_image);

                                        Facebook_friend_list.get(ii).setMatch(
                                                true);
                                        // break;
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Facebook_friend_list.clear();
                    }
                } else {
                    msg = jsondata.getString("message");
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }catch(Exception ex){
                ex.printStackTrace();
            }
            return null;
        }
    }
}
