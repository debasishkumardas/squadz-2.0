package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.managers.LocationPickerManager;
import com.andolasoft.squadz.models.LocationUnregisteredVenueModel;
import com.andolasoft.squadz.models.LocationVenueModel;
import com.andolasoft.squadz.models.VenueModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.ListUtils;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.PlaceJSONParser;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.adapters.LocationUnregisteredVenueAdapter;
import com.andolasoft.squadz.views.adapters.LocationVenueAdapter;
import com.andolasoft.squadz.views.adapters.PlaceAutocompleteAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * This class is designed to display all the court listing accroding to user preferred location.
 * This class also handling the court search.
 *
 * @author Soumya Priya Nayak
 * @version 1.0
 * @since 2017-01-04
 */

public class VenueListing extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    public static VenueListing venueListing;
    public static String[] spinnerArray;
    public static ArrayList<VenueModel> venueModelArrayList;
    //private EditText edt_search_field;
    public static AutoCompleteTextView locationPicker;
    public static ArrayList<String> currLocItems = null;
    public static boolean isApiCalled = false;
    public static GPSTracker gpsTracker;
    SnackBar snackBar;
    JSONArray venue_listing_array = null;
    Button btn_creat_game;
    boolean isSoftKeyboardDisplay = false;
    ArrayList<String> images_arraylist;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String userId;
    SwipeRefreshLayout swipe_refresh_layout_venue;
    TextView no_game_found_create_game;
    LinearLayout filter_modify_layout;
    Button modify_btn, reset_btn;
    ImageView cross_icon;
    ProgressBar barProgress;
    boolean isPageLoadFirstTime = true;
    ParserTask parserTask;
    PlacesTask placesTask;
    LocationVenueAdapter locationVenueAdapter;
    LocationUnregisteredVenueAdapter locationUnregisteredVenueAdapter;

    ArrayList<LocationVenueModel> locationVenueModelArrayList;
    LocationVenueModel locationVenueModel;

    ArrayList<LocationUnregisteredVenueModel> locationUnregisteredVenueModels;
    LocationUnregisteredVenueModel locationUnregisteredVenueModel;

    private RecyclerView venue_recycler_view;
    private RelativeLayout lay_no_event_found;
    private VenueModel venueModel;
    private VenueAdapter venueAdapter;
    private Spinner spinner_venue_list;
    private ImageView venue_filter_icon, venue_search_icon,
            search_back_icon, venue_location_icon;
    private RelativeLayout search_layout, venue_back_icon;
    private Toolbar venue_toolbar;
    private MaterialFavoriteButton favorite;
    private ProgressDialog dialog;
    private HashMap<String, String> map_venue_name_spiner;
    private ArrayList<String> venue_spinner_name_array, venue_spinner_id_array;
    private int counterCheck = 0;
    boolean is_Unregistred_Venues_Available = false;
    ListView listView,listView2;
    RelativeLayout list_layout,list2_layout;
    ScrollView scroll_bars;
    TextView registered_venue_text;
    protected GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter mAdapter;
    private static final LatLngBounds BOUNDS_GREATER = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));
    ArrayList<PlaceAutocomplete> resultList;
    AutocompleteFilter autocompleteFilter;
    String short_address = "",full_address = "";
    public static String currentCity = "",game_address = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set up for Font for the screen
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

       /* *//**Initializing API client*//*
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 *//* clientId *//*, this)
                .addApi(Places.GEO_DATA_API)
                .build();*/

        setContentView(R.layout.activity_venue_listing);

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(VenueListing.this);

        //INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
        initReferences();

        //Function call to get Venue courts from server
        getVenuesFilterListing();

        //ADDED CLICK EVENT ON THE VIEWS
        setOnClickEvents();

        /*autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();*/

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

       /* //Get AWS Credentials

        // Initialize the Amazon Cognito credentials provider
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-east-1:3b26eea3-6262-4dca-9ade-d626c646bbe3", // Identity Pool ID
                Regions.US_EAST_1 // Region
        );

        AmazonDynamoDBClient ddbClient = new AmazonDynamoDBClient(credentialsProvider);
        DynamoDBMapper mapper = new DynamoDBMapper(ddbClient);

        Venue selectedBook = mapper.load(Venue.class, "6d42f986-850d-4170-bed8-ca87b1e6d1c3");
        System.out.print(""+selectedBook);*/


    }

    /**
     * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
     */
    public void initReferences() {
        venue_recycler_view = (RecyclerView) findViewById(R.id.venue_recycler_view);
        spinner_venue_list = (Spinner) findViewById(R.id.spinner_venue_list);
        venue_filter_icon = (ImageView) findViewById(R.id.venue_filter_icon);
        venue_search_icon = (ImageView) findViewById(R.id.venue_search_icon);
        venue_location_icon = (ImageView) findViewById(R.id.location_icon_venue);
        venue_back_icon = (RelativeLayout) findViewById(R.id.back_icon_venue_listinglayout);
        search_back_icon = (ImageView) findViewById(R.id.back_image_search);
        search_layout = (RelativeLayout) findViewById(R.id.search_layout);
        venue_toolbar = (Toolbar) findViewById(R.id.venue_toolbar);
        //edt_search_field = (EditText) findViewById(R.id.search_edit_field);
        locationPicker = (AutoCompleteTextView) findViewById(R.id.pickaddress_et);
        swipe_refresh_layout_venue = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout_venue);
        btn_creat_game = (Button) findViewById(R.id.btn_create_game);
        lay_no_event_found = (RelativeLayout) findViewById(R.id.lay_no_event_found_create_game);
        no_game_found_create_game = (TextView) findViewById(R.id.no_game_found_create_game);
        filter_modify_layout = (LinearLayout) findViewById(R.id.filter_modify_layout);
        modify_btn = (Button) findViewById(R.id.modify_btn);
        reset_btn = (Button) findViewById(R.id.reset_btn);
        barProgress = (ProgressBar) findViewById(R.id.progressLoading);
        cross_icon =(ImageView) findViewById(R.id.cross_icon);
        barProgress.setVisibility(View.INVISIBLE);
        lay_no_event_found.setVisibility(View.GONE);
        listView = (ListView) findViewById(R.id.listView);
        listView2 = (ListView) findViewById(R.id.listView2);
        list_layout = (RelativeLayout) findViewById(R.id.list_layout);
        list2_layout = (RelativeLayout) findViewById(R.id.list2_layout);
        scroll_bars = (ScrollView) findViewById(R.id.scroll_bars);
        registered_venue_text = (TextView) findViewById(R.id.registered_venue_text);
        //registered_venue_text.setVisibility(View.INVISIBLE);

        if (locationPicker != null) {
            locationPicker.setThreshold(1);
        }

        //Change the Swipe layout color
        swipe_refresh_layout_venue.setColorSchemeResources(R.color.orange,
                R.color.orange,
                R.color.orange);

        //Hiding the Soft Key Board
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //Initializing the snack Bar
        snackBar = new SnackBar(VenueListing.this);

        //Initializing the GPS tracker
        gpsTracker = new GPSTracker(this);

        //Initializing the Material Favorite Button
        favorite = new MaterialFavoriteButton.Builder(this).create();

        //Initializng the Context
        venueListing = this;

        //Initializing the Shared Preferance
        preferences = PreferenceManager
                .getDefaultSharedPreferences(VenueListing.this);
        editor = preferences.edit();

        //Getting the Logged in user id from shared Preferance
        if (preferences.contains("loginuser_id")) {
            userId = preferences.getString("loginuser_id", "");
        }
    }

    /**
     * ADDED CLICK EVENT ON THE VIEWS
     */
    public void setOnClickEvents() {
        venue_filter_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //finish();
                Intent intent_Filter = new Intent(VenueListing.this, Filter.class);
                startActivity(intent_Filter);
            }
        });

        venue_search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                locationPicker.setText("");
                venue_toolbar.setVisibility(View.INVISIBLE);
                search_layout.setVisibility(View.VISIBLE);
                /*edt_search_field.requestFocus();
                edt_search_field.setText("");*/

                //Displaying the Soft Keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        });

       /* edt_search_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                venueAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    venue_recycler_view.setLayoutManager(mLayoutManager);
                    venue_recycler_view.setItemAnimator(new DefaultItemAnimator());
                    venue_recycler_view.setAdapter(venueAdapter);
                    venueAdapter.notifyDataSetChanged();
                }
            }
        });*/

        venue_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //KEEPING ALL VALUES TO NULL
                Constants.setAllValuesToNull();
                removeDataFromFile();
                if (VenueListing.venueListing != null) {
                    VenueListing.venueListing.finish();
                }
                VenueListing.venueListing.finish();
            }
        });

        search_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View view2 = getCurrentFocus();
                if (view2 != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                venue_toolbar.setVisibility(View.VISIBLE);
                search_layout.setVisibility(View.INVISIBLE);

                //Setting Adapter to Recycler view
                setDataToRecyclerView();

            }
        });

        /**
         * CHECKING IF SOFT KEYBOARD IS UP OR DOWN
         */
      /* edt_search_field.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (keyboardShown(edt_search_field.getRootView())) {
                    isSoftKeyboardDisplay = true;
                } else {

                }
            }
        });*/


        //Location icon click
        venue_location_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //finish();
                Intent intent_availability = new Intent(VenueListing.this, VenueMapView.class);
                startActivity(intent_availability);
            }
        });


        spinner_venue_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Displaying the Location Picker
                    LocationPickerManager locationPickerManager
                            = new LocationPickerManager(VenueListing.this);
                }
                return true;
            }
        });

        //Pull to refresh functionality
        swipe_refresh_layout_venue.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getVenuesFilterListing();
                swipe_refresh_layout_venue.setRefreshing(false);
            }
        });


        btn_creat_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(full_address)) {
                    Constants.SEARCH_LOCATION_ADDRESS = short_address + ", " + full_address;
                } else if (!TextUtils.isEmpty(short_address)){
                    Constants.SEARCH_LOCATION_ADDRESS = short_address;
                }
                startActivity(new Intent(VenueListing.this, CreateEventWithoutCourt.class));
            }
        });

        /**
         * Places API call
         */
       /* locationPicker.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                try {
                    if (ApplicationUtility.isConnectingToInternet(VenueListing.this))
                    {
                        int lenght = s.length();
                        if(lenght >= 2) {
                            scroll_bars.setVisibility(View.VISIBLE);
                            getVenuesFromSearch(s.toString());
                        }
                        else if(lenght == 0)
                        {
                            scroll_bars.setVisibility(View.GONE);
                        }
                    } else {
                        snackBar.setSnackBarMessage("Please check your internet connection");
                    }

                } catch (Exception exp) {
                    exp.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }
        });*/

       /* mAdapter = new PlaceAutocompleteAdapter(VenueListing.this, mGoogleApiClient, BOUNDS_GREATER,
                null);
        locationPicker.setAdapter(mAdapter);*/

        locationPicker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                try {
                    if (ApplicationUtility.isConnectingToInternet(VenueListing.this))
                    {
                        int lenght = s.length();
                        if(lenght >= 2) {
                            scroll_bars.setVisibility(View.VISIBLE);
                            getVenuesFromSearch(s.toString());
                        }
                        else if(lenght == 0)
                        {
                            scroll_bars.setVisibility(View.GONE);
                        }
                    } else {
                        snackBar.setSnackBarMessage("Please check your internet connection");
                    }

                } catch (Exception exp) {
                    exp.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {

                    InputMethodManager imm = (InputMethodManager) VenueListing.this
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(locationPicker.getWindowToken(), 0);

                    View view2 = getCurrentFocus();
                    if (view2 != null) {
                        InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm2.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                   // venue_toolbar.setVisibility(View.VISIBLE);
                    //search_layout.setVisibility(View.INVISIBLE);


                    if(locationVenueModelArrayList.size() > 0) {

                        short_address = locationVenueModelArrayList.get(position).get_Venue_Short_address();
                        full_address = locationVenueModelArrayList.get(position).get_Venue_Full_address();
                        String venue_Id = locationVenueModelArrayList.get(position).get_Venue_Id();
                        boolean venue_registered_status = locationVenueModelArrayList.get(position).get_Venue_Registered_status();

                        /**Keeping the address for displaying in POI game additional info page*/
                        locationPicker.setText("");
                        if (!TextUtils.isEmpty(full_address)) {
                            Constants.SEARCH_LOCATION_ADDRESS = short_address + ", " + full_address;
                        } else {
                            Constants.SEARCH_LOCATION_ADDRESS = short_address;
                        }
                        locationPicker.setText(Constants.SEARCH_LOCATION_ADDRESS);

                        if (!venue_registered_status) {
                            /**Initializing the adapter class*/
                            lay_no_event_found.setVisibility(View.VISIBLE);
                            venue_recycler_view.setVisibility(View.GONE);
                            swipe_refresh_layout_venue.setVisibility(View.GONE);
                        }
                        else{
                            if(!TextUtils.isEmpty(venue_Id)) {
                                getVenuesSearchListing(venue_Id);
                            }
                            else{
                                snackBar.setSnackBarMessage("No venue found");
                            }
                        }
                    }

                    /**Hiding Listview*/
                    /*list_layout.setVisibility(View.GONE);
                    list2_layout.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                    listView2.setVisibility(View.GONE);*/

                    scroll_bars.setVisibility(View.GONE);

                    /*locationVenueModelArrayList.clear();
                    locationVenueAdapter = new LocationVenueAdapter(VenueListing.this, locationVenueModelArrayList);
                    listView.setAdapter(locationVenueAdapter);
                    locationVenueAdapter.notifyDataSetChanged();*/

                }catch (Exception exp)
                {
                    exp.printStackTrace();
                }

                //String locationAddress = parent.getItemAtPosition(position).toString();

                //Getting latitude & longitude based on the city name*/
               /* Geocoder coder = new Geocoder(VenueListing.this);
                try {
                    ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(locationAddress, 10);
                    for(Address add : adresses){
                        double longitude = add.getLongitude();
                        double latitude = add.getLatitude();
                        //Toast.makeText(VenueListing.this, ""+longitude+","+latitude, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch(IllegalArgumentException e){
                    e.printStackTrace();
                }*/
            }
        });

        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {

                    InputMethodManager imm = (InputMethodManager) VenueListing.this
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(locationPicker.getWindowToken(), 0);

                    View view2 = getCurrentFocus();
                    if (view2 != null) {
                        InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm2.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    // venue_toolbar.setVisibility(View.VISIBLE);
                    //search_layout.setVisibility(View.INVISIBLE);


                    if(locationUnregisteredVenueModels.size() > 0) {

                        short_address = locationUnregisteredVenueModels.get(position).get_Venue_Short_address();
                        full_address = locationUnregisteredVenueModels.get(position).get_Venue_Full_address();
                        String venue_Id = locationUnregisteredVenueModels.get(position).get_Venue_Id();
                        boolean venue_registered_status = locationUnregisteredVenueModels.get(position).get_Venue_Registered_status();

                        /**Keeping the address for displaying in POI game additional info page*/
                        locationPicker.setText("");
                        if (!TextUtils.isEmpty(full_address)) {
                            Constants.SEARCH_LOCATION_ADDRESS = short_address + ", " + full_address;
                            game_address = full_address;
                        } else {
                            Constants.SEARCH_LOCATION_ADDRESS = short_address;
                            game_address = short_address;
                        }
                        locationPicker.setText(Constants.SEARCH_LOCATION_ADDRESS);

                        if (!venue_registered_status) {
                            /**Initializing the adapter class*/
                            lay_no_event_found.setVisibility(View.VISIBLE);
                            venue_recycler_view.setVisibility(View.GONE);
                            swipe_refresh_layout_venue.setVisibility(View.GONE);
                        }
                        else{
                            if(!TextUtils.isEmpty(venue_Id)) {
                                getVenuesSearchListing(venue_Id);
                            }
                            else{
                                snackBar.setSnackBarMessage("No venue found");
                            }
                        }
                    }

                    /**Hiding Listview*/
                    scroll_bars.setVisibility(View.GONE);
                    /*list_layout.setVisibility(View.GONE);
                    list2_layout.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                    listView2.setVisibility(View.GONE);*/
                    /*locationUnregisteredVenueModels.clear();

                    locationUnregisteredVenueAdapter = new LocationUnregisteredVenueAdapter(VenueListing.this, locationUnregisteredVenueModels);
                    listView2.setAdapter(locationUnregisteredVenueAdapter);
                    locationUnregisteredVenueAdapter.notifyDataSetChanged();*/

                }catch (Exception exp)
                {
                    exp.printStackTrace();
                }

                //Getting latitude & longitude based on the city name*/
                Geocoder coder = new Geocoder(VenueListing.this);
                try {
                    ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(Constants.SEARCH_LOCATION_ADDRESS, 10);
                    for(Address add : adresses){

                        double longitude = add.getLongitude();
                        double latitude = add.getLatitude();
                        Constants.GAME_LOCATION = latitude+","+longitude;
                        currentCity = LocationPickerManager.getCity(VenueListing.this,
                                latitude, longitude);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch(IllegalArgumentException e){
                    e.printStackTrace();
                }
            }
        });

        cross_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationPicker.setText("");
            }
        });
    }

    /**
     * getting the Soft Keyboard Displaying Status
     *
     * @param rootView The RootView
     * @return The key board shown status
     */
    private boolean keyboardShown(View rootView) {
        final int softKeyboardHeight = 100;
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
        int heightDiff = rootView.getBottom() - r.bottom;
        return heightDiff > softKeyboardHeight * dm.density;
    }

    /**
     * REMOVING DATA FROM SHARED PREFERENCE OF FILTER VENUES SPECIFICATIONS
     */
    public void removeDataFromFile() {
        try {
            SharedPreferences prefs = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear();
            editor.apply();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    /**
     * Function to get Venue courts from server
     */
    public void getVenuesFilterListing() {
        isApiCalled = true;
        //Handling the loader state
        LoaderUtility.handleLoader(VenueListing.this, true);
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(VenueListing.this, true);
                //Displaying the Error Message if the call get failed
                if (!ApplicationUtility.isConnected(VenueListing.this)) {
                    ApplicationUtility.displayNoInternetMessage(VenueListing.this);
                } else {
                    ApplicationUtility.displayErrorMessage(VenueListing.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", image = "", name = "", latitude = "", longitude = "",
                        review_rating = "", review_count = "", price = "", court_type = "", venueName_listing = "";
                boolean favourite_status = false;

                try {
                    venueModelArrayList = new ArrayList<>();
                    venue_listing_array = new JSONArray(responseData);

                    for (int i = 0; i < venue_listing_array.length(); i++) {
                        venueModel = new VenueModel();
                        images_arraylist = new ArrayList<String>();
                        JSONObject responseObject = venue_listing_array.getJSONObject(i);

                        if (responseObject.has("id") && !responseObject.isNull("id")) {
                            id = responseObject.getString("id");
                        }
                        if (responseObject.has("name") && !responseObject.isNull("name")) {
                            name = responseObject.getString("name");
                        }
                        if (responseObject.has("latitude") && !responseObject.isNull("latitude")) {
                            latitude = responseObject.getString("latitude");
                        }
                        if (responseObject.has("longitude") && !responseObject.isNull("longitude")) {
                            longitude = responseObject.getString("longitude");
                        }
                        if (responseObject.has("review_rating") && !responseObject.isNull("review_rating")) {
                            review_rating = responseObject.getString("review_rating");
                        }
                        if (responseObject.has("review_count") && !responseObject.isNull("review_count")) {
                            review_count = responseObject.getString("review_count");
                        }
                        if (responseObject.has("price") && !responseObject.isNull("price")) {
                            price = responseObject.getString("price");
                        }
                        if (responseObject.has("is_favorite") && !responseObject.isNull("is_favorite")) {
                            favourite_status = responseObject.getBoolean("is_favorite");
                        }

                        if (responseObject.has("venue_name") && !responseObject.isNull("venue_name")) {
                            venueName_listing = responseObject.getString("venue_name");
                        }

                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            JSONArray images_json_array = responseObject.getJSONArray("image");
                            for (int j = 0; j < images_json_array.length(); j++) {
                                images_arraylist.add(images_json_array.getString(j));
                            }
                            venueModel.setImages_array(images_arraylist);
                        }

                        //Getting the court distance from the user location
                        double distance = ApplicationUtility.distance(gpsTracker.getLatitude(), gpsTracker.getLongitude(),
                                Double.valueOf(latitude), Double.valueOf(longitude));

                        //Setting all the data in the model
                        venueModel.setVenueId(id);
                        venueModel.setVenueImage(image);
                        venueModel.setVenueName(name);
                        venueModel.setLatitude(latitude);
                        venueModel.setLongitude(longitude);
                        venueModel.setVenueRatings(review_rating);
                        venueModel.setReview_count(review_count);
                        venueModel.setVenuePrice(price);
                        venueModel.setFavourited(favourite_status);
                        venueModel.setMiles(distance);
                        venueModel.setVenueName_listing(venueName_listing);

                        //Adding the model into the Arraylist
                        venueModelArrayList.add(venueModel);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                VenueListing.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(VenueListing.this, false);

                        //Keeping vebue names inside spinner
                        setVenueAppletonType();

                        if (venue_listing_array != null) {
                            if (venue_listing_array.length() > 0) {

                                //Handling the views
                                enableViews(true);

                                //Enabling the
                                swipe_refresh_layout_venue.setVisibility(View.VISIBLE);

                                //Displaying the events in the page
                                venue_recycler_view.setVisibility(View.VISIBLE);

                                lay_no_event_found.setVisibility(View.GONE);

                                //Add data to Recycler view
                                setDataToRecyclerView();

                            } else {
                                //Displaying the no events in the page
                                swipe_refresh_layout_venue.setVisibility(View.GONE);
                                lay_no_event_found.setVisibility(View.VISIBLE);
                                venueModelArrayList = new ArrayList<VenueModel>();
                                /*venueAdapter = new VenueAdapter(VenueListing.this, venueModelArrayList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                venue_recycler_view.setLayoutManager(mLayoutManager);
                                venue_recycler_view.setItemAnimator(new DefaultItemAnimator());
                                venue_recycler_view.setAdapter(venueAdapter);
                                venueAdapter.notifyDataSetChanged();*/
                                // snackBar.setSnackBarMessage("No courts found");
                                if (Constants.VENUE_FILTER_BTN_CLICK) {
                                    venue_filter_icon.setFocusable(true);
                                    venue_filter_icon.setClickable(true);
                                    //venue_search_icon.setFocusable(false);
                                    //venue_search_icon.setClickable(false);
                                    venue_location_icon.setFocusable(false);
                                    venue_location_icon.setClickable(false);
                                    Constants.VENUE_FILTER_BTN_CLICK = false;

                                    /**Display message with button if no results found in filter*/
                                    displayMessageButton();

                                } else {
                                    //Handling the views
                                    enableViews(false);
                                }
                                //snackBar.setSnackBarMessage("No courts found");
                            }
                        } else {
                            if (Constants.VENUE_FILTER_BTN_CLICK) {
                                venue_filter_icon.setFocusable(true);
                                venue_filter_icon.setClickable(true);
                                //venue_search_icon.setFocusable(false);
                                //venue_search_icon.setClickable(false);
                                venue_location_icon.setFocusable(false);
                                venue_location_icon.setClickable(false);
                                Constants.VENUE_FILTER_BTN_CLICK = false;

                                /**Display message with button if no results found in filter*/
                                displayMessageButton();

                            } else {
                                //Handling the views
                                enableViews(false);
                            }
                            // snackBar.setSnackBarMessage("No courts found");
                        }
                    }
                });
            }
        });
    }


    /**
     * Function handling all the views enable/disable state
     *
     * @param status
     */
    public void enableViews(boolean status) {
        if (status) {
            venue_filter_icon.setFocusable(true);
            venue_filter_icon.setClickable(true);
            venue_search_icon.setFocusable(true);
            venue_search_icon.setClickable(true);
            venue_location_icon.setFocusable(true);
            venue_location_icon.setClickable(true);
        } else {
            venue_filter_icon.setFocusable(false);
            venue_filter_icon.setClickable(false);
            //venue_search_icon.setFocusable(false);
            //venue_search_icon.setClickable(false);
            venue_location_icon.setFocusable(false);
            venue_location_icon.setClickable(false);

        }
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_venue_courts").newBuilder();
        urlBuilder.addQueryParameter("location", String.valueOf(HomeFragment.currLocLatitude + "," + HomeFragment.currLocLongitude));
        urlBuilder.addQueryParameter("sportid", Constants.SPORTS_ID);
        urlBuilder.addQueryParameter("sport_name", Constants.SPORTS_NAME);
        urlBuilder.addQueryParameter("current_city", HomeFragment.current_city);
        urlBuilder.addQueryParameter("user_id", userId);

        if (Constants.VENUE_FILTER_BTN_CLICK) {
            urlBuilder.addQueryParameter("filter", Boolean.TRUE.toString());

            if (Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (String s : Constants.FILTER_SPECIFICATIN_CHECKBOX_ARRAY) {
                    sb.append(s);
                    sb.append(",");
                }

                urlBuilder.addQueryParameter("specs", sb.toString());
            }
            if (!TextUtils.isEmpty(Constants.FILTER_PAID_VENUES_NAME)) {
                urlBuilder.addQueryParameter("proximity", Constants.FILTER_PAID_VENUES_NAME);
            }

            if (!TextUtils.isEmpty(Constants.FILTER_SELECT_DATE)) {
                urlBuilder.addQueryParameter("date", ApplicationUtility.customFormattedDateForEventFilter(Constants.FILTER_SELECT_DATE));//date - dd/mm/yyyy
            }

            if (!TextUtils.isEmpty(Constants.FILTER_FROM_TIME)) {
                urlBuilder.addQueryParameter("start_time", ApplicationUtility.formattedTime(Constants.FILTER_FROM_TIME).toLowerCase());
            }
            if (!TextUtils.isEmpty(Constants.FILTER_TO_TIME)) {
                urlBuilder.addQueryParameter("end_time", ApplicationUtility.formattedTime(Constants.FILTER_TO_TIME).toLowerCase());
            }
            if (!TextUtils.isEmpty(Constants.FILTER_MAP_RADIUS_MILES)) {
                urlBuilder.addQueryParameter("miles", Constants.FILTER_MAP_RADIUS_MILES);
            }


        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        /*Request request = new Request.Builder()
                .url(EndPoints.BASE_URL_VENUE+"get_venue_courts").method("POST",
                        RequestBody.create(null, new byte[0]))
                .build();*/

        return request;
    }


    /**
     * Function to get Venue lists from server
     */
    public void getVenuesListing_Courts() {
        //Handling the loader state
        LoaderUtility.handleLoader(VenueListing.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(VenueListing.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", image = "", name = "", latitude = "", longitude = "",
                        review_rating = "", review_count = "",
                        price = "", venue_spinner_id = "", venue_spinner_name = "";

                try {

                    JSONArray venue_listing_array = new JSONArray(responseData);
                    venueModelArrayList = new ArrayList<>();
                    venue_spinner_name_array = new ArrayList<String>();
                    venue_spinner_id_array = new ArrayList<String>();

                    for (int i = 0; i < venue_listing_array.length(); i++) {

                        JSONObject venue_listing_court_object = venue_listing_array.getJSONObject(i);

                        venue_spinner_id = venue_listing_court_object.getString("id");
                        venue_spinner_name = venue_listing_court_object.getString("name");

                        venue_spinner_name_array.add(venue_spinner_name);
                        venue_spinner_id_array.add(venue_spinner_id);

                        if (venue_listing_court_object.has("court_lists")) {
                            JSONArray courts_lists_array = venue_listing_court_object.getJSONArray("court_lists");

                            for (int j = 0; j < courts_lists_array.length(); j++) {

                                venueModel = new VenueModel();

                                JSONObject court_lists_object = courts_lists_array.getJSONObject(i);

                                if (court_lists_object.has("id") && !court_lists_object.isNull("id")) {
                                    id = court_lists_object.getString("id");
                                }
                                if (court_lists_object.has("image") && !court_lists_object.isNull("image")) {
                                    image = court_lists_object.getString("image");
                                }
                                if (court_lists_object.has("name") && !court_lists_object.isNull("name")) {
                                    name = court_lists_object.getString("name");
                                }
                                if (court_lists_object.has("latitude") && !court_lists_object.isNull("latitude")) {
                                    latitude = court_lists_object.getString("latitude");
                                }
                                if (court_lists_object.has("longitude") && !court_lists_object.isNull("longitude")) {
                                    longitude = court_lists_object.getString("longitude");
                                }
                                if (court_lists_object.has("review_rating") && !court_lists_object.isNull("review_rating")) {
                                    review_rating = court_lists_object.getString("review_rating");
                                }
                                if (court_lists_object.has("review_count") && !court_lists_object.isNull("review_count")) {
                                    review_count = court_lists_object.getString("review_count");
                                }
                                if (court_lists_object.has("price") && !court_lists_object.isNull("price")) {
                                    price = court_lists_object.getString("price");
                                }

                                venueModel.setVenueId(id);
                                venueModel.setVenueImage(image);
                                venueModel.setVenueName(name);
                                venueModel.setLatitude(latitude);
                                venueModel.setLongitude(longitude);
                                venueModel.setVenueRatings(review_rating);
                                venueModel.setReview_count(review_count);
                                venueModel.setVenuePrice(price);

                                venueModelArrayList.add(venueModel);
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                //Handling the loader state
                LoaderUtility.handleLoader(VenueListing.this, false);

                VenueListing.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Keeping vebue names inside spinner
                        setVenueAppletonType();

                        //Setting Adapter to Recycler view
                        setDataToRecyclerView();
                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForListing() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "venue_lists").newBuilder();
        //urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, user_name);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(VenueListing.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * KEEPING VENUE APPLETON NAME INSIDE THE SPINNER
     */
    public void setVenueAppletonType() {

       /* //Getting venue name & id as a array to display inside the spinner
        spinnerArray = new String[venue_spinner_id_array.size()];
        map_venue_name_spiner = new HashMap<String, String>();

        for (int i = 0; i < venue_spinner_id_array.size(); i++) {
            map_venue_name_spiner.put(venue_spinner_name_array.get(i), venue_spinner_id_array.get(i));
            spinnerArray[i] = venue_spinner_name_array.get(i);
        }*/

        currLocItems = new ArrayList<>();
        currLocItems.add(HomeFragment.current_city);
        //Displaying name inside spinner
        displaySpinnerInfo(currLocItems);
    }

    /**
     * Displaying name inside spinner
     */
    public void displaySpinnerInfo(ArrayList<String> spinnerArray) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(VenueListing.this, android.R.layout.simple_spinner_item, spinnerArray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_venue_list.setAdapter(dataAdapter);
        spinner_venue_list.setSelection(0);
        //spinner_venue_list.setClickable(false);
    }

    /**
     * Setting Adapter to Recycler view
     */
    public void setDataToRecyclerView() {
        //Passing venue info to Recycler view adapter
        if (isPageLoadFirstTime) {
            isPageLoadFirstTime = false;
            Collections.sort(venueModelArrayList, new Comparator<VenueModel>() {
                @Override
                public int compare(VenueModel lhs, VenueModel rhs) {
                    return String.valueOf(lhs.getMiles()).compareTo(String.valueOf(rhs.getMiles()));
                }
            });
        }

        venueAdapter = new VenueAdapter(VenueListing.this, venueModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        venue_recycler_view.setLayoutManager(mLayoutManager);
        venue_recycler_view.setItemAnimator(new DefaultItemAnimator());
        venue_recycler_view.setAdapter(venueAdapter);
        venueAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if (isSoftKeyboardDisplay) {
            isSoftKeyboardDisplay = false;
            venue_toolbar.setVisibility(View.VISIBLE);
            search_layout.setVisibility(View.INVISIBLE);

            //Setting Adapter to Recycler view
            setDataToRecyclerView();
        } else {
            //KEEPING ALL VALUES TO NULL
            Constants.setAllValuesToNull();
            removeDataFromFile();

            if (VenueListing.venueListing != null) {
                VenueListing.venueListing.finish();
            }
            VenueListing.venueListing.finish();
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Google", "Place Selected: " + place.getName());
                HomeFragment.address = place.getAddress().toString();
                String latLng = place.getLatLng().toString();
                String[] latlngArr = latLng.split(" ");
                String latlngVal = latlngArr[1].substring(1, latlngArr[1].length() - 1);
                String[] finalArray = latlngVal.split(",");
                HomeFragment.currLocLatitude = Double.parseDouble(finalArray[0].toString());
                HomeFragment.currLocLongitude = Double.parseDouble(finalArray[1].toString());
                setVenueAppletonType();
                AppConstants.isLocationPicked_venue_listing = true;
                AppConstants.isLocationPicked = true;
                AppConstants.isLocationPicked_venue_mapview = true;
                isApiCalled = false;

                // Display attributions if required.
                CharSequence attributions = place.getAttributions();
                if (!TextUtils.isEmpty(attributions)) {
                    //mPlaceAttribution.setText(Html.fromHtml(attributions.toString()));
                } else {
                    //mPlaceAttribution.setText("");
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("Google", "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }
    }

    /**
     * Display message with button if no results found in filter
     */
    public void displayMessageButton() {
        no_game_found_create_game.setText("No venues found based on your filters, please modify your filters to search for venues.");
        btn_creat_game.setVisibility(View.GONE);

        filter_modify_layout.setVisibility(View.VISIBLE);

        /**Modify button click*/
        modify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_Filter = new Intent(VenueListing.this, Filter.class);
                startActivity(intent_Filter);
            }
        });

        /**Reset button click*/
        reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVenuesFilterListing();
                Constants.setAllValuesToNull();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setVenueAppletonType();
        LocationPickerManager.getCurrentCity(VenueListing.this);
        if (AppConstants.isLocationPicked_venue_listing &&
                !isApiCalled) {
            //AppConstants.isLocationPicked = false;
            AppConstants.isLocationPicked_venue_listing = false;
            isApiCalled = true;
            getVenuesFilterListing();
        } else if (Constants.VENUE_FILTER_BTN_CLICK) {
            // Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
            getVenuesFilterListing();
        }
        /**Getting venue id if that is found in POI additional page location search*/
        else if(!TextUtils.isEmpty(Constants.SEARCH_VENUE_ID))
        {
            getVenuesSearchListing(Constants.SEARCH_VENUE_ID);
        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();

            br.close();
        } catch (Exception e) {
            // Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    public class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {  //39.332641
            // For storing data from web service
            String data = "";
            // Obtain browser key from https://code.google.com/apis/console
            //String key = "key=AIzaSyAqGDJ21KFY_iuYsKeeucLbydRUBVcCXXo";
            String key = "key=AIzaSyAfwuoJ0Vn4QQ7mmlpe_FibC0keZ_Uv2V8";
            //String key = "key=AIzaSyCOgTHJkQztzyJTLnI7_jjXJ_jvkzcJGJw";
            //AIzaSyAfwuoJ0Vn4QQ7mmlpe_FibC0keZ_Uv2V8

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            // place type to be searched
            String types = "types=establishment|geocode";
            // Sensor enabled
            //String sensor = "sensor=false";
            //String radius = "32500";
            String location = "location="+gpsTracker.getLatitude()+","+gpsTracker.getLongitude();
            //String radius = preferences.getString("RADIUS_COUNT","");
            // String component="components=country:"+iso_countrycode;
            // Building the parameters to the web service
           /* String parameters = input + "&" + types + "&" + radius + "&"
                    + sensor + "&" + key;*/
            String parameters = input + "&" + location + "&" + key;
            // Output format
            String output = "json";
            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"
                    + output + "?" + parameters;
            try {
                // Fetching the data from web service in background
                data = downloadUrl(url);
                System.out.println("url----" + url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Creating ParserTask
            parserTask = new ParserTask();
            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    public class ParserTask extends
            AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;
        JSONArray contacts = null;
        JSONObject structured_formatting_object = null;
        ArrayList<String> places = new ArrayList<String>();
        ArrayAdapter<String> adapter;

        @Override
        protected List<HashMap<String, String>> doInBackground(
                String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {

                jObject = new JSONObject(jsonData[0]);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
                System.out.println("Places are" + places);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            try {
                String[] id_val;
                String short_address = "", full_address = "";

                if(locationVenueModelArrayList.size() > 0) {
                    registered_venue_text.setVisibility(View.VISIBLE);
                    locationVenueAdapter = new LocationVenueAdapter(VenueListing.this, locationVenueModelArrayList);
                    listView.setAdapter(locationVenueAdapter);
                    locationVenueAdapter.notifyDataSetChanged();
                    ListUtils.setDynamicHeight_teammate(listView);
                }
                else{
                    registered_venue_text.setVisibility(View.INVISIBLE);
                    locationVenueAdapter = new LocationVenueAdapter(VenueListing.this, locationVenueModelArrayList);
                    listView.setAdapter(locationVenueAdapter);
                    locationVenueAdapter.notifyDataSetChanged();
                    ListUtils.setDynamicHeight_teammate(listView);
                }

                /**Unregistered venues*/
                locationUnregisteredVenueModels = new ArrayList<>();

                contacts = jObject.getJSONArray("predictions");

                for (int i = 0; i < contacts.length(); i++) {
                    locationUnregisteredVenueModel = new LocationUnregisteredVenueModel();
                    JSONObject c = contacts.getJSONObject(i);

                    String address = c.getString("description");
                    String reference = c.getString("reference");
                    String place_id = c.getString("place_id");

                    if(c.has("structured_formatting")) {
                        structured_formatting_object = c.getJSONObject("structured_formatting");

                        if (structured_formatting_object.has("main_text") && !structured_formatting_object.isNull("main_text")) {
                            short_address = structured_formatting_object.getString("main_text");
                        }
                        if (structured_formatting_object.has("secondary_text") && !structured_formatting_object.isNull("secondary_text")) {
                            full_address = structured_formatting_object.getString("secondary_text");
                        }
                        locationUnregisteredVenueModel.setVenue_Short_Address(short_address);
                        locationUnregisteredVenueModel.setVenue_Full_Address(full_address);
                        locationUnregisteredVenueModel.setVenue_Registered_status(false);

                        locationUnregisteredVenueModels.add(locationUnregisteredVenueModel);
                    }
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                // Setting the adapter
                locationUnregisteredVenueAdapter = new LocationUnregisteredVenueAdapter(VenueListing.this, locationUnregisteredVenueModels);
                listView2.setAdapter(locationUnregisteredVenueAdapter);
                locationUnregisteredVenueAdapter.notifyDataSetChanged();
                ListUtils.setDynamicHeight_teammate(listView2);
                /*locationVenueAdapter = new LocationVenueAdapter(VenueListing.this, locationVenueModelArrayList,is_Unregistred_Venues_Available);
                listView.setAdapter(locationVenueAdapter);
                locationVenueAdapter.notifyDataSetChanged();*/

            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    /**
     * Function to get Venue by search from server
     */
    public void getVenuesFromSearch(final String venue_name) {
        //Handling the loader state
        //LoaderUtility.handleLoader(VenueListing.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyFroSearch(venue_name);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(VenueListing.this, true);
                //Displaying the Error Message if the call get failed
                if (!ApplicationUtility.isConnected(VenueListing.this)) {
                    ApplicationUtility.displayNoInternetMessage(VenueListing.this);
                } else {
                    ApplicationUtility.displayErrorMessage(VenueListing.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", name = "", location = "";

                try {
                    locationVenueModelArrayList = new ArrayList<>();

                    JSONObject response_Object = new JSONObject(responseData);
                    if(response_Object.has("status"))
                    {
                        String status = response_Object.getString("status");
                        if(status.equalsIgnoreCase("success"))
                        {
                            if(response_Object.has("venues") && !response_Object.isNull("venues"))
                            {
                                JSONArray jsonArray = response_Object.getJSONArray("venues");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    locationVenueModel = new LocationVenueModel();

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    id = jsonObject.getString("id");
                                    name = jsonObject.getString("name");
                                    location = jsonObject.getString("location");

                                    locationVenueModel.setVenue_Id(id);
                                    locationVenueModel.setVenue_Short_Address(name);
                                    locationVenueModel.setVenue_Full_Address(location);
                                    locationVenueModel.setVenue_Registered_status(true);

                                    locationVenueModelArrayList.add(locationVenueModel);
                                }
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                VenueListing.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        //LoaderUtility.handleLoader(VenueListing.this, false);
                        try {
                            placesTask = new PlacesTask();
                            placesTask.execute(venue_name);

                            /*locationVenueAdapter = new LocationVenueAdapter(VenueListing.this, locationVenueModelArrayList);
                            listView.setAdapter(locationVenueAdapter);
                            locationVenueAdapter.notifyDataSetChanged();
                            ListUtils.setDynamicHeight_teammate(listView);

                            getPredictions(venue_name);*/

                            /*if(locationVenueModelArrayList.size() > 0) {
                                locationVenueAdapter = new LocationVenueAdapter(VenueListing.this, locationVenueModelArrayList);
                                listView.setAdapter(locationVenueAdapter);
                                locationVenueAdapter.notifyDataSetChanged();
                                ListUtils.setDynamicHeight_teammate(listView);
                            }*/

                        }catch (Exception exp)
                        {
                            exp.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyFroSearch(String venue_name) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "search_venues").newBuilder();
        urlBuilder.addQueryParameter("venue_name", venue_name);
        urlBuilder.addQueryParameter("sport_name", Constants.SPORTS_NAME);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Function to get Venue by search from server
     */
    public void getVenuesSearchListing(final String venue_id) {
        //Handling the loader state
        LoaderUtility.handleLoader(VenueListing.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodySearchListing(venue_id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(VenueListing.this, true);
                //Displaying the Error Message if the call get failed
                if (!ApplicationUtility.isConnected(VenueListing.this)) {
                    ApplicationUtility.displayNoInternetMessage(VenueListing.this);
                } else {
                    ApplicationUtility.displayErrorMessage(VenueListing.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                String id = "", image = "", name = "", latitude = "", longitude = "",
                        review_rating = "", review_count = "", price = "", court_type = "";
                boolean favourite_status = false;

                try {
                    venueModelArrayList = new ArrayList<>();
                    venue_listing_array = new JSONArray(responseData);

                    for (int i = 0; i < venue_listing_array.length(); i++) {
                        venueModel = new VenueModel();
                        images_arraylist = new ArrayList<String>();
                        JSONObject responseObject = venue_listing_array.getJSONObject(i);

                        if (responseObject.has("id") && !responseObject.isNull("id")) {
                            id = responseObject.getString("id");
                        }
                        if (responseObject.has("name") && !responseObject.isNull("name")) {
                            name = responseObject.getString("name");
                        }
                        if (responseObject.has("latitude") && !responseObject.isNull("latitude")) {
                            latitude = responseObject.getString("latitude");
                        }
                        if (responseObject.has("longitude") && !responseObject.isNull("longitude")) {
                            longitude = responseObject.getString("longitude");
                        }
                        if (responseObject.has("review_rating") && !responseObject.isNull("review_rating")) {
                            review_rating = responseObject.getString("review_rating");
                        }
                        if (responseObject.has("review_count") && !responseObject.isNull("review_count")) {
                            review_count = responseObject.getString("review_count");
                        }
                        if (responseObject.has("price") && !responseObject.isNull("price")) {
                            price = responseObject.getString("price");
                        }
                        if (responseObject.has("is_favorite") && !responseObject.isNull("is_favorite")) {
                            favourite_status = responseObject.getBoolean("is_favorite");
                        }

                        if (responseObject.has("image") && !responseObject.isNull("image")) {
                            JSONArray images_json_array = responseObject.getJSONArray("image");
                            for (int j = 0; j < images_json_array.length(); j++) {
                                images_arraylist.add(images_json_array.getString(j));
                            }
                            venueModel.setImages_array(images_arraylist);
                        }

                        //Getting the court distance from the user location
                        double distance = ApplicationUtility.distance(gpsTracker.getLatitude(), gpsTracker.getLongitude(),
                                Double.valueOf(latitude), Double.valueOf(longitude));

                        //Setting all the data in the model
                        venueModel.setVenueId(id);
                        venueModel.setVenueImage(image);
                        venueModel.setVenueName(name);
                        venueModel.setLatitude(latitude);
                        venueModel.setLongitude(longitude);
                        venueModel.setVenueRatings(review_rating);
                        venueModel.setReview_count(review_count);
                        venueModel.setVenuePrice(price);
                        venueModel.setFavourited(favourite_status);
                        venueModel.setMiles(distance);

                        //Adding the model into the Arraylist
                        venueModelArrayList.add(venueModel);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                VenueListing.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(VenueListing.this, false);

                        //Keeping vebue names inside spinner
                        setVenueAppletonType();

                        if (venue_listing_array != null) {
                            if (venue_listing_array.length() > 0) {

                                //Handling the views
                                enableViews(true);
                                //Enabling the
                                swipe_refresh_layout_venue.setVisibility(View.VISIBLE);
                                //Displaying the events in the page
                                venue_recycler_view.setVisibility(View.VISIBLE);
                                lay_no_event_found.setVisibility(View.GONE);

                                //Add data to Recycler view
                                setDataToRecyclerView();

                            } else {
                                //Displaying the no events in the page
                                swipe_refresh_layout_venue.setVisibility(View.GONE);
                                lay_no_event_found.setVisibility(View.VISIBLE);
                                venueModelArrayList = new ArrayList<VenueModel>();

                                if (Constants.VENUE_FILTER_BTN_CLICK) {
                                    venue_filter_icon.setFocusable(true);
                                    venue_filter_icon.setClickable(true);
                                    venue_location_icon.setFocusable(false);
                                    venue_location_icon.setClickable(false);
                                    Constants.VENUE_FILTER_BTN_CLICK = false;

                                    /**Display message with button if no results found in filter*/
                                    displayMessageButton();

                                } else {
                                    //Handling the views
                                    enableViews(false);
                                }
                            }
                        } else {
                            if (Constants.VENUE_FILTER_BTN_CLICK) {
                                venue_filter_icon.setFocusable(true);
                                venue_filter_icon.setClickable(true);
                                venue_location_icon.setFocusable(false);
                                venue_location_icon.setClickable(false);
                                Constants.VENUE_FILTER_BTN_CLICK = false;

                                /**Display message with button if no results found in filter*/
                                displayMessageButton();

                            } else {
                                //Handling the views
                                enableViews(false);
                            }
                        }
                    }
                });
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodySearchListing(String venue_id) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_courts").newBuilder();
        urlBuilder.addQueryParameter("venue_id", venue_id);
        urlBuilder.addQueryParameter("sport_name", Constants.SPORTS_NAME);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

//        Log.e(TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
//                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    /*@Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();
        setVenueAppletonType();
        LocationPickerManager.getCurrentCity(VenueListing.this);
        if(AppConstants.isLocationPicked_venue_listing){
            //AppConstants.isLocationPicked = false;
            AppConstants.isLocationPicked_venue_listing = false;
            getVenuesFilterListing();
        }
    }*/


    private void getPredictions(final CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, "" + constraint.toString(),
                                    BOUNDS_GREATER, autocompleteFilter);

            final AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    // if (mGoogleApiClient.isConnected()) {

                    locationUnregisteredVenueModels = new ArrayList<>();
                    //Log.i(TAG, "Executing autocomplete query for: " + constraint);


                    final Status status = autocompletePredictions.getStatus();
                    if (!status.isSuccess()) {
//                Toast.makeText(this, "Error: " + status.toString(),
//                        Toast.LENGTH_SHORT).show();
                        //Log.e(TAG, "Error getting place predictions: " + status.toString());
                        autocompletePredictions.release();
                        //return null;
                    }

                    Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();

                    //resultList = new ArrayList<>(autocompletePredictions.getCount());
                    while (iterator.hasNext()) {
                        AutocompletePrediction prediction = iterator.next();
                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                .getPlaceById(mGoogleApiClient, prediction.getPlaceId());
                        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(PlaceBuffer places) {
                                if (!places.getStatus().isSuccess()) {
                                    Log.e("ERROR", "Place query did not complete. Error: " +
                                            places.getStatus().toString());
                                    return;
                                } else {
                                    final Place place = places.get(0);
                                    locationUnregisteredVenueModel = new LocationUnregisteredVenueModel();

                                    String name = place.getName().toString();

                                    locationUnregisteredVenueModel.setVenue_Short_Address(name);
                                    locationUnregisteredVenueModel.setVenue_Full_Address(name);
                                    locationUnregisteredVenueModels.add(locationUnregisteredVenueModel);
                                }

                                //resultList.add(new PlaceAutocomplete(place.getId(), place.getName()));

                                locationUnregisteredVenueAdapter = new LocationUnregisteredVenueAdapter(VenueListing.this, locationUnregisteredVenueModels);
                                listView2.setAdapter(locationUnregisteredVenueAdapter);
                                locationUnregisteredVenueAdapter.notifyDataSetChanged();
                                ListUtils.setDynamicHeight_teammate(listView2);

                            }
                        });
                    }
                    autocompletePredictions.release();
                    //return resultList;
                    // }
                }
            }, 100);

            //Log.e(TAG, "Google API client is not connected.");
            //return null;
        }
    }
}
