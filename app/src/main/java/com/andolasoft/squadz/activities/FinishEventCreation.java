package com.andolasoft.squadz.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookDialog;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class FinishEventCreation extends AppCompatActivity {

    //@InjectView(R.id.layout)
    RelativeLayout layout;
    SnackBar snackBar;
    @InjectView(R.id.layout2)
    RelativeLayout layout2;
    @InjectView(R.id.fb_layout)
    RelativeLayout fb_layout;
    @InjectView(R.id.skip_layout)
    RelativeLayout skip_layout;
    @InjectView(R.id.back_icon_finish_event)
    ImageView iv_back;
    ShareDialog shareDialog;
    public static SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private CallbackManager callbackManager;
    private LoginManager manager;
    static String phnNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_finish_event_creation);
        ButterKnife.inject(this);

        iv_back.setVisibility(View.GONE);

        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.PLAYER_IDS_ARRAY_FROM_ADD_TEAMMATES.clear();
                Constants.TEAM_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
                Constants.PLAYER_IDS_FINAL_ARRAY_FROM_ADD_TEAMMATES.clear();
                Intent intent = new Intent(FinishEventCreation.this, AddTeammates.class);
                startActivity(intent);
            }
        });

        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deepLink = ApplicationUtility.
                        buildFirebaseDeepLinkingURL(EndPoints.BASE_URL_DEEP_LINKING+
                                "gameId="+AppConstants.GAME_ID);
                sendSMS("Join my "+ Summary.eventSport +" game on Squadz! "+ deepLink, FinishEventCreation.this);
            }
        });

        skip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Clearing all the values
                Summary.storeSharingValues();
                finishOpenActivities();
                FinishEventCreation.this.finish();
            }
        });


        fb_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                               // .setContentTitle("Squadz Game Posts")
                                .setContentDescription(Summary.eventSport + " on " + Summary.eventDate + " at "+ Summary.eventstartTIme + " to " + Summary.eventEndTime)
                                .setContentUrl(Uri.parse(EndPoints.BASE_URL_DEEP_LINKING+"game/"+AppConstants.GAME_ID))
                                //.setContentUrl(Uri.parse("http://34.192.117.108"))
                                .build();
                        ShareDialog shareDialog_new = new ShareDialog(FinishEventCreation.this);
                        shareDialog_new.show(linkContent, ShareDialog.Mode.NATIVE);
                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {
                                Toast.makeText(FinishEventCreation.this, "Shared your post successfully", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onCancel() {
                                //Toast.makeText(FinishEventCreation.this, "Cancel", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(FacebookException e) {
                                e.printStackTrace();
                                Toast.makeText(FinishEventCreation.this,  e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        snackBar.setSnackBarMessage("Please check your internet connection");
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                }
            }
        });
    }


    /*public void buildShareDialog(){
        ShareOpenGraphObject object = new ShareOpenGraphObject.Builder()
                .putString("og:type", "books.book")
                .putString("og:title", "A Game of Thrones")
                .putString("og:description", "In the frozen wastes to the north of Winterfell, sinister and supernatural forces are mustering.")
                .putString("books:isbn", "0-553-57340-3")
                .build();
        // Create an action
        ShareOpenGraphAction action = new ShareOpenGraphAction.Builder()
                .setActionType("books.reads")
                .putObject("book", object)
                .build();
        // Create the content
        ShareOpenGraphContent content = new ShareOpenGraphContent.Builder()
                .setPreviewPropertyName("Game")
                .setAction(action)
                .build();

        ShareDialog.show(FinishEventCreation.this, content);

        //ShareApi.share(content, null);
    }
*/
    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {
        layout = (RelativeLayout) findViewById(R.id.layout);
        layout2 = (RelativeLayout) findViewById(R.id.layout2);
        skip_layout = (RelativeLayout) findViewById(R.id.skip_layout);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        snackBar = new SnackBar(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        shareDialog = new ShareDialog(this);

        preferences = PreferenceManager
                .getDefaultSharedPreferences(FinishEventCreation.this);
        editor = preferences.edit();

        //Getting log in type from sign in page
        if (preferences.contains("LOGIN_TYPE")) {
            String user_login_type = preferences.getString("LOGIN_TYPE", "");
            if (!TextUtils.isEmpty(user_login_type) && user_login_type.equalsIgnoreCase("NORMAL_LOGIN")) {
                fb_layout.setClickable(true);
                fb_layout.setEnabled(true);
            } else {
                fb_layout.setClickable(true);
                fb_layout.setEnabled(true);
            }
        }
    }


    /**
     * Close Open Activities
     */
    public void finishOpenActivities() {
        if (Additionalinfo.additionalinfo != null) {
            Additionalinfo.additionalinfo.finish();
        }
        if (SkillLevel.skillLevel != null) {
            SkillLevel.skillLevel.finish();
        }
        if (Summary.summary != null) {
            Summary.summary.finish();
        }
        if (VenueListing.venueListing != null) {
            VenueListing.venueListing.finish();
        }
        if (VenueDetails.venueDetails != null) {
            VenueDetails.venueDetails.finish();
        }
        if (Availability.availability != null) {
            Availability.availability.finish();
        }
        if (VenueMapView.venueMapView != null) {
            VenueMapView.venueMapView.finish();
        }
        if (MyWishList.myWishList != null) {
            MyWishList.myWishList.finish();
        }
        if(CreateEventWithoutCourt.createEventWithoutCourt != null){
            CreateEventWithoutCourt.createEventWithoutCourt.finish();
        }
        FinishEventCreation.this.finish();
        if(EventDetail.isEventDetailsApiCalled){
            EventDetail.isEventDetailsApiCalled = false;
            AppConstants.userIdArray.clear();
            EventDetail.eventDetails.finish();
            AppConstants.userModelArary.clear();
            EventDetail.releseDeeLinkData();
            startActivity(new Intent(FinishEventCreation.this, SplashScreen.class));
        }
        if(!TextUtils.isEmpty(Constants.PUSH_NOTIFICATION_GAME_ID))
        {
            Constants.PUSH_NOTIFICATION_GAME_ID = "";
            if(NavigationDrawerActivity.baseActivity != null)
            {
                NavigationDrawerActivity.baseActivity.finish();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishOpenActivities();
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);

    }

/*
    *//**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     *//*
    public Request buildApiRequestBody() {
        JSONObject rootObject = null;
        try{
            rootObject = new JSONObject();
            JSONObject dynamic_link_info_obj = new JSONObject();
            dynamic_link_info_obj.put("dynamicLinkDomain","https://m8nyv.app.goo.gl/");
            dynamic_link_info_obj.put("link","My squadz team");
            rootObject.put("dynamicLinkInfo",dynamic_link_info_obj);
            JSONObject android_info = new JSONObject();
            android_info.put("androidPackageName",AppConstants.APP_PACKAGE_NAME);
            android_info.put("androidFallbackLink",AppConstants.APP_FALLBACK_LINK);
            android_info.put("androidMinPackageVersionCode",AppConstants.APP_MINiMUM_SUPPORTED_VERSION);
            dynamic_link_info_obj.put("androidInfo",android_info);
            JSONObject suffix_info = new JSONObject();
            suffix_info.put("option","SHORT");
            rootObject.put("suffix",suffix_info);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        Request request = new Request.Builder()
                .url(AppConstants.FIREBASE_DYNAMIC_LINK_BASE_URL)
                .method("POST", RequestBody.create(JSON, rootObject.toString()))
                .build();
        return request;
    }


    public void resetUserPassword(){

        // should be a singleton
        OkHttpClient client = new OkHttpClient();

        Request request = buildApiRequestBody();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                SplashScreen.displayMessageInUiThread(FinishEventCreation.this,e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    String status = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages

                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages

                    }else{

                }

                }catch(Exception ex){
                    ex.printStackTrace();

                }
                //Handling the loader state
            }
        });
    }*/


    /**
     * Send SMS
     */
    public static void  sendSMS(String deepLink, Context context) {
        initializeSharedPreferance(context);
        phnNumber = preferences.getString("user_phone", null);
        /*Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        //smsIntent.putExtra("address", phnNumber);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("sms_body", deepLink);
        context.startActivity(smsIntent);*/

        Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+""));
        intentsms.putExtra("sms_body", deepLink);
        context.startActivity(intentsms);
    }

    /**
     * Initializing the preferance
     * @param context
     */
    public static void initializeSharedPreferance(Context context){
        preferences = PreferenceManager
                .getDefaultSharedPreferences(context);
    }


    /**
     * Function responsible for sharing the firebase deep link through SMS
     * @param objectId
     * @param object_type
     * @param context
     */
    public static void shareDeepLinkUsingSms(String objectId,
                                             String object_type,
                                             Context context,
                                             String sport_type){
        //Sharing for the event
        if(object_type != null &&
                object_type.equalsIgnoreCase("event")){
            String deepLink = ApplicationUtility.
                    buildFirebaseDeepLinkingURL(EndPoints.BASE_URL_DEEP_LINKING+
                            "gameId="+ objectId);
            FinishEventCreation.sendSMS("Join my "+ sport_type +" game on Squadz! "+deepLink, context);
        }
        //Sharing for the listing
        else if(object_type != null &&
                object_type.equalsIgnoreCase("list")){
            String deepLink = ApplicationUtility.
                    buildFirebaseDeepLinkingURL(EndPoints.BASE_URL_DEEP_LINKING+
                            "listId="+ objectId);
            FinishEventCreation.sendSMS(deepLink, context);
        }
    }
}

