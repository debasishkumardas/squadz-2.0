package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.utils.ActionSheetDialog;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.OnOperItemClickL;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FacebookUserRegistrationStepOne extends AppCompatActivity {

    EditText customgame_et;
    @InjectView(R.id.username) EditText etUsername;
    @InjectView(R.id.choose_primary_sport) EditText et_choose_primary_sport;
    @InjectView(R.id.tv_done)
    TextView tvDone;
    @InjectView(R.id.btn_back)ImageView btnBack;
    String[] sportStringArray = null;
    ArrayList <String> sportNameArray = null;
    ArrayList <String> sportIdArray = null;
    String[] getSportName = new String[AppConstants.SPORT_ARRAY.length()];
    int clickedPosition = 0;
    public static String mSportId,mUsername, mPrimarySport;
    DatabaseAdapter db;
    SnackBar snackbar = new SnackBar(FacebookUserRegistrationStepOne.this);
    boolean isUserExist = false;
    public static FacebookUserRegistrationStepOne facebookSignuppageoneContext;
    SharedPreferences profileSp;
    SharedPreferences.Editor profileEditor;
    private String blockCharacterSet = "~#^|$%&*!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_new_fb_signup);
        ButterKnife.inject(this);

        //Blocking the special characters and spaces
        etUsername.setFilters(new InputFilter[] { filter });

        //getting the context
        facebookSignuppageoneContext = this;

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(FacebookUserRegistrationStepOne.this);

        //Handling the done button visibility
        //handleDoneButtonVIsibility(false);

        //Hiding the actionBar
        hideActionBar();

        //initializing the preferance
        profileSp = getSharedPreferences("SignupData", MODE_PRIVATE);
        profileEditor = profileSp.edit();

        //Initializing the database class
        db = new DatabaseAdapter(FacebookUserRegistrationStepOne.this);


        et_choose_primary_sport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_choose_primary_sport.setFocusable(true);
                //Displaying the dialog to select sport
                sportsDialog();
            }
        });


        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Checking the user existance
                checkUser();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Clearing all the facebook user values
                clearAllFacebookValues();
                FacebookUserRegistrationStepOne.this.finish();
                //clearAllFacebookValues();
            }
        });
    }

    /**
     * Hiding the ActionBar
     */
    public void hideActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.hide();
    }

    /***
     * display  list dialog
     ***/
    private void sportsDialog() {
        //sport = sportNameArray.toArray(sportNameArray);
        getSportName = getALlSportNames();
        final ActionSheetDialog dialog = new ActionSheetDialog(FacebookUserRegistrationStepOne.this,
                getSportName, null);
        dialog.title("Select Sport")
                .titleTextSize_SP(15.5f)
                .show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long _id) {
                dialog.dismiss();
                clickedPosition = position;
                String mgetSportName = getSportName[position];
                mSportId = db.getcurrentSportId(mgetSportName);
                if(mgetSportName != null &&
                        mgetSportName.equalsIgnoreCase("Custom")){
                    openDialog();
                }else{
                    et_choose_primary_sport.setText(mgetSportName);
                    //Calling the API to validate the user
                    checkUser();
                }



            }
        });

    }

    private void openDialog(){
        LayoutInflater inflater = LayoutInflater.from(FacebookUserRegistrationStepOne.this);
        View subView = inflater.inflate(R.layout.dialog_custom_gamename, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialog_edittext);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Custom Game Name");
        builder.setMessage("");
        builder.setView(subView);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String custom_game_name = subEditText.getText().toString();
                if(!TextUtils.isEmpty(custom_game_name)){
                    et_choose_primary_sport.setText(custom_game_name);
                    checkUser();
                }else{
                    snackbar.setSnackBarMessage("Please enter name for your custom game");
                }


            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    /**
     * Getting all the Sports
     * @return
     */
    public String[] getALlSportNames(){
        sportStringArray = new String[AppConstants.SPORT_ARRAY.length() - 1];
        sportNameArray = new ArrayList<>();
        sportIdArray = new ArrayList<>();
        for(int i = 0; i < AppConstants.SPORT_ARRAY.length(); i++){
            try{
                JSONObject sportObject = AppConstants.SPORT_ARRAY.getJSONObject(i);
                String sportName = sportObject.getString("name");
                String sportId = sportObject.getString("id");
                if(sportName != null &&
                        !sportName.equalsIgnoreCase("None") &&
                        !sportName.equalsIgnoreCase("All")){
                    sportNameArray.add(sportName);
                    sportIdArray.add(sportId);
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }


        }
        return sportStringArray = sportNameArray.toArray(sportStringArray);
    }


    /**
     * Validating the username and calling the API
     * to check user existance in teh server.
     */
    public void checkUser(){
        mUsername = etUsername.getText().toString();
        mPrimarySport = et_choose_primary_sport.getText().toString().trim();
        if(TextUtils.isEmpty(mUsername)){
            Toast.makeText(FacebookUserRegistrationStepOne.this,"Please Enter Username to Proceed", Toast.LENGTH_LONG).show();
        }else if(TextUtils.isEmpty(mPrimarySport)){
            Toast.makeText(FacebookUserRegistrationStepOne.this,"Please Select Your Primary Sport to Proceed", Toast.LENGTH_LONG).show();
        } else{
            checkUserName(mUsername);
        }
    }


    /**
     * Function to Check username Existance
     * @param userName
     */
    public void checkUserName(final String userName){

        final ProgressDialog dialog = ProgressDialog
                .show(FacebookUserRegistrationStepOne.this, "", "Please wait ..");
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setCancelable(false);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(userName);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displaying the Error message to the user
                SplashScreen.displayMessageInUiThread(FacebookUserRegistrationStepOne.this, e.toString());
                dialog.dismiss();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    //Log.d("Response",responseObject.toString());
                    String status = responseObject.getString("status");
                    if(status != null && status.equalsIgnoreCase("Success")) {
                        isUserExist = responseObject.getBoolean("is_available");
                    } else if(status != null && status.equalsIgnoreCase("error")) {

                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                }

                //Handling the loader state
                FacebookUserRegistrationStepOne.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        if(!isUserExist){
                            snackbar.setSnackBarMessage("UserName Already Taken. Please choose another one");
                           // handleDoneButtonVIsibility(true);
                        }else{
                            profileEditor.putString(Constants.PARAM_USERNAME, userName);
                            profileEditor.putString(Constants.PARAM_PRIMATY_SPORT,
                                    mPrimarySport);
                            profileEditor.commit();
                            AppConstants.USER_LOGIN_TYPE_FACEBOOK = true;
                            Intent intent = new Intent(FacebookUserRegistrationStepOne.this,NormalRegistrationStepTwo.class);
                            startActivity(intent);
                        }
                    }
                });
            }
        });
    }

    /**
     * Building the APi request
     * @param user_name
     * @return
     */

    public Request buildApiRequestBody(String user_name){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"check_username?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, user_name);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to hand;e the Button Visibiliy
     * @param status
     */
    public void handleDoneButtonVIsibility(boolean status){
        if(status){
            tvDone.setVisibility(View.VISIBLE);
        }else{
            tvDone.setVisibility(View.GONE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start,
                                   int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        clearAllFacebookValues();
    }

    //Clearing all the user facebook values
    public static void clearAllFacebookValues(){
        SigninActivity.fb_user_firstname = null;
        SigninActivity.fb_user_lastname = null;
        SigninActivity.fb_user_name = null;
        SigninActivity.fb_user_email = null;
        SigninActivity.fb_user_image = null;
        SigninActivity.fb_user_id = null;
    }


}
