package com.andolasoft.squadz.activities;


import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.drawable.StateListDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.HomeFragment;
import com.andolasoft.squadz.fragments.TabFragment;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.FriendsModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.adapters.NavigationDrawerAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class NavigationDrawerActivity extends AppCompatActivity {
    public static Toolbar toolbar;
    public static TextView toolbartitle, toolbardate, app_version_name;
    public static NavigationDrawerActivity baseActivity;
    SharedPreferences.Editor editor;
    private DrawerLayout mDrawerLayout;
    public static ListView mDrawerList;
    public static ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    public static String[] mPlanetTitles,mPlanetTitles_facebook;
    private long lastBackPressTime = 0;
    private Spinner spinner_game_names, spinner_appleton;
    SharedPreferences preferences;
    SharedPreferences privacy_preferences;
    public static SharedPreferences.Editor profileEditor;
    SharedPreferences.Editor privacyEditor;
    private TextView tv_user_name, tv_phone_number, tv_email;
    CircleImageView imgv_profile_image;
    GPSTracker gpsTracer;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    ProgressDialog dialog;
    RelativeLayout profile_main_layout;
    public static String user_id = "",auth_token = "",rating_value = "";
    MaterialDialog mMaterialDialog;
    DatabaseAdapter db;
    SnackBar snackBar;
    SharedPreferences pref;
    Fragment fragment = null;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_base);
        baseActivity = this;

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(NavigationDrawerActivity.this);

        mTitle = mDrawerTitle = getTitle();
        // mPlanetTitles = getResources().getStringArray(R.array.nav_drawer_labels);
        mPlanetTitles = getResources().getStringArray(R.array.nav_drawer_labels_after_login);
        mPlanetTitles_facebook = getResources().getStringArray(R.array.nav_drawer_labels_facebook_login);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        toolbartitle = (TextView) findViewById(R.id.titletool);
        spinner_game_names = (Spinner) findViewById(R.id.spinner_sports_names);
        spinner_appleton = (Spinner) findViewById(R.id.spinner_appleton);


        toolbar.setBackgroundColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        db = new DatabaseAdapter(this);
        snackBar = new SnackBar(this);
        this.preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.profileEditor = this.preferences.edit();

        /**
         * KEEPING GAMES NAME INSIDE THE SPINNER
         */
        setGamesType();

        /**
         * KEEPING APPLETON NAME INSIDE THE SPINNER
         */
        setAppletonType();

        //toolbardate.setText("Home");

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header,
                mDrawerList, false);

        tv_user_name = (TextView) header.findViewById(R.id.user_name);
        tv_phone_number = (TextView) header.findViewById(R.id.phone_number);
        tv_email = (TextView) header.findViewById(R.id.email_id);
        imgv_profile_image = (CircleImageView) header.findViewById(R.id.profile_image);
        profile_main_layout = (RelativeLayout) header.findViewById(R.id.profile_main_layout);
        TextView edit_text = (TextView) header.findViewById(R.id.edit_text);
        mDrawerList.addHeaderView(header, null, false);


        if(preferences.contains("USERNAME")) {
            String user_Name = preferences.getString("USERNAME","");
            tv_user_name.setText(user_Name);
        }

        setUserData();


        profile_main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(mDrawerList);
//                Intent intent = new Intent(NavigationDrawerActivity.this,AddTeammates.class);
//                startActivity(intent);
               // Intent intent = new Intent(NavigationDrawerActivity.this,CreateEventWithoutCourt.class);
               // startActivity(intent);
            }
        });

        edit_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NavigationDrawerActivity.this, EditUserProfile.class);
                startActivity(intent);
            }
        });

        /*edit_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

//        LayoutInflater inflater2 = getLayoutInflater();
//        ViewGroup header2 = (ViewGroup) inflater2.inflate(R.layout.footer_view,
//                mDrawerList, false);
//        mDrawerList.addFooterView(header2, null, false);


//        font_awesome_edit_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(NavigationDrawerActivity.this, EditProfile.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                startActivity(intent);
//                finish();
//            }
//        });


       /* NavigationDrawerAdapter baseActivityAdapter = new NavigationDrawerAdapter(this, mPlanetTitles, "LOGGED_IN");
        mDrawerList.setAdapter(baseActivityAdapter);*/

        //Getting the application version from Manifest
        try {
            String app_version = NavigationDrawerActivity.this.getPackageManager().getPackageInfo(
                    this.getPackageName(), 0).versionName;
            //app_version_name.setText("GreasePay v" + app_version);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {

            }

            public void onDrawerOpened(View drawerView) {

            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        // toolbartitle.setText("Nav");
        mDrawerToggle.syncState();

        if (savedInstanceState == null) {
            selectItem(0);
        }

    }

    private void selectItem(int position) {

        // Load your conten here
        displayView(position);
        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, false);
        mDrawerLayout.closeDrawer(mDrawerList);

    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    //Getting user Login Type
    public String getUserLoginType(){
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String login_type = preferences.getString("LOGIN_TYPE", "");
        return login_type;
    }

    private void displayView(int position) {
        if(getUserLoginType() != null &&
                !getUserLoginType().equalsIgnoreCase("FACEBOOK_LOGIN")) {

            switch (position) {
                case 0:
                    Constants.MY_SPORTS_CLICKED = false;
                    /**
                     *  Condition Implementation to handle the Push Notifivcation
                     *  and Deep Link page redirection
                     */
                    if (AppConstants.isDeepLinkRecieved) {
                        NavigationDrawerActivity.this.finish();
                        startActivity(new Intent(NavigationDrawerActivity.this, EventDetail.class));
                    } else if (AppConstants.isListDeepLinkRecieved) {
                        NavigationDrawerActivity.this.finish();
                        startActivity(new Intent(NavigationDrawerActivity.this, VenueDetails.class));
                    } else if (AppConstants.CHAT_CHANNEL_ID_DUPLICATE != null) {
                        NavigationDrawerActivity.this.finish();
                        AppConstants.isChatMessageRecieveedInBackground = true;
                        startActivity(new Intent(NavigationDrawerActivity.this, ChatActivity.class));
                    } else if (!TextUtils.isEmpty(Constants.PUSH_NOTIFICATION_GAME_ID)) {
                        NavigationDrawerActivity.this.finish();
                        startActivity(new Intent(NavigationDrawerActivity.this, EventDetail.class));
                    } else if (Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND) {
                        //NavigationDrawerActivity.this.finish();
                        fragment = new TabFragment();
                    } else if (Constants.FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY) {
                        Constants.FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY = false;
                        NavigationDrawerActivity.this.finish();
                        startActivity(new Intent(NavigationDrawerActivity.this, TeamMates.class));
                    } else {
                        fragment = new TabFragment();
                        setTitle("");
                    }
                    break;

                case 1:
                    /**Open Upcoming page*/
                    Constants.MY_SPORTS_CLICKED = true;
                    fragment = new TabFragment();
                    setTitle("");
                    break;

                case 2:
                    startActivity(new Intent(NavigationDrawerActivity.this, TeamMates.class));
                    setTitle("");
                    break;
                case 3:
                    startActivity(new Intent(NavigationDrawerActivity.this, StoredPaymentCardsScreen.class));
                    setTitle("");
                    break;
                case 4:
//                clearStoredValues();
                    Intent intent3 = new Intent(NavigationDrawerActivity.this, MapRadius.class);
                    startActivity(intent3);

//                clearStoredValues();
               /* Intent intent3 = new Intent(NavigationDrawerActivity.this, MapRadius.class);
                startActivity(intent3);*/

                    setTitle("");
                    break;
                case 5:
                    Intent inten = new Intent(NavigationDrawerActivity.this, MyWishList.class);
                    //Intent inten = new Intent(NavigationDrawerActivity.this, FinishEventCreation.class);
                    startActivity(inten);
                    setTitle("");
                    break;
                case 6:
                    setTitle("");
                    Intent intent_privacy = new Intent(NavigationDrawerActivity.this, Privacy.class);
                    startActivity(intent_privacy);
                    break;
                case 7:
                    if (getUserLoginType() != null &&
                            !getUserLoginType().equalsIgnoreCase("FACEBOOK_LOGIN")) {
                        setTitle("");
                        Intent intent4 = new Intent(NavigationDrawerActivity.this, ChangePassword.class);
                        startActivity(intent4);
                    }
                    break;
                case 8:
                    setTitle("");
                    Intent intent_settings = new Intent(NavigationDrawerActivity.this, Settings.class);
                    startActivity(intent_settings);
                    break;
                case 9:
                    setTitle("");
                    Intent intent_faq = new Intent(NavigationDrawerActivity.this, FAQ.class);
                    startActivity(intent_faq);
                    break;
                case 10:
                    setTitle("");
                    Intent intent_terms = new Intent(NavigationDrawerActivity.this, Terms.class);
                    startActivity(intent_terms);
                    break;
                case 11:

                    /**Clear Logged user FCM Id from server for Push notification*/
                    clearLoggedUserFCMId(user_id);

                    break;
                default:
                    break;
            }
        }
        else{
            display_Facebook_Items(position);
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.commit();
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == HomeFragment.REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
               // Toast.makeText(this, "GPS enabled", Toast.LENGTH_LONG).show();
                //Getting the Current Location
                getCurrentLocation();
            } else {
                //Toast.makeText(NavigationDrawerActivity.this, "GPS is not enabled", Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == HomeFragment.PERMISSION_REQUEST_CODE){
            //Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show();
        }


        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Google", "Place Selected: " + place.getName());
                HomeFragment.address = place.getName().toString();
                String latLng = place.getLatLng().toString();
                String[] latlngArr = latLng.split(" ");
                String latlngVal = latlngArr[1].substring(1, latlngArr[1].length()-1);
                String[] finalArray =  latlngVal.split(",");
                HomeFragment.currLocLatitude = Double.parseDouble(finalArray[0].toString());
                HomeFragment.currLocLongitude = Double.parseDouble(finalArray[1].toString());
                AppConstants.isLocationPicked = true;
                AppConstants.isLocationPicked_venue_listing = true;
                AppConstants.isLocationPicked_venue_mapview = true;
                HomeFragment.getAddress();
                getJoinGameAvailibity();
                CharSequence attributions = place.getAttributions();
                if (!TextUtils.isEmpty(attributions)) {
                    //mPlaceAttribution.setText(Html.fromHtml(attributions.toString()));
                } else {
                    //mPlaceAttribution.setText("");
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("Google", "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }


    }


    /**
     * Function to get the address from latitude and longitude
     */
    public void getAddress(){

        try{
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(NavigationDrawerActivity.this, Locale.getDefault());
            addresses = geocoder.getFromLocation(HomeFragment.currLocLatitude, HomeFragment.currLocLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            HomeFragment.address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            HomeFragment.current_city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
           // Toast.makeText(this,"venue called", Toast.LENGTH_LONG).show();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Function Responsible for proving the current location latitude and longitude
     */
    public void getCurrentLocation(){
        gpsTracer = new GPSTracker(NavigationDrawerActivity.this);
        if(gpsTracer.canGetLocation()) {
            HomeFragment.currLocLatitude = gpsTracer.getLatitude();
            HomeFragment.currLocLongitude = gpsTracer.getLongitude();
        }
        getAddress();
        //Toast.makeText(this,"address captured", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(NavigationDrawerActivity.baseActivity != null) {
            NavigationDrawerActivity.baseActivity.finish();
        }
        if(EventDetail.eventDetails != null) {
            EventDetail.eventDetails.finish();
        }
        NavigationDrawerActivity.baseActivity.finish();
    }

    /**
     * KEEPING GAMES NAME INSIDE THE SPINNER
     */
    public void setGamesType() {
        ArrayList<String> games_array = new ArrayList<>();
        games_array.add("Basketball");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(NavigationDrawerActivity.this, android.R.layout.simple_spinner_item, games_array);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_game_names.setAdapter(dataAdapter);
        spinner_game_names.setSelection(0);
    }

    /**
     *
     * KEEPING APPLETON NAME INSIDE THE SPINNER
     */
    public void setAppletonType() {
        ArrayList<String> appleton_array = new ArrayList<>();
        appleton_array.add("Appleton");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(NavigationDrawerActivity.this, android.R.layout.simple_spinner_item, appleton_array);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_appleton.setAdapter(dataAdapter);
        spinner_appleton.setSelection(0);
    }


    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            selectItem(position - 1);
            mDrawerList.setSelector(new StateListDrawable());

        }
    }

    /**
     * Function to clear all the stored values
     */
    public void clearStoredValues(){
        profileEditor.clear();
        profileEditor.commit();
    }

    public Runnable openDrawerRunnable(final boolean drawerOpen) {
        return new Runnable() {

            @Override
            public void run() {
				/*
				 * mDrawerLayout.openDrawer(Gravity.LEFT);
				 *
				 * mDrawerLayout.closeDrawer(mDrawerList);
				 */

                if (drawerOpen) {

                    boolean drawerOpenStaus = mDrawerLayout
                            .isDrawerOpen(mDrawerList);
                    if (drawerOpenStaus) {
                        mDrawerLayout.closeDrawer(mDrawerList);
                    } else {
                        mDrawerLayout.openDrawer(Gravity.LEFT);
                    }
                } else {
                    boolean drawerOpenStaus = mDrawerLayout
                            .isDrawerOpen(mDrawerList);
                    if (drawerOpenStaus) {
                        mDrawerLayout.closeDrawer(mDrawerList);
                    }
                }
            }
        };
    }

    public void setUserData(){
        if(preferences.contains("USERNAME")) {
            String first_name = preferences.getString("firstname","");
            String last_name = preferences.getString("lastname","");
            String userEmail = preferences.getString("user_email","");
            String userPhone = preferences.getString("user_phone","");
            String userName = preferences.getString("USERNAME","");
            user_id = preferences.getString("loginuser_id","");
            auth_token = preferences.getString("auth_token","");
            String image = preferences.getString("image","");

            if(TextUtils.isEmpty(first_name) ||
                    TextUtils.isEmpty(last_name) ||
                    first_name.equalsIgnoreCase("null") ||
                    last_name.equalsIgnoreCase("null")){
                tv_user_name.setText(userName);
            }else{
                tv_user_name.setText(first_name + " " +last_name);
            }
            tv_phone_number.setText(userPhone);
            tv_email.setText(userEmail);

            if(!TextUtils.isEmpty(image) &&
                    image != null &&
                    !image.equalsIgnoreCase("null")){
                Glide.with(NavigationDrawerActivity.this).load(image).into(imgv_profile_image);
            }

            /**Displaying rating dialog after game is over*/
            displayRatingDialog(user_id);
        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    /**
     * Function Responsible for the API call for the game Availibility
     */
    public void getJoinGameAvailibity() {
        //Handling the loader state
        LoaderUtility.handleLoader(NavigationDrawerActivity.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildGameVerifyApiRequestBody();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(NavigationDrawerActivity.this, false);
                if(!ApplicationUtility.isConnected(NavigationDrawerActivity.this)){
                    ApplicationUtility.displayNoInternetMessage(NavigationDrawerActivity.this);
                }else{
                    ApplicationUtility.displayErrorMessage(NavigationDrawerActivity.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    HomeFragment.isGameAvailable = responseObject.getBoolean("is_game_available");
                }catch(Exception ex){
                    ex.printStackTrace();
                }


                NavigationDrawerActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(NavigationDrawerActivity.this, false);

                        //Handling the API response
                        if(HomeFragment.isGameAvailable){
                            //Setting the back grond image of the image view
                            HomeFragment.join_game_icon.setBackgroundResource(R.mipmap.img_join_game_updated);
                            //Enabling the card view
                            HomeFragment.card_view_join_game.setEnabled(true);
                            //Enabling the card view for click events
                            HomeFragment.card_view_join_game.setClickable(true);
                            HomeFragment.join_game_icon.setClickable(true);
                            HomeFragment.join_game_icon.setEnabled(true);
                            HomeFragment.join_game_icon.setFocusable(true);
                            HomeFragment.join_layout.setFocusable(true);
                            HomeFragment.join_layout.setClickable(true);
                            HomeFragment.join_layout.setEnabled(true);
                            HomeFragment.lay_placeholder.setVisibility(View.GONE);
                        }else{
                            //Setting the back grond image of the image view
                            HomeFragment.join_game_icon.setBackgroundResource(R.drawable.ic_join_game);
                            //Disabling the card view
                            HomeFragment.card_view_join_game.setEnabled(false);
                            //Disabling the card view for click events
                            HomeFragment.card_view_join_game.setClickable(false);
                            HomeFragment.join_game_icon.setClickable(false);
                            HomeFragment.join_game_icon.setEnabled(false);
                            HomeFragment.join_game_icon.setFocusable(false);
                            HomeFragment.join_layout.setFocusable(false);
                            HomeFragment.join_layout.setClickable(false);
                            HomeFragment.join_layout.setEnabled(false);
                            HomeFragment.lay_placeholder.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });
    }



    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildGameVerifyApiRequestBody() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "verify_game_created").newBuilder();
        urlBuilder.addQueryParameter("location", HomeFragment.currLocLatitude+","+ HomeFragment.currLocLongitude);
        urlBuilder.addQueryParameter("current_city", HomeFragment.current_city);
        urlBuilder.addQueryParameter("city", HomeFragment.current_location_city);
        urlBuilder.addQueryParameter("state", HomeFragment.current_location_state);
        urlBuilder.addQueryParameter("country", HomeFragment.current_location_country);
        urlBuilder.addQueryParameter("sport_name", Constants.SPORTS_NAME);
        urlBuilder.addQueryParameter("current_time", ApplicationUtility.getDeviceCurrentTime());
        urlBuilder.addQueryParameter("date", ApplicationUtility.getDeviceCurrentDate());
        urlBuilder.addQueryParameter("email", HomeFragment.user_email);
        urlBuilder.addQueryParameter("user_id", HomeFragment.user_id);
        urlBuilder.addQueryParameter("user_profile_image", HomeFragment.user_profile_image);
        urlBuilder.addQueryParameter("user_name", HomeFragment.user_name);
        urlBuilder.addQueryParameter("device_type", Constants.DEVICE_TYPE);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }


    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(NavigationDrawerActivity.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Fetching Radius,wishlist & Card count
     */
    /**
     * API integration for all counts
     */
    public void getCountAPI(String user_id, String auth_token) {
        //Handling the loader state
        //handleLoader(true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(user_id,auth_token);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                if(!ApplicationUtility.isConnected(NavigationDrawerActivity.this)){
                    ApplicationUtility.displayNoInternetMessage(NavigationDrawerActivity.this);
                }else{
                    ApplicationUtility.displayErrorMessage(NavigationDrawerActivity.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();

                String radius = "0",cards = "0",wish_lists_count = "0";
                try {

                    JSONObject radius_jsonObject = new JSONObject(responseData);
                    JSONObject user_json_object = radius_jsonObject.getJSONObject("user_info");

                    if(user_json_object.has("radius") && !user_json_object.isNull("radius")) {
                        radius = user_json_object.getString("radius");
                    }
                    if(user_json_object.has("cards") && !user_json_object.isNull("cards")) {
                        cards = user_json_object.getString("cards");
                    }
                    if(user_json_object.has("wish_lists_count") && !user_json_object.isNull("wish_lists_count")) {
                        wish_lists_count = user_json_object.getString("wish_lists_count");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalRadius = radius;
                final String finalCards = cards;
                final String finalWish_lists_count = wish_lists_count;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        //handleLoader(false);
                        if(getUserLoginType() != null &&
                                !getUserLoginType().equalsIgnoreCase("FACEBOOK_LOGIN")) {

                            if (finalRadius.equalsIgnoreCase("0")) {
                                NavigationDrawerAdapter baseActivityAdapter = new NavigationDrawerAdapter(NavigationDrawerActivity.this, mPlanetTitles, "LOGGED_IN", "10", finalCards, finalWish_lists_count);
                                mDrawerList.setAdapter(baseActivityAdapter);
                            } else {
                                NavigationDrawerAdapter baseActivityAdapter = new NavigationDrawerAdapter(NavigationDrawerActivity.this, mPlanetTitles, "LOGGED_IN", finalRadius, finalCards, finalWish_lists_count);
                                mDrawerList.setAdapter(baseActivityAdapter);
                            }
                        }
                        else{
                            if (finalRadius.equalsIgnoreCase("0")) {
                                NavigationDrawerAdapter baseActivityAdapter = new NavigationDrawerAdapter(NavigationDrawerActivity.this, mPlanetTitles_facebook, "FACEBOOK_LOGIN", "10", finalCards, finalWish_lists_count);
                                mDrawerList.setAdapter(baseActivityAdapter);
                            } else {
                                NavigationDrawerAdapter baseActivityAdapter = new NavigationDrawerAdapter(NavigationDrawerActivity.this, mPlanetTitles_facebook, "FACEBOOK_LOGIN", finalRadius, finalCards, finalWish_lists_count);
                                mDrawerList.setAdapter(baseActivityAdapter);
                            }
                        }

                        //Store counts for Radius, Billings (Card listing) & Wishlist
                        setCountDatainPreferences(finalRadius, finalCards, finalWish_lists_count);

                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String user_id, String auth_token) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_profile_info").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("user_id", user_id);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Fetching Radius,wishlist & Card count
     */
    /**
     * API integration for all counts
     */
    public void clearLoggedUserFCMId(String user_id) {
        //Handling the loader state
        LoaderUtility.handleLoader(NavigationDrawerActivity.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForClearUserInfo(user_id);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                //Handling the loader state
                LoaderUtility.handleLoader(NavigationDrawerActivity.this, false);
                //handleLoader(false);
                if(!ApplicationUtility.isConnected(NavigationDrawerActivity.this)){
                    ApplicationUtility.displayNoInternetMessage(NavigationDrawerActivity.this);
                }else{
                    ApplicationUtility.displayErrorMessage(NavigationDrawerActivity.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();

                String status = "",message = "";
                try {

                    JSONObject user_jsonObject = new JSONObject(responseData);

                    if(user_jsonObject.has("status") && !user_jsonObject.isNull("status")) {
                        status = user_jsonObject.getString("status");
                    }
                    if(user_jsonObject.has("message") && !user_jsonObject.isNull("message")) {
                        message = user_jsonObject.getString("message");
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalStatus = status;
                final String finalMessage = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(NavigationDrawerActivity.this, false);
                        if(!TextUtils.isEmpty(finalMessage) &&
                                finalMessage.equalsIgnoreCase("Successfully cleared data.")) {
                            clearStoredValues();
                            removePrivacyData();
                            SigninActivity.releaseValues();
                            DatabaseAdapter db = new DatabaseAdapter(NavigationDrawerActivity.this);
                            db.deleteAllData(NavigationDrawerActivity.this);
                            NavigationDrawerActivity.this.finish();
                            Constants.PUSH_NOTIFICATION_GAME_ID = "";

                            //Clearing all the notification from the notification tray
                            clearNotifications(NavigationDrawerActivity.this);
                            Intent intent_venue_details = new Intent(NavigationDrawerActivity.this, SigninActivity.class);
                            startActivity(intent_venue_details);
                            //Toast.makeText(NavigationDrawerActivity.this, ""+ finalMessage, Toast.LENGTH_SHORT).show();
                        } else{
                            SigninActivity.releaseValues();
                            Toast.makeText(NavigationDrawerActivity.this, ""+ finalMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }


    /**
     * Fubnction responsible for clearing all the notifications from the system tray
     */
    public static void clearNotifications(Activity activity){
        NotificationManager notificationManager =
                (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForClearUserInfo(String user_id) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "clear_user").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("device_type", Constants.DEVICE_TYPE);
        urlBuilder.addQueryParameter("firebase_registration_id", getFcmId());
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Storing the Fcm_id in the temp storage
     */
    public String getFcmId(){
        SharedPreferences firebase_token_preferences =
                getSharedPreferences("Firebase_Token", Context.MODE_PRIVATE);
        String fcmId = firebase_token_preferences.getString("Fcm_id", null);
        return fcmId;
    }

    /**
     * Store counts for Radius, Billings (Card listing) & Wishlist
     */
    public void setCountDatainPreferences(String radius_count, String billing_count,
                                          String wishlist_count) {
        if(radius_count.equalsIgnoreCase("0")) {
            profileEditor.putString("RADIUS_COUNT","10");
        } else{
            profileEditor.putString("RADIUS_COUNT",radius_count);
        }
        profileEditor.putString("BILLING_COUNT",billing_count);
        profileEditor.putString("WISHLIST_COUNT",wishlist_count);
        profileEditor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(Constants.FAVOURITE_CLICKED) {
            Constants.FAVOURITE_CLICKED = false;
            getCountAPI(user_id,auth_token);
        } else if(preferences.contains("RADIUS_COUNT")) {
            String radius_count = preferences.getString("RADIUS_COUNT","");
            String billing_count = preferences.getString("BILLING_COUNT","");
            String wishlist_count = preferences.getString("WISHLIST_COUNT","");

            if(getUserLoginType() != null &&
                    !getUserLoginType().equalsIgnoreCase("FACEBOOK_LOGIN")) {

                NavigationDrawerAdapter baseActivityAdapter = new NavigationDrawerAdapter(NavigationDrawerActivity.this, mPlanetTitles, "LOGGED_IN", radius_count, billing_count, wishlist_count);
                mDrawerList.setAdapter(baseActivityAdapter);
            }
            else{
                NavigationDrawerAdapter baseActivityAdapter = new NavigationDrawerAdapter(NavigationDrawerActivity.this, mPlanetTitles_facebook, "FACEBOOK_LOGIN", radius_count, billing_count, wishlist_count);
                mDrawerList.setAdapter(baseActivityAdapter);
            }


        } else{
            getCountAPI(user_id,auth_token);
        }


        if(EditUserProfile.isProfileUpdated){
            EditUserProfile.isProfileUpdated = false;
            //Setting the user data
            setUserData();
        }

    }


    /**
     * Function responsible for clearing all the temp. Storage Data
     */

    public void removePrivacyData(){
        SharedPreferences sharedpreferences = getSharedPreferences("Privacy_Data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorPrivacy = getSharedPreferences("Privacy_Data", MODE_PRIVATE).edit();

        SharedPreferences preferences = getSharedPreferences("Settings_Data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        SharedPreferences add_playerpreferences = getSharedPreferences("AddPlayerPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor add_playereditor = add_playerpreferences.edit();

        editorPrivacy.clear();
        editorPrivacy.apply();
        editor.clear();
        editor.apply();
        add_playereditor.clear();
        add_playereditor.apply();
    }

    /**
     * Displaying rating dialog after game is over
     */
    public void displayRatingDialog(String user_id) {
        String name = "",list_id = "",event_id = "",
                object_type = "",game_end_time = "";

        Cursor cursor = db.getGameInfoForReview(user_id, ApplicationUtility.currentFormmatedDate());

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    name = cursor.getString(cursor
                            .getColumnIndex("name"));
                    list_id = cursor.getString(cursor
                            .getColumnIndex("list_id"));
                    event_id = cursor.getString(cursor
                            .getColumnIndex("event_id"));
                    object_type = cursor.getString(cursor
                            .getColumnIndex("object_type"));
                    game_end_time = cursor.getString(cursor
                            .getColumnIndex("game_end_time")); //Date & Time

                } while (cursor.moveToNext());
            }

            //long game_creation_time_in_milisecond = getGameDateTimeInMilisecond(game_end_time);
            long current_Time= System.currentTimeMillis();

            if(Long.valueOf(game_end_time) <= current_Time) {
                if(!TextUtils.isEmpty(object_type) && (object_type.equalsIgnoreCase("event")
                                                   || object_type.equalsIgnoreCase("Event"))) {
                    ratingDialog(name,event_id,object_type);
                } else{
                    ratingDialog(name,list_id,object_type);
                }
            }
        }
    }
    /**
     * Displaying dialog for giving rating after game is over
     */
    public void ratingDialog(String game_name, final String list_id, final String object_type){
        mMaterialDialog = new MaterialDialog(NavigationDrawerActivity.this);
        View view = LayoutInflater.from(NavigationDrawerActivity.this)
                .inflate(R.layout.future_rating_dialog_layout,
                        null);

        TextView tv_skip_rating = (TextView) view.findViewById(R.id.tv_skip_rating);
        TextView tv_submit_rating = (TextView) view.findViewById(R.id.tv_submit_rating);
        TextView tv_game_name_rating = (TextView) view.findViewById(R.id.tv_game_name_rating);
        final EditText reviews_description_rating = (EditText) view.findViewById(R.id.reviews_description_rating);
        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.future_ratingbar);

        tv_game_name_rating.setText(game_name);

        /**Rating bar change listener*/
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if(rating<1.0f) {
                    ratingBar.setRating(1.0f);
                }
                String ratings = String.valueOf(ratingBar.getRating());
                if(ratings.contains(".0")) {
                    DecimalFormat decimalFormat=new DecimalFormat("#.#");
                    rating_value = String.valueOf(decimalFormat.format(ratingBar.getRating()));
                } else{
                    rating_value = ratings;
                }
            }
        });

        tv_skip_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();

                /**Remove recods from Rating table*/
                db.deleteRatingsInfo();
            }
        });

        tv_submit_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // mMaterialDialog.dismiss();

                String get_reviews_description = reviews_description_rating.getText().toString().trim();
                String ratings = String.valueOf(ratingBar.getRating());

                if(ratings.equalsIgnoreCase("0.0") || ratings.equalsIgnoreCase("0"))
                {
                    snackBar.setSnackBarMessage("Rating should not be empty");
                }
                else{
                    saveReview(user_id, list_id, rating_value,
                            get_reviews_description, object_type);
                }
            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(false);
        mMaterialDialog.setView(view).show();
    }

    /**
     * API integration for Add Review
     */
    public void saveReview(String userId, String list_id, String rating,
                           String review, String object_type) {
        //Handling the loader state
        LoaderUtility.handleLoader(NavigationDrawerActivity.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForAddReview(userId, list_id, rating, review,object_type);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(NavigationDrawerActivity.this, false);
                if(!ApplicationUtility.isConnected(NavigationDrawerActivity.this)){
                    ApplicationUtility.displayNoInternetMessage(NavigationDrawerActivity.this);
                }else{
                    ApplicationUtility.displayErrorMessage(NavigationDrawerActivity.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    if(jsonObject.has("status") && !jsonObject.isNull("status")) {
                        status = jsonObject.getString("status");
                    }
                    if(jsonObject.has("message") && !jsonObject.isNull("message")) {
                        message = jsonObject.getString("message");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalStatus = status;
                final String finalMessage = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(NavigationDrawerActivity.this, false);
                        if(finalStatus.equalsIgnoreCase("success")) {
                            mMaterialDialog.dismiss();
                            /**Remove recods from Rating table*/
                            db.deleteRatingsInfo();
                            Toast.makeText(NavigationDrawerActivity.this, ""+ finalMessage, Toast.LENGTH_SHORT).show();
                        } else{
                            Toast.makeText(NavigationDrawerActivity.this, ""+ finalMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForAddReview(String userId, String list_id, String rating, String review,String object_type) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "save_review").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("rating", rating);
        urlBuilder.addQueryParameter("review", review);

        if(object_type != null
                && (object_type.equalsIgnoreCase("Event")
                || object_type.equalsIgnoreCase("event")))
        {
            urlBuilder.addQueryParameter("event_id", list_id);
            urlBuilder.addQueryParameter("object_type", "event");
        }
        else
        {
            urlBuilder.addQueryParameter("list_id", list_id);
            urlBuilder.addQueryParameter("object_type", "list");
        }

        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Display Items for Facebook Login users & Hiding Change password
     */
    public void display_Facebook_Items(int position)
    {
        switch (position) {
            case 0:
                Constants.MY_SPORTS_CLICKED = false;
                /**
                 *  Condition Implementation to handle the Push Notifivcation
                 *  and Deep Link page redirection
                 */
                if(AppConstants.isDeepLinkRecieved){
                    NavigationDrawerActivity.this.finish();
                    startActivity(new Intent(NavigationDrawerActivity.this, EventDetail.class));
                }else if(AppConstants.isListDeepLinkRecieved){
                    NavigationDrawerActivity.this.finish();
                    startActivity(new Intent(NavigationDrawerActivity.this, VenueDetails.class));
                } else if(AppConstants.CHAT_CHANNEL_ID_DUPLICATE != null){
                    NavigationDrawerActivity.this.finish();
                    AppConstants.isChatMessageRecieveedInBackground = true;
                    startActivity(new Intent(NavigationDrawerActivity.this, ChatActivity.class));
                } else if(!TextUtils.isEmpty(   Constants.PUSH_NOTIFICATION_GAME_ID)) {
                    NavigationDrawerActivity.this.finish();
                    startActivity(new Intent(NavigationDrawerActivity.this, EventDetail.class));
                } else if(Constants.FROM_PUSH_NOTIFICATION_INVITE_FRIEND) {
                    //NavigationDrawerActivity.this.finish();
                    fragment = new TabFragment();
                } else if(Constants.FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY) {
                    Constants.FROM_PUSH_NOTIFICATION_TEAMMATE_ACCEPTED_SUCCESSFULLY = false;
                    NavigationDrawerActivity.this.finish();
                    startActivity(new Intent(NavigationDrawerActivity.this, TeamMates.class));
                } else{
                    fragment = new TabFragment();
                    setTitle("");
                }
                break;

            case 1:
                /**Open Upcoming page*/
                Constants.MY_SPORTS_CLICKED = true;
                fragment = new TabFragment();
                setTitle("");
                break;

            case 2:
                startActivity(new Intent(NavigationDrawerActivity.this, TeamMates.class));
                setTitle("");
                break;
            case 3:
                startActivity(new Intent(NavigationDrawerActivity.this, StoredPaymentCardsScreen.class));
                setTitle("");
                break;
            case 4:
                Intent intent3 = new Intent(NavigationDrawerActivity.this, MapRadius.class);
                startActivity(intent3);

                setTitle("");
                break;
            case 5:
                Intent inten = new Intent(NavigationDrawerActivity.this, MyWishList.class);
                startActivity(inten);
                setTitle("");
                break;
            case 6:
                setTitle("");
                Intent intent_privacy = new Intent(NavigationDrawerActivity.this, Privacy.class);
                startActivity(intent_privacy);
                break;

            case 7:
                setTitle("");
                Intent intent_settings = new Intent(NavigationDrawerActivity.this, Settings.class);
                startActivity(intent_settings);
                break;
            case 8:
                setTitle("");
                Intent intent_faq = new Intent(NavigationDrawerActivity.this, FAQ.class);
                startActivity(intent_faq);
                break;
            case 9:
                setTitle("");
                Intent intent_terms = new Intent(NavigationDrawerActivity.this, Terms.class);
                startActivity(intent_terms);
                break;
            case 10:

                /**Clear Logged user FCM Id from server for Push notification*/
                clearLoggedUserFCMId(user_id);

                break;
            default:
                break;
        }
    }
}
