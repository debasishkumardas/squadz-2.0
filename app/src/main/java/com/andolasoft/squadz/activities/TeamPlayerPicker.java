package com.andolasoft.squadz.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.AddPlayerModel;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.views.adapters.AddPlayerAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class TeamPlayerPicker extends AppCompatActivity {
    @InjectView(R.id.back_add_player_layout)
    RelativeLayout back_add_player_layout;
    @InjectView(R.id.all_layout)
    RelativeLayout all_layout;
    @InjectView(R.id.baseball_layout)
    RelativeLayout baseball_layout;
    @InjectView(R.id.basketball_layout)
    RelativeLayout basketball_layout;
    @InjectView(R.id.cricket_layout)
    RelativeLayout cricket_layout;
    @InjectView(R.id.football_layout)
    RelativeLayout football_layout;
    @InjectView(R.id.soccer_layout)
    RelativeLayout soccer_layout;
    @InjectView(R.id.tennis_layout)
    RelativeLayout tennis_layout;
    @InjectView(R.id.volleyball_layout)
    RelativeLayout volleyball_layout;
    @InjectView(R.id.custom_layout)
    RelativeLayout custom_layout;
    @InjectView(R.id.golf_layout)
    RelativeLayout golf_layout;
    @InjectView(R.id.running_layout)
    RelativeLayout running_layout;
    @InjectView(R.id.biking_layout)
    RelativeLayout biking_layout;
    @InjectView(R.id.rugby_layout)
    RelativeLayout rugby_layout;
    @InjectView(R.id.ultimate_frisbee_layout)
    RelativeLayout ultimate_frisbee_layout;

    @InjectView(R.id.all_view)
    View all_view;
    @InjectView(R.id.baseball_view)
    View baseball_view;
    @InjectView(R.id.basketball_view)
    View basketball_view;
    @InjectView(R.id.cricket_view)
    View cricket_view;
    @InjectView(R.id.football_view)
    View football_view;
    @InjectView(R.id.soccer_view)
    View soccer_view;
    @InjectView(R.id.tennis_view)
    View tennis_view;
    @InjectView(R.id.volleyball_view)
    View volleyball_view;
    @InjectView(R.id.custom_view)
    View custom_view;
    @InjectView(R.id.golf_view)
    View golf_view;
    @InjectView(R.id.running_view)
    View running_view;
    @InjectView(R.id.biking_view)
    View biking_view;
    @InjectView(R.id.rugby_view)
    View rugby_view;
    @InjectView(R.id.ultimate_frisbee_view)
    View ultimate_frisbee_view;
    @InjectView(R.id.add_player_recycler_view)
    RecyclerView add_player_recycler_view;
    @InjectView(R.id.hsv)
    HorizontalScrollView horizontalScrollView;
    @InjectView(R.id.join_btn_add_player)
    Button join_btn_add_player;

    @InjectView(R.id.all_team_count)
    TextView all_team_count;
    @InjectView(R.id.baseball_team_count)
    TextView baseball_team_count;
    @InjectView(R.id.basketball_team_count)
    TextView basketball_team_count;
    @InjectView(R.id.cricket_team_count)
    TextView cricket_team_count;
    @InjectView(R.id.football_team_count)
    TextView football_team_count;
    @InjectView(R.id.soccer_team_count)
    TextView soccer_team_count;
    @InjectView(R.id.tennis_team_count)
    TextView tennis_team_count;
    @InjectView(R.id.volleyball_team_count)
    TextView volleyball_team_count;
    @InjectView(R.id.custom_team_count)
    TextView custom_team_count;
    @InjectView(R.id.golf_team_count)
    TextView golf_team_count;
    @InjectView(R.id.running_team_count)
    TextView running_team_count;
    @InjectView(R.id.biking_team_count)
    TextView biking_team_count;
    @InjectView(R.id.rugby_team_count)
    TextView rugby_team_count;
    @InjectView(R.id.ultimate_frisbee_team_count)
    TextView ultimate_frisbee_team_count;

    public static TextView team_name_title;
    public static TextView team_name_count;

    String sports_type = "All";
    DatabaseAdapter db;
    ArrayList<AddPlayerModel> addPlayerModelArrayList;
    AddPlayerAdapter addPlayerAdapter;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    SnackBar snackBar;
    Set<String> set;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_player_picker);

        /**Initializing view*/
        initViews();

        /**Adding clcik events on the views*/
        setClickEvents();

        /**Default displaying for All*/
        getAllFriendFromDBWithFilter(sports_type);

        /**Set Player count*/
        setPlayerCount();
        if(Constants.IS_EDIT_BUTTON_CLICKED)
        {
            join_btn_add_player.setText("UPDATE");
        }
    }

    /**
     * Adding click events on the views
     */
    public void setClickEvents()
    {
        back_add_player_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        all_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sports_type = "All";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);
                horizontalScrollView.smoothScrollTo(0,100);
            }
        });

        baseball_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Baseball";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);
                horizontalScrollView.smoothScrollTo(0,100);
            }
        });
        basketball_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Basketball";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);
                horizontalScrollView.smoothScrollTo(350,200);
            }
        });
        cricket_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Cricket";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);
                horizontalScrollView.smoothScrollTo(750,100);
            }
        });
        football_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Football";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);
                horizontalScrollView.smoothScrollTo(1100,100);
            }
        });
        soccer_layout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            sports_type = "Soccer";
            displayOrangeDivider(sports_type);
            getAllFriendFromDBWithFilter(sports_type);
            horizontalScrollView.smoothScrollTo(1500,100);
        }
    });
        tennis_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Tennis";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);

                horizontalScrollView.smoothScrollTo(1900,100);
            }
        });
        volleyball_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Volleyball";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);

                horizontalScrollView.smoothScrollTo(2250,100);
            }
        });
        custom_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Custom";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);

                horizontalScrollView.smoothScrollTo(2650,100);
            }
        });
        golf_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Golf";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);

                horizontalScrollView.smoothScrollTo(3000,100);
            }
        });
        running_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Running";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);

                horizontalScrollView.smoothScrollTo(3350,100);
            }
        });
        biking_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Biking";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);

                horizontalScrollView.smoothScrollTo(3700,100);
            }
        });
        rugby_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Rugby";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);

                horizontalScrollView.smoothScrollTo(4050,100);
            }
        });
        ultimate_frisbee_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sports_type = "Ultimate Frisbee";
                displayOrangeDivider(sports_type);
                getAllFriendFromDBWithFilter(sports_type);

                horizontalScrollView.smoothScrollTo(4300,100);
            }
        });

        join_btn_add_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.IS_EDIT_BUTTON_CLICKED && Constants.ADD_PLAYER_ARRAYLIST.size() > 0)
                {
                    Set<String> set = new HashSet<String>();
                    set.addAll(Constants.ADD_PLAYER_ARRAYLIST);
                    editor.putStringSet(Constants.EDIT_NAME_SPORT_NAME, set);
                    editor.commit();

                    finish();
                }
                else if(Constants.ADD_PLAYER_ARRAYLIST.size() > 0) {
                    set = new HashSet<String>();
                    set.addAll(Constants.ADD_PLAYER_ARRAYLIST);
                    editor.putStringSet(sports_type, set);
                    editor.commit();

                    finish();
                }
                else{
                    snackBar.setSnackBarMessage("Please select any player to add in your team");
                }
            }
        });

    }
    /**Displaying orange color view on view click based on the sports type*/

    public void displayOrangeDivider(String sports_type)
    {
        if(sports_type.equalsIgnoreCase("All"))
        {
            all_view.setVisibility(View.VISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Baseball"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.VISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Basketball"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.VISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Cricket"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.VISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Football"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.VISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Soccer"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.VISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Tennis"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.VISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Volleyball"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.VISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Custom"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.VISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Golf"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.VISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Running"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.VISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Biking"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.VISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Rugby"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.VISIBLE);
            ultimate_frisbee_view.setVisibility(View.INVISIBLE);
        }
        else if(sports_type.equalsIgnoreCase("Ultimate Frisbee"))
        {
            all_view.setVisibility(View.INVISIBLE);
            baseball_view.setVisibility(View.INVISIBLE);
            basketball_view.setVisibility(View.INVISIBLE);
            cricket_view.setVisibility(View.INVISIBLE);
            football_view.setVisibility(View.INVISIBLE);
            soccer_view.setVisibility(View.INVISIBLE);
            tennis_view.setVisibility(View.INVISIBLE);
            volleyball_view.setVisibility(View.INVISIBLE);
            custom_view.setVisibility(View.INVISIBLE);
            golf_view.setVisibility(View.INVISIBLE);
            running_view.setVisibility(View.INVISIBLE);
            biking_view.setVisibility(View.INVISIBLE);
            rugby_view.setVisibility(View.INVISIBLE);
            ultimate_frisbee_view.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Function to pull all the friends from Local DB
     */

    public void getAllFriendFromDBWithFilter(String sportName) {
        addPlayerModelArrayList = new ArrayList<>();
        Cursor cursor = null;
        if(sportName.equalsIgnoreCase("All"))
        {
            cursor = db.getAllFriendsDeomDb();
        }
        else{
            cursor = db.getFilteredFriendList(sportName);
        }

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String user_id = cursor.getString(cursor
                            .getColumnIndex("user_id"));
                    String username = cursor.getString(cursor
                            .getColumnIndex("username"));
                    String firstname = cursor.getString(cursor
                            .getColumnIndex("firstname"));
                    String lastname = cursor.getString(cursor
                            .getColumnIndex("lastname"));
                    String image = cursor.getString(cursor
                            .getColumnIndex("image"));
                    String sport = cursor.getString(cursor
                            .getColumnIndex("sport"));
                    String phonenumber = cursor.getString(cursor
                            .getColumnIndex("phonenumber"));
                    String status = cursor.getString(cursor
                            .getColumnIndex("status"));
                    String reward = cursor.getString(cursor
                            .getColumnIndex("reward"));

                    AddPlayerModel addPlayerModel = new AddPlayerModel(user_id, username, firstname,
                            lastname, image, sport, phonenumber, status, reward);
                    addPlayerModelArrayList.add(addPlayerModel);

                } while (cursor.moveToNext());
            }
        }
        if(addPlayerModelArrayList.size() > 0)
        {
            addPlayerAdapter = new AddPlayerAdapter(TeamPlayerPicker.this,addPlayerModelArrayList,sportName);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            add_player_recycler_view.setLayoutManager(mLayoutManager);
            add_player_recycler_view.setItemAnimator(new DefaultItemAnimator());
            add_player_recycler_view.setAdapter(addPlayerAdapter);
            addPlayerAdapter.notifyDataSetChanged();
        }
        else{
            Toast.makeText(TeamPlayerPicker.this, "No Players Found", Toast.LENGTH_LONG).show();
            addPlayerAdapter = new AddPlayerAdapter(TeamPlayerPicker.this,addPlayerModelArrayList,sportName);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            add_player_recycler_view.setLayoutManager(mLayoutManager);
            add_player_recycler_view.setItemAnimator(new DefaultItemAnimator());
            add_player_recycler_view.setAdapter(addPlayerAdapter);
            addPlayerAdapter.notifyDataSetChanged();
        }

    }

    /**Initializing view*/
    public void initViews()
    {
        ButterKnife.inject(this);
        db = new DatabaseAdapter(TeamPlayerPicker.this);
        snackBar = new SnackBar(this);
        prefs = getSharedPreferences("AddPlayerPrefs", Context.MODE_PRIVATE);
        editor = prefs.edit();

        team_name_title = (TextView) findViewById(R.id.team_name_title);
        team_name_count = (TextView) findViewById(R.id.team_name_count);

        team_name_title.setText(AddNewTeam.teamName);

        if(AddNewTeam.set != null) {
            team_name_count.setText("(" + AddNewTeam.set.size() + ")");
        }else {
            if(Constants.ADD_PLAYER_ARRAYLIST.size() > 0) {

                //set = prefs.getStringSet(TeamMates.SELECTED_SPORT_NAME, null);
                team_name_count.setText("(" +Constants.ADD_PLAYER_ARRAYLIST.size() + ")");
            }
            else{
                team_name_count.setText("(" + 0 + ")");
            }
        }
    }
    /**
     * Set Player count
     */
    public void setPlayerCount()
    {
        int player_count_all = db.getAllPlayerCount();
        int player_count_baseball = db.getPlayerCount("Baseball");
        int player_count_basketball = db.getPlayerCount("Basketball");
        int player_count_cricket = db.getPlayerCount("Cricket");
        int player_count_football = db.getPlayerCount("Football");
        int player_count_soccer = db.getPlayerCount("Soccer");
        int player_count_tennis = db.getPlayerCount("Tennis");
        int player_count_volleyball = db.getPlayerCount("Volleyball");
        int player_count_custom = db.getPlayerCount("Custom");
        int player_count_golf = db.getPlayerCount("Golf");
        int player_count_running = db.getPlayerCount("Running");
        int player_count_biking = db.getPlayerCount("Biking");
        int player_count_rugby = db.getPlayerCount("Rugby");
        int player_count_ultimate_frisbee = db.getPlayerCount("Ultimate Frisbee");

        all_team_count.setText(String.valueOf("("+player_count_all)+")");
        baseball_team_count.setText(String.valueOf("("+player_count_baseball)+")");
        basketball_team_count.setText(String.valueOf("("+player_count_basketball)+")");
        cricket_team_count.setText(String.valueOf("("+player_count_cricket)+")");
        football_team_count.setText(String.valueOf("("+player_count_football)+")");
        soccer_team_count.setText(String.valueOf("("+player_count_soccer)+")");
        tennis_team_count.setText(String.valueOf("("+player_count_tennis)+")");
        volleyball_team_count.setText(String.valueOf("("+player_count_volleyball)+")");
        custom_team_count.setText(String.valueOf("("+player_count_custom)+")");
        golf_team_count.setText(String.valueOf("("+player_count_golf)+")");
        running_team_count.setText(String.valueOf("("+player_count_running)+")");
        biking_team_count.setText(String.valueOf("("+player_count_biking)+")");
        rugby_team_count.setText(String.valueOf("("+player_count_rugby)+")");
        ultimate_frisbee_team_count.setText(String.valueOf("("+player_count_ultimate_frisbee)+")");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

