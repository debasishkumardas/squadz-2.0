package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SpNayak on 1/31/2017.
 */

public class MapRadius extends AppCompatActivity {

    @InjectView(R.id.map_radius_back_layout)
    RelativeLayout map_radius_back_layout;
    @InjectView(R.id.map_radius_plus_btn)
    Button btn_plus;
    @InjectView(R.id.map_radius_minus_btn)
    Button btn_minus;
    @InjectView(R.id.mapRadius_miles)
    TextView mapRadius_miles;
    @InjectView(R.id.done_btn_layout)
    RelativeLayout done_btn_layout;
    int minteger = 10;
    ProgressDialog dialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String user_id = "",auth_token = "",radius_count = "";
    String status = "", message = "";
    SnackBar snackBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_map_radius);

        /**
         * Initializing views
         */
        initViews();

        /**
         * Adding Click events on te views
         */
        setClickEvents();

        /**
         * Set miles count if exist
         */
        setMilesIfExist();

    }

    /**
     * Adding Click events on te views
     */
    public void setClickEvents()
    {
        //Back button click
        map_radius_back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Plus button click
        btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                minteger = minteger + 5;
                display(minteger);
            }
        });

        //Minus button clcik
        btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(minteger != 10)
                {
                    minteger = minteger - 5;
                    display(minteger);
                }
            }
        });
        //Done button click
        done_btn_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String map_radius = String.valueOf(minteger);

                //API integration for map radius
                setMapRadiusAPI(map_radius);
            }
        });
    }
    /**
     * Display radius count
     * @param number
     */

    private void display(int number) {
        mapRadius_miles.setText("" + number + " Miles");
    }

    /**
     * API integration for map radius
     */
    public void setMapRadiusAPI(String map_radius) {
        //Handling the loader state
        LoaderUtility.handleLoader(MapRadius.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(map_radius);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(MapRadius.this, false);
                if(!ApplicationUtility.isConnected(MapRadius.this)){
                    ApplicationUtility.displayNoInternetMessage(MapRadius.this);
                }else{
                    ApplicationUtility.displayErrorMessage(MapRadius.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();

                try {

                    JSONObject radius_jsonObject = new JSONObject(responseData);
                    if(radius_jsonObject.has("status") && !radius_jsonObject.isNull("status"))
                    {
                        status = radius_jsonObject.getString("status");
                        message = radius_jsonObject.getString("message");
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(MapRadius.this, false);
                        if(!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                           // snackBar.setSnackBarMessage(message);
                            Toast.makeText(MapRadius.this,message, Toast.LENGTH_LONG).show();
                            //Store updated miles in Shared preference
                            editor.putString("RADIUS_COUNT",String.valueOf(minteger));
                            editor.apply();
                            finish();
                        }
                        else{
                            snackBar.setSnackBarMessage(message);
                        }
                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String map_radius) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "save_user_info").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("radius", map_radius);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(MapRadius.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Initializing views
     */
    public void initViews() {
        ButterKnife.inject(this);
        snackBar = new SnackBar(this);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(MapRadius.this);
        editor = preferences.edit();

        if(preferences.contains("loginuser_id"))
        {
            user_id = preferences.getString("loginuser_id","");
            auth_token = preferences.getString("auth_token","");
        }

        if(preferences.contains("RADIUS_COUNT")) {
            radius_count = preferences.getString("RADIUS_COUNT","");
        }
    }

    /**
     * Set miles count if exist
     */
    public void setMilesIfExist() {
        if(preferences.contains("RADIUS_COUNT")) {
            radius_count = preferences.getString("RADIUS_COUNT","");
            mapRadius_miles.setText("" + radius_count + " Miles");
            minteger = Integer.valueOf(radius_count);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
