package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.adapters.UpcomingPastAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Additionalinfo extends AppCompatActivity {

    public static Additionalinfo additionalinfo;
    private SwitchCompat switch_button_add_info;
    public TextView private_add_info, public_add_info,
            skill_level_text, players_count_add_info, tv_game_visibility_text;
    private RelativeLayout skill_level_layout,ic_back_layout;
    private Button minus_btn_add_info,plus_btn_add_info,
            confirm_btn_add_info;
    private EditText edit_game_name_add_info,edit_little_notes_add_info;
    SnackBar snackBar;
    int players;
    ProgressDialog dialog;
    private String status = "";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String userName = "", image = "",user_Id = "", auth_token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_additionalinfo);

        //Initializing all views belongs to this layout
        initReferences();

        //Click events on the views
        addClickEvents();

        //Set data coming from Venue listing page
        setData();

    }

    /**
     * Click events on the views
     */
    public void addClickEvents() {
        switch_button_add_info.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    public_add_info.setTextColor(getResources().getColor(R.color.orange));
                    private_add_info.setTextColor(getResources().getColor(R.color.gray_new));
                    tv_game_visibility_text.setText("Anyone can join.");
                    Constants.GAME_VISIBILITY = "Public";
                } else {
                    public_add_info.setTextColor(getResources().getColor(R.color.gray_new));
                    private_add_info.setTextColor(getResources().getColor(R.color.orange));
                    tv_game_visibility_text.setText("Only teammates can join");
                    Constants.GAME_VISIBILITY = "Private";


                }
                /**Hide Keyboard*/
                ApplicationUtility.hideSoftKeyBoard(Additionalinfo.this);
            }
        });

        skill_level_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Additionalinfo.this, SkillLevel.class);
                startActivity(intent);
            }
        });

        //Confirm Button click
        confirm_btn_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String get_game_name = edit_game_name_add_info.getText().toString().trim();
                String get_players_count = players_count_add_info.getText().toString().trim();
                String get_skill_level = skill_level_text.getText().toString().trim();
                String get_notes = edit_little_notes_add_info.getText().toString().trim();

                if(TextUtils.isEmpty(get_game_name)) {
                    snackBar.setSnackBarMessage("Please add your game name");
                } else if(confirm_btn_add_info.getText().toString().equalsIgnoreCase("UPDATE")) {
                    /**Update Game API*/
                    updateGameAPI(get_game_name,get_notes,Constants.SKILL_LEVEL,Constants.GAME_VISIBILITY,get_players_count);
                } else{
                    Constants.IS_EDIT_GAME_CLICKED = false;
                    Constants.GAME_NAME = get_game_name;
                    Constants.PLAYERS_COUNT = get_players_count;
                    Constants.SKILL_LEVEL = get_skill_level;
                    Constants.NOTES_INFO = get_notes;

                    Intent intent = new Intent(Additionalinfo.this, Summary.class);
                    startActivity(intent);
                }
            }
        });

        minus_btn_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**Hide Keyboard*/
                ApplicationUtility.hideSoftKeyBoard(Additionalinfo.this);
                minus_btn_add_info.setText("-");
                plus_btn_add_info.setText("+");
                players = Integer.valueOf(players_count_add_info.getText().toString().trim());

                if(players == 1) {
                    snackBar.setSnackBarMessage("Court Minimum player exceed");
                    plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));
                    minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    if(players == 1) {
                        minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));
                        plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                        minus_btn_add_info.setTextColor(getResources().getColor(R.color.white));
                        plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                    }
                } else if(players == Constants.NO_OF_PARTICIPANT) {
                    minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));
                    plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    minus_btn_add_info.setTextColor(getResources().getColor(R.color.white));
                    plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                    snackBar.setSnackBarMessage("Court Minimum player exceed");
                } else{
                    players--;
                    players_count_add_info.setText(String.valueOf(players));
                    plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                }
            }
        });

        plus_btn_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**Hide Keyboard*/
                ApplicationUtility.hideSoftKeyBoard(Additionalinfo.this);


                plus_btn_add_info.setText("+");
                minus_btn_add_info.setText("-");
                players = Integer.valueOf(players_count_add_info.getText().toString().trim());

                if(Constants.COURT_MAX_PLAYER_ == 1) {
                    players++;
                    players_count_add_info.setText(String.valueOf(players));

                    minus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                    plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));

                    plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                } else {
                    if(Constants.IS_EDIT_GAME_CLICKED && players == Constants.COURT_TOTAL_MAX_PLAYER_EDIT) {
                        snackBar.setSnackBarMessage("Court Maximum player exceed");
                        plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));
                        minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));

                        minus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                        plus_btn_add_info.setTextColor(getResources().getColor(R.color.white));
                    }
                    else if (!Constants.IS_EDIT_GAME_CLICKED && Constants.COURT_MAX_PLAYER_ == players) {
                        snackBar.setSnackBarMessage("Court Maximum player exceed");

                        plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));
                        minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                    } else {
                        plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                        minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                        players++;
                        players_count_add_info.setText(String.valueOf(players));

                        minus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                        plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));

                        if (!Constants.IS_EDIT_GAME_CLICKED && Constants.COURT_MAX_PLAYER_ == players) {
                            plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));

                            minus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                            plus_btn_add_info.setTextColor(getResources().getColor(R.color.white));
                        }
                    }
                }
            }
        });

        //Back button click
        ic_back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.IS_EDIT_GAME_CLICKED = false;
                finish();
            }
        });
    }

    /**
     * Set data coming from Venue listing page
     */
    public void setData() {
        String playersCount = String.valueOf(Constants.COURT_MAX_PLAYER_);
        if(!TextUtils.isEmpty(playersCount)) {
            if(playersCount.equalsIgnoreCase("undefined")) {
                players_count_add_info.setText("0");
            } else{
                players_count_add_info.setText(playersCount);
            }
        } else{
            players_count_add_info.setText("0");
        }

        if(Constants.COURT_MAX_PLAYER_ == 1) {
            minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button));
            plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));

            minus_btn_add_info.setTextColor(getResources().getColor(R.color.white));
            plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
        }

        //Getting selected Skil level from Skill lever page
        skill_level_text.setText(Constants.SKILL_LEVEL);

        /**Coming from Edit game page*/
        if(Constants.IS_EDIT_GAME_CLICKED) {
            //Constants.IS_EDIT_GAME_CLICKED = false;
            confirm_btn_add_info.setText("UPDATE");
            edit_game_name_add_info.setText(Constants.GAME_NAME);
            edit_game_name_add_info.setSelection(edit_game_name_add_info.getText().length());
            if(!TextUtils.isEmpty(Constants.NOTE) && !Constants.NOTE.equalsIgnoreCase("null")) {
                edit_little_notes_add_info.setText(Constants.NOTE);
                edit_little_notes_add_info.setSelection(edit_little_notes_add_info.getText().length());
            }
            if(Constants.COURT_MAX_PLAYER_ < Constants.COURT_TOTAL_MAX_PLAYER_EDIT) {
                minus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));
                plus_btn_add_info.setBackgroundDrawable(getResources().getDrawable(R.drawable.circular_button_orange));

                minus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
                plus_btn_add_info.setTextColor(getResources().getColor(R.color.orange));
            }
        } else{
            confirm_btn_add_info.setText("CONFIRM");
        }
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {
        switch_button_add_info = (SwitchCompat) findViewById(R.id.switch_button_add_info);
        private_add_info = (TextView) findViewById(R.id.private_add_info);
        public_add_info = (TextView) findViewById(R.id.public_add_info);
        skill_level_text = (TextView) findViewById(R.id.skill_level_text);
        players_count_add_info = (TextView) findViewById(R.id.players_count_add_info);
        skill_level_layout = (RelativeLayout) findViewById(R.id.skill_level_layout);
        ic_back_layout = (RelativeLayout) findViewById(R.id.back_icon_add_info_layout);
        minus_btn_add_info = (Button) findViewById(R.id.minus_btn_add_info);
        plus_btn_add_info = (Button) findViewById(R.id.plus_btn_add_info);
        confirm_btn_add_info = (Button) findViewById(R.id.confirm_btn_add_info);
        edit_game_name_add_info = (EditText) findViewById(R.id.edit_game_name_add_info);
       // setFocusChangeListenre();
        edit_little_notes_add_info = (EditText) findViewById(R.id.edit_little_notes_add_info);
        tv_game_visibility_text = (TextView) findViewById(R.id.tv_game_visibility_text);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(Additionalinfo.this);
        editor = preferences.edit();

        snackBar = new SnackBar(Additionalinfo.this);
        additionalinfo = this;
        switch_button_add_info.setChecked(true);
        Constants.GAME_VISIBILITY = "Public";
        public_add_info.setTextColor(getResources().getColor(R.color.orange));
        private_add_info.setTextColor(getResources().getColor(R.color.gray_new));
        tv_game_visibility_text.setText("Anyone can join.");

        if(!Constants.IS_EDIT_GAME_CLICKED) {
            switch_button_add_info.setChecked(true);
            Constants.GAME_VISIBILITY = "Public";
            public_add_info.setTextColor(getResources().getColor(R.color.orange));
            private_add_info.setTextColor(getResources().getColor(R.color.gray_new));
        }
        else if(Constants.GAME_VISIBILITY.equalsIgnoreCase("Public")){
            switch_button_add_info.setChecked(true);
            Constants.GAME_VISIBILITY = "Public";
            public_add_info.setTextColor(getResources().getColor(R.color.orange));
            private_add_info.setTextColor(getResources().getColor(R.color.gray_new));
        }
        else{
            switch_button_add_info.setChecked(false);
            Constants.GAME_VISIBILITY = "Private";
            public_add_info.setTextColor(getResources().getColor(R.color.gray_new));
            private_add_info.setTextColor(getResources().getColor(R.color.orange));
        }

        if (preferences.contains("USERNAME")) {
            userName = preferences.getString("USERNAME", "");
            image = preferences.getString("image", "");
            auth_token = preferences.getString("auth_token", "");
            user_Id = preferences.getString("loginuser_id", "");

            if (TextUtils.isEmpty(image)) {
                image = " ";
            }
        }
    }

    /**
     * Game create API call
     */
    public void gameCreate(String user_id, String game_id,String list_id, String sport_name, String name,
                           String note, String no_of_player, String skill_level,
                           String visibility, String userName, String image) {
        //Handling the loader state
        LoaderUtility.handleLoader(Additionalinfo.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, game_id, list_id, sport_name, name, note,
                no_of_player, skill_level, visibility, userName, image);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(Additionalinfo.this, false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    if (responseObject.has("status") && !responseObject.isNull("status")) {
                        status = responseObject.getString("status");
                        AppConstants.GAME_ID = responseObject.getString("game_id");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                Additionalinfo.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(Additionalinfo.this, false);
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                Constants.isEventUpdated = true;
                                //Time sloe array value set to zero after successfully created a game
                                Constants.AVAILABILITY_TIME_SLOT_ARRAY = new ArrayList<String>();
                                Toast.makeText(Additionalinfo.this, "Event updated successfully", Toast.LENGTH_SHORT).show();
                                finish();

                            } else {
                                snackBar.setSnackBarMessage("Unable to update this event");
                            }
                        } else {
                            snackBar.setSnackBarMessage("Network error! please try after sometime");
                        }
                    }
                });
            }
        });
    }


    /**
     *  */
    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     *//*
     * @return
     */
    public Request buildApiRequestBody(String user_id, String game_id,String list_id, String sport_name, String name,
                                       String note, String no_of_player, String skill_level,
                                       String visibility, String userName, String image) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "save_game_info").newBuilder();
        urlBuilder.addQueryParameter("user_id", user_id);
        urlBuilder.addQueryParameter("game_id", game_id);
        urlBuilder.addQueryParameter("list_id", list_id);
        urlBuilder.addQueryParameter("sport_name", sport_name);
        urlBuilder.addQueryParameter("name", name);
        urlBuilder.addQueryParameter("note", note);
        urlBuilder.addQueryParameter("no_of_player", no_of_player);
        urlBuilder.addQueryParameter("skill_level", skill_level);
        urlBuilder.addQueryParameter("visibility", visibility);
        urlBuilder.addQueryParameter("device_type", "android");
        urlBuilder.addQueryParameter("user_name", userName);
        urlBuilder.addQueryParameter("user_profile_image", image);

        String url = urlBuilder.build().toString();
        url = url.replace(" ", "");

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(Additionalinfo.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Update Game API
     */
    public void updateGameAPI(String game_name, String note,
                              String skill_level,String visibility,
                              String no_of_player)
    {
        if(Constants.UPCOMING_PAST_CLICKED)
        {
            Constants.UPCOMING_PAST_CLICKED_FOR_UPDATE_GAME = true;
            Constants.UPCOMING_GAME_UPDATED_SUCCESS = true;
            String game_id = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Id();
            String sport_name = UpcomingPastAdapter.eventListingModelArrayList.get(0).getEvent_Sports_Type();

            gameCreate(user_Id, game_id,UpcomingPastAdapter.event_id, sport_name, game_name,
                    note, no_of_player, skill_level,visibility, userName, image);
        }
        else
        {
            String game_id = EventAdapter.eventListingModelArrayList.get(0).getEvent_Id();
            String sport_name = EventAdapter.eventListingModelArrayList.get(0).getEvent_Sports_Type();

            gameCreate(user_Id,game_id, EventAdapter.event_Id, sport_name, game_name,
                    note, no_of_player, skill_level,visibility, userName, image);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Getting selected Skil level from Skill lever page
        skill_level_text.setText(Constants.SKILL_LEVEL);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.IS_EDIT_GAME_CLICKED = false;
        finish();
    }



}
