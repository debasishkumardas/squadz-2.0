package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.FaqAdapter;
import com.andolasoft.squadz.views.adapters.NearByFriendAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddFriendUsingNearBy extends AppCompatActivity {

    @InjectView(R.id.nearbylistView)
    ListView lv_friend;
    @InjectView(R.id.titletxt)
    TextView tv_title;
    @InjectView(R.id.back_icon_summary)
    ImageView ivBack;
    ProgressDialog dialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    GPSTracker gpsTracer;
    double currLocLatitude,currLocLongitude;
    String radius_count, auth_token,logged_in_user_id = null;
    ArrayList<UserMolel> list;
    NearByFriendAdapter near_by_adapter;
    SnackBar snackbar = new SnackBar(AddFriendUsingNearBy.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend_using_near_by);
        ButterKnife.inject(this);

        tv_title.setText("Nearby Friends");

        //Getting the current location
        getCurrentLocation();

        //Getting the Map Radius
        getMapRadius();

        //Initializing the array
        initializeArray();

        //Get all Near By friends
        getNearByFriends();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddFriendUsingNearBy.this.finish();
            }
        });

    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String current_lat,
                                       String current_lon,
                                       String max_distance){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"nearby_users?").newBuilder();
        urlBuilder.addQueryParameter("user_id", logged_in_user_id);
        urlBuilder.addQueryParameter("current_location", current_lat +","+current_lon);
        urlBuilder.addQueryParameter("max_distance", max_distance);
        urlBuilder.addQueryParameter("unit", "mile");
        //urlBuilder.addQueryParameter("user_id",logged_in_user_id);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(AddFriendUsingNearBy.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }


    public void getNearByFriends() {
        //Handling the loader state
        LoaderUtility.handleLoader(AddFriendUsingNearBy.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(String.valueOf(currLocLatitude),
                String.valueOf(currLocLongitude),/*radius_count*/"50");

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(AddFriendUsingNearBy.this, false);

                if(!ApplicationUtility.isConnected(AddFriendUsingNearBy.this)){
                    ApplicationUtility.displayNoInternetMessage(AddFriendUsingNearBy.this);
                }else{
                    ApplicationUtility.displayErrorMessage(AddFriendUsingNearBy.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                JSONObject nearby_jsonObject = new JSONObject(responseData);
                JSONArray jsonArray = nearby_jsonObject.getJSONArray("nearby_users");

                        if (jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                try {
                                    JSONObject nearbyJson = jsonArray.getJSONObject(i);
                                    String user_id = nearbyJson.getString("user_id");
                                    String username = nearbyJson.getString("username");
                                    String first_name = nearbyJson.getString("first_name");
                                    String last_name = nearbyJson.getString("last_name");
                                    String sport = nearbyJson.getString("sport");
                                    String phone_no = nearbyJson.getString("phone_number");
                                    String user_profileimage = nearbyJson.getString("image");
                                    String user_status = nearbyJson.getString("request_status");

                                    if (!nearbyJson.getBoolean("request_status")) {
                                        UserMolel user_profile = new UserMolel();

                                        if((TextUtils.isEmpty(first_name) || TextUtils.isEmpty(last_name)) ||
                                                (first_name.equalsIgnoreCase("null") || last_name.equalsIgnoreCase("null"))){
                                            user_profile.setUserFullName(username);
                                        }else {
                                            user_profile.setUserFullName(first_name + " "
                                                    + last_name);
                                        }
                                        user_profile.setUserId(user_id);
                                        user_profile.setUserName(username);
                                        user_profile.setUserPhoneNumber(phone_no);
                                        user_profile.setUserSport(sport);
                                        user_profile.setUserProfileImage(user_profileimage);
                                        user_profile.setUserStatus(user_status);
                                        list.add(user_profile);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(AddFriendUsingNearBy.this, false);

                        if (list.size() != 0) {
                            near_by_adapter = new NearByFriendAdapter(
                                    AddFriendUsingNearBy.this, list, AddFriendUsingNearBy.this);
                            lv_friend.setAdapter(near_by_adapter);
                        } else{
                            snackbar.setSnackBarMessage("No records found");
                        }
                    }
                });

            }
        });
    }

    /**
     * Function Responsible for proving the current location latitude and longitude
     */
    public void getCurrentLocation(){
        gpsTracer = new GPSTracker(AddFriendUsingNearBy.this);
        if(gpsTracer.canGetLocation()) {
            currLocLatitude = gpsTracer.getLatitude();
            currLocLongitude = gpsTracer.getLongitude();
        }
    }

    /**
     * Set miles count if exist
     */
    public void getMapRadius() {
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if(preferences.contains("RADIUS_COUNT")) {
            radius_count = preferences.getString("RADIUS_COUNT","");
            auth_token = preferences.getString("auth_token", "");
            logged_in_user_id = preferences.getString("loginuser_id", "");
        }
    }

    public void initializeArray(){
        list = new ArrayList<>();
    }
}
