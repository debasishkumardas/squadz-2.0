package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.ExpandCollapseListAdapter;
import com.andolasoft.squadz.views.adapters.FaqAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class FAQ extends AppCompatActivity {

    @InjectView(R.id.back_icon_faq_layout)
    RelativeLayout back_icon_faq_layout;
//    @InjectView(R.id.faq_listView)
//    ListView faq_listView;
    @InjectView(R.id.faqlist)
    ExpandableListView faq_Expand_list;
    @InjectView(R.id.swipe_refresh_layout_faq)
    SwipeRefreshLayout swipe_refresh_layout_faq;
    FaqAdapter faqAdapter;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    SnackBar snackBar;
    ProgressDialog dialog;
    ExpandCollapseListAdapter expandCollapseListAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private int lastExpandedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_faq_layout);

        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        /**
         * Adding click events on the views
         */
        addClickEvents();

        /**
         * API integration for display Questions with Answers
         */
        displayFAQ();
    }

    /**
     * Adding click events on the views
     */
    public void addClickEvents() {
        back_icon_faq_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        swipe_refresh_layout_faq.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                displayFAQ();
                swipe_refresh_layout_faq.setRefreshing(false);
            }
        });

        /**Collapsing other rows except selected row */
        faq_Expand_list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    faq_Expand_list.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });
    }

    /**
     * API integration for display Questions with Answers
     */
    public void displayFAQ() {
        //Handling the loader state
        LoaderUtility.handleLoader(FAQ.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(FAQ.this, false);

                if(!ApplicationUtility.isConnected(FAQ.this)){
                    ApplicationUtility.displayNoInternetMessage(FAQ.this);
                }else{
                    ApplicationUtility.displayErrorMessage(FAQ.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                final ArrayList<String> faq_arrayList = new ArrayList<String>();
                listDataHeader = new ArrayList<String>();
                listDataChild = new HashMap<String, List<String>>();
                List<String> answer_array;

                try {

                    JSONObject faq_jsonObject = new JSONObject(responseData);
                    JSONArray faq_json_array = faq_jsonObject.getJSONArray("faqs");

                    for (int i = 0; i < faq_json_array.length(); i++) {

                        answer_array = new ArrayList<String>();
                        JSONObject jsonObject = faq_json_array.getJSONObject(i);

                        String question = jsonObject.getString("question");
                        String answer = jsonObject.getString("answer");
                        answer_array.add(answer);

                        listDataHeader.add(question);
                        listDataChild.put(question, answer_array);

                        //faq_arrayList.add(question + "####" + answer);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Handling the loader state
                        LoaderUtility.handleLoader(FAQ.this, false);

                        if(listDataHeader.size() > 0) {
                            //Sending Questionaries to Adapter class to display the result
                            expandCollapseListAdapter = new ExpandCollapseListAdapter(FAQ.this, listDataHeader, listDataChild);
                            // setting list adapter
                            faq_Expand_list.setAdapter(expandCollapseListAdapter);
                            /*faqAdapter = new FaqAdapter(FAQ.this, faq_arrayList);
                            faq_listView.setAdapter(faqAdapter);
                            faqAdapter.notifyDataSetChanged();*/
                        }
                        else{
                            snackBar.setSnackBarMessage("No records found");
                        }

                    }
                });

            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing() {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_faq").newBuilder();

        String url = urlBuilder.build().toString();

        /*Request request = new Request.Builder()
                .url(url)
                .method("GET", RequestBody.create(null, new byte[0]))
                .build();*/
        Request request = new Request.Builder()
                .url(url)
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(FAQ.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {

        ButterKnife.inject(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        snackBar = new SnackBar(this);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(FAQ.this);
        editor = preferences.edit();

        swipe_refresh_layout_faq.setColorSchemeResources(R.color.orange,
                R.color.orange,
                R.color.orange);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

