package com.andolasoft.squadz.activities;

import android.graphics.Color;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.GMapV2Direction;
import com.andolasoft.squadz.utils.GPSTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.w3c.dom.Document;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ShowDirections extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    @InjectView(R.id.back_icon_finish_event)
    ImageView iv_back;
    GMapV2Direction md;
    Polyline polyline;
    ArrayList<Marker> markers;
    LatLng currentLatLng, destLatLng;
    Marker my_loc, dest_loc;
    double court_latitude,court_longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_directions);
        ButterKnife.inject(this);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Initializing the Marker array
        markers = new ArrayList<Marker>();
        md = new GMapV2Direction();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowDirections.this.finish();
            }
        });

        getLocation();
        //Applying strict mode
        setStrictMode();
        //Building the latlngs
        buildLatLngs();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
       /* LatLng sydney = new LatLng(-34, 151);
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

        my_loc = googleMap.addMarker(new MarkerOptions()
                .position(currentLatLng)
                .snippet("My Location")
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title("ME"));
        dest_loc = googleMap.addMarker(new MarkerOptions()
                .position(destLatLng)
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .title(Constants.EVENT_NAME_MAP));

        markers.add(my_loc);
        markers.add(dest_loc);

        // Adding the OnMapLoadedCallback listener
        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                if (markers.size() != 0) {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (Marker marker : markers) {
                        builder.include(marker.getPosition());
                    }
                    LatLngBounds bounds = builder.build();
                    int padding = 200; // offset from edges of the map in pixels

                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
                            bounds, padding));
                    mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                    markers.clear();
                }
            }
        });


        // Setting the route to show for DRIVING mode
        Document doc = md.getDocument(currentLatLng, destLatLng,
                GMapV2Direction.MODE_DRIVING);

        // Getting all the points from source to destination as latlng
        ArrayList<LatLng> directionPoint = md.getDirection(doc);

        // Adding the polyline color as RED
        PolylineOptions rectLine = new PolylineOptions().width(8).color(
                Color.RED);

        // Getting all the points from the directionPoint array
        for (int i = 0; i < directionPoint.size(); i++) {
            rectLine.add(directionPoint.get(i));
        }

        // Drawing the Polyline between source to destination
        polyline = googleMap.addPolyline(rectLine);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ShowDirections.this.finish();
    }

    /**
     * Function responsible for handling the strict mode
     */
    public void setStrictMode(){
        // Adding the StrictMode policy
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public void buildLatLngs(){
        currentLatLng = new LatLng(court_latitude, court_longitude);
        destLatLng = new LatLng(Constants.EVENT_LATITUDE,Constants.EVENT_LONGITUDE);
    }

    public void getLocation(){
        GPSTracker gpsTracker = new GPSTracker(ShowDirections.this);
        court_latitude = gpsTracker.getLatitude();
        court_longitude = gpsTracker.getLongitude();
    }
}
