package com.andolasoft.squadz.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.fragments.TabFragment;
import com.andolasoft.squadz.models.EventListingModel;
import com.andolasoft.squadz.models.UserMolel;
import com.andolasoft.squadz.models.VenueDetailModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.GPSTracker;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.MySpannable;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.utils.SportsImagePicker;
import com.andolasoft.squadz.utils.SportsSurfaceTypePicker;
import com.andolasoft.squadz.views.adapters.EventAdapter;
import com.andolasoft.squadz.views.adapters.ImageSlideAdapter;
import com.andolasoft.squadz.views.adapters.UpcomingPastAdapter;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.adapters.WishListAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.text.Line;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EventDetail extends AppCompatActivity implements OnMapReadyCallback {

    private RelativeLayout event_detail_back_icon_layout, lay_participants,
            game_detail_menu_icon_layout,poi_spot_remaining_layout,players_price_main_layout;
    private LinearLayout court_layout_field;
    private View view2_venue_details,detail_view2;
    private RatingBar rating_event_details;
    private TextView rating_event_details_reviews,
            event_detail_name,event_detail_miles,
            description_event_detail, eventSkillLevel,
            priceEventDetails, event_venue_type, tv_noOfParticipants, tv_spots_left,
            tv_datetime, tv_player_count, tv_sport_type_event,
            tv_eventDetailsPlayer, tv_CreatorUsername, tv_priceperplayertxt,
            tv_eventspots, tv_minimum_price, tv_equipments, tv_event_address, tv_available,
            add_info_text_game_detail,court_name_event_detail,poi_total_participants,
            poi_spots_remaining,tv_game_detail_list_name,tv_game_detail_venue_name;
    private ImageView /*event_detail_image,*/ iv_navigation, img_equipment_available;
    GoogleMap mMap;
    private GPSTracker gpsTracker;
    private Double current_lat, current_long;
    private boolean gps_enabled = false;
    MaterialFavoriteButton ic_favorite;
    ProgressDialog dialog;
    String status = null,  userId = null, eventId = null;
    boolean isAddedToFavorite = false;
    SharedPreferences prefs;
    SnackBar snackBar = new SnackBar(EventDetail.this);
    Button btn_join_game;
    public static EventDetail eventDetails;
    ViewPager viewpager_event;
    ImageButton left_nav_event, right_nav_event;
    FrameLayout event_detail_image_frame;
    CircleImageView iv_creator_profile_image;
    ScrollView join_btn_add_info;
    boolean fav_status = false;
    public static ArrayList<EventListingModel> eventListingModelArrayList;
    private ArrayList<EventListingModel> EventListingModelList;
    public EventListingModel eventListingModel;
    String deepLink;
    public static boolean isEventDetailsApiCalled = false;
    public static String event_lat, event_lon,game_Creator_userId = "",
            name = "",visibility = "",skillLevel = "",note = "",
            total_player_count = "",accepted_player_count,list_Id = "",
            eventDate = "",start_time = "",end_time = "",eventAddress = "",
            eventSportName = "",
            court_type = "",eventLat = "",eventLon = "",type = "",
            court_total_player_capacity = "",total_price = "0",getMinimumPrice = "0";

    int event_max_player, event_accepted_player;
    ImageView iv_share_event_details,indoor_icon_event;
    ShareDialog shareDialog;
    private CallbackManager callbackManager;
    private LoginManager manager;
    MaterialDialog mMaterialDialog;
    RelativeLayout ratings_layout;
    String is_Refundable_Status = "true";
    ImageView court_icon_event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_event_detail);


        prefs = PreferenceManager.getDefaultSharedPreferences(EventDetail.this);
        userId = prefs.getString("loginuser_id", null);
        eventDetails = this;
        /**
         * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
         */
        initReferences();

        /**
         * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
         */
        isLocationEnabled();

        btn_join_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Summary.storeSharingValues(eventSportName, eventDate, start_time, end_time);
                Intent intent = new Intent(EventDetail.this, JoinGameConfirmationScreen.class);
                startActivity(intent);
            }
        });

        //Event added for favorite button
        ic_favorite.setOnFavoriteAnimationEndListener(
                new MaterialFavoriteButton.OnFavoriteAnimationEndListener() {
                    @Override
                    public void onAnimationEnd(MaterialFavoriteButton buttonView, boolean favorite) {
                        if (favorite) {
                            makeListFavorite(userId,eventId,favorite);
                        } else {
                            makeListFavorite(userId,eventId,favorite);
                        }
                    }
                });

        lay_participants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EventDetail.this, EventParticipantsScreen.class);
                startActivity(intent);
            }
        });
        /**
         * Set click events on the views
         */
        setClickEvents();

        /**
         * Checking the deeplinkg URL Status and calling the API accrodingly to refresh the view.
         */
        if(!TextUtils.isEmpty(AppConstants.DEEP_LINKING_EVENT_ID)){
            getVenuesCourtsDetailsInfor(AppConstants.DEEP_LINKING_EVENT_ID,userId);
        }
        else if(!TextUtils.isEmpty(Constants.PUSH_NOTIFICATION_GAME_ID)) {
            //Refreshing the badge count
            refreshBadgeCount();
            getVenuesCourtsDetailsInfor(Constants.PUSH_NOTIFICATION_GAME_ID,userId);
        } else{
            /**
             * Fetching Event details from Event adapter class
             */
            setEventData();
            /**
             * INITIALIZING MAP WITH GPS TRACKER
             */
            initMapGps();
        }

    }


    /**
     * Function to get Venue courts from server
     */
    public void makeListFavorite(String user_id, String event_id, final boolean isFavorite) {
        //Handling the loader state
        LoaderUtility.handleLoader(EventDetail.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(user_id, event_id, isFavorite);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(EventDetail.this, false);
                if(!ApplicationUtility.isConnected(EventDetail.this)){
                    ApplicationUtility.displayNoInternetMessage(EventDetail.this);
                }else{
                    ApplicationUtility.displayErrorMessage(EventDetail.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    isAddedToFavorite = responseObject.getBoolean("isFavorite");
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                EventDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(EventDetail.this, false);

                        if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            if(isAddedToFavorite){
                                snackBar.setSnackBarMessage("Successfully Added to WishList");
                            }else{
                                snackBar.setSnackBarMessage("Successfully Removed from WishList");
                            }
                        }else if(status != null && status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                            snackBar.setSnackBarMessage("Failed to Add to WidhList");
                        }else{
                            snackBar.setSnackBarMessage(AppConstants.API_RESPONSE_ERROR_MESSAGE_ERR);
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(EventDetail.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (gps_enabled) {
            LatLng lat_lng = new LatLng(Double.parseDouble(event_lat), Double.parseDouble(event_lon));
            mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)).position(lat_lng).title(EventAdapter.eventListingModelArrayList.get(0).getEvent_Name()));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat_lng, 15));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.getUiSettings().setAllGesturesEnabled(false);
            mMap.isMyLocationEnabled();
        }
    }

    /**
     * CHECKING THE DEVICE LOCATION SERVICE IS ON OR OFF
     *
     * @return
     */
    public boolean isLocationEnabled() {
        gps_enabled = false;
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return gps_enabled;
    }


    /**
     * INITIALIZING MAP WITH GPS TRACKER
     */
    public void initMapGps() {
        gpsTracker = new GPSTracker(this);

        if (!gps_enabled) {
            Toast.makeText(EventDetail.this, "please enable your location service", Toast.LENGTH_SHORT).show();
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
        } else {
            if (gpsTracker.canGetLocation()) {
                current_lat = gpsTracker.getLatitude();
                current_long = gpsTracker.getLongitude();
            }
        }
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView_Event_Detail);
        mapFragment.getMapAsync(this);
    }

    /**
     * Fetching Event details from Event adapter class
     */
    public void setEventData() {
        try {
            game_Creator_userId = EventAdapter.eventListingModelArrayList.get(0).getCreator_user_id();
            name = EventAdapter.eventListingModelArrayList.get(0).getEvent_Name();
            Constants.EVENT_NAME_FOR_RATING = name;
            event_lat = EventAdapter.eventListingModelArrayList.get(0).getLatitude();
            event_lon = EventAdapter.eventListingModelArrayList.get(0).getLongitude();
            court_type = EventAdapter.eventListingModelArrayList.get(0).getCourt_type();
            skillLevel = EventAdapter.eventListingModelArrayList.get(0).getSkill_level();
            String noOfParticipants = EventAdapter.eventListingModelArrayList.get(0).getEvent_Total_Participants();
            eventDate = EventAdapter.eventListingModelArrayList.get(0).getEventDate();
            String eventStartTIme = EventAdapter.eventListingModelArrayList.get(0).getEventStartTime();
            String eventEndTIme = EventAdapter.eventListingModelArrayList.get(0).getEventEndTime();
            Constants.EVENT_END_TIME = eventEndTIme;
            //String eventPrice = EventAdapter.eventListingModelArrayList.get(0).getEvent_Price();
            String eventCourtName = EventAdapter.eventListingModelArrayList.get(0).getEvent_court_name();
            String venue_name = EventAdapter.eventListingModelArrayList.get(0).getVenue_name();
            eventId = EventAdapter.eventListingModelArrayList.get(0).getEvent_Id();
            eventSportName = EventAdapter.eventListingModelArrayList.get(0).getEvent_Sports_Type();
            String event_creator_name = EventAdapter.eventListingModelArrayList.get(0).getCreator_name();
            String event_creator_image = EventAdapter.eventListingModelArrayList.get(0).getEvent_Creator_Image();
            eventAddress = EventAdapter.eventListingModelArrayList.get(0).getAddress();
            visibility = EventAdapter.eventListingModelArrayList.get(0).getVisibility();
            note = EventAdapter.eventListingModelArrayList.get(0).getNote();
            list_Id = EventAdapter.eventListingModelArrayList.get(0).getList_id();
            Constants.LIST_ID = list_Id;
            start_time = EventAdapter.eventListingModelArrayList.get(0).getEventStartTime();
            end_time = EventAdapter.eventListingModelArrayList.get(0).getEventEndTime();
            Constants.CANCELLATION_POLICY_TYPE = EventAdapter.eventListingModelArrayList.get(0).getCancel_policy_type();
            Constants.CANCELLATION_POLICY_TYPE_POINT = EventAdapter.eventListingModelArrayList.get(0).getCancellation_point();
            AppConstants.OBJECT_TYPE_GAME = EventAdapter.eventListingModelArrayList.get(0).getObject_Type();
            eventLat = EventAdapter.eventListingModelArrayList.get(0).getLatitude();
            eventLon = EventAdapter.eventListingModelArrayList.get(0).getLongitude();
            total_price = EventAdapter.eventListingModelArrayList.get(0).getEvent_Total_Price();

            String review_rating = EventAdapter.eventListingModelArrayList.get(0).getEvent_Reviews();
            String review_count = EventAdapter.eventListingModelArrayList.get(0).getEvent_Ratings();
            court_total_player_capacity = EventAdapter.eventListingModelArrayList.get(0).getCapacity();

            current_lat = Double.parseDouble(eventLat);
            current_long = Double.parseDouble(eventLon);

            total_player_count = EventAdapter.eventListingModelArrayList.get(0).getEvent_Total_Participants();
            accepted_player_count = EventAdapter.eventListingModelArrayList.get(0).getEvent_accepted_Participants();

            //Event Max players and accepted players
            event_accepted_player = Integer.valueOf(accepted_player_count);
            event_max_player = Integer.valueOf(total_player_count);

            int spots_left = Integer.valueOf(total_player_count) - Integer.valueOf(accepted_player_count);
            getMinimumPrice = EventAdapter.eventListingModelArrayList.get(0).getEvent_Minimum_Price();
            boolean isFavorite = EventAdapter.eventListingModelArrayList.get(0).isFavorite();
            boolean isEquipmentPrivided = EventAdapter.eventListingModelArrayList.get(0).isEquipmentAvailable();
            if(isFavorite){
                fav_status = true;
                ic_favorite.setFavorite(true);
            }

            tv_eventDetailsPlayer.setText(accepted_player_count /*+ " of " + total_player_count*/);
            if(!TextUtils.isEmpty(event_creator_name)){
                tv_CreatorUsername.setText(event_creator_name);
            }else{
                tv_CreatorUsername.setText("Name Display");
            }

            if(!TextUtils.isEmpty(event_creator_image.trim())){
                Glide.with(EventDetail.this).load(event_creator_image).into(iv_creator_profile_image);
            }

            ArrayList<String> imagesArray =
                    EventAdapter.eventListingModelArrayList.get(0).getImages_array();

            if(imagesArray.size() > 0){
                /**
                 * Set images inside view pager
                 */
                setImagesInViewPager(imagesArray);
            }else{
                int getDefaultImages = SportsImagePicker.getDefaultSportImage(eventSportName);
                event_detail_image_frame.setBackgroundResource(getDefaultImages);
            }


            if(!TextUtils.isEmpty(list_Id)) {
                if (spots_left > 1) {
                    tv_spots_left.setText(spots_left + " spots Remaining");
                    tv_eventspots.setText(spots_left + " spots remaining");
                } else {
                    tv_spots_left.setText(spots_left + " spot Remaining");
                    tv_eventspots.setText(spots_left + " spot remaining");
                }
            }
            else{
                if (spots_left > 1) {
                    tv_spots_left.setText(spots_left + " spots Remaining");
                    poi_spots_remaining.setText(spots_left + " spots Remaining");
                } else {
                    tv_spots_left.setText(spots_left + " spot Remaining");
                    poi_spots_remaining.setText(spots_left + " spot Remaining");
                }
                poi_total_participants.setText(accepted_player_count);
                players_price_main_layout.setVisibility(View.GONE);
                detail_view2.setVisibility(View.GONE);

            }

            if(court_type.equalsIgnoreCase("Outdoor")) {
                indoor_icon_event.setImageResource(R.mipmap.ic_court_outdoor);
            } else{
                indoor_icon_event.setImageResource(R.mipmap.ic_court_indoor);
            }

            event_venue_type.setText(court_type);

            tv_sport_type_event.setText(eventSportName);
            eventSkillLevel.setText(skillLevel);
            tv_noOfParticipants.setText(noOfParticipants);
            tv_player_count.setText(accepted_player_count + " Participants");

            /*if(!TextUtils.isEmpty(total_price) &&
                    !total_price.equalsIgnoreCase("0")){
                priceEventDetails.setText("$ "+total_price);
                tv_minimum_price.setText("As low as $" + getMinimumPrice);
            }else{
                priceEventDetails.setText("FREE");
                tv_minimum_price.setVisibility(View.GONE);
                tv_priceperplayertxt.setVisibility(View.GONE);
            }*/

            /**Displaying player price depending upon the object type*/
            displayPlayerPrice();

            if(isEquipmentPrivided){
                SportsImagePicker sport = new SportsImagePicker();
                int isEquipmentAvailable = sport.getSportImage(eventSportName);
//                img_equipment_available.setImageResource(isEquipmentAvailable);
//                tv_equipments.setText("Equipments Provided");
            }


            if(AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Game") || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("game"))
            {
                String minAge = EventAdapter.eventListingModelArrayList.get(0).getCourt_min_players();
                String spaceType = EventAdapter.eventListingModelArrayList.get(0).getSpace_type();
                String courtSpaceDesc = EventAdapter.eventListingModelArrayList.get(0).getCourt_space_desc();

                int getCourtSpaceTypeImage = SportsSurfaceTypePicker.getSportSurfaceType(spaceType);
                court_icon_event.setBackgroundResource(getCourtSpaceTypeImage);
                tv_equipments.setText(minAge);
                court_name_event_detail.setText(courtSpaceDesc);

                add_info_text_game_detail.setVisibility(View.VISIBLE);
            }

            else{
                add_info_text_game_detail.setVisibility(View.GONE);
            }




            if(eventDate.contains(" ")) {
                tv_datetime.setText(ApplicationUtility.formattedDateContainSpace(eventDate) +"\n" + eventStartTIme + " to "+eventEndTIme);
            }
            else{
                tv_datetime.setText(ApplicationUtility.formattedDate(eventDate) +"\n" + eventStartTIme + " to "+eventEndTIme);
            }

            event_detail_name.setText(EventAdapter.eventListingModelArrayList.get(0).getEvent_Name());

            /**Hiding Rating fields for Unregistered games*/
            if(TextUtils.isEmpty(list_Id))
            {
                rating_event_details_reviews.setVisibility(View.GONE);
                rating_event_details.setVisibility(View.GONE);
            }
            else{
                rating_event_details_reviews.setVisibility(View.VISIBLE);
                rating_event_details.setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(review_rating))
                {
                    rating_event_details_reviews.setText(review_rating + " Reviews");
                }
                else{
                    rating_event_details_reviews.setText("0 Reviews");
                }

                if(!TextUtils.isEmpty(review_count))
                {
                    rating_event_details.setRating(Float.parseFloat(review_count));
                }
                else{
                    rating_event_details.setRating(0);
                }
            }
            description_event_detail.setText(EventAdapter.eventListingModelArrayList.get(0).getDescription());

            /**
             * Read more functionality on text view
             */

            description_event_detail.post(new Runnable() {

                @Override
                public void run() {
                    if (description_event_detail.getLineCount() > 3) {
                        makeTextViewResizable(description_event_detail, 3, "Read More", true);
                    }

                }
            });


            /*ArrayList<String> imagesArray = EventAdapter.eventListingModelArrayList.get(0).getImages_array();
            if(!TextUtils.isEmpty(imagesArray.get(0))){
                Glide.with(this).load(imagesArray.get(0)).into(event_detail_image);
            }*/
            /*double current_loc_latitude = HomeFragment.currLocLatitude;
            double current_loc_longitude = HomeFragment.currLocLongitude;*/

            if(!TextUtils.isEmpty(event_lat)) {
                double dest_loc_latitude = Double.valueOf(event_lat);
                double dest_loc_longitude = Double.valueOf(event_lon);
                double distance = ApplicationUtility.distance(new GPSTracker(EventDetail.this).getLatitude(), new GPSTracker(EventDetail.this).getLongitude(),
                        dest_loc_latitude, dest_loc_longitude);
                event_detail_miles.setText(String.valueOf(distance) + " miles");
            } else{
                event_detail_miles.setText("");
            }

            //Displaying the Event address
            if(eventAddress != null){
                tv_event_address.setText(eventAddress);
            }else{
                tv_event_address.setVisibility(View.GONE);
            }

            /**Checking whether this game current status is Active or Past*/
            if(checkGameRunningStatus()) {
                //Handling the event join button visibility
                if(AppConstants.userIdArray.contains(userId)){
                    btn_join_game.setVisibility(View.GONE);
                }
                //Checking the event maximum player and the accepted player count and
                //Making the join button visible and invisible
                else if(event_accepted_player == event_max_player){
                    btn_join_game.setVisibility(View.GONE);
                    tv_available.setText("Not Available");
                    tv_available.setTextColor(Color.parseColor("#8B0000"));
                    Toast.makeText(EventDetail.this, "Event maximum player reached", Toast.LENGTH_LONG).show();
                }
            } else{
                btn_join_game.setVisibility(View.GONE);
                Toast.makeText(EventDetail.this, "This game is no longer available as it's already over !!", Toast.LENGTH_LONG).show();
            }


            /**Checking this event is created from WEB, so that the menu icon won't be display*/
            if(Constants.UPCOMING_PAST_TYPE.equalsIgnoreCase("UPCOMING")) {
                if (!TextUtils.isEmpty(AppConstants.OBJECT_TYPE_GAME)
                        && (AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event")
                        || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event"))) {

                    game_detail_menu_icon_layout.setVisibility(View.GONE);
                    court_layout_field.setVisibility(View.GONE);
                    view2_venue_details.setVisibility(View.GONE);
                    tv_minimum_price.setVisibility(View.INVISIBLE);

                    tv_game_detail_list_name.setVisibility(View.VISIBLE);
                    tv_game_detail_venue_name.setVisibility(View.VISIBLE);

                    tv_game_detail_list_name.setText(eventCourtName);
                    tv_game_detail_venue_name.setText(venue_name);

                } else {
                    if(!TextUtils.isEmpty(list_Id))
                    {
                        court_layout_field.setVisibility(View.VISIBLE);
                        view2_venue_details.setVisibility(View.VISIBLE);
                        poi_spot_remaining_layout.setVisibility(View.GONE);

                        tv_game_detail_list_name.setVisibility(View.VISIBLE);
                        tv_game_detail_venue_name.setVisibility(View.VISIBLE);

                        tv_game_detail_list_name.setText(eventCourtName);
                        tv_game_detail_venue_name.setText(venue_name);
                    }
                    else{
                        court_layout_field.setVisibility(View.GONE);
                        view2_venue_details.setVisibility(View.GONE);
                        add_info_text_game_detail.setVisibility(View.GONE);
                        poi_spot_remaining_layout.setVisibility(View.VISIBLE);

                        tv_game_detail_list_name.setVisibility(View.GONE);
                        tv_game_detail_venue_name.setVisibility(View.GONE);

                    }
                    /**Check whether this game is created by this logged user or not,
                     * so that edit game icon will be displayed*/
                    if (userId.equals(game_Creator_userId)) {
                        game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
                    } else if (AppConstants.userIdArray.contains(userId)) {
                        game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
                    } else {
                        game_detail_menu_icon_layout.setVisibility(View.GONE);
                    }
                }
            }

            if(AppConstants.OBJECT_TYPE_GAME != null &&
                    (AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event") ||
                            AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))){
                VenueAdapter.venueDetailModelArrayList = new ArrayList<>();
                VenueDetailModel venueModel = new VenueDetailModel();
                venueModel.setName(name);
                venueModel.setId(eventId);
                venueModel.setReview_rating(review_rating);
                venueModel.setReview_count(review_count);
                venueModel.setAddress(eventAddress);
                venueModel.setImages_array(EventAdapter.eventListingModelArrayList.get(0).getImages_array());
                VenueAdapter.venueDetailModelArrayList.add(venueModel);
            }

            //Storing the Values
            storeValues(name,eventId, eventDate,
                    eventStartTIme + " to " +eventEndTIme,total_price,null,
                    noOfParticipants,eventSportName, eventCourtName);
        }catch (Exception exp)
        {
            exp.printStackTrace();
        }
    }




    /**
     * Read more & Less functionality
     *
     * @param tv
     * @param maxLine
     * @param expandText
     * @param viewMore
     */
    public void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }



    public SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                    final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new MySpannable(false) {
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "Read less", false);
                        join_btn_add_info.fullScroll(View.FOCUS_DOWN);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, "Read more", true);
                        join_btn_add_info.fullScroll(View.FOCUS_UP);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    /**
     * INITIALIZING ALL VIEWS BELONGS TO THIS LAYOUT
     */
    public void initReferences() {
        event_detail_back_icon_layout = (RelativeLayout) findViewById(R.id.event_detail_back_icon_layout);
        rating_event_details = (RatingBar) findViewById(R.id.rating_event_details);
        rating_event_details_reviews = (TextView) findViewById(R.id.rating_event_details_reviews);
        //event_detail_image = (ImageView) findViewById(R.id.event_detail_image);
        event_detail_name = (TextView) findViewById(R.id.event_detail_name);
        event_detail_miles = (TextView) findViewById(R.id.event_detail_miles);
        description_event_detail = (TextView) findViewById(R.id.description_event_detail);
        eventSkillLevel = (TextView) findViewById(R.id.event_sport_name);
        priceEventDetails = (TextView) findViewById(R.id.event_detail_price);
        event_venue_type = (TextView) findViewById(R.id.indoor_name_venue_detail);
        tv_noOfParticipants = (TextView) findViewById(R.id.total_member_name_venue_detail);
        tv_datetime = (TextView) findViewById(R.id.event_detail_date_time);
        tv_player_count = (TextView) findViewById(R.id.participant_title);
        tv_spots_left = (TextView) findViewById(R.id.spots_remaining_title);
        ic_favorite = (MaterialFavoriteButton) findViewById(R.id.favourite_event_detail);
        btn_join_game = (Button) findViewById(R.id.join_btn_add_info);
        tv_sport_type_event = (TextView) findViewById(R.id.description_title_event_detail);
        lay_participants = (RelativeLayout) findViewById(R.id.participant_sub_layout);
        tv_eventDetailsPlayer = (TextView) findViewById(R.id.event_detail_participants);
        iv_navigation = (ImageView) findViewById(R.id.navigation);
        viewpager_event  = (ViewPager) findViewById(R.id.viewpager_event);
        left_nav_event = (ImageButton) findViewById(R.id.left_nav_event);
        right_nav_event = (ImageButton) findViewById(R.id.right_nav_event);
        event_detail_image_frame = (FrameLayout) findViewById(R.id.event_detail_image_frame);
        iv_creator_profile_image = (CircleImageView) findViewById(R.id.event_creator_image);
        tv_CreatorUsername = (TextView) findViewById(R.id.event_creator_name);
        tv_priceperplayertxt = (TextView) findViewById(R.id.event_price_players);
        tv_eventspots = (TextView) findViewById(R.id.event_spots);
        join_btn_add_info = (ScrollView) findViewById(R.id.join_scroll);
        tv_minimum_price = (TextView) findViewById(R.id.event_detail_Minimum_Amount_Text);
        img_equipment_available = (ImageView) findViewById(R.id.equipments_icon);
        tv_equipments = (TextView) findViewById(R.id.equipments_name_venue_detail);
        tv_event_address = (TextView) findViewById(R.id.tv_event_address);
        iv_share_event_details = (ImageView) findViewById(R.id.share_event_detail);
        indoor_icon_event =  (ImageView) findViewById(R.id.indoor_icon_event);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        game_detail_menu_icon_layout = (RelativeLayout) findViewById(R.id.game_detail_menu_icon_layout);
        court_layout_field =  (LinearLayout) findViewById(R.id.court_layout_field);
        view2_venue_details = (View) findViewById(R.id.view2_venue_details);
        ratings_layout = (RelativeLayout) findViewById(R.id.ratings_event_layout);
        tv_available = (TextView) findViewById(R.id.event_detail_available_text);
        add_info_text_game_detail = (TextView) findViewById(R.id.add_info_text_game_detail);
        court_name_event_detail =  (TextView) findViewById(R.id.court_name_event_detail);
        court_icon_event =  (ImageView) findViewById(R.id.court_icon_event);
        poi_spot_remaining_layout = (RelativeLayout) findViewById(R.id.poi_spot_remaining_layout);
        poi_total_participants = (TextView) findViewById(R.id.poi_total_participants);
        poi_spots_remaining =  (TextView) findViewById(R.id.poi_spots_remaining);
        tv_game_detail_list_name = (TextView) findViewById(R.id.tv_game_detail_list_name);
        tv_game_detail_venue_name = (TextView) findViewById(R.id.tv_game_detail_venue_name);

        players_price_main_layout = (RelativeLayout) findViewById(R.id.players_price_main_layout);
        detail_view2 =  (View) findViewById(R.id.detail_view2);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }



    /**
     * Get Current Date and time
     * @return
     */
    public static String getCurrentDateAndTIme(){
        //TimeZone utc = TimeZone.getTimeZone("UTC");
        DateFormat dateFormatter = new SimpleDateFormat("dd/mm/yyyy hh:mm a", Locale.US);
       // dateFormatter.setTimeZone(utc);
        //dateFormatter.setLenient(false);
        Date today = new Date();
        String current_time = dateFormatter.format(today);
        return current_time;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.isUnRegisteredvenueGameCreated = false;
        //releasing the values
        releseValues();
        finish();
        AppConstants.isDeepLinkRecieved = false;
        if(Constants.FROM_WISHLIST ) {
            Constants.FROM_WISHLIST = false;
            AppConstants.userIdArray.clear();
            AppConstants.userModelArary.clear();
            releseDeeLinkData();
            Intent intent = new Intent(EventDetail.this, MyWishList.class);
            startActivity(intent);
        }else if(isEventDetailsApiCalled){
            isEventDetailsApiCalled = false;
            AppConstants.userIdArray.clear();
            AppConstants.userModelArary.clear();
            releseDeeLinkData();
            EventDetail.this.finish();
            startActivity(new Intent(EventDetail.this, SplashScreen.class));
        } else if(Constants.FROM_NOTIFICATION) {
            Constants.FROM_NOTIFICATION = false;
            finish();
        } else if(!TextUtils.isEmpty(Constants.PUSH_NOTIFICATION_GAME_ID) && Constants.FROM_PUSH_NOTIFICATION) {
            Constants.PUSH_NOTIFICATION_GAME_ID = "";
            Constants.FROM_PUSH_NOTIFICATION = false;
            finish();
        } else if(!TextUtils.isEmpty(Constants.PUSH_NOTIFICATION_GAME_ID)) {
            Constants.PUSH_NOTIFICATION_GAME_ID = "";
            if(NavigationDrawerActivity.baseActivity != null) {
                NavigationDrawerActivity.baseActivity.finish();
            }
            finish();
            startActivity(new Intent(EventDetail.this, SplashScreen.class));
        } else{
            Intent intent = new Intent(EventDetail.this, EventListing.class);
            startActivity(intent);
        }
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String userId, String objectId, boolean favoriteStatus) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "add_to_favorite").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("object_id", objectId);

        if(!TextUtils.isEmpty(AppConstants.OBJECT_TYPE_GAME)
                && ( AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")))
        {
            urlBuilder.addQueryParameter("object_type", "event");
        }
        else {
            urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_EVENT);
        }
        if(favoriteStatus){
            urlBuilder.addQueryParameter("is_favorite", Boolean.TRUE.toString());
        }else{
            urlBuilder.addQueryParameter("is_favorite", Boolean.FALSE.toString());
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }


    /**
     * Function Responsible for storing the data
     * @param eventName
     * @param eventId
     * @param eventdate
     * @param eventTime
     * @param eventPrice
     * @param eventCourt
     * @param eventPlayerCount
     * @param eventSportName
     */
    public static void storeValues(String eventName, String eventId,
                            String eventdate,String  eventTime,
                            String  eventPrice, String eventCourt,
                            String eventPlayerCount, String eventSportName,
                            String courtName){
        Constants.EVENT_NAME = eventName;
        Constants.CURRENT_EVENT_ID = eventId;
        Constants.EVENT_DATE = ApplicationUtility.formattedDate(eventdate);
        Constants.EVENT_TIME = eventTime;
        Constants.EVENT_PRICE = eventPrice;
        Constants.EVENT_VENUE_COURT = eventCourt;
        Constants.EVENT_PLAYERS_COUNT = eventPlayerCount;
        Constants.EVENT_PLAYERS_SPORT_NAME = eventSportName;
        Constants.EVENT_COURT_NAME = courtName;
    }

    /**
<<<<<<< HEAD
     * Function responsible for handling the hold values
     */
    public void releseValues() {
        AppConstants.userModelArary.clear();
        AppConstants.userIdArray.clear();
        Constants.EVENT_LONGITUDE = 0;
        Constants.EVENT_LATITUDE = 0;
        Constants.EVENT_NAME_MAP = null;
    }

        /**
         * Dispalying the Event images in view pager
         */
    public void setImagesInViewPager(ArrayList<String> images_array) {
        if(images_array.size() == 1) {
            ImageSlideAdapter imageSlideAdapter =
                    new ImageSlideAdapter(EventDetail.this,images_array);
            viewpager_event.setAdapter(imageSlideAdapter);

            left_nav_event.setVisibility(View.INVISIBLE);
            right_nav_event.setVisibility(View.INVISIBLE);
        } else if(images_array.size() > 0) {
            ImageSlideAdapter imageSlideAdapter =
                    new ImageSlideAdapter(EventDetail.this,images_array);
            viewpager_event.setAdapter(imageSlideAdapter);
        } else{
            //Display place holder image
            left_nav_event.setVisibility(View.INVISIBLE);
            right_nav_event.setVisibility(View.INVISIBLE);
            event_detail_image_frame.setBackgroundResource(R.mipmap.ic_no_image_found);
        }
    }

    /**
     * Set click events on the views
     */
    public void setClickEvents() {
        event_detail_back_icon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.isUnRegisteredvenueGameCreated = false;
                AppConstants.userModelArary.clear();
                AppConstants.userIdArray.clear();
                AppConstants.DEEP_LINKING_EVENT_ID = null;
                Constants.EVENT_LONGITUDE = 0;
                Constants.EVENT_LATITUDE = 0;
                Constants.EVENT_NAME_MAP = null;
                AppConstants.isDeepLinkRecieved = false;
                releseValues();
                finish();
                if(Constants.FROM_WISHLIST ) {
                    Constants.FROM_WISHLIST = false;
                    AppConstants.userIdArray.clear();
                    AppConstants.userModelArary.clear();
                    Intent intent = new Intent(EventDetail.this, MyWishList.class);
                    startActivity(intent);
                }else if(isEventDetailsApiCalled){
                    isEventDetailsApiCalled = false;
                    AppConstants.userIdArray.clear();
                    AppConstants.userModelArary.clear();
                    releseDeeLinkData();
                    EventDetail.this.finish();
                    startActivity(new Intent(EventDetail.this, SplashScreen.class));
                } else if(Constants.FROM_NOTIFICATION) {
                    Constants.FROM_NOTIFICATION = false;
                    finish();
                } else if(!TextUtils.isEmpty(Constants.PUSH_NOTIFICATION_GAME_ID) && Constants.FROM_PUSH_NOTIFICATION) {
                    Constants.PUSH_NOTIFICATION_GAME_ID = "";
                    Constants.FROM_PUSH_NOTIFICATION = false;
                    finish();
                } else if(!TextUtils.isEmpty(Constants.PUSH_NOTIFICATION_GAME_ID)) {
                    Constants.PUSH_NOTIFICATION_GAME_ID = "";
                    if(NavigationDrawerActivity.baseActivity != null) {
                        NavigationDrawerActivity.baseActivity.finish();
                    }
                    finish();
                    startActivity(new Intent(EventDetail.this, SplashScreen.class));
                } else{
                    Intent intent = new Intent(EventDetail.this, EventListing.class);
                    startActivity(intent);
                }
            }
        });

        btn_join_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Storing the values for further Use
                Summary.storeSharingValues(eventSportName, eventDate, start_time, end_time);
                Intent intent = new Intent(EventDetail.this, JoinGameConfirmationScreen.class);
                startActivity(intent);
            }
        });

        //Event added for favorite button
        ic_favorite.setOnFavoriteAnimationEndListener(
                new MaterialFavoriteButton.OnFavoriteAnimationEndListener() {
                    @Override
                    public void onAnimationEnd(MaterialFavoriteButton buttonView, boolean favorite) {
                        if (favorite) {
                            if(!fav_status){
                                makeListFavorite(userId,eventId,favorite);
                            }
                            fav_status = false;
                        } else {
                            makeListFavorite(userId,eventId,favorite);
                        }
                    }
                });

        lay_participants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EventDetail.this, EventParticipantsScreen.class);
                startActivity(intent);
            }
        });

        iv_navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EventDetail.this, ShowDirections.class);
                startActivity(intent);
            }
        });

        // Images left navigation
        left_nav_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewpager_event.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    viewpager_event.setCurrentItem(tab);
                } else if (tab == 0) {
                    viewpager_event.setCurrentItem(tab);
                }
            }
        });

        // Images right navigatin
        right_nav_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewpager_event.getCurrentItem();
                tab++;
                viewpager_event.setCurrentItem(tab);
            }
        });

        iv_share_event_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Function call to share the event in social
                // plat forms
                displayShareDialog();

            }
        });

        game_detail_menu_icon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayMenuPopUp();
            }
        });

        ratings_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AppConstants.OBJECT_TYPE_GAME != null &&
                        (AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event") ||
                                AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))){
                   startActivity(new Intent(EventDetail.this, ReviewDetail.class));
                }
            }
        });

        /**More Information text clicked*/
        add_info_text_game_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EventDetail.this,ListingAdditionalInfo.class);
                intent.putExtra("FROM_PAGE","GAME");
                startActivity(intent);
            }
        });
    }


    /**
     * Function responsible for sharing the game details in facebook
     */
    public void shareGameDetails(){
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    //.setContentTitle("Squadz Game Posts")
                    .setContentDescription(eventSportName + " on " + eventDate + " at "+ start_time + " to " + end_time)
                    .setContentUrl(Uri.parse(EndPoints.BASE_URL_DEEP_LINKING+"game/"+Constants.CURRENT_EVENT_ID))
                    //.setContentUrl(Uri.parse("http://34.192.117.108"))
                    .build();
            ShareDialog shareDialog_new = new ShareDialog(EventDetail.this);
            shareDialog_new.show(linkContent, ShareDialog.Mode.NATIVE);
            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                @Override
                public void onSuccess(Sharer.Result result) {
                    Toast.makeText(EventDetail.this, "Shared your post successfully", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancel() {
                    //Toast.makeText(FinishEventCreation.this, "Cancel", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException e) {
                    e.printStackTrace();
                    Toast.makeText(EventDetail.this,  e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            snackBar.setSnackBarMessage("Please check your internet connection");
        }
        /**Menu icon click*/
        game_detail_menu_icon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayMenuPopUp();
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public void relesePushNotificationValues(){
        Constants.PUSH_NOTIFICATION_GAME_ID = null;
        AppConstants.DEEP_LINKING_EVENT_ID = null;
    }

    /**
     * Function to get Venue Courts detail from server
     */
    public void getVenuesCourtsDetailsInfor(String court_Id, String userId) {
        //Handling the loader state
        LoaderUtility.handleLoader(EventDetail.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(court_Id, userId);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                EventDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        relesePushNotificationValues();

                        if(!TextUtils.isEmpty(Constants.PUSH_NOTIFICATION_GAME_ID)) {
                            isEventDetailsApiCalled = false;
                        } else if (Constants.isEventUpdated) {
                            Constants.isEventUpdated = false;
                            isEventDetailsApiCalled = false;
                        } else{
                            isEventDetailsApiCalled = true;
                        }
                        LoaderUtility.handleLoader(EventDetail.this, false);
                        Toast.makeText(EventDetail.this,"Something went wrong", Toast.LENGTH_LONG).show();
                    }
                });


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                boolean isSuccessStatus = true;
                String id = "",venue_id ="",capacity = "", name = "",
                        latitude = "", longitude = "",review_rating = "",
                        review_count = "",court_type = "",description = "",
                        price = "",address = "",city = "",state = "",
                        country = "", total_number_players = "",
                        game_date = "", startTIme = "", endTime = "",
                        skillLevel = "", no_of_participants = "",
                        eventSportName = "", eventCourtName = "",
                        event_creator_name = "", event_creator_image = "",
                        event_creator_user_id = "", minimum_price = "",
                        actual_price = "",list_id = "",object_type = "", cancel_policy_type = "",
                        cancellation_point = "",minimum_age = "", space_type = "", space_desc = "",
                        phone = "", website = "", rules = "",venue_name = "";
                boolean isFavorite = false, isEquipmenmtAvailable = false;
                ArrayList<String> space_desc_array = new ArrayList<String>();
                ArrayList<String> equipments_array = new ArrayList<String>();
                ArrayList<String> amenities_array = new ArrayList<String>();

                try {
                    //JSONArray jsonArrays_venue = new JSONArray(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    EventAdapter.eventListingModelArrayList = new ArrayList<EventListingModel>();
                    eventListingModel = new EventListingModel();
                    ArrayList<String> images_arraylist = new ArrayList<String>();

                    if(jsonObject.has("id") && !jsonObject.isNull("id")) {
                        id = jsonObject.getString("id");
                    }
                    if(jsonObject.has("venue_id") && !jsonObject.isNull("venue_id")) {
                        venue_id = jsonObject.getString("venue_id");
                    }
                    if(jsonObject.has("list_id") && !jsonObject.isNull("list_id")) {
                        list_id = jsonObject.getString("list_id");
                    }
                    if(jsonObject.has("name") && !jsonObject.isNull("name")) {
                        name = jsonObject.getString("name");
                        Constants.EVENT_NAME_MAP = name;
                    }
                    if(jsonObject.has("latitude") && !jsonObject.isNull("latitude")) {
                        latitude = jsonObject.getString("latitude");
                        Constants.EVENT_LATITUDE = Double.parseDouble(latitude);
                    }
                    if(jsonObject.has("longitude") && !jsonObject.isNull("longitude")) {
                        longitude = jsonObject.getString("longitude");
                        Constants.EVENT_LONGITUDE = Double.parseDouble(longitude);
                    }
                    if(jsonObject.has("review_rating") && !jsonObject.isNull("review_rating")) {
                        review_rating = jsonObject.getString("review_rating");
                    }

                    if(jsonObject.has("review_count") && !jsonObject.isNull("review_count")) {
                        review_count = jsonObject.getString("review_count");
                    }

                    if(jsonObject.has("court_type") && !jsonObject.isNull("court_type")) {
                        court_type = jsonObject.getString("court_type");
                    }
                    if(jsonObject.has("description") && !jsonObject.isNull("description")) {
                        description = jsonObject.getString("description");
                    }
                    if(jsonObject.has("address") && !jsonObject.isNull("address")) {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.has("city") && !jsonObject.isNull("city")) {
                        city = jsonObject.getString("city");
                    }
                    if(jsonObject.has("state") && !jsonObject.isNull("state")) {
                        state = jsonObject.getString("state");
                    }
                    if(jsonObject.has("country") && !jsonObject.isNull("country")) {
                        country = jsonObject.getString("country");
                    }

                    if(jsonObject.has("no_of_player") && !jsonObject.isNull("no_of_player")){
                        total_number_players = jsonObject.getString("no_of_player");
                    }

                    if(jsonObject.has("no_of_partitipants") && !jsonObject.isNull("no_of_partitipants")){
                        no_of_participants = jsonObject.getString("no_of_partitipants");
                    }

                    if(jsonObject.has("is_favorite") && !jsonObject.isNull("is_favorite")){
                        isFavorite = jsonObject.getBoolean("is_favorite");
                    }

                    if(jsonObject.has("price") && !jsonObject.isNull("price")) {
                        try{
                            price = jsonObject.getString("price");
                            int totalPlayer = Integer.valueOf(no_of_participants) + 1;
                            float price_pre_player = Float.parseFloat(price) / (float)totalPlayer;
                            actual_price = String.valueOf(new DecimalFormat("##.##").format(price_pre_player));
                            float minimum_price_per_player = Float.parseFloat(price) / Float.parseFloat(total_number_players);
                            minimum_price = String.valueOf(new DecimalFormat("##.##").format(minimum_price_per_player));
                        }catch(Exception ex){
                            ex.printStackTrace();;
                        }
                    }

                    if(jsonObject.has("sport_name") && !jsonObject.isNull("sport_name")){
                        eventSportName = jsonObject.getString("sport_name");
                    }

                    if(jsonObject.has("game_date") && !jsonObject.isNull("game_date")){
                        game_date = jsonObject.getString("game_date");
                    }

                    if(jsonObject.has("skill_level") && !jsonObject.isNull("skill_level")){
                        skillLevel = jsonObject.getString("skill_level");
                    }

                    if(jsonObject.has("list_name") && !jsonObject.isNull("list_name")){
                        eventCourtName = jsonObject.getString("list_name");
                    }
                    if(jsonObject.has("venue_name") && !jsonObject.isNull("venue_name")){
                        venue_name = jsonObject.getString("venue_name");
                    }

                    if(jsonObject.has("user_name") && !jsonObject.isNull("user_name")){
                        event_creator_name = jsonObject.getString("user_name");
                    }

                    if(jsonObject.has("user_id") && !jsonObject.isNull("user_id")){
                        event_creator_user_id = jsonObject.getString("user_id");
                    }

                    if(jsonObject.has("user_profile_image") && !jsonObject.isNull("user_profile_image")){
                        event_creator_image = jsonObject.getString("user_profile_image");
                    }

                    if(jsonObject.has("visibility") && !jsonObject.isNull("visibility")){
                        visibility = jsonObject.getString("visibility");
                    }
                    if(jsonObject.has("note") && !jsonObject.isNull("note")) {
                        note = jsonObject.getString("note");
                    }
                    if(jsonObject.has("capacity") && !jsonObject.isNull("capacity")) {
                        capacity = jsonObject.getString("capacity");
                    }
                    if(jsonObject.has("object_type")&& !jsonObject.isNull("object_type"))
                    {
                        object_type = jsonObject.getString("object_type");
                    }

                    if(jsonObject.has("cancel_policy_type")&& !jsonObject.isNull("cancel_policy_type")) {
                        cancel_policy_type = jsonObject.getString("cancel_policy_type");
                    }
                    if(jsonObject.has("cancellation_point")&& !jsonObject.isNull("cancellation_point")) {
                        cancellation_point = jsonObject.getString("cancellation_point");
                    }

                    if(jsonObject.has("equipments") && !jsonObject.isNull("equipments")){
                        JSONArray equipmentArray = jsonObject.getJSONArray("equipments");

                        if(equipmentArray.length() > 0){
                            isEquipmenmtAvailable = true;
                        }
                    }

                    if(jsonObject.has("time_slot")&& !jsonObject.isNull("time_slot")) {
                        JSONArray time_slot_array_json_array = jsonObject.getJSONArray("time_slot");
                        for (int j = 0; j < time_slot_array_json_array.length(); j++) {
                            JSONObject getValues = time_slot_array_json_array.getJSONObject(j);
                            startTIme = getValues.getString("start_time");
                            endTime = getValues.getString("end_time");
                            /*for(int x = 0; x < getValues.length(); x++){
                                if(j == 0){
                                    startTIme = getValues.getString(1);
                                }else{
                                    endTime = getValues.getString(1);
                                }
                            }*/
                        }
                    }


                    if(jsonObject.has("images")&& !jsonObject.isNull("images")) {
                        ArrayList<String> images_array = new ArrayList<String>();
                        JSONArray images_json_array = jsonObject.getJSONArray("images");
                        for (int j = 0; j < images_json_array.length(); j++) {
                            images_array.add(images_json_array.getString(j));
                        }
                        eventListingModel.setImages_array(images_array);
                    }


                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        JSONObject add_info_json_object = jsonObject.getJSONObject("additional_info");

                        JSONArray jsonArray = add_info_json_object.getJSONArray("space_desc");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String value = jsonArray.get(i).toString();
                            space_desc_array.add(value);
                        }

                        if(add_info_json_object.has("equipments")&& !add_info_json_object.isNull("equipments")) {

                            JSONArray equipments_json_array = add_info_json_object.getJSONArray("equipments");
                            for (int i = 0; i < equipments_json_array.length(); i++) {
                                String value = equipments_json_array.get(i).toString();
                                equipments_array.add(value);
                            }

                        }
                        if(add_info_json_object.has("amenities")&& !add_info_json_object.isNull("amenities")) {

                            JSONArray amenities_json_array = add_info_json_object.getJSONArray("amenities");
                            for (int i = 0; i < amenities_json_array.length(); i++) {
                                String value = amenities_json_array.get(i).toString();
                                amenities_array.add(value);
                            }
                        }
                        if(add_info_json_object.has("phone")&& !add_info_json_object.isNull("phone")) {
                            phone = add_info_json_object.getString("phone");
                        }
                        if(add_info_json_object.has("website")&& !add_info_json_object.isNull("website")) {
                            website = add_info_json_object.getString("website");
                        }
                        if(add_info_json_object.has("rules")&& !add_info_json_object.isNull("rules")) {
                            rules = add_info_json_object.getString("rules");
                        }

                        eventListingModel.setAdditional_space_desc_array(space_desc_array);
                        eventListingModel.setAdditional_equipmentes_array(equipments_array);
                        eventListingModel.setAdditional_amenities_array(amenities_array);
                        eventListingModel.setAdditional_phone_number(phone);
                        eventListingModel.setAdditional_website(website);
                        eventListingModel.setAdditional_venue_rules(rules);
                    }


                    if(jsonObject.has("minimum_age")&& !jsonObject.isNull("minimum_age")) {
                        minimum_age = jsonObject.getString("minimum_age");
                        if(minimum_age != null && !minimum_age.equalsIgnoreCase("No minimum age required")){
                            minimum_age = minimum_age + " + ";
                        }
                    }

                    if(jsonObject.has("space_type")&& !jsonObject.isNull("space_type")) {
                        space_type = jsonObject.getString("space_type");
                    }

                    if(jsonObject.has("additional_info")&& !jsonObject.isNull("additional_info")) {
                        String space = "";
                        JSONObject jsonObject_additional_info = jsonObject.getJSONObject("additional_info");

                        if(jsonObject_additional_info.has("space_desc") && !jsonObject_additional_info.isNull("space_desc")){
                            JSONArray spaceArray = jsonObject_additional_info.getJSONArray("space_desc");
                            for (int i = 0; i < spaceArray.length(); i++) {

                                // if(!TextUtils.isEmpty(space)){
                                space = space + "," + spaceArray.get(i);
                                // }
                            }

                            if(TextUtils.isEmpty(space)){
                                space_desc = "No Space Available";
                            }else {
                                if (space != null
                                        && space
                                        .length() > 1) {
                                    space_desc = space.substring(1,space.length());
                                }
                                //space_desc = space;
                            }
                        }
                    }


                    if(jsonObject.has("game_partitipants")&& !jsonObject.isNull("game_partitipants")){
                        try{
                            JSONArray particiapntsArray = jsonObject.getJSONArray("game_partitipants");

                            if(particiapntsArray.length() > 0){
                                //CLearing all the previous values
                                AppConstants.userIdArray.clear();
                                AppConstants.userModelArary.clear();
                                for(int i = 0; i < particiapntsArray.length(); i++){
                                    UserMolel model = new UserMolel();
                                    JSONObject participantsObj = particiapntsArray.getJSONObject(i);
                                    String userName = participantsObj.getString("user_name");
                                    String userId = participantsObj.getString("user_id");
                                    String user_profile_image = participantsObj.getString("user_profile_image");
                                    model.setUserFullName(userName);
                                    model.setUserId(userId);
                                    model.setUserProfileImage(user_profile_image);
                                    AppConstants.userIdArray.add(userId);
                                    AppConstants.userModelArary.add(model);
                                }
                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }
                    eventListingModel.setEvent_Id(id);
                    eventListingModel.setList_id(list_id);
                    eventListingModel.setEvent_Name(name);
                    eventListingModel.setLatitude(latitude);
                    eventListingModel.setLongitude(longitude);
                    eventListingModel.setEvent_Ratings(review_rating);
                    eventListingModel.setEvent_Reviews(review_count);
                    eventListingModel.setEvent_Price(actual_price);
                    eventListingModel.setCourt_type(court_type);
                    eventListingModel.setDescription(description);
                    eventListingModel.setAddress(address);
                    eventListingModel.setCity(city);
                    eventListingModel.setState(state);
                    eventListingModel.setCountry(country);
                    eventListingModel.setCapacity(capacity);
                    eventListingModel.setEvent_Total_Participants(total_number_players);
                    eventListingModel.setEventDate(game_date);
                    eventListingModel.setEventStartTime(startTIme);
                    eventListingModel.setEventEndTime(endTime);
                    eventListingModel.setSkill_level(skillLevel);
                    eventListingModel.setEvent_accepted_Participants(no_of_participants);
                    eventListingModel.setEvent_Sports_Type(eventSportName);
                    eventListingModel.setEvent_court_name(eventCourtName);
                    eventListingModel.setEvent_Creator_Image(event_creator_image);
                    eventListingModel.setCreator_name(event_creator_name);
                    eventListingModel.setCreator_user_id(event_creator_user_id);
                    eventListingModel.setEvent_Minimum_Price(minimum_price);
                    eventListingModel.setFavorite(isFavorite);
                    eventListingModel.setVisibility(visibility);
                    eventListingModel.setNote(note);
                    eventListingModel.setObject_Type(object_type);
                    eventListingModel.setEquipmentAvailable(isEquipmenmtAvailable);
                    eventListingModel.setEvent_Total_Price(price);
                    eventListingModel.setCancel_policy_type(cancel_policy_type);
                    eventListingModel.setCancellation_point(cancellation_point);
                    eventListingModel.setCancellation_point(cancellation_point);
                    eventListingModel.setCourt_min_players(minimum_age);
                    eventListingModel.setCourt_space_desc(space_desc);
                    eventListingModel.setSpace_type(space_type);
                    eventListingModel.setVenue_name(venue_name);

                    EventAdapter.eventListingModelArrayList.add(eventListingModel);
                }catch (Exception exp) {
                    isSuccessStatus = false;
                    exp.printStackTrace();
                }
                //Handling the loader state
                LoaderUtility.handleLoader(EventDetail.this, false);

                if(isSuccessStatus) {
                    EventDetail.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            relesePushNotificationValues();
                            if(!TextUtils.isEmpty(Constants.PUSH_NOTIFICATION_GAME_ID)) {
                                isEventDetailsApiCalled = false;
                            } else if (Constants.isEventUpdated || Constants.isEventCreated) {
                                Constants.isEventUpdated = false;
                                Constants.isEventCreated = false;
                                isEventDetailsApiCalled = false;
                            } else{
                                isEventDetailsApiCalled = true;
                            }

                            setEventData();
                            //releseDeeLinkData();
                            /**
                             * INITIALIZING MAP WITH GPS TRACKER
                             */
                            initMapGps();

                        }
                    });
                } else {
                    isEventDetailsApiCalled = true;
                    snackBar.setSnackBarMessage("Network error! please try after sometime");
                }
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String court_Id, String userId) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "get_game_detail").newBuilder();
        urlBuilder.addQueryParameter(Constants.EVENT_ID, court_Id);
        urlBuilder.addQueryParameter("user_id", userId);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Relesing the facebook Deep Link Data
     */
    public static void  releseDeeLinkData(){
        AppConstants.DEEP_LINKING_EVENT_ID = null;
        AppConstants.isDeepLinkRecieved = false;
    }

    /**
     * Display Menu pop up window
     */
    public void displayMenuPopUp() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.edit_game_menu_info_popup);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        wlp.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        TextView edit_game_text = (TextView)dialog.findViewById(R.id.edit_game_text);
        edit_game_text.setVisibility(View.GONE);
        TextView tv_cancel_game = (TextView)dialog.findViewById(R.id.cancel_game_text);
        tv_cancel_game.setVisibility(View.GONE);
        TextView tv_leave_game = (TextView)dialog.findViewById(R.id.leave_game_text);
        tv_leave_game.setVisibility(View.GONE);

        if(userId.equals(game_Creator_userId)) {
            //game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
            edit_game_text.setVisibility(View.VISIBLE);
            tv_cancel_game.setVisibility(View.VISIBLE);
        } else if(AppConstants.userIdArray.contains(userId)){
            //game_detail_menu_icon_layout.setVisibility(View.VISIBLE);
            tv_leave_game.setVisibility(View.VISIBLE);
        } else{
            game_detail_menu_icon_layout.setVisibility(View.GONE);
        }

        final String eventId = EventAdapter.eventListingModelArrayList.get(0).getEvent_Id();

        edit_game_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Constants.IS_EDIT_GAME_CLICKED = true;
                Constants.GAME_NAME = name;
                Constants.GAME_VISIBILITY = visibility;
                Constants.SKILL_LEVEL = skillLevel;
                Constants.NOTE = note;
                Constants.COURT_MAX_PLAYER_ = event_max_player;

                /*Keeping Court Total Player Count*/
                if(!TextUtils.isEmpty(court_total_player_capacity)) {
                    Constants.COURT_TOTAL_MAX_PLAYER_EDIT = Integer.parseInt(court_total_player_capacity);
                }
                if(!TextUtils.isEmpty(accepted_player_count)) {
                    Constants.NO_OF_PARTICIPANT = Integer.parseInt(accepted_player_count);
                }

                if(!TextUtils.isEmpty(list_Id)) {
                    startActivity(new Intent(EventDetail.this, Additionalinfo.class));
                } else{
                    Constants.POI_GAME_ADDRESS = eventAddress;
                    Constants.POI_GAME_DATE = eventDate;
                    Constants.POI_GAME_START_TIME = start_time;
                    Constants.POI_GAME_END_TIME = end_time;
                    Constants.POI_GAME_END_COURT_TYPE = court_type;
                    Constants.GAME_VISIBILITY = visibility;
                    Constants.SKILL_LEVEL = skillLevel;
                    Constants.COURT_MAX_PLAYER_ = event_max_player;
                    Constants.POI_LATITUDE = eventLat;
                    Constants.POI_LONGITUDE = event_lon;
                    if(!TextUtils.isEmpty(accepted_player_count)) {
                        Constants.NO_OF_PARTICIPANT = Integer.parseInt(accepted_player_count);
                    }
                    startActivity(new Intent(EventDetail.this, CreateEventWithoutCourt.class));
                }
            }
        });

        tv_cancel_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                type = "cancel_game";
                if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE_POINT)) {
                    long upcoming_time_milisecond_policy = getUpcomingTimeInMiliseonds(Integer.parseInt(Constants.CANCELLATION_POLICY_TYPE_POINT));
                    long time_slots_milisecond_policy = getTimeSlotsDateTimeinMilisecond();

                    if(time_slots_milisecond_policy < upcoming_time_milisecond_policy) {
                        String policyTitle = "Cancellation Policy\n ( "+Constants.CANCELLATION_POLICY_TYPE + " )";
                        String policyDescription = "The payment is non-refundable, because you have crossed the cancellation point.";

                        cancellationPolicyDialog(policyTitle,policyDescription);
                    } else{
                        cancelGame(eventId);
                    }
                }
                else{
                    cancelGame(eventId);
                }
            }
        });

        tv_leave_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "";
                if(!TextUtils.isEmpty(Constants.CANCELLATION_POLICY_TYPE_POINT)) {
                    long upcoming_time_milisecond_policy = getUpcomingTimeInMiliseonds(Integer.parseInt(Constants.CANCELLATION_POLICY_TYPE_POINT));
                    long time_slots_milisecond_policy = getTimeSlotsDateTimeinMilisecond();

                    if(time_slots_milisecond_policy < upcoming_time_milisecond_policy) {
                        String policyTitle = "Cancellation Policy\n ( "+Constants.CANCELLATION_POLICY_TYPE + " )";
                        String policyDescription = "The payment is non-refundable, because you have crossed the cancellation point.";

                        cancellationPolicyDialog(policyTitle,policyDescription);
                    } else{
                        leaveGame(eventId);
                    }
                }
                else{
                    leaveGame(eventId);
                }
            }
        });

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Constants.FROM_WISHLIST_GAME_UPDATED) {
            Constants.FROM_WISHLIST_GAME_UPDATED = false;
            getVenuesCourtsDetailsInfor(WishListAdapter.venueId,userId);
        } else if(Constants.isEventUpdated || Constants.isEventCreated) {
            getVenuesCourtsDetailsInfor(EventAdapter.event_Id,userId);
        }

        //Refreshing the Review Count
        if(Constants.REVIEW_ADDED_EVENT){
            Constants.REVIEW_ADDED_EVENT = false;
            rating_event_details_reviews.setText(String.valueOf(Constants.REVIEW_LIST_COUNT) + " Reviews");
            Constants.REVIEW_LIST_COUNT = 0;
        }

    }



    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyLeaveGame(String game_id){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE+"leave_game?").newBuilder();
        urlBuilder.addQueryParameter("game_id", game_id);
        urlBuilder.addQueryParameter("user_id", userId);
        urlBuilder.addQueryParameter("leaved_at", getCurrentDateAndTIme());
        urlBuilder.addQueryParameter("is_refundable", is_Refundable_Status);//true/false
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Calling the API to getting the sports
     */

    public void leaveGame(String eventId){
        //Displaying loader
        LoaderUtility.handleLoader(EventDetail.this, true);
        // should be a singleton
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBodyLeaveGame(eventId);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                SplashScreen.displayMessageInUiThread(EventDetail.this,e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages
                        displayMessage(message);
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                EventDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            Constants.LEAVED_STATUS = true;
                            EventDetail.this.finish();
                        }
                    }
                });
                LoaderUtility.handleLoader(EventDetail.this, false);
            }
        });
    }

    /**
     * Dispaly ing the message
     * @param message status message
     */
    public void displayMessage(final String message){
        EventDetail.this.runOnUiThread(new Runnable() {
            public void run() {

                //Displaying the success message after successful sign up
                //Toast.makeText(ForgotPasswordScreen.this,message, Toast.LENGTH_LONG).show();
                // snackbar.setSnackBarMessage(message);
                Toast.makeText(EventDetail.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Function responsible for displaying of the dialog
     */
    public void cancellationPolicyDialog(String policy_title, String policy_description){
        mMaterialDialog = new MaterialDialog(EventDetail.this);
        View view = LayoutInflater.from(EventDetail.this)
                .inflate(R.layout.layout_dialog_cancellation_policy,
                        null);

        TextView tv_cancellation_policy_title = (TextView) view.findViewById(R.id.tv_cancellation_policy_title);
        TextView tv_cancellation_policy = (TextView) view.findViewById(R.id.tv_cancellation_policy);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel_policy);
        TextView tv_confirm = (TextView) view.findViewById(R.id.tv_confirm_policy);

        tv_cancellation_policy_title.setText(policy_title);
        tv_cancellation_policy.setText(policy_description);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();

            }
        });

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                is_Refundable_Status = "false";
                if(!TextUtils.isEmpty(type) && type.equalsIgnoreCase("cancel_game"))
                {
                    /**Cancel game API*/
                    cancelGame(eventId);
                }
                else{
                    /**Leave game API*/
                    leaveGame(eventId);
                }
            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }

    /**
     * Getting next day's 24 time from the current date n time
     */
    public long getUpcomingTimeInMiliseonds(int hour)
    {
        long time_milisecond = 0;
        SimpleDateFormat inputFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        String currentDateandTime = sdf.format(new Date());
        Date current_time = null;
        try {
            current_time = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();
        c.setTime(current_time);
        //c.add(Calendar.DATE, 1);
        c.add(Calendar.HOUR, hour);

        String current = sdf.format(c.getTime());

        Date current_timee = null;
        try {
            current_timee = sdf.parse(current);
            time_milisecond = current_timee.getTime();
            System.out.print(""+time_milisecond);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time_milisecond;
    }

    /**
     * Event end time converted into milisecond
     */
    public long getTimeSlotsDateTimeinMilisecond()
    {
        long time_milisecond = 0;
        SimpleDateFormat inputFormatter = null;
        String date = eventDate;

        //String timeSlots = date+" "+start_time.trim();

        if(start_time.startsWith(" "))
        {
            start_time = start_time.replace(" ","");
            inputFormatter = new SimpleDateFormat("dd/MM/yyyy h:mma", Locale.US);
        }
        else if(start_time.contains(" "))
        {
            inputFormatter = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.US);
        }
        else{
            inputFormatter = new SimpleDateFormat("dd/MM/yyyy h:mma", Locale.US);
        }

        try {
            String timeSlots = date+" "+start_time.trim();

            Date date1 = inputFormatter.parse(timeSlots);
            time_milisecond = date1.getTime();

            System.out.print(""+time_milisecond);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time_milisecond;
    }

    /**
     * Cancel Game API
     */
    public void cancelGame(String eventId){
        //Displaying loader
        LoaderUtility.handleLoader(EventDetail.this, true);
        // should be a singleton
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBodyCancelGame(eventId);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                SplashScreen.displayMessageInUiThread(EventDetail.this,e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages
                        displayMessage(message);
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                EventDetail.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            //Constants.CANCEL_STATUS = true;
                            EventDetail.this.finish();
                            Intent intent = new Intent(EventDetail.this, EventListing.class);
                            startActivity(intent);
                        }
                    }
                });
                LoaderUtility.handleLoader(EventDetail.this, false);
            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyCancelGame(String game_id){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE+"cancel_game").newBuilder();
        urlBuilder.addQueryParameter("game_id", game_id);
        urlBuilder.addQueryParameter("cancelled_at", getCurrentDateAndTIme());
        urlBuilder.addQueryParameter("is_refundable", is_Refundable_Status);//true/false
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * Function responsible for diaplying of the dialog
     */
    public void displayShareDialog(){
        mMaterialDialog = new MaterialDialog(EventDetail.this);
        View view = LayoutInflater.from(EventDetail.this)
                .inflate(R.layout.layout_update_card_status,
                        null);
        TextView tv_card_numner = (TextView) view.findViewById(R.id.tv_card_number);
        TextView tv_share_using_facebook = (TextView) view.findViewById(R.id.tv_make_card_primary);
        TextView tv_share_using_sms = (TextView) view.findViewById(R.id.tv_delete_card);
        TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_cancel.setText("CANCEL");
        tv_card_numner.setText("Share Using");
        tv_share_using_facebook.setText("Facebook");
        tv_share_using_sms.setText("SMS");


        tv_share_using_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                //Sharing and deep linking the list details in to facebook
                //Initializing the facebook share dialog
                //Calling the function to share the game details in facebook
                shareGameDetails();

            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
            }
        });

        tv_share_using_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMaterialDialog.dismiss();
                String eventSportName = EventAdapter.eventListingModelArrayList.get(0).getEvent_Sports_Type();
                //Sharing the venue details through SMS
                FinishEventCreation.shareDeepLinkUsingSms(Constants.CURRENT_EVENT_ID,
                        AppConstants.OBJECT_TYPE_EVENT,
                        EventDetail.this, eventSportName);


            }
        });
        mMaterialDialog.setCanceledOnTouchOutside(true);
        mMaterialDialog.setView(view).show();
    }

    /**
     * Displaying player price depending upon the object type
     */
    public void displayPlayerPrice() {
        if(TextUtils.isEmpty(list_Id)) {
            tv_minimum_price.setVisibility(View.INVISIBLE);
            priceEventDetails.setVisibility(View.GONE);
            tv_priceperplayertxt.setVisibility(View.GONE);
            rating_event_details_reviews.setVisibility(View.INVISIBLE);
            rating_event_details.setVisibility(View.INVISIBLE);
        }
         else if((AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Game")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("game"))) {
            if(!TextUtils.isEmpty(total_price) &&
                    !total_price.equalsIgnoreCase("0")){
                priceEventDetails.setText("$ "+total_price);
                tv_minimum_price.setText("As low as $" + ApplicationUtility.twoDigitAfterDecimal(Double.valueOf(getMinimumPrice)));
                tv_minimum_price.setVisibility(View.VISIBLE);
            }else{
                priceEventDetails.setText("FREE");
                tv_minimum_price.setVisibility(View.GONE);
                tv_priceperplayertxt.setVisibility(View.GONE);
            }
        } else if((AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))) {
            tv_minimum_price.setVisibility(View.GONE);
            if(!TextUtils.isEmpty(total_price) &&
                    !total_price.equalsIgnoreCase("0")){
                priceEventDetails.setText("$ "+total_price);
            }else{
                priceEventDetails.setText("FREE");
                tv_priceperplayertxt.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Checking whether this game current status is Active or Past
     */
    public boolean checkGameRunningStatus() {
        boolean game_status = true;
        long game_date = getTimeSlotsDateTimeinMilisecond();
        long system_current_time = System.currentTimeMillis();
        if(game_date < system_current_time) {
            game_status = false;
        }
        return game_status;
    }


    /**
     * Function responsible for refreshing the badge count
     */
    public static void refreshBadgeCount(){
        if(Constants.PUSH_NOTIFICATION_BADGE_COUNT > 0){
            Constants.PUSH_NOTIFICATION_BADGE_COUNT = Constants.PUSH_NOTIFICATION_BADGE_COUNT - 1;
            if(Constants.PUSH_NOTIFICATION_BADGE_COUNT > 0){
                TabFragment.notification_count.setVisibility(View.VISIBLE);
                TabFragment.notification_count.setText(String.valueOf(Constants.PUSH_NOTIFICATION_BADGE_COUNT));
            }
        }
    }
}
