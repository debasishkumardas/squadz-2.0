package com.andolasoft.squadz.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.actionbar.StatusBarUtils;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordScreen extends AppCompatActivity {

    @InjectView(R.id.user_email_id) EditText etUsername;
    @InjectView(R.id.reset_password) Button btnResetPassword;
    @InjectView(R.id.btn_back) ImageView btnBack;
    SnackBar snackbar = new SnackBar(ForgotPasswordScreen.this);
    Dialog dialog;
    String status = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_forgot_password_screen);
        ButterKnife.inject(this);

        //CHanging the status bar color
        StatusBarUtils.changeStatusBarColor(ForgotPasswordScreen.this);

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mUsername = etUsername.getText().toString().trim();
                hideSoftKeyBoard();
                if(!TextUtils.isEmpty(mUsername)){
                    resetUserPassword(mUsername);
                }else{
                    snackbar.setSnackBarMessage("Please enter username");
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ForgotPasswordScreen.this.finish();
            }
        });
    }


    /**
     * Calling the API to getting the sports
     */

    public void resetUserPassword(String username){
        //Displaying loader
        LoaderUtility.handleLoader(ForgotPasswordScreen.this, true);
        // should be a singleton
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBody(username);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                //Displying the error message to the User
                //Handling the loader state
                ForgotPasswordScreen.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LoaderUtility.handleLoader(ForgotPasswordScreen.this, false);
                        if(!ApplicationUtility.isConnected(ForgotPasswordScreen.this)){
                            ApplicationUtility.displayNoInternetMessage(ForgotPasswordScreen.this);
                        }else{
                            ApplicationUtility.displayErrorMessage(ForgotPasswordScreen.this, e.getMessage().toString());
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try{
                    JSONObject responseObject = new JSONObject(responseData);
                    status = responseObject.getString("status");
                    String message = responseObject.getString("message");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        //Displaying the messages
                        displayMessage(message);
                    }else if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        //Displaying the messages
                        displayMessage(message);
                    }else{
                        displayMessage("Some thing went wrong");
                    }

                }catch(Exception ex){
                    ex.printStackTrace();
                    displayMessage("Some thing went wrong");
                }

                //Handling the loader state
                ForgotPasswordScreen.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(status != null &&
                                status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            etUsername.setText(null);
                            ForgotPasswordScreen.this.finish();
                        }
                    }
                });

                LoaderUtility.handleLoader(ForgotPasswordScreen.this, false);
            }
        });
    }

    /**
     * Dispaly ing the message
     * @param message status message
     */
    public void displayMessage(final String message){
        ForgotPasswordScreen.this.runOnUiThread(new Runnable() {
            public void run() {

                //Displaying the success message after successful sign up
                //Toast.makeText(ForgotPasswordScreen.this,message, Toast.LENGTH_LONG).show();
               // snackbar.setSnackBarMessage(message);
                Toast.makeText(ForgotPasswordScreen.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status){
        if(status){
            dialog = ProgressDialog
                    .show(ForgotPasswordScreen.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        }else{
            dialog.dismiss();
        }
    }

    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String user_name){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"forgot_password?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_USERNAME, user_name);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ForgotPasswordScreen.this.finish();
    }

    /**
     * Hiding soft key board
     */
    public void hideSoftKeyBoard(){
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
