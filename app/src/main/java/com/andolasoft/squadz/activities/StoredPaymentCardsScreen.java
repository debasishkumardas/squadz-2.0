package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.SavedPaymentCardsModel;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.CredentialSingleton;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.SavedCardAdapter;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;import com.stripe.model.ExternalAccountCollection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class StoredPaymentCardsScreen extends AppCompatActivity {

    @InjectView(R.id.back_icon_card_listing_layout)
    RelativeLayout ivBack;
    @InjectView(R.id.btnAddPayment)
    Button btnAddPayment;
    public static DatabaseAdapter db;
    public static ArrayList<SavedPaymentCardsModel> savedCardArray;
    public static SavedCardAdapter cardAdapter;
    public static RecyclerView cardrecyclerView;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;
    ProgressDialog progressFragment;
    public static SharedPreferences preferences;
    public static SharedPreferences.Editor editor;
    String user_id = "",auth_token = "", default_card_value = "";
    boolean isPageLoad = true;
    int default_card_status = 0;
    public static StoredPaymentCardsScreen storedCards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_stored_payment_cards_screen);
        ButterKnife.inject(this);
        cardrecyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        storedCards = this;
        AppConstants.isPaymentCardAdded = false;

        db = new DatabaseAdapter(StoredPaymentCardsScreen.this);

        //Change the Swipe layout color
        swipe_refresh_layout.setColorSchemeResources(R.color.orange,
                R.color.orange,
                R.color.orange);

        preferences = PreferenceManager
                .getDefaultSharedPreferences(StoredPaymentCardsScreen.this);
        editor = preferences.edit();
        /**
         * Getting user Id & Auth token
         */
        getUserInfo(preferences);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StoredPaymentCardsScreen.this.finish();
            }
        });

        btnAddPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StoredPaymentCardsScreen.this, AddPaymentDetailsScreen.class));
            }
        });

        /**
         * PULL TO REFRESH FUNCTIONALITY
         */
        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
                swipe_refresh_layout.setRefreshing(false);
            }
        });


    }


    public void updateUserPrimaryCard(final String card_id, String cust_id) {
        //Displaying loader
        LoaderUtility.handleLoader(StoredPaymentCardsScreen.this, true);
        // should be a singleton
        OkHttpClient client = Singleton.getInstance().getClient();

        Request request = buildApiRequestBody(card_id,cust_id);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Displying the error message to the User
                SplashScreen.displayMessageInUiThread(StoredPaymentCardsScreen.this, e.toString());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String responseData = response.body().string();
                try {
                    JSONObject responseObject = new JSONObject(responseData);
                    String message = responseObject.getString("message");
                    if (message != null &&
                            message.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)) {
                        //Getting the default card fron Db
                        String default_card = db.getPrimaryPaymentCard(1);
                        //Displaying the messages
                        SplashScreen.displayMessageInUiThread(StoredPaymentCardsScreen.this, message);
                        //Updating the stored default card staus
                        db.updateUserPrimaryCardstatus(default_card, 0);
                        //Updating the default card
                        db.updateUserPrimaryCard(card_id,1);
                        StoredPaymentCardsScreen.this.runOnUiThread(new Runnable() {
                            public void run() {

                            }
                        });
                    } else if (message != null &&
                            message.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)) {
                        //Displaying the messages
                        SplashScreen.displayMessageInUiThread(StoredPaymentCardsScreen.this, message);
                    } else {
                        SplashScreen.displayMessageInUiThread(StoredPaymentCardsScreen.this, message);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    SplashScreen.displayMessageInUiThread(StoredPaymentCardsScreen.this, ex.toString());
                }
                LoaderUtility.handleLoader(StoredPaymentCardsScreen.this, false);
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBody(String card_id, String cust_id){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE+"make_card_primary?").newBuilder();
        urlBuilder.addQueryParameter("card_id", card_id);
        urlBuilder.addQueryParameter("cust_id", cust_id);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    public static void getStoredCarddetails() {
        savedCardArray = new ArrayList<>();
        Cursor cursor = db.getSavedCards();
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    String brand = cursor.getString(cursor.getColumnIndex("brand"));
                    String country = cursor.getString(cursor.getColumnIndex("country"));
                    String funding = cursor.getString(cursor.getColumnIndex("funding"));
                    String lastfour = cursor.getString(cursor.getColumnIndex("lastfour"));
                    String customer_id = cursor.getString(cursor.getColumnIndex("customer_id"));
                    String card_id = cursor.getString(cursor.getColumnIndex("card_id"));
                    int primary_card_status = cursor.getInt(cursor.getColumnIndex("default_card"));
                    SavedPaymentCardsModel savedCardModel = new SavedPaymentCardsModel(brand, country,
                            funding, lastfour, customer_id, card_id,primary_card_status);
                    savedCardArray.add(savedCardModel);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
    }

    /**
     * Refreshing the
     */

    public static void refreshView() {
        if (db.getSavedCardsCount() > 0) {
           /* getStoredCarddetails();
            //Keeping Total number of cards in shared preferences
            editor.putString("BILLING_COUNT",String.valueOf(savedCardArray.size()));
            editor.apply();

            cardAdapter = new SavedCardAdapter(storedCards, savedCardArray);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(storedCards);
            cardrecyclerView.setLayoutManager(mLayoutManager);
            cardrecyclerView.setItemAnimator(new DefaultItemAnimator());
            cardrecyclerView.setAdapter(cardAdapter);
            cardAdapter.notifyDataSetChanged();*/

            //Refreshing the card list view adapter
            setCardListViewadapter();
        } else {
            //Displaying the message if there is no asved cards
            Toast.makeText(storedCards, "No Saved Cards Found", Toast.LENGTH_LONG).show();
            //Refreshing the card list view adapter
            setCardListViewadapter();
        }
    }

    /**
     * Function responsible for refreshing the card list view
     */
    public static void setCardListViewadapter(){
        getStoredCarddetails();
        //Keeping Total number of cards in shared preferences
        editor.putString("BILLING_COUNT",String.valueOf(savedCardArray.size()));
        editor.apply();

        cardAdapter = new SavedCardAdapter(storedCards, savedCardArray);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(storedCards);
        cardrecyclerView.setLayoutManager(mLayoutManager);
        cardrecyclerView.setItemAnimator(new DefaultItemAnimator());
        cardrecyclerView.setAdapter(cardAdapter);
        cardAdapter.notifyDataSetChanged();
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            progressFragment = ProgressDialog
                    .show(StoredPaymentCardsScreen.this, "", "Please wait ...");
            progressFragment.getWindow().setGravity(Gravity.CENTER);
            progressFragment.setCancelable(false);
        } else {
            progressFragment.dismiss();
        }
    }

    /**
     * Pull to refresh functionality
     */
    public void refreshContent() {
        try {
            fetchListOfCardsInfo(user_id, auth_token);
        } catch (Exception exp) {
            exp.printStackTrace();
        }

    }

    /**
     * API integration for fectching list of all cards
     */
    public void fetchListOfCardsInfo(String user_id, String auth_token) {
        //Handling the loader state
        LoaderUtility.handleLoader(StoredPaymentCardsScreen.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(user_id,auth_token);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(StoredPaymentCardsScreen.this, false);
                if(!ApplicationUtility.isConnected(StoredPaymentCardsScreen.this)){
                    ApplicationUtility.displayNoInternetMessage(StoredPaymentCardsScreen.this);
                }else{
                    ApplicationUtility.displayErrorMessage(StoredPaymentCardsScreen.this, e.getMessage().toString());
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                db.deleteCardDetail();
                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    default_card_value = jsonObject.getString("default_card");
                    JSONArray cards_json_array = jsonObject.getJSONArray("cards");
                    for (int i = 0; i < cards_json_array.length() ; i++) {
                        JSONObject cards_json_object = cards_json_array.getJSONObject(i);
                        String cardBrand_type = cards_json_object.getString("brand");
                        String cardcountry = cards_json_object.getString("country");
                        String cardFunding = cards_json_object.getString("funding");
                        String cardlastfour = cards_json_object.getString("last4");
                        String cardCustId = cards_json_object.getString("customer");
                        String cardId = cards_json_object.getString("id");
                        String cardHolderName = cards_json_object.getString("name");
                        String exp_month = cards_json_object.getString("exp_month");
                        String exp_year = cards_json_object.getString("exp_year");
                        if(default_card_value.equalsIgnoreCase(cardId)){
                            default_card_status = 1;
                        }

                        //Saving card info local database
                        db.insertCarddetails(cardBrand_type,
                                cardcountry, cardFunding,
                                cardlastfour, cardCustId, cardId,
                                cardHolderName, exp_month, exp_year,
                                default_card_status);
                        default_card_status = 0;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(StoredPaymentCardsScreen.this, false);
                        refreshView();
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String user_id, String auth_token) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "card_details").newBuilder();
        urlBuilder.addQueryParameter("auth_token", auth_token);
        urlBuilder.addQueryParameter("user_id", user_id);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Getting user Id & Auth token
     */
    public void getUserInfo(SharedPreferences preferences) {
        if (preferences.contains("loginuser_id")) {
            user_id = preferences.getString("loginuser_id", "");
            auth_token = preferences.getString("auth_token", "");
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Constants.ADD_PAYMENY_BTN_CLICKED ||
                isPageLoad) {
            fetchListOfCardsInfo(user_id, auth_token);
            isPageLoad = false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
