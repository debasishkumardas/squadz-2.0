package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Website_Webview_Additional_Info extends AppCompatActivity {

    @InjectView(R.id.back_icon_website_layout)
    RelativeLayout back_icon_website_layout;
    @InjectView(R.id.webView_website)
    WebView webView_website;
    SnackBar snackBar;
    String venue_website = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_website_webview_additional_info);

        /*** Initializing all views belongs to this layout*/
        initReferences();

        /*** Adding click events on the views*/
        addClickEvents();

        /**
         * Load URL in web view to display Terms
         */
        loadUrl();

    }

    /**
     * Adding click events on the views
     */
    public void addClickEvents() {
        back_icon_website_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {

        ButterKnife.inject(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        venue_website = getIntent().getStringExtra("WEBSITE");
        snackBar = new SnackBar(this);
        webView_website.getSettings().setJavaScriptEnabled(true);

    }
    /**
     * Load URL in web view to display Terms
     */
    public void loadUrl()
    {
        if(!venue_website.contains("http")) {
            startWebView("http://"+venue_website);
        }
        else{
            startWebView(venue_website);
        }
    }

    private void startWebView(String url) {

        webView_website.getSettings().setJavaScriptEnabled(true); // enable javascript

        webView_website.getSettings().setLoadWithOverviewMode(true);
        webView_website.getSettings().setUseWideViewPort(true);

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(Website_Webview_Additional_Info.this);
        progressDialog.setMessage("Loading...");

        webView_website.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(Website_Webview_Additional_Info.this, description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                progressDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressDialog.dismiss();
                String webUrl = webView_website.getUrl();
            }
        });

        webView_website.loadUrl(url);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

