package com.andolasoft.squadz.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.managers.DatabaseAdapter;
import com.andolasoft.squadz.models.*;
import com.andolasoft.squadz.models.Profile;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.ImageUploadManager;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.FaqAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;
import com.bumptech.glide.Glide;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class EditUserProfile extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener {

    @InjectView(R.id.btn_back)
    ImageView iv_back;
    @InjectView(R.id.tv_done)
    TextView tvUpdate;
    @InjectView(R.id.image_edit_profile)
    ImageView ivProfileImage;
    @InjectView(R.id.first_edittext_profile)
    EditText etFirstName;
    @InjectView(R.id.last_edittext_profile)
    EditText etLastName;
    @InjectView(R.id.birthday_et__profile)
    TextView tvBirthDate;
    @InjectView(R.id.email_edittext_profile)
    EditText etEmail;
    @InjectView(R.id.phone_edittext_profile)
    EditText etPhone;
    @InjectView(R.id.interest_exp__edit_profile)
    RelativeLayout rlInterestandexperience;
    @InjectView(R.id.about_me__edittext_profile)
    EditText etAboutme;
    @InjectView(R.id.street_name_profile)
    EditText etstreetName;
    @InjectView(R.id.locality_name_profile)
    EditText etLocality;
    @InjectView(R.id.province_name_profile)
    EditText etProvince;
    @InjectView(R.id.postal_code_name_profile)
    EditText etPostalCode;
    @InjectView(R.id.country_name_profile)
    EditText etCountryName;
    Dialog uploadoptions;
    public static final int REQUEST_IMAGE_OPEN = 1;
    public static final int REQUEST_IMAGE_CAPTURE = 2;
    File cameraImagefile;
    Uri mCropImagedUri;
    String selectedimagePath;
    DatabaseAdapter db;
    ProgressDialog dialog;
    SharedPreferences prefs;
    String authToken, logged_in_userid, userSport, skillLevel,edit_profile_api_status;
    com.andolasoft.squadz.models.Profile userProfile;
    SharedPreferences sp;
    SharedPreferences.Editor edit;
    long selected_date_milisecond;
    public static boolean isProfileUpdated = false;
    String status = "",message = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_profile);
        ButterKnife.inject(this);

        //Registering all the click events
        registerClickEvents();

        //Initialize the database
        initializeDatabase();

        //Gettting loggged in user data
        getLoggedInUserdata();

        initializeTempStorge();

        int getStatus = db.getCurrentUserStatus();

        if(getStatus == 0){
            getUserProfile();
        }else{
            //Fetching data from local storage
            getUserProfileInformation();
        }

    }

    public void initializeTempStorge(){
        sp = getSharedPreferences("ProgressState", MODE_PRIVATE);
        edit = sp.edit();
    }

    /**
     * Getting the logged in user details
     */
    public void getLoggedInUserdata(){
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        authToken = prefs.getString("auth_token", null);
        logged_in_userid = prefs.getString("loginuser_id", null);
    }

    /**
     * Initializing the database
     */
    public void initializeDatabase(){
        db = new DatabaseAdapter(EditUserProfile.this);
    }

    /**
     * Function to handle all the click events
     */

    public void registerClickEvents(){
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditUserProfile.this.finish();
            }
        });

        ivProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Function to display the image picking dialog
                //showProfilePictureDialog();
                CheckUserPermissions();
            }
        });

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Api call for Edit Profile
                validateUserData();
            }
        });

        rlInterestandexperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditUserProfile.this, Intrestandexperiance.class);
                intent.putExtra("friend_id",logged_in_userid);
                startActivity(intent);
            }
        });

        tvBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Hiding the Soft KeyBoard
                tvBirthDate.setFocusable(true);
                // ApplicationUtility.hideSoftKeyboard(NormalRegistrationStepTwo.this);
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        EditUserProfile.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMaxDate(now);
                dpd.setAccentColor(getResources().getColor(R.color.orange));
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

    }



    /**
     * RedirectToAddressBook
     */
    public void CheckUserPermissions() {
        int MyVersion = Build.VERSION.SDK_INT;

        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                // showProfilePictureDialog();
                requestForSpecificPermission();
            } else{
                showProfilePictureDialog();
            }
        } else{
            showProfilePictureDialog();
        }
    }

    /**
     * Check permission
     * @return
     */
    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Request persmission for Post lollipop devices
     */
    private void requestForSpecificPermission() {
        android.support.v4.app.ActivityCompat.requestPermissions(EditUserProfile.this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Getting Contacts from Phone book
                    showProfilePictureDialog();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // Toast.makeText(Profile.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    /**
     * PARSING DATE FORMAT WITH CUSTOM DATE
     */
    public long formattedDate(String inputDate) {

        String finalFormatedDate = "";

        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat toConvertFormatter = new SimpleDateFormat("MMM dd yyyy");

        try {

            Date date1 = inputFormatter.parse(inputDate);
            //finalFormatedDate = toConvertFormatter.format(date1);

            selected_date_milisecond = date1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return selected_date_milisecond;
    }

    //Delegate method to set the date
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year + "/"+ (++monthOfYear)+ "/"+ dayOfMonth;
        String OutputDate_For_Display = ApplicationUtility.formattedDateForDisplayForProfile(date);
        //Getting current date from Calender & Converting into in milisecond
        long current_date_milisecond = currentDateInMilisecond();
        long finalOutputDate = convertToMilliseconds(date);
       // selected_date_milisecond = getMills(date);

        if (finalOutputDate > current_date_milisecond) {
        } else {
            //tvBirthDate.setText(date);
            tvBirthDate.setText(OutputDate_For_Display);
        }
    }

    public long getMills(String date){
        long timeInMilliseconds = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date mDate = sdf.parse(date);
            timeInMilliseconds = mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    /**
     * Return date into milisecond
     *
     * @param date
     * @return
     */
    public long convertToMilliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();

            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Getting current date from Calender & Converting into in milisecond
     */
    public long currentDateInMilisecond() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        String formattedDate = df.format(c.getTime());

        return convertToMilliseconds(formattedDate);
    }



    // Showing the Dialog for Profile pic upload options
    public void showProfilePictureDialog() {

        try {
            // initializing the uploadoptions dialog
            uploadoptions = new Dialog(EditUserProfile.this);

            // Setting no title for the dialog
            uploadoptions.requestWindowFeature(Window.FEATURE_NO_TITLE);

            // Setting the dialog to be cancellable
            uploadoptions.setCancelable(true);

            // Setting the view for the dialog
            uploadoptions.setContentView(R.layout.uploadimages_fromcamera);

            uploadoptions.getWindow().setGravity(Gravity.BOTTOM);
            uploadoptions.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            uploadoptions.getWindow().setBackgroundDrawableResource(
                    android.R.color.transparent);
            uploadoptions.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            // Initializing all the views of the uploadoptions dialog
            Button fromcamera_txt = (Button) uploadoptions
                    .findViewById(R.id.fromcamera_signup);
            Button fromsdcard_txt = (Button) uploadoptions
                    .findViewById(R.id.fromsdcard_signup);
            Button cancel_signup = (Button) uploadoptions
                    .findViewById(R.id.cancel_edit_photo_signup);

            fromcamera_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uploadoptions.dismiss();
                    //opening the camera
                    openCamera();

                }
            });

            fromsdcard_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Calling the function for opening the Cameraroll
                    // openCameraRoll();
                    uploadoptions.dismiss();
                    //Opening the image picker
                    piskImage();

                }
            });
            cancel_signup.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub
                    uploadoptions.dismiss();
                }
            });

            // Showing the uploadoptions dialog
            uploadoptions.show();

        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
    }

    /**
     * Function responsibel for picking the image files from the gallery
     */
    public void piskImage(){
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_IMAGE_OPEN);
    }

    /**
     * Function responsible for opening the camera
     */
    public void openCamera(){
        Intent intent = new Intent(
                "android.media.action.IMAGE_CAPTURE");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        cameraImagefile = new File(Environment
                .getExternalStorageDirectory()
                + File.separator
                + timeStamp + "_image.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(cameraImagefile));
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    /**
     * Handling the camera image intents
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5) {
            if(data != null){
                Bundle selectedImage = data.getExtras();
                Bitmap pickedImage = (Bitmap) selectedImage.getParcelable("data");
                Bitmap rsz_image = NormalRegistrationStepTwo.getRoundedShape(pickedImage);
                ivProfileImage.setImageBitmap(rsz_image);
                selectedimagePath = storeImage(pickedImage);
                //Bitmap resizedBitmap = Bitmap.createBitmap(pickedImage, 0, 0, pickedImage.getWidth(), pickedImage.getHeight());
                ImageUploadManager imguplload = new ImageUploadManager(EditUserProfile.this,selectedimagePath);
            }
        } else if (requestCode == REQUEST_IMAGE_OPEN &&
                resultCode == RESULT_OK) {
            Uri fullPhotoUri = data.getData();
            performCrop(fullPhotoUri);
            Log.d("File Path", fullPhotoUri.getPath().toString());
            // Do work with full size photo saved at fullPhotoUri

        }else if(requestCode == REQUEST_IMAGE_CAPTURE &&
                resultCode == RESULT_OK){
            Uri uri_gallery = Uri.fromFile(cameraImagefile);
            performCrop(uri_gallery);
            Log.d("File Path", uri_gallery.getPath().toString());
        }
    }

    private String storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("This class",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("This class", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("This class", "Error accessing file: " + e.getMessage());
        }

        return pictureFile.toString();
    }


    /** Create a File for saving an image or video */
    private  File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Squadz");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            mediaStorageDir.mkdirs();
            // Create a media file name
            String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
            File mediaFile;
            String mImageName="MI_"+ timeStamp +".jpg";
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);

            try{
                if (!mediaFile.exists()){
                    mediaFile.createNewFile();
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }

            return mediaFile;

        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="image_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }



    /**
     * Function responsible for cropping the image
     *
     * @param uri The image file Uri
     */

    private void performCrop(Uri uri) {
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(uri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 600);
            cropIntent.putExtra("outputY", 600);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            File path = getCreatedFilePath();
            mCropImagedUri = Uri.fromFile(path);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 5);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast
                    .makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public File getCreatedFilePath(){
        File angel1 = new File(Environment.getExternalStorageDirectory() + "/.cropImageFolder/");
        if(!angel1.exists()) {
            angel1.mkdirs();
        }
        long i2 = System.currentTimeMillis();
        File file2 = new File(angel1, String.valueOf(i2) + ".jpg");
        return file2;
    }


    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(EditUserProfile.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForListing(String userId) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"users/" +
                    authToken +"/profile?").newBuilder();
        urlBuilder.addQueryParameter(Constants.PARAM_USER_ID, userId);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }


    /**
     * API integration for display Questions with Answers
     */
    public void getUserProfile() {
        //Handling the loader state
        LoaderUtility.handleLoader(EditUserProfile.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForListing(logged_in_userid);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(EditUserProfile.this, false);
                if(!ApplicationUtility.isConnected(EditUserProfile.this)){
                    ApplicationUtility.displayNoInternetMessage(EditUserProfile.this);
                }else{
                    ApplicationUtility.displayErrorMessage(EditUserProfile.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject user_data = new JSONObject(responseData);
                    String status = user_data.getString("status");
                    if(status != null &&
                            status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        JSONObject profile = user_data.getJSONObject("profile");
                        String user_id = profile.getString("user_id");
                        String user_name = profile.getString("username");
                        String name = profile.getString("name");
                        String phone_number = profile.getString("phone_number");
                        String user_sport = profile.getString("sport");
                        String user_email = profile.getString("email");
                        String user_profileimage = profile.getString("image");
                        String user_aboutme = profile.getString("about_me");
                        String expertlevel = profile.getString("skill_level");
                        String bitrh_date = profile.getString("birth_date");
                        int rewards = profile.getInt("rewards");
                        JSONArray secondarySports = profile.getJSONArray("secondary_sports");
                        String secondary_sports = secondarySports.toString();

                        String street_name = null, locality = null,
                                postal_code = null,province_abbrevation = null, country = null;
                        if (profile.has("street_name")
                                && !profile.isNull("street_name")) {
                             street_name = profile.getString("street_name");
                        }
                        if (profile.has("locality") && !profile.isNull("locality")) {
                             locality = profile.getString("locality");
                        }
                        if (profile.has("postal_code")
                                && !profile.isNull("postal_code")) {
                             postal_code = profile.getString("postal_code");
                        }
                        if (profile.has("province_abbreviation")
                                && !profile.isNull("province_abbreviation")) {
                             province_abbrevation = profile
                                    .getString("province_abbreviation");
                        }
                        if (profile.has("country") && !profile.isNull("country")) {
                             country = profile.getString("country");
                        }
                        if (!bitrh_date.equalsIgnoreCase("null")
                                && bitrh_date.length() > 1) {
                            bitrh_date = bitrh_date.substring(0,
                                    bitrh_date.length() - 15);
                        }

                        String firstName = null, lastName = null;
                        if(name != null && name.contains(" ")){
                            String[] nameArray = name.split(" ");
                            firstName = nameArray[0];
                            lastName = nameArray[1];

                        }

                        //Setting the data in the model class
                        userProfile = new Profile(user_id, user_name, phone_number,
                                name, firstName, lastName,
                                user_email, bitrh_date, user_profileimage,
                                user_aboutme, user_sport, expertlevel,
                                rewards, street_name,
                                locality, province_abbrevation,
                                postal_code, country);
                        //Initialize the database
                        initializeDatabase();

                        //Deleting all the data from user table
                        db.deleteCurrentUserData(EditUserProfile.this);

                        //Inserting the user data in the local database
                        db.insertCurrentUserDetails(user_id, user_name, phone_number, firstName, lastName,user_email,
                                bitrh_date, user_profileimage, user_aboutme, user_sport, expertlevel, null,String.valueOf(rewards),
                                street_name , locality, province_abbrevation , postal_code , country);

                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(EditUserProfile.this, false);
                        displayUserDate(userProfile);
                        //Gettting loggged in user data
                       // getLoggedInUserdata();

                        //initializeTempStorge();


                       // String otherSportDetails = getOtherSportDetail();
                        //Log.d("Sports", otherSportDetails);
                    }
                });
            }
        });
    }

    /**
     * Function responsible for diplaying user data
     * @param userProfileData
     */
    public void displayUserDate(Profile userProfileData){

        if(userProfileData != null)
        try{
            //Getting and setting user data
            etFirstName.setText(userProfileData.getFirstName());
            etLastName.setText(userProfileData.getLastName());

            etEmail.setText(userProfileData.getEmail());
            etEmail.setEnabled(false);
            etAboutme.setText(userProfileData.getAboutMe());
            userSport = userProfileData.getSport();
            skillLevel = userProfileData.getSkillLevel();

            //Formatting the date before setting in the view
            if(!TextUtils.isEmpty(userProfileData.getBirthDate())){
                String formattedBirthDate = ApplicationUtility.
                        formattedDateForDisplayForEditProfile(userProfileData.getBirthDate());
                tvBirthDate.setText(formattedBirthDate);
            }

            //Setting the user profile image
            String profileImage = userProfileData.getImage();
            if(!TextUtils.isEmpty(profileImage.trim())){
                Glide.with(EditUserProfile.this).load(profileImage).into(ivProfileImage);
            }

            if(userProfileData.getPhoneNumber().length() > 10){
                String code = NormalRegistrationStepTwo.GetCountryZipCode(EditUserProfile.this);
                String phoneNumber = null;
                if (!TextUtils.isEmpty(code)) {
                    if(userProfileData.getPhoneNumber().contains(code)){
                        phoneNumber = userProfileData.getPhoneNumber().replace(code, "");
                    }
                }
                etPhone.setText(phoneNumber);
            }else{
                etPhone.setText(userProfileData.getPhoneNumber());
            }

            if(userProfileData.getStreetName() != null &&
                    !isEmptyString(userProfileData.getStreetName().trim())){
                etstreetName.setText(userProfileData.getStreetName());
            }

            if(userProfileData.getLocality() != null &&
                    !isEmptyString(userProfileData.getLocality().trim())){
                etLocality.setText(userProfileData.getLocality());
            }

            if(userProfileData.getProvinceAbbreviation() != null &&
                    !isEmptyString(userProfileData.getProvinceAbbreviation())){
                etProvince.setText(userProfileData.getProvinceAbbreviation());
            }

            if(userProfileData.getPostalCode() != null &&
                    !isEmptyString(userProfileData.getPostalCode())){
                etPostalCode.setText(userProfileData.getPostalCode());
            }

            if(userProfileData.getCountry() != null &&
                    !isEmptyString(userProfileData.getCountry())){
                etCountryName.setText(userProfileData.getCountry());
            }


        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    /**
     * Function responsible for cheking the string as empty
     * @param value String to be cjhecked
     * @return Return as true or false
     */
    public boolean isEmpty(String value){
        boolean isstringEmpty = false;

        if(TextUtils.isEmpty(value)){
            isstringEmpty = true;
        }
        return  isstringEmpty;
    }

    /**
     * Function responsible for cheking the string as empty
     * @param value String to be cjhecked
     * @return Return as true or false
     */
    public boolean isEmptyString(String value){
        boolean isstringEmpty = false;

        if(!TextUtils.isEmpty(value) &&
                value.equalsIgnoreCase("<null>")){
            isstringEmpty = true;
        }
        return  isstringEmpty;
    }



    /**
     * Function responsible for fetchingthe user information from the local database
     */
    public void getUserProfileInformation(){
        //Getting the user profile onject
        Profile userProfileData = db.getCurrentUserDetails();

        if(userProfileData != null){
            //Setting the user data in the view
            displayUserDate(userProfileData);
        }

    }

    /**
     * Function responsible for validating the user inputted data
     */
    public void validateUserData(){
        String firstName = etFirstName.getText().toString().trim();
        String lastName  = etLastName.getText().toString().trim();
        String birthDate = tvBirthDate.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String phone_number = etPhone.getText().toString().trim();
        String about_me = etAboutme.getText().toString().trim();
        String street_name = etstreetName.getText().toString().trim();
        String locality = etLocality.getText().toString().trim();
        String province = etProvince.getText().toString().trim();
        String postal_code = etPostalCode.getText().toString().trim();
        String country_name = etCountryName.getText().toString().trim();


        if (isEmpty(firstName)){
            Toast.makeText(EditUserProfile.this,"First Name cant be left blank", Toast.LENGTH_LONG).show();
            keepFocus(etFirstName);
        }else if (isEmpty(lastName)){
            Toast.makeText(EditUserProfile.this,"Last Name cant be left blank", Toast.LENGTH_LONG).show();
            keepFocus(etLastName);
        }else if (isEmpty(email)){
            Toast.makeText(EditUserProfile.this,"Email cant be left blank", Toast.LENGTH_LONG).show();
            keepFocus(etEmail);
        }else if (isEmpty(phone_number)){
            Toast.makeText(EditUserProfile.this,"Phone Number cant be left blank", Toast.LENGTH_LONG).show();
            keepFocus(etPhone);
        }else if (isEmpty(about_me)){
            Toast.makeText(EditUserProfile.this,"About me cant be left blank", Toast.LENGTH_LONG).show();
            keepFocus(etAboutme);
        }else {
            //Formatting the date for sending to server
            if(!TextUtils.isEmpty(birthDate)){
                //String reverseFormattedDate = ApplicationUtility.formattedDateForDisplayForEditProfileReverse
                birthDate = ApplicationUtility.formattedDateForSignup(birthDate);
            }

            //Getting the country COde
            String code = NormalRegistrationStepTwo.GetCountryZipCode(EditUserProfile.this);

            //Appending the country cide with phone number
            if(!isEmpty(code)){
                phone_number = code + " " + phone_number;
            }

            //Calling the edit profile API
            editUserProfile(firstName, lastName,birthDate, email,phone_number,
                    about_me, street_name, locality, province, postal_code, country_name);
        }


    }

    /**
     * Function to keep the focus
     * @param et Edit text that to be keep in focus
     */
    public void keepFocus(EditText et){
        et.requestFocus();
    }

    public void editUserProfile(String firstName, String lastName,String birthDate, String email,String phone_number,
                                String about_me, String street_name, String locality, String province, String postal_code,
                                String country_name){
        LoaderUtility.handleLoader(EditUserProfile.this, true);

        //Getting the OkHttpClient
        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForEditProfile( firstName,  lastName, birthDate,  email, phone_number,
                 about_me,  street_name,  locality,  province,  postal_code,
                 country_name);


        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(EditUserProfile.this, false);
                if(!ApplicationUtility.isConnected(EditUserProfile.this)){
                    ApplicationUtility.displayNoInternetMessage(EditUserProfile.this);
                }else{
                    ApplicationUtility.displayErrorMessage(EditUserProfile.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject user_data = new JSONObject(responseData);
                    edit_profile_api_status = user_data.getString("status");
                    if(edit_profile_api_status != null &&
                            edit_profile_api_status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                        JSONObject profile = user_data.getJSONObject("profile");
                        String user_id = profile.getString("user_id");
                        String user_name = profile.getString("username");
                        String name = profile.getString("name");
                        String phone_number = profile.getString("phone_number");
                        String user_sport = profile.getString("sport");
                        String user_email = profile.getString("email");
                        String user_profileimage = profile.getString("image");
                        String user_aboutme = profile.getString("about_me");
                        String expertlevel = profile.getString("skill_level");
                        String bitrh_date = profile.getString("birth_date");
                        int rewards = profile.getInt("rewards");
                        JSONArray secondarySports = profile.getJSONArray("secondary_sports");
                        String secondary_sports = secondarySports.toString();

                        String street_name = null, locality = null,
                                postal_code = null,province_abbrevation = null, country = null;
                        if (profile.has("street_name")
                                && !profile.isNull("street_name")) {
                            street_name = profile.getString("street_name");
                        }
                        if (profile.has("locality") && !profile.isNull("locality")) {
                            locality = profile.getString("locality");
                        }
                        if (profile.has("postal_code")
                                && !profile.isNull("postal_code")) {
                            postal_code = profile.getString("postal_code");
                        }
                        if (profile.has("province_abbreviation")
                                && !profile.isNull("province_abbreviation")) {
                            province_abbrevation = profile
                                    .getString("province_abbreviation");
                        }
                        if (profile.has("country") && !profile.isNull("country")) {
                            country = profile.getString("country");
                        }
                        if (!bitrh_date.equalsIgnoreCase("null")
                                && bitrh_date.length() > 1) {
                            bitrh_date = bitrh_date.substring(0,
                                    bitrh_date.length() - 15);
                        }

                        String firstName = null, lastName = null;
                        if(name != null && name.contains(" ")){
                            String[] nameArray = name.split(" ");
                            firstName = nameArray[0];
                            lastName = nameArray[1];

                        }

                        //Deleting all the data from user table
                        db.deleteCurrentUserData(EditUserProfile.this);

                        //Refreshing all the values in the shared preferance
                        updateUserData(firstName, lastName, user_profileimage, user_email, phone_number);

                        //Inserting the user data in the local database
                        db.insertCurrentUserDetails(user_id, user_name, phone_number, firstName, lastName,user_email,
                                bitrh_date, user_profileimage, user_aboutme, user_sport, expertlevel, null,String.valueOf(rewards),
                                street_name , locality, province_abbrevation , postal_code , country);

                    }else if(edit_profile_api_status != null &&
                            edit_profile_api_status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR)){
                        message = user_data.getString("message");
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                EditUserProfile.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(EditUserProfile.this, false);
                        if(edit_profile_api_status != null &&
                                edit_profile_api_status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_SUCCESS)){
                            ImageUploadManager.uploadedFileName = null;
                            Constants.isImageUploaded = false;
                            ImageUploadManager.profilePicS3Url = null;
                            ImageUploadManager.profilePicS3Url = null;
                            isProfileUpdated = true;
                            EditUserProfile.this.finish();
                            Toast.makeText(EditUserProfile.this, "Profile Information Updated Successfully", Toast.LENGTH_LONG).show();
                        }else if(edit_profile_api_status != null &&
                                edit_profile_api_status.equalsIgnoreCase(AppConstants.API_RESPONSE_STATUS_ERROR))
                            Toast.makeText(EditUserProfile.this, message, Toast.LENGTH_LONG).show();
                        else if(edit_profile_api_status == null){
                            Toast.makeText(EditUserProfile.this, "Unable To Update Profile", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });


    }

    /**
     * Function to build the edit Profile Parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForEditProfile(String firstName, String lastName,String birthDate, String email,String phone_number,
                                                     String about_me, String street_name, String locality, String province, String postal_code,
                                                     String country_name) {
//        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"users/" +
//                authToken +"/edit_profile?").newBuilder();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL+"edit_profile?").newBuilder();
        urlBuilder.addQueryParameter("user_id", logged_in_userid);
        urlBuilder.addQueryParameter("first_name", firstName);
        urlBuilder.addQueryParameter("last_name", lastName);
        urlBuilder.addQueryParameter("phone_no", phone_number);
        urlBuilder.addQueryParameter("email", email);
        urlBuilder.addQueryParameter("about_me", about_me);
        urlBuilder.addQueryParameter("sport_id", db.getcurrentSportId(userSport));
        urlBuilder.addQueryParameter("birth_date", birthDate);
        //urlBuilder.addQueryParameter("skill_level", skillLevel);
        urlBuilder.addQueryParameter("skill_level", "Professional Athlete");
        urlBuilder.addQueryParameter("other_sports", getOtherSportDetail());
        urlBuilder.addQueryParameter("street_name", street_name);
        urlBuilder.addQueryParameter("locality", locality);
        urlBuilder.addQueryParameter("province_abbreviation", province);
        urlBuilder.addQueryParameter("postal_code", postal_code);
        urlBuilder.addQueryParameter("country", country_name);
        //urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE_NAME, ImageUploadManager.profilePicS3Url);
       /* if(!isEmpty(ImageUploadManager.profilePicS3Url)){
            urlBuilder.addQueryParameter("image", ImageUploadManager.profilePicS3Url);
            urlBuilder.addQueryParameter("image_file_name", "imgae1.jpeg");
            urlBuilder.addQueryParameter("image_content_type", "image/jpeg");

        }*/
        if(Constants.isImageUploaded &&
                TextUtils.isEmpty(ImageUploadManager.profilePicS3Url)){
            ImageUploadManager.profilePicS3Url = ImageUploadManager.getProfilePictureUrlAgain(ImageUploadManager.uploadedFileName);
            urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, ImageUploadManager.profilePicS3Url);
            urlBuilder.addQueryParameter("image_file_name", "imgae1.jpeg");
            urlBuilder.addQueryParameter("image_content_type", "image/jpeg");
        }else if(!TextUtils.isEmpty(ImageUploadManager.profilePicS3Url)){
            urlBuilder.addQueryParameter(Constants.PARAM_USER_IMAGE, ImageUploadManager.profilePicS3Url);
            urlBuilder.addQueryParameter("image_file_name", "imgae1.jpeg");
            urlBuilder.addQueryParameter("image_content_type", "image/jpeg");
        }

        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        //Making the user Profile link null
        ImageUploadManager.profilePicS3Url = null;

        return request;
    }

    /**
     * Keeping the user data in the shared preferance after successful login
     * @param firstname user firstname
     * @param lastname user lastname
     * @param image user Profile image
     */

    public void updateUserData(String firstname, String lastname, String image,
                                            String userEmail, String userPhone){
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(EditUserProfile.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("firstname", firstname);
        editor.putString("lastname", lastname);
        editor.putString("image", image);
        editor.putString("user_email", userEmail);
        editor.putString("user_phone", userPhone);
        editor.commit();
    }

    // set text value according to seekbar position
    public String setSeekbarText(int num) {
        String text;

        if (isBetween(num, 1, 25)) {
            text = "Recreational Player";
        } else if (isBetween(num, 26, 50)) {
            text = "High School Athlete";
        } else if (isBetween(num, 51, 75)) {
            text = "Collegiate Athlete";
        } else if (isBetween(num, 76, 100)) {
            text = "Professional Athlete";
        } else {

            text = "No Experience";
        }

        return text;
    }

    // check whether the num is between x
    public static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }



    /**
     *  Function to get the final string of sports
     */

    public String getOtherSportDetail() {
        String text = "";
        String primary_sport = db.getPrimarySport(logged_in_userid);

        if (!(setSeekbarText(sp.getInt("Basketball", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Basketball")) {
            text = text.concat(db.getcurrentSportId("Basketball") + "~"
                    + setSeekbarText(sp.getInt("Basketball", 0)) + ",");

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Basketball") + "~" +
		 * setSeekbarText(sp.getInt("Basketball", 0)) + ","); }
		 */
        if (!(setSeekbarText(sp.getInt("Baseball", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Baseball")) {
            text = text.concat(db.getcurrentSportId("Baseball") + "~"
                    + setSeekbarText(sp.getInt("Baseball", 0)) + ",");

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Baseball") + "~" +
		 * setSeekbarText(sp.getInt("Baseball", 0)) + ","); }
		 */

        if (!(setSeekbarText(sp.getInt("Football", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Football")) {
            text = text.concat(db.getcurrentSportId("Football") + "~"
                    + setSeekbarText(sp.getInt("Football", 0)) + ",");

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Football") + "~" +
		 * setSeekbarText(sp.getInt("Football", 0)) + ","); }
		 */
        if (!(setSeekbarText(sp.getInt("Soccer", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Soccer")) {
            text = text.concat(db.getcurrentSportId("Soccer") + "~"
                    + setSeekbarText(sp.getInt("Soccer", 0)) + ",");

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Soccer") + "~" +
		 * setSeekbarText(sp.getInt("Soccer", 0)) + ","); }
		 */
        if (!(setSeekbarText(sp.getInt("Tennis", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Tennis")) {
            text = text.concat(db.getcurrentSportId("Tennis") + "~"
                    + setSeekbarText(sp.getInt("Tennis", 0)) + ",");

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Tennis") + "~" +
		 * setSeekbarText(sp.getInt("Tennis", 0)) + ","); }
		 */
        if (!(setSeekbarText(sp.getInt("Volleyball", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Volleyball")) {
            text = text.concat(db.getcurrentSportId("Volleyball") + "~"
                    + setSeekbarText(sp.getInt("Volleyball", 0)) + ",");

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Volleyball") + "~" +
		 * setSeekbarText(sp.getInt("Volleyball", 0)) + ","); }
		 */
        if (!(setSeekbarText(sp.getInt("Golf", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Golf")) {
            text = text.concat(db.getcurrentSportId("Golf") + "~"
                    + setSeekbarText(sp.getInt("Golf", 0)) + ",");

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Golf") + "~" +
		 * setSeekbarText(sp.getInt("Golf", 0)) + ","); }
		 */
        if (!(setSeekbarText(sp.getInt("Cricket", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Cricket")) {
            text = text.concat(db.getcurrentSportId("Cricket") + "~"
                    + setSeekbarText(sp.getInt("Cricket", 0)) + ",");

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Cricket") + "~" +
		 * setSeekbarText(sp.getInt("Cricket", 0)) + ","); }
		 */
        if (!(setSeekbarText(sp.getInt("Biking", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Biking")) {
            text = text.concat(db.getcurrentSportId("Biking") + "~"
                    + setSeekbarText(sp.getInt("Biking", 0)) + ",");

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Biking") + "~" +
		 * setSeekbarText(sp.getInt("Biking", 0)) + ","); }
		 */

        if (!(setSeekbarText(sp.getInt("Rugby", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Rugby")) {
            text = text.concat(db.getcurrentSportId("Rugby") + "~"
                    + setSeekbarText(sp.getInt("Rugby", 0)) + ",");

        }

        if (!(setSeekbarText(sp.getInt("Running", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Running")) {
            text = text.concat(db.getcurrentSportId("Running") + "~"
                    + setSeekbarText(sp.getInt("Running", 0)) + ",");

        }

		/*
		 * else { text = text.concat(db.getcurrentSportId("Rugby") + "~" +
		 * setSeekbarText(sp.getInt("Rugby", 0)) + ","); }
		 */

        if (!(setSeekbarText(sp.getInt("Ultimate Frisbee", 0))
                .contentEquals("No Experience"))
                && !primary_sport.contentEquals("Ultimate Frisbee")) {
            text = text.concat(db.getcurrentSportId("Ultimate Frisbee") + "~"
                    + setSeekbarText(sp.getInt("Ultimate Frisbee", 0)));

        } /*
		 * else { text = text.concat(db.getcurrentSportId("Ultimate Frisbee") +
		 * "~" + setSeekbarText(sp.getInt("Ultimate Frisbee", 0))); }
		 */

        return text;
    }

    @Override
    protected void onResume() {
        super.onResume();


    }
}
