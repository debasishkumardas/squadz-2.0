package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.AppConstants;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.Constants;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.adapters.VenueAdapter;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class AddReview extends AppCompatActivity {

    @InjectView(R.id.back_add_review_layout)
    RelativeLayout back_add_review_layout;
    @InjectView(R.id.add_review_submit_btn)
    Button add_review_submit_btn;
    @InjectView(R.id.ratingbar)
    RatingBar ratingbar;
    @InjectView(R.id.ratings_count)
    TextView ratings_count;
    @InjectView(R.id.reviews_description)
    EditText reviews_description;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    SnackBar snackBar;
    ProgressDialog dialog;
    String userId = "",rating_value = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_add_review_layout);

        /**
         * Initializing all views belongs to this layout
         */
        initReferences();

        /**
         * Adding click events on the views
         */
        addClickEvents();

        /**
         * Set exist review data of the current user for updating
         */
        checkExistReview();

    }

    /**
     * Adding click events on the views
     */
    public void addClickEvents() {
        back_add_review_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /**Rating bar change listener*/
        ratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if(rating<1.0f) {
                    ratingBar.setRating(1.0f);
                }
                String ratings = String.valueOf(ratingBar.getRating());
                if(ratings.contains(".0"))
                {
                    DecimalFormat decimalFormat=new DecimalFormat("#.#");
                    rating_value = String.valueOf(decimalFormat.format(ratingBar.getRating()));
                }
                else{
                    rating_value = ratings;
                }
                ratings_count.setText(""+rating_value+"/5");
            }
        });

        /**Submit button click*/
        add_review_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String get_reviews_description = reviews_description.getText().toString().trim();
                String ratings = String.valueOf(ratingbar.getRating());

                if(ratings.equalsIgnoreCase("0.0") ||
                        ratings.equalsIgnoreCase("0")) {
                    snackBar.setSnackBarMessage("Rating should not be empty");
                } else{
                    /**Add review API integration*/
                    if(!TextUtils.isEmpty(rating_value)) {
                        getVenueId();
                        saveReview(userId, VenueAdapter.venueId, rating_value, get_reviews_description);
                    } else{
                        getVenueId();
                        saveReview(userId, VenueAdapter.venueId, rating_value, get_reviews_description);
                     }

                }
            }
        });

    }

    /**
     * Getting the venue Id
     */
    public void getVenueId(){
        if(AppConstants.OBJECT_TYPE_GAME != null
                && (AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))){
            VenueAdapter.venueId = VenueAdapter.venueDetailModelArrayList.get(0).getId();
        }
    }

    /**
     * API integration for Add Review
     */
    public void saveReview(String userId, String list_id,
                           String rating, String review) {
        //Handling the loader state
        LoaderUtility.handleLoader(AddReview.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBodyForAddReview(userId, list_id, rating, review);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LoaderUtility.handleLoader(AddReview.this, false);
                if(!ApplicationUtility.isConnected(AddReview.this)){
                    ApplicationUtility.displayNoInternetMessage(AddReview.this);
                }else{
                    ApplicationUtility.displayErrorMessage(AddReview.this, e.getMessage().toString());
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                String status = "",message = "";
                // Read data on the worker thread
                String responseData = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    if(jsonObject.has("status") &&
                            !jsonObject.isNull("status")) {
                        status = jsonObject.getString("status");
                    }
                    if(jsonObject.has("message") &&
                            !jsonObject.isNull("message")) {
                        message = jsonObject.getString("message");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                final String finalStatus = status;
                final String finalMessage = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader state
                        LoaderUtility.handleLoader(AddReview.this, false);
                        if(finalStatus.equalsIgnoreCase("success")) {
                            Constants.REVIEW_ADDED = true;
                            Constants.REVIEW_ADDED_FOR_BACK = true;
                            Toast.makeText(AddReview.this, ""+ finalMessage, Toast.LENGTH_SHORT).show();
                            finish();
                        } else{
                            Toast.makeText(AddReview.this, ""+ finalMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }


    /**
     * Function to build the Login APi request parameters
     * @return APi request
     */
    public Request buildApiRequestBodyForAddReview(String userId, String list_id, String rating, String review) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(EndPoints.BASE_URL_VENUE + "save_review").newBuilder();
        urlBuilder.addQueryParameter("user_id", userId);

        urlBuilder.addQueryParameter("rating", rating);
        urlBuilder.addQueryParameter("review", review);
//        urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_GAME);
        if(AppConstants.OBJECT_TYPE_GAME != null
                && (AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("Event")
                || AppConstants.OBJECT_TYPE_GAME.equalsIgnoreCase("event"))){
            urlBuilder.addQueryParameter("event_id", list_id);
            urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_GAME);
        }else {
            urlBuilder.addQueryParameter("list_id", list_id);
            urlBuilder.addQueryParameter("object_type", AppConstants.OBJECT_TYPE_LIST);
        }
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();
        return request;
    }

    /**
     * Function to handle the progress loader
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(AddReview.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Initializing all views belongs to this layout
     */
    public void initReferences() {

        ButterKnife.inject(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        snackBar = new SnackBar(this);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(AddReview.this);
        editor = preferences.edit();
        userId = preferences.getString("loginuser_id", null);
    }

    /**
     * Set exist review data of the current user for updating
     */
    public void checkExistReview() {
        String user_rating = "",user_review = "";
        if(VenueAdapter.user_review_arrayList != null) {
            if (VenueAdapter.user_review_arrayList.size() > 0) {
                for (int i = 0; i < VenueAdapter.user_review_arrayList.size(); i++) {
                    String[] user_rating_split = VenueAdapter.user_review_arrayList.get(i).split("@@@@");
                    if(user_rating_split.length == 1)
                    {
                        user_rating = user_rating_split[0];
                    }
                    else{
                        user_rating = user_rating_split[0];
                        user_review = user_rating_split[1];
                    }
                }
                ratingbar.setRating(Float.valueOf(user_rating));
                reviews_description.setText(user_review);
                ratings_count.setText(String.valueOf(ratingbar.getRating() + "/5"));
            } else {
                ratingbar.setRating(0);
                reviews_description.setText("");
                ratings_count.setText("0/5");
            }
        }
        else {
            ratingbar.setRating(0);
            reviews_description.setText("");
            ratings_count.setText("0/5");
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

