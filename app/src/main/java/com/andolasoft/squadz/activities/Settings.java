package com.andolasoft.squadz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andolasoft.squadz.R;
import com.andolasoft.squadz.utils.ApplicationUtility;
import com.andolasoft.squadz.utils.EndPoints;
import com.andolasoft.squadz.utils.LoaderUtility;
import com.andolasoft.squadz.utils.Singleton;
import com.andolasoft.squadz.views.widgets.SnackBar;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SpNayak on 1/31/2017.
 */

public class Settings extends AppCompatActivity {

    @InjectView(R.id.back_icon_settings_layout)
    RelativeLayout back_icon_settings_layout;
    @InjectView(R.id.switch_button_enable_notif)
    SwitchCompat switch_button_enable_notif;
    @InjectView(R.id.switch_button_player_request)
    SwitchCompat switch_button_player_request;
    @InjectView(R.id.switch_button_game_invitations)
    SwitchCompat switch_button_game_invitations;
    @InjectView(R.id.switch_button_game_updates)
    SwitchCompat switch_button_game_updates;
    @InjectView(R.id.switch_button_game_edit)
    SwitchCompat switch_button_game_edit;
    @InjectView(R.id.switch_button_join_leaving_game)
    SwitchCompat switch_button_join_leaving_game;
    @InjectView(R.id.switch_button_message_board)
    SwitchCompat switch_button_message_board;
    /*@InjectView(R.id.switch_button_comments_on_post)
    SwitchCompat switch_button_comments_on_post;*/
    @InjectView(R.id.switch_button_cancel_game)
    SwitchCompat switch_button_cancel_game;
    @InjectView(R.id.switch_button_nearby_games)
    SwitchCompat switch_button_nearby_games;
    @InjectView(R.id.switch_button_only_primary_games)
    SwitchCompat switch_button_only_primary_games;
    @InjectView(R.id.switch_button_all_games)
    SwitchCompat switch_button_all_games;
    SnackBar snackBar;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String notification_type = "";
    ProgressDialog dialog;
    String auth_token = "";
    boolean is_Notify_Enable = true, is_player_request = false,
            is_nearby_game = false, is_game_invitation = false, is_game_update = false,
            is_message_board = false, is_comment_on_board = false, is_game_cancel = false,
            is_game_edit = false, is_game_join_leave = false, is_only_primary_game = false,
            is_all_games = false;
    int enable_notif_counter = 0,enable_notif_player_request = 0,
            enable_notif_game_invitations = 0,enable_notif_game_updates = 0,
            enable_notif_game_edit = 0,enable_notif_join_leaving_game = 0,
            enable_notif_message_board = 0,enable_notif_cancel_game = 0,
            enable_notif_nearby_games = 0,enable_notif_only_primary_games,
            enable_notif_all_games = 0;
    static Boolean isTouched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/proxima_nova_semibold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_settings_layout);

        /**
         * Initializing views
         */
        initViews();

        /**
         * Adding Click events on te views
         */
        setClickEvents();

    }

    /**
     * Adding Click events on te views
     */
    public void setClickEvents() {
        //Back button click
        back_icon_settings_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        switch_button_enable_notif.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_counter = enable_notif_counter + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    setSwitchCompatStatus(true);
                    switch_button_enable_notif.setChecked(true);
                    is_Notify_Enable = true;
                }
                else{
                    status = "off";
                    setSwitchCompatStatus(false);
                    switch_button_enable_notif.setChecked(false);
                    is_Notify_Enable = false;
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "ENABLE_NOTIFICATION", status);
                }
            }
        });

        /*switch_button_enable_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_Notify_Enable) {
                    is_Notify_Enable = true;
                    enableNotifications(auth_token, "ENABLE_NOTIFICATION", "on");
                    setSwitchCompatStatus(true);
                    switch_button_enable_notif.setChecked(true);
                } else {
                    is_Notify_Enable = false;
                    enableNotifications(auth_token, "ENABLE_NOTIFICATION", "off");
                    setSwitchCompatStatus(false);
                    switch_button_enable_notif.setChecked(false);
                }
            }
        });*/

        switch_button_player_request.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_player_request = enable_notif_player_request + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_player_request.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_player_request.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "player_request", status);
                }
//                if(enable_notif_player_request > 1)
//                {
//                    enableNotifications(auth_token, "player_request", status);
//                }
            }
        });

        /*switch_button_player_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_player_request) {
                    is_player_request = true;
                    enableNotifications(auth_token, "player_request", "on");
                    switch_button_player_request.setChecked(true);
                } else {
                    is_player_request = false;
                    enableNotifications(auth_token, "player_request", "off");
                    switch_button_player_request.setChecked(false);
                }
            }
        });*/

        switch_button_game_invitations.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_game_invitations = enable_notif_game_invitations + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_game_invitations.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_game_invitations.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "game_invitation", status);
                }
            }
        });



        /*switch_button_game_invitations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_game_invitation) {
                    is_game_invitation = true;
                    enableNotifications(auth_token, "game_invitation", "on");
                    switch_button_game_invitations.setChecked(true);
                } else {
                    is_game_invitation = false;
                    enableNotifications(auth_token, "game_invitation", "off");
                    switch_button_game_invitations.setChecked(false);
                }
            }
        });
*/
        switch_button_game_updates.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_game_updates = enable_notif_game_updates + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_game_updates.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_game_updates.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "game_update", status);
                }
            }
        });

        /*switch_button_game_updates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_game_update) {
                    is_game_update = true;
                    enableNotifications(auth_token, "game_update", "on");
                    switch_button_game_updates.setChecked(true);
                } else {
                    is_game_update = false;
                    enableNotifications(auth_token, "game_update", "off");
                    switch_button_game_updates.setChecked(false);
                }
            }
        });*/

        switch_button_game_edit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_game_edit = enable_notif_game_edit + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_game_edit.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_game_edit.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "game_edit", status);
                }
            }
        });


        /*switch_button_game_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_game_edit) {
                    is_game_edit = true;
                    enableNotifications(auth_token, "game_edit", "on");
                    switch_button_game_edit.setChecked(true);
                } else {
                    is_game_edit = false;
                    enableNotifications(auth_token, "game_edit", "off");
                    switch_button_game_edit.setChecked(false);
                }
            }
        });*/

        switch_button_join_leaving_game.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_join_leaving_game = enable_notif_join_leaving_game + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_join_leaving_game.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_join_leaving_game.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "game_join_leave", status);
                }
            }
        });


        /*switch_button_join_leaving_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_game_join_leave) {
                    is_game_join_leave = true;
                    enableNotifications(auth_token, "game_join_leave", "on");
                    switch_button_join_leaving_game.setChecked(true);
                } else {
                    is_game_join_leave = false;
                    enableNotifications(auth_token, "game_join_leave", "off");
                    switch_button_join_leaving_game.setChecked(false);
                }
            }
        });*/

        switch_button_message_board.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_message_board = enable_notif_message_board + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_message_board.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_message_board.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "message_board", status);
                }
            }
        });


        /*switch_button_message_board.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_message_board) {
                    is_message_board = true;
                    enableNotifications(auth_token, "message_board", "on");
                    switch_button_message_board.setChecked(true);
                } else {
                    is_message_board = false;
                    enableNotifications(auth_token, "message_board", "off");
                    switch_button_message_board.setChecked(false);
                }
            }
        });*/

        switch_button_cancel_game.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_cancel_game = enable_notif_cancel_game + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_cancel_game.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_cancel_game.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "game_cancel", status);
                }
            }
        });


        /*switch_button_cancel_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_game_cancel) {
                    is_game_cancel = true;
                    enableNotifications(auth_token, "game_cancel", "on");
                    switch_button_cancel_game.setChecked(true);
                } else {
                    is_game_cancel = false;
                    enableNotifications(auth_token, "game_cancel", "off");
                    switch_button_cancel_game.setChecked(false);
                }
            }
        });*/

        switch_button_nearby_games.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_nearby_games = enable_notif_nearby_games + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_nearby_games.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_nearby_games.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "nearby_game", status);
                }
            }
        });


       /* switch_button_nearby_games.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_nearby_game) {
                    is_nearby_game = true;
                    enableNotifications(auth_token, "nearby_game", "on");
                    switch_button_nearby_games.setChecked(true);
                } else {
                    is_nearby_game = false;
                    enableNotifications(auth_token, "nearby_game", "off");
                    switch_button_nearby_games.setChecked(false);
                }
            }
        });*/

        switch_button_only_primary_games.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_only_primary_games = enable_notif_only_primary_games + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_only_primary_games.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_only_primary_games.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "nearby_game_primary", status);
                }
            }
        });


       /* switch_button_only_primary_games.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_only_primary_game) {
                    is_only_primary_game = true;
                    enableNotifications(auth_token, "nearby_game_primary", "on");
                    switch_button_only_primary_games.setChecked(true);
                } else {
                    is_only_primary_game = false;
                    enableNotifications(auth_token, "nearby_game_primary", "off");
                    switch_button_only_primary_games.setChecked(false);
                }
            }
        });*/

        switch_button_all_games.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //enable_notif_all_games = enable_notif_all_games + 1 ;
                String status = "";
                if(isChecked)
                {
                    status = "on";
                    switch_button_all_games.setChecked(true);
                }
                else{
                    status = "off";
                    switch_button_all_games.setChecked(false);
                }

                if(buttonView.isPressed())
                {
                    enableNotifications(auth_token, "nearby_game_all", status);
                }
            }
        });


       /* switch_button_all_games.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1 ;
                if (!is_all_games) {
                    is_all_games = true;
                    enableNotifications(auth_token, "nearby_game_all", "on");
                    switch_button_all_games.setChecked(true);
                } else {
                    is_all_games = false;
                    enableNotifications(auth_token, "nearby_game_all", "off");
                    switch_button_all_games.setChecked(false);
                }
            }
        });
*/
    }

    /**
     * Initializing views
     */
    public void initViews() {
        ButterKnife.inject(this);
        snackBar = new SnackBar(this);

        //preferences = getSharedPreferences("Settings_Data", Context.MODE_PRIVATE);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        if (preferences.contains("auth_token")) {
            auth_token = preferences.getString("auth_token", "");
        }
        editor.putString("auth_token", auth_token);
        editor.apply();

    }

    /**
     * Set enable / Disable for Switch compat button
     */
    public void setSwitchCompatStatus(boolean enable_notification_status) {
        if (enable_notification_status) {
            switch_button_player_request.setEnabled(true);
            switch_button_game_invitations.setEnabled(true);
            switch_button_game_updates.setEnabled(true);
            switch_button_game_edit.setEnabled(true);
            switch_button_join_leaving_game.setEnabled(true);
            switch_button_message_board.setEnabled(true);
            //switch_button_comments_on_post.setEnabled(true);
            switch_button_cancel_game.setEnabled(true);
            switch_button_nearby_games.setEnabled(true);
            switch_button_only_primary_games.setEnabled(true);
            switch_button_all_games.setEnabled(true);

            //Fetch Notification status from Shared preferences
            fetchNotificationData();
        } else {
            switch_button_player_request.setEnabled(false);
            switch_button_game_invitations.setEnabled(false);
            switch_button_game_updates.setEnabled(false);
            switch_button_game_edit.setEnabled(false);
            switch_button_join_leaving_game.setEnabled(false);
            switch_button_message_board.setEnabled(false);
            //switch_button_comments_on_post.setEnabled(false);
            switch_button_cancel_game.setEnabled(false);
            switch_button_nearby_games.setEnabled(false);
            switch_button_only_primary_games.setEnabled(false);
            switch_button_all_games.setEnabled(false);

            switch_button_player_request.setChecked(false);
            switch_button_game_invitations.setChecked(false);
            switch_button_game_updates.setChecked(false);
            switch_button_game_edit.setChecked(false);
            switch_button_join_leaving_game.setChecked(false);
            switch_button_message_board.setChecked(false);
            //switch_button_comments_on_post.setChecked(false);
            switch_button_cancel_game.setChecked(false);
            switch_button_nearby_games.setChecked(false);
            switch_button_only_primary_games.setChecked(false);
            switch_button_all_games.setChecked(false);

            //Clear Shared preferences
            editor.clear();
            editor.apply();
        }
    }

    /**
     * Fetch Notification status from Shared preferences
     */
    public void fetchNotificationData() {
        if (preferences.contains("PLAYER_REQUEST")) {
            boolean player_request = preferences.getBoolean("PLAYER_REQUEST", false);
            boolean nearby_game = preferences.getBoolean("NEARBY_GAME", false);
            boolean game_invitation = preferences.getBoolean("GAME_INVITATION", false);
            boolean game_update = preferences.getBoolean("GAME_UPDATE", false);
            boolean message_board = preferences.getBoolean("MESSAGE_BOARD", false);
            boolean comment_on_board = preferences.getBoolean("COMMENT_ON_POST", false);
            boolean game_cancel = preferences.getBoolean("GAME_CANCEL", false);
            boolean game_edit = preferences.getBoolean("GAME_EDIT", false);
            boolean game_join_leave = preferences.getBoolean("GAME_JOIN_LEAVE", false);
            boolean only_primary_game = preferences.getBoolean("ONLY_PRIMARY", false);
            boolean all_games = preferences.getBoolean("ALL_GAMES", false);

            if (player_request) {
                switch_button_player_request.setChecked(true);
                is_player_request = true;
            } else {
                switch_button_player_request.setChecked(false);
                is_player_request = false;
            }
            if (game_invitation) {
                switch_button_game_invitations.setChecked(true);
                is_game_invitation = true;
            } else {
                switch_button_game_invitations.setChecked(false);
                is_game_invitation = false;
            }
            if (game_update) {
                switch_button_game_updates.setChecked(true);
                is_game_update = true;
            } else {
                if (!game_edit && !game_join_leave && !message_board && !game_cancel) {
                    switch_button_game_edit.setEnabled(false);
                    switch_button_join_leaving_game.setEnabled(false);
                    switch_button_message_board.setEnabled(false);
                    switch_button_cancel_game.setEnabled(false);
                    switch_button_game_edit.setChecked(false);
                    switch_button_join_leaving_game.setChecked(false);
                    switch_button_message_board.setChecked(false);
                    switch_button_cancel_game.setChecked(false);

                    is_game_update = false;
                } else {
                    switch_button_game_updates.setChecked(false);
                    is_game_update = false;
                }

            }
            if (game_edit) {
                switch_button_game_edit.setChecked(true);
                switch_button_game_edit.setEnabled(true);
                is_game_edit = true;
            } else {
                switch_button_game_edit.setChecked(false);
                is_game_edit = false;
            }
            if (game_join_leave) {
                switch_button_join_leaving_game.setChecked(true);
                switch_button_join_leaving_game.setEnabled(true);
                is_game_join_leave = true;
            } else {
                switch_button_join_leaving_game.setChecked(false);
                is_game_join_leave = false;
            }
            if (message_board) {
                switch_button_message_board.setChecked(true);
                switch_button_message_board.setEnabled(true);
                is_message_board = true;
            } else {
                switch_button_message_board.setChecked(false);
                is_message_board = false;
            }
            /*if(comment_on_board) {
                switch_button_comments_on_post.setChecked(true);
                is_comment_on_board = true;
            }*/
            if (game_cancel) {
                switch_button_cancel_game.setChecked(true);
                switch_button_cancel_game.setEnabled(true);
                is_game_cancel = true;
            } else {
                switch_button_cancel_game.setChecked(false);
                is_game_cancel = false;
            }
            if (nearby_game) {
                switch_button_nearby_games.setChecked(true);
                is_nearby_game = true;
            }
            else if(!is_Notify_Enable)
            {
                switch_button_only_primary_games.setChecked(false);
                switch_button_all_games.setChecked(false);
                switch_button_only_primary_games.setEnabled(false);
                switch_button_all_games.setEnabled(false);

                is_nearby_game = false;
            }
            else {
                if (!only_primary_game && !all_games) {
                    switch_button_only_primary_games.setChecked(false);
                    switch_button_all_games.setChecked(false);
//                    switch_button_only_primary_games.setEnabled(false);
//                    switch_button_all_games.setEnabled(false);
                    switch_button_only_primary_games.setEnabled(true);
                    switch_button_all_games.setEnabled(true);

                    is_nearby_game = false;
                } else {
                    switch_button_nearby_games.setChecked(false);
                    is_nearby_game = false;
                }

            }
            if (only_primary_game) {
                switch_button_only_primary_games.setChecked(true);
                switch_button_only_primary_games.setEnabled(true);
                is_only_primary_game = true;
            } else {
                switch_button_only_primary_games.setChecked(false);
                is_only_primary_game = false;
            }
            if (all_games) {
                switch_button_all_games.setChecked(true);
                switch_button_all_games.setEnabled(true);
                is_all_games = true;
            } else {
                switch_button_all_games.setChecked(false);
                is_all_games = false;
            }
        }
    }

    /**
     * Integrated Notification API
     */
    public void enableNotifications(String auth_token, String notification_type, String notification_status) {
        //Handling the loader state
        LoaderUtility.handleLoader(Settings.this, true);

        //Getting the OkHttpClient
        OkHttpClient client = Singleton.getInstance().getClient();

        //Building the Request Parameters
        Request request = buildApiRequestBody(auth_token, notification_type, notification_status);

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, final IOException e) {

                Settings.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LoaderUtility.handleLoader(Settings.this, false);

                        if(!ApplicationUtility.isConnected(Settings.this)){
                            ApplicationUtility.displayNoInternetMessage(Settings.this);
                        }else{
                            ApplicationUtility.displayErrorMessage(Settings.this, e.getMessage().toString());
                        }

                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String status = "", message = "";
                // Read data on the worker thread
                String responseData = response.body().string();

                try {
                    JSONObject jsonObject = new JSONObject(responseData);

                    if (jsonObject.has("status") && !jsonObject.isNull("status")) {
                        status = jsonObject.getString("status");
                    }
                    if (jsonObject.has("message") && !jsonObject.isNull("message")) {
                        message = jsonObject.getString("message");
                    }

                    if (jsonObject.has("notifications") && !jsonObject.isNull("notifications")) {
                        JSONObject array = jsonObject.getJSONObject("notifications");
                        boolean player_request = (Boolean) array.get("player_request");
                        boolean game_invitation = (Boolean) array.get("game_invitation");
                        boolean nearby_games = (Boolean) array.get("nearby_game");
                        boolean nearby_games_all = (Boolean) array.get("nearby_game_all");
                        boolean nearby_games_primary = (Boolean) array
                                .get("nearby_game_primary");
                        boolean message_board = (Boolean) array.get("message_board");
                        boolean game_update = (Boolean) array.get("game_update");
                        boolean comment_on_post = (Boolean) array.get("comment_on_post");
                        boolean game_cancellation = (Boolean) array.get("game_cancel");
                        boolean game_edit = (Boolean) array.get("game_edit");
                        boolean game_join_leave = (Boolean) array.get("game_join_leave");

                        //Store Notification data in shared preferences
                        storeNotificationDataInSharedPref(player_request, nearby_games, game_invitation,
                                game_update, message_board, comment_on_post,
                                game_cancellation, game_edit, game_join_leave,
                                nearby_games_primary, nearby_games_all);
                    }

                } catch (Exception exp) {
                    exp.printStackTrace();
                }


                final String finalStatus = status;
                final String finalMessage = message;

                Settings.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handling the loader stateN
                        LoaderUtility.handleLoader(Settings.this, false); 

                        if (finalStatus.equalsIgnoreCase("success")) {
                            fetchNotificationData();

                            if(enable_notif_counter > 1) {
                                snackBar.setSnackBarMessage(finalMessage);
                            }
                            enable_notif_counter = enable_notif_counter + 2 ;
                        } else {
                            snackBar.setSnackBarMessage(finalMessage);
                        }

                    }
                });

            }
        });
    }

    /**
     * Function to build the Login APi request parameters
     *
     * @return APi request
     */
    public Request buildApiRequestBody(String auth_token, String notification_type, String notification_status) {

        HttpUrl.Builder urlBuilder = null;
        if (notification_type.equalsIgnoreCase("ENABLE_NOTIFICATION")) {
            urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "/users/" + auth_token + "/" + "push_notification/" + notification_status).newBuilder();
        } else {
            urlBuilder = HttpUrl.parse(EndPoints.BASE_URL + "/users/" + auth_token + "/" + "push_notification/" + notification_status).newBuilder();
            urlBuilder.addQueryParameter("notification_type", notification_type);
        }

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", RequestBody.create(null, new byte[0]))
                .build();

        return request;
    }

    /**
     * Function to handle the progress loader
     *
     * @param status true/false
     */
    public void handleLoader(boolean status) {
        if (status) {
            dialog = ProgressDialog
                    .show(Settings.this, "", "Please wait ...");
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.setCancelable(false);
        } else {
            dialog.dismiss();
        }
    }

    /**
     * Store Notification data in shared preferences
     */
    public void storeNotificationDataInSharedPref(boolean player_request, boolean nearby_game, boolean game_invitation,
                                                  boolean game_update, boolean message_board, boolean comment_on_post,
                                                  boolean game_cancel, boolean game_edit, boolean game_join_leave,
                                                  boolean nearby_game_primary, boolean nearby_game_all) {
        editor.putBoolean("PLAYER_REQUEST", player_request);
        editor.putBoolean("NEARBY_GAME", nearby_game);
        editor.putBoolean("GAME_INVITATION", game_invitation);
        editor.putBoolean("GAME_UPDATE", game_update);
        editor.putBoolean("MESSAGE_BOARD", message_board);
        editor.putBoolean("COMMENT_ON_POST", comment_on_post);
        editor.putBoolean("GAME_CANCEL", game_cancel);
        editor.putBoolean("GAME_EDIT", game_edit);
        editor.putBoolean("GAME_JOIN_LEAVE", game_join_leave);
        editor.putBoolean("ONLY_PRIMARY", nearby_game_primary);
        editor.putBoolean("ALL_GAMES", nearby_game_all);
        editor.putBoolean("PAGE_LOAD_FIRST_TIME", true);
        editor.putString("auth_token", auth_token);
        if (is_Notify_Enable) {
            editor.putBoolean("NOTIFICATION_ENABLE", true);
        } else {
            editor.putBoolean("NOTIFICATION_ENABLE", false);
        }

        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = getSharedPreferences("Settings_Data", Context.MODE_PRIVATE);
        editor = preferences.edit();

        if (preferences.contains("auth_token")) {
            auth_token = preferences.getString("auth_token", "");
        }
        /**
         * First time Notification should enable
         * If data exist in Shared preference tyen fetch from it otherwise it will call API
         */
        if (preferences.contains("NOTIFICATION_ENABLE")) {
            boolean status = preferences.getBoolean("NOTIFICATION_ENABLE", false);
            if (status) {
                switch_button_enable_notif.setChecked(true);
                is_Notify_Enable = true;
                setSwitchCompatStatus(true);
            } else {
                is_Notify_Enable = false;
                switch_button_enable_notif.setChecked(false);
                setSwitchCompatStatus(false);
            }
            enable_notif_counter = enable_notif_counter + 2;
        } else {
            enable_notif_counter = enable_notif_counter + 1 ;
            enableNotifications(auth_token, "ENABLE_NOTIFICATION", "on");
            switch_button_enable_notif.setChecked(true);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
