package com.andolasoft.squadz.fonts;

/**
 * Created by star on 11/8/2016.
 */
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.andolasoft.squadz.R;

/**
 * Created by chris on 17/03/15.
 * For Calligraphy.
 */
public class TextField extends TextView {

    public TextField(final Context context, final AttributeSet attrs) {
        super(context, attrs, R.attr.textFieldStyle);
    }

}
