package com.andolasoft.squadz.fonts;

/**
 * Created by star on 11/8/2016.
 */
import android.app.Application;

import com.andolasoft.squadz.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by chris on 06/05/2014.
 * For Calligraphy.
 */
public class CalligraphyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/proxima_nova_regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .addCustomViewWithSetTypeface(CustomViewWithTypefaceSupport.class)
                        .addCustomStyle(TextField.class, R.attr.textFieldStyle)
                        .build()
        );
    }
}